<?php
@error_reporting(E_NONE);
ini_set("display_errors", 0);
$array_days = array();
$array_passengers = array();
$array_passengers2 = array();
$month_days = array();
$array_years = array();
$array_arrival = array();
$array_days = array();
$array_departure = array();
$airlines1 = array();
$airlines2 = array();
$rooms = array();
$boards = array();
$countries = array();
$stars = array();

if ($statement = $db->query("select * from `aeroport_codes` order by `name_city`")) {
    while ($row = $statement->fetch()) {
        $array_arrival[] = array(
            'id' => $row['name_city'].', '.$row['name_country'].' - '.$row['name_apt'].' ('.$row['code_IATA'].')',
            'name' => $row['name_city'].', '.$row['name_country'].' - '.$row['name_apt'].' ('.$row['code_IATA'].')',
        );
    }
}

$airlines1[] = array(
        'id' => 'Alege',
        'name' => 'Alege',
    );
$airlines2[] = array(
        'id' => 'Alege',
        'name' => 'Alege',
    );
if ($statement = $db->query("select * from `airlines_codes` where `type`=0 order by `name`")) {
    while ($row = $statement->fetch()) {
        $airlines1[] = array(
            'id' => $row['name'],
            'name' => $row['name'],
        );
    }
}

if ($statement = $db->query("select * from `airlines_codes` where `type`=1 order by `name`")) {
    while ($row = $statement->fetch()) {
        $airlines2[] = array(
            'id' => $row['name'],
            'name' => $row['name'],
        );
    }
}

if ($statement = $db->query("select * from `fibula_board_types_data` where `lang`='ro' and `status`=1 order by `name`")) {
    while ($row = $statement->fetch()) {
        $boards[] = array(
            'id' => $row['_id'],
            'name' => $row['name'],
        );
    }
}

if ($statement = $db->query("SELECT 
								p.* ,pd.*
							FROM `xp_cities` p
							inner join `xp_cities_data` pd
							 on p.`id`=pd.`_id` WHERE p.`status`=1 and pd.`name`<>'' and pd.`lang`='en' ORDER BY `name` ASC")) {
    while ($row = $statement->fetch()) {
        $countries[] = array(
            'id' => $row['name'],
            'name' => $row['name'],
        );
    }
}

$array_passenger_type = array(
	array(
		'id'=>1,
		'name'=>'Dl.'
	),
	array(
		'id'=>2,
		'name'=>'Dna.'
	)
);
$clasa=array(
	array(
		'id'=>'All',
		'name'=>'All'
	),
	array(
		'id'=>'First (F)',
		'name'=>'First (F)'
	),
	array(
		'id'=>'Business (C)',
		'name'=>'Business (C)'
	),
	array(
		'id'=>'Economy (Y)',
		'name'=>'Economy (Y)'
	),
	array(
		'id'=>'Economy Low (M)',
		'name'=>'Economy Low (M)'
	),
	array(
		'id'=>'Economy / Coach Premium (W)',
		'name'=>'Economy / Coach Premium (W)'
	),
);
$stars=array(
	array(
		'id'=>'1 stea',
		'name'=>'1 stea'
	),
	array(
		'id'=>'2 stele',
		'name'=>'2 stele'
	),
	array(
		'id'=>'3 stele',
		'name'=>'3 stele'
	),
	array(
		'id'=>'4 stele',
		'name'=>'4 stele'
	),
	array(
		'id'=>'5 stele',
		'name'=>'5 stele'
	)
);
$array_companies = 
	array(
		array(
			'id'=>1,
			'name'=>'Alege'
		),
	);
for (
	$i=1;
	$i<=31;
	$i++
){
	$array_days[$i] = array(
		'id'=>$i,
		'name'=>$i,
	);
};
for (
	$i=0;
	$i<=3;
	$i++
){
	$array_days2[$i] = array(
		'id'=>$i,
		'name'=>$i,
	);
};
for (
	$p=1;
	$p<=9;
	$p++
){
	$array_passengers[$p] = array(
		'id'=>$p,
		'name'=>$p,
	);
};
for (
	$p=1;
	$p<=9;
	$p++
){
	$array_days[$p] = array(
		'id'=>$p,
		'name'=>$p,
	);
};
for (
	$p=0;
	$p<=9;
	$p++
){
	$array_passengers2[$p] = array(
		'id'=>$p,
		'name'=>$p,
	);
};
for (
	$m=1;
	$m<=12;
	$m++
){
	$month_days[$m] = array(
		'id'=>$m,
		'name'=>$m,
	);
};
for (
	$y=1950;
	$y<=2015;
	$y++
){
	$array_years[$y] = array(
		'id'=>$y,
		'name'=>$y,
	);
}
for (
	$m=0;
	$m<=6;
	$m++
){
	$rooms[$m] = array(
		'id'=>$m,
		'name'=>($m==0?'Selecteaza':$m),
	);
};

//print_a($array_days);
$array_form = array(
	'type' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'type cf',
			'elements'=>
			array(
				array(
					'id'=>'passengers',
					'name'=>'',
					'tag'=>'input',
					'type'=>'radio',
					'autocomplete'=>false,
					'options'=>
						array(
							1 => 'Dus - Intors',
							2 => 'Doar dus',
							3 => 'Destinatii multiple',
						), 
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_from' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure cf',
			'elements'=>
			array(
				array(
					'id'=>'departure',
					'name'=>'Plecare din',
					'title'=>'Plecare din',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fl',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'arrival',
					'name'=>'Sosire in',
					'title'=>'Sosire in',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fr',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_from_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-one-way',
					'name'=>'Plecare din',
					'title'=>'Plecare din',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fl',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'arrival-one-way',
					'name'=>'Sosire in',
					'title'=>'Sosire in',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fr',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_from_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-multiple',
					'name'=>'Plecare din',
					'title'=>'Plecare din',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fl',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'arrival-multiple',
					'name'=>'Sosire in',
					'title'=>'Sosire in',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fr',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_from_multiple_another' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-multiple-another',
					'name'=>'Plecare din',
					'title'=>'Plecare din',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fl',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'arrival-multiple-another',
					'name'=>'Sosire in',
					'title'=>'Sosire in',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fr',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_from_multiple_another_1' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-multiple-another-1',
					'name'=>'Plecare din',
					'title'=>'Plecare din',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fl',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'arrival-multiple-another-1',
					'name'=>'Sosire in',
					'title'=>'Sosire in',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'fr',
					'options'=>$array_arrival,
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_type' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-type inline cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-type',
					'name'=>'',
					'tag'=>'input',
					'type'=>'radio',
					'required'=>false,
					'options'=>
						array(
							'Fara escala' => 'Fara escala',
							'Cu escala' => 'Cu escala',
						), 
					'regexp'=>'textarea',
				),
				array(
					'id'=>'booking-class',
					'name'=>'Alege clasa de rezervare',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$clasa, 
					'hclass'=>'booking fr',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_type_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-type inline cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-type-one-way',
					'name'=>'',
					'tag'=>'input',
					'type'=>'radio',
					'required'=>false,
					'options'=>
						array(
							'Fara escala' => 'Fara escala',
							'Cu escala' => 'Cu escala',
						), 
					'regexp'=>'textarea',
				),
				array(
					'id'=>'booking-class-one-way',
					'name'=>'Alege clasa de rezervare',
					'tag'=>'select',
					'options'=>$clasa, 
					'hclass'=>'booking fr',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_type_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-type inline cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-type-multiple',
					'name'=>'',
					'tag'=>'input',
					'type'=>'radio',
					'required'=>false,
					'options'=>
						array(
							'Fara escala' => 'Fara escala',
							'Cu escala' => 'Cu escala',
						), 
					'regexp'=>'textarea',
				),
				array(
					'id'=>'booking-class-multiple',
					'name'=>'Alege clasa de rezervare',
					'tag'=>'select',
					'options'=>$clasa, 
					'hclass'=>'booking fr',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_date' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-date fl cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-date',
					'name'=>'Data plecare',
					'title'=>'Data plecare',
					'tag'=>'input',
					'type'=>'text',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'hclass'=>'medium',
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'departure-days',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_date_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-date fl cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-date-one-way',
					'name'=>'Data plecare',
					'title'=>'Data plecare',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'departure-days-one-way',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_date_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-date fl cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-date-multiple',
					'name'=>'Data plecare',
					'title'=>'Data plecare',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				array(
					'id'=>'departure-days-multiple',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_date_multiple_another' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-date fl cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-date-multiple-another',
					'name'=>'Data plecare',
					'title'=>'Data plecare',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'departure-days-multiple-another',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'departure_date_multiple_another_1' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'departure-date fl cf',
			'elements'=>
			array(
				array(
					'id'=>'departure-date-multiple-another-1',
					'name'=>'Data plecare',
					'title'=>'Data plecare',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'departure-days-multiple-another-1',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'return_date' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'return-date fr cf',
			'elements'=>
			array(
				array(
				'id'=>'return-date',
				'name'=>'Data retur',
				'tag'=>'input',
				'type'=>'text',
				'hclass'=>'medium',
				'required'=>false,
				'value'=>date('d/m/Y', strtotime("+1 days")),
				'regexp'=>'textarea',
				),
				array(
					'id'=>'return-days',
					'name'=>'+ / -',
					'tag'=>'select',
					'type'=>'text',
					'options'=>$array_days2,
					'hclass'=>'small',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'passengers_type' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers inline cf', 
			'elements'=>
			array(
				array(
					'id'=>'adults',
					'name'=>'Adulti',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'children',
					'name'=>'Copii (2 -11.99)',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'infant',
					'name'=>'Infanti( &lt; 2)',
					'type'=>'text',
					'tag'=>'select',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'passengers_type_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers inline cf', 
			'elements'=>
			array(
				array(
					'id'=>'adults-one-way',
					'name'=>'Adulti',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'children-one-way',
					'name'=>'Copii (2 -11.99)',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'infant-one-way',
					'name'=>'Infanti( &lt; 2)',
					'type'=>'text',
					'tag'=>'select',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'passengers_type_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers inline cf', 
			'elements'=>
			array(
				array(
					'id'=>'adults-multiple',
					'name'=>'Adulti',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'children-multiple',
					'name'=>'Copii (2 -11.99)',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'infant-multiple',
					'name'=>'Infanti( &lt; 2)',
					'type'=>'text',
					'tag'=>'select',
					'hclass'=>'medium-small',
					'required'=>false,
					'options'=>$array_passengers2,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'optionals' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'optionals cf',
			'elements'=>
			array(
				array(
					'id'=>'preferred-company-1',
					'name'=>'Companie Aeriana',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines1, 
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'preferred-company-2',
					'name'=>'Companie aeriana low-cost',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines2,
					'required'=>false,
					'regexp'=>'textarea',
				),
//				array(
//					'id'=>'mile-card',
//					'name'=>'Card de mile',
//					'tag'=>'input',
//					'type'=>'text',
//					'hclass'=>'medium',
//					'required'=>false,
//					'regexp'=>'textarea',
//				),
			),
		),
	),
	'optionals_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'optionals cf',
			'elements'=>
			array(
				array(
					'id'=>'preferred-company-1-one-way',
					'name'=>'Companie Aeriana',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines1, 
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'preferred-company-2-one-way',
					'name'=>'Companie aeriana low-cost',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines2,
					'required'=>false,
					'regexp'=>'textarea',
				),
//				array(
//					'id'=>'mile-card-one-way',
//					'name'=>'Card de mile',
//					'tag'=>'input',
//					'type'=>'text',
//					'hclass'=>'medium',
//					'required'=>false,
//					'regexp'=>'textarea',
//				),
			),
		),
	),	
	'optionals_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'optionals cf',
			'elements'=>
			array(
				array(
					'id'=>'preferred-company-1-multiple',
					'name'=>'Companie aeriana',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines1, 
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'preferred-company-2-multiple',
					'name'=>'Companie aeriana low-cost',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'options'=>$airlines2,
					'required'=>false,
					'regexp'=>'textarea',
				),
//				array(
//					'id'=>'mile-card-multiple',
//					'name'=>'Card de mile',
//					'tag'=>'input',
//					'type'=>'text',
//					'hclass'=>'medium',
//					'required'=>false,
//					'regexp'=>'textarea',
//				),
			),
		),
	),
	'passengers_infos' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers-info cf',
			'elements'=>
			array(
				array(
					'id'=>'adults-1',
					'name'=>'1. Adult',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'small',
					'required'=>false,
					'options'=>$array_passenger_type,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'name-1',
					'name'=>'Nume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'first-name-1',
					'name'=>'Prenume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'birthday-1',
					'name'=>'zi',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'small',
					'options'=>$array_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'month-1',
					'name'=>'luna',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$month_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'year-1',
					'name'=>'an',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$array_years,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'passengers_infos_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers-info cf',
			'elements'=>
			array(
				array(
					'id'=>'adults-1-one-way',
					'name'=>'1. Adult',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'small',
					'required'=>false,
					'options'=>$array_passenger_type,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'name-1-one-way',
					'name'=>'Nume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'first-name-1-one-way',
					'name'=>'Prenume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'birthday-1-one-way',
					'name'=>'zi',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'small',
					'options'=>$array_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'month-1-one-way',
					'name'=>'luna',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$month_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'year-1-one-way',
					'name'=>'an',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$array_years,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'passengers_infos_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'passengers-info cf',
			'elements'=>
			array(
				array(
					'id'=>'adults-1-multiple',
					'name'=>'1. Adult',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'small',
					'required'=>false,
					'options'=>$array_passenger_type,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'name-1-multiple',
					'name'=>'Nume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'first-name-1-multiple',
					'name'=>'Prenume',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'birthday-1-multiple',
					'name'=>'zi',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'small',
					'options'=>$array_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'month-1-multiple',
					'name'=>'luna',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$month_days,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'year-1-multiple',
					'name'=>'an',
					'tag'=>'select',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium-small',
					'options'=>$array_years,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'flight_observations' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'plane-observations inline textarea cf',
			'elements'=>
			array(
				array(
					'id'=>'flight-observations',
					'name'=>'Observatii',
					'tag'=>'textarea',
					'maxlenght'=>1000,
					'required'=>false,
					'type'=>'text',
					'regexp'=>'textarea',
				),
			),
		),
	),
	'booking_observations' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'booking-observations textarea cf',
			'elements'=>
			array(
				array(
					'id'=>'booking-observations',
					'name'=>'Observatii hotel',
					'tag'=>'textarea',
					'maxlenght'=>1000,
					'type'=>'text',
					'hclass'=>'inline',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'transfer_observations' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'transfer-observations textarea cf',
			'elements'=>
			array(
				array(
					'id'=>'transfer-observations',
					'name'=>'Observatii transfer',
					'tag'=>'textarea',
					'maxlenght'=>1000,
					'hclass'=>'inline',
					'required'=>false,
					'type'=>'text',
					'regexp'=>'textarea',
				),
			),
		),
	),
	'rent_a_car_observations' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'inline textarea cf',
			'elements'=>
			array(
				array(
					'id'=>'rent-a-car-observations',
					'name'=>'Observatii Rent A Car',
					'tag'=>'textarea',
					'maxlenght'=>1000,
					'required'=>false,
					'type'=>'text',
					'regexp'=>'textarea',
				),
			),
		),
	),
	'insurance_observations' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'inline textarea cf',
			'elements'=>
			array(
				array(
					'id'=>'insurance-observations',
					'name'=>'Observatii Asigurare',
					'tag'=>'textarea',
					'maxlenght'=>1000,
					'type'=>'text',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'check_in' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'check-in cf',
			'elements'=>
			array(
				array(
					'id'=>'checkin',
					'name'=>'Check-in',
					'title'=>'Check-in',
					'tag'=>'input',
					'hclass'=>'inline medium',
					'required'=>false,
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'type'=>'text',
					'regexp'=>'textarea',
				),
				array(
					'id'=>'checkout',
					'name'=>'Check-out',
					'title'=>'Check-out',
					'tag'=>'input',
					'hclass'=>'inline big medium',
					'required'=>false,
					'type'=>'text',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				array(
					'id'=>'hotel',
					'name'=>'Categorie hotel',
					'title'=>'Categorie hotel',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium-small2 inline',
					'required'=>false,
					'options'=>$stars,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'room-nr',
					'name'=>'Nr.Camere',
					'title'=>'Nr.Camere',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'options'=>$rooms,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'meals',
					'name'=>'Tip masa',
					'title'=>'Tip masa',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'options'=>$boards,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'locations',
					'name'=>'Oras',
					'title'=>'Oras',
					'tag'=>'select',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'options'=>$countries,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'transfer' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'cf',
			'elements'=>
			array(
				array(
					'id'=>'transfer-type',
					'name'=>'Doriti transfer',
					'tag'=>'input',
					'type'=>'radio',
					'hclass'=>'inline',
					'options'=>
						array(
							'Da' => 'Da',
							'Nu' => 'Nu',
						), 
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'check-in-date',
					'name'=>'Transfer Aeroport Hotel',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'inline big medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				array(
					'id'=>'check-out-date',
					'name'=>'Transfer Hotel Aeroport',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'inline big medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				
			),
		),
	),
	'rent_a_car' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'cf',
			'elements'=>
			array(
				array(
					'id'=>'rentacar-type',
					'name'=>'Doriti inchiriere masina',
					'tag'=>'input',
					'type'=>'radio',
					'hclass'=>'inline',
					'options'=>
						array(
							'Da' => 'Da',
							'Nu' => 'Nu',
						), 
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'car-take',
					'name'=>'Preluare',
					'tag'=>'input',
					'type'=>'text',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'hclass'=>'medium',
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'take-from',
					'name'=>'Preluare de la',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium big',
					'required'=>false,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'car-give',
					'name'=>'Predare',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'value'=>date('d/m/Y', strtotime("+13 days")),
					'required'=>false,
					'regexp'=>'textarea',
				),
				
				array(
					'id'=>'give-to',
					'name'=>'Predare la',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'insurance' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'cf',
			'elements'=>
			array(
				array(
					'id'=>'insurance-on',
					'name'=>'De la',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'medium',
					'required'=>false,
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				array(
					'id'=>'insurance-to',
					'name'=>'Pana la',
					'tag'=>'input',
					'type'=>'text',
					'required'=>false,
					'hclass'=>'medium',
					'value'=>date('d/m/Y', strtotime("+1 days")),
					'regexp'=>'textarea',
				),
				array(
					'id'=>'insurance-type',
					'name'=>'',
					'tag'=>'input',
					'type'=>'radio',
					'hclass'=>'inline',
					'options'=>
						array(
							'Medicala' => 'Medicala',
							'Medicala + Bagaje + Anularea calatoriei' => 'Medicala + Bagaje + Anularea calatoriei',
							'Medicala + Bagaje + Anularea calatoriei + Pierderea conexiuni' => 'Medicala + Bagaje + Anularea calatoriei + Pierderea conexiuni',
						), 
					'required'=>false,
					'regexp'=>'textarea',
				),
			),
		),
	),
	'contact' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'inline contact cf',
			'elements'=>
			array(
				array(
					'id'=>'name',
					'name'=>'Nume / Prenume',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'email',
					'name'=>'E-mail',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'phone',
					'name'=>'Telefon',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'observations',
					'name'=>'Observatii',
					'tag'=>'textarea',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>false,
					'maxlenght'=> 1000,
					'regexp'=>'textarea',
				),
			), 
		),
	),
	'contact_one_way' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'inline contact cf',
			'elements'=>
			array(
				array(
					'id'=>'name-one-way',
					'name'=>'Nume / Prenume',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'email-one-way',
					'name'=>'E-mail',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'phone-one-way',
					'name'=>'Telefon',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'observations-one-way',
					'name'=>'Observatii',
					'tag'=>'textarea',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>false,
					'maxlenght'=> 1000,
					'regexp'=>'textarea',
				),
			), 
		),
	),
	'contact_multiple' =>
	array(
		array(
			'wrapper'=>'paragraph',
			'class'=>'inline contact cf',
			'elements'=>
			array(
				array(
					'id'=>'name-multiple',
					'name'=>'Nume / Prenume',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'email-multiple',
					'name'=>'E-mail',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'phone-multiple',
					'name'=>'Telefon',
					'tag'=>'input',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>true,
					'regexp'=>'textarea',
				),
				array(
					'id'=>'observations-multiple',
					'name'=>'Observatii',
					'tag'=>'textarea',
					'type'=>'text',
					'hclass'=>'inline big',
					'required'=>false,
					'maxlenght'=> 1000,
					'regexp'=>'textarea',
				),
			), 
		),
	),
);


if(
	isset($_POST['validation']) &&
	$_POST['validation']!=1
){
		if(
			isset($_POST['act'])
		){
			if(
				$_POST['act']=='avion'
			){

			 
			
			#error checking
			$error = $form->validate($form_contact_one_way,$_POST);
			
		
			
			#insert data
			if(
				!isset($error) ||
				empty($error)
			){
				
					$vars=
					array_merge(
						$_POST,
						array('date'=>date('d-m-y H:i:s',$_POST['validation']))
					);
			
//					print_a($_POST);
//					die();
					if ($_POST['passengers']==1) {
						$vars['traseu']='Dus - Intors';
						$vars['plecared']='<p><strong>Data plecare:</strong> '.$_POST['departure-date'].' '.($_POST['departure-days']>0?' +/- '.$_POST['departure-days'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport plecare:</strong> '.$_POST['departure'].'</p>';
						$vars['plecared'].='<p><strong>Data intoarcere: </strong> '.$_POST['return-date'].' '.($_POST['return-days']>0?' +/- '.$_POST['return-days'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport intoarcere: </strong> '.$_POST['arrival'].'</p>';

						$vars['aeriana']='';
						
						if ($_POST['preferred-company-1']!='Alege') {
							$vars['aeriana'].='<p><strong>Companie aeriana linie:</strong> '.$_POST['preferred-company-1'].'</p>';
						}
						
						if ($_POST['preferred-company-2']!='Alege') {
							$vars['aeriana'].='<p><strong>Companie aeriana low-cost:</strong> '.$_POST['preferred-company-2'].'</p>';
						}

						if ($_POST['passengers']==1) {
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if ($_POST['passengers']==2) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-one-way'])&&$_POST['name-'.$i.'-one-way']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-one-way'].' '.$_POST['first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-one-way'].'/'.$_POST['month-'.$i.'-one-way'].'/'.$_POST['year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-one-way'])&&$_POST['children-name-'.$i.'-one-way']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-one-way'].' '.$_POST['children-first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-one-way'].'/'.$_POST['children-month-'.$i.'-one-way'].'/'.$_POST['children-year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
					
						if ($_POST['passengers']==3) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-multiple'])&&$_POST['name-'.$i.'-multiple']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-multiple'].' '.$_POST['first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-multiple'].'/'.$_POST['month-'.$i.'-multiple'].'/'.$_POST['year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-multiple'])&&$_POST['children-name-'.$i.'-multiple']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-multiple'].' '.$_POST['children-first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-multiple'].'/'.$_POST['children-month-'.$i.'-multiple'].'/'.$_POST['children-year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if (empty($vars['childreni'])) {
							$vars['childreni']='';
						}
						
						if (empty($vars['infanti'])) {
							$vars['infanti']='';
						}
						
						if ($_POST['passengers']==1) {
							$vars['traseu']='Dus - Intors';
						} elseif ($_POST['passengers']==2) {
							$vars['traseu']='Doar dus';
						} elseif ($_POST['passengers']==3) {
							$vars['traseu']='Destinatii multiple';
						}
						
						
						if ($_POST['room-nr']>0) {
							$vars['cazare']='<p style="font-size:16px;color:blue;"><strong>Cazare</strong></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-in:</strong> '.$_POST['checkin'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-out:</strong> '.$_POST['checkout'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Categorie hotel:</strong> '.$_POST['hotel'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Nr. camere:</strong> '.$_POST['room-nr'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Tip masa:</strong> '.$_POST['meals'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Preferinta locatie:</strong> '.$_POST['locations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Observatii cazare:</strong> '.$_POST['booking-observations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							
							
						} else {
							$vars['cazare'].='<p></p>';
						}
						
						if ($_POST['transfer-type']=='Da') {
							$vars['transfer']='<p style="font-size:16px;color:blue;"><strong>Transfer</strong></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Aeroport Hotel:</strong> '.$_POST['check-in-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Hotel Aeroport:</strong> '.$_POST['check-out-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Observatii transfer:</strong> '.$_POST['transfer-observations'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
						} else {
							$vars['transfer'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if (isset($_POST['insurance-type'])) {
							$vars['insurance']='<p style="font-size:16px;color:blue;"><strong>Asigurare</strong></p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>De la data:</strong> '.$_POST['insurance-on'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Pana la data:</strong> '.$_POST['insurance-to'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Tip asigurare:</strong> '.$_POST['insurance-type'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Observatii asigurare:</strong> '.$_POST['insurance-observations'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p></p>';
						} else {
							$vars['insurance'].='<p></p>';
						}
					
					} elseif ($_POST['passengers']==2) {
						$vars['traseu']='Doar dus';
						
						$vars['flight-observations']=$_POST['flight-observations2'];
						
						$vars['plecared']='<p><strong>Data plecare:</strong> '.$_POST['departure-date-one-way'].' '.($_POST['departure-days-one-way']>0?' +/- '.$_POST['departure-days-one-way'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport plecare:</strong> '.$_POST['departure-one-way'].'</p>';
						$vars['plecared'].='<p><strong>Aeroport sosire: </strong> '.$_POST['arrival-one-way'].'</p>';
						
						$vars['preferred-company-1']=$_POST['preferred-company-1-one-way'];
						$vars['preferred-company-2']=$_POST['preferred-company-2-one-way'];
						
						$vars['aeriana']='';
						
						if ($_POST['preferred-company-1-one-way']!='Alege') {
							$vars['aeriana'].='<p><br /><strong>Companie aeriana linie:</strong> '.$_POST['preferred-company-1-one-way'].'</p>';
						}
						
						if ($_POST['preferred-company-2-one-way']!='Alege') {
							$vars['aeriana'].='<p><strong>Companie aeriana low-cost:</strong> '.$_POST['preferred-company-2-one-way'].'</p>';
						}

						$vars['booking-class']=$_POST['booking-class-one-way'];
						if ($_POST['passengers']==1) {
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if ($_POST['passengers']==2) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-one-way'])&&$_POST['name-'.$i.'-one-way']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-one-way'].' '.$_POST['first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-one-way'].'/'.$_POST['month-'.$i.'-one-way'].'/'.$_POST['year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-one-way'])&&$_POST['children-name-'.$i.'-one-way']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-one-way'].' '.$_POST['children-first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-one-way'].'/'.$_POST['children-month-'.$i.'-one-way'].'/'.$_POST['children-year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
					
						if ($_POST['passengers']==3) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-multiple'])&&$_POST['name-'.$i.'-multiple']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-multiple'].' '.$_POST['first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-multiple'].'/'.$_POST['month-'.$i.'-multiple'].'/'.$_POST['year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-multiple'])&&$_POST['children-name-'.$i.'-multiple']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-multiple'].' '.$_POST['children-first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-multiple'].'/'.$_POST['children-month-'.$i.'-multiple'].'/'.$_POST['children-year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if (empty($vars['childreni'])) {
							$vars['childreni']='';
						}
						
						if (empty($vars['infanti'])) {
							$vars['infanti']='';
						}
						
						if ($_POST['passengers']==1) {
							$vars['traseu']='Dus - Intors';
						} elseif ($_POST['passengers']==2) {
							$vars['traseu']='Doar dus';
						} elseif ($_POST['passengers']==3) {
							$vars['traseu']='Destinatii multiple';
						}
						
						
						if ($_POST['room-nr']>0) {
							$vars['cazare']='<p style="font-size:16px;color:blue;"><strong>Cazare</strong></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-in:</strong> '.$_POST['checkin'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-out:</strong> '.$_POST['checkout'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Categorie hotel:</strong> '.$_POST['hotel'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Nr. camere:</strong> '.$_POST['room-nr'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Tip masa:</strong> '.$_POST['meals'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Preferinta locatie:</strong> '.$_POST['locations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Observatii cazare:</strong> '.$_POST['booking-observations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							
							
						} else {
							$vars['cazare'].='<p></p>';
						}
						
						if ($_POST['transfer-type']=='Da') {
							$vars['transfer']='<p style="font-size:16px;color:blue;"><strong>Transfer</strong></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Aeroport Hotel:</strong> '.$_POST['check-in-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Hotel Aeroport:</strong> '.$_POST['check-out-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Observatii transfer:</strong> '.$_POST['transfer-observations'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
						} else {
							$vars['transfer'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if (isset($_POST['insurance-type'])) {
							$vars['insurance']='<p style="font-size:16px;color:blue;"><strong>Asigurare</strong></p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>De la data:</strong> '.$_POST['insurance-on'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Pana la data:</strong> '.$_POST['insurance-to'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Tip asigurare:</strong> '.$_POST['insurance-type'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Observatii asigurare:</strong> '.$_POST['insurance-observations'].'</p>';
							$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p></p>';
						} else {
							$vars['insurance'].='<p></p>';
						}
						
					} elseif ($_POST['passengers']==3) {
						$vars['traseu']='Destinatii multiple';
						
						$vars['flight-observations']=$_POST['flight-observations3'];
						
						$vars['plecared']='<p><strong>Data plecare:</strong> '.$_POST['departure-date-multiple'].' '.($_POST['departure-days-multiple']>0?' +/- '.$_POST['departure-days-multiple'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport plecare:</strong> '.$_POST['departure-multiple'].'</p>';
						//$vars['plecared'].='<p><strong>Data intoarcere: </strong> '.$_POST['return-date-multiple'].'</p>';
						$vars['plecared'].='<p><strong>Aeroport Sosire: </strong> '.$_POST['arrival-multiple'].'</p>';

						$vars['plecared'].='<p><strong>Data plecare:</strong> '.$_POST['departure-date-multiple-another'].' '.($_POST['departure-days-multiple-another']>0?' +/- '.$_POST['departure-days-multiple-another'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport plecare:</strong> '.$_POST['departure-multiple-another'].'</p>';
						//$vars['plecared'].='<p><strong>Data intoarcere: </strong> '.$_POST['return-date-multiple'].'</p>';
						$vars['plecared'].='<p><strong>Aeroport Sosire: </strong> '.$_POST['arrival-multiple-another'].'</p>';
					

						$vars['plecared'].='<p><strong>Data plecare:</strong> '.$_POST['departure-date-multiple-another-1'].' '.($_POST['departure-days-multiple-another-1']>0?' +/- '.$_POST['departure-days-multiple-another-1'].' zile':'').'</p>';
						$vars['plecared'].='<p><strong>Aeroport plecare:</strong> '.$_POST['departure-multiple-another-1'].'</p>';
						//$vars['plecared'].='<p><strong>Data intoarcere: </strong> '.$_POST['return-date-multiple'].'</p>';
						$vars['plecared'].='<p><strong>Aeroport Sosire: </strong> '.$_POST['arrival-multiple-another-1'].'</p>';						
						
						$vars['aeriana']='';
							 
						if ($_POST['preferred-company-1-multiple']!='Alege') {
							$vars['aeriana'].='<p><br /><strong>Companie aeriana linie:</strong> '.$_POST['preferred-company-1'].'</p>';
						}
						
						if ($_POST['preferred-company-2-multiple']!='Alege') {
							$vars['aeriana'].='<p><br /><strong>Companie aeriana low-cost:</strong> '.$_POST['preferred-company-2'].'</p>';
						}
						
						$vars['booking-class']=$_POST['booking-class-multiple'];
						if ($_POST['passengers']==1) {
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if ($_POST['passengers']==2) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-one-way'])&&$_POST['name-'.$i.'-one-way']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-one-way'].' '.$_POST['first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-one-way'].'/'.$_POST['month-'.$i.'-one-way'].'/'.$_POST['year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-one-way'])&&$_POST['children-name-'.$i.'-one-way']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-one-way'].' '.$_POST['children-first-name-'.$i.'-one-way'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-one-way'].'/'.$_POST['children-month-'.$i.'-one-way'].'/'.$_POST['children-year-'.$i.'-one-way'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
					
						if ($_POST['passengers']==3) {
	//						$i=0;
	//						for($i=1;$i<10;$i++) {
	//							if (isset($_POST['name-'.$i.'-multiple'])&&$_POST['name-'.$i.'-multiple']!='') {
	//								$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i.'-multiple'].' '.$_POST['first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i.'-multiple'].'/'.$_POST['month-'.$i.'-multiple'].'/'.$_POST['year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//							if (isset($_POST['children-name-'.$i.'-multiple'])&&$_POST['children-name-'.$i.'-multiple']!='') {
	//								$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i.'-multiple'].' '.$_POST['children-first-name-'.$i.'-multiple'].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i.'-multiple'].'/'.$_POST['children-month-'.$i.'-multiple'].'/'.$_POST['children-year-'.$i.'-multiple'].'</p>';
	//							}
	//							
	//						}
							$i=0;
							for($i=1;$i<10;$i++) {
								if (isset($_POST['name-'.$i])&&$_POST['name-'.$i]!='') {
									$vars['pasageri'].='<p><br /><strong>Adult '.$i.':</strong> '.$_POST['name-'.$i].' '.$_POST['first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['birthday-'.$i].'/'.$_POST['month-'.$i].'/'.$_POST['year-'.$i].'</p>';
								}
								
								if (isset($_POST['children-name-'.$i])&&$_POST['children-name-'.$i]!='') {
									$vars['childreni'].='<p><br /><strong>Copil '.$i.':</strong> '.$_POST['children-name-'.$i].' '.$_POST['children-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['children-birthday-'.$i].'/'.$_POST['children-month-'.$i].'/'.$_POST['children-year-'.$i].'</p>';
								}
								
								if (isset($_POST['infants-name-'.$i])&&$_POST['infants-name-'.$i]!='') {
									$vars['infanti'].='<p><br /><strong>Infant '.$i.':</strong> '.$_POST['infants-name-'.$i].' '.$_POST['infants-first-name-'.$i].' | <strong>Data nasterii:</strong> '.$_POST['infants-birthday-'.$i].'/'.$_POST['infants-month-'.$i].'/'.$_POST['infants-year-'.$i].'</p>';
								}
							}
						}
						
						if (empty($vars['childreni'])) {
							$vars['childreni']='';
						}
						
						if (empty($vars['infanti'])) {
							$vars['infanti']='';
						}
						
						if ($_POST['passengers']==1) {
							$vars['traseu']='Dus - Intors';
						} elseif ($_POST['passengers']==2) {
							$vars['traseu']='Doar dus';
						} elseif ($_POST['passengers']==3) {
							$vars['traseu']='Destinatii multiple';
						}
						
						
						if ($_POST['room-nr']>0) {
							$vars['cazare']='<p style="font-size:16px;color:blue;"><strong>Cazare</strong></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-in:</strong> '.$_POST['checkin'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Check-out:</strong> '.$_POST['checkout'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Categorie hotel:</strong> '.$_POST['hotel'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Nr. camere:</strong> '.$_POST['room-nr'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Tip masa:</strong> '.$_POST['meals'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Preferinta locatie:</strong> '.$_POST['locations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p><strong>Observatii cazare:</strong> '.$_POST['booking-observations'].'</p>';
							$vars['cazare'].='<p></p>';
							$vars['cazare'].='<p></p>';
							
							
						} else {
							$vars['cazare'].='<p></p>';
						}
						
						if ($_POST['transfer-type']=='Da') {
							$vars['transfer']='<p style="font-size:16px;color:blue;"><strong>Transfer</strong></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Aeroport Hotel:</strong> '.$_POST['check-in-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Transfer Hotel Aeroport:</strong> '.$_POST['check-out-date'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p><strong>Observatii transfer:</strong> '.$_POST['transfer-observations'].'</p>';
							$vars['transfer'].='<p></p>';
							$vars['transfer'].='<p></p>';
						} else {
							$vars['transfer'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if ($_POST['rentacar-type']=='Da') {
							$vars['rentacar']='<p style="font-size:16px;color:blue;"><strong>Rent a car</strong></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare la data:</strong> '.$_POST['car-take'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Preluare din:</strong> '.$_POST['take-from'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare la data:</strong> '.$_POST['car-give'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Predare din:</strong> '.$_POST['give-to'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p><strong>Observatii rent a car:</strong> '.$_POST['rent-a-car-observations'].'</p>';
							$vars['rentacar'].='<p></p>';
							$vars['rentacar'].='<p></p>';
						} else {
							$vars['rentacar'].='<p></p>';
						}
						
						if (isset($_POST['insurance-type'])) {
							$vars['insurance']='<p style="margin:1px; padding:1px;font-size:16px;color:blue;"><strong>Asigurare</strong></p>';
							//$vars['insurance'].='<p></p>';
							//$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>De la data:</strong> '.$_POST['insurance-on'].'</p>';
							//$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Pana la data:</strong> '.$_POST['insurance-to'].'</p>';
							//$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Tip asigurare:</strong> '.$_POST['insurance-type'].'</p>';
							//$vars['insurance'].='<p></p>';
							$vars['insurance'].='<p><strong>Observatii asigurare:</strong> '.$_POST['insurance-observations'].'</p>';
							//$vars['insurance'].='<p></p>';
							//$vars['insurance'].='<p></p>';
						} else {
							$vars['insurance'].='<p></p>';
						}
					}
					
					template_mail('avion',array('cosmin@computerfun.ro','travel@clickandgo.ro'),$vars,$config->site_email);
					
					template_mail('avion',$_POST['email-one-way'],$vars,$config->site_email);
					//template_mail('autoreply',$_POST['email'],$vars,$config->site_email);
		
					exit(header("Location: {$$page_info['link']}#action,{$page_info['constructor']},success"));
				
			}
		
			else{
				$globals['window'] = "Window.init( \"".tr("Va rugam completati datele de contact!")."<br/><br/>".implode('<br/><br/>',$error)."\", '".tr('Actiune nereusita!')."', 'error' )";
			}
		}		
	}
}

foreach( $array_form as $k => $v ){
    $page_info['form_' . $k] = $form->factory( $v, $_REQUEST );
}
