<?php
if( ip() == '89.33.79.179' ){

#start cancelReservation
// 	$id_order = $_GET['order'];
	
// 	$order = $db->fetchRow("
// 		select *
// 		from `xp_orders`
// 		where `id` = ? ",
// 		$id_order
// 	);
	
// 	$order['cancellationPolicyOmega'] = unserialize($order['cancellationPolicyOmega']);
	
// // 	print_a($order);
	
// 	$continue = true;
	
// 	#options parameters for SoapClient() request
// 	$option = array(
// 		'compression' => 1,
// 		'trace' => 1,
// 		'connection_timeout' => 180
// 	);

// 	#try to connect with SoapClient
// 	try {
// 		$Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
// 	}
// 	catch (Exception $e) {
// 		$continue = false;
// 	}
	
	
// 	#if SoapClient() connection succeeded check the reservation to see the current status
// 	#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
// 	if( $continue ){
	
// 		$checkReservation = array(
// 			'PartnerCode' => 'mondialvoyages',
// 			'PlatformName' => 'mondialvoyages',
// 			'Username' => 'mondialvoyages',
// 			'Password' => 'gr$sd45Ds8vr',
// 			'Version' => 1,
// 			'ReservationReference' => $order['reservationReferenceOmega']
// 		);
	
// 		try {
// 			$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
// 		}
// 		catch (SoapFault $Exception) {
// 			$continue = false;
// 		}
// 	}
	
// 	#verify if CheckReservesion() method returned an error or status of reservation is not Requires confirmation
// 	if( $continue && (
// 		isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ||
// 		! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status)
// 		)
// 	){
// 		$continue = false;
// 	}
// 	else {
		
// 		$order_status = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status;
		
// 		if( $order['statusOmega'] != $order_status ){
			
// 			$db->query("
// 				update `xp_orders`
// 				set `statusOmega` = :statusOmega
// 				where `id` = :id_order ",
// 				array(
// 					'statusOmega' => $order_status,
// 					'id_order' => $id_order
// 				)
// 			);
// 		}
// 	}
	
// 	$cancellation_policy = false;	
// 	if( $continue ){
		
// 		if( $order_status == 'Confirmed' ){

// 			if( $order['cancellationPolicyOmega'] ){
// 				foreach( $order['cancellationPolicyOmega'] as $c ){
// 					if( $c->Price > 0 ){
						
// 						$from_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $c->FromDate );
// 						$to_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $c->ToDate );
						
// 						if( time() >= $from_date->format('U') && time() <= $to_date->format('U') ){
							
// 							$continue = false;
// 							$cancellation_policy = $c;
							
// 							break;
// 						}
// 					}
// 				}	
// 			}
// 		}
// 		elseif( in_array($order_status, array('Canceled', 'Pending cancelation')) ){
// 			$continue = false;
// 		}
// 	}
	
// 	if( $continue ){
		
// 		$cancelReservation = array(
// 			'PartnerCode' => 'mondialvoyages',
// 			'PlatformName' => 'mondialvoyages',
// 			'Username' => 'mondialvoyages',
// 			'Password' => 'gr$sd45Ds8vr',
// 			'Version' => 1,
// 			'ReservationReference' => $order['reservationReferenceOmega']
// 		);
		
// 		try {
// 			$cancelReservationResultOmega = $Client->CancelReservation($cancelReservation);
// 		}
// 		catch (SoapFault $Exception) {
// 			$continue = false;
// 		}
		
// 		$db->query("
// 			update `xp_orders_products`
// 			set `cancelReservationParametersOmega` = :cancelReservationParametersOmega, `cancelReservationResultOmega` = :cancelReservationResultOmega
// 			where `id_order` = :id_order ",
// 			array(
// 				'cancelReservationParametersOmega' => serialize($cancelReservation),
// 				'cancelReservationResultOmega' => isset($cancelReservationResultOmega) ? serialize($cancelReservationResultOmega) : '',
// 				'id_order' => $id_order
// 			)
// 		);
		
// 		if( isset($cancelReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
		
// 			$db->query("
// 				update `xp_orders`
// 				set `errorMessageCancelReservationOmega` = :errorMessageCancelReservationOmega
// 				where `id` = :id_order ",
// 				array(
// 					'errorMessageCancelReservationOmega' => $cancelReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
// 					'id_order' => $id_order
// 				)
// 			);
// 		}
		
// 		elseif( ! isset($cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->Status) ){
			
// 			$db->query("
// 				update `xp_orders`
// 				set `errorMessageCancelReservationOmega` = :errorMessageCancelReservationOmega
// 				where `id` = :id_order ",
// 				array(
// 					'errorMessageCancelReservationOmega' => 'Clickandgo Error: Cancellation could not be performed. Omega did not return any error or status.',
// 					'id_order' => $id_order
// 				)
// 			);
// 		}
		
// 		elseif( isset($cancelReservationResultOmega) ){
			
// 			$db->query("
// 				update `xp_orders`
// 				set `statusOmega` = :statusOmega
// 				where `id` = :id_order ",
// 				array(
// 					'statusOmega' => $cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->Status,
// 					'id_order' => $id_order
// 				)
// 			);
			
// 			$db->query("
// 				update `xp_orders_products`
// 				set `canceledTotalOmega` = :canceledTotalOmega
// 				where `id_order` = :id_order ",
// 				array(
// 					'canceledTotalOmega' => $cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->ReservationResponseTotalAmount->Total,
// 					'id_order' => $id_order
// 				)
// 			);
// 		}
// 	}

// 	print_a($checkReservationResultOmega);
// 	print_a($order_status);
// 	var_dump($continue);
// 	print_a($cancellation_policy);
// 	print_a($cancelReservationResultOmega);
#end cancelReservation
	

	
// 	$sejururi = array();
	
	/* $statement = $db->query("
		select `id`
		from `xp_sejur`
		where `code` LIKE '%fibula-%'
		order by `id`	
	");
	
	while( $sej = $statement->fetch() ){
		$sejururi[] = $sej['id'];
	}
	
	print_a($sejururi); */
// 	$db->query("delete from `xp_sejur_cost` where `id_sejur` in (".implode(',', $sejururi).")");
	
	
// 	for($i = 0; $i < 5; $i++){
// 		print_a($i);
		
// // 		sleep(2);
// 	}
	
	
// 	print_a('delete from `xp_sejur_cost` where `id_sejur` in ('.implode(',',$sejururi).')');

// 	$test = $db->fetchAll("select `id` from `xp_sejur_cost` where `id_sejur` = 1053 limit 10");
	
// 	print_a($test);
	
// 	print_a($sejururi);
	
// 	$sejururi = implode(',', $sejururi);

// 	print_a($sejururi);
	
// 	print_a($db->fetchOne("select count(`id`) as count from `xp_sejur_cost`"));
	
// 	$db->query("delete from `xp_sejur_cost` where `id` = 1");
	
// 	print_a($db->fetchOne("select count(`id`) as count from `xp_sejur_cost`"));
	
	/* $statement = $db->fetchAll("
		select distinct s.`id`, sc.`availability_start`, sc.`availability_end`
		from
			`xp_sejur` s
			left join (
				select `id_sejur`, `availability_start`, `availability_end`
				from `xp_sejur_cost` ssc
				where `availability_end` = (
					select max(`availability_end`)
					from `xp_sejur_cost`
					where `id_sejur` = ssc.`id_sejur`
				)
				limit 1
			) as sc
				on sc.`id_sejur` = s.`id`
	"); */
	
// 	print_a($statement);
	
	#start
// 	$ceva = microtime(true);
	
// 	$statement = $db->query("
// 		select distinct s.`id`
// 		from
// 			`xp_sejur` s
// 			inner join `xp_sejur_cost` sc
// 				on sc.`id_sejur` = s.`id`
// 		order by `id` asc
// 	");
	
// 	while( $sej = $statement->fetch() ){
		
// 		$sej['max'] = $db->fetchRow("
// 			select sc.`id`, sc.`availability_start`, sc.`availability_end`
// 			from `xp_sejur_cost` sc
// 			where
// 				sc.`id_sejur` = :id_sejur and
// 				sc.`availability_end` = (
// 					select max(`availability_end`)
// 					from `xp_sejur_cost`
// 					where `id_sejur` = :id_sejur
// 				)
// 			order by sc.`id` desc
// 			limit 1	",
// 			array('id_sejur' => $sej['id'])
// 		);
		
// 		$db->query("
// 			update `xp_sejur`
// 			set
// 				`availability_start` = :availability_start,
// 				`availability_end` = :availability_end
// 			where `id` = :id ",
// 			array(
// 				'availability_start' => $sej['max']['availability_start'],
// 				'availability_end' => $sej['max']['availability_end'],
// 				'id' => $sej['id']
// 			)
// 		);
		
// 		$sejururi[] = $sej;
// 	}
	
// 	print_a($sejururi);
// 	print_a($ceva);
// 	print_a(microtime(true) - $ceva);

	#end
	
	/* print_a($db->fetchAll("
			select sc.`availability_start`, sc.`availability_end`
			from `xp_sejur_cost` sc
			where
				sc.`id_sejur` = :id_sejur and
				sc.`availability_end` = (
					select max(`availability_end`)
					from `xp_sejur_cost` ssc
					where
						ssc.`id_sejur` = :id_sejur and
						`id` = (
							select max(`id`)
							from `xp_sejur_cost`
							where
								`id_sejur` = :id_sejur and
								`availability_end` = ssc.`availability_end`
						)
				)
				
			",
			array('id_sejur' => 1056))); */
	
// 	print_a($sejururi);
	
// 	print_a(count($sejururi));
// 	print_a($sejururi);

// 	print_a($db->fetchOne("select count(`id`) as count from `xp_sejur_cost`"));

	/* print_a($db->fetchRow("
			select sc.`availability_start`, sc.`availability_end`
			from `xp_sejur_cost` sc
			where
				sc.`id_sejur` = 1056 and
				sc.`availability_end` = (
					select max(`availability_end`)
					from `xp_sejur_cost`
					where `id_sejur` = 1056
				)
			"
		
	)); */
	
	/* print_a($db->fetchRow("
			select sc.`availability_start`, sc.`availability_end`
			from `xp_sejur_cost` sc
			where
				sc.`id_sejur` = 1056 and
				sc.`availability_end` = (
					select max(`availability_end`)
					from `xp_sejur_cost` ssc
					where
						`id_sejur` = 1056 and
						`id` = (
							select max(`id`)
							from `xp_sejur_cost`
							where
								`id_sejur` = 1056 and
								`availability_end` = ssc.`availability_end`
						)
					
				)
			"
	
		)); */
	
}