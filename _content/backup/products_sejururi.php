<?php
$array_contact =
array(
	array( 'id' => '00', 'name' => 'selecteaza' ),
	array( 'id' => '01', 'name' => 'selecteaza' ),
	array( 'id' => '02', 'name' => 'selecteaza' ),
	array( 'id' => '03', 'name' => 'selecteaza' )
);
$array_form=
array(
	array(
		'wrapper'=>'definition list',
		'class'=>'cf',
		'elements'=>
		array(
			array('id'=>'name','name'=>'Nume, Prenume','title'=>'Nume',	'placeholder'=>'','lang'=>false,'label'=>true,'tag'=>'input','type'=>'text','maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
			array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'','lang'=>false,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
			array('id'=>'phone','name'=>'Telefon','title'=>'Telefon','placeholder'=>'','lang'=>false,'label'=>true,'tag'=>'input','type'=>'text','maxlength'=>200,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
			array('id'=>'contact_mode','name'=>'Cum doriti sa fiti contactat','title'=>'Cum doriti sa fiti contactat?', 'placeholder'=>'','lang'=>true,'label'=>true,'tag'=>'select','type'=>'text','options'=>$array_contact,'maxlength'=>60,'autocomplete'=>false,'autofocus'=>false,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
			array('id'=>'message','name'=>'Mesaj','title'=>'Mesaj','placeholder'=>'','lang'=>false,'label'=>true,'tag'=>'textarea','maxlength'=>1000,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
		)
	),
);


if(
	isset($_POST['validation']) &&
	$_POST['validation']!=1
){
	$error = $form->validate($array_form,$_POST);

	if(
		empty($error)
	){
		$vars=
		array_merge(
			$_POST,
			array('date'=>date('d-m-y H:i:s',$_POST['validation']))
		);

		template_mail('contact',$config->site_email,$vars,$_POST['email']);
		template_mail('autoreply',$_POST['email'],$vars,$config->site_email);

		exit(header("Location: {$b}#action,{$page_info['constructor']},success"));
	}

	else{
		$globals['window']="_we('".tr("Este necesar sa completati toate campurile pentru a putea continua!")."<br/><br/>".implode('<br/><br/>',$error)."')";
	}
}

$page_info['form'] = $form->factory($array_form);