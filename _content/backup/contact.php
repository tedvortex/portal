<?php

$array_form=
array(
	array(
		'wrapper'=>'definition list',
		'class'=>'cf',
		'elements'=>
		array(
			array('id'=>'name','name'=>'Nume','title'=>'Nume',	'placeholder'=>'nume','lang'=>false,'label'=>false,'tag'=>'input','type'=>'text','maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
			array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'email','lang'=>false,'label'=>false,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
			array('id'=>'phone','name'=>'Telefon','title'=>'Telefon',	'placeholder'=>'telefon','lang'=>false,'label'=>false,'tag'=>'input','type'=>'text','maxlength'=>200,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
		)
	),
	array(
		'wrapper'=>'definition list',
		'class'=>'cf',
		'elements'=>
		array(
			array('id'=>'message','name'=>'Mesaj','title'=>'Mesaj','placeholder'=>'mesaj','lang'=>false,'label'=>false,'tag'=>'textarea','maxlength'=>1000,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
		)
	),
);


if(
	isset($_POST['validation']) &&
	$_POST['validation']!=1
){
	$error = $form->validate($array_form,$_POST);

	if(
		empty($error)
	){
		$vars=
		array_merge(
			$_POST,
			array('date'=>date('d-m-y H:i:s',$_POST['validation']))
		);

		template_mail('contact',$config->site_email,$vars,$_POST['email']);
		template_mail('autoreply',$_POST['email'],$vars,$config->site_email);

		exit(header("Location: {$b}#action,{$page_info['constructor']},success"));
	}

	else{
		$globals['window']="_we('".tr("Este necesar sa completati toate campurile pentru a putea continua!")."<br/><br/>".implode('<br/><br/>',$error)."')";
	}
}

$page_info['form'] = $form->factory($array_form);
?>