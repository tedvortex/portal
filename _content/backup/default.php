<?php

$page_info['slide'] = array();

$select_slide = clone($select['slide']);
$select_slide->where('s.`type` = 0');
$select_slide->where('sd.`lang` = ?',$_SESSION['lang']);

if( $stmt_slide = $db->query($select_slide) )
{
	while( $sl = $stmt_slide->fetch() )
	{
		if( is_file(s.'i/slide/'.$sl['image']) )
		{
			$sl['name'] = $engine->escape($sl['name']);
			$sl['link'] = $sl['link'] ? $sl['link'] : '#';
			$sl['image'] = image_uri('i/slide/'.$sl['image']);

			$page_info['slide'][] = $sl;
		}
	}
}

$page_info['summar'] = array();


$select = $db->select()
			 ->from(array('s' => $t['s']))
			 ->join(array('cr' => $t['c']),
			 		'cr.`id` = s.`id_currency`',
			 		array('code'))
			 ->where('s.`id_parent` = 0')
			 ->where('s.`status` = 1')
			 ->order('s.order asc');

$select_tags = $db->select()
				  ->from(array('st' => $t['s'].'_tags'),
				  		 array('id', 'name', 'short_description','link'))
				  ->order('st.order asc');

#sumar level 1
if( $statement = $db->query($select) ){

	while( $sum = $statement->fetch() ){

		$sum['name'] = $engine->escape($sum['name']);
		$sum['price'] = round($sum['price'], 2);

		$sum['container_class'] = '';
		switch ($sum['type']){
			case 0:
				$sum['container_class'] = 'item oversea';
				break;
			case 1:
				$sum['container_class'] = 'booking-detailed-container row';
				break;
			case 2:
				$sum['container_class'] = 'tour-detailed-container row';
				break;
			case 3:
				$sum['container_class'] = 'item cruise';
				break;
		}

		$sum['offers'] = $sum['tags'] = array();

		if( $sum['type'] == 0 ){
			if( is_file(s.'i/promoted/'.$sum['image']) ){
				$sum['image'] = image_uri('i/promoted/'.$sum['image']);
			}
			else{
				$sum['image'] = '';
			}
		}

		$select_tags->reset(Zend_Db_Select::WHERE)
					->where('st.`id_summar` = ?', $sum['id'])
					->where('st.`status` = 1');

		$sum['tags'] = $db->fetchAll($select_tags);
				
		//$sum['tags'] = implode(' / ', $sum['tags']);

		//print_a($sum['tags']);
		#sumar level 2
		$select->reset(Zend_Db_Select::WHERE)
			   ->where('s.`id_parent` = ?', $sum['id'])
			   ->where('s.`status` = 1');

		if( $statement_sec = $db->query($select) ){

			while( $sec_sum = $statement_sec->fetch() ){

				$sec_sum['name'] = $engine->escape($sec_sum['name']);
				$sec_sum['price'] = round($sec_sum['price'], 2);

				if( $sum['type'] != 0 ){
					if( is_file(s.'i/promoted/'.$sec_sum['image']) ){
						$sec_sum['image'] = image_uri('i/promoted/'.$sec_sum['image']);
					}
					else{
						$sec_sum['image'] = '';
					}
				}

				$sec_sum['offers'] = $sec_sum['tags'] = array();

				$select_tags->reset(Zend_Db_Select::WHERE)
							->where('st.`id_summar` = ?', $sec_sum['id'])
							->where('st.`status` = 1');

				if( ($sum['type'] == 3 && $sum['model'] == 0) xor  ($sum['type'] == 1 && $sum['model'] == 0)){
					$sec_sum['tags'] = $db->fetchAll($select_tags);
				}
				else{
					$sec_sum['tags'] = $db->fetchAll($select_tags);
//					$sec_sum['tags'] = $db->fetchPairs($select_tags);
//					$sec_sum['tags'] = implode(' / ', $sec_sum['tags']);
				}

				#sumar level 3
				$select->reset(Zend_Db_Select::WHERE)
					   ->where('s.`id_parent` = ?', $sec_sum['id'])
					   ->where('s.`status` = 1');

				if( $statement_tri = $db->query($select) ){

					while( $tri_sum = $statement_tri->fetch() ){

						$tri_sum['name'] = $engine->escape($tri_sum['name']);
						$tri_sum['price'] = round($tri_sum['price'], 2);
						$tri_sum['offers'] = $tri_sum['tags'] = array();

						$select_tags->reset( Zend_Db_Select::WHERE)
							->where('st.`id_summar` = ?', $tri_sum['id'])
							->where('st.`status` = 1');

						$tri_sum['tags'] = $db->fetchAll($select_tags);

						$sec_sum['offers'][] = $tri_sum;
					}
				}

				$sum['offers'][] = $sec_sum;
			}
		}

		$page_info['summar'][] = $sum;
	}
}

//print_a($page_info['summar']);