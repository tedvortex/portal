<?php
if (! isset($_GET['page']) || ! is_numeric($_GET['page']) || $_GET['page'] < 1) {
    $_GET['page'] = 1;
}

$page_info['per_page'] = 20;
$page_info['products'] = array();
$page_info['pagination'] = '';

if (! isset($page_info['query'])) {
	$page_info['query'] = null;
}

$_constructors = array(
    'sejururi',
    'cazari',
    'croaziere',
    'circuite'
);

if (in_array($page_info['name_seo'], $_constructors)) {
    $_construct['template'] .= '/' . url($page_info['name_seo']);

    require_once('products/' . url($page_info['name_seo']) . '.php');
}