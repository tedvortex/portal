<?php
if (isset($page_info['booking'])) {
	$_construct['template'] .= '/order';

	$page_info['correct_combination'] = 1;

	if(
		isset($_GET['checkin']) && DateTime::createFromFormat('Y-m-d H:i:s', $_GET['checkin'].' 00:00:00' ) &&
		isset($_GET['duration']) && is_numeric($_GET['duration']) &&
		isset($_GET['transport']) && is_numeric($_GET['transport']) &&
		isset($_GET['room']) &&
		isset($_GET['board']) &&
		isset($_GET['adults']) && is_numeric($_GET['adults']) &&
		isset($_GET['children']) && is_numeric($_GET['children'])
	){
		
		//aeroport de intoarcere
		
		$page_info['airport_return']= $db->fetchOne("
					select `name`
					from `fibula_countries_data`
					where `_id` = ? ",
					$page_info['item']['id_airport']);
		
        $selectTransportTax = $db->select()
	                             ->from(array('stt' => 'xp_sejur_transport_tax'),
	                                    array('*'))
	                             ->where('`stt`.`id_sejur_transport` = ?', $_GET['transport'])
	                             ->where('`stt`.`status` = 1');

        $selectTransportTaxDus = clone $selectTransportTax;
        $selectTransportTaxDus->where('`stt`.`type` = 0');
        
        $selectTransportTaxIntors = clone $selectTransportTax;
        $selectTransportTaxIntors->where('`stt`.`type` = 1')
                                 ->where('`stt`.`id_region` = ?', $page_info['item']['id_airport']);

		$page_info['children_ages_string'] = array();
		$ages = array();

		if ($_GET['children'] > 0 &&
			(!isset($_GET['children_ages']) || !is_array($_GET['children_ages']) || count($_GET['children_ages']) != $_GET['children'])
		) {
			$page_info['correct_combination'] = 0;
		} elseif ($_GET['children'] > 0) {
			for ($i = 0; $i < $_GET['children']; $i++) {
				if(!isset($_GET['children_ages'][$i]) || !is_numeric($_GET['children_ages'][$i])){
					$page_info['correct_combination'] = 0;
				} else {
					$page_info['children_ages_string'][] = $_GET['children_ages'][$i].' ani';
					$ages[] = $_GET['children_ages'][$i];
				}
			}
		}

		$page_info['children_ages_string'] = implode(', ', $page_info['children_ages_string']);

		$transport_tax_dus = $db->fetchAll($selectTransportTaxDus);
        $page_info['taxa_children_dus']=$page_info['taxa_adult_dus']=$page_info['taxa_adult_intors']=$page_info['taxa_children_intors']=0;
		$_ages=$ages;
        foreach ($transport_tax_dus as $k) {
        	
        	if ($k['is_adult']==1) {
        		$page_info['taxa_adult_dus']=$_GET['adults']*$k['price'];
        	} else {
        		foreach ($_ages as $key=>$a) {
        			if ($k['age_start']<$a && $a<=$k['age_end']) {
        				$page_info['taxa_children_dus'] += $k['price'];
        				unset($_ages[$key]);
        			}
        		}
        	}
        }

        $transport_tax_intors = $db->fetchAll($selectTransportTaxIntors);

       // print_a($ages);
        $_ages=$ages;
        foreach ($transport_tax_intors as $k) {
        	
            if ($k['is_adult']==1) {
        		$page_info['taxa_adult_intors']=$_GET['adults']*$k['price'];
        	} else {
        		foreach ($_ages as $key=>$a) {
        			if ($k['age_start']<$a && $a<=$k['age_end']) {	
        				$page_info['taxa_children_intors']+=$k['price'];
        				unset($_ages[$key]);
        			}
        		}
        	}
        }

		$page_info['transport_tax_dus'] = $transport_tax_dus;
		$page_info['transport_tax_intors'] = $transport_tax_intors;

        $select = $db->select()
                     ->from(array('he' => 'fibula_hotels_entities'),
                            array(new Zend_Db_Expr('distinct `he`.*')))
                     ->where('`he`.`id_hotel` = ?', $page_info['item']['id_hotel']);

        if (is_numeric($_GET['adults'])) {
            $select->where('`he`.`adults` = ?', $_GET['adults']);
        }

        if (is_numeric($_GET['children'])) {
            $select->where('`he`.`children` = ?', $_GET['children']);
        }

        $select->where(new Zend_Db_Expr('exists (' . $db->select()
                                                        ->from(array('sc' => 'xp_sejur_cost'),
                                                               array(new Zend_Db_Expr('distinct `id_hotel_entity`')))
                                                        ->where('`sc`.`id_sejur` = ?', $page_info['item']['id'])
                                                        ->where('`sc`.`id_room_type` = ?', $_GET['room'])
                                                        ->where('`sc`.`nights` = ?', $_GET['duration'])
                                                        ->where('`sc`.`id_board_type` = ?', $_GET['board'])
                                                        ->where('`sc`.`id_hotel_entity` = `he`.`id`')
                                                        ->limit(1)
                                                   . ')'));

        $availableChildrenEntity = $availableChildrenCount = array();

        for ($i = 0; $i < $_GET['children']; $i++) {
            if (isset($_GET['children_ages'][$i])) {
                $selectHec{$i} = $db->select()
                                    ->from(array('hec' => 'fibula_hotels_entities_children'),
                                           array('mindif' . $i => new Zend_Db_Expr('max(' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ' - `hec`.`age_start`)'),
                                                 'maxdif' . $i => new Zend_Db_Expr('max(`hec`.`age_end` - ' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ')'),
                                                 'id_hotel_entity',
                                                 'count',
                                                 'id'))
                                    ->joinInner(array('he' => 'fibula_hotels_entities'),
                                                '`he`.`id` = `hec`.`id_hotel_entity`',
                                                array(''))
                                    ->where('`hec`.`age_start` <= ? and `hec`.`age_end` >= ?', $_GET['children_ages'][$i])
                                    ->group('id');

                if (is_numeric($_GET['adults'])) {
                    $selectHec{$i}->where('`he`.`adults` = ?', $_GET['adults']);
                }
                
                if (is_numeric($_GET['children'])) {
                    $selectHec{$i}->where('`he`.`children` = ?', $_GET['children']);
                }
                                    
                $select->joinInner(array('hec' . $i => $selectHec{$i}),
                                   '`he`.`id` = `hec' . $i . '`.`id_hotel_entity`',
                                   array(''));

                $select->order('mindif' . $i);
                $select->order('maxdif' . $i);
                
                $availableChildrenEntity[] = '`hec' . $i . '`.`id`';
                $availableChildrenCount[] = '`hec' . $i . '`.`count`';
            }
        }

        if (count($availableChildrenEntity) > 0) {
		    $select->where(implode($availableChildrenEntity, ' <> '))
                   ->where(implode($availableChildrenCount, ' + ') . ' = ?', $_GET['children']);
		}

		$hotel_entity = $db->fetchRow($select);

		if(! $hotel_entity ){
			$page_info['correct_combination'] = 0;
		}

	    $select = $db->select()
		             ->from(array('sc' => 'xp_sejur_cost'))
		             ->join(array('sj' => 'xp_sejur'),
		             		'`sj`.`id` = `sc`.`id_sejur`',
		             		array('id_currency', 'id_furnizor'))
		             ->join(array('tr' => 'xp_sejur_transport'),
		             		'`tr`.`id` = `sc`.`id_sejur_transport`',
		             		array('price_transport' => 'price', 'price_transport_description' => 'description'))
		             ->join(array('ct' => 'xp_confirmation_type'),
		             		'`ct`.`id` = `sc`.`id_confirmation_type`',
		             		array())
             		 ->join(array('ctd' => 'xp_confirmation_type_data'),
		             		'`ctd`.`_id` = `ct`.`id`',
		             		array('confirmation_type_name' => 'name'))
		             ->join(array('bt' => 'fibula_board_types'),
		             		'`bt`.`id` = `sc`.`id_board_type`',
		             		array())
             		 ->join(array('btd' => 'fibula_board_types_data'),
		             		'`btd`.`_id` = `bt`.`id`',
		             		array('board_type_name' => 'name'))
		             ->join(array('rt' => 'fibula_room_types'),
		             		'`rt`.`id` = `sc`.`id_room_type`',
		             		array())
             		 ->join(array('rtd' => 'fibula_room_types_data'),
		             		'`rtd`.`_id` = `rt`.`id`',
		             		array('room_type_name' => 'name'))
	                 ->where('`sc`.`id_hotel_entity` = ?', $hotel_entity['id'])
	                 ->where('`sc`.`id_sejur` = ?', $page_info['item']['id'])
	                 ->where('`sc`.`date_start` = ?', $_GET['checkin'])
	                 ->where('`sc`.`id_room_type` = ?', $_GET['room'])
	                 ->where('`sc`.`nights` = ?', $_GET['duration'])
	                 ->where('`sc`.`id_board_type` = ?', $_GET['board'])
	                 ->where('now() between `sc`.`availability_start` and `sc`.`availability_end`')
	                 ->where('`ctd`.`lang` = ? ', $_SESSION['lang'])
	                 ->where('`btd`.`lang` = ? ', $_SESSION['lang'])
	                 ->where('`rtd`.`lang` = ? ', $_SESSION['lang'])
             		 ->order('sc.is_standard_price desc');

		$page_info['cost'] = $db->fetchRow($select);

		#type = 0 - pret / entitate , 1 - pret / camera
		#is_standard_price = 0 - de la, 1 - pret fix


		if( !$page_info['cost'] ){
			$page_info['correct_combination'] = 0;
		} else {
			$page_info['cost']['total'] = number_format($page_info['cost']['price'] + $page_info['cost']['price_transport'], 2, '.', '');
			$page_info['cost']['total_order'] = number_format($page_info['cost']['price'] + $page_info['cost']['price_transport']
			                                                + $page_info['taxa_children_dus'] + $page_info['taxa_children_intors']
			                                                + $page_info['taxa_adult_dus'] + $page_info['taxa_adult_intors'], 2, '.', '');

			$page_info['cost']['price_intro'] = '';

			if( $page_info['cost']['is_standard_price'] == 0 ){
				$page_info['cost']['price_intro'] = ' ';
			} elseif( $page_info['cost']['is_standard_price'] == 2 ){
				$page_info['cost']['price_intro'] = 'promotional ';
			}

			if( $page_info['cost']['price_transport_description'] ){
				$page_info['cost']['price_transport_description'] = nl2br($page_info['cost']['price_transport_description']);
			}
		}

		$select = $db->select()
					->from(array('h' => 'fibula_hotels'),
							array('id'))
					->join(array('hd' => 'fibula_hotels_data'),
							'`hd`.`_id` = `h`.`id`',
							array('name'))
					->where('`h`.`id` = ? ', $page_info['item']['id_hotel']);

		$page_info['hotel_details'] = $db->fetchRow($select);

		if( !$page_info['hotel_details'] ){
			$page_info['correct_combination'] = 0;
		}
    } else {
		$page_info['correct_combination'] = 0;
	}

	if ( $page_info['correct_combination'] ){

        #formare get url
        $page_info['get_url'] = '?checkin='.$_GET['checkin'].'&duration='.$_GET['duration'].'&board='.$_GET['board'].'&transport='.$_GET['transport'].'&room='.$_GET['room'].'&adults='.$_GET['adults'].'&children='.$_GET['children'];

        if( isset($_GET['children_ages']) ){
        	foreach($_GET['children_ages'] as $age){
        		$page_info['get_url'] .= '&children_ages[]='.$age;
        	}
        }

        if (isset($_GET['intervals'])) {
        	foreach ($_GET['intervals'] as $age) {
        		$page_info['get_url'] .= '&intervals[]='.$age;
        	}
        }

        #date formular

        $array_countries = $db->fetchAll("
    		select `id_country` id, `name`
    		from `ev_country`
    		order by `name` asc");

		if(!isset($_REQUEST['country'])){
			$_REQUEST['country'] = 183;
		}

		$array_form_individual =
		array(
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=> false,
				'elements'=>
				array(
					array('id'=>'lastname','name'=>'Nume','title'=>'Nume','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'name',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'firstname','name'=>'Prenume','title'=>'Prenume','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'name',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'address','name'=>'Adresa','title'=>'Adresa','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>240,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'country','name'=>'Tara','title'=>'Tara', 'tip'=>'','placeholder'=>'Tara','lang'=>true,'label'=>true,'tag'=>'select','type'=>'text','options'=>$array_countries,'maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'state','name'=>'Judet/Sector','title'=>'Judet/Sector','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'city','name'=>'Oras','title'=>'Oras','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'name',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'cnp','name'=> ($_REQUEST['country'] == 183 ? 'CNP' : 'PIN / Passport No.'),'title'=>'Cnp','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>20,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'phone','name'=>'Telefon','title'=>'Telefon','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
		);

		$array_form_company =
		array(
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'company_name','name'=>'Nume firma','title'=>'Nume firma','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'name',),
				),
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'address','name'=>'Adresa','title'=>'Adresa','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>240,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'city','name'=>'Oras','title'=>'Oras','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'name',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'country','name'=>'Tara','title'=>'Tara', 'tip'=>'','placeholder'=>'Tara','lang'=>true,'label'=>true,'tag'=>'select','type'=>'text','options'=>$array_countries,'maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'state','name'=>'Judet/Sector','title'=>'Judet/Sector','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf reg-com',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'company_reg','name'=>'Nr. Reg. Com.','title'=>'Nr. Reg. Com.','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>false,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'side cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'company_cui','name'=>'CUI','title'=>'CUI','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
				)
			),
			array(
				'wrapper'=>'definition list',
				'class'=>'cf',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'phone','name'=>'Telefon','title'=>'Telefon','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
				)
			),
		);

		$array_form_details =
		array(
			array(
				'wrapper'=>'definition list',
				'class'=>'cf full-size',
				'heading'=>'',
				'heading_tag'=>'h2',
				'inside'=>false,
				'elements'=>
				array(
					array('id'=>'details','name'=>'Alte detalii sau intrebari despre rezervarea dvs.','title'=>'Alte detalii sau intrebari despre rezervarea dvs.','placeholder'=>'','lang'=>true,'label'=>true,'tag'=>'textarea','maxlength'=>1000,'required'=>false,'errors'=>true,'regexp'=>'textarea',),
				),
			),
		);

		$array_payment_methods = array(
			0 => 'Banca (cash/depunere)',
			1 => 'Cash la sediul Click&Go',
			2 => 'Card bancar'
		);

		//print_a($array_countries);

		#verificare formular
		$tourists_form_values = array();
		if( isset($_POST['sejur_reservation']) && $_POST['sejur_reservation'] != 1 ){

			//print_a($_POST);
			if( !isset($_POST['person_type']) || !is_numeric($_POST['person_type']) || !in_array($_POST['person_type'], array(0,1)) ){
				$error['person_type'] = 'Trebuie sa introduceti datele de facturare!';
			}
			else{
				$error = $form->validate( $_POST['person_type'] == 0 ? $array_form_individual : $array_form_company , $_POST);
			}

			if( isset($_POST['country']) && is_numeric($_POST['country']) ){
				if(
					!$country_name = $db->fetchOne("
					select `name`
					from `ev_country`
					where `id_country` = ? ",
					$_POST['country'])
				){
					$error['country'] = 'Trebuie sa alegeti tara pentru detaliile de facturare';
				}
			}
			else{
				$error['country'] = 'Trebuie sa alegeti tara pentru detaliile de facturare';
			}
			#verif tara

			$error = array_merge($error, $form->validate($array_form_details, $_POST));

			if( isset($_POST['adults']) && is_array($_POST['adults']) && count($_POST['adults']) == $_GET['adults'] ){
				foreach( $_POST['adults'] as $k => $a ){
					if(
						!isset($a['title']) || is_array($a['title']) || !in_array($a['title'], range(0,2)) ||
						!isset($a['lastname']) || is_array($a['lastname']) || $a['lastname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $a['lastname']) ||
						!isset($a['firstname']) || is_array($a['firstname']) || $a['firstname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $a['firstname']) ||
						!isset($a['birthdate'])
					){
						$error['adults'] = 'Detaliile turistilor (adulti) sunt obligatorii!';
					}
					else{
						if( !$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' ) ){
							$error['adults'] = 'Detaliile turistilor (adulti) sunt obligatorii!';
						}
						else{
							$tourists_form_values['adults'][$k] = $a;
						}
					}
				}
			}
			else{
				$error['adults'] = 'Detaliile turistilor sunt obligatorii!';
			}

			if( isset($_POST['children']) && is_array($_POST['children']) && count($_POST['children']) == $_GET['children'] && $_GET['children'] > 0 ){

				foreach( $_POST['children'] as $k => $c ){

					$array_ages = array( date('Y', time()) - $_GET['children_ages'][$k] );
					$array_ages[1] = $array_ages[0] - 1;

					if(
						!isset($c['title']) || is_array($c['title']) || !in_array($c['title'], range(0,2)) ||
						!isset($c['lastname']) || is_array($c['lastname']) || $c['lastname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $c['lastname']) ||
						!isset($c['firstname']) || is_array($c['firstname']) || $c['firstname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $c['firstname']) ||
						!isset($c['birthdate'])
					){
						$error['children'] = 'Detaliile turistilor (copii) sunt obligatorii!';
					}

					else{

						if( !$date = DateTime::createFromFormat('d.m.Y H:i:s', $c['birthdate'].' 00:00:00' ) ){
							$error['children'] = 'Detaliile turistilor (copii) sunt obligatorii!';
						}
						elseif( $_GET['children_ages'][$k] != age($c['birthdate']) ){
							$error['children_dates'] = 'Datele de nastere ale copiilor nu corespund cu varstele introduse!';
						}
						else{
							$tourists_form_values['children'][$k] = $c;
						}
					}
				}

			}
			elseif(isset($_POST['children']) || ( !isset($_POST['children']) && $_GET['children'] > 0 ) ){
				$error['children'] = 'Detaliile turistilor (copii) sunt obligatorii! 4';
			}

			if( !isset($_POST['payment_method']) || !is_numeric($_POST['payment_method']) || !in_array($_POST['payment_method'], range(0,2)) ){
				$error['payment_method'] = 'Trebuie sa alegeti metoda de plata!';
			}

			if( !isset($_POST['accept_terms']) || $_POST['accept_terms'] != 1 ){
				$error['accept_terms'] = 'Pentru a putea trece mai departe trebuie sa fiti de acord cu Termenii si conditiile si cu Politica de anulare';
			}

			//print_a($error);

			if( empty($error) ){

				$no_order = generate_random_string(6, $page_info['cost']['id_furnizor'].'-'.$page_info['id'].'-'.$page_info['cost']['id']);

				while( $db->fetchOne("
					select `id`
					from `{$t['o']}`
					where `no_order` = ? ",
					$no_order
				)){
					$no_order = generate_random_string(6, $page_info['cost']['id_furnizor'].'-'.$page_info['id'].'-'.$page_info['cost']['id']);
				}

				$values = array(
					'id_category' => $page_info['id'],
					'category_name' => $page_info['name'],
					'id_customer' => 0,
					'no_order' => $no_order,
					'status' => 0,
					'date' => time(),
					'adults' => $_GET['adults'],
					'children' => $_GET['children'],
					'payment_method' => $_POST['payment_method'],
					'buyer_type' => $_POST['person_type'],
					'buyer_address' => $_POST['address'],
					'buyer_city' => $_POST['city'],
					'buyer_id_country' => $_POST['country'],
					'buyer_country' => $country_name,
					'buyer_state' => $_POST['state'],
					'buyer_email' => $_POST['email'],
					'buyer_phone' => $_POST['phone'],
					'ip' => ip(),
					'details' => isset($_POST['details']) && !is_array($_POST['details']) && $_POST['details'] ? $_POST['details'] : ''
				);

				$values = array_merge(
					$values,
					$_POST['person_type'] == 0 ?
					array(
						'buyer_last_name' => $_POST['lastname'],
						'buyer_first_name' => $_POST['firstname'],
						( $_POST['country'] == 183 ? 'buyer_cnp' : 'buyer_pin' ) => $_POST['cnp'],
					) :
					array(
						'buyer_company' => $_POST['company_name'],
						'buyer_reg_com' => $_POST['country'] == 183 ? (isset($_POST['company_reg']) && !is_array($_POST['company_reg']) ? $_POST['company_reg'] : '') : '',
						( $_POST['country'] == 183 ? 'buyer_cui' : 'buyer_vat' ) => $_POST['company_cui'],
					)
				);

				$db->insert($t['o'],$values);

				$id_order = $db->lastInsertId();

				foreach($_POST['adults'] as $a){

					$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' );

					$db->insert($t['o'].'_people', array(
						'id_order' => $id_order,
						'is_child' => 0,
						'title' => $a['title'],
						'birthdate' => $date->format('Y-m-d'),
						'last_name' => $a['lastname'],
						'first_name' => $a['firstname'],
					));
				}

				if( isset($_POST['children']) ){
					foreach($_POST['children'] as $a){

						$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' );

						$db->insert($t['o'].'_people', array(
							'id_order' => $id_order,
							'is_child' => 1,
							'title' => $a['title'],
							'birthdate' => $date->format('Y-m-d'),
							'last_name' => $a['lastname'],
							'first_name' => $a['firstname'],
						));
					}
				}

				$values_product = array(
					'id_order' => $id_order,
					'id_product' => $page_info['cost']['id_sejur'],
					'id_hotel_entity' => $page_info['cost']['id_hotel_entity'],
					'id_hotel' => $page_info['hotel_details']['id'],
					'id_cost' => $page_info['cost']['id'],
					'hotel_entity_code' => $hotel_entity['code'],
					'checkin' => $_GET['checkin'],
					'duration' => $page_info['cost']['nights'],
					'transport' => $_GET['transport'],
					'id_board_type' => $page_info['cost']['id_board_type'],
					'id_room_type' => $page_info['cost']['id_room_type'],
					'id_currency' => $page_info['cost']['id_currency'],
					'id_confirmation_type' => $page_info['cost']['id_confirmation_type'],
					'cost_type' => $page_info['cost']['type'],
					'is_standard_price' => $page_info['cost']['is_standard_price'],
					'price' => $page_info['cost']['price'],
					'price_transport' => $page_info['cost']['price_transport']
			                           + $page_info['taxa_children_dus'] + $page_info['taxa_children_intors']
			                           + $page_info['taxa_adult_dus'] + $page_info['taxa_adult_intors'],
					'total' => $page_info['cost']['total_order'],
					'name' => $page_info['item']['name'],
					'hotel_name' => $page_info['hotel_details']['name'],
					'board_type_name' => $page_info['cost']['board_type_name'],
					'room_type_name' => $page_info['cost']['room_type_name'],
					'confirmation_type_name' => $page_info['cost']['confirmation_type_name'],
				);

				$db->insert($t['o'].'_products', $values_product);

				if( $values['buyer_type'] == 0 ){

					$buyer =
					'<p>Nume: '.$values['buyer_last_name'].'</p>
					<p>Prenume: '.$values['buyer_first_name'].'</p>
					<p>'.( $_POST['country'] == 183 ? 'CNP' : 'PIN / Passport No.' ).': '.( $_POST['country'] == 183 ? $values['buyer_cnp'] : $values['buyer_pin'] ).'</p>';
				}
				else{
					$buyer =
					'<p>Companie: '.$values['buyer_company'].'</p>';
					'<p>Nr. reg. com.: '.($values['buyer_reg_com'] ? $values['buyer_reg_com'] : '-').'</p>
					<p>'.( $_POST['country'] == 183 ? 'CUI' : 'Numar inregistrare (VAT number)' ).': '.( $_POST['country'] == 183 ? $values['buyer_cui'] : $values['buyer_vat'] ).'</p>';
				}

				$email_part1 = array(
					'Numar comanda' => '#'.$no_order,
					'Data rezervare' => date('Y-m-d'),
					'Sejur' => $values_product['name'],
					'Data cazare' => $values_product['checkin'],
					'Durata sejur' => $values_product['duration'].' nopti',
					'Hotel' => $values_product['hotel_name'],
					'Masa' => $values_product['board_type_name'],
					'Camera' => $values_product['room_type_name'],
					'Turisti' => $_GET['adults'].' '.($_GET['adults'] == 1 ? 'adult' : 'adulti').( $_GET['children'] > 0 ? ' si '.$_GET['children'].' copii ('.$page_info['children_ages_string'].')' : ''),
					'Pret' => $page_info['cost']['price_intro'].$values_product['total'].' '.$page_info['item']['token'],
					'Transport' => $page_info['cost']['price_transport_description'] ? $page_info['cost']['price_transport_description'] : '-',
				);

				if ($transport_tax_dus) {
				    if ($page_info['taxa_adult_dus']>0) {
				        $email_part1['Taxe aeroport adult (DUS)'] = $page_info['taxa_adult_dus'].' Euro</p>';
				    }

				    if ($page_info['taxa_children_dus']>0) {
				        $email_part1['Taxe aeroport copii (DUS)'] = $page_info['taxa_children_dus'].' Euro</p>';
				    }
				}

				if ($transport_tax_intors) {
				    if ($taxa_adult_intors>0) {
				        $email_part1['Taxe aeroport adult (INTORS)'] = $page_info['taxa_adult_intors'].' Euro</p>';
				    }

				    if ($taxa_children_intors>0) {
				        $email_part1['Taxe aeroport copii (INTORS)'] = $page_info['taxa_children_intors'].' Euro</p>';
				    }
                }

                $email_part1['Confirmare'] = $values_product['confirmation_type_name'];
                $email_part1['Total de plata'] = $page_info['cost']['total_order'].' '.$page_info['item']['token'];

				$email_part2 = array(
					'Modalitate de plata' => $array_payment_methods[$values['payment_method']],
					'Tip persoana' => $values['buyer_type'] == 0 ? 'Persoana fizica' : 'Persoana juridica (companie)',
				);

				if( $values['buyer_type'] == 0 ){

					$email_part2['Nume'] = $values['buyer_last_name'];
					$email_part2['Prenume'] = $values['buyer_first_name'];
					$email_part2[ $_POST['country'] == 183 ? 'CNP' : 'PIN / Passport No.' ] = ( $_POST['country'] == 183 ? $values['buyer_cnp'] : $values['buyer_pin'] );
				}
				else{

					$email_part2['Companie'] = $values['buyer_company'];
					$email_part2['Nr. reg. com.'] = ($values['buyer_reg_com'] ? $values['buyer_reg_com'] : '-');
					$email_part2[ $_POST['country'] == 183 ? 'CUI' : 'Numar inregistrare (VAT number)' ] = $_POST['country'] == 183 ? $values['buyer_cui'] : $values['buyer_vat'];
				}

				$email_part2 = array_merge($email_part2, array(
					'Email' => $values['buyer_email'],
					'Telefon' => $values['buyer_phone'],
					'Tara' => $values['buyer_country'],
					'Judet/Sector' => $values['buyer_state'],
					'Oras' => $values['buyer_city'],
					'Adresa' => $values['buyer_address'],
					'Detalii' => $values['details'] ? $values['details'] : '-',
				));

				/*template_mail(
					'order_sejur',
					array($config->site_email),
					array(
						'no_order' => $no_order,
						'date' => date('Y-m-d'),
						'category' => $values['category_name'],
						'sejur_name' => $values_product['name'],
						'checkin' => $values_product['checkin'],
						'duration' => $values_product['duration'],
						'hotel' => $values_product['hotel_name'],
						'board' => $values_product['board_type_name'],
						'room' => $values_product['room_type_name'],
						'count' => $_GET['adults'].' '.($_GET['adults'] == 1 ? 'adult' : 'adulti').( $_GET['children'] > 0 ? ' si '.$_GET['children'].' copii ('.$page_info['children_ages_string'].')' : ''),
						'price' => $page_info['cost']['price_intro'].$values_product['total'].' '.$page_info['item']['token'],
						'transport' => $page_info['cost']['price_transport_description'] ? $page_info['cost']['price_transport_description'] : '-',
						'confirmation' => $values_product['confirmation_type_name'],

						'people' => return_html_mail_tourists($_POST['adults'], isset($_POST['children']) ? $_POST['children'] : array()),

						'payment_method' => $array_payment_methods[$values['payment_method']],
						'person_type' => $values['buyer_type'] == 0 ? 'Persoana fizica' : 'Persoana juridica (companie)',

						'buyer' => $buyer,

						'buyer_email' => $values['buyer_email'],
						'buyer_phone' => $values['buyer_phone'],
						'buyer_country' => $values['buyer_country'],
						'buyer_state' => $values['buyer_state'],
						'buyer_city' => $values['buyer_city'],
						'buyer_address' => $values['buyer_address'],
						'details' => $values['details'] ? $values['details'] : '-',

					),
					$config->site_email
				);*/

				template_mail('order_sejur_new',
					array($config->site_email, $_POST['email']),
					array(
						'category' => $values['category_name'],
						'part1' => return_html_mail_list($email_part1, 'Detalii rezervare'),
						'people' => return_html_mail_tourists($_POST['adults'], isset($_POST['children']) ? $_POST['children'] : array()),
						'part2' => return_html_mail_list($email_part2, 'Detalii facturare'),
					)
				);

				exit(header("Location: {$b}#action,order,success,code:".$no_order));

			}
			else{
				$globals['window'] = "Window.init( \"".tr("Este necesar sa completati toate campurile marcate pentru a va putea inregistra!")."<br/><br/>".implode('<br/><br/>',$error)."\", '"._('Actiune nereusita!')."', 'error' )";
			}
		}

		$page_info['tourists_form'] = write_tourists_form($_GET['adults'], (isset($_GET['children_ages']) ? $_GET['children_ages'] : array()), $tourists_form_values);

		$page_info['form_individual'] = $form->factory($array_form_individual);
		$page_info['form_company'] = $form->factory($array_form_company);
		$page_info['form_details'] = $form->factory($array_form_details);
		//print_a($page_info['form_individual']);
	}

// 	print_a($page_info);
}