<?php
$globals['listing'] = true;
$_GET['page'] = (isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1);
$page_info['per_page'] = 20;
$page_info['pagination'] = '';

// if ($locator == 'basic') {
    $globals['box_link'] = $page_info['link'];
// }

$filters = array(
    'departure',
    'duration',
    'country',
    'city',
    'hotel',
    'adults',
    'children-ages',
    'search_term',
    'sort_price',
    'sort_name'
);

$custom_build = $custom_build_sort = array();

foreach ($filters as $f) {
    if (isset($_GET['filter-' . $f])) {
        $custom_build['filter-' . $f] = $_GET['filter-' . $f];
        
        if( !in_array($f, array('sort_price', 'sort_name')) ){
            $custom_build_sort['filter-' . $f] = $_GET['filter-' . $f];
        }
    }
}

$custom_build = (! empty($custom_build) ? '?' . http_build_query($custom_build) : null);
$custom_build_sort = (! empty($custom_build_sort) ? '?' . http_build_query($custom_build_sort) : null);

/**
 * filters section
 */
$page_info['criteria'] = $_filters = $__filters = array();

$basic_filter = $page_info['sort_filter'] = null;

$durationSql = $db->select()
                  ->from(array('s' => 'xp_sejur'),
                         array('min' => new Zend_Db_Expr('min(`duration`)'),
                               'max' => new Zend_Db_Expr('max(`max_stay`)')))
                  ->where('`s`.`status` = 1')
                  ->where('`s`.`duration` <> 0')
                  ->where('`s`.`max_stay` <> 0');

$globals['filter_duration'] = $db->fetchRow($durationSql);

$imageSql = $db->select()
               ->from(array('si' => 'fibula_hotels_images'),
                      array('image'))
               ->where('si.`id_hotel` = s.`id_hotel`')
               ->order('order asc')
               ->limit(1, 0);

$offerSql = $db->select()
               ->from(array('sc' => 'xp_sejur_cost'),
                      array(new Zend_Db_Expr('count(distinct `id_room_type`)')))
               /* ->where('sc.`id_sejur` = s.`id`') */;

$sql = $db->select()
          ->from(array('s' => 'xp_sejur'),
                 array(
                     new Zend_Db_Expr('sql_calc_found_rows `s`.`id`'),
                     /* 'offers' => new Zend_Db_Expr('(' . $offerSql . ')'), */
                     'id_hotel', 'price', 'stock', 'date_start', 'date_end', 'duration', 'max_stay', 'code', 'image' => new Zend_Db_Expr('(' . $imageSql . ')')))
          ->joinInner(array('sd' => 'xp_sejur_data'),
                      's.`id` = sd.`_id`',
                      array('name', 'name_seo', 'short_description'))
          ->joinInner(array('cr' => 'xp_currencies'),
                      'cr.`id` = s.`id_currency`',
                      array('token'))
          ->joinInner(array('c' => 'fibula_countries'),
                      'c.`id` = s.`id_region`',
                      array('id as id_region'))
          ->joinInner(array('cd' => 'fibula_countries_data'),
                      'c.`id` = cd.`_id`',
                      array('name as city_name'))
          ->joinInner(array('cc' => 'fibula_countries'),
                      'cc.`id` = s.`id_country`',
                      array(''))
          ->joinInner(array('cdc' => 'fibula_countries_data'),
                      'cc.`id` = cdc.`_id`',
                      array('name as country_name')) 
          ->joinInner(array('h' => 'fibula_hotels'),
                      'h.`id` = s.`id_hotel`',
                      array('rating', 'latitude', 'longitude'))
          ->joinInner(array('hd' => 'fibula_hotels_data'),
                      'h.`id` = hd.`_id`',
                      array('address', 'postal_code', 'telephone', 'fax', 'website', 'email', 'hotel' => 'name','description'))
          ->where('`s`.`status` = 1')
          ->where(new Zend_Db_Expr('now() between `s`.`availability_start` and `s`.`availability_end`'))
          ->where('`sd`.`lang` = ?', $_SESSION['lang'])
          ->order('s.price asc');

//if (isset($_GET['filter-duration']) && is_numeric($_GET['filter-duration'])) {
//    $sql->where('`s`.`duration` <= ?', $_GET['filter-duration'])
//        ->where('`s`.`max_stay` >= ?', $_GET['filter-duration']);
//}

if (isset($_GET['filter-duration']) && is_numeric($_GET['filter-duration']) && $_GET['filter-duration'] != 0 ){
    $sql->where('`s`.`duration` <= ?', $_GET['filter-duration'])
        ->where('`s`.`max_stay` >= ?', $_GET['filter-duration']);
}

if (isset($_GET['filter-country']) && is_numeric($_GET['filter-country'])) {
    $sql->where('`s`.`id_country` = ?', $_GET['filter-country']);
}

if (isset($_GET['filter-city']) && is_numeric($_GET['filter-city'])) {
    $sql->where('`s`.`id_region` = ?', $_GET['filter-city']);
}

if (isset($_GET['filter-hotel']) && is_numeric($_GET['filter-hotel']) && $_GET['filter-hotel'] > 0) {
    $sql->where('`s`.`id_hotel` = ?', $_GET['filter-hotel']);
}

if (isset($_GET['filter-transport']) && is_numeric($_GET['filter-transport'])) {
    $sqlTransportFilter = $db->select()
                             ->from(array('stt' => 'xp_sejur_to_transport'),
                                    array('id'))
                             ->where('`stt`.`id_sejur` = `s`.`id`')
                             ->where('`stt`.`id_sejur_transport` = ?', $_GET['filter-transport']);

    $sql->where(new Zend_Db_Expr('exists (' . $sqlTransportFilter . ')'));
}

if (isset($_GET['filter-adults']) && is_numeric($_GET['filter-adults']) && $_GET['filter-adults'] > 0
    && isset($_GET['filter-children']) && is_numeric($_GET['filter-children']) && $_GET['filter-adults'] > 0) 
{
    $selectEntity = $db->select()
                         ->from(array('he' => 'fibula_hotels_entities'),
                                array(new Zend_Db_Expr('distinct `he`.*')))
                         ->where('`he`.`id_hotel` = `s`.`id_hotel`')
                         ->where('`he`.`adults` = ?', $_GET['filter-adults'])
                         ->where('`he`.`children` = ?', $_GET['filter-children']);

    $availableChildrenEntity = $availableChildrenCount = array();

    for ($i = 0; $i < $_GET['filter-children']; $i++) {
        if (isset($_GET['filter-children-ages'][$i])) {
            $selectHec{$i} = $db->select()
                                ->from(array('hec' => 'fibula_hotels_entities_children'),
                                       array('mindif' . $i => new Zend_Db_Expr('max(' . $db->quote($_GET['filter-children-ages'][$i], 'INTEGER') . ' - `hec`.`age_start`)'),
                                             'maxdif' . $i => new Zend_Db_Expr('max(`hec`.`age_end` - ' . $db->quote($_GET['filter-children-ages'][$i], 'INTEGER') . ')'),
                                             'id_hotel_entity',
                                             'count',
                                             'id'))
                                ->joinInner(array('he' => 'fibula_hotels_entities'),
                                            '`he`.`id` = `hec`.`id_hotel_entity`',
                                            array(''))
                                ->where('`hec`.`age_start` <= ? and `hec`.`age_end` >= ?', $_GET['filter-children-ages'][$i])
                                ->group('id');

            if (is_numeric($_GET['filter-adults'])) {
                $selectHec{$i}->where('`he`.`adults` = ?', $_GET['filter-adults']);
            }
            
            if (is_numeric($_GET['filter-children'])) {
                $selectHec{$i}->where('`he`.`children` = ?', $_GET['filter-children']);
            }
                                
            $selectEntity->joinInner(array('hec' . $i => $selectHec{$i}),
                                     '`he`.`id` = `hec' . $i . '`.`id_hotel_entity`',
                                     array(''));

            $selectEntity->order('mindif' . $i);
            $selectEntity->order('maxdif' . $i);
            
            $availableChildrenEntity[] = '`hec' . $i . '`.`id`';
            $availableChildrenCount[] = '`hec' . $i . '`.`count`';
        }
    }

    if (count($availableChildrenEntity) > 0) {
        $selectEntity->where(implode($availableChildrenEntity, ' <> '))
                     ->where(implode($availableChildrenCount, ' + ') . ' = ?', $_GET['filter-children']);
    }

    $sql->where(new Zend_Db_Expr('exists(' . $selectEntity . ')'));
}

$globals['filter_sorts'] = $page_info['filter_sorts_links'] = $sorts = array();

// print_a($sql->__toString());

$page_info['search_heading'] = '';
if( isset($_GET['filter-search_term']) && mb_strlen( $_GET['filter-search_term'] ) > 2 && preg_match('%([\p{L}\p{N}_\+\-\s\.]+)%', $_GET['filter-search_term']) ){
    
    $term = $engine->escape( $_GET['filter-search_term'] );
    $sql->where('sd.`name` like ? or sd.`description` like ?', array('%'.$term.'%'));
    
    $page_info['search_heading'] = 'Cautare: '.$term.'. ';

}

//print_a($sql->__toString());

if(isset($_GET['filter-sort_price']) && in_array( $_GET['filter-sort_price'], array('asc', 'desc') )){
    $sorts[] = 's.price '.$_GET['filter-sort_price'];
    $globals['filter_sorts']['filter-sort_price'] = $_GET['filter-sort_price'];
}

if(isset($_GET['filter-sort_name']) && in_array( $_GET['filter-sort_name'], array('asc', 'desc') )){
    $sorts[] = 'sd.name '.$_GET['filter-sort_name'];
    $globals['filter_sorts']['filter-sort_name'] = $_GET['filter-sort_name'];
}

if( !empty($sorts) ){
    $sql->reset(Zend_Db_Select::ORDER)
        ->order($sorts);
}

// print_a($globals['filter_sorts']);
// print_a($sorts);
// print_a($custom_build);

// print_a($sql->__toString());

// print_a($sql->__toString());
//           ->limit($page_info['per_page'], ($_GET['page'] - 1) * $page_info['per_page']);
// print_a($_GET['page']);
// print_a($page_info['per_page']);
// print_a($sql->__toString() . ' limit ' . $page_info['per_page'] . ', ' . ($_GET['page'] - 1) * $page_info['per_page']);

if ($locator == 'basic') {
    $select['criteria'] = $db->select()
                                ->from(array('c' => 'xp_filters'),
                                        array('id_criteria' => 'c.id', 'name', 'name_seo','is_boxes'))
                            ->where('`c`.`status` = 1')
                            //->where('`c`.`is_boxes` = 1')
                            ->where('`c`.`id_category` = ?', $page_info['id'])
                            ->order(array(
                                'c.order asc',
                                'c.name'));

    $select['option'] = $db->select()
                            ->from(array('c' => 'xp_filters_options'),
                                    array('id_option' => 'c.id', 'name', 'name_seo'))
                            ->where('c.status = 1')
                            ->order(array(
                                    'c.order asc',
                                    'c.name'));
                            
    if (isset($_GET['filter']) && ! empty($_GET['filter'])) {
        $_filters = explode(';',$_GET['filter']);

        if (sizeof($_filters) > 0) {
            foreach ($_filters as $k => $f) {
                $_filters[$k] = array(
                    'name' => stristr($f,':',true),
                    'options' => explode(',',substr(stristr($f,':'),1)),
                );

                $__filters[stristr($f,':',true)] = explode(',', substr(stristr($f, ':'), 1));

                $criteriaSelect = clone $select['criteria'];

                $criteria = $db->fetchRow($criteriaSelect->where('`c`.`name_seo` = ?', url($_filters[$k]['name'], $_SESSION['lang'], ' ', null, false)));

                if ($criteria) {
                    $_options = array();
                    
                    foreach ($_filters[$k]['options'] as $ko => $o) {
                        $optionSelect = clone $select['option'];

                        $option = $db->fetchRow($optionSelect->where('`c`.`name_seo` = ?', url($o, $_SESSION['lang'], ' ', null, false))
                                                             ->where('`c`.`id_filter` = ?', $criteria['id_criteria']));

                        if ($option) {
                            $_options[] = $option['id_option'];
                        } else {
                            unset($_filters[$k]['options'][$ko]);
                        }
                    }
                    
                    if (! empty($_options)) {
                        $sqlFilterOption = $db->select()
                                              ->from(array('cto' => 'xp_sejur_filters_values'),
                                                     array('id'))
                                              ->where('`cto`.`id_sejur` = `s`.`id`')
                                              ->where('`cto`.`id_option` in (?)', $_options);
                        
                        $sql->where(new Zend_Db_Expr('exists(' . $sqlFilterOption . ')'));
                    }
                } else {
                    unset($__filters[$k]);
                }
            }

            $basic_filter = filters_url(array(), $_filters);
            //print_a($basic_filter);
        }
    }
    
    $statement = $db->query($select['criteria']);
    
    if ($statement) {
        while ($c = $statement->fetch()) {
            $optionSelect = clone $select['option'];

            $c['checked'] = false;
            $c['options'] = array();

            $options = $db->fetchAll($optionSelect->where('`c`.`id_filter` = ?', $c['id_criteria']));

            if ($options) {
                foreach ($options as $k => $o) {
                    $o['class'] = null;

                    $o['url'] = $page_info['link'] . 
                                filters_url(array(
                                    'name' => url($c['name_seo']),
                                    'options' => array(url($o['name_seo']))
                                ), $_filters)
                                .$custom_build;

                    if (isset($__filters[url($c['name_seo'])])) {
                        if (in_array(url($o['name_seo']), $__filters[url($c['name_seo'])])) {                            
                            $o['checked'] = true;
                            $o['class'] = ' checked';
                        }
                    }

                    $c['options'][] = $o;
                }
            }

            $page_info['criteria'][$c['id_criteria']] = $c;
        }
    }
    
    $statement = $db->query($sql . ' limit ' . ($_GET['page'] - 1) * $page_info['per_page'] . ', ' . $page_info['per_page']);
    
    $page_info['total'] = 0;

//     $defaultSql->reset(Zend_Db_Select::COLUMNS)->columns(array('id'));

    if ($statement) {
        $page_info['total'] = $db->fetchOne("select found_rows()");

        if ($page_info['total'] > $page_info['per_page']) {
            $max_pages = floor($page_info['total'] / $page_info['per_page']);

            if ($page_info['total'] % $page_info['per_page'] != 0) {
                $max_pages++;
            }

            $page_info['pagination'] .= '<div class="pagination-container"><p>Sunteti pe pagina ' . $_GET['page'] . ' din ' . $max_pages . '</p>';
            $page_info['pagination'] .= '<div class="pagination">'
                                     . pagination($page_info['total'], $_GET['page'], $page_info['per_page'], $page_info['link'] .$basic_filter. '/◬' . $custom_build) . '</div>'
                                     . '</div>';
        }

        while ($p = $statement->fetch()) {
            $offerSqlD = clone $offerSql;

            $p['image'] =  ($p['image'] ? 'i/hotel-fibula/130/' .$p['image'] : 'images/logo15anv05.png');
            $p['link'] = $page_info['link'] . '/' . lnk('sejur', $p);
            $p['offers'] = $db->fetchOne($offerSqlD->where('`sc`.`id_sejur` = ?', $p['id']));

            $lowestCost = $db->select()
                             ->distinct()
                             ->from(array('sc' => 'xp_sejur_cost'),
                                    array('price', 'id_board_type'))
                             ->joinInner(array('rtd' => 'fibula_room_types_data'),
                                         '`sc`.`id_room_type` = `rtd`.`_id`',
                                         array('roomName' => 'name'))
                             ->joinInner(array('btd' => 'fibula_board_types_data'),
                                         '`sc`.`id_board_type` = `btd`.`_id`',
                                         array('boardName' => 'name'))
                             ->joinInner(array('he' => 'fibula_hotels_entities'),
                                         '`sc`.`id_hotel_entity` = `he`.`id`',
                                         array('hotelEntity' => 'code'))
                             ->joinInner(array('ctd' => 'xp_confirmation_type_data'),
                                         '`sc`.`id_confirmation_type` = `ctd`.`_id`',
                                         array('confirmationName' => 'name'))
                             ->where('`sc`.`id_sejur` = ?', $p['id'])
                             ->group('sc.id_room_type')
                             ->order('sc.price asc');

//             $p['cost'] = $db->fetchAll($lowestCost);
            $p['cost'] = false;

            $page_info['products'][$p['id']] = $p;
        }
    }

   
    
    $page_info['filter_sorts_links'] = array(
    	'filter-sort_price' => array(
    	   'val' => isset($globals['filter_sorts']['filter-sort_price']) ? ( $globals['filter_sorts']['filter-sort_price'] == 'asc' ? 'Pret crescator' : 'Pret descrescator' ) : 'Pret crescator',
    	   'options' => array(
    	       'asc' => array(
                   'name' => 'Pret crescator',
    	           'link' => $page_info['link'] . $basic_filter . $custom_build_sort . ( $custom_build_sort ? '&' : '?' ) . 'filter-sort_price=asc' . ( isset($globals['filter_sorts']['filter-sort_name']) ? '&filter-sort_name='.$globals['filter_sorts']['filter-sort_name'] : '')
    	       ),
    	       'desc' => array(
    	           'name' => 'Pret descrescator',
    	           'link' => $page_info['link'] . $basic_filter . $custom_build_sort . ( $custom_build_sort ? '&' : '?' ) . 'filter-sort_price=desc' . ( isset($globals['filter_sorts']['filter-sort_name']) ? '&filter-sort_name='.$globals['filter_sorts']['filter-sort_name'] : '')
    	       )
           )    	        
        ),        
        'filter-sort_name' => array(
            'val' => isset($globals['filter_sorts']['filter-sort_name']) ? ( $globals['filter_sorts']['filter-sort_name'] == 'asc' ? 'Alfabetic crescator' : 'Alfabetic descrescator' ) : 'Alfabetic crescator',
            'options' => array(
                'asc' => array(
                    'name' => 'Alfabetic crescator',
                    'link' => $page_info['link'] . $basic_filter . $custom_build_sort . ( $custom_build_sort ? '&' : '?' ) . 'filter-sort_name=asc' . ( isset($globals['filter_sorts']['filter-sort_price']) ? '&filter-sort_price='.$globals['filter_sorts']['filter-sort_price'] : '')
                ),
    	        'desc' => array(
    	           'name' => 'Alfabetic descrescator',
    	           'link' => $page_info['link'] . $basic_filter . $custom_build_sort . ( $custom_build_sort ? '&' : '?' ) . 'filter-sort_name=desc' . ( isset($globals['filter_sorts']['filter-sort_price']) ? '&filter-sort_price='.$globals['filter_sorts']['filter-sort_price'] : '')
    	        )
            )
        ),
    );
    
//     print_a($page_info['filter_sorts_links']);
}

    $defaultSql = $sql;

    $costSql = $db->select()
                  ->from(array('sc' => 'xp_sejur_cost'),
                         array(new Zend_Db_Expr('distinct `sc`.`id_board_type`')))
                  ->where(new Zend_Db_Expr('`sc`.`id_sejur` in (' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                               ->columns(array(new Zend_Db_Expr('distinct `s`.`id`'))) . ')'));

    $distinctBoardSql = $db->select()
                           ->distinct()
                           ->from(array('sc' => 'xp_sejur_cost'),
                                  array('id_board_type'))
                           ->joinInner(array('s' => 'xp_sejur'),
                                       '`s`.`id` = `sc`.`id_sejur`',
                                       array(''));

    $boardsSqlWhere = $defaultSql->getPart(Zend_Db_Select::WHERE);
    
    $sqlLangRemover = (function ($value, $key) use (&$boardsSqlWhere) {
        if (preg_match('%\`lang\`%is', $value)) {
           unset($boardsSqlWhere[$key]);
        }
    });

    array_walk($boardsSqlWhere, $sqlLangRemover);
//  aici
    $boardsSql = $db->select()
                    ->distinct()
                    ->from(array('bt' => 'fibula_board_types'),
                           array('id'))
                    ->joinInner(array('bd' => 'fibula_board_types_data'),
                                '`bt`.`id` = `bd`.`_id`',
                                array('name'))
                    ->where('`bd`.`lang` = ?', $_SESSION['lang'])
                    ->where(new Zend_Db_Expr('exists(' . $distinctBoardSql->__toString() . (is_array($boardsSqlWhere) ? ' WHERE ' . implode(' ', $boardsSqlWhere) : '') . ' )'));
// //                     exit(print_a($boardsSql->__toString()));
//     $statement = $db->query($boardsSql->__toString());
// //     exit(print_a(microtime(true) - $time));
//     if ($statement) {
//         while ($p = $statement->fetch()) {
//             $globals['filter_boards'][] = $p;
//         }
//     }
                    
    $ratingsSql = $db->select()
                     ->from(array('h' => 'fibula_hotels'),
                            array(new Zend_Db_Expr('distinct `rating`')))
                     ->joinInner(array('hd' => 'fibula_hotels_data'),
                                 '`h`.`id` = `hd`.`_id`',
                                 array(''))
                     ->where("`hd`.`lang` = 'ro'", $_SESSION['lang'])
                     ->where(new Zend_Db_Expr('`h`.`id` in (' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                           ->columns(array('id_hotel')) . ')'));
                     
//     $statement = $db->query($ratingsSql);

//     if ($statement) {
//         while ($p = $statement->fetch()) {
//             $globals['filter_rating'][] = $p;
//         }
//     }

    $countrySql = $db->select()
                     ->from(array('c' => 'fibula_countries'),
                            array('id'))
                     ->joinInner(array('cd' => 'fibula_countries_data'),
                                 '`c`.`id` = `cd`.`_id`',
                                 array('name'))
//                      ->where('`cd`.`lang` = ?', $_SESSION['lang'])
                     ->where(new Zend_Db_Expr('`c`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                            ->reset(Zend_Db_Select::WHERE)
                                                                            ->where('`s`.`status` = 1')
                                                                            ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)'))) . ')'));

    $statement = $db->query($countrySql);

    if ($statement) {
        while ($p = $statement->fetch()) {
            $globals['filter_countries'][] = $p;
        }
    }

    $transportSql = $db->select()
                       ->from(array('st' => 'xp_sejur_transport'),
                              array('id', 'name' => new Zend_Db_Expr('concat(`td`.`name`, " - ", `st`.`name`)')))
                       ->joinInner(array('t' => 'xp_transport'),
                                   '`st`.`id_transport` = `t`.`id`',
                                   array(''))
                       ->joinInner(array('td' => 'xp_transport_data'),
                                   '`t`.`id` = `td`.`_id`',
                                   array(''))
                       ->where('`t`.`status` = 1')
                       ->where('`td`.`lang` = ?', 'ro')
                       ->where(new Zend_Db_Expr('`st`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                               ->columns(array('stt.id_sejur_transport'))
                                                                               ->joinInner(array('stt' => 'xp_sejur_to_transport'),
                                                                                           '`stt`.`id_sejur_transport` = `s`.`id`',
                                                                                           array('')) . ')'))
                       ->order('t.order asc');
                       
    $statement = $db->query($transportSql);

    if ($statement) {
        while ($p = $statement->fetch()) {
            $globals['filter_transport'][] = $p;
        }
    }

$globals['criteria'] = $page_info['criteria'];