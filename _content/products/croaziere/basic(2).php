<?php
$globals['listing'] = true;


/**
 * filters section
 */
$page_info['criteria'] = $_filters = $__filters = array();

$basic_filter = $page_info['sort_filter'] = null;

$imageSql = $db->select()
               ->from(array('si' => 'fibula_hotels_images'),
                      array('image'))
               ->where('si.`id_hotel` = s.`id_hotel`')
               ->order('order asc')
               ->limit(1, 0);

$offerSql = $db->select()
               ->from(array('sc' => 'xp_sejur_cost'),
                      array(new Zend_Db_Expr('count(distinct `id_room_type`)')))
               ->where('sc.`id_sejur` = s.`id`');

$sql = $db->select()
          ->from(array('s' => 'xp_sejur'),
                 array(
                     new Zend_Db_Expr('sql_calc_found_rows `s`.`id`'),
                     'offers' => new Zend_Db_Expr('(' . $offerSql . ')'),
                     'id_hotel', 'price', 'stock', 'date_start', 'date_end', 'duration', 'max_stay', 'code', 'image' => new Zend_Db_Expr('(' . $imageSql . ')')))
          ->joinInner(array('sd' => 'xp_sejur_data'),
                      's.`id` = sd.`_id`',
                      array('name', 'name_seo', 'short_description', 'description'))
          ->joinInner(array('cr' => 'xp_currencies'),
                      'cr.`id` = s.`id_currency`',
                      array('token'))
          ->joinInner(array('cd' => 'fibula_cities_data'),
                      'cd.`_id` = s.`id_city`',
                      array('city_name' => 'name'))
          ->joinInner(array('h' => 'fibula_hotels'),
                      'h.`id` = s.`id_hotel`',
                      array('rating', 'latitude', 'longitude'))
          ->joinInner(array('hd' => 'fibula_hotels_data'),
                      'h.`id` = hd.`_id`',
                      array('address', 'postal_code', 'telephone', 'fax', 'website', 'email', 'hotel' => 'name'))
          ->where('s.`status` = 1')
          ->where('sd.`lang` = ?', $_SESSION['lang']);

if ($locator == 'basic') {
    $select['criteria'] = $db->select()
                                ->from(array('c' => 'xp_filters'),
                                        array('id_criteria' => 'c.id', 'name', 'name_seo'))
                            ->where('`c`.`status` = 1')
                            ->where('`c`.`id_category` = ?', $page_info['id'])
                            ->order(array(
                                'c.order asc',
                                'c.name'));

    $select['option'] = $db->select()
                            ->from(array('c' => 'xp_filters_options'),
                                    array('id_option' => 'c.id', 'name', 'name_seo'))
                            ->where('c.status = 1')
                            ->order(array(
                                    'c.order asc',
                                    'c.name'));

    if (isset($_GET['filter']) && ! empty($_GET['filter'])) {
        $_filters = explode(';',$_GET['filter']);

        if (sizeof($_filters) > 0) {
            foreach ($_filters as $k => $f) {
                $_filters[$k] = array(
                    'name' => stristr($f,':',true),
                    'options' => explode(',',substr(stristr($f,':'),1)),
                );

                $__filters[stristr($f,':',true)] = explode(',', substr(stristr($f, ':'), 1));

                $criteriaSelect = clone $select['criteria'];

                $criteria = $db->fetchRow($criteriaSelect->where('c.name_seo = ?', url($_filters[$k]['name'], $_SESSION['lang'], ' ', null, false)));

                if ($criteria) {
                    foreach ($_filters[$k]['options'] as $ko => $o) {
                        $optionSelect = clone $select['option'];

                        $option = $db->fetchRow($optionSelect->where('c.name_seo = ?', $o)
                                                            ->where('c.id_filter = ?', $criteria['id_criteria']));

                        if ($option) {
                            $sql->where('exists(
                                     		select `id`
                                     		from `xp_sejur_filters_values` cto
                                     		where
                                     			cto.id_sejur = s.id
                                     			and cto.id_option = ?)', $option['id_option']);
                        } else {
                            unset($_filters[$k]['options'][$ko]);
                        }
                    }
                } else {
                    unset($__filters[$k]);
                }
            }

            $basic_filter = filters_url(array(), $_filters);
        }
    }

    $statement = $db->query($select['criteria']);

    if ($statement) {
        while ($c = $statement->fetch()) {
            $optionSelect = clone $select['option'];

            $c['checked'] = false;
            $c['options'] = array();

            $options = $db->fetchAll($optionSelect->where('c.id_filter = ?', $c['id_criteria']));

            if ($options) {
                foreach ($options as $k => $o) {
                    $o['class'] = null;

                    $o['url'] = $page_info['link'] . filters_url(array(
                        'name' => url($c['name_seo']),
                        'options' => array(url($o['name_seo']))
                    ), $_filters);

                    if (isset($__filters[url($c['name_seo'])])) {
                        if (in_array($o['name_seo'], $__filters[url($c['name_seo'])])) {
                            $o['checked'] = true;
                            $o['class'] = ' checked';
                        }
                    }

                    $c['options'][] = $o;
                }
            }

            $page_info['criteria'][$c['id_criteria']] = $c;
        }
    }
}
echo $sql;
$statement = $db->query($sql);

$page_info['total'] = 0;

$defaultSql = $sql;

// $defaultSql->reset(Zend_Db_Select::COLUMNS)->columns(array('id'));

if ($statement) {
    $page_info['total'] = $db->fetchOne("select found_rows()");

    while ($p = $statement->fetch()) {
        $p['image'] = 'i/hotel-fibula/' . ($p['image'] ? $p['image'] : 'no-image.jpg');
        $p['link'] = $page_info['link'] . '/' . lnk('sejur', $p);

        $page_info['products'][$p['id']] = $p;
    }
}

$costSql = $db->select()
              ->from(array('sc' => 'xp_sejur_cost'),
                     array(new Zend_Db_Expr('distinct `sc`.`id_board_type`')))
              ->where(new Zend_Db_Expr('`sc`.`id_sejur` in (' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                           ->columns(array(new Zend_Db_Expr('distinct `s`.`id`'))) . ')'));

$boardsSql = $db->select()
                ->from(array('b' => 'fibula_board_types'),
                       array('id'))
                ->joinInner(array('bd' => 'fibula_board_types_data'),
                            '`b`.`id` = `bd`.`_id`',
                            array('name'))
                ->where('`bd`.`lang` = ?', $_SESSION['lang'])
                ->where(new Zend_Db_Expr('`b`.`id` in (' . $costSql . ')'));

$statement = $db->query($boardsSql);

if ($statement) {
    while ($p = $statement->fetch()) {
        $globals['filter_boards'][] = $p;
    }
}

$ratingsSql = $db->select()
                 ->from(array('h' => 'fibula_hotels'),
                        array(new Zend_Db_Expr('distinct `rating`')))
                 ->joinInner(array('hd' => 'fibula_hotels_data'),
                             '`h`.`id` = `hd`.`_id`',
                             array(''))
                 ->where("`hd`.`lang` = 'en'", $_SESSION['lang'])
                 ->where(new Zend_Db_Expr('`h`.`id` in (' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                       ->columns(array('id_hotel')) . ')'));

$statement = $db->query($ratingsSql);

if ($statement) {
    while ($p = $statement->fetch()) {
        $globals['filter_rating'][] = $p;
    }
}

$countrySql = $db->select()
                 ->from(array('c' => 'fibula_countries'),
                        array('id'))
                 ->joinInner(array('cd' => 'fibula_countries_data'),
                             '`c`.`id` = `cd`.`_id`',
                             array('name'))
//                  ->where('`cd`.`lang` = ?', $_SESSION['lang'])
                 ->where(new Zend_Db_Expr('`c`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                        ->columns(array('id_country')) . ')'));

$statement = $db->query($countrySql);

if ($statement) {
    while ($p = $statement->fetch()) {
        $globals['filter_countries'][] = $p;
    }
}

$transportSql = $db->select()
                   ->from(array('st' => 'xp_sejur_transport'),
                          array('id'))
                   ->joinInner(array('t' => 'xp_transport'),
                               '`st`.`id_transport` = `t`.`id`',
                               array(''))
                   ->where('`t`.`status` = 1')
                   ->where(new Zend_Db_Expr('`st`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                           ->columns(array('stt.id_sejur_transport'))
                                                                           ->joinInner(array('stt' => 'xp_sejur_to_transport'),
                                                                                       '`stt`.`id_sejur_transport` = `s`.`id`',
                                                                                       array('')) . ')'))
                   ->order('t.order asc');

$statement = $db->query($transportSql);

if ($statement) {
    while ($p = $statement->fetch()) {
        $globals['filter_transport'][] = $p;
    }
}

$globals['criteria'] = $page_info['criteria'];