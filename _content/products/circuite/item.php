<?php
if (isset($page_info['item'])) {
	if(! isset($page_info['booking']) ){
		$_construct['template'] .= '/item';
	}

    $page_info['location'] = false;

    $page_info['hotel'] = $db->fetchRow("
            select *
            from
                `fibula_hotels` h
                inner join `fibula_hotels_data` hd
                    on h.`id` = hd.`_id`
            where
                h.`status` = 1
                and h.`id` = ?",
            array($page_info['item']['id_hotel']));

    if ($page_info['hotel']) {
        $page_info['hotel']['images'] = $db->fetchAll("
                select *
                from `xp_circuit_images`
                where `id_circuit` = ?",
                array($page_info['item']['id']));
    }

    $page_info['transport'] = $db->fetchPairs("
            select `stt`.`id_circuit_transport`, `st`.`name`
            from
                `xp_transport` as `t`
                inner join `xp_transport_data` as `td`
                    on `t`.`id` = `td`.`_id`
                inner join `xp_circuit_transport` as `st`
                    on `st`.`id_transport` = `t`.`id`
                inner join `xp_circuit_to_transport` as `stt`
                    on `st`.`id` = `stt`.`id_circuit_transport`
            where
                `stt`.`id_circuit` = ?
                and `td`.`lang` = 'ro'
            order by `st`.`order`",
            array($page_info['item']['id']));
            
       
		
//     $page_info['item'] = $page_info['products'][$page_info['item']['id']];

		$page_info['airport_return']= $db->fetchOne("
					select `name`
					from `fibula_countries_data`
					where `_id` = ? ",
					$page_info['item']['id_airport']);

	$page_info['item']['images'] = $db->fetchAll("
		select *
		from `xp_circuit_images`
		where `id_circuit` = ?
		order by `order` asc ",
		array($page_info['item']['id'])
	);

	$page_info['pricing'] = array(
	    'range' => $db->fetchRow("
	                    select min(`date_start`) min, max(`date_start`) max
	                    from `xp_circuit_cost` sc
	                    where sc.`id_circuit` = ? and
	                    sc.`date_start`>=now() ",
	                    array($page_info['item']['id'])),

	    'start_min' => null,

	    'start_max' => null,

	    'intervals' => array(),

	    'duration' => $db->fetchAll("
	                    select distinct sc.`nights`
	                    from `xp_circuit_cost` sc
	                    where sc.`id_circuit` = ?
	                    order by sc.`nights`", $page_info['item']['id']),

	    'rooms' => $db->fetchAll("
	                    select distinct sc.`id_room_type`, rt.`max_adults`, rtd.`name`
	                    from
	                        `xp_circuit_cost` sc
	                        inner join `fibula_room_types` rt
	                            on sc.`id_room_type` = rt.`id`
	                        inner join `fibula_room_types_data` rtd
	                            on rtd.`_id` = rt.`id`
	                    where
	                        sc.`id_circuit` = ?
	                        and rt.`status` = 1
	                        and rtd.`lang` = ?
	                    order by rt.`order` asc",
	                    array($page_info['item']['id'], $_SESSION['lang'])), 

	   'boards' => $db->fetchPairs("
	                    select distinct sc.`id_board_type`, btd.`name`
	                    from
	                        `xp_circuit_cost` sc
	                        inner join `fibula_board_types` bt
	                            on sc.`id_board_type` = bt.`id`
	                        inner join `fibula_board_types_data` btd
	                            on btd.`_id` = bt.`id`
	                    where
	                        sc.`id_circuit` = ?
	                        and bt.`status` = 1
	                        and btd.`lang` = ?",
	                    array($page_info['item']['id'], $_SESSION['lang'])),

	    'transport' => $db->fetchAll("
	                    select distinct `st`.`id`, `st`.`name`
	                    from
	                        `xp_circuit_transport` as `st`
	                        inner join `xp_transport` as `t`
	                            on `t`.`id` = `st`.`id_transport`
                            inner join `xp_transport_data` as `td`
                                on `t`.`id` = `td`.`_id`
	                    where
	                        `t`.`status` = 1
	                        and `td`.`lang` = ?
	                        and `st`.`id` in (
	                            select `stt`.`id_circuit_transport`
	                            from `xp_circuit_to_transport` stt
	                            where `stt`.`id_circuit` = ?)
	                    order by `st`.`order`",
	                    array($_SESSION['lang'], $page_info['item']['id'])),

	    'method' => $db->fetchAll("
	                    select distinct `td`.`name`
	                    from
	                        `xp_circuit_transport` as `st`
	                        inner join `xp_transport` as `t`
	                            on `t`.`id` = `st`.`id_transport`
                            inner join `xp_transport_data` as `td`
                                on `t`.`id` = `td`.`_id`
	                    where
	                        `t`.`status` = 1
	                        and `td`.`lang` = ?
	                        and `st`.`id` in (
	                            select `stt`.`id_circuit_transport`
	                            from `xp_circuit_to_transport` stt
	                            where `stt`.`id_circuit` = ?)
	                    order by `st`.`order`",
	            array($_SESSION['lang'], $page_info['item']['id'])),
	);
	
	$itinerariu = $db->select()
                     ->from(array('cic' => 'xp_circuit_in_cities'),
                                 array())
                     ->joinInner(array('c' => 'fibula_countries'),
                                 'c.`id` = cic.`id_city`',
                                 array('id'))
                     ->joinInner(array('cd' => 'fibula_countries_data'),
                                 'cd.`_id` = c.`id`',
                                 array('name'))
                     
                     ->where('c.`status` = 1')
                     ->where('cic.`id_circuit` = ? ', $page_info['item']['id'])
                     ->order('cic.id');
                     
    $page_info['itinerariu'] = $db->fetchPairs($itinerariu); 
    $page_info['itinerariu'] = implode(', ', $page_info['itinerariu']);                  

    if ($page_info['pricing']['range']) {
        $page_info['pricing']['start_min'] = DateTime::createFromFormat('Y-m-d', $page_info['pricing']['range']['min']);
        $page_info['pricing']['start_max'] = DateTime::createFromFormat('Y-m-d', $page_info['pricing']['range']['max']);

        if ($page_info['pricing']['start_min'] && $page_info['pricing']['start_max']) {
            $start = $page_info['pricing']['start_min'];

            while ($start->format('U') <= $page_info['pricing']['start_max']->format('U')) {
                $page_info['pricing']['intervals'][] = array(
                    'value' => $start->format('Y-m'),
                    'locale' =>  mb_ucfirst(strftime('%B %Y',$start->format('U'))),
                );

                $start->setDate($start->format('Y'), $start->format('n'), 1)
                      ->modify('+1 month');
            }
        }
    }

	$page_info['tabs'] = array(
	    'header' => array(),
	    'footer' => array()
	);

	$statement = $db->query("
			select
				stt.`description`,
				t.`position`, t.`name`
			from
				`xp_circuit_to_tabs` stt
				inner join `xp_tabs` t
					on t.`id` = stt.`id_tab`
			where
				stt.`id_product` = ? and
				t.`id_category` = ? and
				t.`position` = 1 and
				stt.`description` <> ''
			order by t.`order`",
	        array($page_info['item']['id'], $page_info['id'])
	);

	if ($statement) {
		while ($tab = $statement->fetch()){
			$page_info['tabs']['footer'][] = $tab;
		}
	}

	$statement2 = $db->query("
			select
				stt.`description`,
				t.`position`, t.`name`
			from
				`xp_circuit_to_tabs` stt
				inner join `xp_tabs` t
					on t.`id` = stt.`id_tab`
			where
				stt.`id_product` = ? and
				t.`position` = 0 and
				stt.`description` <> ''
			order by t.`order`",
	        array($page_info['item']['id'])
	);

	if ($statement2) {
		while ($tab = $statement2->fetch()){
			$page_info['tabs']['header'][] = $tab;
		}
	}

}