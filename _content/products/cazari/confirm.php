<?php

if( $locator == 'confirm' && isset($page_info['confirm']) ){
	
	$_construct['template'] .= '/confirm';

	$page_info['error'] = array(
		'set' => false,
		'message' => ''
	);
	
	#options parameters for SoapClient() request
	$option = array(
		'compression' => 1,
		'trace' => 1,
		'connection_timeout' => 180
	);
	
	#options for exit
	$exit_params = array(
		'location' => $b,
		'window' => true,
		'module' => 'omega',
		'type' => 'error',
		'params' => array( 'code:'.$page_info['confirm']['no_order'] )
	);
	
	$continue = true;
	
	#try to connect with SoapClient
	try {
		$Client = new SoapClient('http://xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
	}
	catch (Exception $e) {
		$continue = false;
	}	
	
	#if SoapClient() connection succeeded check the reservation to see the current status
	#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
	if( $continue ){
	
		$checkReservation = array(
			'PartnerCode' => 'mondialvoyages',
			'PlatformName' => 'clickandgo.ro',
			'Username' => 'mondialvoyages',
			'Password' => 'gr$sd45Ds8vr',
			'Version' => 1,
			'ReservationReference' => $page_info['confirm']['reservationReferenceOmega']
		);
	
		try {
			$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
		}
		catch (SoapFault $Exception) {
			$continue = false;
		}
	}
	
	#verify if CheckReservesion() method returned an error or status of reservation is not Requires confirmation
	if( $continue && (
			isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) || 
			! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status) ||
			$checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status != 'Requires confirmation'
		)
	){
		$continue = false;
		
		$page_info['error']['set'] = true;
		
		if( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status) ){
			
			if( $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Confirmed' ){
				
				$page_info['error']['message'] = 'In acest moment rezervarea dvs. aparare ca fiind <span class="white-color green-background">Confirmata</span> in sistemul nostru de rezervari. <span class="red-color">Daca nu va amintiti sa fi confirmat personal aceasta rezervare ('.$page_info['confirm']['no_order'].')</span>, va rugam sa ne contactati cat mai urgent posibil!';
			}
			
			elseif( $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Pending confirmation' ){
				
				$page_info['error']['message'] = 'In acest moment rezervarea dvs. apare ca fiind <span class="white-color orange-background">In asteptare</span>. Acest lucru se datoreaza unei confirmari efectuate anterior de catre dvs. <span class="red-color">Daca nu va amintiti sa fi confirmat personal aceasta rezervare ('.$page_info['confirm']['no_order'].')</span>, va rugam sa ne contactati cat mai urgent posibil!';
			}
			
			elseif( $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Canceled' ){
					
				$page_info['error']['message'] = 'In acest moment rezervarea dvs. apare ca fiind <span class="white-color red-background">Anulata</span>. <span class="red-color">Daca nu va amintiti sa fi anulat personal aceasta rezervare ('.$page_info['confirm']['no_order'].')</span>, va rugam sa ne contactati cat mai urgent posibil!';
			}
			
			elseif( $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Pending cancelation' ){
					
				$page_info['error']['message'] = 'In acest moment rezervarea dvs. apare ca fiind <span class="white-color red-background">In asteptarea anularii</span>. <span class="red-color">Daca nu va amintiti sa fi anulat personal aceasta rezervare ('.$page_info['confirm']['no_order'].')</span>, va rugam sa ne contactati cat mai urgent posibil!';
			}
			else {
				$page_info['error']['message'] = 'In procesul de rezervare si confirmare a rezervarii dvs. a intervenit o <span class="white-color red-background">eroare</span>. <span class="red-color">Pentru mai multe detalii va rugam sa ne contactati telefonic!</span>';
			}
			
			#set statusOmega according with new CheckReservation status
			$db->query("
           		update `xp_orders`
            	set `statusOmega` = :statusOmega
            	where `id` = :id_order ",
			    array('statusOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status, 'id_order' => $page_info['confirm']['id'])
			);
		}
		
		else {		
			$page_info['error']['message'] = 'In procesul de rezervare si confirmare a rezervarii dvs. a intervenit o <span class="white-color red-background">eroare</span>. <span class="red-color">Pentru mai multe detalii va rugam sa ne contactati telefonic!</span>';
		}
	}
	
	#if CheckReservation() not succeeded and we have an error message save it on database
	if( ! $continue ){
	
		#set status of order with value 4, which means that it was a problem on confirmation
		$db->query("
            update `xp_orders`
            set `status` = 4
            where `id` = ? ",
           	$page_info['confirm']['id']
		);
		
		#if received an error on CheckReservation() method save it in database
		if( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
		
			$db->query("
	            update `xp_orders`
	            set `errorMessageCheckReservationConfirmOmega` = :errorMessageCheckReservationConfirmOmega
	            where `id` = :id_order ",
				array(
					'errorMessageCheckReservationConfirmOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
					'id_order' => $page_info['confirm']['id']
				)
			);
		}
		
		#if has not been set an error message set it now
		if( ! $page_info['error']['set'] ){
			
			$page_info['error']['set'] = true;
			$page_info['error']['message'] = 'In procesul de rezervare si confirmare a rezervarii dvs. a intervenit o <span class="white-color red-background">eroare</span>. <span class="red-color">Pentru mai multe detalii va rugam sa ne contactati telefonic!</span>';
		}
	}

	#if was no error on CheckReservation() method, status received is Requires confirmation and confirmation form is filled   
	if( $continue && isset($_POST['validation']) && $_POST['validation'] != 1 && isset($_POST['confirm']) && is_numeric($_POST['confirm']) && $_POST['confirm'] == 1 ){
		
		#array for ConfirmReservation() method
		$confirmReservation = array(
			'PartnerCode' => 'mondialvoyages',
			'PlatformName' => 'clickandgo.ro',
			'Username' => 'mondialvoyages',
			'Password' => 'gr$sd45Ds8vr',
			'Version' => 1,
			'ReservationReference' =>  $page_info['confirm']['reservationReferenceOmega']
		);
		
		#try ConfirmReservation() method
		try {
			$confirmReservationResultOmega = $Client->ConfirmReservation($confirmReservation);
		}
		catch (SoapFault $Exception) {
			
			$continue = false;
		}
		
		#verify if ConfirmReservenion() method returned an error or not isset status
		if( $continue && (
				isset( $confirmReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage ) ||
				! isset($confirmReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status)
			)
		){
			$continue = false;
		}
		
		#if ConfirmReservation() succeeded
		if( $continue ){
			
			#if status received is Confirmed reservation is finished and exit with success message
			if( $confirmReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Confirmed' ){
				$exit_params['type'] = 'success';
			}
			
			#if status received is Pending confirmation client must waiting for the response from Omega
			#now we will exit with information message
			elseif( $confirmReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status == 'Pending confirmation' ){
				$exit_params['type'] = 'information';
			}
			
			$db->query("
           		update `xp_orders`
            	set `statusOmega` = :statusOmega
            	where `id` = :id_order ",
			    array('statusOmega' => $confirmReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status, 'id_order' => $page_info['confirm']['id'])
			);
			
			$db->query("
           		update `xp_orders_products`
            	set `confirmReservationResultOmega` = :confirmReservationResultOmega
            	where `id_order` = :id_order ",
			    array('confirmReservationResultOmega' => serialize($confirmReservationResultOmega), 'id_order' => $page_info['confirm']['id'])
			);
		}
		
		else {
			
			#set status of order with value 4, which means that it was a problem on confirmation
			$db->query("
	            update `xp_orders`
	            set `status` = 4
	            where `id` = ? ",
	           	$page_info['confirm']['id']
			);
		}
		
		#if received an error on ConfirmReservation() method save it in database
		if( isset($confirmReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
				
			$db->query("
	            update `xp_orders`
	            set `errorMessageConfirmReservationOmega` = :errorMessageConfirmReservationOmega
	            where `id` = :id_order ",
				array(
					'errorMessageConfirmReservationOmega' => $confirmReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
					'id_order' => $page_info['confirm']['id']
				)
			);
		}
		
		#exit with proper message
		$string_exit = "Location: {$exit_params['location']}";
		if( $exit_params['window'] ){
			$string_exit .= '#action,'.$exit_params['module'].','.$exit_params['type'];
			 
			if( ! empty($exit_params['params']) ){
				$string_exit .= ','.implode('&', $exit_params['params']);
			}
		}
		
		exit(header($string_exit));
	}
}