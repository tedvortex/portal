<?php
$page_info['_adults'] = $page_info['_children'] = 0;

$array_contact = array(
    array(
        'id' => '00',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '01',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '02',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '03',
        'name' => 'selecteaza'
    )
);

$array_form = array(
    array(
        'wrapper' => 'definition list',
        'class' => 'cf',
        'elements' => array(
            array(
                'id' => 'name',
                'name' => 'Nume, Prenume',
                'tag' => 'input',
                'type' => 'text',
                'maxlength' => 60,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'email',
                'name' => 'Email',
                'maxlength' => 80,
                'tag' => 'input',
                'type' => 'email',
                'regexp' => 'email'
            ),
            array(
                'id' => 'phone',
                'name' => 'Telefon',
                'tag' => 'input',
                'type' => 'text',
                'maxlength' => 200,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'contact_mode',
                'name' => 'Cum doriti sa fiti contactat',
                'tag' => 'select',
                'options' => $array_contact,
                'maxlength' => 60,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'message',
                'name' => 'Mesaj',
                'tag' => 'textarea',
                'maxlength' => 1000,
                'regexp' => 'textarea'
            )
        )
    )
);

if (isset($_POST['validation']) && $_POST['validation'] != 1) {
    $error = $form->validate($array_form, $_POST);

    if (empty($error)) {
        $vars = array_merge($_POST, array(
            'date' => date('d-m-y H:i:s', $_POST['validation'])
        ));

        template_mail('contact', $config->site_email, $vars, $_POST['email']);
        template_mail('autoreply', $_POST['email'], $vars, $config->site_email);

        exit(header("Location: {$b}#action,{$page_info['constructor']},success"));
    }

    else {
        $globals['window'] = "_we('" . tr("Este necesar sa completati toate campurile pentru a putea continua!") .
                 "<br/><br/>" . implode('<br/><br/>', $error) . "')";
    }
}

$page_info['form'] = $form->factory($array_form);

$required = 6;

$room = array(
    'NumberOfRooms' => '1',
    'Adults' => 0,
    'Children' => 0,
    'AvailabilityRequestChildrenAges' => array(
        'AvailabilityRequestChildrenAge' => array(),
    )
);

$parameters = array(
    'PartnerCode' => 'mondialvoyages',
    'PlatformName' => 'clickandgo.ro',
    'Username' => 'mondialvoyages',
    'Password' => 'gr$sd45Ds8vr',
    'Version' => '1',
    'AvailabilityRequestItems' => array(
        'AvailabilityRequestHotel' => array(
            'ItemId' => 1,
            'AvailabilityRequestLocalization' => array(
                'CityCode' => 0
            ),
            'CheckInDate' => '',
            'Nights' => 0,
            'OnRequest' => false,
            'AvailabilityRequestRooms' => array(
                'AvailabilityRequestRoom' => array()
            )
        )
    )
);

if (isset($_REQUEST['city']) && is_numeric($_REQUEST['city'])) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode'] = $_REQUEST['city'];

    $required --;
}

if (isset($_REQUEST['nights']) && is_numeric($_REQUEST['nights']) && in_array($_REQUEST['nights'], range(1, 30))) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['Nights'] = $_REQUEST['nights'];

    $required --;
}

if (isset($_REQUEST['date'])) {
    $date = DateTime::createFromFormat('d/m/Y', $_REQUEST['date']);

    if ($date) {
        $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['CheckInDate'] = $date->format('Y-m-d');
    }

    $required --;
}

if (isset($_REQUEST['rooms']) && is_numeric($_REQUEST['rooms']) && in_array($_REQUEST['rooms'], range(1, 3))) {
    for ($i = 0; $i < $_REQUEST['rooms']; $i++) {
        $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom'][$i] = $room;
    }

    $required --;
}

if (isset($_REQUEST['hotel']) && is_numeric($_REQUEST['hotel'])) {
    $hotelStatement = $db->select()
                         ->from(array('hd' => 'xp_hotels_data'),
                                array('name'))
                         ->where('hd.`_id` = ?', $_REQUEST['hotel']);

    $hotel = $db->fetchOne($hotelStatement);

    if ($hotel) {
        $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestHotelCodes']['AvailabilityRequestHotelCode'] = $_REQUEST['hotel'];
    }
}

$people = array();

if (isset($_REQUEST['people']) && is_array($_REQUEST['people'])) {
    $required -= 2;

    foreach ($_REQUEST['people'] as $k => $p) {
        if (isset($p['adults']) && is_numeric($p['adults'])) {
            $page_info['_adults'] += $p['adults'];

            $people[$k]['adults'] = $p['adults'];

            $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom'][$k]['Adults'] = $p['adults'];
        }

        if (isset($p['children']) && is_numeric($p['children'])) {
            $page_info['_children'] += $p['children'];

            $people[$k]['children'] = $p['children'];

            $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom'][$k]['Children'] = $p['children'];

            for ($i = 0; $i < $p['children']; $i++) {
                if (isset($p['children-ages'][$i]) && is_numeric($p['children-ages'][$i])) {
                    $people[$k]['age'][$i] = $p['children-ages'][$i];

                    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom'][$k]['AvailabilityRequestChildrenAges']['AvailabilityRequestChildrenAge'][$i] = $p['children-ages'][$i];
                }
            }
        }
    }
}

$table = new Zend_Db_Table('xp_search_queries');
$tableItems = new Zend_Db_Table('xp_search_items');
$tableRooms = new Zend_Db_Table('xp_search_rooms');
$tableBoards = new Zend_Db_Table('xp_search_boards');

if ($required <= 0) {
    $option = array(
        'compression' => 1,
        'trace' => 1,
        'connection_timeout' => 10
    );

    try {
        $Client = new SoapClient('http://xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
    } catch (Exception $e) {
        print "Unable to fetch WSDL file!";
    }
    
   

    try {
        $result = $Client->Availability($parameters);
    } catch (SoapFault $Exception) {
        print_a($Exception);

//         echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
//         echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
    }
    
//    var_dump($result);
//    die();

    if (isset($result) && is_object($result->AvailabilityResponseItems)) {
        if (! isset($result->AvailabilityResponseItems->ErrorMessage)) {
            $row = $table->fetchRow($table->select()
                                          ->where('reference = ?', $result->AvailabilityResponseItems->SearchReference));

            if (! $row) {
                $select = $db->select()
        					 ->from(array('c' => 'xp_cities'),
        					        array(''))
        					 ->join(array('ctr' => 'xp_countries'),
        					        '`ctr`.`id` = `c`.`id_country`',
        					        array('id'))
        					 ->where('`c`.`id` = ?', $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode']);

				$data = array(
                    'id_country' => $db->fetchOne($select),
                    'id_city' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode'],
                    'id_hotel' => (isset($_REQUEST['hotel']) && is_numeric($_REQUEST['hotel']) && $_REQUEST['hotel'] ? $_REQUEST['hotel'] : null),
                    'reference' => $result->AvailabilityResponseItems->SearchReference,
                    'nights' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['Nights'],
                    'rooms' => $_GET['rooms'],
                    'adults' => $page_info['_adults'],
                    'children' => $page_info['_children'],
                    'checkin' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['CheckInDate'],
				    'people' => serialize($people),
                );

                $id_query = $table->insert($data);

                if (is_array($result->AvailabilityResponseItems->AvailabilityResponseHotel)) {
                    foreach ($result->AvailabilityResponseItems->AvailabilityResponseHotel as $hotel) {
                        $_data = array(
                            'id_search_query' => $id_query,
                            'id_item' => $hotel->ItemId,
                            'id_city' => $hotel->CityCode,
                            'id_request' => $hotel->RequestItemId,
                            'id_hotel' => $hotel->HotelCode,
                            'price' => 0,
                            'hotel_name' => $hotel->HotelName,
                            'city_name' => $hotel->CityName,
                            'description' => serialize($hotel->AvailabilityResponseHotelRooms)
                        );

                        if ($hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom instanceof stdClass) {
                            $rooms = array(
                                $hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom
                            );
                        } else {
                            $rooms = $hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom;
                        }

                        $tableItems->insert($_data);

                        $id_search_item = $db->lastInsertId();

                        foreach ($rooms as $room) {

                            $__data = array(
                                'id_search_item' => $id_search_item,
                                'id_room' => $room->Id,
                                'adults' => $room->Adults,
                                'children' => $room->Children,
                                'name' => $room->Name,
                                'room' => serialize($room)
                            );

                            $tableRooms->insert($__data);

                            $id_search_room = $db->lastInsertId();

                            if (isset($room->AvailabilityResponseBoards->AvailabilityResponseBoard)) {
                                if ($room->AvailabilityResponseBoards->AvailabilityResponseBoard instanceof stdClass) {
                                    $boards = array(
                                        $room->AvailabilityResponseBoards->AvailabilityResponseBoard
                                    );
                                } else {
                                    $boards = $room->AvailabilityResponseBoards->AvailabilityResponseBoard;
                                }

                                foreach ($boards as $board) {
                                    if ($_data['price'] == 0) {
                                        $_data['price'] = $board->AvailabilityResponseRoomPrice->Total;
                                        $_data['currency'] = $board->AvailabilityResponseRoomPrice->Currency;
                                    }
// print_a($board);
                                    $___data = array(
                                        'id_search_room' => $id_search_room,
                                        'id_item' => $board->Id,
                                        'price' => $board->AvailabilityResponseRoomPrice->Total,
                                        'currency' => $board->AvailabilityResponseRoomPrice->Currency,
                                        'code' => $board->Code,
                                        'name' => $board->Name,
                                        'pricing' => serialize($board->AvailabilityResponsePrices)
                                    );

                                    $tableBoards->insert($___data);
                                }
                            }

                            $db->query("
                                    update `xp_search_items`
                                    set
                                        `price` = ?,
                                        `currency` = ?
                                    where `id_search_item` = ?",
                                    array($_data['price'], $_data['currency'], $id_search_item));
                        }
                    }
                }

                $hash = md7($id_query . $data['id_city'] . $data['reference']);

                $table->update(array('hash' => $hash), $table->getAdapter()->quoteInto('id = ?', $id_query));

                exit(header("Location: {$page_info['link']}/{$hash}"));
            }
        } else {
            $page_info['not_found'] = true;
        }
    } else {
        $page_info['not_found'] = true;
    }
}

if (isset($page_info['not_found'])) {
    $page_info['_country'] = $page_info['_city'] = null;

    $page_info['_location'] = array();

    if (isset($_GET['country']) && is_numeric($_GET['country'])) {
        $page_info['_country'] = $db->fetchOne($db->select()
                ->from(array('xp_countries_data'),
                        array('name'))
                ->where('`lang` = ?', 'en')
                ->where('`_id` = ?', $_GET['country']));

        if ($page_info['_country']) {
            $page_info['_location'][] = $page_info['_country'];
        }
    }

    if (isset($_GET['city']) && is_numeric($_GET['city'])) {
        $page_info['_city'] = $db->fetchOne($db->select()
                ->from(array('xp_cities_data'),
                        array('name'))
                ->where('`lang` = ?', 'en')
                ->where('`_id` = ?', $_GET['city']));

        if ($page_info['_city']) {
            $page_info['_location'][] = $page_info['_city'];
        }
    }
}