<?php
$array_contact = array(
    array(
        'id' => '00',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '01',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '02',
        'name' => 'selecteaza'
    ),
    array(
        'id' => '03',
        'name' => 'selecteaza'
    )
);

$array_form = array(
    array(
        'wrapper' => 'definition list',
        'class' => 'cf',
        'elements' => array(
            array(
                'id' => 'name',
                'name' => 'Nume, Prenume',
                'tag' => 'input',
                'type' => 'text',
                'maxlength' => 60,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'email',
                'name' => 'Email',
                'maxlength' => 80,
                'tag' => 'input',
                'type' => 'email',
                'regexp' => 'email'
            ),
            array(
                'id' => 'phone',
                'name' => 'Telefon',
                'tag' => 'input',
                'type' => 'text',
                'maxlength' => 200,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'contact_mode',
                'name' => 'Cum doriti sa fiti contactat',
                'tag' => 'select',
                'options' => $array_contact,
                'maxlength' => 60,
                'regexp' => 'textarea'
            ),
            array(
                'id' => 'message',
                'name' => 'Mesaj',
                'tag' => 'textarea',
                'maxlength' => 1000,
                'regexp' => 'textarea'
            )
        )
    )
);

if (isset($_POST['validation']) && $_POST['validation'] != 1) {
    $error = $form->validate($array_form, $_POST);

    if (empty($error)) {
        $vars = array_merge($_POST, array(
            'date' => date('d-m-y H:i:s', $_POST['validation'])
        ));

        template_mail('contact', $config->site_email, $vars, $_POST['email']);
        template_mail('autoreply', $_POST['email'], $vars, $config->site_email);

        exit(header("Location: {$b}#action,{$page_info['constructor']},success"));
    }

    else {
        $globals['window'] = "_we('" . tr("Este necesar sa completati toate campurile pentru a putea continua!") .
                 "<br/><br/>" . implode('<br/><br/>', $error) . "')";
    }
}

$page_info['form'] = $form->factory($array_form);

$required = 6;

$parameters = array(
    'PartnerCode' => 'mondialvoyages',
    'PlatformName' => 'mondialvoyages',
    'Username' => 'mondialvoyages',
    'Password' => 'gr$sd45Ds8vr',
    'Version' => 1,
    'AvailabilityRequestItems' => array(
        'AvailabilityRequestHotel' => array(
            'ItemId' => 1,
            'AvailabilityRequestLocalization' => array(
                'CityCode' => 0
            ),
            'CheckInDate' => '',
            'Nights' => 0,
            'OnRequest' => true,
            'AvailabilityRequestRooms' => array(
                'AvailabilityRequestRoom' => array(
                    'NumberOfRooms' => 0,
                    'Adults' => 0,
                    'Children' => 0,
                    'AvailabilityRequestChildrenAges' => '',
                    array(
                        array(
                            'AvailabilityRequestChildrenAge' => '5'
                        )
                    )
                )
            )
        )
    )
);

if (isset($_REQUEST['city']) && is_numeric($_REQUEST['city'])) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode'] = $_REQUEST['city'];

    $required --;
}

if (isset($_REQUEST['nights']) && is_numeric($_REQUEST['nights']) && in_array($_REQUEST['nights'], range(1, 30))) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['Nights'] = $_REQUEST['nights'];

    $required --;
}

if (isset($_REQUEST['date'])) {
    $date = DateTime::createFromFormat('d/m/Y', $_REQUEST['date']);

    if ($date) {
        $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['CheckInDate'] = $date->format('Y-m-d');
    }

    $required --;
}

if (isset($_REQUEST['rooms']) && is_numeric($_REQUEST['rooms']) && in_array($_REQUEST['rooms'], range(1, 9))) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['NumberOfRooms'] = $_REQUEST['rooms'];

    $required --;
}

if (isset($_REQUEST['adults']) && is_numeric($_REQUEST['adults']) && in_array($_REQUEST['adults'], range(1, 30))) {
    $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Adults'] = $_REQUEST['adults'];

    $required --;
}

if (isset($_REQUEST['children']) && is_numeric($_REQUEST['children'])) {
    if ($_REQUEST['children'] > 0) {
        for ($i = 0; $i < $_REQUEST['children']; $i ++) {
            if (isset($_REQUEST['children-ages'][$i])) {
                $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['AvailabilityRequestChildrenAges'][] = array(
                    'AvailabilityRequestChildrenAge' => $_REQUEST['children-ages'][$i]
                );
            } else {
                $_REQUEST['children'] --;
            }
        }

        $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Children'] = $_REQUEST['children'];
    }

    $required --;
}

$table = new Zend_Db_Table('xp_search_queries');
$tableItems = new Zend_Db_Table('xp_search_items');
$tableRooms = new Zend_Db_Table('xp_search_rooms');
$tableBoards = new Zend_Db_Table('xp_search_boards');

if ($required <= 0) {
    $option = array(
        'compression' => 1,
        'trace' => 1,
        'connection_timeout' => 10
    );

    try {
        $Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
    } catch (Exception $e) {
        print "Unable to fetch WSDL file!";
    }

    try {
        $result = $Client->Availability($parameters);
    } catch (SoapFault $Exception) {
        print_a($Exception);

//         echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
//         echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
    }

    if (isset($result) && is_object($result->AvailabilityResponseItems)) {
        if (! isset($result->AvailabilityResponseItems->ErrorMessage)) {
            $row = $table->fetchRow($table->select()
                                          ->where('reference = ?', $result->AvailabilityResponseItems->SearchReference));

            if (! $row) {
                $data = array(
                    'id_country' => $_REQUEST['country'],
                    'id_city' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode'],
                    'id_hotel' => $_REQUEST['hotel'],
                    'reference' => $result->AvailabilityResponseItems->SearchReference,
                    'nights' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['Nights'],
                    'rooms' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['NumberOfRooms'],
                    'adults' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Adults'],
                    'children' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Children'],
                    'checkin' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['CheckInDate']
                );

                $id_query = $table->insert($data);

                if (is_array($result->AvailabilityResponseItems->AvailabilityResponseHotel)) {
                    foreach ($result->AvailabilityResponseItems->AvailabilityResponseHotel as $hotel) {
                        $_data = array(
                            'id_search_query' => $id_query,
                            'id_item' => $hotel->ItemId,
                            'id_city' => $hotel->CityCode,
                            'id_request' => $hotel->RequestItemId,
                            'id_hotel' => $hotel->HotelCode,
                            'price' => 0,
                            'hotel_name' => $hotel->HotelName,
                            'city_name' => $hotel->CityName,
                            'description' => serialize($hotel->AvailabilityResponseHotelRooms)
                        );

                        if ($hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom instanceof stdClass) {
                            $rooms = array(
                                $hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom
                            );
                        } else {
                            $rooms = $hotel->AvailabilityResponseHotelRooms->AvailabilityResponseHotelRoom;
                        }

                        $tableItems->insert($_data);

                        $id_search_item = $db->lastInsertId();

                        foreach ($rooms as $room) {
                            $__data = array(
                                'id_search_item' => $id_search_item,
                                'id_room' => $room->Id,
                                'adults' => $room->Adults,
                                'children' => $room->Children,
                                'name' => $room->Name
                            );

                            $tableRooms->insert($__data);

                            $id_search_room = $db->lastInsertId();

                            if (isset($room->AvailabilityResponseBoards->AvailabilityResponseBoard)) {
                                if ($room->AvailabilityResponseBoards->AvailabilityResponseBoard instanceof stdClass) {
                                    $boards = array(
                                        $room->AvailabilityResponseBoards->AvailabilityResponseBoard
                                    );
                                } else {
                                    $boards = $room->AvailabilityResponseBoards->AvailabilityResponseBoard;
                                }

                                foreach ($boards as $board) {
                                    if ($_data['price'] == 0) {
                                        $_data['price'] = $board->AvailabilityResponseRoomPrice->Total;
                                    }

                                    $___data = array(
                                        'id_search_room' => $id_search_room,
                                        'id_item' => $board->Id,
                                        'price' => $board->AvailabilityResponseRoomPrice->Total,
                                        'currency' => $board->AvailabilityResponseRoomPrice->Currency,
                                        'code' => $board->Code,
                                        'name' => $board->Name,
                                        'pricing' => serialize($board->AvailabilityResponsePrices)
                                    );

                                    $tableBoards->insert($___data);
                                }
                            }

                            $db->query("
                                    update `xp_search_items`
                                    set `price` = ?
                                    where `id_search_item` = ?",
                                    array($_data['price'], $id_search_item));
                        }
                    }
                }

                $hash = md7($id_query . $data['id_city'] . $data['reference']);

                $table->update(array('hash' => $hash), $table->getAdapter()->quoteInto('id = ?', $id_query));

                exit(header("Location: {$page_info['link']}/{$hash}"));
            }
        } else {
            $page_info['not_found'] = true;
        }
    } else {
        $page_info['not_found'] = true;
    }
}