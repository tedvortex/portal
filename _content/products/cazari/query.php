<?php
if (isset($page_info['query']) && $page_info['query']) {
    $page_info['link'] .= '/' . $page_info['query']['hash'];

    $globals['cities'] = $db->fetchAll("
            select c.`id`, `name`
            from
                `xp_cities` c
                inner join `xp_cities_data` cd
                    on c.`id` = cd.`_id`
            where
                c.`status`
                and c.`id_country` = ? ",
            array($page_info['query']['id_country']));

    $globals['hotels'] = $db->fetchAll("
            select c.`id`, concat(cd.`name`, ' - ', c.`rating`, '*') name
            from
                `xp_hotels` c
                inner join `xp_hotels_data` cd
                    on c.`id` = cd.`_id`
            where
                c.`status`
                and c.`id_city` = ?
            order by cd.`name`",
            array($page_info['query']['id_city']));

    array_unshift($globals['hotels'], array(
        'id' => 0,
        'name' => tr('Alege hotel')
    ));

    $statement = $db->query("
            select sql_calc_found_rows
                h.`latitude`, h.`longitude`, h.`id` id_hotel, h.`rating`,
                hd.`description` short_description,
                si.*,
                (select `image`
                 from `xp_hotels_images`
                 where `id_hotel` = si.`id_hotel`
                 order by `id_hotel_image`
                 limit 1) image
            from
                `xp_search_items` si
                inner join `xp_hotels` h
                    on h.`id` = si.`id_hotel`
                inner join `xp_hotels_data` hd
                    on hd.`_id` = h.`id`
            where
                si.`id_search_query` = ?
            order by si.`id_item`
            limit {$db->quote(($_GET['page'] - 1) * $page_info['per_page'], 'INTEGER')}, {$db->quote($page_info['per_page'], 'INTEGER')}",
            array($page_info['query']['id']));

    $page_info['total'] = $db->fetchOne("select found_rows()");

    if ($statement) {
        while ($p = $statement->fetch()) {
            $p['description'] = unserialize($p['description']);
            $p['offers'] = 0;
            $p['image'] = 'i/hotel/' . ($p['image'] ? $p['image'] : 'no-image.jpg');

            unset($p['description']);

            $statementRooms = $db->query("
                    select *
                    from `xp_search_rooms` sr
                    where sr.`id_search_item` = ?",
                    array($p['id_search_item']));

            if ($statement) {
                while ($r = $statementRooms->fetch()) {
                    $r['boards'] = $db->fetchAll("
                            select *
                            from `xp_search_boards` sb
                            where sb.`id_search_room` = ?
                            order by sb.`price` asc",
                            array($r['id_search_room']));

                    $p['rooms'][] = $r;

                    $p['offers'] += count($r['boards']);
                }
            }

            $page_info['products'][$p['id_search_item']] = $p;
        }
    }

    if ($page_info['total'] > $page_info['per_page']) {
        $page_info['pagination'] = '<div class="pagination">' . pagination($page_info['total'], $_GET['page'], $page_info['per_page'], $page_info['link'] . '/') . '</div>';
    }
}

$globals['query'] = $page_info['query'];