<?php
if (isset($page_info['item']) && $locator == 'item') {
    $_construct['template'] .= '/item';

    $page_info['location'] = false;

    $page_info['hotel'] = $db->fetchRow("
            select *
            from
                `xp_hotels` h
                inner join `xp_hotels_data` hd
                    on h.`id` = hd.`_id`
            where
                h.`status` = 1
                and h.`code` = ?",
            array($page_info['item']['id_hotel']));

    if (isset($page_info['products'][$page_info['item']['id_search_item']])) {
        $page_info['location'] = $page_info['products'][$page_info['item']['id_search_item']];

        $page_info['hotel']['images'] = $db->fetchAll("
                select *
                from `xp_hotels_images` hi
                where hi.`id_hotel` = ?",
                array($page_info['hotel']['_id']));
    }

    $page_info['link'] .= '/' . $page_info['item']['id_hotel'];
}