<?php

if( $locator == 'view' && isset($page_info['view']) ){
	
	$_construct['template'] .= '/view';
	
	$page_info['alert_box'] = array(
		'heading' => '',
		'heading_class' => 'blue-background',
		'messages' => array() 
	);
	
	#url to confirm the price on Omega if statusOmega is Requires confirmation
	$page_info['requiresConfirmationUrl'] = $b . url($page_info['name_seo']) . '/confirm/' . $page_info['view']['no_order'];
		
	#if statusOmega is Requires confirmation redirect to requiresConfirmationUrl
	if( $page_info['view']['statusOmega'] == 'Requires confirmation' ){
		
		exit(header("Location: {$page_info['requiresConfirmationUrl']}"));
	}
		
	#if payment method is not credit card
	if( $page_info['view']['payment_method'] != 2 ){

		#if error
		if( in_array($page_info['view']['status'], array(3, 4, 7)) || $page_info['view']['statusOmega'] == 'Reservation error' ){
			
			$page_info['alert_box']['heading'] = 'Ne pare rau, a aparut o eroare';
			$page_info['alert_box']['heading_class'] = 'red-background';
			$page_info['alert_box']['messages'][] = 'Confirmarea rezervarii dvs. cu numarul <span class="red-color">'.$page_info['view']['no_order'].'</span> nu a putut fi finalizata.';
		}

		#if canceled
		elseif( $page_info['view']['status'] == 5 || $page_info['view']['statusOmega'] == 'Canceled' ){
			
			$page_info['alert_box']['heading'] = 'Rezervare anulata';
			$page_info['alert_box']['heading_class'] = 'orange-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este anulata.';
		}
		
		#if money returned
		elseif( $page_info['view']['status'] == 2 ){
			
			$page_info['alert_box']['heading'] = 'Rezervare anulata';
			$page_info['alert_box']['heading_class'] = 'orange-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este anulata.';
			$page_info['alert_box']['messages'][] = 'Suma de bani pe care ati platit-o v-a fost inapoiata in totalitate.';
		}
		
		#if is paid by client means that the reservation is confirmed by omega because payment method is no credit card and status 1 is set manually
		elseif( $page_info['view']['status'] == 1 ){
				
			$page_info['alert_box']['heading'] = 'Rezervare confirmata si platita';
			$page_info['alert_box']['heading_class'] = 'green-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="green-color">'.$page_info['view']['no_order'].'</span> este confirmata si platita.';
		}
		
		#if Confirmed
		elseif( $page_info['view']['statusOmega'] == 'Confirmed' ) {
			
			$page_info['alert_box']['heading'] = 'Rezervare confirmata';
			$page_info['alert_box']['heading_class'] = 'green-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="green-color">'.$page_info['view']['no_order'].'</span> este confirmata.';
			$page_info['alert_box']['messages'][] = '<span class="red-color">In acest moment suma de plata pentru rezervarea efectuata nu este platita. Va rugam sa achitati suma cat mai repede posibil.</span>';
			$page_info['alert_box']['messages'][] = 'Daca totusi ati achitat rezervarea va rugam sa ne trimiteti o copie a facturii sau a ordinului de plata. Va multumim !';
		}
		
		#if Pending confirmation
		elseif( $page_info['view']['statusOmega'] == 'Pending confirmation' ){
			
			$page_info['alert_box']['heading'] = 'Rezervare in asteptare';
			$page_info['alert_box']['heading_class'] = 'blue-background';
			$page_info['alert_box']['messages'][] = 'In acest moment rezervarea dvs. apare ca fiind <span class="blue-color">In asteptare</span>.';
			$page_info['alert_box']['messages'][] = 'In cel mai scurt timp posibil unul dintre operatorii nostri va va contacta. Va multumim !';
		}
		
		#if Pending cancelation
		elseif( $page_info['view']['statusOmega'] == 'Pending cancelation' ){
				
			$page_info['alert_box']['heading'] = 'Rezervare in curs de anulare';
			$page_info['alert_box']['heading_class'] = 'orange-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este in curs de anulare.';
		}
		
		#if In progress. this status is recived when make CheckReservation() after check-in date on Confirmed reservation
		elseif( $page_info['view']['statusOmega'] == 'In progress' ){
		
			$page_info['alert_box']['heading'] = 'Rezervare in desfasurare';
			$page_info['alert_box']['heading_class'] = 'blue-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este in curs de desfasurare.';
			$page_info['alert_box']['messages'][] = 'Va dorim vacanta placuta !';
		}
		
		#if Finished. this status is recived when make CheckReservation() after check-out date on Confirmed reservation
		elseif( $page_info['view']['statusOmega'] == 'Finished' ){
		
			$page_info['alert_box']['heading'] = 'Rezervare terminata';
			$page_info['alert_box']['heading_class'] = 'blue-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este terminata.';
			$page_info['alert_box']['messages'][] = 'Va multumim ca ati ales sa va petreceti vacanta cu noi !';
		}
		
		#if no case client must contact an operator
		else {
			$page_info['alert_box']['heading'] = 'Ne pare rau, a aparut o eroare';
			$page_info['alert_box']['heading_class'] = 'red-background';
			$page_info['alert_box']['messages'][] = 'Confirmarea rezervarii dvs. cu numarul <span class="red-color">'.$page_info['view']['no_order'].'</span> nu a putut fi finalizata.';
			$page_info['alert_box']['messages'][] = 'Daca pana in acest moment nu ati fost contactat de catre un operator va rugam sa ne contactati pentru a va pune la dispozitie detaliile necesare despre rezervarea dvs. Va multumim !';
		}
	}
	
	#if payment method is not credit card, current status is a combination of statusMobilpay, status and statusOmga
	#statusMobilpay is status of transaction
	#status is general status of reservation. this status is set automatically on mobilpay_confirm_card.php and manually from admin
	#statusOmega is status recived from Omega requests
	else {

		#if error on Mobilpay
		if( $page_info['view']['status'] == 6 ){
			
			$page_info['alert_box']['heading'] = 'Ne pare rau, a aparut o eroare';
			$page_info['alert_box']['heading_class'] = 'red-background';
			$page_info['alert_box']['messages'][] = 'Confirmarea rezervarii dvs. cu numarul <span class="red-color">'.$page_info['view']['no_order'].'</span> nu a putut fi finalizata.';
			$page_info['alert_box']['messages'][] = '<span class="red-color">Plata cu cardul nu a putut fi efectuata sau tranzactia este inca in asteptare.</span>';
			$page_info['alert_box']['messages'][] = 'Pentru mai multe detalii va rugam sa ne contactati.';
			
		}
		
		#if error
		elseif( in_array($page_info['view']['status'], array(3, 4, 7)) || $page_info['view']['statusOmega'] == 'Reservation error' ){
			
			$page_info['alert_box']['heading'] = 'Ne pare rau, a aparut o eroare';
			$page_info['alert_box']['heading_class'] = 'red-background';
			$page_info['alert_box']['messages'][] = 'Confirmarea rezervarii dvs. cu numarul <span class="red-color">'.$page_info['view']['no_order'].'</span> nu a putut fi finalizata.';
			
			#if is paid
			if( $page_info['view']['statusMobilPay'] == 1 ){
				$page_info['alert_box']['messages'][] = 'Plata cu cardul a fost finalizata, va rugam sa ne contactati in cazul in care banii nu au fost inapoiati in contul dvs.';
			}
		}
		
		#if canceled
		elseif( $page_info['view']['status'] == 5 || $page_info['view']['statusOmega'] == 'Canceled' ){
				
			$page_info['alert_box']['heading'] = 'Rezervare anulata';
			$page_info['alert_box']['heading_class'] = 'orange-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este anulata.';
			
			#if is paid
			if( $page_info['view']['statusMobilPay'] == 1 ){
				$page_info['alert_box']['messages'][] = 'Plata cu cardul a fost finalizata, va rugam sa ne contactati in cazul in care banii nu au fost inapoiati in contul dvs.';
			}
		}
		
		#if money returned
		elseif( $page_info['view']['status'] == 2 ){
				
			$page_info['alert_box']['heading'] = 'Rezervare anulata';
			$page_info['alert_box']['heading_class'] = 'orange-background';
			$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este anulata.';
			$page_info['alert_box']['messages'][] = 'Suma de bani pe care ati platit-o v-a fost inapoiata in totalitate.';
		}
		
		#if is paid by client not means necessarily that the reservation is confirmed by omega
		elseif( $page_info['view']['status'] == 1 ){
		
			#if confirmed
			if( $page_info['view']['statusOmega'] == 'Confirmed' ){
				
				$page_info['alert_box']['heading'] = 'Rezervare confirmata si platita';
				$page_info['alert_box']['heading_class'] = 'green-background';
				$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="green-color">'.$page_info['view']['no_order'].'</span> este confirmata si platita.';
			}
			
			#if Pending confirmation
			elseif( $page_info['view']['statusOmega'] == 'Pending confirmation' ){
					
				$page_info['alert_box']['heading'] = 'Rezervare in asteptare';
				$page_info['alert_box']['heading_class'] = 'blue-background';
				$page_info['alert_box']['messages'][] = 'In acest moment rezervarea dvs. apare ca fiind <span class="blue-color">In asteptare</span>.';
				$page_info['alert_box']['messages'][] = 'In cel mai scurt timp posibil unul dintre operatorii nostri va va contacta. Va multumim !';
			}
			
			#if Pending cancelation
			elseif( $page_info['view']['statusOmega'] == 'Pending cancelation' ){
				
				$page_info['alert_box']['heading'] = 'Rezervare in curs de anulare';
				$page_info['alert_box']['heading_class'] = 'orange-background';
				$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este in curs de anulare.';
				$page_info['alert_box']['messages'][] = 'Pentru mai multe detalii va rugam sa ne contactati.';
			}
			
			#if In progress. this status is recived when make CheckReservation() after check-in date on Confirmed reservation
			elseif( $page_info['view']['statusOmega'] == 'In progress' ){
			
				$page_info['alert_box']['heading'] = 'Rezervare in desfasurare';
				$page_info['alert_box']['heading_class'] = 'blue-background';
				$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este in curs de desfasurare.';
				$page_info['alert_box']['messages'][] = 'Va dorim vacanta placuta !';
			}
			
			#if Finished. this status is recived when make CheckReservation() after check-out date on Confirmed reservation
			elseif( $page_info['view']['statusOmega'] == 'Finished' ){
			
				$page_info['alert_box']['heading'] = 'Rezervare terminata';
				$page_info['alert_box']['heading_class'] = 'blue-background';
				$page_info['alert_box']['messages'][] = 'Rezervarea dvs. cu numarul <span class="orange-color">'.$page_info['view']['no_order'].'</span> este terminata.';
				$page_info['alert_box']['messages'][] = 'Va multumim ca ati ales sa va petreceti vacanta cu noi !';
			}
		}
		
		#if no case client must contact an operator
		else {
			$page_info['alert_box']['heading'] = 'Ne pare rau, a aparut o eroare';
			$page_info['alert_box']['heading_class'] = 'red-background';
			$page_info['alert_box']['messages'][] = 'Confirmarea rezervarii dvs. cu numarul <span class="red-color">'.$page_info['view']['no_order'].'</span> nu a putut fi finalizata.';
			$page_info['alert_box']['messages'][] = 'Daca pana in acest moment nu ati fost contactat de catre un operator va rugam sa ne contactati pentru a va pune la dispozitie detaliile necesare despre rezervarea dvs. Va multumim !';
		}
		
	}
}