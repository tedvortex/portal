<?php
$start = microtime(true);

if (isset($page_info['room']) && isset($page_info['order_board']) && $locator == 'order') {
	
	$_construct['template'] .= '/order';
    
	$page_info['stop'] = false;
	
    if (mb_strlen($page_info['order_board']['cancellation']) <= 0) {
        
    	$option = array(
            'compression' => 1,
            'trace' => 1,
            'connection_timeout' => 10
        );

        try {
            $Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
        } catch (Exception $e) {
        	
        	$page_info['stop'] = true;
        	
            //print_a("Unable to fetch WSDL file!");
        }

        $cancellation = array(
            'PartnerCode' => 'mondialvoyages',
            'PlatformName' => 'mondialvoyages',
            'Username' => 'mondialvoyages',
            'Password' => 'gr$sd45Ds8vr',
            'Version' => 1,
            'SearchReference' => $page_info['query']['reference'],
            'CancellationPolicyRequestItems' => array(
                'CancellationPolicyRequestHotel' => array(
                    'ReservationRequestItemId' => 1,
                    'SearchResponseItemId' => $page_info['item']['id_item'],
                    'HotelRoomId' => $page_info['room']['id_room'],
                    'BoardId' => $page_info['order_board']['id_item'],
                ),
            ),
        );

        try {
            $result = $Client->CancellationPolicy($cancellation);
            
            if (isset($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy)
                && is_array($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy))
            {
                $db->query("
                        update `xp_search_boards`
                        set `cancellation` = ?
                        where `id_search_board` = ?", array(serialize($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy), $page_info['order_board']['id_search_board']));

                $page_info['order_board']['cancellation'] = $result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy;
            }
            elseif(
            	isset($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy)
            	&& $result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy instanceof stdClass
            ){
            	$db->query("
                        update `xp_search_boards`
                        set `cancellation` = ?
                        where `id_search_board` = ?", array(serialize(array($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy)), $page_info['order_board']['id_search_board']));
            	
            	$page_info['order_board']['cancellation'] = array($result->CancellationPolicyResponseItems->CancellationPolicyResponseHotel->CancellationPolicyResponseHotelCancellationPolicies->CancellationPolicyResponseCancellationPolicy);
            }
            
        } catch (SoapFault $Exception) {
        	
        	$page_info['stop'] = true;
        	
            //print_a($Exception);
        }
    }
    else {
        $page_info['order_board']['cancellation'] = unserialize($page_info['order_board']['cancellation']);
    }
    
    $deadline = false;
    $page_info['payment_only_card'] = true;
    
    #if no cancellation policy we can not reserve
    if(isset($page_info['order_board']['cancellation']) && $page_info['order_board']['cancellation']){
        
    	# try deadline: first cancellation interval with price > 0
    	foreach($page_info['order_board']['cancellation'] as $c){
    		
    		if( $c->Price > 0 ){
    			
    			$deadline = $c;
    			break;
    		}
    	}
    	
    	# if it was found a deadline interval we must verify the days difference between current datetime and starting date of deadline
    	# if number of days is bigger or equal with $config->omega_until_deadline until we enter the deadline period customer can pay in any way, else he can pay only with credit card
    	if( $deadline !== false ){
    		
    		$present = DateTime::createFromFormat( 'Y-m-d H:i:s', date('Y-m-d H:i:s', time()) );
     		$from_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $deadline->FromDate );
    		 		
    		$diff = $present->diff($from_date);
    		
    		if($diff->invert == 0 && $diff->d >= $config->omega_until_deadline){
    			$page_info['payment_only_card'] = false;
    		}
	   	}
	   	# if not found a deadline interval mean that is no cancel pollicy with cancellation charge, so customer can pay in any way
	   	else {
	   		$page_info['payment_only_card'] = false;
	   	}
    }
    else{
    	$page_info['stop'] = true;
    }
    
    #stop reservation if price currency received from omega not exists in our database or it's not active
    if( ! isset($currenciesAllOmega[ $page_info['order_board']['currency'] ]) ){
    	$page_info['stop'] = true;
    } 
    
    $page_info['location'] = false;

    $tourists_form_values = array();

    $array_countries = $db->fetchAll("
            select `id_country` id, `name`
            from `ev_country`
            order by `name` asc");

    if(!isset($_REQUEST['country'])){
        $_REQUEST['country'] = 183;
    }

    $array_form_individual =
    array(
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=> false,
            'elements'=>
            array(
                array('id'=>'lastname','name'=>'Nume','title'=>'Nume','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'firstname','name'=>'Prenume','title'=>'Prenume','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'address','name'=>'Adresa','title'=>'Adresa','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>240,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'country','name'=>'Tara','title'=>'Tara', 'tip'=>'','placeholder'=>'Tara','lang'=>true,'label'=>true,'tag'=>'select','type'=>'text','options'=>$array_countries,'maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'state','name'=>'Judet/Sector','title'=>'Judet/Sector','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'city','name'=>'Oras','title'=>'Oras','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'cnp','name'=> ($_REQUEST['country'] == 183 ? 'CNP' : 'PIN / Passport No.'),'title'=>'Cnp','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>20,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'phone','name'=>'Telefon','title'=>'Telefon','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
    );

    $array_form_company =
    array(
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'company_name','name'=>'Nume firma','title'=>'Nume firma','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            ),
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'address','name'=>'Adresa','title'=>'Adresa','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>240,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'city','name'=>'Oras','title'=>'Oras','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'country','name'=>'Tara','title'=>'Tara', 'tip'=>'','placeholder'=>'Tara','lang'=>true,'label'=>true,'tag'=>'select','type'=>'text','options'=>$array_countries,'maxlength'=>60,'autocomplete'=>false,'autofocus'=>true,'required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'state','name'=>'Judet/Sector','title'=>'Judet/Sector','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf reg-com',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'company_reg','name'=>'Nr. Reg. Com.','title'=>'Nr. Reg. Com.','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>false,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'side cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'company_cui','name'=>'CUI','title'=>'CUI','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'email','name'=>'Email','title'=>'Email','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'email','required'=>true,'errors'=>true,'regexp'=>'email',),
            )
        ),
        array(
            'wrapper'=>'definition list',
            'class'=>'cf',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'phone','name'=>'Telefon','title'=>'Telefon','placeholder'=>'','lang'=>true,'label'=>true,'maxlength'=>80,'autocomplete'=>false,'tag'=>'input','type'=>'text','required'=>true,'errors'=>true,'regexp'=>'textarea',),
            )
        ),
    );

    $array_form_details =
    array(
        array(
            'wrapper'=>'definition list',
            'class'=>'cf full-size',
            'heading'=>'',
            'heading_tag'=>'h2',
            'inside'=>false,
            'elements'=>
            array(
                array('id'=>'details','name'=>'Alte detalii sau intrebari despre rezervarea dvs.','title'=>'Alte detalii sau intrebari despre rezervarea dvs.','placeholder'=>'','lang'=>true,'label'=>true,'tag'=>'textarea','maxlength'=>1000,'required'=>false,'errors'=>true,'regexp'=>'textarea',),
            ),
        ),
    );

    $array_payment_methods = array(
        0 => 'Banca (cash/depunere)',
        1 => 'Cash la sediul Click&Go',
        2 => 'Card bancar'
    );

    $page_info['tourists_form'] = write_tourists_form($page_info['query']['adults'], (isset($_GET['children_ages']) ? $_GET['children_ages'] : array()), (isset($tourists_form_values) ? $tourists_form_values : array()));

    $page_info['form_individual'] = $form->factory($array_form_individual);
    $page_info['form_company'] = $form->factory($array_form_company);
    $page_info['form_details'] = $form->factory($array_form_details);
    
    $page_info['returnUrlCard'] = $b . url($page_info['name_seo']) . '/view/';
    
    if ( ! $page_info['stop'] && isset($_POST['sejur_reservation']) && $_POST['sejur_reservation'] != 1) {
        
    	if (! isset($_POST['person_type']) || ! is_numeric($_POST['person_type']) || ! in_array($_POST['person_type'], array(0,1))) {
            $error['person_type'] = 'Trebuie sa introduceti datele de facturare!';
        } else {
            $error = $form->validate( $_POST['person_type'] == 0 ? $array_form_individual : $array_form_company , $_POST);
        }

        if (isset($_POST['country']) && is_numeric($_POST['country'])) {
            if (! $country_name = $db->fetchOne("
                        select `name`
                        from `ev_country`
                        where `id_country` = ? ",
                        $_POST['country'])
            ) {
                $error['country'] = 'Trebuie sa alegeti tara pentru detaliile de facturare';
            }
        } else {
            $error['country'] = 'Trebuie sa alegeti tara pentru detaliile de facturare';
        }

        $error = array_merge($error, $form->validate($array_form_details, $_POST));

        if (isset($_POST['adults']) && is_array($_POST['adults'])) {
            foreach( $_POST['adults'] as $k => $a ){
                if(
                !isset($a['title']) || is_array($a['title']) || !in_array($a['title'], range(0,2)) ||
                !isset($a['lastname']) || is_array($a['lastname']) || $a['lastname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $a['lastname']) ||
                !isset($a['firstname']) || is_array($a['firstname']) || $a['firstname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $a['firstname']) ||
                !isset($a['birthdate'])
                ){
                    $error['adults'] = 'Detaliile turistilor (adulti) sunt obligatorii!';
                }
                else{
                    if( !$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' ) ){
                        $error['adults'] = 'Detaliile turistilor (adulti) sunt obligatorii!';
                    }
                    else{
                        $tourists_form_values['adults'][$k] = $a;
                    }
                }
            }
        } else {
            $error['adults'] = 'Detaliile turistilor sunt obligatorii!';
        }

        if (isset($_POST['children']) && is_array($_POST['children'])) {
            foreach( $_POST['children'] as $k => $c ){
                $array_ages = array( date('Y', time()) - $_GET['children_ages'][$k] );
                $array_ages[1] = $array_ages[0] - 1;

                if(
                !isset($c['title']) || is_array($c['title']) || !in_array($c['title'], range(0,2)) ||
                !isset($c['lastname']) || is_array($c['lastname']) || $c['lastname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $c['lastname']) ||
                !isset($c['firstname']) || is_array($c['firstname']) || $c['firstname'] == '' || !preg_match('%^[\p{L}\p{M}\p{P}\s]+$%', $c['firstname']) ||
                !isset($c['birthdate'])
                ){
                    $error['children'] = 'Detaliile turistilor (copii) sunt obligatorii!';
                }

                else{

                    if( !$date = DateTime::createFromFormat('d.m.Y H:i:s', $c['birthdate'].' 00:00:00' ) ){
                        $error['children'] = 'Detaliile turistilor (copii) sunt obligatorii!';
                    }
                    elseif( $_GET['children_ages'][$k] != age($c['birthdate']) ){
                        $error['children_dates'] = 'Datele de nastere ale copiilor nu corespund cu varstele introduse!';
                    }
                    else{
                        $tourists_form_values['children'][$k] = $c;
                    }
                }
            }

        }

        if (
        	! isset($_POST['payment_method']) || ! is_numeric($_POST['payment_method']) || ! in_array($_POST['payment_method'], range(0,2)) || 
        	($page_info['payment_only_card'] && $_POST['payment_method'] != 2)
        ){
            $error['payment_method'] = 'Trebuie sa alegeti una dintre metodele de plata!';
        }

        if (! isset($_POST['accept_terms']) || $_POST['accept_terms'] != 1) {
            $error['accept_terms'] = 'Pentru a putea trece mai departe trebuie sa fiti de acord cu Termenii si conditiile si cu Politica de anulare';
        }
        
        if (empty($error)) {
        	
       		#first insert in xp_orders the reservation with default statusMobilpay, status, statusOmega, omegaInitiated, referenceCode, reservationReferenceOmega, errorMessageReservationOmega, errorMessageCheckReservationOmega and errorMessageCheckReservationCronOmega
       		#then insert in xp_orders_products reservation details with default reservationParametersOmega, reservationResultOmega, checkReservationResultOmega, confirmReservationResultOmega and checkReservationCronResultOmega
            $no_order = generate_random_string(6, 'omega-' . $page_info['order_board']['id_search_board']);

            while ($db->fetchOne("
                    select `id`
                    from `{$t['o']}`
                    where `no_order` = ? ",
                    $no_order
            )) {
                $no_order = generate_random_string(6, 'omega-' . $page_info['order_board']['id_search_board']);
            }
            
            $page_info['returnUrlCard'] .= $no_order;

            #statusMobilpay is last status recived from mobilpay
            #status is the current status of order: 0 - unpaid, 1 - paid, 2 - money returned, 3 - error on Reservation() or CheckReservation, 4 - error on ConfirmReservation(), 5 - canceled, 6 - error Mobilpay, 7 - error on omegaStatusUpdate Cron
            #statusOmega is the status of reservation on Omega sistem
            #omegaInitiated is 1 if Reservation() succeded first time (it is use on mobilpay payment - check mobilpay_confirm_card.php)
            $values = array(
                'id_category' => $page_info['id'],
                'category_name' => $page_info['name'],
                'id_customer' => 0,
                'no_order' => $no_order,
            	'statusMobilpay' => 0,
                'status' => 0,
            	'statusOmega' => '',
            	'omegaInitiated' => 0,
            	'referenceCodeOmega' => str_replace('-', '', $no_order),
            	'reservationReferenceOmega' => '',
            	'errorMessageReservationOmega' => '',
            	'errorMessageCheckReservationOmega' => '',
            	'errorMessageCheckReservationConfirmOmega' => '',
            	'errorMessageConfirmReservationOmega' => '',
            	'errorMessageCheckReservationCronOmega' => '',
                'date' => time(),
                'adults' => $page_info['room']['adults'],
                'children' => $page_info['room']['children'],
                'payment_method' => $_POST['payment_method'],
                'buyer_type' => $_POST['person_type'],
                'buyer_address' => $_POST['address'],
                'buyer_city' => $_POST['city'],
                'buyer_id_country' => $_POST['country'],
                'buyer_country' => $country_name,
                'buyer_state' => $_POST['state'],
                'buyer_email' => $_POST['email'],
                'buyer_phone' => $_POST['phone'],
                'ip' => ip(),
                'details' => isset($_POST['details']) && !is_array($_POST['details']) && $_POST['details'] ? $_POST['details'] : '');

            $values = array_merge($values,
                    ( $_POST['person_type'] == 0 
                    ? array(
                    'buyer_last_name' => $_POST['lastname'],
                    'buyer_first_name' => $_POST['firstname'],
                    ( $_POST['country'] == 183 ? 'buyer_cnp' : 'buyer_pin' ) => $_POST['cnp'],)
                    : array(
                    'buyer_company' => $_POST['company_name'],
                    'buyer_reg_com' => $_POST['country'] == 183 ? (isset($_POST['company_reg']) && !is_array($_POST['company_reg']) ? $_POST['company_reg'] : '') : '',
                    ( $_POST['country'] == 183 ? 'buyer_cui' : 'buyer_vat' ) => $_POST['company_cui'],)));

            $db->insert($t['o'],$values);

            $id_order = $db->lastInsertId();

            foreach($_POST['adults'] as $a){
                
            	$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' );

                $db->insert($t['o'].'_people', array(
                        'id_order' => $id_order,
                        'is_child' => 0,
                        'title' => $a['title'],
                        'birthdate' => $date->format('Y-m-d'),
                        'last_name' => $a['lastname'],
                        'first_name' => $a['firstname'],
                    ));
            }

            if( isset($_POST['children']) ){
                foreach($_POST['children'] as $a){
                    
                	$date = DateTime::createFromFormat('d.m.Y H:i:s', $a['birthdate'].' 00:00:00' );

                    $db->insert($t['o'].'_people', array(
                        'id_order' => $id_order,
                        'is_child' => 1,
                        'title' => $a['title'],
                        'birthdate' => $date->format('Y-m-d'),
                        'last_name' => $a['lastname'],
                        'first_name' => $a['firstname'],
                    ));
                }
            }

            $name = $page_info['room']['name'] . ' - ' . $page_info['item']['hotel_name'] . ', ' . $page_info['query']['city_name'];

            $values_product = array(
                'id_order' => $id_order,
                'id_product' => $page_info['order_board']['id_search_board'],
                //'id_hotel_entity' => $page_info['cost']['id_hotel_entity'],
                'id_hotel' => $page_info['item']['id_hotel'],
                //'id_cost' => $page_info['cost']['id'],
                //'hotel_entity_code' => $hotel_entity['code'],
                'checkin' => $page_info['query']['checkin'],
                'duration' => $page_info['query']['nights'],
                //'transport' => $_GET['transport'],
                //'id_board_type' => $page_info['cost']['id_board_type'],
                //'id_room_type' => $page_info['cost']['id_room_type'],
                'id_currency' => $currenciesAllOmega[ $page_info['order_board']['currency'] ]['id'],
                'id_confirmation_type' => 1,
                'cost_type' => 0,
                'is_standard_price' => 0,
                'price' => $page_info['order_board']['price'],
            	'priceAvailabilityOmega' => $page_info['order_board']['price'],
            	'commission' => $config->comision_omega,
                //'price_transport' => $page_info['cost']['price_transport'],
            	'totalAvailabilityOmega' => commission($page_info['order_board']['price']),
                'total' => commission($page_info['order_board']['price']),
                'name' => $name,
                'hotel_name' => $page_info['item']['hotel_name'],
                'board_type_name' => $page_info['order_board']['name'],
                'room_type_name' => $page_info['room']['name'],
                //'confirmation_type_name' => $page_info['cost']['confirmation_type_name'],
                'reservationParametersOmega' => '',
                'reservationResultOmega' => '',
            	'checkReservationResultOmega' => '',
            	'confirmReservationResultOmega' => '',
            	'checkReservationCronResultOmega' => ''
            );

            $db->insert($t['o'].'_products', $values_product);

            if ($values['buyer_type'] == 0) {
                $buyer = '<p>Nume: '.$values['buyer_last_name'].'</p>'
                       . '<p>Prenume: '.$values['buyer_first_name'].'</p>'
                       . '<p>'.( $_POST['country'] == 183 ? 'CNP' : 'PIN / Passport No.' ).': '.( $_POST['country'] == 183 ? $values['buyer_cnp'] : $values['buyer_pin'] ).'</p>';
            } else {
                $buyer = '<p>Companie: '.$values['buyer_company'].'</p>'
                       . '<p>Nr. reg. com.: '.($values['buyer_reg_com'] ? $values['buyer_reg_com'] : '-').'</p>'
                       . '<p>'.( $_POST['country'] == 183 ? 'CUI' : 'Numar inregistrare (VAT number)' ).': '.( $_POST['country'] == 183 ? $values['buyer_cui'] : $values['buyer_vat'] ).'</p>';
            }

            $email_part1 = array(
                'Numar comanda' => '#'.$no_order,
                'Data rezervare' => date('Y-m-d'),
                'Sejur' => $values_product['name'],
                'Data cazare' => $values_product['checkin'],
                'Durata sejur' => $values_product['duration'].' nopti',
                'Hotel' => $values_product['hotel_name'],
                'Masa' => $values_product['board_type_name'],
                'Camera' => $values_product['room_type_name'],
                'Turisti' => $page_info['room']['adults'] . ' adult' . ($page_info['room']['adults'] == 1 ? '' : 'i') . ($page_info['room']['children'] > 0 ? ' si ' . $page_info['room']['children'] . ' copii ('.$page_info['children_ages_string'].')' : ''),
                'Pret' => $values_product['total'].' '.$page_info['order_board']['currency'],
                //'Transport' => $page_info['cost']['price_transport_description'] ? $page_info['cost']['price_transport_description'] : '-',
                //'Confirmare' => $values_product['confirmation_type_name'],
            );

            $email_part2 = array(
                'Modalitate de plata' => $array_payment_methods[$values['payment_method']],
                'Tip persoana' => $values['buyer_type'] == 0 ? 'Persoana fizica' : 'Persoana juridica (companie)',
            );

            if ($values['buyer_type'] == 0) {
            	
                $email_part2['Nume'] = $values['buyer_last_name'];
                $email_part2['Prenume'] = $values['buyer_first_name'];
                $email_part2[ $_POST['country'] == 183 ? 'CNP' : 'PIN / Passport No.' ] = ( $_POST['country'] == 183 ? $values['buyer_cnp'] : $values['buyer_pin'] );
            }
            else {
                $email_part2['Companie'] = $values['buyer_company'];
                $email_part2['Nr. reg. com.'] = ($values['buyer_reg_com'] ? $values['buyer_reg_com'] : '-');
                $email_part2[ $_POST['country'] == 183 ? 'CUI' : 'Numar inregistrare (VAT number)' ] = $_POST['country'] == 183 ? $values['buyer_cui'] : $values['buyer_vat'];
            }

            $email_part2 = array_merge($email_part2, array(
                'Email' => $values['buyer_email'],
                'Telefon' => $values['buyer_phone'],
                'Tara' => $values['buyer_country'],
                'Judet/Sector' => $values['buyer_state'],
                'Oras' => $values['buyer_city'],
                'Adresa' => $values['buyer_address'],
                'Detalii' => $values['details'] ? $values['details'] : '-',));
            
            #send email
            template_mail(
           		'order_omega',
            	array($config->site_email, $values['buyer_email']),
            	array(
            		'category' => $values['category_name'],
            		'part1' => return_html_mail_list($email_part1, 'Detalii rezervare'),
            		'people' => return_html_mail_tourists($_POST['adults'], isset($page_info['room']['children']) ? $page_info['room']['children'] : array()),
            		'part2' => return_html_mail_list($email_part2, 'Detalii facturare'),
            		'view_url' => '<a href="'.$page_info['returnUrlCard'].'" title="Vezi rezervare">'.$page_info['returnUrlCard'].'</a>'
            	)
            );
            
            #construct $reservation parameters for Omega Reservation() method request
            $reservation = array(
            	'PartnerCode' => 'mondialvoyages',
            	'PlatformName' => 'mondialvoyages',
            	'Username' => 'mondialvoyages',
            	'Password' => 'gr$sd45Ds8vr',
            	'Version' => 1,
            	'SearchReference' => $page_info['query']['reference'],
            	'HolderId' => 1,
            	'ReservationRequestItems' => array(
            		'ReservationRequestHotel' => array(
            			'ItemId' => 1,
            			'SearchResponseItemId' => $page_info['item']['id_item'],
            			'HotelRoomId' => $page_info['room']['id_room'],
            			'BoardId' => $page_info['order_board']['id_item'],
            			'Currency' => $page_info['item']['currency'],
            			'Reference' => $values['referenceCodeOmega'],
            			'Remarks' => isset($_POST['details']) && $_POST['details'] ? $_POST['details'] : '',
            			'ReservationRequestRooms' => array(
            				'ReservationRequestRoom' => array()
            			)
            		),
            	)
            );
             
            $room_all = unserialize($page_info['room']['room']);
             
            $room_all = $room_all->AvailabilityResponseRooms->AvailabilityResponseRoom;
             
            if($room_all instanceof stdClass ){
            	$room_all = array($room_all);
            }
             
            $ord_pax = 0;
            $ord_id = 0;
            foreach($room_all as $k => $room){
            	 
            	for($j = 1; $j <= $room->NumberOfRooms; $j++){
            		 
            		$ord_id++;
            		 
            		$xml_room = array(
            			'RoomId' => $ord_id,
            			'SearchRoomId' => $room->Id,
            			'Code' => $room->Code,
            			'ReservationRequestPaxList' => array()
            		);
            		 
            		$child_name = array('FirstName' => 'Child', 'LastName' => 'Name');
            		 
            		for($i = 1; $i <= $room->Adults; $i++ ){
            			 
            			$ord_pax++;
            			 
            			$pax = $ord_pax == 1 ? reset($_POST['adults']) : next($_POST['adults']);
            			 
            			if( $pax ){
            				 
            				if( $i == 1 ){
            					$child_name = array('FirstName' => $pax['firstname'], 'LastName' => $pax['lastname']);
            				}
            				 
            				$date = DateTime::createFromFormat('d.m.Y H:i:s', $pax['birthdate'].' 00:00:00' );
            				 
            				$xml_room['ReservationRequestPaxList']['ReservationRequestPax'][] = array(
            					'Id' => $ord_pax,
            					'FirstName' => $pax['firstname'],
            					'LastName' => $pax['lastname'],
            					'ReservationPaxType' => 'AD',
            					'Birthday' => $date->format('Y-m-d'),
            					'Age' => age($date->format('d.m.Y'))
            				);
            			}
            		}
            		 
            		if( $room->Children > 0 ){
            			for($i = 1; $i <= $room->Children; $i++ ){
            				$ord_pax++;
            				 
            				$xml_room['ReservationRequestPaxList']['ReservationRequestPax'][] = array(
            					'Id' => $ord_pax,
            					'FirstName' => $child_name['FirstName'],
            					'LastName' => $child_name['LastName'],
            					'ReservationPaxType' => 'CH',
            					'Age' => $room->Children == 1 ? $room->AvailabilityResponseChildrenAges->ChildAge : $room->AvailabilityResponseChildrenAges->ChildAge[$i-1]
            				);
            				 
            			}
            		}
            		 
            		$reservation['ReservationRequestItems']['ReservationRequestHotel']['ReservationRequestRooms']['ReservationRequestRoom'][] = $xml_room;
            	}
            }

            #options parameters for SoapClient() request
            $option = array(
            	'compression' => 1,
            	'trace' => 1,
            	'connection_timeout' => 180
            );
            
            #options for exit
            $exit_params = array(
            	'location' => $b,
            	'window' => true,
            	'module' => 'omega',
            	'type' => 'error',
            	'params' => array( 'code:'.$no_order )
            );
            
            #save $reservation in database
            $db->query("
            	update `xp_orders_products`
            	set `reservationParametersOmega` = :reservationParametersOmega
            	where `id_order` = :id_order ",
            	array(
            		'reservationParametersOmega' => serialize($reservation),
            		'id_order' => $id_order
            	)
            );
            
            $continue = true;
            
            #if payment method is not credit card
            if( $_POST['payment_method'] != 2 ){
            	
            	#try to connect with SoapClient
           	    try {
            		$Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
            	}
            	catch (Exception $e) {
					$continue = false;            		
            	}
            	
            	#try Reservation() method
            	if( $continue ){
            		
            		try {
            			$result = $Client->Reservation($reservation);
            		}
            		catch (SoapFault $Exception) {
            			$continue = false;
            		}
            	}
            	
            	#verify if Reservesion() method returned an error
            	if( $continue && ( isset($result->ReservationResponseReservationVersions->ErrorMessage) || ! isset($result->ReservationResponseReservationVersions->ReservationReference) ) ){
            		
            		$continue = false;
            	}
            	
            	#if Reservation() succeeded check the reservation to see the current status
            	#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
            	if( $continue ){

            		#with ReservationReference will identify on Omega the current reservation. is like an unique id of reservation
            		$values['reservationReferenceOmega'] = $result->ReservationResponseReservationVersions->ReservationReference;
            		
            		#save serialized result from Reservation() method
            		$values_product['reservationResultOmega'] = serialize($result);
            		
            		$checkReservation = array(
            			'PartnerCode' => 'mondialvoyages',
            			'PlatformName' => 'mondialvoyages',
            			'Username' => 'mondialvoyages',
            			'Password' => 'gr$sd45Ds8vr',
            			'Version' => 1,
            			'ReservationReference' => $values['reservationReferenceOmega']
            		);
            		
            		try {
            			$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
            		}
            		catch (SoapFault $Exception) {
            			$continue = false;            			
            		}
            	}
            	
            	#verify if CheckReservenion() method returned an error
            	if( $continue && ( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) || ! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status) ) ){
            		
            		$continue = false;
            	}
            	
            	#if CheckReservation() succeeded
            	if( $continue ){
            		
            		#mark as initiated
            		$values['omegaInitiated'] = 1;
            		
            		#save the current status of reservation
            		$values['statusOmega'] = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status;
            		
            		#save the price of reservation returned by CheckReservation(). This price is same with the price recived by availability step, saved in $page_info['order_board'].
            		$values_product['price'] = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->ReservationResponseItems->ReservationResponseHotel->ReservationResponseReservationPrice->Total;
            		$values_product['total'] = commission($values_product['price']);
            		
            		#save serialized result from CheckReservation() method
            		$values_product['checkReservationResultOmega'] = serialize($checkReservationResultOmega);
            		
            		$db->query("
            			update `xp_orders`
            			set `omegaInitiated` = :omegaInitiated, `reservationReferenceOmega` = :reservationReferenceOmega, `statusOmega` = :statusOmega
            			where `id` = :id_order ",
            			array(
            				'omegaInitiated' => $values['omegaInitiated'],
            				'reservationReferenceOmega' => $values['reservationReferenceOmega'],
            				'statusOmega' => $values['statusOmega'],
            				'id_order' => $id_order
            			)
            		);
            		
            		$db->query("
            			update `xp_orders_products`
            			set `price` = :price, `total` = :total, `reservationResultOmega` = :reservationResultOmega, `checkReservationResultOmega` = :checkReservationResultOmega
            			where `id_order` = :id_order ",
            			array(
            				'price' => $values_product['price'],
            				'total' => $values_product['total'],
            				'reservationResultOmega' => $values_product['reservationResultOmega'],
            				'checkReservationResultOmega' => $values_product['checkReservationResultOmega'],
            				'id_order' => $id_order
            			)
            		);
            		
            		#if statusOmega is Confirmed reservation is finished and exit with success message
            		if( $values['statusOmega'] == 'Confirmed' ){
            			$exit_params['type'] = 'success';
            		}
            		
            		#if statusOmega is Pending confirmation client must waiting for the response from Omega
            		#now we will exit with information message
            		elseif( $values['statusOmega'] == 'Pending confirmation' ){
            			$exit_params['type'] = 'information';
            		}
            		
            		#if statusOmega is Requires confirmation client must Confirm the price again (or the new price)
            		#to do that we will redirect the page on cazari/confirm/no_order
            		#until he will confirm the reservation the status will remain 0 and the statusOmega will remain Requires confirmation
            		#if the reservation will not be confirmed in 10 minutes, Omega system will cancel automatically the reservation
            		elseif( $values['statusOmega'] == 'Requires confirmation' ){
            			
            			$exit_params['location'] = $b . url($page_info['name_seo']) . '/confirm/' . $values['no_order'];
            			$exit_params['window'] = false;
            		}
            		
            		#basically in this moment we can not recive Canceled or Pending cancelation statuses (according to the Omega documentation)
            		#in case we recived one of this two statuses we will exit with error message because the reservation is not confirmed
            		#last status that we can recive from Omega is Reservation error 
            		else {
            			#we have no modification to do here because error message was set on start 
            		}
            	}
            	
            	#if $continue is false means that Reservation() or CheckReservation() has not been successfully completed
            	#in this case we have an error on Reservation() or CheckReservation(), or connection with Omega has not been made in one of this two methods
            	if( ! $continue ){
            		
            		#set status of order with value 3, which means that it was a problem 
            		$db->query("
            			update `xp_orders`
            			set `status` = 3
            			where `id` = ? ",
            			$id_order
            		);
            		
            		#save in database the results return by Request on omega: Reservation() and CheckReservation()
            		$db->query("
            			update `xp_orders_products`
            			set `reservationResultOmega` = :reservationResultOmega, `checkReservationResultOmega` = :checkReservationResultOmega
            			where `id_order` = :id_order ",
            			array(
            				'reservationResultOmega' => isset($result) ? serialize($result) : '',
            				'checkReservationResultOmega' => isset($checkReservationResultOmega) ? serialize($checkReservationResultOmega) : '',
            				'id_order' => $id_order
            			)
            		);
            		
            		#if recived an error on Reservation() method save it in database
            		if( isset($result->ReservationResponseReservationVersions->ErrorMessage) ){
            			
            			$db->query("
	            			update `xp_orders`
	            			set `errorMessageReservationOmega` = :errorMessageReservationOmega
	            			where `id` = :id_order ",
							array(
            					'errorMessageReservationOmega' => $result->ReservationResponseReservationVersions->ErrorMessage,
								'id_order' => $id_order
            				)
            			);
            		}
            		
            		#if recived an error on CheckReservation() method save it in database
            		if( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
            			
            			$db->query("
	            			update `xp_orders`
	            			set `errorMessageCheckReservationOmega` = :errorMessageCheckReservationOmega
	            			where `id` = :id_order ",
            				array(
            					'errorMessageCheckReservationOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
            					'id_order' => $id_order
            				)
            			);
            		}
            	}
            	
            	#exit with proper message
            	$string_exit = "Location: {$exit_params['location']}";
            	if( $exit_params['window'] ){
            		$string_exit .= '#action,'.$exit_params['module'].','.$exit_params['type'];
            		 
            		if( ! empty($exit_params['params']) ){
            			$string_exit .= ','.implode('&', $exit_params['params']);
            		}
            	}
            	
            	exit(header($string_exit));
            }
            
            #if payment method is credit card
            else {
            	
            	require_once(dirname(dirname(dirname(__FILE__))) .'/order_card_omega.php');
            }
            
	           /* template_mail(
                'order_sejur_new',
                array($config->site_email, 'dinu.andreidr@yahoo.com'),
                array(
                    'category' => $values['category_name'],
                    'part1' => return_html_mail_list($email_part1, 'Detalii rezervare'),
                    'people' => return_html_mail_tourists($_POST['adults'], isset($page_info['room']['children']) ? $page_info['room']['children'] : array()),
                    'part2' => return_html_mail_list($email_part2, 'Detalii facturare'))); */

//              exit(header("Location: {$b}#action,order,success,code:".$no_order));
        } else {
            $globals['window'] = "Window.init( \"".tr("Este necesar sa completati toate campurile marcate pentru a va putea inregistra!")."<br/><br/>".implode('<br/><br/>',$error)."\", '"._('Actiune nereusita!')."', 'error' )";
        }
    }
}