<?php
//if(isset($_POST)){
//	$data_post = serialize($_POST);
//	$db->insert('xp_test',array('value'=>$data_post));
//}
//exit();

//print_a($_SERVER['REQUEST_METHOD']);
//$test = $db->fetchOne("select `value` from `xp_test` order by `id` desc");
//$_POST = unserialize($test);

require_once(i . 'Mobilpay/Payment/Request/Abstract.php');
require_once(i . 'Mobilpay/Payment/Request/Card.php');
require_once(i . 'Mobilpay/Payment/Request/Notify.php');
require_once(i . 'Mobilpay/Payment/Invoice.php');
require_once(i . 'Mobilpay/Payment/Address.php');

$errorCode = 0;
$errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_NONE;
$errorMessage = '';



function update_order ($status_code)
{
    global $db, $objPmReq;

    $status = array(
    	'anulata' => 0,
    	'confirmed' => 1,
    	'confirmed_pending' => 2,
    	'paid_pending' => 3,
    	'paid' => 4,
    	'canceled' => 5,
    	'credit' => 6
    );
    
    //select order from database
    $current_order = $db->fetchRow("
    	select
    		o.`id`, o.`id_category`, o.`omegaInitiated`,
    		op.`reservationParametersOmega`
    	from
    		`xp_orders` o
    		inner join `xp_orders_products` op
    			on op.`id_order` = o.`id`
    	where `no_order` = ? ",
    	$objPmReq->orderId
	);

    if( $current_order ){
    
	    //insert in history of mobilpay actions
	    $db->insert('xp_orders_mobilpay', array(
	    	'id_order' => $current_order['id'],
	    	'no_order' => $objPmReq->orderId,
	    	'errorCode' => $objPmReq->objPmNotify->errorCode,
	    	'status' => ! $objPmReq->objPmNotify->errorCode ? $status[$status_code] : 0,
	    	'date' => date('Y-m-d H:i:s', time())
	    ));
	    
	    // comanda nu este de tip Omega
	    if( $current_order['id_category'] != 2 ){
	    	
		    $db->query("
		        update `xp_orders`
		        set
		            `status` = ?,
		            `errorCode` = ?
		        where `no_order` = ?",
		        array((! $objPmReq->objPmNotify->errorCode ? $status[$status_code] : 0), $objPmReq->objPmNotify->errorCode, $objPmReq->orderId));
	    }
	    
	    // comanda este de tip Omega
	    else {
	    	
	    	$db->query("
		        update `xp_orders`
		        set
		            `statusMobilpay` = ?,
		            `errorCode` = ?,
	    			`status` = ?
		        where `no_order` = ?",
	    		array(
	    			(! $objPmReq->objPmNotify->errorCode ? $status[$status_code] : 0),
	    			$objPmReq->objPmNotify->errorCode,
	    			(! $objPmReq->objPmNotify->errorCode && $status_code == 'confirmed' ? 1 : 6),
	    			$objPmReq->orderId
	    		)
	    	);
	    	
	    	//if transaction is confirmed we can reserve on Omega if reservation was not initiated yet
	    	if(! $objPmReq->objPmNotify->errorCode && $status_code == 'confirmed' && $current_order['omegaInitiated'] == 0 ){
	    		
	    		$continue = true;
	    		
	    		#options parameters for SoapClient() request
	    		$option = array(
		    		'compression' => 1,
		    		'trace' => 1,
		    		'connection_timeout' => 180
	    		);
	    		
	    		#try to connect with SoapClient
	    		try {
	    			$Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
	    		}
	    		catch (Exception $e) {
	    			$continue = false;
	    		}
	    		 
	    		#try Reservation() method
	    		if( $continue ){
	    		
	    			try {
	    				$result = $Client->Reservation( unserialize($current_order['reservationParametersOmega']) );
    				}
	    			catch (SoapFault $Exception) {
	    				$continue = false;
	    			}
    			}
	    			 
    			#verify if Reservesion() method returned an error
    			if( $continue && ( isset($result->ReservationResponseReservationVersions->ErrorMessage) || ! isset($result->ReservationResponseReservationVersions->ReservationReference) ) ){
	    		
	    			$continue = false;
	    		}
	    		 
	    		#if Reservation() succeeded check the reservation to see the current status
	    		#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
    			if( $continue ){
	    		
	    			#with ReservationReference will identify on Omega the current reservation. is like an unique id of reservation
	    			$values['reservationReferenceOmega'] = $result->ReservationResponseReservationVersions->ReservationReference;
	    		
	    			#save serialized result from Reservation() method
	    			$values_product['reservationResultOmega'] = serialize($result);
	    		
	    			$checkReservation = array(
	    				'PartnerCode' => 'mondialvoyages',
	    				'PlatformName' => 'mondialvoyages',
	    				'Username' => 'mondialvoyages',
	    				'Password' => 'gr$sd45Ds8vr',
	    				'Version' => 1,
	    				'ReservationReference' => $values['reservationReferenceOmega']
    				);
	    		
    				try {
	    				$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
    				}
    				catch (SoapFault $Exception) {
    					$continue = false;
	    			}
	    		}
	    						 
	    		#verify if CheckReservenion() method returned an error
	    		if( $continue && ( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) || ! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status) ) ){
	    		
    				$continue = false;
    			}
    			
    			#if CheckReservation() succeeded
    			if( $continue ){
    			
    				#mark as succeeded
    				$values['omegaInitiated'] = 1;
    			
    				#save the current status of reservation
    				$values['statusOmega'] = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status;
    			
    				#save the price of reservation returned by CheckReservation(). This price is same with the price received by availability step, saved in $page_info['order_board'].
    				$values_product['price'] = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->ReservationResponseItems->ReservationResponseHotel->ReservationResponseReservationPrice->Total;
    				$values_product['total'] = commission($values_product['price']);
    			
    				#save serialized result from CheckReservation() method
    				$values_product['checkReservationResultOmega'] = serialize($checkReservationResultOmega);
    			
    				$db->query("
    					update `xp_orders`
    					set `omegaInitiated` = :omegaInitiated, `reservationReferenceOmega` = :reservationReferenceOmega, `statusOmega` = :statusOmega
    					where `id` = :id_order ",
    					array(
    						'omegaInitiated' => $values['omegaInitiated'],
            				'reservationReferenceOmega' => $values['reservationReferenceOmega'],
            				'statusOmega' => $values['statusOmega'],
            				'id_order' => $current_order['id']
    					)
    				);
    			
    				$db->query("
    					update `xp_orders_products`
    					set `price` = :price, `total` = :total, `reservationResultOmega` = :reservationResultOmega, `checkReservationResultOmega` = :checkReservationResultOmega
    					where `id_order` = :id_order ",
    					array(
    						'price' => $values_product['price'],
    						'total' => $values_product['total'],
            				'reservationResultOmega' => $values_product['reservationResultOmega'],
            				'checkReservationResultOmega' => $values_product['checkReservationResultOmega'],
            				'id_order' => $current_order['id']
    					)
    				);
    			}
    			
    			#if $continue is false means that Reservation() or CheckReservation() has not been successfully completed
    			#in this case we have an error on Reservation() or CheckReservation(), or connection with Omega has not been made in one of this two methods
    			if( ! $continue ){
    			
    				#set status of order with value 3, which means that it was a problem
    				$db->query("
    					update `xp_orders`
    					set `status` = 3
    					where `id` = ? ",
            			$current_order['id']
            		);
    				
    				#save in database the results return by Request on omega: Reservation() and CheckReservation()
    				$db->query("
						update `xp_orders_products`
						set `reservationResultOmega` = :reservationResultOmega, `checkReservationResultOmega` = :checkReservationResultOmega
						where `id_order` = :id_order ",
    					array(
    						'reservationResultOmega' => isset($result) ? serialize($result) : '',
    						'checkReservationResultOmega' => isset($checkReservationResultOmega) ? serialize($checkReservationResultOmega) : '',
    						'id_order' => $current_order['id']
    					)
    				);
    				
            		#if received an error on Reservation() method save it in database
    			    if( isset($result->ReservationResponseReservationVersions->ErrorMessage) ){
    			            		 
    			    	$db->query("
    			        	update `xp_orders`
    			        	set `errorMessageReservationOmega` = :errorMessageReservationOmega
	            			where `id` = :id_order ",
							array(
            					'errorMessageReservationOmega' => $result->ReservationResponseReservationVersions->ErrorMessage,
    			            	'id_order' => $current_order['id']
    			            )
    			        );
    			    }
    			
    			    #if received an error on CheckReservation() method save it in database
    			    if( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
    			            		 
    			    	$db->query("
    			        	update `xp_orders`
    			            set `errorMessageCheckReservationOmega` = :errorMessageCheckReservationOmega
	            			where `id` = :id_order ",
            				array(
            					'errorMessageCheckReservationOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
            					'id_order' => $current_order['id']
    			            )
    			        );
    			    }
    			}
	    	}
	    }
    }
}

if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {
    if (isset($_POST['env_key']) && isset($_POST['data'])) {
        // calea catre cheia privata
        // cheia privata este generata de mobilpay, accesibil in Admin -> Conturi
        // de comerciant -> Detalii -> Setari securitate


        $privateKeyFilePath = i . 'Mobilpay/private.key';

        try {
            $objPmReq = Mobilpay_Payment_Request_Abstract::factoryFromEncrypted($_POST['env_key'], $_POST['data'], $privateKeyFilePath);

            switch ($objPmReq->objPmNotify->action) {
                // price action este insotit de un cod de eroare si de un mesaj
                // de eroare. Acestea pot fi citite folosind $cod_eroare =
                // $objPmReq->objPmNotify->errorCode; respectiv $mesaj_eroare =
                // $objPmReq->objPmNotify->errorMessage;
                // pentru a identifica ID-ul comenzii pentru care primim
                // rezultatul platii folosim $id_comanda = $objPmReq->orderId;
                case 'confirmed':
                    // cand action este confirmed avem certitudinea ca banii au
                    // plecat din contul posesorului de card si facem update al
                    // starii comenzii si livrarea produsului
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('confirmed');
                    break;
                case 'confirmed_pending':
                    // cand action este confirmed_pending inseamna ca tranzactia
                    // este in curs de verificare antifrauda. Nu facem
                    // livrare/expediere. In urma trecerii de aceasta verificare
                    // se va primi o noua notificare pentru o actiune de
                    // confirmare sau anulare.
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('confirmed_pending');
                    break;
                case 'paid_pending':
                    // cand action este paid_pending inseamna ca tranzactia este
                    // in curs de verificare. Nu facem livrare/expediere. In
                    // urma trecerii de aceasta verificare se va primi o noua
                    // notificare pentru o actiune de confirmare sau anulare.
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('paid_pending');
                    break;
                case 'paid':
                    // cand action este paid inseamna ca tranzactia este in curs
                    // de procesare. Nu facem livrare/expediere. In urma
                    // trecerii de aceasta procesare se va primi o noua
                    // notificare pentru o actiune de confirmare sau anulare.
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('paid');
                    break;
                case 'canceled':
                    // cand action este canceled inseamna ca tranzactia este
                    // anulata. Nu facem livrare/expediere.
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('canceled');
                    break;
                case 'credit':
                    // cand action este credit inseamna ca banii sunt returnati
                    // posesorului de card. Daca s-a facut deja livrare, aceasta
                    // trebuie oprita sau facut un reverse.
                    $errorMessage = $objPmReq->objPmNotify->getCrc();
                    update_order('credit');
                    break;
                default:
                    $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                    $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_ACTION;
                    $errorMessage = 'mobilpay_refference_action paramaters is invalid';
                    break;
            }
        } catch (Exception $e) {
            $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_TEMPORARY;
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
        }
    }

    else {
        $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
        $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
        $errorMessage = 'mobilpay.ro posted invalid parameters';
    }
}

else {
    $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
    $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_METHOD;
    $errorMessage = 'invalid request metod for payment confirmation';
}

header('Content-type: application/xml', true);
echo '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL;

if ($errorCode == 0) {
    $content = '<crc>' . $errorMessage . '</crc>';
}

else {
    $content = '<crc error_type="' . $errorType . '" error_code="' . $errorCode . '">' . $errorMessage . '</crc>';
}

exit($content);