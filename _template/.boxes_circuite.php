<?php $resources = array(
    'transport',
    'countries',
    'duration',
    'rating',
    'boards'
);

foreach ($resources as $resource) {
    $$resource = $this->get('filter_' . $resource);
}

$criteria = $this->get('criteria');?>

<?php // if ($this->get('list_filters')) {?>
<div class="main-filters <?php echo $this->get('class')?>">
	<strong>Cauta <?php echo mb_strtolower($this->get('page_name'))?></strong>
	<div id="search-filters">
		<form action="<?php echo $this->get('box_link') ?>" method="get">
		    <?php
		    if($sorts = $this->get('filter_sorts')){
                foreach( $sorts as $get_parameter => $value ){
            ?>
            <input type="hidden" name="<?php echo $get_parameter?>" value="<?php echo $value?>" />
            <?php
                }
            }   
		    ?>
			<ul class="nav main-filters-tree">
			    <?php if ($transport) {?>
				<!--<li>
					<p class="custom-label">Transport</p>
					<div class="custom-select country-select">
						<select class="value chain" name="filter-transport" id="filter-transport">
						    <?php foreach ($transport as $c) {?>
						 	<option value="<?php echo $c['id'] ?>" <?php echo (isset($_GET['filter-transport']) && $c['id'] == $_GET['filter-transport'] ? 'selected="selected"' : false) ?>><?php echo $c['name'] ?></option>
						 	<?php }?>
						</select>
					</div>
				</li>-->
				<?php }?>

			    <?php
			    if ($duration) {?>
				<li>
					<ul class="nav cf shorts dates">
						<li>
							<p class="custom-label"><label for="filter-departure"><?php echo tr('Data plecare intre:')?></label></p>
							<input type="text" value="<?php echo (isset($_GET['filter-departure']) ? $_GET['filter-departure'] : date('d/m/Y'))?>" name="filter-departure" id="filter-departure" autocomplete="off" class="datepick double-row-datepick"/>
							<span class="sprite date-icon"></span>
						</li>
						<li>
							<p class="custom-label"><label for="filter-departure2"></label></p>
							<input type="text" value="<?php echo (isset($_GET['filter-departure2']) ? $_GET['filter-departure2'] : date('d/m/Y', (time() + 604800)))?>" name="filter-departure2" id="filter-departure2" autocomplete="off" class="datepick double-row-datepick"/>
							<span class="sprite date-icon"></span>
						</li>
					</ul>
				</li>
				<li>
					<p class="custom-label">Durata/zile</p>
					<div class="custom-select country-select">
						<select class="value" name="filter-duration" id="filter-duration">
						    <option value="0">Selecteaza</option>
						    <?php for ($i = $duration['min']; $i <= $duration['max']; $i++) {?>
						 	<option value="<?php echo $i ?>" <?php echo (isset($_GET['filter-duration']) && $i == $_GET['filter-duration'] ? 'selected="selected"' : false) ?>><?php echo $i ?></option>
						 	<?php }?>
						</select>
					</div>
				</li>
				<?php }?>

				<?php if ($countries) {?>
				<li>
					<p class="custom-label">Tara destinatie</p>
					<div class="custom-select country-select">
						<select class="value chain" name="filter-country" id="filter-country" data-initializer="true" data-schain="<?php echo (isset($_GET['filter-city']) ? $_GET['filter-city'] : false) ?>">
						    <option value="0">Selecteaza</option>
						    <?php foreach ($countries as $c) {?>
						 	<option value="<?php echo $c['id'] ?>" <?php echo (isset($_GET['filter-country']) && $c['id'] == $_GET['filter-country'] ? 'selected="selected"' : false) ?>><?php echo $c['name'] ?></option>
						 	<?php }?>
						</select>
					</div>
				</li>
				<?php }?>

				<li>
					<p class="custom-label">Zona/localitate</p>
					<div class="custom-select country-select">
						<select class="value chain" name="filter-city" id="filter-city" data-chain="filter-country" data-module="fibula-city" data-schain="<?php echo (isset($_GET['filter-hotel']) ? $_GET['filter-hotel'] : false) ?>"></select>
					</div>
				</li>
				<!--<li>
					<p class="custom-label">Hotel</p>
					<div class="custom-select country-select">
						<select class="value chain" name="filter-hotel" id="filter-hotel" data-chain="filter-city" data-module="fibula-hotel"></select>
					</div>
				</li>-->
				<?php
				if($route = $this->get('route')){
				    $filters_route_countries = $this->get('filters_route_countries');
				    $filters_route_cities = $this->get('filters_route_cities');
				?>
				<li>
				    <p class="custom-label">Itinerariu</p>
				    <div class="check-select">
				        <div class="check-select-value"><p id="route-check-select">Toate variantele</p><span class="triangle"></span></div>
				        <div class="check-select-storage">
				            <ul data-name="route-check-select">
				                <li>
				                    <label for="cs-all">Toate variantele</label>
				                    <input class="select-all" id="cs-all" type="checkbox" name="filter-cs_all" value="0" />
				                </li>
				                <?php foreach($route as $c){ ?>
				                <li>
				                    <label for="cs-country-<?php echo $c['id_country']?>"><?php echo $c['country_name']?></label>
				                    <input id="cs-country-<?php echo $c['id_country']?>" type="checkbox" name="filter-route_countries[]" value="<?php echo $c['id_country']?>" <?php echo( in_array($c['id_country'], $filters_route_countries) ? 'checked="checked"' : '')?> />
				                </li>
				                    <?php
				                    if($c['cities']){
                                        foreach($c['cities'] as $ct){
                                    ?>
                                <li>
                                    <label class="side" for="cs-city-<?php echo $ct['id']?>"><?php echo $ct['name']?></label>
                                    <input id="cs-city-<?php echo $ct['id']?>" type="checkbox" name="filter-route_cities[]" value="<?php echo $ct['id']?>" <?php echo ( isset($filters_route_cities[$ct['id']]) ? 'checked="checked"' : '' )?> />
                                </li>
                                    <?php
                                        }
                                    }
                                    ?>
				                <?php } ?>
				            </ul>
				        </div>
				    </div>
				</li>
				<?php } ?>
				<li>
					<ul class="nav cf shorts person">
						<li>
							<p class="custom-label">Nr. adulti</p>
							<div class="custom-select country-select">
								<select class="value" name="filter-adults" id="filter-adults">
								    <option value="0">Selecteaza</value>
								 	<?php for ($i = 1; $i < 11; $i++) {?>
        						 	<option value="<?php echo $i ?>" <?php echo ((isset($_GET['filter-adults']) && $i == $_GET['filter-adults']) ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
        						 	<?php }?>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Nr. copii</p>
							<div class="custom-select country-select">
								<select class="value" name="filter-children" id="filter-children">
								 	<?php for ($i = 0; $i < 4; $i++) {?>
        						 	<option value="<?php echo $i ?>" <?php echo (isset($_GET['filter-children']) && $i == $_GET['filter-children'] ? 'selected="selected"' : false) ?>><?php echo $i ?></option>
        						 	<?php }?>
								</select>
							</div>
						</li>
						<?php if (isset($_GET['filter-children']) && is_numeric($_GET['filter-children'])) {
						    for ($c = 0; $c < $_GET['filter-children']; $c ++) {?>
                        <!--<li class="children-age">
                            <p class="custom-label">Varsta copil</p>
                            <div class="custom-select">
                                <select class="value" name="filter-children-ages[]">
                                    <?php for ($ca = 1; $ca < 17; $ca ++) {?>
                                    <option value="<?php echo $ca ?>" <?php echo (isset($_GET['filter-children-ages'][$i]) && $_GET['filter-children-ages'][$i] == $ca ? 'selected="selected"' : '') ?>><?php echo $ca ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </li>-->
                            <?php }
                        }?>
					</ul>
				</li>
			</ul>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Cauta"><span class="arrow edge"></span><span class="sprite search-icon-small"></span>Cauta</a>
			</div>
		</form>
	</div>
	<?php //}?>

	<?php if ($this->get('list_sort')) {?>
	<strong>Filtreaza <?php echo mb_strtolower($this->get('page_name'))?></strong>
	<div id="real-filters">
		<form action="" method="get">
			<ul class="nav main-filters-tree cf">
			    <?php if ($rating) {?>
				<li>
					<p class="radio-heading">Tip oferta</p>
					<ul class="nav checklists radio-nav">
					    <?php foreach ($rating as $p) {?>
						<li>
							<input class="n" type="radio" name="stars-<?php echo $p['rating'] ?>" value="<?php echo $p['rating'] ?>"/><span class="circle"></span>
							<label><span class="stars orange-stars-<?php echo $p['rating'] ?>"></span></label>
						</li>
						<?php }?>
					</ul>
				</li>
				<?php }?>

				<?php if ($boards) {?>
				<li>
					<p class="radio-heading">Masa</p>
					<ul class="nav radio-nav">
					    <?php foreach ($boards as $p) {?>
						<li><input class="n" type="radio" name="filter-board" value="<?php echo $p['id'] ?>"/><span class="circle"></span><label><span><?php echo $p['name'] ?></span></label></li>
						<?php }?>
					</ul>
				</li>
				<?php }?>
			</ul>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Filtreaza"><span class="arrow edge"></span><span class="sprite filter-icon"></span>Filtreaza</a>
			</div>
		</form>
	</div>
	<?php }?>
</div>
<div class="<?php echo $this->get('class')?>" id="main-filters">
    <?php if ($criteria) {?>
    <ul id="criteria-filters" class="filters cf">
        <?php foreach ($criteria as $c) {
            if (count($c['options']) > 0) {?>
        <li>
            <strong class="heading"><?php echo $c['name']?></strong>
            <ul class="options">
                <?php foreach ($c['options'] as $o) {?>
			    <li <?php echo ($c['checked'] ? 'class="active"' : '') ?>>
                    <input type="checkbox" name="filters[]" id="option-<?php echo $o['id_option'] ?>" value="<?php echo $o['id_option'] ?>" />
            		<div class="filter-checkbox<?php echo $o['class'] ?>"><div class="sprite"></div></div>
            		<label for="option-<?php echo $o['id_option'] ?>">
                    <a title="<?php echo tr('Filtreaza dupa') . ': ' . $c['name'] . ', ' . $o['name'] ?>" href="<?php echo $o['url'] ?>"><?php echo $o['name'] ?></a>
            	    </label>
                </li>
				<?php }?>
            </ul>
        </li>
            <?php }
        }?>
    </ul>
    <?php }?>
</div>
<?php
if(
	$partners = $this->get('partners')
){
?>
<section id="partners" class="box">
	<ul class="cf">
		<?php
		foreach(
			$partners as
			$k => $pr
		){
		?>
		<li>
			<a class="external image" href="<?php echo $pr['link']?>" title="<?php echo $pr['name']?>"><img src="<?php echo $pr['image']?>" alt="<?php echo $pr['name']?>" /></a>
		</li>
		<?php
		}
		?>
	</ul>
</section>
<?php
}
?>
<section id="secured-payment" class="box">
	<p>Tranzactii securizate prin 3D Secure</p>
	<span class="card-pay"></span>
	<span class="mobilpay"></span>
</section>