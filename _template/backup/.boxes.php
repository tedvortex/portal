<?php
$categories = $this->get('main_categories');
?>

<div>
	<?php if ($this->get('constructor') != 'products') {?>
	<section id="reservation-box" class="box main-tabs-container">
		<ul class="nav cf tabs-selection" id="reservation-selection">
			<li class="tab-ref">
				<span class="sprite sejururi-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('sejururi')) ?></span></strong>
			</li>
			<li class="selected tab-ref">
				<span class="sprite cazari-icon-hover"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('cazari')) ?></span></strong>
			</li>
			<li class="tab-ref">
				<span class="sprite zboruri-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('zboruri')) ?></span></strong>
			</li>
			<li class="tab-ref">
				<span class="sprite croaziere-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('croaziere')) ?></span></strong>
			</li>
			<li class="tab-ref">
				<span class="sprite circuite-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('circuite')) ?></span></strong>
			</li>
		</ul>
		<div id="reservation-tabs" class="tabs-container">
			<div class="tab tab-1 n" id="tab-1">
				<form action="<?php echo $categories[2]['link'] ?>" method="get">
				    <div>
    					<ul class="cf select-list nav">
    						<li>
    							<p class="custom-label">Tara</p>
    							<div class="custom-select country-select">
    							    <?php if ($this->get('countries')) {?>
    								<select class="value chain" name="country" id="sejur-country" data-initializer="true">
    								    <?php foreach ($this->get('countries') as $c) {?>
    									<option value="<?php echo $c['id'] ?>" <?php echo ($c['name_seo'] == 'romania' ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
    									<?php }?>
    								</select>
    								<?php }?>
    								<!--<div class="value">
    									<input type="hidden" value="0" name="country" id="tab-1-country" />
    									<p>Romania</p>
    									<span class="arrow default"></span>
    								</div>
    								<div class="select-container" style="height: 0px;">
    									<ul class="cf">
    										<li data-name="Romania" data-value="0">Romania</li>
    										<li data-name="Bulgaria" data-value="1">Bulgaria</li>
    									</ul>
    								</div>-->
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Oras</p>
    							<div class="custom-select city-select">
    							    <select class="value chain" name="city" id="sejur-city" data-chain="sejur-country" data-module="city"></select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Hotel</p>
    							<div class="custom-select hotel-select">
    							    <select class="value chain" name="hotel" id="sejur-hotel" data-chain="sejur-city" data-module="hotel"></select>
    							</div>
    						</li>
    						<!--<li>
    							<p class="custom-label">Categorie</p>
    							<div class="custom-select stars-select">
    								<select class="value" name="rating" id="sejur-category" data-chain="sejur-rating" data-module="rating"></select>
    								<div class="value">
    									<input type="hidden" value="0" name="hotel" id="tab-1-stars" />
    									<p>4 stele</p>
    									<span class="arrow default"></span>
    								</div>
    								<div class="select-container" style="height: 0px;">
    									<ul class="cf">
    										<li data-name="4 stele" data-value="0">4 stele</li>
    										<li data-name="5 stele" data-value="1">5 stele</li>
    									</ul>
    								</div>
    							</div>
    						</li>-->
    					</ul>
    					<ul class="cf nav date-select">
    						<li>
    							<p class="custom-label"><label for="date-check-in"><?php echo tr('De la')?></label></p>
    							<input type="text" value="<?php echo date('d/m/Y', time()) ?>" name="date-check-in" id="date-check-in" autocomplete="off" class="datepick"/>
    						</li>
    						<li>
    							<p class="custom-label"><label for="date-check-out"><?php echo tr('Pana la')?></label></p>
    							<input type="text" value="<?php echo date('d/m/Y', time() + 86400) ?>" name="date-check-out" id="date-check-out" autocomplete="off" class="datepick"/>
    						</li>
    					</ul>
    					<div class="buttons">
    					    <button type="submit" class="n"></button>
    						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>Cauta</a>
    					</div>
					</div>
				</form>
			</div>
			<div class="tab tab-2" id="tab-2">
				<form action="<?php echo $categories[2]['link'] ?>" method="get">
				    <div>
		    			<ul class="nav select-list cf">
		    				<li>
		    					<p class="custom-label">Cauta dupa Oras</p>
		    					<div class="ai-input">
		    						<input type="hidden" name="ai_city" value="0" />
		    						<input type="hidden" name="ai_country" value="0" />
		    						<input type="text" name="location" value="" autocomplete="off" data-chain="housing-country" />
		    						<div><ul class="ai-list"></ul></div>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Tara destinatie</p>
		    					<div class="custom-select country-select">
		    						<?php if ($this->get('countries')) {?>
		    						<select class="value chain" name="country" id="housing-country" <?php echo (! isset($query) ? 'data-initializer="true"' : '') ?>>
		    						    <?php foreach ($this->get('countries') as $c) {?>
		    							<option value="<?php echo $c['id'] ?>" <?php echo ($c['id'] == (isset($query['id_country']) && $query['id_country'] == $c['id'] ? $c['id'] : 1506) ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    							<?php }?>
		    						</select>
		    						<?php }?>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Zona/localitate</p>
		    					<div class="custom-select country-select">
		    						<select class="value chain" name="city" id="housing-city" data-chain="housing-country" data-module="city">
		    						    <?php foreach ($this->get('cities') as $c) {?>
		    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_city']) && $query['id_city'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    						    <?php }?>
		    						</select>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Hotel</p>
		    					<div class="custom-select country-select">
		    						<select class="value chain" name="hotel" id="housing-hotel" data-chain="housing-city" data-module="hotel">
		    						    <?php foreach ($this->get('hotels') as $c) {?>
		    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_hotel']) && $query['id_hotel'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    						    <?php }?>
		    						</select>
		    					</div>
		    				</li>
		    			</ul>
    					<ul class="nav select-list cf">
    						<li>
    							<p class="custom-label"><label for="date"><?php echo tr('Data plecare')?></label></p>
    							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date" id="housing-date" autocomplete="off" class="custom-date datepick"/>
    							<span class="sprite date-icon"></span>
    						</li>
    						<li>
    							<p class="custom-label">Durata/nopti</p>
    							<div class="custom-select country-select">
    								<select class="value" name="nights" id="housing-nights">
    								    <?php for ($i = 1; $i <= 30; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['nights']) && $query['nights'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    					</ul>
    					<ul class="cf select-list small-select nav">
    					    <li>
    							<p class="custom-label">Nr. camere</p>
    							<div class="custom-select country-select">
    								<select class="value3" name="rooms" id="housing-rooms">
    								 	<?php for ($i = 1; $i <= 10; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['rooms']) && $query['rooms'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Nr. adulti</p>
    							<div class="custom-select country-select">
    								<select class="value3" name="adults" id="housing-adults">
    								 	<?php for ($i = 1; $i <= 10; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['adults']) && $query['adults'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?> AD</option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Nr. copii</p>
    							<div class="custom-select country-select">
    								<select class="value3" name="children" id="housing-children">
    								 	<?php for ($i = 0; $i <= 10; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['children']) && $query['children'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?> CH</option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    						<?php for ($j = 0; $j <= 10; $j++) {?>
    						<li class="children-age<?php echo ((isset($query['children']) && $j >= $query['children']) || ! isset($query['children']) ? ' n' : '') ?>" id="children-<?php echo $j ?>">
    							<p class="custom-label">Varsta copil</p>
    							<div class="custom-select country-select">
    								<select class="value3" name="children-ages[]">
    								    <?php for ($i = 1; $i <= 16; $i++) {?>
    								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    						<?php }?>
    					</ul>
					</div>
					<div class="buttons">
						<button type="submit"></button>
						<a class="button orange form-submitter filter" href="#" title="Cauta"><span class="sprite orange-edge"></span><span class="sprite search-icon-small"></span>Cauta</a>
					</div>
				</form>
			</div>
			<div class="tab tab-3 n" id="tab-3">
				<form action="" method="POST">
					<ul class="cf select-list nav">
						<li>
							<p class="custom-label">Plecare din</p>
							<input type="text" name="flight_from" value=""/>
						</li>
						<li>
							<p class="custom-label">Sosire in</p>
							<input type="text" name="flight_from" value=""/>
						</li>
					</ul>
					<ul class="cf nav date-select">
						<li>
							<p class="custom-label"><label for="date-check-in"><?php echo tr('De la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date-check-in" id="date-check-in" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"><label for="date-check-out"><?php echo tr('Pana la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() + 86400 )?>" name="date-check-out" id="date-check-out" autocomplete="off" class="datepick"/>
						</li>
					</ul>
					<ul class="cf select-list small-select nav">
						<li>
							<p class="custom-label">Adulti 12ani</p>
							<div class="custom-select country-select">
								<select class="value2">
									<option value="0">1</option>
									<option value="1">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="adult" />
									<p>1</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<?php for ($i=1;$i<=9;$i++) { ?>
											<li data-name="<?php echo $i?>" data-value="<?php echo $i?>"><?php echo $i?></li>
										<?php } ?>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Copii 12ani</p>
							<div class="custom-select city-select">
								<select class="value2">
									<option value="0">1</option>
									<option value="1">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="children" />
									<p>1</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<?php for ($i=1;$i<=9;$i++) { ?>
											<li data-name="<?php echo $i?>" data-value="<?php echo $i?>"><?php echo $i?></li>
										<?php } ?>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Bebelusi 2ani</p>
							<div class="custom-select hotel-select">
								<select class="value2">
									<option value="0">1</option>
									<option value="1">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="bebe" />
									<p>1</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<?php for ($i=1;$i<=9;$i++) { ?>
											<li data-name="<?php echo $i?>" data-value="<?php echo $i?>"><?php echo $i?></li>
										<?php } ?>
									</ul>
								</div>-->
							</div>
						</li>
					</ul>
					<div class="buttons">
						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>cauta</a>
					</div>
				</form>
			</div>
			<div class="tab tab-4 n" id="tab-4">
				<form action="" method="POST">
					<ul class="cf select-list nav">
						<li>
							<p class="custom-label">Tara</p>
							<div class="custom-select country-select">
								<select class="value">
									<option value="0">Romania</option>
									<option value="1">Bulgaria</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="country" id="tab-4-country" />
									<p>Romania</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Romania" data-value="0">Romania</li>
										<li data-name="Bulgaria" data-value="1">Bulgaria</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Oras</p>
							<div class="custom-select city-select">
								<select class="value">
									<option value="0">Bucuresti</option>
									<option value="1">Sinaia</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="city" id="tab-4-city" />
									<p>Bucuresti</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Bucuresti" data-value="0">Bucuresti</li>
										<li data-name="Sinaia" data-value="1">Sinaia</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Hotel</p>
							<div class="custom-select hotel-select">
								<select class="value">
									<option value="0">Ritz</option>
									<option value="1">Belvedere</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="hotel" id="tab-4-hotel" />
									<p>Ritz</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Ritz" data-value="0">Ritz</li>
										<li data-name="Belvedere" data-value="1">Belvedere</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Categorie</p>
							<div class="custom-select stars-select">
								<select class="value">
									<option value="0">4 stele</option>
									<option value="1">5 stele</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="hotel" id="tab-4-stars" />
									<p>4 stele</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="4 stele" data-value="0">4 stele</li>
										<li data-name="5 stele" data-value="1">5 stele</li>
									</ul>
								</div>-->
							</div>
						</li>
					</ul>
					<ul class="cf nav date-select">
						<li>
							<p class="custom-label"><label for="date-check-in"><?php echo tr('De la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date-check-in" id="date-check-in" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"><label for="date-check-out"><?php echo tr('Pana la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() + 86400 )?>" name="date-check-out" id="date-check-out" autocomplete="off" class="datepick" />
						</li>
					</ul>
					<div class="buttons">
						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>cauta</a>
					</div>
				</form>
			</div>
			<div class="tab tab-5 n" id="tab-5">
				<form action="" method="POST">
					<ul class="cf select-list nav">
						<li>
							<p class="custom-label">Tara</p>
							<div class="custom-select country-select">
								<select class="value">
									<option value="0">Romania</option>
									<option value="1">Bulgaria</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="country" id="tab-5-country" />
									<p>Romania</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Romania" data-value="0">Romania</li>
										<li data-name="Bulgaria" data-value="1">Bulgaria</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Oras</p>
							<div class="custom-select city-select">
								<select class="value">
									<option value="0">Bucuresti</option>
									<option value="1">Sinaia</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="city" id="tab-5-city" />
									<p>Bucuresti</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Bucuresti" data-value="0">Bucuresti</li>
										<li data-name="Sinaia" data-value="1">Sinaia</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Hotel</p>
							<div class="custom-select hotel-select">
								<select class="value">
									<option value="0">Ritz</option>
									<option value="1">Belvedere</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="hotel" id="tab-5-hotel" />
									<p>Ritz</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Ritz" data-value="0">Ritz</li>
										<li data-name="Belvedere" data-value="1">Belvedere</li>
									</ul>
								</div>-->
							</div>
						</li>
						<li>
							<p class="custom-label">Categorie</p>
							<div class="custom-select stars-select">
								<select class="value">
									<option value="0">4 stele</option>
									<option value="1">5 stele</option>
								</select>
								<!--<div class="value">
									<input type="hidden" value="0" name="hotel" id="tab-5-stars" />
									<p>4 stele</p>
									<span class="arrow default"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="4 stele" data-value="0">4 stele</li>
										<li data-name="5 stele" data-value="1">5 stele</li>
									</ul>
								</div>-->
							</div>
						</li>
					</ul>
					<ul class="cf nav date-select">
						<li>
							<p class="custom-label"><label for="date-check-in"><?php echo tr('De la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date-check-in" id="date-check-in" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"><label for="date-check-out"><?php echo tr('Pana la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() + 86400 )?>" name="date-check-out" id="date-check-out" autocomplete="off" class="datepick"/>
						</li>
					</ul>
					<div class="buttons">
						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>cauta</a>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php } ?>
    <section id="offers-box" class="box">
    	<ul class="nav">
    		<li class="orange-background"><a href="#" title="<?php echo tr('Oferte speciale') ?>"><?php echo tr('Oferte speciale') ?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="lightgreen-background"><a href="#" title="<?php echo tr('Oferte sezon') ?>"><?php echo tr('Oferte sezon') ?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="violet-background"><a href="#" title="<?php echo tr('Oferte tematice') ?>"><?php echo tr('Oferte tematice') ?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="blue-background"><a href="#" title="<?php echo tr('Oferte personalizate') ?>"><?php echo tr('Oferte personalizate') ?><span class="sprite oferte-icon-hover-box"></span></a></li>
    	</ul>
    </section>
    <?php if($this->get('constructor')!='products'){?>
    <section id="contact-box" class="box">
        <p><?php echo tr('Ne puteti contacta la numarul de telefon') ?></p>
        <p class="phone">+40 21 317 2606</p>
        <p class="phone">+40 21 317 2608</p>
        <p class="phone">+40 21 318 8871</p>
    </section>
    <section id="newsletter-box" class="box">
    	<p>Newsletter</p>
    	<form action="" method="POST">
    		<div class="form-container">
    			<input type="hidden" value="newsletter" name="action" />
    			<input type="text" name="newsletter-email" value="" />
    			<div class="buttons">
    				<button type="submit"></button>
    				<a href="#" class="button form-submitter blue" title="trimite"><span class="edge arrow"></span>Trimite</a>
    			</div>
    		</div>
    	</form>
    </section>
   	<?php
	   	if($slides = $this->get('slides'))
	   	{
	   		if( $slides['first'] || $slides['second'] )
	   		{
	   			foreach( $slides as $k => $slide )
	   			{
	   				if( $slide )
	   				{
   	?>
   	<section class="boxes-slide">
	   	<ul class="nextgen-slider nav" data-navigator="boxes-<?php echo $k?>-slide-navigator" data-speed="1000" data-timeout="4500">
			<?php foreach( $slide as $sl ){ ?>
			<li>
				<a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>">
					<img src="<?php echo $sl['image']?>" alt="<?php echo $sl['name']?>" />
				</a>
			</li>
			<?php } ?>
	   	</ul>
	   	<ul id="boxes-<?php echo $k?>-slide-navigator" class="boxes-slider-navigator cf"></ul>
   	</section>
   	<?php
	   				}
	   			}
	   		}
   		}
   	}
   	?>
</div>