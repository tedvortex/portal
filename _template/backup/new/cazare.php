<?php $this->extend('layout') ?>
<section id="main-products-container" class="<?php echo $class?>">
	<ul id="main-products" class="nav">
		<li>
			<div class="offer-container cf">
				<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2>Rixos Premium</h2>
						<p>Kemer Turcia</p>
					</div>
					<div class="right-offer-center">
						<ul class="stars">
							<?php
							for (
								$i=0;
								$i<5;
								$i++
							){
							?>
							<li class="sprite"></li>
							<?php
							}
							?>
						</ul>
						<a class="map-tag" href="#" title="#"><span class="sprite map-icon"></span>Harta</a>
					</div>
					<div class="right-offer-bottom">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
					</div>
				</div>
			</div>
			<ul class="products-table">
				<?php
				for (
					$i=0;
					$i<4;
					$i++
				){
				?>
				<li>
					<ul class="products-table-list cf">
						<li class="col-1">
							<p>lorem ipsum dolor<br /> lorem ipsum dolor sith</p>
						</li>
						<li class="col-2"><p>Full Breakfast </p></li>
						<li class="col-3"><p>2AD + 2 CHD (5, 11) <br />2AD + 2 CHD (5, 11)</p></li>
						<li class="col-4"><p>Confirmare imediata</p></li>
						<li class="special-col"><p>1234 Eur</p></li>
						<li class="col-5"><a class="button checked" href="#" title="#"><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a></li>
					</ul>
					<div class="conditions">
						<p>Conditii de anulare <span class="redspan">Nerambursabil</span> <span class="greenspan">Anulare gratuita pana la 27.07.2013 ora 00:00</span></p>
					</div>
				</li>
				<?php
				}
				?>
			</ul>
		</li>
	</ul>
</section>
