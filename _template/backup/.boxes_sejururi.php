<div class="<?php echo $this->get('class')?>" id="main-filters">
	<strong>Filtreaza <?php echo mb_strtolower($this->get('page_name'))?></strong>
	<div id="search-filters">
		<form action="" method="GET">
			<ul class="nav main-filters-tree">
				<li>
					<p class="radio-heading">Transport</p>
					<ul class="nav radio-nav">
						<li><input class="n" type="radio" name="transportation" value="0" checked="checked"/><span class="circle"><span class="selected"></span></span><span>Cu avionul</span></li>
						<li><input class="n" type="radio" name="transportation" value="1"/><span class="circle"></span><span>Cu autocarul</span></li>
						<li><input class="n" type="radio" name="transportation" value="2"/><span class="circle"></span><span>Cu trasnport individual</span></li>
					</ul>
				</li>
				<li>
					<p class="custom-label">Tara destinatie</p>
					<div class="custom-select country-select">
						<div class="value">
							<input type="hidden" value="0" name="country" />
							<p>Romania</p>
							<span class="arrow orange"></span>
						</div>
						<div class="select-container" style="height: 0px;">
							<ul class="cf">
								<li data-name="Romania" data-value="0">Romania</li>
								<li data-name="Bulgaria" data-value="1">Bulgaria</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<p class="custom-label">Zona/localitate</p>
					<div class="custom-select country-select">
						<div class="value">
							<input type="hidden" value="0" name="country" />
							<p>Kemer</p>
							<span class="arrow orange"></span>
						</div>
						<div class="select-container" style="height: 0px;">
							<ul class="cf">
								<li data-name="Romania" data-value="0">Istanbul</li>
								<li data-name="Bulgaria" data-value="1">Bosfor</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<ul class="nav cf shorts dates">
						<li>
							<p class="custom-label"><label for="date-departure"><?php echo tr('Data plecare')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date-departure" id="date-departure" autocomplete="off" class="datepick"/>
							<span class="sprite date-icon"></span>
						</li>
						<li>
							<p class="custom-label">Durata/nopti</p>
							<div class="custom-select country-select">
								<div class="value">
									<input type="hidden" value="0" name="country" />
									<p>Kemer</p>
									<span class="arrow orange"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="Istanbul" data-value="0">Istanbul</li>
										<li data-name="Bosfor" data-value="1">Bosfor</li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</li>
				<li>
					<p class="custom-label">Hotel</p>
					<div class="custom-select country-select">
						<div class="value">
							<input type="hidden" value="0" name="hotel" />
							<p>Rixos premium</p>
							<span class="arrow orange"></span>
						</div>
						<div class="select-container" style="height: 0px;">
							<ul class="cf">
								<li data-name="Ritz" data-value="0">Ritz</li>
								<li data-name="Intercontinental" data-value="1">Intercontinental</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<ul class="nav cf shorts person">
						<li>
							<p class="custom-label">Nr. adulti</p>
							<div class="custom-select country-select">
								<div class="value">
									<input type="hidden" value="0" name="adults" />
									<p>2 AD</p>
									<span class="arrow orange"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="3 AD" data-value="0">3 AD</li>
										<li data-name="4 AD" data-value="1">4 AD</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<p class="custom-label">Nr. copii</p>
							<div class="custom-select country-select">
								<div class="value">
									<input type="hidden" value="0" name="country" />
									<p>2 CH</p>
									<span class="arrow orange"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="3 CH" data-value="0">3 CH</li>
										<li data-name="4 CH" data-value="1">4 CH</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<p class="custom-label">Varsta copii</p>
							<div class="custom-select country-select">
								<div class="value">
									<input type="hidden" value="0" name="country" />
									<p>5</p>
									<span class="arrow orange"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="6" data-value="6">6</li>
										<li data-name="7" data-value="7">7</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<p class="custom-label">&nbsp;</p>
							<div class="custom-select country-select">
								<div class="value">
									<input type="hidden" value="0" name="age-max" />
									<p>11</p>
									<span class="arrow orange"></span>
								</div>
								<div class="select-container" style="height: 0px;">
									<ul class="cf">
										<li data-name="12" data-value="0">12</li>
										<li data-name="13" data-value="1">13</li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Cauta"><span class="arrow edge"></span><span class="sprite search-icon-small"></span>Cauta</a>
			</div>
		</form>
	</div>
	<div id="real-filters">
		<form action="" method="POST">
			<ul class="nav main-filters-tree cf">
				<li>
					<p class="radio-heading">Categorie hotel</p>
					<ul class="nav radio-nav">
						<li><input class="n" type="radio" name="offer_type" value="0" checked="checked"/><span class="circle"><span class="selected"></span></span><span>Early Booking</span></li>
						<li><input class="n" type="radio" name="offer_type" value="1"/><span class="circle"></span><span>Special Offer</span></li>
					</ul>
				</li>
				<li class="empty"></li>
				<li>
					<p class="radio-heading">Tip oferta</p>
					<ul class="nav radio-nav">
						<li>
							<input class="n" type="radio" name="stars" value="1" checked="checked"/>
							<span class="circle"><span class="selected"></span></span>
							<ul class="stars nav">
								<li class="sprite"></li>
							</ul>
						</li>
						<li>
							<input class="n" type="radio" name="stars" value="2"/>
							<span class="circle"></span>
							<ul class="stars nav">
								<li class="sprite"></li>
								<li class="sprite"></li>
							</ul>
						</li>
						<li>
							<input class="n" type="radio" name="stars" value="3"/>
							<span class="circle"></span>
							<ul class="stars nav">
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
							</ul>
						</li>
						<li>
							<input class="n" type="radio" name="stars" value="4"/>
							<span class="circle"></span>
							<ul class="stars nav">
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
							</ul>
						</li>
						<li>
							<input class="n" type="radio" name="stars" value="5"/>
							<span class="circle"></span>
							<ul class="stars nav">
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
								<li class="sprite"></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<p class="radio-heading">Masa</p>
					<ul class="nav radio-nav">
						<li><input class="n" type="radio" name="meal" value="0"/><span class="circle"><span class="selected"></span></span><span>SC Room Only</span></li>
						<li><input class="n" type="radio" name="meal" value="1"/><span class="circle"></span><span>BB Bad and Breakfast</span></li>
						<li><input class="n" type="radio" name="meal" value="0"/><span class="circle"></span><span>HB Half Board</span></li>
						<li><input class="n" type="radio" name="meal" value="1"/><span class="circle"></span><span>AI All Inclusive</span></li>
						<li><input class="n" type="radio" name="meal" value="0"/><span class="circle"></span><span>UAI Ultra All Inclusive</span></li>
					</ul>
				</li>
			</ul>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Filtreaza"><span class="arrow edge"></span><span class="sprite filter-icon"></span>Filtreaza</a>
			</div>
		</form>
	</div>
</div>