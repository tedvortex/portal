<?php
$query = $this->get('query');
$categories = $this->get('main_categories');

if (isset($query['checkin'])) {
    $query['checkin'] = DateTime::createFromFormat('Y-m-d', $query['checkin']);
    $query['people'] = unserialize($query['people']);
} ?>
<div class="<?php echo $this->get('class')?>" id="main-filters">
	<strong>Filtreaza <?php echo mb_strtolower($this->get('page_name'))?></strong>
	<div id="search-filters">
		<form action="<?php echo $categories[2]['link'] ?>" method="get">
		    <div>
    			<ul class="nav main-filters-tree">
    				<li>
    					<p class="custom-label">Cauta dupa Oras</p>
    					<div class="ai-input">
    						<input type="hidden" name="ai_city" value="0" />
    						<input type="hidden" name="ai_country" value="0" />
    						<input type="text" name="location" value="" autocomplete="off" data-chain="country" />
    						<div><ul class="ai-list"></ul></div>
    					</div>
    				</li>
    				<li>
    					<p class="custom-label">Tara destinatie</p>
    					<div class="custom-select country-select">
    						<?php if ($this->get('countries')) {?>
    						<select class="value chain" name="country" id="country" <?php echo (! $query ? 'data-initializer="true"' : '') ?>>
    						    <?php foreach ($this->get('countries') as $c) {?>
    							<option value="<?php echo $c['id'] ?>" <?php echo ($c['id'] == (isset($query['id_country']) ? $query['id_country'] : 1522) ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
    							<?php }?>
    						</select>
    						<?php }?>
    					</div>
    				</li>
    				<li>
    					<p class="custom-label">Zona/localitate</p>
    					<div class="custom-select country-select">
    						<select class="value chain" name="city" id="city" data-chain="country" data-module="city">
    						    <?php foreach ((array)$this->get('cities') as $c) {?>
    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_city']) && $query['id_city'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
    						    <?php }?>
    						</select>
    					</div>
    				</li>
    				<li>
    					<p class="custom-label">Hotel</p>
    					<div class="custom-select country-select">
    						<select class="value chain" name="hotel" id="hotel" data-chain="city" data-module="hotel">
    						    <?php foreach ((array)$this->get('hotels') as $c) {?>
    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_hotel']) && $query['id_hotel'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
    						    <?php }?>
    						</select>
    					</div>
    				</li>
    				<li>
    					<ul class="nav cf shorts dates">
    						<li>
    							<p class="custom-label"><label for="date"><?php echo tr('Data plecare')?></label></p>
    							<input type="text" value="<?php echo (isset($query['checkin']) ? $query['checkin']->format('d/m/Y') : date('d/m/Y', time()+86400)) ?>" name="date" id="date" autocomplete="off" class="datepick-cazari"/>
    							<span class="sprite date-icon"></span>
    						</li>
    						<li>
    							<p class="custom-label">Durata/nopti</p>
    							<div class="custom-select country-select">
    								<select class="value" name="nights" id="nights">
    								    <?php for ($i = 1; $i <= 30; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['nights']) && $query['nights'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    					</ul>
    				</li>
    				<li id="cazari-rooms" class="rooms-container">
    					<div>
    						<p class="custom-label">Nr. camere</p>
							<div class="custom-select">
								<select class="value rooms-total" name="rooms" data-container="cazari-rooms">
								 	<?php for ($i = 1; $i < 4; $i++){ ?>
								 	<option value="<?php echo $i ?>" <?php echo (isset($query['rooms']) && $query['rooms'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
								 	<?php } ?>
								</select>
							</div>
						</div>
						<?php if (isset($query['people']) && is_array($query['people'])) {
						    for ($i = 0; $i < $query['rooms']; $i++) {?>
						<div class="room room-<?php echo $i ?>">
							<ul class="nav cf shorts person fix">
								<li class="room-order"><p class="custom-label">Camera <?php echo $i+1 ?>:</p></li>
								<li>
	    							<p class="custom-label">Nr. adulti</p>
	    							<div class="custom-select">
	    								<select class="value room-adults" name="people[<?php echo $i ?>][adults]">
	 										<?php for ($_i = 1; $_i < 10; $_i++){ ?>
	    									<option value="<?php echo $_i ?>" <?php echo (isset($query['people'][$i]['adults']) && $query['people'][$i]['adults'] == $_i ? 'selected="selected"' : '') ?>><?php echo $_i ?></option>
	    									<?php } ?>
	    								</select>
	    							</div>
	    						</li>
	    						<li>
	    							<p class="custom-label">Nr. copii</p>
	    							<div class="custom-select">
	    								<select class="value room-children" name="people[<?php echo $i ?>][children]">
	    								 	<?php for ($_i = 0; $_i < 10; $_i++){?>
	    								 	<option value="<?php echo $_i ?>" <?php echo (isset($query['people'][$i]['children']) && $query['people'][$i]['children'] == $_i ? 'selected="selected"' : '') ?>><?php echo $_i ?></option>
	    								 	<?php }?>
	    								</select>
	    							</div>
	    						</li>
	    						<?php if (isset($query['people'][$i]['children']) && is_numeric($query['people'][$i]['children'])) {
	    						    for ($c = 0; $c < $query['people'][$i]['children']; $c ++) {?>
                                <li class="children-age">
                                    <p class="custom-label">Varsta copil</p>
                                    <div class="custom-select">
                                        <select class="value" name="people[<?php echo $i ?>][children-ages][]">
                                            <?php for ($ca = 1; $ca < 17; $ca ++) {?>
                                            <option value="<?php echo $ca ?>" <?php echo (isset($query['people'][$i]['age'][$c]) && $query['people'][$i]['age'][$c] == $ca ? 'selected="selected"' : '') ?>><?php echo $ca ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </li>
                                    <?php }
                                }?>
	    					</ul>
						</div>
						    <?php }
                        } else {?>
						<div class="room room-0">
							<ul class="nav cf shorts person fix">
								<li class="room-order"><p class="custom-label">Camera 1:</p></li>
								<li>
	    							<p class="custom-label">Nr. adulti</p>
	    							<div class="custom-select">
	    								<select class="value room-adults" name="people[0][adults]">
	 										<?php for ($i = 1; $i < 10; $i++){ ?>
	    								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
	    								 	<?php } ?>
	    								</select>
	    							</div>
	    						</li>
	    						<li>
	    							<p class="custom-label">Nr. copii</p>
	    							<div class="custom-select">
	    								<select class="value room-children" name="people[0][children]">
	    								 	<?php for ($i = 0; $i < 10; $i++){?>
	    								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
	    								 	<?php }?>
	    								</select>
	    							</div>
	    						</li>
	    					</ul>
						</div>
						<?php }?>
    				</li>
    			</ul>
			</div>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Cauta"><span class="arrow edge"></span><span class="sprite search-icon-small"></span>Cauta</a>
			</div>
		</form>
	</div>

	<?php if (isset($query2)) {?>
	<div id="real-filters">
		<form action="" method="post">
			<ul class="nav main-filters-tree cf">
				<li>
					<p class="radio-heading">Tip oferta</p>
					<ul class="nav checklists radio-nav">
						<li>
							<input class="n" type="radio" name="stars-1" value="1" checked="checked"/>
							<span class="circle"><span class="selected"></span></span>
							<label>
								<span class="stars blue-stars-1"></span>
							</label>
						</li>
						<li>
							<input class="n" type="radio" name="stars-2" value="2"/>
							<span class="circle"></span>
							<label>
								<span class="stars blue-stars-2"></span>
							</label>
						</li>
						<li>
							<input class="n" type="radio" name="stars-3" value="3"/>
							<span class="circle"></span>
							<label>
								<span class="stars blue-stars-3"></span>
							</label>
						</li>
						<li>
							<input class="n" type="radio" name="stars-4" value="4"/>
							<span class="circle"></span>
							<label>
								<span class="stars blue-stars-4"></span>
							</label>
						</li>
						<li>
							<input class="n" type="radio" name="stars-5" value="5"/>
							<span class="circle"></span>
							<label>
								<span class="stars blue-stars-5"></span>
							</label>
						</li>
					</ul>
				</li>
				<li>
					<p class="radio-heading">Masa</p>
					<ul class="nav radio-nav">
						<li><input class="n" type="radio" name="meal" value="0" checked="checked"/><span class="circle"><span class="selected"></span></span><label><span>SC Room Only</span></label></li>
						<li><input class="n" type="radio" name="meal" value="1"/><span class="circle"></span><label><span>BB Bed and Breakfast</span></label></li>
						<li><input class="n" type="radio" name="meal" value="0"/><span class="circle"></span><label><span>HB Half Board</span></label></li>
						<li><input class="n" type="radio" name="meal" value="1"/><span class="circle"></span><label><span>AI All Inclusive</span></label></li>
						<li><input class="n" type="radio" name="meal" value="0"/><span class="circle"></span><label><span>UAI Ultra All Inclusive</span></label></li>
					</ul>
				</li>
			</ul>
			<div class="buttons">
				<button type="submit"></button>
				<a class="button form-submitter filter" href="#" title="Filtreaza"><span class="arrow edge"></span><span class="sprite filter-icon"></span>Filtreaza</a>
			</div>
		</form>
	</div>
	<?php }?>
</div>
<?php
if(
	$partners = $this->get('partners')
){
?>
<section id="partners" class="box">
	<ul class="cf">
		<?php
		foreach(
			$partners as
			$k => $pr
		){
		?>
		<li>
			<a class="external image" href="<?php echo $pr['link']?>" title="<?php echo $pr['name']?>"><img src="<?php echo $pr['image']?>" alt="<?php echo $pr['name']?>" /></a>
		</li>
		<?php
		}
		?>
	</ul>
</section>
<?php
}
?>
<section id="secured-payment" class="box">
	<p>Tranzactii securizate prin 3D Secure</p>
	<span class="card-pay"></span>
	<span class="mobilpay"></span>
</section>