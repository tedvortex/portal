<?php
$categories = $this->get('main_categories');
$countries = $this->get('filter_countries');
$croaziere_countries = $this->get('croaziere_filter_countries');
$croaziere_route = $this->get('croaziere_route');
$circuite_countries = $this->get('circuite_filter_countries');
$circuite_route = $this->get('circuite_route');
?>

<div>
	<?php if ($this->get('constructor') != 'products') {?>
	<section id="reservation-box" class="box main-tabs-container">
		<ul class="nav cf tabs-selection" id="reservation-selection">
			<li class="tab-ref">
				<span class="sprite sejururi-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('sejururi')) ?></span></strong>
			</li>
			<li class="selected tab-ref">
				<span class="sprite cazari-icon-hover"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('cazari')) ?></span></strong>
			</li>
			<li class="tab-ref"  onclick="window.location='/bilete-de-avion'; return false;">
				<span class="sprite zboruri-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('zboruri')) ?></span></strong>
			</li>
			<li class="tab-ref">
				<span class="sprite croaziere-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('croaziere')) ?></span></strong>
			</li>
			<li class="tab-ref">
				<span class="sprite circuite-icon"></span><br/>
				<strong><span><?php echo mb_strtoupper(tr('circuite')) ?></span></strong>
			</li>
		</ul>
		<div id="reservation-tabs" class="tabs-container">
			<div class="tab tab-1 n" id="tab-1">
				<form action="<?php echo $categories[1]['link'] ?>" method="get">
				    <div>
    					<ul class="cf select-list nav">
    						<li>
    							<p class="custom-label">Tara</p>
    							<div class="custom-select country-select">
    							    <?php if ($countries) {?>
    								<select class="value chain" name="filter-country" id="sejururi-filter-country" data-initializer="true">
    								    <?php foreach ($countries as $c) {?>
    									<option value="<?php echo $c['id'] ?>" <?php echo ($c['name_seo'] == 'turcia' ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
    									<?php }?>
    								</select>
    								<?php }?>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Oras</p>
    							<div class="custom-select city-select">
    							    <select class="value chain" name="filter-city" id="sejururi-filter-city" data-chain="sejururi-filter-country" data-module="fibula-city"></select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Hotel</p>
    							<div class="custom-select hotel-select">
    							    <select class="value chain" name="filter-hotel" id="sejururi-filter-hotel" data-chain="sejururi-filter-city" data-module="fibula-hotel"></select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label"><label for="date-check-in"><?php echo tr('De la')?></label></p>
    							<input type="text" value="<?php echo date('d/m/Y', time()) ?>" name="filter-departure" id="sejururi-date-check-in" autocomplete="off" class="datepick"/>
    						</li>
    						<li>
    							<p class="custom-label">Durata/nopti</p>
    							<div class="custom-select">
    								<select class="value" name="filter-duration" id="sejururi-filter-duration">
    								    <?php for($i=0;$i<15;$i++) {?>
    									<option value="<?php echo $i ?>"><?php echo ($i == 0 ? 'Selecteaza' : $i)?></option>
    									<?php }?>
    								</select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Nr. adulti</p>
    							<div class="custom-select">
    								<select class="value" name="filter-adults" id="sejururi-filter-adults">
    								    <?php for($i=1;$i<=10;$i++) {?>
    									<option value="<?php echo $i ?>"><?php echo $i?></option>
    									<?php }?>
    								</select>
    							</div>
    						</li>
    						<li>
    							<p class="custom-label">Nr. copii</p>
    							<div class="custom-select">
    								<select class="value" name="filter-children" id="sejururi-filter-children">
    								    <?php for($i=0;$i<4;$i++) {?>
    									<option value="<?php echo $i ?>"><?php echo $i?></option>
    									<?php }?>
    								</select>
    							</div>
    						</li>
    					</ul>
    					<div class="buttons">
    					    <button type="submit" class="n"></button>
    						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>Cauta</a>
    					</div>
					</div>
				</form>
			</div>
			<div class="tab tab-2" id="tab-2">
				<form action="<?php echo $categories[2]['link'] ?>" method="get">
				    <div>
		    			<ul class="nav select-list cf">
		    				<li>
		    					<p class="custom-label">Cauta dupa Oras</p>
		    					<div class="ai-input">
		    						<input type="hidden" name="ai_city" value="0" />
		    						<input type="hidden" name="ai_country" value="0" />
		    						<input type="text" name="location" value="" autocomplete="off" data-chain="housing-country" />
		    						<div><ul class="ai-list"></ul></div>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Tara destinatie</p>
		    					<div class="custom-select country-select">
		    						<?php if ($this->get('countries')) {?>
		    						<select class="value chain" name="country" id="housing-country" <?php echo (! isset($query) ? 'data-initializer="true"' : '') ?>>
		    						    <?php foreach ($this->get('countries') as $c) {?>
		    							<option value="<?php echo $c['id'] ?>" <?php echo ($c['id'] == (isset($query['id_country']) && $query['id_country'] == $c['id'] ? $c['id'] : 1522) ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    							<?php }?>
		    						</select>
		    						<?php }?>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Zona/localitate</p>
		    					<div class="custom-select country-select">
		    						<select class="value chain" name="city" id="housing-city" data-chain="housing-country" data-module="city">
		    						    <?php foreach ((! $this->get('cities') ? array() : $this->get('cities')) as $c) {?>
		    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_city']) && $query['id_city'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    						    <?php }?>
		    						</select>
		    					</div>
		    				</li>
		    				<li>
		    					<p class="custom-label">Hotel</p>
		    					<div class="custom-select country-select">
		    						<select class="value chain" name="hotel" id="housing-hotel" data-chain="housing-city" data-module="hotel">
		    						    <?php foreach ((! $this->get('hotels') ? array() : $this->get('hotels')) as $c) {?>
		    						    <option value="<?php echo $c['id'] ?>" <?php echo (isset($query['id_hotel']) && $query['id_hotel'] == $c['id'] ? 'selected="selected"' : '') ?>><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
		    						    <?php }?>
		    						</select>
		    					</div>
		    				</li>
		    			</ul>
    					<ul class="nav select-list cf">
    						<li>
    							<p class="custom-label"><?php echo tr('Data plecare')?></p>
    							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date" id="housing-date" autocomplete="off" class="custom-date datepick"/>
    							<span class="sprite date-icon"></span>
    						</li>
    						<li>
    							<p class="custom-label">Durata/nopti</p>
    							<div class="custom-select country-select">
    								<select class="value" name="nights" id="housing-nights">
    								    <?php for ($i = 1; $i <= 30; $i++) {?>
    								 	<option value="<?php echo $i ?>" <?php echo (isset($query['nights']) && $query['nights'] == $i ? 'selected="selected"' : '') ?>><?php echo $i ?></option>
    								 	<?php }?>
    								</select>
    							</div>
    						</li>
    					</ul>
    					<ul class="cf select-list small-select nav">
    					   <li id="cazari-rooms" class="rooms-container">
            					<div>
            						<p class="custom-label">Nr. camere</p>
        							<div class="custom-select">
        								<select class="value rooms-total" name="rooms" data-container="cazari-rooms">
        								 	<?php for ($i = 1; $i < 4; $i++){ ?>
        								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
        								 	<?php } ?>
        								</select>
        							</div>
        						</div>
        						<div class="room room-0">
        							<ul class="nav cf shorts person fix">
        								<li class="room-order"><p class="custom-label">Camera 1:</p></li>
        								<li>
        	    							<p class="custom-label">Nr. adulti</p>
        	    							<div class="custom-select">
        	    								<select class="value room-adults" name="people[0][adults]">
        	 										<?php for ($i = 1; $i < 10; $i++){ ?>
        	    								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
        	    								 	<?php } ?>
        	    								</select>
        	    							</div>
        	    						</li>
        	    						<li>
        	    							<p class="custom-label">Nr. copii</p>
        	    							<div class="custom-select">
        	    								<select class="value room-children" name="people[0][children]">
        	    								 	<?php for ($i = 0; $i < 10; $i++){?>
        	    								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
        	    								 	<?php }?>
        	    								</select>
        	    							</div>
        	    						</li>
        	    					</ul>
        						</div>
            				</li>
    					</ul>
					</div>
					<div class="buttons">
						<button type="submit"></button>
						<a class="button orange form-submitter filter" href="#" title="Cauta"><span class="sprite orange-edge"></span><span class="sprite search-icon-small"></span>Cauta</a>
					</div>
				</form>
			</div>
			<div class="tab tab-3 n" id="tab-3">
			     <div style="text-align:center"><img src="<?php echo image_uri('images/search-loader.gif')?>" alt="loader" /></div>
			</div>
			<div class="tab tab-4 n" id="tab-4">
				<form action="<?php echo $categories[3]['link'] ?>" method="get">
					<ul class="cf select-list nav">
					    <li>
							<p class="custom-label"><label for="croaziere-date-check-in"><?php echo tr('Data plecare intre')?></label></p>
							<input type="text" value="<?php echo date('d/m/Y', time()) ?>" name="filter-departure" id="croaziere-date-check-in" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"></p>
							<input type="text" value="<?php echo date('d/m/Y', (time() + 604800)) ?>" name="filter-departure2" id="croaziere-date-check-in-2" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label">Durata/nopti</p>
							<div class="custom-select">
							    <?php if($duration = $this->get('filter_duration')){?>
								<select class="value" name="filter-duration" id="croaziere-filter-duration">
								    <option value="0">Selecteaza</option>
								    <?php for ($i = $duration['min']; $i <= $duration['max']; $i++) {?>
								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
								 	<?php }?>
								</select>
								<?php }?>
							</div>
						</li>
						<li class="n">
							<p class="custom-label">Destinatie</p>
							<div class="custom-select">
							    <?php if ($croaziere_countries) {?>
								<select class="value chain" name="filter-country" id="croaziere-filter-country" data-initializer="true">
								    <?php foreach ($croaziere_countries as $c) {?>
									<option value="<?php echo $c['id'] ?>"><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
									<?php }?>
								</select>
								<?php }?>
							</div>
						</li>
						<li>
							<p class="custom-label">Linii de croaziera</p>
							<div class="custom-select">
							    <select class="value chain" name="filter-city" id="croaziere-filter-city" data-chain="croaziere-filter-country" data-module="fibula-city"></select>
							</div>
						</li>
						<?php if($croaziere_route){ ?>
        				<li class="boxes-check-select-itinerariu">
        				    <p class="custom-label">Itinerariu</p>
        				    <div class="check-select">
        				        <div class="check-select-value"><p id="route-check-select-croaziere">Toate variantele</p><span class="triangle"></span></div>
        				        <div class="check-select-storage">
        				            <ul data-name="route-check-select-croaziere">
        				                <li>
        				                    <label for="cs-all">Toate variantele</label>
        				                    <input class="select-all" id="cs-all" type="checkbox" name="filter-cs_all" value="0" />
        				                </li>
        				                <?php foreach($croaziere_route as $c){ ?>
        				                <li>
        				                    <label for="cs-country-<?php echo $c['id_country']?>"><?php echo $c['country_name']?></label>
        				                    <input id="cs-country-<?php echo $c['id_country']?>" type="checkbox" name="filter-route_countries[]" value="<?php echo $c['id_country']?>" />
        				                </li>
        				                    <?php
        				                    if($c['cities']){
                                                foreach($c['cities'] as $ct){
                                            ?>
                                        <li>
                                            <label class="side" for="cs-city-<?php echo $ct['id']?>"><?php echo $ct['name']?></label>
                                            <input id="cs-city-<?php echo $ct['id']?>" type="checkbox" name="filter-route_cities[]" value="<?php echo $ct['id']?>" />
                                        </li>
                                            <?php
                                                }
                                            }
                                            ?>
        				                <?php } ?>
        				            </ul>
        				        </div>
        				    </div>
        				</li>
        				<?php } ?>
        				<li>
							<p class="custom-label">Nr. adulti</p>
							<div class="custom-select">
								<select class="value" name="filter-adults" id="croaziere-filter-adults">
								    <?php for($i=0;$i<=10;$i++) {?>
									<option value="<?php echo $i ?>"><?php echo $i?></option>
									<?php }?>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Nr. copii</p>
							<div class="custom-select">
								<select class="value" name="filter-children" id="croaziere-filter-children">
								    <?php for($i=0;$i<4;$i++) {?>
									<option value="<?php echo $i ?>"><?php echo $i?></option>
									<?php }?>
								</select>
							</div>
						</li>
					</ul>
					<div class="buttons">
					    <button type="submit"></button>
						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>cauta</a>
					</div>
				</form>
			</div>
			<div class="tab tab-5 n" id="tab-5">
				<form action="<?php echo $categories[4]['link'] ?>" method="get">
					<ul class="cf select-list nav">
					    <li>
							<p class="custom-label"><label for="circuite-date-check-in"><?php echo tr('Data plecare intre')?></label></p>
							<input type="text" value="<?php echo date('d/m/Y', time()) ?>" name="filter-departure" id="circuite-date-check-in" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"></p>
							<input type="text" value="<?php echo date('d/m/Y', (time() + 604800)) ?>" name="filter-departure2" id="circuite-date-check-in-2" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label">Durata/nopti</p>
							<div class="custom-select">
							    <?php if($duration = $this->get('circuite_filter_duration')){?>
								<select class="value" name="filter-duration" id="circuite-filter-duration">
								    <option value="0">Selecteaza</option>
								    <?php for ($i = $duration['min']; $i <= $duration['max']; $i++) {?>
								 	<option value="<?php echo $i ?>"><?php echo $i ?></option>
								 	<?php }?>
								</select>
								<?php }?>
							</div>
						</li>
						<li>
							<p class="custom-label">Tara destinatie</p>
							<div class="custom-select">
							    <?php if ($circuite_countries) {?>
								<select class="value chain" name="filter-country" id="circuite-filter-country" data-initializer="true">
								    <option value="0">Selecteaza</option>
								    <?php foreach ($circuite_countries as $c) {?>
									<option value="<?php echo $c['id'] ?>"><?php echo mb_convert_case($c['name'], MB_CASE_TITLE) ?></option>
									<?php }?>
								</select>
								<?php }?>
							</div>
						</li>
						<li>
							<p class="custom-label">Zona/localitate</p>
							<div class="custom-select">
							    <select class="value chain" name="filter-city" id="circuite-filter-city" data-chain="circuite-filter-country" data-module="fibula-city"></select>
							</div>
						</li>
						<?php if($circuite_route){ ?>
        				<li class="boxes-check-select-itinerariu">
        				    <p class="custom-label">Itinerariu</p>
        				    <div class="check-select">
        				        <div class="check-select-value"><p id="route-check-select-circuite">Toate variantele</p><span class="triangle"></span></div>
        				        <div class="check-select-storage">
        				            <ul data-name="route-check-select-circuite">
        				                <li>
        				                    <label for="cs-all-circuite">Toate variantele</label>
        				                    <input class="select-all" id="cs-all-circuite" type="checkbox" name="filter-cs_all" value="0" />
        				                </li>
        				                <?php foreach($circuite_route as $c){ ?>
        				                <li>
        				                    <label for="cs-country-<?php echo $c['id_country']?>"><?php echo $c['country_name']?></label>
        				                    <input id="cs-country-<?php echo $c['id_country']?>" type="checkbox" name="filter-route_countries[]" value="<?php echo $c['id_country']?>" />
        				                </li>
        				                    <?php
        				                    if($c['cities']){
                                                foreach($c['cities'] as $ct){
                                            ?>
                                        <li>
                                            <label class="side" for="cs-city-<?php echo $ct['id']?>"><?php echo $ct['name']?></label>
                                            <input id="cs-city-<?php echo $ct['id']?>" type="checkbox" name="filter-route_cities[]" value="<?php echo $ct['id']?>" />
                                        </li>
                                            <?php
                                                }
                                            }
                                            ?>
        				                <?php } ?>
        				            </ul>
        				        </div>
        				    </div>
        				</li>
        				<li>
							<p class="custom-label">Nr. adulti</p>
							<div class="custom-select">
								<select class="value" name="filter-adults" id="circuite-filter-adults">
								    <?php for($i=0;$i<=10;$i++) {?>
									<option value="<?php echo $i ?>"><?php echo $i?></option>
									<?php }?>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Nr. copii</p>
							<div class="custom-select">
								<select class="value" name="filter-children" id="circuite-filter-children">
								    <?php for($i=0;$i<4;$i++) {?>
									<option value="<?php echo $i ?>"><?php echo $i?></option>
									<?php }?>
								</select>
							</div>
						</li>
        				<?php } ?>
						<!-- <li>
							<p class="custom-label">Tara</p>
							<div class="custom-select country-select">
								<select class="value">
									<option value="0">Romania</option>
									<option value="1">Bulgaria</option>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Oras</p>
							<div class="custom-select city-select">
								<select class="value">
									<option value="0">Bucuresti</option>
									<option value="1">Sinaia</option>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Hotel</p>
							<div class="custom-select hotel-select">
								<select class="value">
									<option value="0">Ritz</option>
									<option value="1">Belvedere</option>
								</select>
							</div>
						</li>
						<li>
							<p class="custom-label">Categorie</p>
							<div class="custom-select stars-select">
								<select class="value">
									<option value="0">4 stele</option>
									<option value="1">5 stele</option>
								</select>
							</div>
						</li> -->
					</ul>
					<!--<ul class="cf nav date-select">
						<li>
							<p class="custom-label"><label for="date-check-in3"><?php echo tr('De la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() )?>" name="date-check-in3" id="date-check-in3" autocomplete="off" class="datepick"/>
						</li>
						<li>
							<p class="custom-label"><label for="date-check-out3"><?php echo tr('Pana la')?></label></p>
							<input type="text" value="<?php echo date( 'd/m/Y', time() + 86400 )?>" name="date-check-out3" id="date-check-out3" autocomplete="off" class="datepick"/>
						</li>
					</ul>-->
					<div class="buttons">
					    <button type="submit"></button>
						<a href="#" class="button orange form-submitter" title="Cauta"><span class="sprite search-icon-small"></span><span class="sprite orange-edge"></span>cauta</a>
					</div>
				</form>
			</div>
		</div>
	</section>
	<?php }
	$linkuri=$this->get('linkuri');?>
    <section id="offers-box" class="box">
    	<ul class="nav">
    		<li class="purple-background">
				<a href="<?php echo $linkuri[0]['link']?>" title="<?php echo $linkuri[0]['name']?>"><?php echo $linkuri[0]['name']?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="lightgreen-background">
				<a href="<?php echo $linkuri[1]['link']?>" title="<?php echo tr('Oferte sezon') ?>"><?php echo $linkuri[1]['name']?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="violet-background">
				<a href="<?php echo $linkuri[2]['link']?>" title="<?php echo tr('Oferte tematice') ?>"><?php echo $linkuri[2]['name']?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="orange-background">
				<a href="<?php echo $linkuri[4]['link']?>" title="<?php echo tr('Oferte tematice') ?>"><?php echo $linkuri[4]['name']?><span class="sprite oferte-icon-box"></span></a></li>
    		<li class="blue-background">
				<a href="<?php echo $linkuri[3]['link']?>" title="<?php echo tr('Oferte personalizate') ?>"><?php echo $linkuri[3]['name']?><span class="sprite oferte-icon-hover-box"></span></a></li>
    	</ul>
    </section>
    <?php if($this->get('constructor')!='products'){?>
    <section id="contact-box" class="box">
        <p><?php echo tr('Ne puteti contacta la numarul de telefon') ?></p>
        <p class="phone">+40 21 317 2606</p>
        <p class="phone">+40 21 317 2608</p>
        <p class="phone">+40 21 318 8871</p>
    </section>
    <section id="newsletter-box" class="box">
    	<p>Abonare newsletter</p>
    	<form action="<?php echo $this->get('b') ?>" method="post">
    		<div class="form-container">
    			<input type="hidden" value="newsletter" name="action" />
    			<input type="text" name="newsletter-email" value="" />
    			<div class="buttons">
    				<button type="submit"></button>
    				<a href="#" class="button form-submitter blue" title="trimite"><span class="edge arrow"></span>Trimite</a>
    			</div>
    		</div>
    	</form>
    </section>
    <?php
	if(
		$partners = $this->get('partners')
	){
	?>
    <section id="partners" class="box">
    	<ul class="cf">
			<?php
			foreach(
				$partners as
				$k => $pr
			){
			?>
			<li>
				<a class="external image" href="<?php echo $pr['link']?>" title="<?php echo $pr['name']?>"><img src="<?php echo $pr['image']?>" alt="<?php echo $pr['name']?>" /></a>
			</li>
			<?php
			}
			?>
		</ul>
    </section>
    <?php
	}
	?>
    <section id="secured-payment" class="box">
    	<p>Tranzactii securizate prin 3D Secure</p>
    	<span class="card-pay"></span>
    	<span class="mobilpay"></span>
    </section>
   	<?php
	   	if($slides = $this->get('slides'))
	   	{
	   		if( $slides['first'] || $slides['second'] )
	   		{
	   			foreach( $slides as $k => $slide )
	   			{
	   				if( $slide )
	   				{
   	?>
   	<section class="boxes-slide">
	   	<ul class="nextgen-slider nav" data-navigator="boxes-<?php echo $k?>-slide-navigator" data-speed="1000" data-timeout="4500">
			<?php foreach( $slide as $sl ){ ?>
			<li>
				<a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>">
					<img src="<?php echo $sl['image']?>" alt="<?php echo $sl['name']?>" />
				</a>
			</li>
			<?php } ?>
	   	</ul>
	   	<ul id="boxes-<?php echo $k?>-slide-navigator" class="boxes-slider-navigator cf"></ul>
   	</section>
   	<?php
	   				}
	   			}
	   		}
   		}
   	}
   	?>
</div>