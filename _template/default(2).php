<?php $this->extend('layout') ?>

<?php if($slide){ ?>
<div class="hero-unit">
    <div id="default-slide-container">
        <ul id="default-slide" class="nextgen-slider nav" data-navigator="default-slide-navigator" data-speed="1000" data-timeout="4500">
			<?php foreach($slide as $sl){ ?>
			<li>
				<a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>">
					<img src="<?php echo $sl['image']?>" alt="<?php echo $sl['name']?>" />
				</a>
			</li>
			<?php } ?>
        </ul>
		<ul id="default-slide-navigator" class="nav cf"></ul>
		<div id="default-slide-description">
			<span class="sprite"></span>
			<span class="sprite right"></span>
			<ul id="default-slide-text" class="cf">
				<?php foreach($slide as $sl){ ?>
				<li><a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>"><?php echo $sl['description']?></a></li>
				<?php } ?>
			</ul>
		</div>
    </div>
</div>
<?php } ?>
<div id="products" class="default-product-listing">
    <div class="hero-unit">
        <div class="item oversea">
            <div class="heading violet-border">
                <h2 class="violet-color"><?php echo tr('Circuite peste hotare') ?></h2>
            </div>
            <a href="#" class="button violet-background" title="<?php echo tr('Circuite peste hotare') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
            <a class="image" href="#" title="<?php echo tr('Circuite peste hotare') ?>">
                <img src="<?php echo $this->get('s').'images/oversea.jpg' ?>" width="830" height="168" alt="<?php echo tr('Circuite peste hotare') ?>" />
                <span class="price violet-background"><?php echo tr('De la') ?><span>1000€</span></span>
            </a>
            <p class="violet-color">Bahamas  /  Cuba  /  Egipt  /  Emiratele Arabe Unite  /  Indonezia  /  Kenya  /  Maldive  /  Maroc  /  Mexic</p>
        </div>
    </div>
    <div class="hero-unit">
        <div class="item oversea">
            <div class="heading violet-border violet-background">
                <h2><?php echo tr('Circuite peste hotare') ?></h2>
            </div>
            <a href="#" class="button violet-background" title="<?php echo tr('Circuite peste hotare') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
            <a class="image" href="#" title="<?php echo tr('Circuite peste hotare') ?>">
                <img src="<?php echo $this->get('s').'images/oversea.jpg' ?>" width="830" height="168" alt="<?php echo tr('Circuite peste hotare') ?>" />
                <span class="price violet-background"><?php echo tr('De la') ?><span>1000€</span></span>
            </a>
            <p class="violet-color">Bahamas  /  Cuba  /  Egipt  /  Emiratele Arabe Unite  /  Indonezia  /  Kenya  /  Maldive  /  Maroc  /  Mexic</p>
        </div>
    </div>
    <div class="hero-unit">
        <div class="booking-detailed-container row">
            <div class="span4">
                <div class="item booking-detailed">
                    <div class="heading lightgreen-border">
                        <h2 class="lightgreen-color"><?php echo tr('Early booking') ?></h2>
                    </div>
                    <a href="#" class="button lightgreen-background" title="<?php echo tr('Early booking') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
                    <a class="image" href="#" title="<?php echo tr('Early booking') ?>">
                        <img src="<?php echo $this->get('s').'images/aztec.jpg' ?>" width="410" height="178" alt="<?php echo tr('Early booking') ?>" />
                        <span class="price lightgreen-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <div class="row">
                        <div class="span2">
                            <h2 class="lightgreen-color lightgreen-border"><?php echo tr('Tururi europene') ?></h2>
                            <ul class="lightgreen-color">
                                <li><span class="sprite check-green"></span><?php echo 'Benelux si Paris' ?></li>
                                <li><span class="sprite check-green"></span><?php echo 'Italia' ?></li>
                                <li><span class="sprite check-green"></span><?php echo 'Europa Centrala' ?></li>
                            </ul>
                        </div>
                        <div class="span2">
                            <h2 class="lightgreen-color lightgreen-border"><?php echo tr('Calatorii indepartate') ?></h2>
                            <ul class="lightgreen-color">
                                <li><span class="sprite check-green"></span><?php echo 'Bangok - Pattaya' ?></li>
                                <li><span class="sprite check-green"></span><?php echo 'Bangok - Phuket' ?></li>
                                <li><span class="sprite check-green"></span><?php echo 'Extremul Orient' ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="item booking-detailed">
                    <div class="heading orange-border">
                        <h2 class="orange-color"><?php echo tr('Vara 2013') ?></h2>
                    </div>
                    <a href="#" class="button orange-background" title="<?php echo tr('Vara 2013') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
                    <a class="image" href="#" title="<?php echo tr('Vara 2013') ?>">
                        <img src="<?php echo $this->get('s').'images/stairs.jpg' ?>" width="410" height="178" alt="<?php echo tr('Vara 2013') ?>" />
                        <span class="price orange-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <div class="row">
                        <div class="span2">
                            <h2 class="orange-color orange-border"><?php echo tr('Tururi europene') ?></h2>
                            <ul class="orange-color">
                                <li><span class="sprite check-orange"></span><?php echo 'Benelux si Paris' ?></li>
                                <li><span class="sprite check-orange"></span><?php echo 'Italia' ?></li>
                                <li><span class="sprite check-orange"></span><?php echo 'Europa Centrala' ?></li>
                            </ul>
                        </div>
                        <div class="span2">
                            <h2 class="orange-color orange-border"><?php echo tr('Calatorii indepartate') ?></h2>
                            <ul class="orange-color">
                                <li><span class="sprite check-orange"></span><?php echo 'Bangok - Pattaya' ?></li>
                                <li><span class="sprite check-orange"></span><?php echo 'Bangok - Phuket' ?></li>
                                <li><span class="sprite check-orange"></span><?php echo 'Extremul Orient' ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-unit">
        <div class="booking-detailed-container row">
            <div class="span4">
                <div class="item booking-detailed">
                    <div class="heading lightgreen-border lightgreen-background">
                        <h2><?php echo tr('Early booking') ?></h2>
                    </div>
                    <a href="#" class="button lightgreen-background" title="<?php echo tr('Early booking') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
                    <a class="image" href="#" title="<?php echo tr('Early booking') ?>">
                        <img src="<?php echo $this->get('s').'images/aztec.jpg' ?>" width="410" height="178" alt="<?php echo tr('Early booking') ?>" />
                        <span class="price lightgreen-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <p class="lightgreen-color">Tururi europene  /  Calatorii indepartate</p>
                </div>
            </div>
            <div class="span4">
                <div class="item booking-detailed">
                    <div class="heading orange-border orange-background">
                        <h2><?php echo tr('Vara 2013') ?></h2>
                    </div>
                    <a href="#" class="button orange-background" title="<?php echo tr('Vara 2013') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
                    <a class="image" href="#" title="<?php echo tr('Vara 2013') ?>">
                        <img src="<?php echo $this->get('s').'images/stairs.jpg' ?>" width="410" height="178" alt="<?php echo tr('Vara 2013') ?>" />
                        <span class="price orange-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <p class="orange-color">China  /  Japonia  /  Bahamas  /  Egipt  /  Maldive</p>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-unit">
        <div class="tour-detailed-container row">
            <div class="span2">
                <div class="item tour-detailed">
                    <div class="heading violet-border violet-background">
                        <h2 class="violet-border"><?php echo tr('Tururi europene') ?></h2>
                    </div>
                    <a class="image" href="#" title="<?php echo tr('Tururi europene') ?>">
                        <img src="<?php echo $this->get('s').'images/dubai.jpg' ?>" width="200" height="268" alt="<?php echo tr('Tururi europene') ?>" />
                        <span class="price violet-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                </div>
            </div>
            <div class="span2">
                <div class="item tour-detailed">
                    <div class="heading orange-border orange-background">
                        <h2 class="orange-border"><?php echo tr('Calatorii indepartate') ?></h2>
                    </div>
                    <a class="image" href="#" title="<?php echo tr('Calatorii indepartate') ?>">
                        <img src="<?php echo $this->get('s').'images/egypt.jpg' ?>" width="200" height="268" alt="<?php echo tr('Calatorii indepartate') ?>" />
                        <span class="price orange-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                </div>
            </div>
            <div class="span2">
                <div class="item tour-detailed">
                    <div class="heading lightgreen-border lightgreen-background">
                        <h2 class="lightgreen-border"><?php echo tr('Asia') ?></h2>
                    </div>
                    <a class="image" href="#" title="<?php echo tr('Asia') ?>">
                        <img src="<?php echo $this->get('s').'images/boats.jpg' ?>" width="200" height="268" alt="<?php echo tr('Asia') ?>" />
                        <span class="price lightgreen-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                </div>
            </div>
            <div class="span2">
                <div class="item tour-detailed">
                    <div class="heading lightblue-border lightblue-background">
                        <h2 class="lightblue-border"><?php echo tr('Exotice') ?></h2>
                    </div>
                    <a class="image" href="#" title="<?php echo tr('Exotice') ?>">
                        <img src="<?php echo $this->get('s').'images/dubai.jpg' ?>" width="200" height="268" alt="<?php echo tr('Exotice') ?>" />
                        <span class="price lightblue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-unit">
        <div class="item cruise">
            <div class="heading blue-border">
                <h2 class="blue-color"><?php echo tr('Croaziere 2013') ?></h2>
            </div>
            <a href="#" class="button blue-background" title="<?php echo tr('Croaziere 2013') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
            <div class="row">
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Asia') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Asia') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color"><?php echo tr('Asia') ?></h2>
                    <ul class="padded-list">
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Norwegian Epic - 7 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Grandeur of the ...- 3 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Roya Caribbean</span><br/>Grandeur of the ... - 3 nopti</li>
                    </ul>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Caraibe') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Caraibe') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color"><?php echo tr('Caraibe') ?></h2>
                    <ul class="padded-list">
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Norwegian Epic - 7 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Grandeur of the ...- 3 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Roya Caribbean</span><br/>Grandeur of the ... - 3 nopti</li>
                    </ul>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Alaska') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Alaska') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color"><?php echo tr('Alaska') ?></h2>
                    <ul class="padded-list">
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Norwegian Epic - 7 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Grandeur of the ...- 3 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Roya Caribbean</span><br/>Grandeur of the ... - 3 nopti</li>
                    </ul>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Marea Neagra') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Marea Neagra') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color"><?php echo tr('Marea Neagra') ?></h2>
                    <ul class="padded-list">
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Norwegian Epic - 7 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Norwegian Cruise Line</span><br/>Grandeur of the ...- 3 nopti</li>
                        <li><span class="sprite check-blue"></span><span class="blue-color">Roya Caribbean</span><br/>Grandeur of the ... - 3 nopti</li>
                    </ul>
                </div>
            </div>
            <p class="blue-color blue-border border-top">Royal Caribbean  /  Costa Cruises  /  Pribcess Cruises  /  Norwegian Cruise Line</p>
        </div>
    </div>
    <div class="hero-unit">
        <div class="item cruise">
            <div class="heading blue-border blue-background">
                <h2><?php echo tr('Croaziere 2013') ?></h2>
            </div>
            <a href="#" class="button blue-background" title="<?php echo tr('Croaziere 2013') ?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
            <div class="row">
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Asia') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Asia') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color transparent-border"><?php echo tr('Asia') ?></h2>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Caraibe') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Caraibe') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color transparent-border"><?php echo tr('Caraibe') ?></h2>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Alaska') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Alaska') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color transparent-border"><?php echo tr('Alaska') ?></h2>
                </div>
                <div class="span2">
                    <a href="#" class="image" title="<?php echo tr('Marea Neagra') ?>">
                        <img src="<?php echo $this->get('s').'images/cruise.jpg' ?>" alt="<?php echo tr('Marea Neagra') ?>" width="200" height="148" />
                        <span class="price blue-background"><?php echo tr('De la') ?><span>1000€</span></span>
                    </a>
                    <h2 class="blue-color transparent-border"><?php echo tr('Marea Neagra') ?></h2>
                </div>
            </div>
            <p class="blue-color blue-border border-top">Royal Caribbean  /  Costa Cruises  /  Pribcess Cruises  /  Norwegian Cruise Line</p>
        </div>
    </div>
</div>