<?php $this->extend('layout') ?>

<section id="main-products-container" class="<?php echo $class?>">
    <?php if ($query) {?>
	<div id="search-result">
		<p>Au fost gasite <?php echo count($products) ?> hoteluri cu destinatia <?php echo '' ?>, Turcia in data de 01 august 2013, cu durata de <?php echo $query['nights'] ?> nopti</p>
	</div>
	<?php }?>

	<?php if ($pagination) {?>
	<div class="pagenav-container cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort_by" name="sort_by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<?php echo $pagination ?>
	</div>
	<?php }?>

	<?php if ($products) {?>
	<ul id="main-products" class="nav">
		<?php foreach ($products as $k => $p) {?>
		<li>
			<div class="offer-container cf">
				<img class="image-holder" src="<?php echo image_uri($p['image'])?>" alt=""/>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2><a href="<?php echo $link . '/' . $p['id_hotel'] ?>" title="Click pentru a alege oferta"><?php echo $p['hotel_name']?></a></h2>
						<p><?php echo $p['city_name']?></p>
					</div>
					<div class="right-offer-right">
						<p><?php echo $p['offers'] . ' ofert' . ($p['offers'] > 1 ? 'e' : 'a') ?> incepand de la <br /> <span><?php echo $p['price'] ?> EUR</span> half board</p>
					</div>
					<div class="right-offer-center">
					    <?php if ($p['rating']) {?>
						<ul class="stars">
							<?php for ($i = 0; $i < $p['rating']; $i++) {?>
							<li class="sprite"></li>
							<?php }?>
						</ul>
						<?php }?>
						<a class="map-tag lightbox" href="https://maps.google.com/maps?q=<?php echo $p['latitude'] . ',' . $p['longitude'] . '&amp;ll' . $p['latitude'] . ',' . $p['longitude'] . '&amp;lightbox[width]=610&lightbox[height]=360'?>" title=""><span class="sprite map-icon"></span>Harta</a>
					</div>
					<div class="right-offer-bottom"><p><?php echo $p['short_description'] ?></p></div>
				</div>
			</div>
			<ul class="products-table">
				<?php
				if (isset($p['description']->AvailabilityResponseHotelRoom)) {
                    foreach ($p['rooms'] as $kk => $r) {
                        foreach ($r->boards as $k => $b) {?>
				<li>
					<ul class="products-table-list cf">
						<li class="col-1">
							<p><span class="sprite dogtag"></span><?php echo $r->Name ?><br /> <?php echo $r->Adults ?> AD + <?php echo $r->Children ?> CHD (5, 11)</p>
							<ul class="cf">
								<li><a href="#" title="">Toate tipurile de camere</a></li>
								<li>|</li>
								<li><a href="#" title="">Mai multe detalii</a></li>
							</ul>
						</li>
						<li class="col-2"><p><?php echo $b->Name ?></p></li>
						<li class="col-3"><p><?php echo $b->AvailabilityResponseRoomPrice->Total . ' ' . $b->AvailabilityResponseRoomPrice->Currency ?></p></li>
						<li class="col-4"><p>Confirmare imediata</p></li>
						<li class="col-5"><a class="button checked" href="#" title=""><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a></li>
					</ul>
				</li>
				        <?php }
			        }
				}?>
			</ul>
		</li>
		<?php }?>
	</ul>
	<?php } else {
	    echo '<p class="no-products" style="padding:25px 20px 0;"><strong>Va rugam folositi caseta laterala pentru a efectua o cautare !</strong></p>';
	?>
		<div id="choice-container">
			<p class="with-underline">Ati ales:</p>
			<p>Cazare in Kemer, Turcia</p>
			<p>Turisti: 2 Adulti + 2chd(5) chd(11)</p>
			<p>Data de plecare: 01.aug.2013, Durata: 7 nopti</p>
			<strong>Pentru perioada selectata oferta nu mai este valabila</strong>
			<p>Daca sunteti prea ocupati, putem extinde noi cautarea dumneavoastra <br />Completati formularul de mai jos si vom raspunde tuturor solicitarilor indifrerent de complexitatea acestora. In cel mai scurt timp primiti cea mai buna varianta posibila!</p>
			<h2>Cerere de oferta personalizata</h2>
			<form id="action" action="" method="post">
				<input type="hidden" name="validation" value="1"/>
				<div id="form-container">
					<?php echo $form?>
				</div>
				<div id="choice-button-container" class="buttons cf">
					<button type="submit" class="n"></button>
					<a href="#" title="" class="button checked"><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a>
				</div>
			</form>
		</div>
	<?php
	}?>

	<?php if ($pagination) {?>
	<div class="pagenav-container with-border cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort-by" name="sort-by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<?php echo $pagination ?>
	</div>
	<?php }?>
</section>