<?php $this->extend('layout') ?>

<form name="frmPaymentRedirect" method="post" action="<?php echo $paymentUrl;?>">
    <div>
    	<input type="hidden" name="env_key" value="<?php echo $env_key?>"/>
    	<input type="hidden" name="data" value="<?php echo $data?>"/>

    	<p>Pentru a finaliza plata vei redirectat catre pagina de plati securizata a mobilpay.ro</p>
    	<!--<p>Daca nu esti redirectat in 5 secunde apasa <input type="image" src="images/12792_mobilpay-96x30.gif" /></p>-->
	</div>
</form>

<script type="text/javascript" language="javascript">
window.setTimeout(document.frmPaymentRedirect.submit(), 500);
</script>