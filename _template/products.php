<?php $this->extend('layout') ?>
<?php if($type==1){?>
<section id="main-products-container" class="<?php echo $class?>">
	<div id="search-result">
		<p>Au fost gasite 30 hoteluri cu destinatia Kemer, Turcia in data de 01 august 2013, cu durata de 7 nopti</p>
	</div>
	<div class="pagenav-container cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort_by" name="sort_by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="pagination">
			<a class="a" href="#">1</a>
			<a class="" href="#">2</a>
			<a href="">»</a>
		</div>
	</div>
	<ul id="main-products" class="nav">
		<?php for ($j = 0; $j < 4; $j++) {?>
		<li>
			<div class="offer-container cf">
				<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2>Rixos Premium</h2>
						<p>Kemer Turcia</p>
					</div>
					<div class="right-offer-right">
						<p>4 oferte incepand de la <br /> <span>646,00 EUR</span> half board</p>
					</div>
					<div class="right-offer-center">
						<ul class="stars">
							<?php
							for (
								$i=0;
								$i<5;
								$i++
							){
							?>
							<li class="sprite"></li>
							<?php
							}
							?>
						</ul>
						<a class="map-tag" href="#" title="#"><span class="sprite map-icon"></span>Harta</a>
					</div>
					<div class="right-offer-bottom">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
					</div>
				</div>
			</div>
			<table class="products-table">
				<tbody>
					<?php
					for (
						$i=0;
						$i<3;
						$i++
					){
					?>
					<tr>
						<td class="col-1">
							<p><span class="sprite dogtag"></span>Family Room Standard Land View 2AD + 2 CHD (5, 11)</p>
							<ul>
								<li><a href="#" title="#">Toate tipurile de camere</a></li>
								<li>|</li>
								<li><a href="#" title="#">Mai multe detalii</a></li>
							</ul>
						</td>
						<td class="col-2">
							<p>Half Board</p>
						</td>
						<td class="col-3">
							<p>15678 euro</p>
						</td>
						<td class="col-4">
							<p>Confirmare imediata</p>
						</td>
						<td class="col-5">
							<a class="button checked" href="#" title="#">
								<span class="sprite white-check"></span><span class="edge arrow"></span>
								Rezerva
							</a>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</li>
		<?php
		}
		?>
	</ul>
	<div class="pagenav-container with-border cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort_by" name="sort_by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="pagination">
			<a class="a" href="#">1</a>
			<a class="" href="#">2</a>
			<a href="">»</a>
		</div>
	</div>
</section>
<?php
}else{
?>
<section id="main-products-container" class="<?php echo $class?>">
	<div id="search-result">
		<p>Au fost gasite 30 hoteluri cu destinatia Kemer, Turcia in data de 01 august 2013, cu durata de 7 nopti</p>
	</div>
	<div class="pagenav-container cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort_by" name="sort_by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="pagination">
			<a class="a" href="#">1</a>
			<a class="" href="#">2</a>
			<a href="">»</a>
		</div>
	</div>
	<ul id="main-products" class="nav">
		<?php
		for (
			$j=0;
			$j<4;
			$j++
		){
		?>
		<li>
			<div class="offer-container cf">
				<div class="images-holder cf">
					<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
					<img class="image-logo" src="<?php echo image_uri('i/products-images/logo-img.jpg')?>" alt=""/>
					<div class="circuit-holder">
						<ul>
							<li><span class="sprite plane"></span>Avion</li>
							<li><span class="sprite bus"></span>Autocar</li>
						</ul>
					</div>
				</div>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2>Croaziera 2013, Caraibe de sud (Ft.Lauderdale)<br /> Lorem ipsum dolor sit amet ... 10 nopti</h2>
					</div>
					<div class="right-offer-right">
						<p>
							4 oferte incepand de la <br /> <span>646,00 EUR</span> half board
						</p>
					</div>
					<div class="right-offer-bottom">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex... Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
						<a href="#" title="#">Mai multe detalii</a>
					</div>
				</div>
			</div>
		</li>
		<?php
		}
		?>
	</ul>
	<div class="pagenav-container with-border cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort_by" name="sort_by" value="0">
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="pagination">
			<a class="a" href="#">1</a>
			<a class="" href="#">2</a>
			<a href="">»</a>
		</div>
	</div>
</section>
<?php
}
?>