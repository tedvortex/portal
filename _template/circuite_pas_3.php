<?php $this->extend('layout')?>

<section class="steps">
	<h2>Circuit: Portugalia - Spania - Maroc / 10 zile</h2>
	<ul class="top-circuit cf">
		<li>
			<span class="sprite plane"></span><strong>Transport:</strong>Avion
		</li>
		<li>
			<span class="sprite date"></span><strong>Perioada:</strong>15 sept. 2013 - 21 sept. 2013
		</li>
	</ul>
	<p class="borders">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex... Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
	<div id="reservation">
		<div class="tab-container">
			<div id="your-reservation">
				Rezervarea ta
			</div>
			<div class="blue-price paleblue-background cf">
				<ul>
					<li>Lorem ipsum dolor sit amet ...</li>
					<li>Lorem ipsum dolor sit amet ...</li>
					<li>Lorem ipsum dolor sit amet ...</li>
					<li>Lorem ipsum dolor sit amet ...</li>
				</ul>
				<div class="right-offer-right">
					<p>
						de la <span>646<strong>00</strong></span> EUR / pers <br /> pret total de la <span>6546<strong>00</strong></span> EUR
					</p>
				</div>
			</div>
			<div class="politics">
				<span class="sprite"></span>
				<h3>Politica de anulare</h3>
				<p>Aceasta rezervare poate fi anulata GRATUIT inainte de Vineri, 12 Aprilie 2013. Incepand cu Vineri, 12 Aprilie 2013 va trebui sa platesti pentru anularea rezervarii o taxa de penalizare de 10 E . Incepand cu Miercuri, 05 Iunie 2013 va trebui sa platesti pentru anularea rezervarii o taxa de penalizare de 316 E</p>
			</div>
		</div>
	</div>
	<section id="unfolded-offer-container">
		<strong>Modalitati de plata</strong>
		<ul id="unfolded-offer-tabs" class="tabs-list tabs-list-big nav cf" data-tabs="unfolded-offer-days">
			<li class="a"><p>Plata Online</p></li>
			<li><p>Plata Offline</p></li>
		</ul>
		<div class="tab-container margin-top">
		<!--<ul class="tab-list">
			<li id="online" class="active">
				Plata OnLine
			</li>
			<li id="offline">
				Plata OffLine
			</li>
		</ul>-->
			<div class="input-container cf">
				<?php
				for (
					$i=0;
					$i<6;
					$i++
				){
				?>
				<div <?php if ($i%3==0){echo 'class="no-margin"';}?>>
					<label for="lorem">Lorem ipsum</label>
					<input type="text" name="lorem" id="lorem" placeholder="lorem ipsum" value=""/>
				</div>
				<?php
				}
				?>
			</div>		
		</div>
	</section>
	<div id="termsandconditions">
		<input type="checkbox" class="n" name="accept_terms" id="accept_terms"/>
		<p><span class="term-checkbox"><span class="normal"></span></span>Sunt de acord cu <a href="#" title="#">Termenii si conditiile</a> si cu <a href="#" title="#">Politica de anulare</a></p>
	</div>
	<div id="button-container" class="cf">
		<a class="button checked" href="#" title="#">Rezerva <span class="sprite white-check"></span><span class="edge arrow"></span></a>
	</div>
</section>