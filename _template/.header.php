<?php $pages = $this->get('pages')?>
<header>
    <div class="container cf">
    	<h1 id="logo">
    		<a href="<?php $this->output('b')?>" title="Click and Go"></a>
    	</h1>
    	<div id="partner-login-container" >
    		<?php if(!$this->get('logged')){?>
    		<p>Login parteneri<span class="circle"><span class="sprite key-login-header"></span></span></p>
    		<div id="partner-login">
				<form id="login" action="<?php $this->output('b')?>" method="post">
					<div>
						<input type="hidden" name="action" value="login"/>
						<dl class="cf">
							<dt><label for="partner-email">e-mail</label></dt>
							<dd><input tabindex="-1" id="partner-email" type="text" name="partner-email" autocomplete="off" required="required" tabindex="1" /></dd>

							<dt><label for="partner-password">parola</label></dt>
							<dd><input tabindex="-1" id="partner-password" type="password" name="partner-password" autocomplete="off" required="required" tabindex="2" /></dd>
						</dl>
						<div class="buttons">
							<button type="submit"></button>
							<a href="#" tabindex="-1" title="Conectare" class="button form-submitter blue">Conectare</a>
						</div>
					</div>
				</form>
			</div>
			<?php }else{ ?>
				<p>Bine ati venit!<span class="circle"><span class="sprite key-login-header"></span></span></p>
				<form action="<?php $this->output('b')?>" class="is-logged" method="post">
					<input type="hidden" name="action" value="logout"/>
					<div id="account-logged">
						<p class="info">Sunteti logat ca <span><?php $this->output('name')?></span></p>
						<div class="buttons">
							<button type="submit" class="n"></button>
							<a href="#" title="Deconectare" class="button form-submitter blue">Deconectare</a>
						</div>
					</div>
				</form>
			<?php } ?>
		</div>
        <section class="social-navigation navbar cf" id="default-social-navigation">
            <ul class="nav">
                <li><a class="facebook-background" href="#" title="<?php echo tr('Facebook') ?>"><span class="sprite facebook-social"></span></a></li>
                <li><a class="twitter-background" href="#" title="<?php echo tr('Twitter') ?>"><span class="sprite twitter-social"></span></a></li>
                <li><a class="googleplus-background" href="#" title="<?php echo tr('Google+') ?>"><span class="sprite googleplus-social"></span></a></li>
                <li><a class="pinterest-background" href="#" title="<?php echo tr('Pinterest') ?>"><span class="sprite pinterest-social"></span></a></li>
            </ul>
        </section>
		<section id="quick-search" class="cf">
		    <?php
	        if( $main_categories = $this->get('main_categories') ){
                $first_cat = reset($main_categories);
	        ?>
			<span class="sprite search-icon-header"></span>
			<input type="text" name="term" value="" placeholder="Cauta in ..." />
			<a id="go-quick-search" class="button normal" href="#" title="Cauta" data-url="<?php echo $first_cat['link']?>"><?php echo tr('Cauta')?></a>
			<p id="quick-search-category">
				<strong><?php echo $first_cat['name']?></strong>
				<span class="remove" style="display:none"><span class="sprite close-icon-header"></span></span>
				<span class="triangle" style="display:block"></span>
			</p>
			<div id="quick-search-categories">
				<ul class="nav">
					<?php
					foreach (
					   $main_categories as
					   $k=>$s
					){
                       if($s['is_search']){
					?>
					<li data-name="<?php echo $s['name']?>" data-url="<?php echo $s['link']?>"><?php echo $s['name']?></li>
					<?php
					   }
					}
					?>
				</ul>
			</div>
			<?php
			}
			?>
		</section>
        <?php
        if( isset($pages['header']) && $pages['header'] ){
        ?>
        <section id="quick-nav">
            <div class="navbar cf">
                <ul class="nav">
                <?php
                foreach ( $pages['header'] as $k=>$p ){
                ?>
                	<li><a class="<?php echo $p['class']?>" href="<?php echo $p['link']?>" title="<?php echo $p['name']?>"><?php echo mb_strtolower($p['name'])?><span><span class="sprite <?php echo $p['class']?>-nav-header"></span></span></a></li>
            	<?php } ?>
                </ul>
            </div>
        </section>
        <?php } ?>
        <?php
        if( $main_categories2 = $this->get('main_categories2') ){
        ?>
        <section id="category-nav">
            <div class="navbar cf">
            	<ul class="nav">
            	<?php
            	$array_colors = array(1=>'orange',2=>'blue',3=>'lightgreen',4=>'violet');
                foreach ( $main_categories2 as $k=>$p ){
                ?>
            		<li class="trip"><a class="<?php echo $array_colors[$k]?>-background" href="<?php echo $p['link']?>" title="<?php echo $p['name']?>"><?php echo $p['name']?><span class="sprite <?php echo $p['class']?>-category-header"></span><span class="triangle"><span class="triangle <?php echo $array_colors[$k]?>-border"></span></span></a></li>
        		<?php } ?>
            	</ul>
        	</div>
        </section>
        <?php } ?>
    </div>
</header>