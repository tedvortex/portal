<?php $this->extend('layout')?>

<section id="order-sejururi">
	<h1>Sejur: <?php echo $item['name']  . ' / ' . $item['duration'] ?> nopti</h1>
	<ul id="offer-management" class="nav cf">
		<li><span class="sprite transport"></span><strong>Transport:</strong> 
		<?php foreach ($pricing['method'] as $transport2) {?>
    		<?php echo $transport2['name'] ?>
		<?php }?>
		cu plecare din <?php echo implode(', ', $transport) ?> 
		si sosire in <?php echo $airport_return?></li>
		<li><span class="sprite date"></span><strong>Perioada:</strong> <?php echo DateTime::createFromFormat('Y-m-d', $item['date_start'])->format('d-m-Y') . ' - ' . DateTime::createFromFormat('Y-m-d', $item['date_end'])->format('d-m-Y') ?></li>
	</ul>

	<?php if($correct_combination){ ?>
	<form method="post" action="<?php echo $booking['link'].$get_url?>">
		<input type="hidden" name="sejur_reservation" value="1" />
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<li class="a"><p>Rezervarea ta</p></li>
			</ul>
			<div class="product-box">
				<ul id="prices-tabs" class="nav">
				    <li class="a">
						<div class="tab-container">
							<div class="blue-price paleblue-background cf">
								<ul>
									<li>Sejur: <?php echo $item['name']?></li>
									<li>Data rezervarii: <?php echo date('d-m-Y')?></li>
									<li>Data cazare: <?php echo DateTime::createFromFormat('Y-m-d', $_GET['checkin'])->format('d-m-Y')?></li>
									<li>Durata sejur: <?php echo $cost['nights']?> nopti</li>
									<li>Plecare din: <?php echo $transport[$_GET['transport']]?> </li>
									<li>Intoarcerea din: <?php echo $airport_return?></li>
									<li>Hotel: <?php echo $hotel_details['name']?></li>
									<li>Masa: <?php echo $cost['board_type_name']?></li>
									<li>Camera: <?php echo $cost['room_type_name']?></li>
									<li>Turisti: <?php echo $_GET['adults'].' '.($_GET['adults'] == 1 ? 'adult' : 'adulti').( $_GET['children'] > 0 ? ' si '.$_GET['children'].' copii ('.$children_ages_string.')' : '')?></li>
								</ul>
								<div class="right-offer-right">
									<p>Pret sejur: <?php echo $cost['price_intro']?><span><?php echo $cost['total']?></span> <?php echo $item['token']?></p>
									<?php
									if ($transport_tax_dus) {
										if ($taxa_adult_dus>0) {
											echo '<p>Taxe aeroport adult (DUS): '.$taxa_adult_dus.' Euro</p>';
										}
										if ($taxa_children_dus>0) {
											echo '<p>Taxe aeroport copii (DUS): '.$taxa_children_dus.' Euro</p>';
										}
									}

									if ($transport_tax_intors) {
										if ($taxa_adult_intors>0) {
											echo '<p>Taxe aeroport adult (INTORS): '.$taxa_adult_intors.' Euro</p>';
										}
										if ($taxa_children_intors>0) {
											echo '<p>Taxe aeroport copii (INTORS): '.$taxa_children_intors.' Euro</p>';
										}
									} ?>

									<p>Total: <span><?php echo $cost['total_order'] . ' Euro'?></span></p>
									<p>Tip confirmare: <?php echo $cost['confirmation_type_name'] ?></p>
								</div>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="adults-children-tabs">
				<li class="a"><p>Detalii turisti</p></li>
			</ul>
			<div class="product-box">
				<ul id="adults-children-tabs" class="nav">
				    <li class="a">
				    	<div id="checkin-date" class="n" data-checkin="<?php echo $_GET['checkin']?>"></div>
						<?php echo $tourists_form?>
				    </li>
				</ul>
				<p class="info-box"><strong>Important</strong>: Nu va putem garanta rezervarea baby cot-ului (in cazul copiilor mai mici de 2 ani) sau a unui pat suplimentar (pentru copiii mai mari de 2 ani). Va rugam sa precizati in campul "Alte detalii (Optional)" - mai jos - daca doriti baby cot sau pat suplimentar. Unul dintre consultantii nostri va verifica daca rezervarea acestor servicii este posibila.</p>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="customer-form">
				<li class="a"><p>Detalii facturare</p></li>
			</ul>
			<div class="product-box">
				<ul id="customer-form" class="nav">
				    <li class="a">
				    	<div class="order-form">
							<div id="person-type">
								<input id="person-type-0" type="radio" name="person_type" value="0" required="required" <?php echo (!isset($_POST['person_type']) || !in_array($_POST['person_type'], array(0,1)) || $_POST['person_type'] == 0 ? 'checked="checked"' : '')?>/><label for="person-type-0">Persoana fizica</label>
								<input id="person-type-1" type="radio" name="person_type" value="1" required="required" <?php echo (isset($_POST['person_type']) && $_POST['person_type'] == 1 ? 'checked="checked"' : '')?>/><label for="person-type-1">Persoana juridica (companie)</label>
							</div>
							<div id="person-type-container">
								<?php if(!isset($_POST['person_type']) || !in_array($_POST['person_type'], array(0,1)) || $_POST['person_type'] == 0){ ?>
								<div id="form-individual" class="form-person-type cf"><?php echo $form_individual?></div>
								<?php } elseif(isset($_POST['person_type']) && $_POST['person_type'] == 1 ){?>
								<div id="form-company" class="form-person-type cf"><?php echo $form_company?></div>
								<?php } ?>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<strong>Modalitati de plata</strong>
			<ul id="unfolded-offer-tabs" class="form-tabs-list tabs-list tabs-list-big nav cf" data-tabs="payment-tabs">
				<li <?php echo ((isset($_POST['payment_method']) && $_POST['payment_method'] == 0) || !isset($_POST['payment_method']) || !in_array($_POST['payment_method'], range(0,2)) ? 'class="a"' : '')?> data-tab="payment-bank">
					<input class="n" type="radio" name="payment_method" value="0" checked="checked" />
					<p>Banca (cash/depunere)</p>
				</li>
				<li <?php echo (isset($_POST['payment_method']) && $_POST['payment_method'] == 1 ? 'class="a"' : '')?> data-tab="payment-store">
					<input class="n" type="radio" name="payment_method" value="1" />
					<p>Cash la sediul Click&Go</p>
				</li>
				<li <?php echo (isset($_POST['payment_method']) && $_POST['payment_method'] == 2 ? 'class="a"' : '')?> data-tab="payment-card">
					<input class="n" type="radio" name="payment_method" value="2" />
					<p>Card bancar</p>
				</li>
			</ul>
			<div class="product-box">
				<div id="payment-tabs" class="order-form">
					<?php if( isset($_POST['payment_method']) && $_POST['payment_method'] == 2 ){ ?>
					<div id="payment-card">
						<p>Introduceti datele cardului dvs.:</p>
					</div>
					<?php } elseif( isset($_POST['payment_method']) && $_POST['payment_method'] == 1 ){ ?>
					<div id="payment-store">
						<p>Plata poate fi facuta cash la sediul Click&Go din Bdul. Magheru 12-14, sc. B, et. 5, ap. 27, sect. 1, Bucuresti</p>
					</div>
					<?php } else { ?>
					<div id="payment-bank">
						<p>Plata se va putea face prin transfer bancar sau in numerar la orice filiala Banca Transilvania, Millenium Bank sau BRD din tara.</p>
						<p>Veti primi detalii complete despre modalitatile de plata in emailul de confirmare a rezervarii. </p>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="details-form">
				<li class="a"><p>Alte detalii (optional)</p></li>
			</ul>
			<div class="product-box">
				<ul id="details-form" class="nav">
				    <li class="a">
				    	<div class="order-form">
							<?php echo $form_details?>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<li class="a"><p>Termeni si conditii</p></li>
			</ul>
			<div class="product-box">
				<ul id="prices-tabs" class="nav">
				    <li class="a">
						<div class="tab-container">
							<div class="politics">
								<span class="sprite"></span>
								<h3>Termeni si conditii</h3>
								<p><?php echo $item['terms']?></p>
							</div>
							<div class="politics">
								<span class="sprite"></span>
								<h3>Politica de anulare</h3>
								<p><?php echo $item['policy']?></p>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<div id="termsandconditions">
			<input type="checkbox" class="n" name="accept_terms" id="accept_terms" value="1" <?php echo (isset($_POST['accept_terms']) && $_POST['accept_terms'] == 1 ? 'checked="checked"' : '')?> />
			<p><span class="term-checkbox"><span class="<?php echo (isset($_POST['accept_terms']) && $_POST['accept_terms'] == 1 ? 'active' : 'normal')?>"></span></span>Sunt de acord cu <a href="#" title="#">Termenii si conditiile</a> si cu <a href="#" title="#">Politica de anulare</a></p>
		</div>
		<div id="button-container" class="cf">
			<button type="submit"></button>
			<a class="button checked form-submitter" href="#" title="#">Rezerva <span class="sprite white-check"></span><span class="edge arrow"></span></a>
		</div>
	</form>
	<?php } ?>
</section>

<?php if($correct_combination){ ?>
<div id="order-forms-storage" class="n">
	<?php if(isset($_POST['person_type']) && $_POST['person_type'] == 1){ ?>
	<div id="form-individual" class="form-person-type cf"><?php echo $form_individual?></div>
	<?php } else { ?>
	<div id="form-company" class="form-person-type cf"><?php echo $form_company?></div>
	<?php } ?>
	<?php if(isset($_POST['payment_method']) && $_POST['payment_method'] != 0){ ?>
	<div id="payment-bank">
		<p>Plata se va putea face prin transfer bancar sau in numerar la orice filiala Banca Transilvania, Millenium Bank sau BRD din tara.</p>
		<p>Veti primi detalii complete despre modalitatile de plata in emailul de confirmare a rezervarii. </p>
	</div>
	<?php } if(!isset($_POST['payment_method']) || $_POST['payment_method'] != 1 ){ ?>
	<div id="payment-store">
		<p>Plata poate fi facuta cash la sediul Click&Go din Bdul. Magheru 12-14, sc. B, et. 5, ap. 27, sect. 1, Bucuresti</p>
	</div>
	<?php } if(!isset($_POST['payment_method']) || $_POST['payment_method'] != 2 ){ ?>
	<div id="payment-card">
		<p>Introduceti datele cardului dvs.:</p>
	</div>
	<?php } ?>
</div>
<?php } else{ ?>
<div class="alert-box">
	<strong>Ne pare rau, a aparut o eroare</strong>
	<div>
		<p>Datele selectate de dvs. nu se incadreaza in nicio oferta disponibila in acest moment pe site.</p>
		<p>Pentru tipul de rezervare ales, va rugam sa ne sunati la unul dintre urmatoarele numere de telefon: <?php echo $this->get('phone').', '.$this->get('phone2').' sau '.$this->get('phone3')?></p>
	</div>
</div>
<?php } ?>