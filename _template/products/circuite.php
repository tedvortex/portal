<?php $this->extend('layout') ?>
<section id="main-products-container" class="<?php echo $class ?>">
    <?php if ($products) {?>
	<div id="search-result">
		<p><?php echo $search_heading?>A<?php echo ($total > 1 ? 'u' : '') ?> fost gasit<?php echo ($total > 1 ? 'e ' : ' ') . $total . ' ' . ($total > 1 ? 'circuite' : 'circuit') ?></p>
	</div>
	<?php }?>

	<?php if ($pagination) {?>
	<div class="pagenav-container cf">
		
		<?php
		if($filter_sorts_links){
            foreach($filter_sorts_links as $filter){
        ?>
        <div class="pagenav category-select cf">
			<div class="custom-select sorting">
				<div class="value">
					<p><?php echo $filter['val']?></p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
					    <?php foreach($filter['options'] as $o){?>
						<li><a href="<?php echo $o['link']?>" title="<?php echo $o['name']?>"><?php echo $o['name']?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
        <?php
            }
        } 
        
        echo $pagination;
        ?>
	</div>
	<?php }?>

	<?php if ($products) {?>
	<ul id="main-products" class="nav">
		<?php foreach ($products as $k => $p) {?>
		<li>
			<div class="offer-container cf">
				<img class="image-holder" src="<?php echo image_uri($p['image']) ?>" alt=""/>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2><a href="<?php echo $p['link'] ?>" title="Click pentru a alege oferta"><?php echo $p['name']?></a></h2>
						<p><?php echo $p['itinerariu']?></p>
					</div>
					<div class="right-offer-right">
						<p><?php echo $p['offers'] . ' ofert' . ($p['offers'] != 1 ? 'e' : 'a') ?> incepand de la <br /> <span><?php echo $p['price'] ?> EUR</span></p>
						
						<a href="<?php echo $p['link'] ?>" title="Click pentru a alege oferta" class="right-offer-right2">Vezi oferta</a>
					</div>

					<div class="right-offer-center">
					    
					</div>
					<div class="right-offer-bottom"><?php echo ($p['short_description']!=''?$p['short_description']:limit_string2(html_entity_decode($description),150)) ?></div>
				</div>
			</div>
			<?php if ($p['cost']) {?>
			<ul class="products-table housing hotel" data-open="false">
				<li>
					<ul class="products-table-list cf">
						<li class="col-1">
							<p><span class="sprite dogtag"></span><?php echo $p['cost']['roomName'] ?><br /> <?php echo $p['cost']['hotelEntity'] ?></p>
						</li>
						<li class="col-2"><p><?php echo "{$p['cost']['boardName']} ({$p['cost']['id_board_type']})" ?></p></li>
						<li class="col-3"><p><?php echo $p['price'] . ' EUR' ?></p></li>
						<li class="col-4"><p><?php echo $p['cost']['confirmationName'] ?></p></li>
						<li class="col-5"><a class="button checked" href="<?php echo $p['link'] ?>" title="Rezerva"><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a></li>
					</ul>
				</li>
			</ul>
            <?php }?>
		</li>
		<?php }?>
	</ul>
	<?php } else {
	    echo '<p class="no-products" style="padding:25px 20px 0;"><strong>Va rugam folositi caseta laterala pentru a efectua o cautare !</strong></p>';

	    if (isset($not_found) && $not_found) {
	?>
		<div id="choice-container">
			<p class="with-underline">Ati ales:</p>
			<p>Cazare in <?php echo $query['city_name'] ?></p>
			<p>Turisti: <?php echo $_GET['adults'] ?> Adulti + <?php echo $_GET['children'] ?> chd</p>
			<p>Data de plecare: <?php echo $_GET['date'] ?>, Durata: <?php echo $_GET['nights'] ?> nopti</p>
			<strong>Pentru perioada selectata oferta nu mai este valabila</strong>
			<p>Daca sunteti prea ocupati, putem extinde noi cautarea dumneavoastra <br />Completati formularul de mai jos si vom raspunde tuturor solicitarilor indifrerent de complexitatea acestora. In cel mai scurt timp primiti cea mai buna varianta posibila!</p>
			<h2>Cerere de oferta personalizata</h2>
			<form id="action" action="" method="post">
				<input type="hidden" name="validation" value="1" />
				<div id="form-container"><?php echo $form ?></div>
				<div id="choice-button-container" class="buttons cf">
					<button type="submit" class="n"></button>
					<a href="#" title="" class="button checked form-submitter"><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a>
				</div>
			</form>
		</div>
	    <?php }
	}?>

	<?php if ($pagination) {?>
	<div class="pagenav-container with-border cf">
		<div class="pagenav category-select cf">
			<span class="sortby">Sorteaza dupa</span>
			<div class="custom-select">
				<div class="value">
					<input type="hidden" id="sort-by" name="sort-by" value="0" />
					<p>Pret crescator</p>
					<span class="arrow black"></span>
				</div>
				<div style="height: 0px;" class="select-container">
					<ul class="cf">
						<li data-value="0" data-name="Pret crescator">Pret crescator</li>
						<li data-value="1" data-name="Pret descrescator">Pret descrescator</li>
					</ul>
				</div>
			</div>
		</div>
		<?php echo $pagination ?>
	</div>
	<?php }?>
</section>