<?php $this->extend('layout')?>


<div id="item-sejururi">
	<h1>Croaziera: <?php echo $item['name']  . ' / ' . $item['max_stay'] ?> nopti</h1>
	<ul id="offer-management" class="nav cf">
		<li><span class="sprite transport"></span>
		<strong>Transport:</strong> 
		<?php foreach ($pricing['method'] as $transport2) {?>
    		<?php echo $transport2['name'] ?>
		<?php }?>
		cu plecare din <?php echo implode(', ', $transport) ?> </li>
		<li><span class="sprite date"></span><strong>Perioada:</strong> <?php echo DateTime::createFromFormat('Y-m-d', $item['date_start'])->format('d-m-Y') . ' - ' . DateTime::createFromFormat('Y-m-d', $item['date_end'])->format('d-m-Y') ?></li>
	</ul>
	<div class="offer-container cf">
	    <?php
	    if($hotel['images']){
	        foreach ($hotel['images'] as $k => $i){
		?>
	    <a class="image-holder lightbox<?php echo ($k > 0 ? ' n' : '') ?>" href="<?php echo _static . 'i/hotel-fibula/' . $i['image'] ?>" data-rel="group" title="<?php echo $i['name'] ?>">
	        <img src="<?php echo _static.'i/hotel-fibula/130/' . $i['image'] ?>" alt="<?php echo $i['name'] ?>" />
	    </a>
	    <?php
			}
	    }
	    elseif($hotel['images']){
	    	foreach ($hotel['images'] as $k => $i){
	    ?>
	    <a class="image-holder lightbox<?php echo ($k > 0 ? ' n' : '') ?>" href="<?php echo _static . 'i/hotel-fibula/' . $i['image'] ?>" data-rel="group" title="<?php echo $i['name'] ?>">
	        <img src="<?php echo _static.'i/hotel-fibula/130/' . $i['image'] ?>" alt="<?php echo $i['name'] ?>" />
	    </a>
	    <?php
	    	}
	    }
	    ?>
		<div class="right-offer cf">
			<div class="right-offer-left"  style="width:432px;">
				<p><?php echo $itinerariu?></p>
			</div>
			<div class="right-offer-right">

				<p><?php echo (isset($item['offers'])?($item['offers'] . ' ofert' . ($item['offers'] > 1 ? 'e' : 'a')):'') ?> incepand de la <br /> <span><?php echo (isset($item['offers'])?$item['price'] . ' ' . $item['token']:'') ?></span> <br/>* loc in camera dubla</p>

			</div>
			<div class="right-offer-center">

			</div>
			<div class="right-offer-bottom"><?php echo ($item['short_description']!=''?$item['short_description']:limit_string2(html_entity_decode(nl2br($hotel['description'])),300)) ?></div>
		</div>
	</div>

	<article id="page">
			<?php echo str_replace(array('../static/','class="group"'),array(_static,'class="lightbox"'),$item['description']);?>
	</article>
	
	
	<section id="product-tabs-container2">
		<ul class="tabs-list tabs-list-big nav cf" data-tabs="product-tabs2">
			<?php
			if( $tabs['header2'] ){
				foreach($tabs['header2'] as $k => $tab){
			?>
			<li <?php echo ($k == 0 ? 'class="a"' : '')?>><p><?php echo $tab['name']?></p></li>
			<?php
				}
			}
			?>
			
		</ul>
		<div class="product-box">
			<ul id="product-tabs2" class="nav">
			    <?php
				if( $tabs['header2'] ){
					foreach($tabs['header2'] as $k => $tab){
				?>
			    <li class="<?php echo ($k == 0 ? 'a' : 'n')?>">
					<div class="page">
						<?php echo str_replace(array('../static/','class="group"'),array(_static,'class="lightbox"'),$tab['description']);?>
					</div>
			    </li>
			    <?php
					}
				}
				?>
			</ul>
		</div>
	</section>

	<section id="product-tabs-container">
		<ul class="tabs-list tabs-list-big nav cf" data-tabs="product-tabs">
			<?php
			if( $tabs['header'] ){
				foreach($tabs['header'] as $k => $tab){
			?>
			<li <?php echo ($k == 0 ? 'class="a"' : '')?>><p><?php echo $tab['name']?></p></li>
			<?php
				}
			}

			if( $item['images'] || $hotel['images'] ){
			?>
			<li <?php echo (!$tabs['header'] ? 'class="a" ' : '')?>><p>Galerie foto</p></li>
			<?php
			}
			?>
		</ul>
		<div class="product-box">
			<ul id="product-tabs" class="nav">
			    <?php
				if( $tabs['header'] ){
					foreach($tabs['header'] as $k => $tab){
				?>
			    <li class="<?php echo ($k == 0 ? 'a' : 'n')?>">
					<div class="page">
						<?php echo str_replace(array('../static/','class="group"'),array(_static,'class="lightbox"'),$tab['description']);?>
					</div>
			    </li>
			    <?php
					}
				}

				if ($item['images'] || $hotel['images'] ) {?>
			    <li class="<?php echo (!$tabs['header'] ? 'a' : 'n')?>">
			    	<ul id="photo-gallery" class="nav cf">
						<?php
					    if ($item['images']) {
					        foreach ($item['images'] as $k => $i){
						?>
						<li <?php echo ($k % 5 == 0 ? 'class="side"' : '') ?>>
						    <a class="lightbox" href="<?php echo _static . 'i/hotel-fibula/' . $i['image'] ?>" data-rel="gallery" title="<?php echo $i['name'] ?>">
						        <img src="<?php echo _static.'i/hotel-fibula/130/' . $i['image'] ?>" alt="<?php echo $i['name'] ?>" />
						    </a>
						</li>
					    <?php
							}
					    }

					    elseif ($hotel['images']) {
					    	foreach ($hotel['images'] as $k => $i) {?>
					    <li <?php echo ($k % 5 == 0 ? 'class="side"' : '') ?>>
						    <a class="lightbox" href="<?php echo _static . 'i/hotel-fibula/' . $i['image'] ?>" data-rel="gallery" title="<?php echo $i['name'] ?>">
						        <img src="<?php echo _static.'i/hotel-fibula/130/' . $i['image'] ?>" alt="<?php echo $i['name'] ?>" />
						    </a>
						</li>
					    <?php }
					    }?>
				    </ul>
			    </li>
			    <?php }?>
			</ul>
		</div>
	</section>

	<section id="product-internal-container">
		<section>
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<li class="a"><p>Tarifele</p></li>
			</ul>
			<form id="prices-select-form" method="get" action="">
    			<div id="tariffs-container" class="product-box cf">
    				<div class="fl">
    					<div id="prices-select" data-id="<?php echo $item['id'] ?>">
    					    <input type="hidden" name="checkin" id="checkin" value=""/>
    						<ul class="cf nav">
    							<li class="medium">
    								<label>Nr. nopti</label>
    								<select class="value" name="duration" id="duration">
    								    <?php foreach ($pricing['duration'] as $k => $duration) {?>
    								    <option value="<?php echo $duration['nights'] ?>"><?php echo $duration['nights'] ?></option>
    								    <?php }?>
    								</select>
    							</li>
    							<li class="medium2">
    								<label>Masa</label>
    								<select class="value" name="board" id="board">
    								 	<?php foreach ($pricing['boards'] as $k => $transport) {?>
    								    <option value="<?php echo $k ?>"><?php echo $transport ?></option>
    								    <?php }?>
    								</select>
    							</li>
    						</ul>
    						<ul class="cf nav">
    						    <li class="big">
    								<label>Transport</label>
    								<select class="value" name="methods" id="methods">
    								 	<?php foreach ($pricing['method'] as $transport) {?>
    								 	<option value="<?php echo $transport['name'] ?>"><?php echo $transport['name'] ?></option>
    								    <?php }?>
    								</select>
    							</li>
    						</ul>
    						<ul class="cf nav">
    						    <li class="big">
    								<label>Port imbarcare</label>
    								<select class="value" name="transport" id="transport">
    								 	<?php foreach ($pricing['transport'] as $transport) {?>
    								    <option value="<?php echo $transport['id'] ?>"><?php echo $transport['name']  ?></option>
    								    <?php }?>
    								</select>
    							</li>
    						</ul>
    	 					<ul class="cf nav">
    							<li class="big">
    								<label>Tip camera</label>
    								<select class="value" name="room" id="room">
    								 	<?php foreach ($pricing['rooms'] as $k => $r) {?>
    								    <option data-adults="<?php echo $r['max_adults'] ?>" value="<?php echo $r['id_room_type'] ?>"><?php echo $r['name'] ?></option>
    								    <?php }?>
    								</select>
    							</li>
    						</ul>
    						<ul class="cf small nav">
    							<li>
    								<label>Nr adulti</label>
    								<select class="value" name="adults" id="adults">
    								    <option value="1">1</option>
    								    <option value="2" selected="selected">2</option>
    								    <option value="3">3</option>
    								    <option value="4">4</option>
    								</select>
    							</li>
    							<li>
    								<label>Nr copii</label>
    								<select class="value" name="children" id="children">
    								 	<option value="0">0</option>
    								 	<option value="1">1</option>
    								 	<option value="2">2</option>
    								 	<option value="3">3</option>
    								 	<option value="4">4</option>
    								</select>
    							</li>
    						</ul>
    					</div>
    					<div id="prices-chart-container"></div>
    				</div>
    				<div id="price-intervals">
    				    <?php
    				    $active = 0;

    				    if (isset($pricing['intervals'])) {
    				        foreach ($pricing['intervals'] as $k => $interval) {
    	                        if ($interval['value'] == date('Y-m')) {
    	                            $active = $k;
    	                        }

    				            echo '<input ' . ($active == $k ? 'class="active"' : '') . ' type="hidden" name="intervals[]" value="' . $interval['value'] . '" data-index="' . $k . '" data-locale="' . $interval['locale'] . '" />';
    				        }
    				    } ?>
    				</div>
    				<div class="fr" id="prices-calendar-container">
    					<div id="prices-calendar-controller">
    						<span class="sprite left"></span>
    						<span class="sprite right"></span>
    						<?php if (isset($pricing['intervals'][$active])) {
    	                        echo '<strong class="active" data-index="' . $active . '">' . $pricing['intervals'][$active]['locale'] . '</strong>';
    	    			    } ?>
    					</div>
    					<table id="prices-calendar">
    						<tr>
    							<th><div>Lu.</div></th>
    							<th><div>Ma.</div></th>
    							<th><div>Mi.</div></th>
    							<th><div>Jo.</div></th>
    							<th><div>Vi.</div></th>
    							<th><div>Sa.</div></th>
    							<th><div>Du.</div></th>
    						</tr>
    						<?php for($i = 0; $i < 5; $i++) {?>
    						<tr>
    							<?php for ($j = 0; $j < 7; $j++) {?>
    							<td><div></div></td>
    							<?php }?>
    						</tr>
    						<?php } ?>
    					</table>

    					<p>* Preturile sunt exprimate in Euro / Camera</p>
    					<div id="prices-calendar-buttons" class="cf">
    						<div class="fl"></div>
    						<div class="fr">
    							<a title="#" href="#" class="button checked">Rezerva <span class="sprite white-check"></span><span class="edge arrow"></span></a>
    						</div>
    					</div>
    				</div>
    				<div class="c"></div>
    				<div id="item-order-container">
        			    <a data-url="<?php echo $item['link'] . '/rezerva' ?>" class="button filter n" href="#" title="Rezerva">Rezerva</a>
        			</div>
    			</div>
			</form>
		</section>

		<?php if($tabs['footer']){ ?>
		<section>
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<?php foreach($tabs['footer'] as $k => $tab){ ?>
				<li <?php echo ($k == 0 ? 'class="a"' : '')?>><p><?php echo $tab['name']?></p></li>
				<?php
				}
				?>
			</ul>
			<div class="product-box">
				<ul id="prices-tabs" class="nav">
					<?php foreach($tabs['footer'] as $k => $tab){ ?>
				    <li class="<?php echo ($k == 0 ? 'a' : 'n')?>">
						<div class="page">
							<?php echo $tab['description']?>
						</div>
				    </li>
				    <?php
					}
					?>
				</ul>
			</div>
		</section>
		<?php } ?>
	</section>
</div>