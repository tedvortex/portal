<?php $this->extend('layout')?>

<?php if( $error['set'] ){ ?>
<div class="alert-box">
	<strong>Ne pare rau, a aparut o eroare</strong>
	<div>
		<p>Stimate(a) <?php echo $confirm['holder']?>,</p>
		<p>Confirmarea rezervarii dvs. cu numarul <span class="red-color"><?php echo $confirm['no_order']?></span> nu poate fi finalizata.</p>
		<p><?php echo $error['message']?></p>
		<p>Numere de telefon la care putem fi contactati: <?php echo $this->get('phone').', '.$this->get('phone2').' sau '.$this->get('phone3')?></p>
	</div>
</div>
<?php } else { ?>
<div class="alert-box">
	<strong class="blue-background">Confirmare pret [<?php echo $confirm['hotel_name']?>]</strong>
	<div>
		<p>Stimate(a) <?php echo $confirm['holder']?>,</p>
		<p>Este posibil ca pretul rezervarii dvs. cu numarul <span class="red-color"><?php echo $confirm['no_order']?></span> sa difere de cel mentionat la pasul anterior. Acest lucru se datoreaza politicii de rezervare a furnizorului in cauza.</p>
		<p>Daca doriti sa continuati cu rezervarea va rugam sa confirmati acceptarea schimbarii pretului. Va informam ca <span class="red-color">daca in decurs de 10 minute nu confirmati noul pret, <span class="white-color red-background">rezervarea dvs. cu numarul <?php echo $confirm['no_order']?> va fi anulata automat ! </span></span></p>
		<br />
		<p>Daca ati ales <span class="red-color">sa platiti cu cardul</span> si transferul s-a efectuat cu succes, va aducem la cunostinta urmatoarele: </p>
		<p>1. In cazul in care nu doriti sa confirmati noul pret al rezervarii, banii dvs. vor fi restituiti in totalitate intr-un timp cat mai scurt posibil.</p>
		<p>2. Daca doriti sa confirmati noul pret trebuie sa acceptati ca in cazul in care pretul nou stabilit este mai mare decat cel anterior o sa vi se emita o noua factura cu diferenta de bani dintre cele doua sume, pe care dvs. sunteti obligat(a) sa o achitati. In acest sens unul dintre operatorii nostri va va contacta intr-un timp cat mai scurt posibil.</p>
		<p>3. Daca noul pret stabilit pentru rezervarea dvs. este mai mic decat cel initial si dvs. doriti sa confirmati rezervarea, va vom inapoia diferenta de bani dintre cele doua sume.</p>
		<br />
		<p>Pretul initial al rezervarii a fost <?php echo $confirm['totalAvailabilityOmega'].' '.$confirm['token_omega']?></p>
		<p>Pretul total in acest moment al rezervarii cu numarul <?php echo $confirm['no_order']?> este <span class="blue-color" style="font-size:20px"><?php echo $confirm['total'].' '.$confirm['token_omega']?></span> si trebuie reconfirmat de catre dvs.:</p>
		<br />
		<form method="post">
			<div class="form">
				<input type="hidden" name="validation" value="1" />
				<p><input id="confirm-checkbox" type="checkbox" name="confirm" value="1" required="required" /><label for="confirm-checkbox">Confirm ca sunt de acord cu pretul final de <?php echo $confirm['total'].' '.$confirm['token_omega']?> pentru rezervarea <?php echo $confirm['no_order']?> si declar ca am luat la cunostinta si sunt de acord cu toate cele mentionate mai sus!</label></p>
				<button type="submit"></button>
				<a class="button white-color blue-background form-submitter" title="Confirma pret">Confirma pret</a>	
			</div>
		</form>
		
	</div>
</div>
<?php } ?>