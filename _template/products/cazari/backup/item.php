<?php $this->extend('layout') ?>

<section id="main-products-container" class="<?php echo $class?>">
	<ul id="main-products" class="nav">
		<li>
			<div class="offer-container cf">
				<a class="lightbox images-holder" data-rel="group" href="#" title="#">
					<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
					<span>Vizualizeaza galerie</span>
				</a>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2>Rixos Premium</h2>
						<p>Kemer Turcia</p>
					</div>
					<div class="right-offer-center">
						<ul class="stars">
							<?php
							for (
								$i=0;
								$i<5;
								$i++
							){
							?>
							<li class="sprite"></li>
							<?php
							}
							?>
						</ul>
						<a class="map-tag" href="#" title="#"><span class="sprite map-icon"></span>Harta</a>
					</div>
					<div class="right-offer-bottom">
						<div class="double-container cf">
							<div class="c-left">
								<p><span>Fax:</span> ++377-93 25 24 44</p>
								<p><span>E-mail:</span> metropole@metropole.mc</p>
								<p><span>Website:</span> http://www.metropole.com</p>
							</div>
							<div class="c-right">
								<p><span>Adress:</span> 4, AVENUE MADONEMONTE CARLOMONACO</p>
								<p><span>Postal code:</span> 98007</p>
								<p><span>Telephone:</span> ++377-3 15 15 15</p>

							</div>
						</div>
					</div>
				</div>
			</div>
			<p class="pdescription">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
			<ul class="products-table">
				<?php
				for ($j=0;$j<4;$j++){
				?>
				<li>
					<ul class="products-table-list with-color cf">
						<li class="col-1">
							<p>Room Type</p>
						</li>
						<li class="col-2">
							<p>Adults</p>
						</li>
						<li class="col-3">
							<p>Children</p>
						</li>
						<li class="col-5">
							<p>Included Meals</p>
						</li>
					</ul>
					<?php
					for ($i=0;$i<2;$i++){
					?>
					<ul class="products-table-list cf">
						<li class="col-1">
							<p><span class="sprite dogtag"></span>Family Room Standard Land View</p>
						</li>
						<li class="col-2">
							<p>2</p>
						</li>
						<li class="col-3">
							<p>1</p>
						</li>
						<li class="col-5">
							<p>Half Breakfast</p>
						</li>
					</ul>
					<?php } ?>
					<ul class="products-table-list grey-color price cf">
						<li class="col-1">
							<p>Pret: <span>12345 €</span></p>
						</li>
						<li class="col-5">
							<a title="#" href="#" class="button checked"><span class="sprite white-check"></span><span class="edge arrow"></span>Cumpara</a>
						</li>
					</ul>
				</li>
				<?php } ?>
			</ul>
		</li>
	</ul>

</section>