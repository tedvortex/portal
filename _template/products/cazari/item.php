<?php $this->extend('layout') ?>

<section id="main-products-container-item" class="<?php echo $class?>">
	<ul id="main-products" class="nav">
		<li>
			<div class="offer-container cf">
			    <?php if (isset($hotel['images']) && $hotel['images']) {
			        foreach ($hotel['images'] as $k => $i) {?>
			    <a class="lightbox images-holder<?php echo ($k ? ' n' : '') ?>" data-rel="group" href="<?php echo _static . 'i/hotel/' . $i['image'] ?>" title="<?php echo $i['name'] ?>">
					<img class="image-holder" src="<?php echo image_uri('i/hotel/' . $i['image'])?>" alt="<?php echo $i['name'] ?>" />
					<span>Vizualizeaza galerie</span>
				</a>
			    <?php }
			    } else {?>
				<a class="lightbox images-holder" data-rel="group" href="#" title="">
					<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
					<span>Vizualizeaza galerie</span>
				</a>
				<?php }?>
				<div class="right-offer cf">
					<div class="right-offer-left">
						<h2><?php echo $item['hotel_name']?></h2>
						<p><?php echo $item['city_name']?></p>
					</div>
					<div class="right-offer-center">
						<ul class="stars">
							<?php for ($i = 0; $i < $hotel['rating']; $i++) {?>
							<li class="sprite"></li>
							<?php }?>
						</ul>

						<?php if ($hotel['latitude'] > 0 && $hotel['longitude'] > 0) {?>
						<a class="map-tag lightbox" href="https://maps.google.com/maps?q=<?php echo $hotel['latitude'] . ',' . $hotel['longitude'] . '&amp;ll' . $hotel['latitude'] . ',' . $hotel['longitude'] . '&amp;lightbox[width]=610&lightbox[height]=360'?>" title=""><span class="sprite map-icon"></span>Harta</a>
						<?php }?>
					</div>
					<div class="right-offer-bottom">
						<div class="double-container cf">
						    <?php if ($hotel['fax']) {?>
							<p><span>Fax:</span> <?php echo $hotel['fax'] ?></p>
							<?php }?>

							<?php if ($hotel['telephone']) {?>
							<p><span>Telephone:</span> <?php echo $hotel['telephone'] ?></p>
							<?php }?>

							<?php if ($hotel['email']) {?>
							<p><span>E-mail:</span> <?php echo $hotel['email'] ?></p>
							<?php }?>

							<?php if ($hotel['website']) {?>
							<p><span>Website:</span> <?php echo $hotel['website'] ?></p>
							<?php }?>

							<?php if ($hotel['address']) {?>
							<p><span>Adress:</span> <?php echo $hotel['address'] ?></p>
							<?php }?>

							<?php if ($hotel['postal_code']) {?>
							<p><span>Postal code:</span> <?php echo $hotel['postal_code'] ?></p>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			<p class="pdescription"><?php echo $hotel['description'] ?></p>

			<?php if ($location) {?>
			<ul class="products-table hotel">
				<?php foreach ($location['rooms'] as $r) {
				    foreach ($r['boards'] as $board) {?>
				<li class="housing-item-list">
					<ul class="products-table-list with-color cf">
						<li class="col-1">
							<p>Tip camera</p>
						</li>
						<li class="col-2">
							<p>Adulti</p>
						</li>
						<li class="col-3">
							<p>Copii</p>
						</li>
						<li class="col-5">
							<p>Mese incluse</p>
						</li>
					</ul>

					<ul class="products-table-list cf">
						<li class="col-1">
							<p><span class="sprite dogtag"></span><?php echo $r['name'] ?></p>
						</li>
						<li class="col-2">
							<p><?php echo $r['adults'] ?></p>
						</li>
						<li class="col-3">
							<p><?php echo $r['children'] ?></p>
						</li>
						<li class="col-5">
							<p><?php echo $board['name'] ?></p>
						</li>
					</ul>

					<ul class="products-table-list grey-color price cf">
						<li class="col-1">
							<p>Pret: <span><?php echo commission($board['price']) . ' ' . $board['currency'] ?></span></p>
						</li>
						<li class="col-5">
							<a title="Rezerva" href="<?php echo $link . '/' . $r['id_search_room'] . '/' . $board['code'] ?>" class="button checked"><span class="sprite white-check"></span><span class="edge arrow"></span>Rezerva</a>
						</li>
					</ul>
				</li>
				    <?php }
				}?>
			</ul>
			<?php }?>
		</li>
	</ul>
</section>