<?php $this->extend('layout')?>

<div class="alert-box">
	<strong class="<?php echo $alert_box['heading_class']?>"><?php echo $alert_box['heading']?></strong>
	<div>
		<p>Stimate(a) <?php echo $view['holder']?>,</p>
		<?php foreach($alert_box['messages'] as $message){ ?>
		<p><?php echo $message?></p>
		<?php }?>
		<p>Numere de telefon la care putem fi contactati: <?php echo $this->get('phone').', '.$this->get('phone2').' sau '.$this->get('phone3')?></p>
	</div>
</div>