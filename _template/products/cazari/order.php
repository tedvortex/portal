<?php $this->extend('layout')?>

<section id="order-sejururi">
	<h1>Cazare: <?php echo mb_strtoupper($item['hotel_name'] . ' ' . $query['city_name']) . ' / ' .$query['nights'] ?> nopti</h1>
	<!--<?php if($item['short_description']){?>
	<p class="borders"><?php echo $item['short_description']?></p>
	<?php } ?>-->

	<?php if( ! $stop ){ ?>
	<form method="post" action="">
		<input type="hidden" name="sejur_reservation" value="1" />
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<li class="a"><p>Rezervarea ta</p></li>
			</ul>
			<div class="product-box">
				<ul id="prices-tabs" class="nav">
				    <li class="a">
						<div class="tab-container">
							<div class="blue-price paleblue-background cf">
								<ul>
									<li>Sejur: <?php echo $item['hotel_name']?></li>
									<li>Data rezervarii: <?php echo date('Y-m-d')?></li>
									<li>Data cazare: <?php echo $query['checkin']?></li>
									<li>Durata sejur: <?php echo $query['nights']?> nopti</li>
									<li>Hotel: <?php echo $item['hotel_name']?></li>
									<li>Masa: <?php echo $order_board['name']?></li>
									<li>Camera: <?php echo $room['name']?></li>
									<li>Turisti: <?php echo $query['adults'].' '.($query['adults'] == 1 ? 'adult' : 'adulti').( $query['children'] > 0 ? ' si '.$query['children'].' copii' : '')?></li>
								</ul>
								<div class="right-offer-right">
									<p>pret <span><?php echo commission($order_board['price']) . ' ' . $order_board['currency'] ?></span></p>
									<p>confirmare: imediata</p>
								</div>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="adults-children-tabs">
				<li class="a"><p>Detalii turisti</p></li>
			</ul>
			<div class="product-box">
				<ul id="adults-children-tabs" class="nav">
				    <li class="a">
				    	<div id="checkin-date" class="n" data-checkin="<?php echo $query['checkin']?>"></div>
						<?php echo $tourists_form?>
				    </li>
				</ul>
				<p class="info-box"><strong>Important</strong>: Nu va putem garanta rezervarea baby cot-ului (in cazul copiilor mai mici de 2 ani) sau a unui pat suplimentar (pentru copiii mai mari de 2 ani). Va rugam sa precizati in campul "Alte detalii (Optional)" - mai jos - daca doriti baby cot sau pat suplimentar. Unul dintre consultantii nostri va verifica daca rezervarea acestor servicii este posibila.</p>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="customer-form">
				<li class="a"><p>Detalii facturare</p></li>
			</ul>
			<div class="product-box">
				<ul id="customer-form" class="nav">
				    <li class="a">
				    	<div class="order-form">
							<div id="person-type">
								<input id="person-type-0" type="radio" name="person_type" value="0" required="required" <?php echo (!isset($_POST['person_type']) || !in_array($_POST['person_type'], array(0,1)) || $_POST['person_type'] == 0 ? 'checked="checked"' : '')?>/><label for="person-type-0">Persoana fizica</label>
								<input id="person-type-1" type="radio" name="person_type" value="1" required="required" <?php echo (isset($_POST['person_type']) && $_POST['person_type'] == 1 ? 'checked="checked"' : '')?>/><label for="person-type-1">Persoana juridica (companie)</label>
							</div>
							<div id="person-type-container">
								<?php if(!isset($_POST['person_type']) || !in_array($_POST['person_type'], array(0,1)) || $_POST['person_type'] == 0){ ?>
								<div id="form-individual" class="form-person-type cf"><?php echo $form_individual?></div>
								<?php } elseif(isset($_POST['person_type']) && $_POST['person_type'] == 1 ){?>
								<div id="form-company" class="form-person-type cf"><?php echo $form_company?></div>
								<?php } ?>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<strong>Modalitati de plata</strong>
			<ul id="unfolded-offer-tabs" class="form-tabs-list tabs-list tabs-list-big nav cf" data-tabs="payment-tabs">
				<?php if( ! $payment_only_card ){ ?>
				<li <?php echo ((isset($_POST['payment_method']) && $_POST['payment_method'] == 0) || !isset($_POST['payment_method']) || !in_array($_POST['payment_method'], range(0,2)) ? 'class="a"' : '')?> data-tab="payment-bank">
					<input class="n" type="radio" name="payment_method" value="0" checked="checked" />
					<p>Banca (  Virament / Depunere )</p>
				</li>
				<li <?php echo (isset($_POST['payment_method']) && $_POST['payment_method'] == 1 ? 'class="a"' : '')?> data-tab="payment-store">
					<input class="n" type="radio" name="payment_method" value="1" />
					<p>Cash la sediul Click&Go</p>
				</li>
				<?php } ?>
				<li <?php echo ( $payment_only_card || (isset($_POST['payment_method']) && $_POST['payment_method'] == 2) ? 'class="a"' : '')?> data-tab="payment-card">
					<input class="n" type="radio" name="payment_method" value="2" <?php echo ($payment_only_card ? 'checked="checked"' : '')?> />
					<p>Card bancar</p>
				</li>
			</ul>
			<div class="product-box">
				<div id="payment-tabs" class="order-form">
					<?php if( $payment_only_card || (isset($_POST['payment_method']) && $_POST['payment_method'] == 2) ){ ?>
					<div id="payment-card">
						<p>Dupa ce veti completa toate datele din aceasta pagina, veti fi redirectionat automat catre pagina de plata cu cardul.</p>
					</div>
					<?php } elseif( isset($_POST['payment_method']) && $_POST['payment_method'] == 1 ){ ?>
					<div id="payment-store">
						<p>Plata poate fi facuta cash la sediul Click&Go din Bdul. Magheru 12-14, sc. B, et. 5, ap. 27, sect. 1, Bucuresti</p>
					</div>
					<?php } else { ?>
					<div id="payment-bank">
						<p>Plata se va putea face prin transfer bancar sau in numerar la orice filiala BRD din tara.</p>
						<p>Veti primi detalii complete despre modalitatile de plata in emailul de confirmare a rezervarii. </p>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="details-form">
				<li class="a"><p>Alte detalii (optional)</p></li>
			</ul>
			<div class="product-box">
				<ul id="details-form" class="nav">
				    <li class="a">
				    	<div class="order-form">
							<?php echo $form_details?>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<section class="order-tab-container">
			<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
				<li class="a"><p>Termeni si conditii</p></li>
			</ul>
			<div class="product-box">
				<ul id="prices-tabs" class="nav">
				    <li class="a">
						<div class="tab-container">
							<div class="politics">
								<span class="sprite"></span>
								<h3>Politica de anulare</h3>
								<?php if ($order_board['cancellation']) {
								    foreach ($order_board['cancellation'] as $c) {
                                        echo '<p>Această rezervare nu se va mai putea anula decât prin plata următoarelor penalizări: ' . $c->Price . ' ' . $c->Currency . ' de la ' . strstr($c->FromDate, ' ', true) . ' pana la ' . strstr($c->ToDate, ' ', true) . '</p>';
	                                }
								} else {?>
								<p>Aceasta rezervare nu are politica de anulare definita !</p>
								<?php }?>
							</div>
						</div>
				    </li>
				</ul>
			</div>
		</section>
		<div id="termsandconditions">
			<input type="checkbox" class="n" name="accept_terms" id="accept_terms" value="1" <?php echo (isset($_POST['accept_terms']) && $_POST['accept_terms'] == 1 ? 'checked="checked"' : '')?> />
			<p><span class="term-checkbox"><span class="<?php echo (isset($_POST['accept_terms']) && $_POST['accept_terms'] == 1 ? 'active' : 'normal')?>"></span></span>Sunt de acord cu Termenii si conditiile si cu Politica de anulare</p>
						<p><span class="term-checkbox"><span class="<?php echo (isset($_POST['accept_terms2']) && $_POST['accept_terms2'] == 1 ? 'active' : 'normal')?>"></span></span>Sunt de acord cu conditiile <a href="/contractul-cadru" class="external">Contractului Cadru cu Turistul</a></p>
		</div>
		<div id="button-container" class="cf">
			<button type="submit"></button>
			<a class="button checked form-submitter" href="#" title="#">Rezerva <span class="sprite white-check"></span><span class="edge arrow"></span></a>
		</div>
	</form>
	<?php } ?>
</section>

<?php if( ! $stop ){ ?>
<div id="order-forms-storage" class="n">
	<?php if(isset($_POST['person_type']) && $_POST['person_type'] == 1){ ?>
	<div id="form-individual" class="form-person-type cf"><?php echo $form_individual?></div>
	<?php } else { ?>
	<div id="form-company" class="form-person-type cf"><?php echo $form_company?></div>
	<?php } ?>
	<?php if(isset($_POST['payment_method']) && $_POST['payment_method'] != 0){ ?>
	<div id="payment-bank">
		<p>Plata se va putea face prin transfer bancar sau in numerar la orice filiala Banca Transilvania, Millenium Bank sau BRD din tara.</p>
		<p>Veti primi detalii complete despre modalitatile de plata in emailul de confirmare a rezervarii. </p>
	</div>
	<?php } if(!isset($_POST['payment_method']) || $_POST['payment_method'] != 1 ){ ?>
	<div id="payment-store">
		<p>Plata poate fi facuta cash la sediul Click&Go din Bdul. Magheru 12-14, sc. B, et. 5, ap. 27, sect. 1, Bucuresti</p>
	</div>
	<?php } if(!isset($_POST['payment_method']) || $_POST['payment_method'] != 2 ){ ?>
	<div id="payment-card">
		<p>Introduceti datele cardului dvs.:</p>
	</div>
	<?php } ?>
</div>
<?php } else { ?>
<div class="alert-box">
	<strong>Ne pare rau, a aparut o eroare</strong>
	<div>
		<p>Datele selectate de dvs. nu se incadreaza in nicio oferta disponibila in acest moment pe site.</p>
		<p>Pentru tipul de rezervare ales, va rugam sa ne contactati la travel@clickandgo.ro sau la unul dintre numerele de telefon +40 21 317 2606, +40 21 317 2608 sau +40 21 318 8871.</p>
		<p>Va multumim!</p>
	</div>
</div>
<?php } ?>