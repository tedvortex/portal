<?php
header('Content-Type: text/html; charset=UTF-8',true);
header('Content-Language: '.$this->get('language'));
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php $this->output('language')?>" xml:lang="<?php $this->output('language')?>">
<?php echo $this->render( $this->get('head') ) ?>
<?php
$class = $this->get('class');
$array_classes = array('sejururi','cazari','croaziere','circuite');
$array_no_navigator = array('default');
?>

<body>
	<div id="wrapper">
	<?php
	if ($this->has('header')) {
		echo $this->render( $this->get('header') );
	}

	if ($this->has('content')) {?>
	<article id="content">
		<div class="container cf">
			<div class="span4" id="box">
			<?php
			if(
				$this->has('boxes')
			){
				echo $this->render( $this->get('boxes') );
			}
			?>
			<?php
			if(
				in_array($class,$array_classes)
			){
				if(
					$this->has('boxes_'.$class)
				){
					echo $this->render( $this->get('boxes_'.$class) );
				}
			}
			?>
			</div>
			<?php if(!in_array($this->get('constructor'), $array_no_navigator)){ ?>
			<section class="navigator span8"><?php $this->output('navigator')?></section>
			<?php } ?>
	        <div class="span8 main-template" id="template-<?php $this->output('template')?>"><?php $this->output('content')?></div>
        </div>
	</article>
	<?php } ?>

	<?php
	if ($this->has('footer')) {
		echo $this->render( $this->get('footer') );
	} ?>
	</div>

	<div id="scripts">
		<script data-main="<?php echo _static . 'scripts/' . $this->get('javascript')?>.js" src="<?php echo _static?>scripts/require-jquery.1.0.2.min.js"></script>
		<div id="window-message"><?php $this->output('window')?></div>
	</div>
</body>
</html>

