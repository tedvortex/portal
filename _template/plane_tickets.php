<?php $this->extend('layout') ?>
<div id="plane-tickets-container">
	<form action="<?php echo $link ?>" method="post">
	<input type="hidden" name="validation" value="1"/>
	<input type="hidden" name="act" value="avion"/>
	<div class="form-box cf">
		<div class="box-container">
			<div class="legend">
				<a class="other-option first" href="#" title="Bilete de avion"><strong>[<span>-</span>]</strong>Bilete de avion</a>
			</div>
				<?php echo $form_type ?>
				<div id="tabs-1" class="tabs">
					<?php echo $form_departure_from ?>
					<div class="cf">
						<?php echo $form_departure_date ?>
						<?php echo $form_return_date ?>
					</div>	
					<?php echo $form_departure_type ?>
					<div class="cf">
						<p class="heading">Optional</p>
						<?php echo $form_optionals ?> 
					</div>
					<!--<div class="cf">
						<p class="heading">Pasageri</p>
						<?php //echo $form_passengers_type ?> 
					</div>
					
					<div class="cf">
						<p class="heading">Informatii despre pasageri:</p>
						<p class="description">(aceste informatii permit efectuarea unei rezervari provizorii si nu implica nici un cost pentru dvs.)</p>
						<?php //echo $form_passengers_infos ?>
						<div class="form-passengers-info"></div>
						<div class="form-childrens-info"></div>
						<div class="form-infants-info"></div>
					</div>-->
					<div class="cf">
						<?php echo $form_flight_observations ?> 
					</div>
				</div>
				<div id="tabs-2" class="tabs n">
					<?php echo $form_departure_from_one_way ?>
					<div class="cf">
						<?php echo $form_departure_date_one_way ?>
					</div>	
					<?php echo $form_departure_type_one_way ?>
					<div class="cf">
						<p class="heading">Optional</p>
						<?php echo $form_optionals_one_way ?> 
					</div>
					<!--<div class="cf">
						<p class="heading">Pasageri</p>
						<?php //echo $form_passengers_type_one_way ?> 
					</div>
					<div class="cf">
						<p class="heading">Informatii despre pasageri:</p>
						<p class="description">(aceste informatii permit efectuarea unei rezervari provizorii si nu implica nici un cost pentru dvs.)</p>
						<?php //echo $form_passengers_infos_one_way ?>
						<div class="form-passengers-info-one-way"></div>
						<div class="form-childrens-info-one-way"></div>
						<div class="form-infants-info-one-way"></div>
					</div>-->
					<!--<div class="cf">
						<p class="heading">Date contact</p>
						<?php //echo $form_contact_one_way ?> 
					</div>-->
					<div class="cf">
						<div class="plane-observations inline textarea cf"><p><label for="flight-observations">Observatii</label><textarea class="normal" autocomplete="off" id="flight-observations2" name="flight-observations2"></textarea></p></div>
					</div>
				</div>
				<div id="tabs-3" class="tabs n">
					<?php echo $form_departure_from_multiple ?>
					<div class="cf">
						<?php echo $form_departure_date_multiple ?>
					</div>
					<div id="another-flight-0" class="another-flight n">
						<?php echo $form_departure_from_multiple_another ?>
						<div class="cf">
							<?php echo $form_departure_date_multiple_another ?>
						</div>
					</div>
					<div id="another-flight-1" class="another-flight n">
						<?php echo $form_departure_from_multiple_another_1 ?>
						<div class="cf">
							<?php echo $form_departure_date_multiple_another_1 ?>
						</div>
					</div>
					<div id="add-flight" class="cf">
						<a class="fl flight-add" href="#" title="Adauga un zbor"><strong class="add-flight"><span class="a"></span><span class="b"></span></strong>Adauga un zbor</a>
						<a class="fr flight-remove" href="#" title="Sterge un zbor"><span class="remove-flight"></span>Sterge un zbor</a>
					</div>
					<?php echo $form_departure_type_multiple ?>
					<div class="cf">
						<p class="heading">Optional</p>
						<?php echo $form_optionals_multiple ?> 
					</div>
					<!--<div class="cf">
						<p class="heading">Pasageri</p>
						<?php //echo $form_passengers_type_multiple ?> 
					</div>
					<div class="cf">
						<p class="heading">Informatii despre pasageri:</p>
						<p class="description">(aceste informatii permit efectuarea unei rezervari provizorii si nu implica nici un cost pentru dvs.)</p>
						<?php //echo $form_passengers_infos_multiple ?>
						<div class="form-passengers-info-multiple"></div>
						<div class="form-childrens-info-multiple"></div>
						<div class="form-infants-info-multiple"></div>
					</div>-->
					<!--<div class="cf">
						<p class="heading">Date contact</p>
						<?php //echo $form_contact_multiple ?> 
					</div>-->
					<div class="cf">
							<div class="plane-observations inline textarea cf">
								<p><label for="flight-observations">Observatii</label><textarea class="normal" autocomplete="off" id="flight-observations3" name="flight-observations3"></textarea></p>
							</div>
					</div>
				</div>
				
			<!--</form>-->
		</div>
	</div>
	<div class="other-services">
		<p class="heading">Informatii turisti</p>
	</div>
	<div class="form-box cf">
		<div class="box-container nospan">
		
			<div class="cf">
				<p class="heading"></p>
				<?php echo $form_passengers_type ?> 
			</div>
			
			<div class="cf">
				<p class="heading">Informatii despre turisti:</p>
				<p class="description">(aceste informatii permit efectuarea unei rezervari provizorii si nu implica nici un cost pentru dvs.)</p>
				<?php echo $form_passengers_infos ?>
				<div class="form-passengers-info"></div>
				<div class="form-childrens-info"></div>
				<div class="form-infants-info"></div>
			</div>

		</div>
		
	</div>
	<div class="other-services">
		<p class="heading">Adauga si alte servicii</p>
	</div>
	<div class="form-box closed cf">
<!--		<a class="button blue remove" href="#" title="trimite"><span class="edge arrow"></span>Elimina serviciul Cazare</a>-->
		<div id="check-in-container" class="box-container">
			<div class="legend"><a class="other-option" href="#" title="Asigurare"><strong>[<span>+</span>]</strong>Cazare</a></div>
			<!--<input type="hidden" name="validation" value="1"/>-->
			<!--<form action="<?php echo $link ?>" method="post">-->
				<?php echo $form_check_in ?>
				<div class="cf">
					<!--<p class="heading">Informatii despre calatori:</p>
					<p class="description">*preia datele</p>-->
					<?php echo $form_booking_observations ?>
				</div>
				<!--<div class="buttons">
					<button class="n" type="submit"></button>
					<a class="button form-submitter blue" href="#" title="trimite"><span class="edge arrow"></span>Confirma serviciul Cazare</a>
				</div>-->
			<!--</form>-->
		</div>
	</div>
	<div class="form-box closed cf">
<!--		<a class="button blue remove" href="#" title="trimite"><span class="edge arrow"></span>Elimina serviciul Transfer</a>-->
		<div id="transfer-container" class="box-container">
			<div class="legend"><a class="other-option" href="#" title="Asigurare"><strong>[<span>+</span>]</strong>Transfer</a></div>
			<!--<input type="hidden" name="validation" value="1"/>-->
			<!--<form action="<?php echo $link ?>" method="post">-->
				<?php echo $form_transfer ?>
				<?php echo $form_transfer_observations ?>
				<!--<div class="buttons">
					<button class="n" type="submit"></button>
					<a class="button form-submitter blue" href="#" title="trimite"><span class="edge arrow"></span>Confirma serviciul Transfer</a>
				</div>-->
			<!--</form>-->
		</div>
	</div>
	<div class="form-box closed cf">
<!--		<a class="button blue remove" href="#" title="trimite"><span class="edge arrow"></span>Elimina serviciul Rent A Car</a>-->
		<div id="rent-a-car-container" class="box-container">
			<div class="legend"><a class="other-option" href="#" title="Asigurare"><strong>[<span>+</span>]</strong>Rent A Car</a></div>
			<!--<input type="hidden" name="validation" value="1"/>-->
			<!--<form action="<?php echo $link ?>" method="post">-->
				<?php echo $form_rent_a_car ?>
				<?php echo $form_rent_a_car_observations ?>
				<!--<div class="buttons">
					<button class="n" type="submit"></button>
					<a class="button form-submitter blue" href="#" title="trimite"><span class="edge arrow"></span>Confirma serviciul Rent A Car</a>
				</div>-->
			<!--</form>-->
		</div>
	</div>
	<div class="form-box closed cf">
<!--		<a class="button blue remove" href="#" title="trimite"><span class="edge arrow"></span>Elimina serviciul Asigurare</a>-->
		<div id="insurance-container" class="box-container">
			<div class="legend"><a class="other-option" href="#" title="Asigurare"><strong>[<span>+</span>]</strong>Asigurare</a></div>
			<!--<input type="hidden" name="validation" value="1"/>-->
			<!--<form action="<?php echo $link ?>" method="post">-->
				<?php echo $form_insurance ?>
				<?php echo $form_insurance_observations ?>
				<!--<div class="buttons">
					<button class="n" type="submit"></button>
					<a class="button form-submitter blue" href="#" title="trimite"><span class="edge arrow"></span>Confirma serviciul Asigurare</a>
				</div>-->
			<!--</form>-->
		</div>
	</div>
	<div class="other-services">
		<p class="heading">Date de contact</p>
	</div>
	<div class="form-box cf">
		<div class="box-container nospan">
		
				<?php echo $form_contact_one_way ?>

		</div>
		<div class="buttons">
			<button class="n" type="submit"></button>
			<a class="button form-submitter blue" href="#" title="trimite"><span class="edge arrow"></span>Solicita oferta</a>
		</div>
	</div>

	</form>
</div>