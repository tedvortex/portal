<?php $pages = $this->get('pages')?>
<footer>
    <div class="container cf">
        <div class="span4">
            <section class="social-navigation navbar cf white-background" id="footer-social-navigation">
                <ul class="nav">
                    <li><a class="facebook-background" href="#" title="<?php echo tr('Facebook') ?>"><span class="sprite facebook-social"></span></a></li>
                    <li><a class="twitter-background" href="#" title="<?php echo tr('Twitter') ?>"><span class="sprite twitter-social"></span></a></li>
                    <li><a class="googleplus-background" href="#" title="<?php echo tr('Google+') ?>"><span class="sprite googleplus-social"></span></a></li>
                    <li><a class="pinterest-background" href="#" title="<?php echo tr('Pinterest') ?>"><span class="sprite pinterest-social"></span></a></li>
                </ul>
            </section>
        </div>
        <div class="span8">
            <div class="row">
           		<?php
		        if( isset($pages['footer']) && $pages['footer'] ){
		        ?>
                <div class="span2">
                    <h2><?php echo tr('Pagini') ?></h2>
                    <ul>
                    	<?php
		                foreach ( $pages['footer'] as $k=>$p ){
		                ?>
                    	<li><a href="<?php echo ($p['url']==''?$p['link']:$p['url'])?>" title="<?php echo $p['name']?>" <?php echo ($p['url']==''?'':'class="external"')?>><?php echo $p['name']?><span class="sprite check-footer"></span></a></li>
                    	<?php } ?>
<!--                    	<li><a href="http://www.anpc.gov.ro" title="ANPC" class="external">ANPC <span class="sprite check-footer"></span></a></li>-->
                    </ul>
                </div>
                <?php } ?>
                <?php
				if( $main_categories = $this->get('main_categories') ){
				?>
                <div class="span2">
                    <h2><?php echo tr('Oferte') ?></h2>
                    <ul>
                    <?php foreach ( $main_categories as $kc=>$c ){ ?>
                    	<li><a href="<?php echo $c['link'] ?>" title="<?php echo $c['name'] ?>"><?php echo $c['name'] ?><span class="sprite check-footer"></span></a></li>
                	 <?php } ?>
                    </ul>
                </div>
                 <?php } ?>
                <div class="span2 offset2">
                    <h2><?php echo tr('Contact') ?></h2>
                    <ul>
                    	<li><?php $this->output('address') ?><span class="sprite check-footer"></span></li>
                    	<li><?php $this->output('phone') ?><span class="sprite phone-footer"></span></li>
                    	<li><?php $this->output('phone2') ?><span class="sprite phone-footer"></span></li>
                    	<li><?php $this->output('phone3') ?><span class="sprite phone-footer"></span></li>
                    	<li><?php $this->output('email') ?><span class="sprite mail-footer"></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <p id="copyright"><?php echo tr('Agentia Mondial Voyages este inregistrata in Registrul de Evidenta a Prelucrarii de Date cu Caracter Personal sub numarul 29636.') ?></p>
		  <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  			ga('create', 'UA-44288173-1', 'clickandgo.ro');
  			ga('send', 'pageview');
		</script>
    </div>
</footer>