<?php $this->extend('layout')?>

<section class="steps">
	<h2 id="step4-heading"><span class="sprite"></span>Rezervarea Dvs. de hotel este confirmata</h2>
	<p class="hdetalis">Pagina de mai jos reprezinta o confirmare a rezervarii Dvs. Pe adresa de email veti primi confirmarea din partea furnizorului nostru de hoteluri. Va rugam sa printati email-ul si sa va prezentati cu el la hotel. De asemenea, va reamintim ca plata rezervarii se va face direct la hotel si nu catre Vola.ro, urmand ca factura fiscala sa fie emisa tot de catre hotel.</p>
	<div id="reservation">
		<div class="tab-container cf">
			<div id="your-reservation">
				Rezervarea ta
			</div>
			<ul class="reservation-list first-list cf">
				<li class="cf">
					<div class="li-desc big">Numar rezervare:</div>
					<div class="li-content big-pink">483456623</div>
				</li>
				<li class="cf">
					<div class="li-desc">Nume:</div>
					<div class="li-content">antoanela popescu</div>
				</li>
				<li class="cf">
					<div class="li-desc">Telefon:</div>
					<div class="li-content">123456789</div>
				</li>
				<li class="cf">
					<div class="li-desc">Email:</div>
					<div class="li-content">john.doe@gmail.com</div>
				</li>
			</ul>
			<ul class="reservation-list second-list cf">
				<li class="cf">
					<div class="li-desc big">Tarif total:</div>
					<div class="li-content big-pink">167.00 €</div>
				</li>
				<li class="cf">
					<div class="li-desc">Date rezervare:</div>
					<div class="li-content">6 nopti, 1 camera, 1 turist</div>
				</li>
				<li class="cf">
					<div class="li-desc">Check-in:</div>
					<div class="li-content">Miercuri, 03.04.2013</div>
				</li>
				<li class="cf">
					<div class="li-desc">Check-out:</div>
					<div class="li-content">Marti, 09.04.2013</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="offer-container cf">
		<img class="image-holder" src="<?php echo image_uri('i/products-images/test.jpg')?>" alt=""/>
		<div class="right-offer cf">
			<div class="right-offer-left">
				<h2>Rixos Premium</h2>
				<p>Kemer Turcia</p>
			</div>
			<div class="right-offer-center">
				<ul class="stars">
					<?php for ($i = 0; $i < 5; $i++) {?>
					<li class="sprite"></li>
					<?php }?>
				</ul>
				<a class="map-tag" href="#" title="#"><span class="sprite map-icon"></span>Harta</a>
			</div>
		</div>
	</div>
	<div id="suboffer">
		<ul class="reservation-list first-list cf">
			<li class="cf">
				<div class="li-desc">Nume:</div>
				<div class="li-content">antoanela popescu</div>
			</li>
			<li class="cf">
				<div class="li-desc">Telefon:</div>
				<div class="li-content">123456789</div>
			</li>
		</ul>
		<ul class="reservation-list second-list cf">
			<li class="cf">
				<div class="li-desc">Date rezervare:</div>
				<div class="li-content">6 nopti, 1 camera, 1 turist</div>
			</li>
			<li class="cf">
				<div class="li-desc">Check-in:</div>
				<div class="li-content">Miercuri, 03.04.2013</div>
			</li>
		</ul>
	</div>
</section>