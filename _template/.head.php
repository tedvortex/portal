<head>
	<title><?php $this->output('header_title')?></title>
	<meta name="description" 			content="<?php $this->output('meta_description')?>"		/>
	<meta name="keywords" 			content="<?php $this->output('meta_keywords')?>"			/>
	<meta name="author" 			content="<?php $this->output('author')?>" 				/>
	<meta name="robots" content="index,nofollow" />
	<meta charset="UTF-8"												/>
	<link href="<?php echo _static.'images/logo.png'?>" rel="shortcut icon" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo _static.'css/bootstrap-responsive.min.css'?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo _static.'css/bootstrap.min.css'?>" />
	<style type="text/css"><?php $this->output('css')?></style>

	<!--[if lt IE 9]>
	<script src="<?php echo $this->get('s').'scripts/html5.js'?>"></script>
	<![endif]-->


	
</head>