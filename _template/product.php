<?php $this->extend('layout')?>

<h1>Circuit: Portugalia - Spania - Maroc / 10 zile</h1>
<ul id="offer-management" class="nav cf">
	<li><span class="sprite transport"></span><strong>Transport:</strong> Avion</li>
	<li><span class="sprite date"></span><strong>Perioada:</strong> 15 sept. 2013 - 21 sept. 2013</li>
</ul>
<article id="page">
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
</article>
<section id="product-internal-container">
	<section id="unfolded-offer-container">
		<strong>Ziua</strong>
		<ul id="unfolded-offer-tabs" class="tabs-list nav cf" data-tabs="unfolded-offer-days">
			<?php for($day = 1; $day <= 10; $day++){ ?>
			<li <?php echo ($day == 1 ? 'class="a"' : '')?>><p><?php echo $day?></p></li>
			<?php } ?>
		</ul>
		<div id="unfolded-offer" class="product-box">
			<ul id="unfolded-offer-days" class="nav">
				<?php for($day = 1; $day <= 10; $day++){ ?>
				<li class="<?php echo ($day == 1 ? 'a' : 'n')?>">
					<h2>Bucuresti - Lisabona <?php echo $day?></h2>
					<div class="page">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...</p>
					</div>
				</li>
				<?php } ?>
			</ul>
		</div>
	</section>

	<section>
		<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
			<li class="a"><p>Tarifele</p></li>
		</ul>
		<div id="tariffs-container" class="product-box cf">
			<div class="fl">
				<div id="prices-select">
					<ul class="cf nav">
						<li class="medium">
							<label>Durata / nopti</label>
							<select class="value" name="children" id="children">
							 	<option value="1">1</option>
							 	<option value="2">2</option>
							 	<option value="3">3</option>
							 	<option value="4">4</option>
							 	<option value="5">5</option>
							 	<option value="6">6</option>
							 	<option value="7">7</option>
							</select>
						</li>
						<li>
							<label>Plecare - aeroport</label>
							<select class="value" name="children" id="children">
							 	<option value="1">Bucuresti</option>
							 	<option value="2">Lorem </option>
							 	<option value="3">Ipsum</option>
							</select>
						</li>
					</ul>
					<ul class="cf nav">
						<li>
							<label>Tip camera</label>
							<select class="value" name="children" id="children">
							 	<option value="1">Camera standard</option>
							 	<option value="2">Camera dubla</option>
							</select>
						</li>
						<li>
							<label>Masa</label>
							<select class="value" name="children" id="children">
							 	<option value="1">Full Breakfast</option>
							 	<option value="2">Half Breakfast</option>
							</select>
						</li>
					</ul>
					<ul class="cf small nav">
						<li>
							<label>Nr adulti</label>
							<select class="value" name="children" id="children">
							 	<option value="1">2 AD</option>
							 	<option value="2">3 AD</option>
							 	<option value="3">4 AD</option>
							</select>
						</li>
						<li>
							<label>Nr copii</label>
							<select class="value" name="children" id="children">
							 	<option value="1">2 CH</option>
							 	<option value="2">3 CH</option>
							 	<option value="3">4 CH</option>
							</select>
						</li>
						<li>
							<label>Varsta copii</label>
							<select class="value" name="children" id="children">
							 	<option value="1">5</option>
							 	<option value="2">6</option>
							 	<option value="3">7</option>
							 	<option value="4">8</option>
							 	<option value="5">9</option>
							 	<option value="6">10</option>
							</select>
						</li>
						<li>
							<label></label>
							<select class="value" name="children" id="children">
							 	<option value="1">11</option>
							 	<option value="2">12</option>
							 	<option value="3">13</option>
							</select>
						</li>
					</ul>
				</div>
				<div id="prices-chart-container"></div>
			</div>
			<div class="fr">
				<div id="prices-calendar-controller">
					<span class="sprite left"></span>
					<span class="sprite right"></span>
					Iunie 2013
				</div>
				<table id="prices-calendar">
					<tr>
						<th><div>Lu.</div></th>
						<th><div>Ma.</div></th>
						<th><div>Mi.</div></th>
						<th><div>Jo.</div></th>
						<th><div>Vi.</div></th>
						<th><div>Sa.</div></th>
						<th><div>Du.</div></th>
					</tr>
					<?php
					$order = 0;
					for($i = 0; $i<5; $i++){
					?>
					<tr>
						<?php for($j = 0; $j < 7; $j++){ ?>
						<td>
							<div <?php echo ($order < 30 ? 'class="exist"' : '')?>>
								<?php if( $order < 30 ){ ?>
								<span><?php echo ++$order?></span>
								<span>1234*</span>
								<?php } ?>
							</div>
						</td>
						<?php } ?>
					</tr>
					<?php } ?>
				</table>
				<div id="prices-calendar-buttons" class="cf">
					<div class="fl">

					</div>
					<div class="fr">
						<a title="#" href="#" class="button checked">Rezerva <span class="sprite white-check"></span><span class="edge arrow"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<ul class="tabs-list tabs-list-big nav cf" data-tabs="prices-tabs">
			<li class="a"><p>Tarifele includ</p></li>
			<li><p>Tarifele nu includ</p></li>
		</ul>
		<div class="product-box">
			<ul id="prices-tabs" class="nav">
				<?php for($i=0;$i<2;$i++){ ?>
				<li class="<?php echo ($i == 0 ? 'a' : 'n')?>">
					<ul class="nav">
						<li><span class="sprite"></span><?php echo $i?></li>
						<?php for($k=0; $k<5; $k++){ ?>
						<li><span class="sprite"></span>Lorem ipsum dolor sit amet ..</li>
						<?php } ?>
					</ul>
				</li>
				<?php } ?>
			</ul>
		</div>
	</section>
</section>