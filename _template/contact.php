<?php $this->extend('layout') ?>

<h1 class="heading"><?php echo $name?></h1>

<section id="map">
	<span class="colorfull"></span>
	<div>
		<div id="contact-container" class="cf">
			<div id="write-message" class="color-box">
				<strong><span class="sprite contact-nav-header"></span>SCRIE-NE UN MESAJ!</strong>
				<div>
					<form id="action" action="" method="post">
						<input type="hidden" name="validation" value="1"/>
						<div id="contact-form" class="cf">
							<?php echo $form?>
						</div>
						<div class="buttons">
							<button type="submit"></button>
							<a class="button normal orange-background form-submitter">Trimite</a>
						</div>
					</form>
				</div>
			</div>
			<div id="contact-us" class="color-box">
				<strong><span class="sprite phone-icon"></span>CONTACTEAZA-NE!</strong>
				<div>
					<p><span class="click">Click & Go </span><br />SC MONDIAL VOYAGES SRL <br />G.S.A. CLUB MEDITERRANEE in Romania <br /><?php echo $this->get('address')?></p>
					<ul>
						<li>
							<p class="phone-contact-us"><span>tel:</span> <?php echo $this->get('phone')?></p>
							<p class="phone-contact-us"><span class="h">tel:</span> <?php echo $this->get('phone2')?></p>
							<p class="phone-contact-us"><span class="h">tel:</span> <?php echo $this->get('phone3')?></p>
							<p class="phone-contact-us"><span>email:</span> <?php echo $this->get('email')?></p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
