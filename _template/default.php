<?php $this->extend('layout') ?>

<?php if($slide){ ?>
<div class="hero-unit">
    <div id="default-slide-container">
        <ul id="default-slide" class="nextgen-slider nav" data-navigator="default-slide-navigator" data-speed="1000" data-timeout="4500">
			<?php foreach($slide as $sl){ ?>
			<li>
				<a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>">
					<img src="<?php echo $sl['image']?>" alt="<?php echo $sl['name']?>" />
				</a>
			</li>
			<?php } ?>
        </ul>
		<ul id="default-slide-navigator" class="nav cf"></ul>
		<div id="default-slide-description">
			<span class="sprite"></span>
			<span class="sprite right"></span>
			<ul id="default-slide-text" class="cf">
				<?php foreach($slide as $sl){ ?>
				<li><a href="<?php echo $sl['link']?>" title="<?php echo $sl['name']?>"><?php echo $sl['description']?></a></li>
				<?php } ?>
			</ul>
		</div>
    </div>
</div>
<?php } ?>
<div id="products" class="default-product-listing">
	<?php
	if($summar){
		foreach($summar as $sum){
	?>
	<div class="hero-unit">
		<div class="<?php echo $sum['container_class']?>">
			<?php if( in_array($sum['type'], array(0,3)) ){ ?>
			<div class="heading <?php echo $sum['color']?>-border<?php echo $sum['model'] == 1 ? ' '.$sum['color'].'-background' : ''?>">
                <h2 <?php echo ($sum['model'] == 0 ? 'class="'.$sum['color'].'-color"' : '')?>><?php echo $sum['name']?></h2>
            </div>
            <a href="<?php echo $sum['link']?>" class="button <?php echo $sum['color']?>-background" title="<?php echo $sum['name']?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
			<?php
			}
			if( $sum['type'] == 0 ){
			?>
			<a class="image" href="<?php echo $sum['link']?>" title="<?php echo $sum['name']?>">
                <img src="<?php echo $sum['image']?>" alt="<?php echo $sum['name']?>" width="830" height="168"/>
					 <?php if ($sum['price']>0) {?>
                	<span class="price <?php echo $sum['color']?>-background"><?php echo tr('De la') ?><span class="pricep"><?php echo $sum['price'].$sum['code']?></span></span>
					 <?php }?>
            </a>
            <p class="<?php echo $sum['color']?>-color">
            
            <?php foreach( $sum['tags'] as $k=>$sec_tag ){ ?>
				<?php echo ($k>0)?'/ ':''?><a href="<?php echo $sec_tag['link']!=''?$sec_tag['link']:'#'?>" class="<?php echo $sum['color']?>-color" title="<?php echo $sec_tag['name']?>"><?php echo $sec_tag['name']?></a> 
			<?php } ?>
            
            </p>
			<?php
			}
			elseif( $sum['type'] == 1 ){
				if( $sum['offers'] ){
					foreach($sum['offers'] as $k_sec => $sec_sum){
						if( $k_sec < 2 ){
			?>
			<div class="span4">
                <div class="item booking-detailed">
                    <div class="heading <?php echo $sec_sum['color']?>-border<?php echo ($sum['model'] == 1 ? ' '.$sec_sum['color'].'-background' : '')?>">
                        <h2 <?php echo ($sum['model'] == 0 ? 'class="'.$sec_sum['color'].'-color"' : '')?>><?php echo $sec_sum['name']?></h2>
                    </div>
                    <a href="<?php echo $sec_sum['link']?>" class="button <?php echo $sec_sum['color']?>-background" title="<?php echo $sec_sum['name']?>"><?php echo tr('Vizualizare oferte') ?><span class="sprite arror-right-content"></span></a>
                    <a class="image" href="<?php echo $sec_sum['link']?>" title="<?php echo $sec_sum['name']?>">
                        <img src="<?php echo $sec_sum['image']?>" alt="<?php echo $sec_sum['name']?>" width="410" height="178"  />
								 <?php if ($sec_sum['price']>0) {?>
                        	<span class="price <?php echo $sec_sum['color']?>-background"><?php echo tr('De la') ?><span class="pricep"><?php echo $sec_sum['price'].$sec_sum['code']?></span></span>
                    		<?php }?>
						  
						  </a>
                    <?php
                    if( $sum['model'] == 0 ){
                    	if($sec_sum['offers']){
                    ?>
					<div class="row">
						<?php
						foreach($sec_sum['offers'] as $k_tri => $tri_sum){
                			if($k_tri < 2){
                		?>
                        <div class="span2">
                            <h2 class="<?php echo $sec_sum['color']?>-color <?php echo $sec_sum['color']?>-border"><a class="<?php echo $sec_sum['color']?>-color" href="<?php echo $tri_sum['link']?>" title="<?php echo $tri_sum['name']?>"><?php echo $tri_sum['name'] ?></a></h2>
                            <?php if( $tri_sum['tags'] ){ ?>
                            <ul class="<?php echo $sec_sum['color']?>-color">
                            	<?php foreach($tri_sum['tags'] as $k_tri_t => $tri_tag){ ?>
                                <li><span class="sprite check-<?php echo $sec_sum['color']?>"></span><?php echo $tri_tag['name'] ?></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>
                        <?php
                			}
						}
						?>
                    </div>
                    <?php
                    	}
                    }
                    else {
                    ?>
                    <p class="<?php echo $sec_sum['color']?>-color">
                    	<?php foreach( $sec_sum['tags'] as $k=>$sec_tag ){ ?>
							<?php echo ($k>0)?'/ ':''?><a href="<?php echo $sec_tag['link']!=''?$sec_tag['link']:'#'?>" class="<?php echo $sum['color']?>-color" title="<?php echo $sec_tag['name']?>"><?php echo $sec_tag['name']?></a>
						<?php } ?>
                    </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
			<?php
						}
					}
				}
			}

			elseif( $sum['type'] == 2 ){
				if( $sum['offers'] ){
					foreach($sum['offers'] as $k_sec => $sec_sum){
						if( $k_sec < 4 ){
			?>
			<div class="span2">
                <div class="item tour-detailed">
                    <div class="heading <?php echo $sec_sum['color']?>-border <?php echo $sec_sum['color']?>-background">
                        <h2 class="<?php echo $sec_sum['color']?>-border"><?php echo $sec_sum['name']?></h2>
                    </div>
                    <a class="image" href="<?php echo $sec_sum['link']?>" title="<?php echo $sec_sum['name']?>">
                        <img src="<?php echo $sec_sum['image']?>" alt="<?php echo $sec_sum['name']?>" width="200" height="268" />
								
								<?php if ($sec_sum['price']>0) {?>
                        <span class="price <?php echo $sec_sum['color']?>-background"><?php echo tr('De la' ) ?><span class="pricep"><?php echo $sec_sum['price'].$sec_sum['code']?></span></span>
								<?php }?>
								
								
                    </a>
                </div>
            </div>
			<?php
						}
					}
				}
			}

			elseif( $sum['type'] == 3 ){
				if( $sum['offers'] ){
			?>
			<div class="row">
					<?php
					foreach($sum['offers'] as $k_sec => $sec_sum){
						if( $k_sec < 4 ){
					?>
                <div class="span2">
                    <a href="<?php echo $sec_sum['link']?>" class="image" title="<?php echo $sec_sum['name']?>">
                        <img src="<?php echo $sec_sum['image']?>" alt="<?php echo $sec_sum['name']?>" width="200" height="148" />
								<?php if ($sec_sum['price']>0) {?>
                        	<span class="price <?php echo $sum['color']?>-background"><?php echo tr('De la ') ?><span class="pricep"><?php echo $sec_sum['price'].$sec_sum['code']?></span></span>
								<?php }?>
                    </a>
                    <h2 class="<?php echo $sum['color']?>-color<?php echo ($sum['model'] == 1 ? ' transparent-border' : '')?>"><a class="<?php echo $sum['color']?>-color" href="<?php echo $sec_sum['link']?>" title="<?php echo $sec_sum['name']?>"><?php echo $sec_sum['name']?></a></h2>
	                    <?php
	                    if( $sum['model'] == 0 ){
	                    	if( $sec_sum['tags'] ){
	                    ?>
                    <ul class="padded-list">
                    	<?php foreach( $sec_sum['tags'] as $sec_tag ){ ?>
                        <li><a href="<?php echo $sec_tag['link']!=''?$sec_tag['link']:'#'?>" title="<?php echo $sec_tag['name']?>"><span class="sprite check-<?php echo $sum['color']?>"></span><span class="<?php echo $sum['color']?>-color"><?php echo $sec_tag['name']?></span><br /><?php echo $sec_tag['short_description']?></a></li>
                        <?php } ?>
                    </ul>
	                    <?php
	                    	}
	                    }
	                    ?>
                </div>
					<?php
						}
					}
					?>
			</div>
			<p data-id="<?php echo $sum['id']?>" class="<?php echo $sum['color']?>-color <?php echo $sum['color']?>-border border-top">
			<?php foreach( $sum['tags'] as $k=>$sec_tag ){ ?>
				<?php echo ($k>0)?'/ ':''?><a href="<?php echo $sec_tag['link']!=''?$sec_tag['link']:'#'?>" class="<?php echo $sum['color']?>-color" title="<?php echo $sec_tag['name']?>"><?php echo $sec_tag['name']?></a>
			<?php } ?>
			</p>
			<?php
				}
			}
			?>
		</div>
	</div>
	<?php
		}
	}
	?>
</div>