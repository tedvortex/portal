<?php
$time = microtime(true);

define('r', dirname(__FILE__) . '/');
require_once (r . '.includes/init.php');

if (preg_match('/(?i)MSIE [1-7]\.[0-9]*;/', $_SERVER['HTTP_USER_AGENT'])) {
    exit(require_once (s . 'ie6-7.php'));
}

require_once (p . 'select.php');
require_once (p . 'get.php');

require_once (p . 'globals.php');

require_once (c . $_construct['template'] . '.php');

$_SESSION['constructor'] = $_construct['template'];

foreach ($globals as $k => $p) {
    $engine->set($k, $p);
}

echo $engine->render($_construct['template'], $page_info);