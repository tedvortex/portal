<?php
require_once(dirname(__FILE__).'/includes/init.php');
//error_reporting(E_ALL);
//print_a($_POST);
//die();
$max_fotos = 50;
$filename = 'produs-'.$_GET['id_product'];
$array_extensions=array('jpg','png','jpeg','gif','JPG','PNG','JPEG','GIF');
$images = array();


$product_info=$db->fetch("
	SELECT 
		`name_seo`,`name` 
	FROM 
		`xp_croaziera` p
		inner join `xp_croaziera_data` pd
		on p.`id`=pd.`_id`
 	WHERE 
		p.`id`=".(int)$_GET['id_product']."",MYSQL_ASSOC);

//print_a($product_info);
$images=$db->fetch_all("SELECT * FROM `xp_croaziera_images` WHERE `id_croaziera`=".(int)$_GET['id_product']." ORDER BY `order`,`id_croaziera` ASC",MYSQL_ASSOC);
//$image_name = str_replace(' ','-',$product_info['name_seo']);

if(
	!isset($_POST['validation'])
){
	for($i=1;$i<=$max_fotos;$i++){
		foreach($array_extensions as $ext){
			@unlink(SITEROOT.'/.tmp_img/'.$filename.'-'.$i.'.'.$ext);
		}
	}
	foreach( $images as $key=>$image ){
		$ad_images[] = $image;
		$i = count($ad_images);
		preg_match( '%(\..{3,4})$%', $image['image'], $ext );

		@copy( SITEROOT.'/static/i/croaziere/'.$image['image'], SITEROOT.'/.tmp_img/'.$filename.'-'.$i.$ext[1] );
//
		$_REQUEST['order-'.$i] = $image['order'];
		$_REQUEST['name-'.$i] = $image['name'];	
	}
}

//print_a($_REQUEST);
for(
	$i=1;
	$i<=$max_fotos;
	$i++
){
	foreach (
		$array_extensions as
		$k=>$ext
	){
		$name = $filename.'-'.$i.'.'.$ext;

		if(
			is_file(SITEROOT."/.tmp_img/".$name)
		){
			
			$a = array(
				'image' => '.tmp_img/'.$name,
				'file' => $name
			);
			
			$photos[] = $a;
				
			break;
		}
	}
}
//print_a($page_info['photos']);
$empty_el=($max_fotos-count($photos));
//print_a($empty_el);
if(
	isset($_POST['validation'])
){
 	foreach($images as $j){
	@unlink(SITEROOT."/static/i/croaziere/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/148/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/130/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/178/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/160/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/200/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/216/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/348/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/408/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/40/".$j['image']);
	@unlink(SITEROOT."/static/i/croaziere/78/".$j['image']);
 }

$db->sql("delete from `xp_croaziera_images` where `id_croaziera` = ".$_GET['id_product']."");
$image_name = str_replace(' ','-',$product_info['name_seo']);
for(
	$i=1;
	$i<=$max_fotos;
	$i++
){
	foreach (
		$array_extensions as
		$k=>$ext
	){
		$name = $filename.'-'.$i.'.'.$ext;

		if(
			is_file(SITEROOT."/.tmp_img/".$name)
		){
			$final_image=$image_name."-".$i.'.'.strtolower($ext);
			$folder=SITEROOT.'/.tmp_img';
			$dimension=640;

			$w=$dimension;

			$f=$folder.'/'.$name;

			list($width,$height,$ftype)=getimagesize($f);
			if($width>$dimension){
				switch($ftype){
					case IMAGETYPE_GIF:
						$image=@imagecreatefromgif($f);
						$_ext='gif';
						break;

					case IMAGETYPE_JPEG:
						$image=@imagecreatefromjpeg($f);
						$_ext='jpg';
						break;

					case IMAGETYPE_PNG:
						$image=@imagecreatefrompng($f);
						$_ext='png';
						break;
				}
				$percent=$w/$width;
				$new_width=$width*$percent;
				$new_height=$height*$percent;
				/*create tranparent img*/
				if($image_p=imagecreatetruecolor($new_width,$new_height)){
					imagealphablending($image_p,false);
					imagesavealpha($image_p,true);
					$transparent=imagecolorallocatealpha($image_p,255,255,255,127);
					imagefilledrectangle($image_p,0,0,$new_width,$new_height,$transparent);
					/*end create transparent img*/
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

					@unlink($f);

					$nf='static/i/croaziere/'.$final_image;

					switch($ftype){
						case IMAGETYPE_GIF:
							imagegif($image_p,SITEROOT."/".$nf);
							break;

						case IMAGETYPE_JPEG:
							imagejpeg($image_p,SITEROOT."/".$nf,100);
							break;

						case IMAGETYPE_PNG:
							imagepng($image_p,SITEROOT."/".$nf,9);
							break;
					}

					imagedestroy($image_p);
				}
			}
			else{
   			 	$temp_file=SITEROOT."/.tmp_img/".$name;
   			 	$target_file=SITEROOT."/static/i/croaziere/".$final_image;
//   			 	@unlink(SITEROOT."/static/i/croaziere/100/".$final_image);
   			 	rename($temp_file,$target_file);
			}
			
			$db->qinsert ( 'xp_croaziera_images', array('id_croaziera'=>$_GET['id_product'],'image'=>$final_image,'name'=>$_POST['name-'.$i],'order'=>$_POST['order-'.$i]));
			break;
		}
	}
}
	exit(header("Location: http://".$_SERVER['HTTP_HOST']."/admin/index.php?mod=croaziere&idprod=".$_GET['id_product']));
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="robots" content="index,nofollow" />
	<title><?=l('ADMINISTRATOR ZONE').' ('.$config['SITE_NAME'].') - Mod: ('.strip_tags($_GET['mod']).')'?></title>

	<link rel="stylesheet" type="text/css" href="includes/css/base.css" media="screen" />
	<script type="text/javascript" src="includes/jquery/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery.fileuploader.js"></script>
	<script type="text/javascript" src="includes/jquery/default.js"></script>
<style type="text/css">	
.cf:after, .c {
    border: 0 none !important;
    clear: both !important;
    content: "" !important;
    display: block !important;
    float: none !important;
    font-size: 0 !important;
    height: 0 !important;
    line-height: 0 !important;
    margin: 0 !important;
    min-height: 0 !important;
    overflow: hidden !important;
    padding: 0 !important;
    visibility: hidden !important;
    width: 0 !important;
}
#menu{
	z-index:30;
	position:relative;
}
#content{
	z-index:20;
	position:relative
}
#file-uploader2{
	position:relative;
	padding:0 0 0 10px
}
#file-uploader2 img{
	margin-bottom:10px;
}
#file-uploader2	h3{
	font-weight:bold;
	position:absolute;
	top:10px;
	left:1px;
}

#file-uploader2.qq-upload-list1{width:418px}

.uploaded
{
	width:200px !important;
	height:250px !important;
	margin:0 7px 7px 0;
	padding:10px;
	border:1px solid #a0a0a0;
	position:relative
}
.qq-upload-drop-area
{
	width:162px;
	height:100px;
	background:#16527C
}	
#file-uploader{width:0}
.qq-upload-button,
.button
{
   display:block;
	width:160px;
	height:30px;
	line-height:30px;
	background:#E3F1FA;
	position:relative;
	zoom:1;
	cursor:pointer;
	border:1px solid #2779AA;
	text-align:center;
	text-decoration:none;
	color:#2779AA;
	font-weight:bold;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;	
}
.button{
	line-height:0
}

.delete{
	position:absolute;
	top:1px;
	right:1px;
	font-size:14px;
	font-weight:bold;
	text-align:center;
	line-height:16px;
	width:20px;
	height:20px;
	display:block;
	color:#fff;
	background:#dd3737;
	cursor:pointer;
}
.image{
	background:#dadada;
	text-align:center;
	float:left;
	width:0;
	height:0;
	
}
.image em{
	display:block;
	position:absolute;
	left:0;
	top:110px;
	text-align:center;
	line-height:12px;
	height:12px;
	width:100%;
}
.image div{
	padding-top:40px
	position:relative
}

.input-text{
	width:60px;
	margin-bottom:10px;
	border:1px solid #a0a0a0;
	padding:5px
} 
.buttons{
	position:absolute;
	bottom:20px;
	right:20px;
}
</style>

	<!--height:0
	overflow:hidden
	padding:0 0 10px 196px-->

	


</head>
<body>
<div id="main">
	<div id="header">
		<div class="logo">
			<a href="index.php" rel="external follow"><img src="images/logo.png" alt="ExpertVision.ro" /></a>
		</div>
		<div class="titleAdmin" style="position:relative;">
			<?=l(ADMINISTRATOR_ZONE)?>
			<div style="position:absolute;font-size:16px;left:250px;width:400px;top:0;">
			   Aveti <b style="color:#FFA24F;font-size:16px;"><?=$totalComenzi?></b> <?=($totalComenzi==1)?'comanda in derulare':'comenzi in derulare'?>
			   <br/>Aveti <b style="color:#FF5FC7;font-size:16px;"><?=$totalComenzi2?></b> <?=($totalComenzi2==1)?'comanda in asteptare':'comenzi in asteptare'?>
			</div>
		</div>
		<div class="right_menu">
			<ul>
				<li><a href="index.php?action=index" ><img src="images/li_home.png" alt="home" /><?=l(ADMIN_HOME)?></a></li>
				<li><a href="<?=BASEHREF?>" target="_blank"><img src="images/li_site.png" alt="view" /><?=l(ADMIN_VIEW_WEBSITE)?></a></li>
				<li><a href="index.php?action=logout" ><img src="images/li_log.png" alt="logout" /><?=l(ADMIN_LOGOUT)?></a></li>
			</ul>
			<div id="languageDiv" style="height:80px;">
				<div id="flags">
					<?php
					$languages = get_languages ();
					//echo LANG;
					foreach ( $languages as $lang ) {
						if (LANG==$lang['prefix']) {
							$classImg='imgLimbaAct';
						} else {
							$classImg='imgLimba';
						}
					?>
					<a href="index.php?mod=<?=$mod?>&amp;lang=<?=$lang['prefix']?>" title="<?=$lang['name']?>"><img src="images/flags/<?=$lang['prefix']?>.png" width="22" height="16" alt="<?=$lang['name']?>" class="<?=$classImg?>" style="margin-left:5px;"/></a>
					<?}?>
				</div>
				<div class="clear"></div>
			</div>
			<?php
			$admin_info=adm_info();
			?>
			<div id="auth">
				<strong><?=l(AUTENTIFICAT_CA)?></strong> : <?=$admin_info['first_name']." ".$admin_info['last_name']?> <br />(<a href="mailto:<?=$admin_info['email']?>&amp;subject=From%20Owner:%20(<?=SITE_NAME?>)"><?=$admin_info['email']?></a>)
			</div>
		</div>
	</div>
	<?=print_menu()?>
	<div id="content">
	<h1 style="padding:10px;"><?php echo $product_info['name']?></h1>


<form method="POST" action="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/admin/image5.php?id_product='.$_GET['id_product']?>">
	<div>
		<input type="hidden" name="validation" value="1" />
		<div id="file-uploader2">
			<ul class="qq-upload-list1 cf">
			<?php
		 	if(
		 		(!empty($photos))
		 	){
		 	?>
				<?php
			 	foreach ($photos as $k=>$p){?>
		 		<li class="image uploaded">
					<img width="200" height="150" src="<?php echo $SERVER['HTTP_HOST'].'/'.$p['image']?>"/>
					<span data-file="<?php echo str_replace('.tmp_img/','',$p['image'])?>" class="delete">x</span>
					<span>Nume</span>
					<input style="width:80px;margin-bottom:10px;border:1px solid #a0a0a0;padding:5px" type="text" class="name" value="<?php echo $_REQUEST['name-'.($k+1)]?>" name="name-<?php echo ($k+1)?>" /><br>
					<span>Ordine</span>
					<input style="width:60px;margin-bottom:10px;border:1px solid #a0a0a0;padding:5px" type="text" value="<?php echo $_REQUEST['order-'.($k+1)]?>" name="order-<?php echo ($k+1)?>" /><br>
				</li>
			<?php
			}
			?>
			 	<?php
			 	}
	
			 	for(
			 		$i=$max_fotos-$empty_el+1;
			 		$i<=$max_fotos;
			 		$i++
			 	){
			 	?>
				<li class="image" data-element="<?php echo $i?>">
					
				</li>
				<?php
			 	}
			 	?>
			</ul>
		</div>
	
		<div style="padding:40px 0 10px 10px">
			<div data-id="<?php echo $_GET['id_product']?>" id="file-uploader" class="cf">
		    	<input id="files_upload" name="file_upload" type="file" />
			</div>
		</div>
		<div class="buttons">
			<button class="button" type="submit">SALVEAZA</button>
		</div>
	</div>
</form>
	</div>
		<div id="footer">Powered by ExpertVision.ro &copy; 2004-<?=date('Y')?></div>
	</div>
</body>
</html>