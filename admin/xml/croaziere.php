<?php
define('ADMINROOT', dirname(dirname(__FILE__)));
require_once (ADMINROOT . '/includes/init.php');

if (empty($_FILES['products']['name'])) {
    exit('<script type="text/javascript">alert("Selectati fisierul !");</script>');
}

$name = preg_replace('%[^\p{N}\p{L}\.]*%', '', $_FILES['products']['name']);

move_uploaded_file($_FILES['products']['tmp_name'], dirname(__FILE__) . "/" . $name);

$file = "admin/xml/" . $name;

$content = file_get_contents("http://www.clickandgo.ro/admin/xml.new?script=import_croaziera&file={$file}");

print_a($content);

exit();