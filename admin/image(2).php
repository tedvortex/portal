<?php
require_once(dirname(__FILE__).'/includes/init.php');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="robots" content="index,nofollow" />
	<title><?=l('ADMINISTRATOR ZONE').' ('.$config['SITE_NAME'].') - Mod: ('.strip_tags($_GET['mod']).')'?></title>

	<link rel="stylesheet" type="text/css" href="includes/css/base.css" media="screen" />

	

	



	<script type="text/javascript" src="includes/jquery/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery.fileuploader.js"></script>
	<script type="text/javascript" src="includes/jquery/default.js"></script>


<style type="text/css">	
.cf:after, .c {
    border: 0 none !important;
    clear: both !important;
    content: "" !important;
    display: block !important;
    float: none !important;
    font-size: 0 !important;
    height: 0 !important;
    line-height: 0 !important;
    margin: 0 !important;
    min-height: 0 !important;
    overflow: hidden !important;
    padding: 0 !important;
    visibility: hidden !important;
    width: 0 !important;
}
#file-uploader2{
	position:relative;
}
#file-uploader2	h3{
	font-weight:bold;
	position:absolute;
	top:10px;
	left:1px;
}
#file-uploader2.qq-upload-list1{width:418px}
		
#file-uploader{width:0}
.qq-upload-button
{
   display:block;
	width:160px;
	height:30px;
	line-height:30px;
	background:#316B97;
	position:relative;
	zoom:1;
	cursor:pointer;
	border:1px solid #273a43;
	text-align:center;
	text-decoration:none;
	color:#fff;
	font-weight:bold;
}

.delete{
	position:absolute;
	top:1px;
	right:1px;
	font-size:14px;
	font-weight:bold;
	text-align:center;
	line-height:16px;
	width:20px;
	height:20px;
	display:block;
	color:#fff;
	background:#ff0000;
	cursor:pointer;
}
.image{
	width:100px;
	height:100px;
	float:left;
	background:gray;
	margin:0 0 50px 2px;
	position:relative;
	border:1px solid #CCCCCC;
	text-align:center;
	background:#FAC81B;
	position:relative;
}
.image em{
	display:block;
	position:absolute;
	left:0;
	top:110px;
	text-align:center;
	line-height:12px;
	height:12px;
	width:100%;
}
.image div{
	padding-top:40px
	position:relative
}
.image input{
	width:100px !important
	padding:0 !important
	left:0
	line-height,height:15px !important
	position:absolute !important
	top:120px !important
}
</style>

	<!--height:0
	overflow:hidden
	padding:0 0 10px 196px-->

	


</head>
<body>
<div id="main">
	<div id="header">
		<div class="logo">
			<a href="index.php" rel="external follow"><img src="images/logo.png" alt="ExpertVision.ro" /></a>
		</div>
		<div class="titleAdmin" style="position:relative;">
			<?=l(ADMINISTRATOR_ZONE)?>
			<div style="position:absolute;font-size:16px;left:250px;width:400px;top:0;">
			   Aveti <b style="color:#FFA24F;font-size:16px;"><?=$totalComenzi?></b> <?=($totalComenzi==1)?'comanda in derulare':'comenzi in derulare'?>
			   <br/>Aveti <b style="color:#FF5FC7;font-size:16px;"><?=$totalComenzi2?></b> <?=($totalComenzi2==1)?'comanda in asteptare':'comenzi in asteptare'?>
			</div>
		</div>
		<div class="right_menu">
			<ul>
				<li><a href="index.php?action=index" ><img src="images/li_home.png" alt="home" /><?=l(ADMIN_HOME)?></a></li>
				<li><a href="<?=BASEHREF?>" target="_blank"><img src="images/li_site.png" alt="view" /><?=l(ADMIN_VIEW_WEBSITE)?></a></li>
				<li><a href="index.php?action=logout" ><img src="images/li_log.png" alt="logout" /><?=l(ADMIN_LOGOUT)?></a></li>
			</ul>
			<div id="languageDiv" style="height:80px;">
				<div id="flags">
					<?php
					$languages = get_languages ();
					//echo LANG;
					foreach ( $languages as $lang ) {
						if (LANG==$lang['prefix']) {
							$classImg='imgLimbaAct';
						} else {
							$classImg='imgLimba';
						}
					?>
					<a href="index.php?mod=<?=$mod?>&amp;lang=<?=$lang['prefix']?>" title="<?=$lang['name']?>"><img src="images/flags/<?=$lang['prefix']?>.png" width="22" height="16" alt="<?=$lang['name']?>" class="<?=$classImg?>" style="margin-left:5px;"/></a>
					<?}?>
				</div>
				<div class="clear"></div>
			</div>
			<?php
			$admin_info=adm_info();
			?>
			<div id="auth">
				<strong><?=l(AUTENTIFICAT_CA)?></strong> : <?=$admin_info['first_name']." ".$admin_info['last_name']?> <br />(<a href="mailto:<?=$admin_info['email']?>&amp;subject=From%20Owner:%20(<?=SITE_NAME?>)"><?=$admin_info['email']?></a>)
			</div>
		</div>
	</div>
	<?=print_menu()?>
	<div id="content">
<?php
$images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$_GET['id_product']." ORDER BY `id` ASC",MYSQL_ASSOC);

$array_extensions=array('jpg','JPG','png','jpeg','gif');

if(
!isset($_POST['validation'])
){
	for($i=0;$i<=20;$i++){
		foreach($array_extensions as $ext){
			@unlink(".tmp_img/".$filename.'-'.$i.'.'.$ext);
		}
	}
}

for(
	$i=0;
	$i<=19;
	$i++
){
	foreach (
		$array_extensions as
		$k=>$ext
	){
		$name = $filename.'-'.$i.'.'.$ext;

		if(
			!isset($_POST['validation'])
		){
			@unlink(".tmp_img/".$name);
		}

		else{
			if(
				is_file(SITEROOT.".tmp_img/".$name)
			){
				$photos[] = '.tmp_img/'.$name;

				break;
			}
		}
	}
}
if(
	$images
){
?>
	<form>
		<ul class="cf" style="padding:0 10px">
		<?php
		foreach(
			$images as 
			$k=>$i
		){
		?>
			<li style="float:left;width:200px;height:210px;margin:0 7px 7px 0;padding:10px;border:1px solid #000;position:relative">
				<img width="200" height="150" style="margin-bottom:10px" src="<?php echo _static.'/i/imagini-produse/'.$i['image']?>" alt="<?php echo $i['image']?>"/><br>
				<span data-file="<?php echo $p?>" class="delete">x</span>
				<span>Ordine</span>
				<input style="width:60px;margin-bottom:10px;border:1px solid #000;padding:5px" type="text" name="order[]" /><br>
				<span>Status</span>
				<span>Activ</span>
				<input type="radio" name="status" value="0"/>
				<span>Inactiv</span>
				<input type="radio" name="status" value="1"/>
			</li>
		<?php
		}
		?>
		</ul>
		</div>
		<div style="padding:20px 0 0 10px">
			<button style="background:#F7F7F7;border:1px solid #000;padding:10px 20px;cursor:pointer" type="submit">Salveaza modificarile</button>
		</div>
	</form>
<?php
}
?>
	<div id="file-uploader2">
		<ul class="qq-upload-list1 cf">
		<?php
	 	if(
	 		(!empty($photos))
	 	){
	 	?>
			<?php
		 	foreach ($photos as $k=>$p){?>
	 		<li style="float:left;width:200px;height:210px;margin:0 7px 7px 0;padding:10px;border:1px solid #000;position:relative" class="image uploaded">
				<img width="200" height="150"  style="margin-bottom:10px" src="<?php echo $p?>"/>
				<span data-file="<?php echo $p?>" class="delete">x</span>
				<span>Ordine</span>
				<input style="width:60px;margin-bottom:10px;border:1px solid #000;padding:5px" type="text" name="order-<?php echo $k?>" /><br>
				<span>Status</span>
				<span>Activ</span>
				<input type="radio" name="status-<?php echo $k?>" value="0"/>
				<span>Inactiv</span>
				<input type="radio" name="status-<?php echo $k?>" value="1"/>
			</li>
		<?php
		}
		?>
		 	<?php
		 	}

		 	for(
		 		$i=1;
		 		$i<=$empty_el;
		 		$i++
		 	){
		 	?>
			<li class="image">
				<em>Poza principala</em>
				<input type="radio" name="main-picture" value="<?php echo $i + count($photos)?>"/>
			</li>
			<?php
		 	}
		 	?>
		</ul>
	</div>

	<div style="padding:40px 0 0 10px">
		<div data-id="<?php echo $_GET['id_product']?>" id="file-uploader" class="cf">
	    	<input id="files_upload" name="file_upload" type="file" />
		</div>
	</div>
	</div>
		<div id="footer">Powered by ExpertVision.ro &copy; 2004-<?=date('Y')?></div>
	</div>
</body>
</html>