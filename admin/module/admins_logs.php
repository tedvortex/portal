<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class admins_logs_module {
	var	$module='admins_logs';
	var $date='27-08-2009';
	var $table=TABLE_ADMIN_USERS_LOGS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function admins_logs_module() {

		$this->name=l('admins_logs');
		$this->title=l('admins_logs');
		$this->description=l('admins_logs');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('data'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		'_nr'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'id_admin'=>array('name'=>l('ID Admin'),'width'=>80,'align'=>'center'),
		'admin'=>array('name'=>l('Admin'),'width'=>200),
		'ip'=>array('name'=>l('IP'),'width'=>160),
		'data'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
	}
	function css() {}
	function json_list(){
		json_list($this);
	}
	function json_list_row($row) {
		global $db;
		$row['admin']=$db->fetch("SELECT `username`,`first_name`,`last_name` FROM `".TABLE_ADMIN_USERS."` WHERE `id`=".$row['id_admin']);
		$row['admin']=$row['admin']['username']." (".$row['admin']['first_name']." ".$row['admin']['last_name'].")";
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
}
$module=new admins_logs_module();

if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
else {
	print_header();
	print_content($module);
	print_footer();
}
?>