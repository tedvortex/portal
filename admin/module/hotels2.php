<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class hotels2_module{
	var $module='hotels2';
	var $date='25-08-2009';
	var $table='fibula_hotels';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $used_filters=array();
	function hotels2_module() {
		global $db;
		
		
		$this->name=l('Hoteluri sejururi');
		$this->title=l('Hoteluri sejururi');
		$this->description=l('Hoteluri sejururi');

		$this->type=array(
		'like'=>array('title','name','address','telephone','email','website','code','owner'),
		'date'=>array('date'),
		'equal'=>array('status','id_city','rating','id_country','id_region'),
		'equal2'=>array('id')
		);

		$this->folder='imagini-pagini';

		$this->grid=array(
		0=>array('order'=>'_id desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>50),
		'code'=>array('name'=>l('Cod'),'width'=>50),
		'name'=>array('name'=>l('Nume'),'width'=>100),
		'code'=>array('name'=>l('Cod'),'width'=>50),
		'email'=>array('name'=>l('Email'),'width'=>60),
		'telephone'=>array('name'=>l('Phone'),'width'=>60),
		'website'=>array('name'=>l('Website'),'width'=>60),
		
		'rating'=>array('name'=>l('Stele'),'width'=>50,'align'=>'center'),
		//'rating2'=>array('name'=>l('Stele text'),'width'=>50,'align'=>'center'),
		'address'=>array('name'=>l('Adresa'),'width'=>60),

		'id_country'=>array('name'=>l('Tara'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select2'),
		'id_region'=>array('name'=>l('Regiunea'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select3'),
		'owner'=>array('name'=>l('Inserat de'),'width'=>50),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_promo'=>array('name'=>l('promovat index cu poza'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_header'=>array('name'=>l('In header'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_footer'=>array('name'=>l('In footer'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_info'=>array('name'=>l('informatie utila'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_navigator'=>array('name'=>l('apare in navigator'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
		
		
		// generez form dinamic {
		$this->form_3=array('tabs'=>array('add'=>'_3'));
		$i=0;
		foreach ($db->fetch_all("SELECT * FROM `xp_tabs` WHERE `position`=0 AND `id_category`=2 order by `order`" ) as $_line){
			$i++;
			$this->form_3['tabs'][$_line['id']]=$_line['name'];
			$this->form_3[]=array('tab'=>$_line['id']);
			$this->form_3[]='nss_univ_'.$_line['name'];

			$this->form_3['add_tab_'.$_line['id']]=array('type'=>'editor','name'=>$_line['name'],'style'=>'width:80%;');
		}

		// generez form dinamic }


		$this->form=array(
		
		'tabs'=>array(
		1=>l('Identitate'),
		2=>l('Descriere'),
		3=>l('Taburi header')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:70%;'),
		'rating'=>array('type'=>'radio',
			'options'=>array(
				'5'=>l('5 stele<br/>'),
				'5.3'=>l('5 tridenti<br/>'),
				'5.2'=>l('HV1<br/>'),
				'4.5'=>l('4 stele plus<br/>'),
				'4.3'=>l('4 tridenti<br/>'),
				'4.2'=>l('HV2<br/>'),
				'4'=>'4 stele<br/>',
				'3'=>l('3 stele<br/>'),
				'3.5'=>l('3 stele plus<br/>'),
				'3.3'=>l('3 tridenti<br/>'),
				'2'=>l('2 stele<br/>'),
				'2.5'=>l('2 stele plus<br/>'),
				'2.2'=>l('2 tridenti<br/>'),
				'1'=>l('1 stea<br/>'),
				'1.5'=>l('1 stea plus<br/>'),
				'1.2'=>l('1 trident<br/>'),
				'0.4'=>l('Pensiune<br/>'),
				'0.3'=>l('Apartament<br />')),
				'name'=>l('Nr. stele')),
		//'rating2'=>array('type'=>'input','name'=>l('Nr stele text'),'style'=>'width:70%;'),
		
		//'name_seo'=>array('type'=>'input','name'=>l('Link url'),'style'=>'width:70%;'),
		
		8=>'show_state',
		9=>'show_city',
		
		'address'=>array('type'=>'input','name'=>l('Adresa'),'style'=>'width:70%;'),
		'postal_code'=>array('type'=>'input','name'=>l('Cod postal'),'style'=>'width:70%;'),
		'telephone'=>array('type'=>'input','name'=>l('Telefon'),'style'=>'width:70%;'),
		'fax'=>array('type'=>'input','name'=>l('Fax'),'style'=>'width:70%;'),
		'website'=>array('type'=>'input','name'=>l('Website'),'style'=>'width:70%;'),
		'email'=>array('type'=>'input','name'=>l('Email'),'style'=>'width:70%;'),
		'latitude'=>array('type'=>'input','name'=>l('Latitudine'),'style'=>'width:70%;'),
		'longitude'=>array('type'=>'input','name'=>l('Longitudine'),'style'=>'width:70%;'),
		'code'=>array('type'=>'input','name'=>l('Cod'),'style'=>'width:70%;'),
		//'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:70%;'),
		//'class'=>array('type'=>'input','name'=>l('Clasa'),'style'=>'width:70%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//'is_promo'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('promovat index cu poza')),
		//'is_footer'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('In footer')),
//		'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('informatie utila')),
		//'is_navigator'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in navigator')),
		//'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in index text')),
	//	'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true,'style'=>'width:70%;height:200px;'),
	//	1=>'show_categories',
		14=>array('tab'=>2),

		'description'=>array('type'=>'editor','name'=>l('Descriere')),
		'directions'=>array('type'=>'editor','name'=>l('Directions')),
		'facilities'=>array('type'=>'editor','name'=>l('Facility')),
		15=>array('tab'=>3),
		16=>'show_tabs_2',

		//2=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine buton'),'text'=>'170x30','folder'=>$this->folder),
		//'big_image'=>array('type'=>'image','name'=>l('Imagine mare'),'text'=>'770x110','folder'=>$this->folder)
		);
	}
	
	function show_tabs_2($date) {
		print_form($this->form_3,$this,$date);
	}
	function show_tabs_1($date) {
		print_form($this->form_2,$this,$date);
		return false;

		print_tabs_form(array(
		'position'=>$this->form['position']['name'],
		'capacity'=>$this->form['capacity']['name'],
		'hotel_facility'=>$this->form['hotel_facility']['name'],
		'room_facility'=>$this->form['room_facility']['name'],
		'free_services'=>$this->form['free_services']['name'],
		'payed_services'=>$this->form['payed_services']['name'],
		));
		?>

		<?php
	}
	
	function __call($mod,$arg) {
		$arg=$arg[0];
		$this->json_list_filter($arg['id_category'],$arg['id'],str_replace("nss_univ_","",$mod));
		?>
		
		<input style="margin-left:28%;margin-top:20px" type="button" class="ui-state-default ui-corner-all padding_2 btn_editor" value="Insereaza " onclick="var $_add=''; $(this).parent().parent().find('.add_filter_add').each(function( index ) {$_add+=this.value+'<br />'; } );  $(this).parent().parent().find('.nss_editor').html($(this).parent().parent().find('.nss_editor').html()+'<br />'+$_add);  $(this).parent().parent().find('.nss_editor_text').html($(this).parent().parent().find('.nss_editor').html());   " />
		
		<?php
	}
	function json_list_filter($id_category=0,$id=0,$on_this="") {
		global $db;
		//echo a;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `xp_categories` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_hotel` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_hotel`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `xp_filters`  WHERE `id_category`=2 order by `order` asc " );
		//echo "SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." order by `order` asc ";
		foreach ($filters as $filter) {
			if (!empty($on_this) && $filter['name']!=$on_this)  continue; else $this->used_filters[]=$on_this;
			if (empty($on_this) && in_array($filter['name'],$this->used_filters)) continue;
		?>
		<dl>
		<dt>

			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label>
		</dt>
		<dd>
			<?php
			$_add='';
			$options=$db->fetch_all("SELECT  * FROM `xp_filters_options` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {


				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `xp_hotels_filters_values` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
				if ($exist)$_add.=$option['name'].", ";
				//				print_a("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input onclick="var $_add=''; $(this).parent().parent().find('input').each(function( index ) { if(this.checked==true) $_add+=$(this).attr('title')+', '; } );  $('#filter_2_<?=$filter['id']?>').val($_add.substr(0, ($_add.length - 2)));" type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> title="<?=$option['name']?>" /> <?=$option['name']?></b>

			</div>
			<?php } ?>
		</dd>

			<input type="hidden" value="<?=$filter['name']?>" id="filter_<?=$filter['id']?>" />
			<input type="hidden" value="<?=$_add?>" class="add_filter_add" id="filter_2_<?=$filter['id']?>" />
		</dl>
	<?php }
	}
	
	function show_state($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_country"><?=l('Tara')?></label>
			</dt>
			<dd>
				<select name="id_country" id="id_country" style="width:200px" onchange="json_list_filter2(this.value,<?=(int)$date['id_country']?>);">
				<?php $this->json_state($date['id_country']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function show_city($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_region"><?=l('Regiunea')?></label>
			</dt>
			<dd>
				<select name="id_region" id="id_region" style="width:200px" onchange="json_list_filter3(this.value,<?=(int)$date['id_region']?>);">
				<?php $this->json_city($date['id_country'],$date['id_region']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_state($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Selectati tara</option><?php
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `fibula_countries` p
							inner join `fibula_countries_data` pd
							 on p.`id`=pd.`_id` WHERE isnull(p.`id_parent`) ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_city($id_state,$id_brand=0) {
		global $db;

		if (empty($id_state)) {
			?><option>Selectati regiunea</option><?php
			return false;
		}
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `fibula_countries` p
							inner join `fibula_countries_data` pd
							 on p.`id`=pd.`_id` WHERE p.`id_parent`=".$id_state." and pd.`lang`='ro'  ORDER BY `name` ASC");
		
	
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_city2($id_brand=0) {
		global $db;

		
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `fibula_countries` p
							inner join `fibula_countries_data` pd
							 on p.`id`=pd.`_id` WHERE p.`id_parent`>0 and p.`status`=1 and pd.`lang`='ro' ORDER BY `name` ASC");
		
	
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}

	function json_list() {
	if (isset($_GET['idprod'])) {
		$new_sql="select sql_calc_found_rows
					p.*,
					pd.`name`,
					pd.`address`,
					pd.`postal_code`,
					pd.`telephone`,
					pd.`fax`,
					pd.`website`,
					pd.`email`,
					pd.`name_seo`,
					pd.`rating2`,
					pd.`description`

				from
					`{$this->table}` p
					inner join `fibula_hotels_data` pd
								on p.`id`=pd.`_id`
					
					
					where p.`id`=".$_GET['idprod'];
	} else {
		$new_sql="select sql_calc_found_rows
					p.*,
					pd.`name`,
					pd.`address`,
					pd.`postal_code`,
					pd.`telephone`,
					pd.`fax`,
					pd.`website`,
					pd.`email`,
					pd.`name_seo`,
					pd.`rating2`,
					pd.`description`

				from
					`{$this->table}` p
					inner join `fibula_hotels_data` pd
								on p.`id`=pd.`_id`
					
					
					where 1";
	}

		json_list($this,false,$new_sql);
	}

	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	
	function generate_select2() {
		$_GET['from_grid']=1;
		$this->json_state();
	}
	
	function generate_select3() {
		$_GET['from_grid']=1;
		$this->json_city2();
	}
	
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function json_list_filter2(id_country,id){
		var id_city=0;
		$("#id_region").load('module/<?=$this->module?>.php?json_city=1&id_country='+id_country+'&id='+id);
		//$("#id_city").load('module/<?=$this->module?>.php?json_city=1&id_country='+id_country+'&id='+id);
		$("#id_hotel").load('module/<?=$this->module?>.php?json_hotel=1&id_region='+id_city+'&id='+id);

		}
	function json_list_filter3(id_region,id){

		$("#id_hotel").load('module/<?=$this->module?>.php?json_hotel=1&id_region='+id_region+'&id='+id);

		}
		
		function json_list_filter4(id_region,id){

		$("#id_city").load('module/<?=$this->module?>.php?json_city=1&id_region='+id_region+'&id='+id);

		}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

		$row['id_region']=$db->fetch_one("select `name` from `fibula_countries_data` WHERE `_id`={$row['id_region']} ");
		$row['id_country']=$db->fetch_one("select `name` from `fibula_countries_data` WHERE `_id`={$row['id_country']} ");
		
		$row['actions']="<a class=\"action\" href=\"".BASEHREF."admin/image3.php?id_product=".$row['id']."\">Galerie foto</a><br/><br/>".$row['actions'];
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') {
			global_delete($this->table);
		}
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `fibula_hotels_images` WHERE `id_hotel`=".$id);
			foreach ($images as $image) unlink(SITEROOT."/static/i/hotel-fibula/".$image);
			$db->delete("fibula_hotels_data",'_id='.$id);


		}
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Oras')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px">
				<?php $this->json_categories($date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_brand=0) {
		global $db;

		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `xp_cities` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_cities_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			//$date_admin = lang_fetch($this->table,"`id`=".$id);
			$date_admin = $db->fetch("
				select
					p.*,
					pd.`name`,
					pd.`address`,
					pd.`postal_code`,
					pd.`telephone`,
					pd.`fax`,
					pd.`website`,
					pd.`email`,
					pd.`name_seo`,
					pd.`description`,
					pd.`directions`,
					pd.`rating2`,
					pd.`facilities`
				from
					`fibula_hotels` p
					inner join `fibula_hotels_data` pd
								on p.`id`=pd.`_id`


				where p.`id`={$id}
			");
			
			
			foreach ($db->fetch_all("SELECT * FROM `xp_tabs` WHERE `id_category`=2 order by `order`" ) as $_line){


			$date_admin['add_tab_'.$_line['id']]=$db->fetch_one("SELECT `description` FROM `xp_hotel_to_tabs` WHERE `id_product`=".$id." AND `id_tab`=".$_line['id']);

			}
		}
		
		
	//	print_a($date_admin);
		
//		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//		} else $date_admin['image']='';
//
//		if ($date_admin['big_image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['big_image'])){
//			$date_admin['big_image']='static/i/'.$this->folder.'/'.$date_admin['big_image'];
//		} else $date_admin['big_image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica Hoteluri'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;
		
		

		$data_filters=$data['filters'];
//		foreach(
//			$data['nss_languages'] as
//			$lang => $d
//		){
//			$data['nss_languages'][$lang]['name_seo'] = url($data['nss_languages'][$lang]['name'], $lang);
//		}

		if ($id) {

			$db->sql("DELETE FROM `xp_hotel_to_tabs` WHERE `id_product`=".$id);
	
			foreach ($db->fetch_all("SELECT * FROM `xp_tabs` WHERE `id_category`=2 order by `order`" ) as $_line){
				//$data['add_tab_'.$_line['id']]=$tabs['add_tab_'.$_line['id']];
	
	
				$db->insert('xp_hotel_to_tabs',array(
				'id_product'=>$id,
				'id_tab'=>$_line['id'],
				'description'=>$data['add_tab_'.$_line['id']],
				));
	
	
				unset($data['add_tab_'.$_line['id']]);
			}
		} else {
			foreach ($db->fetch_all("SELECT * FROM `xp_tabs` WHERE `id_category`=2 order by `order`" ) as $_line){
				
	
				unset($data['add_tab_'.$_line['id']]);
			}
		}
		
		if ($id) {
			$data['name_seo']=escapeIlegalChars($data['name']," ");
		} else {
			$data['name_seo']=escapeIlegalChars($data['name']," ");
		}
			$data2=array(

					'name'=>$data['name'],
					'name_seo'=>$data['name_seo'],
					'website'=>$data['website'],
					'email'=>$data['email'],
					'telephone'=>$data['telephone'],
					'fax'=>$data['fax'],
					'address'=>$data['address'],
					'postal_code'=>$data['postal_code'],
					'rating2'=>$data['rating2'],
					'lang'=>'ro',
					//'description'=>$data['description']
					);
		if (isset($data['description'])) $data2['description']=$data['description'];
		if (isset($data['directions'])) $data2['directions']=$data['directions'];
		if (isset($data['facilities'])) $data2['facilities']=$data['facilities'];
		unset($data['name_seo']);
		unset($data['name']);
		unset($data['website']);
		unset($data['email']);
		unset($data['telephone']);
		unset($data['fax']);
		unset($data['address']);
		unset($data['rating2']);
		unset($data['postal_code']);
		//unset($data['id_country']);
		unset($data['description']);
		unset($data['directions']);
		unset($data['facilities']);
		unset($data['filters']);
		
		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'],'-'),'static/i/'.$this->folder,''));
//			$data['big_image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['big_image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'].' big','-'),'static/i/'.$this->folder,''));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				$db->update($this->table, $data," `id`=".$id);
					//$data2['name_seo']=$data2['name_seo']." ".$id;
				$db->update($this->table.'_data', $data2," `_id`=".$id);
				print_alerta('Hotelul a fost updatat');
			} else {
				$data['owner']='Mondial';
				$db->insert($this->table,$data);
				$id=mysql_insert_id();
				$data2['_id']=$id;
				$data2['name_seo']=$data2['name_seo']." ".$id;
				$db->insert($this->table.'_data',$data2);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new hotels2_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_list_filter']==1) $module->json_list_filter(fget('id_category'),fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['json_state']==1) $module->json_state(fget('id'));
elseif ($_GET['json_city']==1) $module->json_city(fget('id_country'),fget('id'));
elseif ($_GET['json_city2']==1) $module->json_city2(fget('id_country'),fget('id'));
elseif ($_GET['json_hotel']==1) $module->json_hotel(fget('id_country'),fget('id_region'),fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>