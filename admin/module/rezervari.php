<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class rezervari_module {
	var $module='rezervari';
	var $date='25-08-2009';
	var $table=TABLE_COMENZI;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function rezervari_module() {

		$this->name=l('Rezervari online');
		$this->title=l('Rezervari online');
		$this->description=l('Rezervari online');

		$this->type=array(
		'like'=>array('id','name','last_name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);
		$this->folder='images';
		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		//		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('First name'),'width'=>100),
		'last_name'=>array('name'=>l('Last name'),'width'=>100),
		'persons'=>array('name'=>l('Nr persoane'),'width'=>50,'align'=>'center'),
		'starting_date'=>array('name'=>l('Data rezervarii'),'width'=>100,'align'=>'center'),
		'days'=>array('name'=>l('Numar de zile'),'width'=>100,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>120,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume pagina'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2,max_100','style'=>'width:70%;','auto'=>'name_seo,header_title,title,meta_keywords,meta_description'),
		'title'=>array('type'=>'input','name'=>l('Title, Alt'),'lang'=>true,'valid'=>'empty,min_2,max_200','style'=>'width:70%;'),
		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:70%;'),
		//'publicat'=>array('type'=>'date','name'=>l('Publicata la data'),'style'=>'width:70%;','info'=>'Ex.: 14-03-2010'),
		//'sursa'=>array('type'=>'input','lang'=>true,'name'=>l('Sursa externa'),'style'=>'width:70%;','info'=>'www.protv.ro'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		1=>'show_categories',
		'content'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		//'short_content'=>array('type'=>'text','name'=>l('Descriere scurta'),'lang'=>true),
		//51=>'show_editor_jmeker',
		//52=>'show_uploader1',
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder)
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		if ($row['type']==0) {
			$row['type']='<img src="images/tick.gif"  width="16" height="16" alt="tick"  />';
		} else {
			$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
			$row['actions']='<a class="action edit" id="id_line_'.$row['id'].'">'.l('edit').'</a>';
		}
		return $row;
	}
	function grid_edit(){
		global_delete($this->table);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie site')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<option value="0">Nici una</option>
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$categories=lang_fetch_all(TABLE_CATEGORIES," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=lang_fetch($this->table,"`id`=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica pagini site'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$gd;
		$admin_info=adm_info();
		$languages=get_languages();
		$data=$_POST;
		//print_a($data);
		foreach ($data['nss_languages'] as $lang=>$vars){
			$data['nss_languages'][$lang]['name_seo']=escapeIlegalChars($data['nss_languages'][$lang]['name_seo']," ");
			//$data['nss_languages'][$lang]['content']=$data['content'];
		}
		/*file1*/
		if (!empty($data['image'])) {
			$data['filed']=$data['image'];
			$ext=file_ext($data['filed']);
			$data['image']=$this->folder.'/'.escapeIlegalChars($data['nss_languages'][LANG]['name_seo']).'.'.$ext;
			list($width, $height, $ftype)=@getimagesize(SITEROOT."/".$data['filed']);
			//print_a($data);
			if (($width!=62) || ($height!=59)) {
				$gd->move_upload(SITEROOT."/".$data['filed'],SITEROOT."/".$data['image'],'UM4MBR170x123sl-#EFFCFF');
				@unlink(SITEROOT."/".$data['filed']);
			} else {
				rename(SITEROOT.'/'.$data['filed'],SITEROOT.'/'.$data['image']);
			}
		} else {
			$data['image']='';
		}
		unset($data['filed']);
		unset($data['content']);
		//print_a($data);
		$errors=form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				//unset($data['sursa']);
				lang_update($this->table, $data," `id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function show_uploader1($date) {
		global $db,$gd;
		$ceva=time();
	?>
	<dl>
		<dt>
			<label for="files"><?=l('Imagine')?></label>
		</dt>
		<dd id="uploader-1-<?=$ceva?>">
			<div class="files">
				<div class="wrapper"><a class="button">&nbsp;</a></div>
				<ol>
					<?
					if (!empty($date['image'])) {
						if (file_exists(SITEROOT.'/'.$date['image'])) {
							echo "
								<li>
								 	<div class=\"img\" style=\"background:url('".BASEHREF.$gd->url('resize',$date['image'],'110x88s-#eff7e6')."');\"></div>
								 	<span onclick=\"delete_file('".$date['image']."',this)\">".l('Sterge poza')."</span>
									<input type=\"hidden\" name=\"image[]\" value=\"{$date['image']}\"/>
								</li>
							";
						}
					}
					?>
				</ol>
				<div class="clear"></div>
			</div>
		</dd>
	</dl>
	<script type="text/javascript">
	$(document).ready(function () {activate_uploader('uploader-1-<?=$ceva?>','<?=$this->folder?>',1,'jpg|jpeg|gif|png','image'); });
	</script>
	<?
	}
}
$module=new rezervari_module();
if ($module_info) $this_module=$module;
elseif ($module_js) {
	?>
	var global_window=true;
	<?
	set_grid($module,array('multiselect'=>true,'sortorder'=>'desc'));
}
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module,false);
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>