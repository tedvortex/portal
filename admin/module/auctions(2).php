<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class auctions_module {
	var	$module='auctions';
	var $date='27-08-2009';
	var $table=TABLE_AUCTIONS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function auctions_module() {

		$this->name=l('Auctions');
		$this->title=l('Auctions');
		$this->description=l('Auctions');

		$this->type=array(
		'like'=>array('name','id','last_bid'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		0=>array('order'=>'id desc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'name'=>array('name'=>l('Product name'),'width'=>200),
		'last_bid'=>array('name'=>l('Last bider'),'width'=>100,'align'=>'center'),
		'start_date'=>array('name'=>l('Auction date'),'width'=>100,'align'=>'center'),
		//'time_add'=>array('name'=>l('Data de start'),'width'=>100,'align'=>'center'),
		//	'reset_time'=>array('name'=>l('Reset time'),'width'=>60,'align'=>'center'),
		//'credite'=>array('name'=>l('Credite'),'width'=>100),
		'end_date'=>array('name'=>l('Data de final'),'width'=>100,'align'=>'center'),
		'max_price'=>array('name'=>l('Pret maxim'),'width'=>60,'align'=>'center'),
		//'price'=>array('name'=>l('Valoare produs'),'width'=>60,'align'=>'center'),
		'price'=>array('name'=>l('Final price'),'width'=>80,'align'=>'center'),
		'bonus'=>array('name'=>l('Bonus'),'width'=>100,'align'=>'center'),
		'end'=>array('name'=>l('Auction state'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Terminata').';0:'.l('Neterminata'))),
		//'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),

		);


//		$infoAuction['start_date']=time();
//		ob_start();
//			?>
//			  <?=l('Hour')?> :
//
//			<select name="ora">
//			<?php for ($i=0;$i<24;$i++)	{?>
//			<option <?=(int)date('H',$infoAuction['start_date'])==$i?'selected':''?>><?=$i?></option>
//			<?php } ?>
//			</select>:
//			<select name="minutul">
//			<?php for ($i=0;$i<60;$i++)	{?>
//			<option <?=(int)date('i',$infoAuction['start_date'])==$i?'selected':''?>><?=$i?></option>
//			<?php } ?>
//			</select>:
//			<select name="secunda">
//			<?php for ($i=0;$i<60;$i++)	{?>
//			<option <?=(int)date('s',$infoAuction['start_date'])==$i?'selected':''?>><?=$i?></option>
//			<?php } ?>
//			</select>
//			<?php
//
//			$text=ob_get_contents();
//			ob_end_clean();

			$this->form=array(
			0=>'',
			1=>'form_product_select',
			'start_date'=>array('type'=>'date','name'=>l('Start date'),'valid'=>'empty,min_2,max_140','style'=>'width:80px;','text'=>$text,'info'=>l('Data licitatie')),
			//'start_date_2'=>array('type'=>'date','name'=>l('Data de start'),'valid'=>'empty,min_2,max_140','style'=>'width:80px;','text'=>$text2,'info'=>'Data de start'),
			'time_add'=>array('type'=>'input','name'=>l('Auction duration'),'valid'=>'empty,max_11,min_7','style'=>'width:80px;','text'=>l(' zz : hh : mm : ss'),'info'=>l('Ex: 00:00:00:30 , inseamna 30 secunde')),
			'bid_price'=>array('type'=>'input','name'=>l('Nr credite pe oferta'),'valid'=>'empty','style'=>'width:80px;','text'=>l('bids')),
			'bids'=>array('type'=>'input','name'=>l('Numar maxim oferte'),'valid'=>'empty','style'=>'width:80px;'),
			'max_price'=>array('type'=>'input','name'=>l('Pret maxim'),'style'=>'width:200px;'),
			//'price'=>array('type'=>'input','name'=>l('Pret produs'),'style'=>'width:200px;'),
			'hours_add'=>array('type'=>'input','name'=>l('Ore adaugate'),'style'=>'width:80px;','text'=>' Ore'),
			'hours_burning'=>array('type'=>'input','name'=>l('Ore adaugate dupa nr max biduri'),'style'=>'width:80px;','text'=>' Ore'),
			'initial_free'=>array('type'=>'input','name'=>l('Limita biduri gratuite'),'style'=>'width:80px;'),

			//'e24h'=>array('type'=>'checkbox','name'=>l('24 H'),'text'=>'Da','info'=>'Licitatie 24 ore'),
			//'ebegin'=>array('type'=>'checkbox','name'=>l('Free bid')),
			//'bonus'=>array('type'=>'radio','name'=>l('Bonus credite'),'options'=>array(
			//	0=>'Fara bonus',
			//	1=>'Bonus 50',
			//	2=>'Bonus 100',
			//	3=>'B0nus 200',
			//	4=>'Bonus 300',
			//	)),
			//'bonus'=>array('type'=>'input','name'=>l('Bonus'),'text'=>'Da','info'=>'Bonus credite acordate la finalul licitatiei'),
			'status'=>array('type'=>'radio','options'=>array(1=>l('Active'),0=>l('Inactive')),'name'=>l('status')),
			//2=>l('Criterii restart time'),
			//3=>'criterii_restart',
			4=>l('Numar biduri gratuite'),
			5=>'criterii_restart_2',
			6=>l('50 %'),
			7=>'criterii_restart_3',
			);
			$this->form_mail=array(
			//1=>'select_letter',
			'subject'=>array('type'=>'input','name'=>l('Subiect email'),'style'=>'width:98%;','valid'=>'empty,min_10,max_200'),
			'emails'=>array('type'=>'text','name'=>l('Email'),'style'=>'width:98%; height:140px;','valid'=>'empty,min_10,max_200'),
			//'logs'=>array('type'=>'checkbox','name'=>l('Salveaza loguri'),'value'=>1),
			//'letter'=>array('type'=>'checkbox','name'=>l('Salveaza scrisoare'),'value'=>1),
			//'link'=>array('type'=>'checkbox','name'=>l('Link dezabonare'),'value'=>1,'text'=>l('pentru userii abonati')),
			'message'=>array('type'=>'editor','name'=>l('Mesaj'),'width'=>732),
			);
	}
	function json_list() {
		json_list($this,false);
	}
	function css() {
		?>
		.module_menu li.update_status {
			background-image:url('../../images/icons/vezi_biduri.png');
		}
		.module_menu li.vezi_clienti {
			background-image:url('../../images/icons/vezi_clienti.png');
		}
		.module_menu li.back_bid {
			background-image:url('../../images/icons/returneaza_credite.png');
		}
		.module_menu li.email_new {
			background-image:url('../../images/icons/send_email.png');
		}
		.module_menu li.update_multistatus { background-image:url('../../images/icons/update_multistatus.png'); }
		<?php
	}
	function criterii_restart_3($date) {
		global $db;
		?>
		<div>
		<?php
		if (!empty($date['id'])) $specifications_options=$db->fetch_all("SELECT * FROM `".TABLE_AUCTIONS_50."` WHERE `id_auction`=".$date['id']." ORDER BY `date_start` ASC");
		$specifications_options=array_merge((array)$specifications_options,array(1=>array('id'=>'new','date_start'=>time())));
		foreach ($specifications_options as $linie) {
			?>
			<div style="padding:1px;">
				<input type="text" class="" name="date_50_start[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:20%;" value="<?=date('d-m-Y H:i',$linie['date_start'])?>" align="absmiddle"  />
				- <input type="text" class="" name="date_50_end[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:20%;" value="<?=(!empty($linie['date_end'])?date('d-m-Y H:i',$linie['date_end']):"")?>" align="absmiddle"  />
				<?php if($linie['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="add"  />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="rem"  />
			</div>
			<?php
		}
		?>
		</div>
		<?php
	}
	function criterii_restart_2($date) {
		global $db;
		?>
		<div>
		<?php
		if (!empty($date['id'])) $specifications_options=$db->fetch_all("SELECT * FROM `".TABLE_FREE_BIDS."` WHERE `id_auction`=".$date['id']." ORDER BY `date` ASC");
		$specifications_options=array_merge((array)$specifications_options,array(1=>array('id'=>'new','date'=>time(),'date_to'=>time())));
		foreach ($specifications_options as $linie) {
			?>
			<div style="padding:1px;">
				De la <input type="text" class="datepicker3" name="date_reset[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:20%;" value="<?=date('d-m-Y H:i',$linie['date'])?>" align="absmiddle"  />
				<!--<input type="text" class="datetimepicker2" name="time_reset[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:10%;" value="<?=$linie['time']?>" align="absmiddle"  />-->
				  &raquo;
				<input type="text" name="bids_reset[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:10%;" value="<?=$linie['bids']?>" align="absmiddle"  /> bids
				<input type="text" name="per_user[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:10%;" value="<?=$linie['per_user']?>" align="absmiddle"  /> bids/user
				&raquo; Pana la <input type="text" class="datepicker3" name="date_to[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:20%;" value="<?=date('d-m-Y H:i',$linie['date_to'])?>" align="absmiddle"  />
				<?php if($linie['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="add"  />  <?php } ?>
				<!--onclick="setTimeout('init_tip',10);"-->
				<img src="images/delicon.gif" align="absmiddle"  class="rem"  />
			</div>
			<?php
		}
		?>
		</div>
		<?php
	}
	function criterii_restart($date) {
		global $db;
		?>
		<div>
		<?php
		if (!empty($date['id'])) $specifications_options=$db->fetch_all("SELECT * FROM `".TABLE_RESTART."` WHERE `id_auction`=".$date['id']." ORDER BY `seconds` DESC");
		$specifications_options=array_merge((array)$specifications_options,array(1=>array('id'=>'new')));
		foreach ($specifications_options as $linie) {
			?>
			<div style="padding:1px;">
				<input type="hidden" name="order_2[]" value="<?=$linie['id']?>">
				<input type="text" name="seconds[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:10%;" value="<?=$linie['seconds']?>" align="absmiddle"  /> seconds &raquo;
				<input type="text" name="bids[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:10%;" value="<?=$linie['bids']?>" align="absmiddle"  /> bids
				<?php if($linie['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="add"  />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="rem"  />
			</div>
			<?php
		}
		?>
		</div>
		<?php
	}
	function form_product_select($date_selected) {
		global $db;
		?>
			<dl>
			<dt>
			<label for="id_product_module"><?=l('Auction product')?></label>
			</dt>
			<dd>
			<select name="id_product" id="id_product">
			<option value="0"><?=l('Selectati produsul')?></option>
			<?php

			$sql="SELECT * FROM `".TABLE_PRODUCTS."`,`".TABLE_PRODUCTS.TABLE_EXTEND."`
			WHERE `".TABLE_PRODUCTS."`.`id`=`".TABLE_PRODUCTS.TABLE_EXTEND."`.`id_main`
			AND `".TABLE_PRODUCTS.TABLE_EXTEND."`.`lang`='".LANG."' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC LIMIT 100";
			//echo $sql;
			$products=$db->fetch_all($sql);

			foreach ($products as $linie) {
				?>
				<option value="<?=$linie['id_main']?>" <?=($linie['id_main']==$date_selected['id_product'])?'selected':''?> ><?=limit_string($linie['name'],60)?> </option>
				<?php
			}
			?>
			</select>
			</dd>
			</dl>
			<?php

	}
	function json_list_row($row) {
		global $gd,$db;

		$row['name']=$db->fetch_one("SELECT `name` FROM `".TABLE_PRODUCTS.TABLE_EXTEND."` WHERE `lang`='".LANG."' AND  `id_main`=".$row['id_product']);
		$row['image']=image_link('static/imagini-produse/'.(string)$db->fetch_one("SELECT `image` FROM `".TABLE_PRODUCTS_IMAGES."` WHERE  `id_product`=".$row['id_product']),40);
		$row['time_add']=date('Y-m-d H:i:s',$row['time_add']+$row['start_date']);
		$row['start_date']=date('Y-m-d H:i:s',$row['start_date']);
		if ($row['bonus']==1) {
			$row['bonus']='50';
		} elseif ($row['bonus']==2) {
			$row['bonus']='100';
		} elseif ($row['bonus']==3) {
			$row['bonus']='200';
		} elseif ($row['bonus']==4) {
			$row['bonus']='300';
		}
		if ($row['end']==1) {
			$row['end_date']=date('Y-m-d H:i:s',$row['end_date']);
		} else {
			$row['end_date']='-';
		}
		//$row['reset_time']=implode(":",return_date($row['reset_time']));
		if ($row['end']==1) {
			$status_licitatie=l('Terminata');
		} else {

			if ($row['start_date']<time()) {
				$status_licitatie=l('Inceputa');
			} else $status_licitatie=l('Neinceputa');
		}
		$row['end']='<b>'.$status_licitatie.'</b>';
		if (!empty($row['image']))	 $row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			
			$date_saved['time_add']=strTime($date_saved['end_date']-$date_saved['start_date']);
			//$date_saved['hours_add']=$date_saved['hours_add']/3600;
			//$date_saved['reset_time']=implode(":",return_date($date_saved['reset_time']));

		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">
		$(document).ready(function (){
			init_tip();
		});
		</script>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);

			//	print_a($_POST);


			$date_saved['hours_add']=$date_saved['hours_add']*3600;

			$start_date=explode('.',$_POST['start_date']);
			//$starOra=explode(':',$_POST['ora']);
			$start_date=mktime((int)$_POST['ora'],(int)$_POST['minutul'],(int)$_POST['secunda'],$start_date[1],$start_date[0],$start_date[2]);

			$data['time_add']=explode(':',$data['time_add']);

			//	$data['reset_time']=explode(':',$data['reset_time']);
		//	echo $data['time_add'][0].' '.$data['time_add'][1].' '.$data['time_add'][2].' '.$data['time_add'][3];
			$data['price']=$data['start_price'];
			$data['time_add']=(int)$data['time_add'][0]*3600*24+(int)$data['time_add'][1]*3600+(int)$data['time_add'][2]*60+(int)$data['time_add'][3];
			//	$data['reset_time']=(int)$data['reset_time'][0]*3600*24+(int)$data['reset_time'][1]*3600+(int)$data['reset_time'][2]*60+(int)$data['reset_time'][3];
			$startDate=explode('-',$data['start_date']);
			$data['start_date']=mktime((int)$data['ora'],(int)$data['minutul'],(int)$data['secunda'],$startDate[1],$startDate[0],$startDate[2]);
			//echo $data['time_add'];
			//echo date('H:i:s',$data['time_add']);
			//$startDate=explode('-',$data['start_date_2']);
			//$data['start_date_2']=mktime((int)$data['ora_2'],(int)$data['minutul_2'],(int)$data['secunda_2'],$startDate[1],$startDate[0],$startDate[2]);

			//$data['time_add']=$data['start_date_2']-$data['start_date'];

			$data['end_date']=$data['start_date']+$data['time_add'];

			unset($data['type_op'],

			$data['time_add'],
			$data['ora'],
			$data['ora_2'],
			$data['minutul'],
			$data['minutul_2'],
			$data['secunda'],
			$data['secunda_2'],
			$data['start_date_2'],
			$data['bids_reset'],
			$data['per_user'],
			$data['date_reset'],
			$data['date_to'],
			$data['date_reset2'],
			$data['date_50_end'],
			$data['date_50_start'],
			$data['prices'],$data['seconds'],$data['order_2']);

			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
				$id=mysql_insert_id();
			}

		/*	if (is_array($_POST['bids'])) {
				$all_ids=array();
				foreach ($_POST['order_2'] as $order=>$key) {
					$value=$_POST['bids'][$key];
					if (is_numeric($key) && $key!='new') {
						$all_ids[]=$key;
						if (!empty($value)) {
							$array_insert=array('bids'=>trim($value));
							$array_insert['id_auction']=$id;
							$array_insert['seconds']=$_POST['seconds'][$key];

							$db->update(TABLE_RESTART,$array_insert," `id`=".$key);
						}
					} else {
						foreach ($value as $key_2=>$value_ins) {
							if (!empty($value_ins)) {
								$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_RESTART."`  WHERE `bids`='".trim($value_ins)."' AND `id_auction`=".$id);
								if (empty($exist)) {
									$array_insert=array('bids'=>trim($value_ins));
									$array_insert['id_auction']=$id;
									$array_insert['seconds']=$_POST['seconds']['new'][$key_2];
									$db->insert(TABLE_RESTART,$array_insert);
									$all_ids[]=$db->insert_id();
								}
							}

						}
					}
				}
				if (empty($all_ids)) $all_ids=array(0);
				$db->delete(TABLE_RESTART,"   `id` NOT IN (".implode(",",$all_ids).") AND `id_auction`=".$id."");
			}
*/
			if (is_array($_POST['date_50_start'])) {
				$all_ids=array();
				foreach ($_POST['date_50_start'] as $key=>$value) {
					if (is_numeric($key) && $key!='new') {
						$value=strtotime(trim($value));
						$all_ids[]=$key;
						if (!empty($value) && !empty($_POST['date_50_end'][$key])) {
							$array_insert=array('date_start'=>$value);
							$array_insert['id_auction']=$id;
							$array_insert['date_end']=strtotime(trim($_POST['date_50_end'][$key]));
							$db->update(TABLE_AUCTIONS_50,$array_insert," `id`=".$key);
						}
					} else {
						foreach ($value as $key_2=>$value_ins) {
							$value_ins=strtotime(trim($value_ins));
							if (!empty($value_ins) && !empty($_POST['date_50_end']['new'][$key_2])) {
								$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_AUCTIONS_50."`  WHERE `date_start`='".($value_ins)."' AND `id_auction`=".$id);
								if (empty($exist)) {
									$array_insert=array('date_start'=>trim($value_ins));
									$array_insert['id_auction']=$id;
									$array_insert['date_end']=strtotime(trim($_POST['date_50_end']['new'][$key_2]));
									$db->insert(TABLE_AUCTIONS_50,$array_insert);
									$all_ids[]=$db->insert_id();
								}
							}

						}
					}
				}
				if (empty($all_ids)) $all_ids=array(0);
				$db->delete(TABLE_AUCTIONS_50,"   `id` NOT IN (".implode(",",$all_ids).") AND `id_auction`=".$id."");
			}
			if (is_array($_POST['date_reset'])) {
				//print_a($_POST);
				$all_ids=array();
				foreach ($_POST['date_reset'] as $key=>$value) {
					if (is_numeric($key) && $key!='new') {
						$value=strtotime(trim($value));
						$all_ids[]=$key;
						if (!empty($value) && !empty($_POST['bids_reset'][$key])) {
							
							$array_insert=array('date'=>$value);
							$array_insert['id_auction']=$id;
							$array_insert['date_to']=strtotime(trim($_POST['date_to'][$key]));
							$array_insert['bids']=(int)$_POST['bids_reset'][$key];
							$array_insert['per_user']=(int)$_POST['per_user'][$key];
						//	print_a($array_insert);
							$db->update(TABLE_FREE_BIDS,$array_insert," `id`=".$key);
						}
					} else {
						foreach ($value as $key_2=>$value_ins) {
							$value_ins=strtotime(trim($value_ins));
							if (!empty($value_ins) && !empty($_POST['bids_reset']['new'][$key_2])) {
								$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FREE_BIDS."`  WHERE `date`='".($value_ins)."' AND `id_auction`=".$id);
								if (empty($exist)) {
									$array_insert=array('date'=>trim($value_ins));
									$array_insert['id_auction']=$id;
									$array_insert['date_to']=strtotime(trim($_POST['date_to']['new'][$key_2]));
									$array_insert['bids']=(int)$_POST['bids_reset']['new'][$key_2];
									$array_insert['per_user']=(int)$_POST['per_user']['new'][$key_2];
									print_a($array_insert);
									$db->insert(TABLE_FREE_BIDS,$array_insert);
									$all_ids[]=$db->insert_id();
								}
							}

						}
					}
				}
				if (empty($all_ids)) $all_ids=array(0);
			//	$db->delete(TABLE_FREE_BIDS,"   `id` NOT IN (".implode(",",$all_ids).") AND `id_auction`=".$id."");
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function mail($id=0) {
		global  $db,$main_buttons;
		//if (!empty($id)) {
			$auctionuser=$db->fetch_all("select DISTINCT `id_user` from `xp_bids` where `id_auction`=".$_GET['to']);
		//}
		//$id=$auctionuser[0]['id_user'];
		?>
		<form action="?mod=<?=$this->module?>&action=send&id=<?=$id?>" method="POST">
		<?php
		$ids=explode(",",$_GET['to']);
		$emails="";
		foreach ($auctionuser as $userid) {
			$e=$db->fetch_one("SELECT `email` FROM `".TABLE_CUSTOMERS."` WHERE id=".$userid['id_user']);
			//print_a($e);
			if (trim($e)!='') $emails.=htmlentities("\r\n".$e);
		}
		//print_a($emails);
//		$emailsAddress=$db->fetch_all("SELECT * FROM `xp_customers` WHERE status=1");
//		foreach ($emailsAddress as $email) {
//			if (preg_match('/^[^@]+@[a-zA-Z0-9-_.-]+?\.[a-zA-Z0-9-]{2,4}$/',$email['email'])) {
//				if ($_GET['to']=='all') {
//					$emails.=htmlentities("\r\n".$email['email']);
//				} elseif (in_array($email['id'],$ids)) {
//					$emails.=htmlentities("\r\n".$email['email']);
//				}
//			}
//		}
		$this->form_mail['emails']['value']=$emails;
		print_form_header(l('New Email'));
		print_form($this->form_mail,$this,$date_saved);
		print_form_footer();
		unset($main_buttons['continue']);
		$main_buttons['save']=array('type'=>'submit','value'=>l('Trimite'));
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function send() {
		global $db,$config;
		$table=$this->table;
	//	$swift =& new Swift(new Swift_Connection_Sendmail());
		$_POST['content']=preg_replace('/(href="|src="|action=")(?!#|[a-zA-Z]+:|")\/?([^"]+)(")/i','$1'.$config['SITE_WEB_ROOT'].'$2$3',$_POST['message']);
		//verificam daca punem link de dezabonare sau nu
		//if ($_POST['link']==1) {
		//	$_POST['message'].='<div style="text-align:right;margin-top:10px;"><a target="_blank" href="'.$config['SITE_WEB_ROOT'].'newsletter.html?tip=del&id={USERID}&cod={RANDNUMBER}">Click aici pentru a va dezabona.</a></div>';
		//}
		//end
		//$message =& new Swift_Message($_POST['subject'], $_POST['message'], "text/html");
		//$message->setReplyTo($config['REPLY_EMAIL']);
		/*	if ($_FILES['fisier']['tmp_name']!='') {
		$message->attach(new Swift_Message_Part($_POST['message'],"text/html"));
		$attachment =& new Swift_Message_Attachment(file_get_contents($_FILES['fisier']['tmp_name']),$_FILES['fisier']['name']);
		//		$file =& new Swift_File($_FILES['fisier']['tmp_name']);
		//		$attachment =& new Swift_Message_Attachment($file);
		$message->attach($attachment);
		}*/
		//$recipients =& new Swift_RecipientList();
		$arrmails=explode("\n",$_POST['emails']);
		//print_a($arrmails);
		$arrreplacements=array();
		foreach ($arrmails as $email) {
			//$recipients->addTo(trim($email));
			//$randNumber=mt_rand();
			$email=trim($email);
//			if ($_POST['link']==1) {
//				$upd=mysql_query("UPDATE ".$table." SET code='$randNumber' WHERE email='$email'");
//				$id=mysql_fetch_array(mysql_query("SELECT id AS id FROM ".$table." WHERE email='$email'"));
//				$arrreplacements[$email]=array('{USERID}'=>$id['id'], '{RANDNUMBER}'=>$randNumber);
			sendHTMLemail($config['SITE_EMAIL'],$config['SITE_NAME'],$email,$_POST['subject'],$_POST['content'],$config['REPLY_EMAIL'],'');
		}
		
		//$swift->attachPlugin(new Swift_Plugin_Decorator($arrreplacements), "decorator");
		//$thename=($_POST['fromName']!=''?$_POST['fromName']:$config['EMAIL_NAME']);
		//$theemail=($_POST['fromEmail']!=''?$_POST['fromEmail']:$config['NEWS_EMAIL']);

//		if ($swift->batchSend($message, $recipients,new Swift_Address($theemail,$thename))) {
//			print_alerta(l('a fost trimis'));
//		} else {
//			print_alerta(l('nu a putut fi trimis'));
//		}

		?>

			<script type="text/javascript">
			$('#window_<?=$this->module?>_email').dialog( 'close' );
			</script>
		<?php
		$swift->disconnect();
	}
	function vezi_biduri($id) {
		global $db;
		unset($_GET['action']);

		$_GET['id_customer']=$id;
		include(dirname(__FILE__)."/auctions_bids.php");

		?>

			<script type="text/javascript"> add_all_url['auctions_bids']='&id_customer=<?=$id?>&back_bid=<?=$_GET['back_bid']?>';



			//grid_width['auctions_bids']=800;

			<?php

			$module->js($id); ?>


		</script>
		<?php
	}
	function update_multistatus($id) {
		global $db;
		if (!empty($id)) {
			$db->update($this->table,array('status'=>2),'`id`='.$id);?>
			<h3 style="text-align:center;"><?=l('A fost updatat')?></h3>
		<?}
	}
}
$module=new auctions_module();
if ($module_info) $this_module=$module;
elseif ($module_js) {
	?> var global_window=true;


	$(document).ready(function(){
			$(".module_menu .update_status").click(function(){
				var window_add_edit_name='<?=l('Vezi biduri')?>';
				var gr = nss_grids['<?=$module->module?>'].ids('last');
				if (gr=='') alerta('Selectati o licitatie'); else  nss_win("<?=$module->module?>_edit",window_add_edit_name,'module/<?=$module->module?>.php?action=vezi_biduri&id='+gr,600, function () {
				after_window_load('<?=$module->module?>','edit');
				},true);
			});

			$(".module_menu .vezi_clienti").click(function(){
				var window_add_edit_name='<?=l('Returneaza credite')?>';
				var gr = nss_grids['<?=$module->module?>'].ids('last');
				if (gr=='') alerta('Selectati o licitatie'); else  nss_win("<?=$module->module?>_edit",window_add_edit_name,'module/<?=$module->module?>.php?action=vezi_biduri&id=-'+gr,600, function () {
				after_window_load('<?=$module->module?>','edit');
				},true);
			});

			$(".module_menu .update_multistatus").click(function(){
				var window_add_edit_name='<?=l('anuleaza licitatii')?>';
				var gr = nss_grids['<?=$module->module?>'].ids('last');
				if (gr=='') alerta('Selectati o licitatie'); else	nss_win("<?=$module->module?>_edit",window_add_edit_name,'module/<?=$module->module?>.php?action=update_multistatus&id='+gr,600, function () {
				after_window_load('<?=$module->module?>','edit');
				},true);
			});
			$(".module_menu .email_new").click(function(){
				var gr = nss_grids['<?=$module->module?>'].ids();
				nss_win("<?=$module->module?>_email",window_add_edit_name,'module/<?=$module->module?>.php?action=email_new&to='+gr,800, function () {
				$('#window_<?=$module->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$module->module?>_email').dialog( 'close' ); });
				},true);
			});
		});

		<?php
		set_grid($module,array('multiselect'=>true,'sortname'=>'start_date','sortorder' => 'desc'));
}
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='send') $module->send();
elseif ($_GET['action']=='email_new') $module->mail(fget('id'));
elseif ($_GET['action']=='vezi_biduri') $module->vezi_biduri(fget('id'));
elseif ($_GET['action']=='update_multistatus') $module->update_multistatus(fget('id'));
else {
	print_header();

	print_content($module,array('update_status'=>l('Vezi biduri'),'vezi_clienti'=>l('Returneaza credite'),'update_multistatus'=>l('Anuleaza licitia'),'email_new'=>l('Trimite email')));
	//print_content($module,array('update_status'=>l('Vezi biduri'),'vezi_clienti'=>l('Returneaza credite'),'send_email'=>l('Trimite email'),'update_multistatus'=>l('Anuleaza licitia')));
	//$module->new_a();
	print_footer();
}
?>