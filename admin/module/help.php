<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class help_module {
	var $module='help';
	var $date='27-08-2009';
	var $table=TABLE_HELP;
	var $folder='categories_images';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function help_module() {

		$this->name=l('Ajutor');
		$this->title=l('Ajutor');
		$this->description=l('Ajutor');

		$this->type=array(
		'like'=>array('name','id'),
		'equal'=>array('status')
		);
		$this->folder='imagini-categorii';
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,min_1,max_200','style'=>'width:92%;'),
		'description'=>array('type'=>'editor','name'=>l('Descriere'),'style'=>'width:92%;'),
		'price'=>array('type'=>'input','name'=>l('Pret'),'style'=>'width:92%;'),
		//'brand_name'=>array('type'=>'input','name'=>l('Lenovo'),'valid'=>'empty,min_1,max_200','style'=>'width:92%;'),
		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'per_page'=>array('type'=>'input','name'=>l('Nr. produse/pagina'),'style'=>'width:92%;'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		//'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		1=>'show_categories',
		//2=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder)
		);
	}
	function css() {}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
			$categories=$db->fetch_all("select * from `".$this->table."` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii ajutor')?></label>
			</dt>
			<dd>
				<select name="id_parent" id="id_parent" style="width:200px">
				<option value="0"><?=l('Niciun parinte')?></option>
				<?=$this->json_categories($date['id_parent'])?>
				</select>
			</dd>
		</dl>
		<?
	}
	function js() {}
	function print_records() {
	?>
	<h2><?=l('Categorii ajutor')?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="nss_grid">
		<table cellspacing="0" cellpadding="0" border="0"  >
		<thead>
			<tr role="rowheader" class="ui-jqgrid-labels">
				<th><?=l('nume')?></th>
				<!--<th style="width: 60px;"><?=l('Anunturi')?></th>-->
				<th style="width: 60px;"><?=l('vizibil')?></th>
				<th style="width: 140px;"><?=l('Actions')?></th>
			</tr>
		</thead>
		</table>
	</div>
	<div id="nss_list_record">
	<?php  $this->reg_get_categs((int)r('cid')); ?>
	</div>
	<br />
	<?php
	}

	function reg_get_categs($id_parent=0,$content="",$nr=0) {
		global $db;
		if ($nr>4) die();
		if ($nr==0) {
			$id_id='id_parent';
		} else $id_id='id';

		$categories=$db->fetch_all("select * from `".$this->table."` where ".$id_id."=".$id_parent." ORDER BY `order` ASC");
		//print_a($categories);
		//echo "<br/> `".$id_id."`=".$id_parent." ORDER BY `order` ASC";
		if (empty($categories)) {
			?>
			<ul style="list-style-type:none;	margin:0;		padding:0;" class="connectedSortable">

			<?php
			?>
				<li id="ele-0">
						<div style="padding:2px; ">
						<a href="?mod=<?=$this->module?>&cid=0" onclick="add_all_url['<?=$this->module?>']='&cid=0'; reload_list_record('<?=$this->module?>'); return false; ">
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l('Categorii')?></span>
						</a>
					</div>
				</li>
			<?=$content?>
			</ul>
			<?php
		} else {


			ob_start();
			$exist_p=fo("SELECT `id` FROM `".$this->table."` WHERE `id`=".(int)$categories[0]['id_parent']);

			?>
			<ul  style="list-style-type:none;	margin:0;	 padding:0;
			 <?php if ($exist_p) { ?>			margin-left:20px; <?php } ?> "
			<?php if ($nr==0)  { ?> class="list_sort" <?php } ?> >
			<?php
			$i=0;
			foreach ($categories as $category) {
				$i++;
				//$nr_products=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_ADS."` WHERE `id_category`=".$category['id']);
				//$nr_products=product_count(','.$category['id_main'].categ_children($category['id_main']));
				if ($category ['status'] == 1) {
					$category ['status'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '" onclick="ajax_ch_status(\''.$this->module.'\',this);" />';
				} else {
					$category ['status'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '"  onclick="ajax_ch_status(\''.$this->module.'\',this);"  />';
				}
				$exist=fo("SELECT `id` FROM `".$this->table."` WHERE `id_parent`=".$category['id']);
			?>
					<li id="ele-<?=$category['id']?>">
						<div style="float:left;">
						<?php if ($nr==0 /*&& $i!=sizeof($categories)*/)  {
							?>
							<img src="images/icons/branch.gif" style="vertical-align:middle;" />
							<?php
						} else { ?>
						<img src="images/icons/branch2.gif" style="vertical-align:middle;" />
						<?php  } if (!empty($exist)) { ?>
						<a href="?mod=<?=$this->module?>&cid=<?=$category['id']?>" onclick="add_all_url['<?=$this->module?>']='&cid=<?=$category['id']?>'; reload_list_record('<?=$this->module?>'); return false; ">
						<?php } ?>
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=$category['name']?></span>
						<?php if (!empty($exist)) { ?>
						</a>
						<?php } ?>
						</div>
						<div style="width: 140px;float:right; padding:2px; padding-top:3px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="delete_record(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 60px;float:right; padding:1px 2px; text-align:center;"><?=$category['status']?></div>
						<!--<div style="width: 60px;float:right; padding:3px 2px; text-align:center;"><?=$nr_products?></div>-->
						<div class="clear"></div>
				</li>
				<?php
				$id_parent=$category['id_parent'];
			}
			echo $content;
			?>
			</ul>
		<?php
		$file_data = ob_get_contents();
		ob_end_clean();
		$this->reg_get_categs($id_parent,$file_data,$nr+1);
		}
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fost('id'));
		function delete_recursive($id,$table) {
			if(!empty($id)) {
				$childs=$db->fetch_all("SELECT * FROM `".$table."` WHERE id_parent=".$id);
				if (empty($childs)) {
					$db->delete($table," id=".$id);
					$db->delete($table.TABLE_EXTEND," id_main=".$id);
				} else {
					foreach ($childs as $child) {
						delete_recursive($child['id'],$table);
					}
				}
			} else return false;
		}
		foreach ($ids as $id) delete_recursive($id,$this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("select * from `".$this->table."` where `id`=".$id);
//		if ($date_saved['image']!=''&&file_exists(SITEROOT.'/static/'.$this->folder.'/'.$date_saved['image'])){
//			$date_saved['image']='static/'.$this->folder.'/'.$date_saved['image'];
//		} else $date_saved['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica categorie servicii'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;

		//$data['name_seo']=escapeIlegalChars($data['name_seo']," ");


		if (empty($id)) $nsid=next_auto_increment($config['SQL_DB'],$this->table);
		else $nsid=$id;

		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			//$data['image']=str_replace('static/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/'.$this->folder,''));
			close_window($this->module);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
			?>
			<script type="text/javascript">
			reload_list_record('<?=$this->module?>');
			</script>
			<?
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function save_order() {
		global_save_order($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
}
$module=new help_module();
/*
foreach ($db->fetch_all("SELECT * FROM `xp_categories`") as $categ) {
	$db->insert('xp_categories_description',array(
	'id_main'=>$categ['id'],
	'name'=>$categ['name'],
	'description'=>$categ['name'],
	'header_title'=>$categ['name'],
	'meta_description'=>$categ['name'],
	'meta_keywords'=>$categ['name'],
	'lang'=>'ro',
	'name_seo'=>escapeIlegalChars($categ['name']," "),
	));
}*/
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->reg_get_categs((int)r('cid'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>