<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class settings_module {
	var	$module='settings';
	var $date='07-11-2009';
	var $table=TABLE_SETTINGS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function settings_module() {
		
		$this->form=array(
		'SITE_NAME'=>array('type'=>'input','name'=>l('Nume site'),'style'=>'width:92%;','valid'=>'empty'),
		'SITE_WEB_ROOT'=>array('type'=>'input','name'=>l('Domeniu site'),'style'=>'width:92%;','valid'=>'empty'),
		'TEMPLATE'=>array('type'=>'input','name'=>l('TEMPLATE'),'style'=>'width:92%;','valid'=>'empty'),
		'DATE_FORMAT'=>array('type'=>'input','name'=>l('Format data'),'style'=>'width:92%;','valid'=>'empty'),
		'DATE_TIME_FORMAT'=>array('type'=>'input','name'=>l('Format timp'),'style'=>'width:92%;','valid'=>'empty'),
		1=>l('Email-uri'),
		'EMAIL_NAME'=>array('type'=>'input','name'=>l('Nume email'),'style'=>'width:60%;','valid'=>'empty'),
		'SITE_EMAIL'=>array('type'=>'input','name'=>l('Email site'),'style'=>'width:60%;','valid'=>'empty'),
		'REPLY_EMAIL'=>array('type'=>'input','name'=>l('Email reply'),'style'=>'width:60%;','valid'=>'empty'),
		'EMAIL_ORDER'=>array('type'=>'input','name'=>l('Email comanda'),'style'=>'width:60%;','valid'=>'empty'),
		'phone'=>array('type'=>'input','name'=>l('Telefon'),'style'=>'width:60%;'),
		'address'=>array('type'=>'text','name'=>l('Adresa'),'style'=>'width:60%;height:100px;'),
		'slide_ro'=>array('type'=>'text','name'=>l('Text banner ro'),'style'=>'width:60%;height:100px;'),
		'slide_en'=>array('type'=>'text','name'=>l('Text banner en'),'style'=>'width:60%;height:100px;'),
//		'flash'=>array('type'=>'input','name'=>l('Promotie Flash'),'style'=>'width:60%;','text'=>'0 - Nu ; 1 - Da'),
//		2=>l('Date facturi'),
//		'company_name'=>array('type'=>'input','name'=>l('name firma'),'style'=>'width:92%;','valid'=>'empty'),
//		'company_address'=>array('type'=>'input','name'=>l('address firma'),'style'=>'width:92%;','valid'=>'empty'),
//		'company_bank'=>array('type'=>'input','name'=>l('bank firma'),'style'=>'width:92%;','valid'=>'empty'),
//		'invoice_content'=>array('type'=>'text','name'=>l('content'),'style'=>'width:92%;','valid'=>'empty'),
//		'invoice_amount'=>array('type'=>'input','name'=>l('amount'),'style'=>'width:80px;','valid'=>'empty'),
		
		//'phone2'=>array('type'=>'input','name'=>l('Telefon 2'),'style'=>'width:60%;'),
		//2=>l('Emails subjects'),
		//'MAIL_SUBJ_CONTACT'=>array('type'=>'input','name'=>l('Subiect contact'),'style'=>'width:80%;','valid'=>'empty'),
		//'MAIL_SUBJ_REGISTER'=>array('type'=>'input','name'=>l('Subiect register'),'style'=>'width:80%;','valid'=>'empty'),
		//'MAIL_SUBJ_ORDER'=>array('type'=>'input','name'=>l('Subiect'),'style'=>'width:80%;','valid'=>'empty'),
		//'MAIL_SUBJ_CH_STATUS_ORDER'=>array('type'=>'input','name'=>l('MAIL_SUBJ_CH_STATUS_ORDER'),'style'=>'width:80%;','valid'=>'empty'),
		//'MAIL_SUBJ_BAN'=>array('type'=>'input','name'=>l('MAIL_SUBJ_BAN'),'style'=>'width:80%;','valid'=>'empty'),
//		3=>l('Limits products'),
//		'limit_af_site'=>array('type'=>'input','name'=>l('limit_af_site'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('products')),
//		'limit_af_home_page'=>array('type'=>'input','name'=>l('limit_af_home_page'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('products')),
//		'limit_af_home_categs'=>array('type'=>'input','name'=>l('limit_af_home_categs'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('products')),
//		'limit_af_product_page'=>array('type'=>'input','name'=>l('limit_af_product_page'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('products')),
//		'limit_af_noutati'=>array('type'=>'input','name'=>l('Pagina noutati'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('products')),
//		4=>l('Imagini'),
//		'product_image_width'=>array('type'=>'input','name'=>l('product_image_width'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('px')),
//		'product_image_height'=>array('type'=>'input','name'=>l('product_image_height'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('px')),
//		'product_image_big_width'=>array('type'=>'input','name'=>l('product_image_big_width'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('px')),
//		'product_image_big_height'=>array('type'=>'input','name'=>l('product_image_big_height'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('px')),
		//5=>l('Altele'),
		//'no_order'=>array('type'=>'input','name'=>l('no order'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('Numarul de start la comenzilor')),		
		);
	}
	function css() {}
	function js() {
		?>
		$(document).ready(function() {
			after_window_load('<?=$this->module?>');
		});
		<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function new_a() {
		global  $db,$main_buttons;
		$date_saved=$db->fetch("SELECT * FROM `".$this->table."` LIMIT 1");
		?>
		<form action="?mod=<?=$this->module?>&amp;action=save" method="POST">
		<?php 
		print_form_header(l('Editeaza setari'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save(){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,1);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			$db->qupdate($this->table,$data,'');
			print_alerta(l('a fost updatat'));
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new settings_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='save') $module->save();
else {
	print_header();
	?>
	<h2><?=$module->title?></h2>
	<?php
	$module->new_a();
	print_footer();
}
?>