<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class label_module {
	var $module='label';
	var $date='27-08-2009';
	var $table='xp_label';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $dimensiuni=array();
	function label_module() {
		global $config;
		$this->name=l('Etichete');
		$this->title=l('Etichete');
		$this->description=l('Etichete');

		$this->type=array(
		'like'=>array('link','name','username','email'),
		'date'=>array('date'),
		'equal'=>array('status','id_product')
		);

		$this->folder='imagini-etichete';

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		'image'=>array('name'=>l('Eticheta'),'width'=>300,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>80,'align'=>'left'),

		//'order'=>array('name'=>l('Pozitia'),'width'=>100,'align'=>'left'),
		
		'status'=>array('name'=>l('Status'),'width'=>60,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),"style"=>'width:98%;'),

		//'order'=>array('type'=>'input','name'=>l('Pozitia'),"style"=>'width:98%;'),
		//'email'=>array('type'=>'input','name'=>l('Email'),"style"=>'width:98%;'),
		//'website'=>array('type'=>'input','name'=>l('Website'),"style"=>'width:98%;'),
		//'link'=>array('type'=>'input','name'=>l('Link'),"style"=>'width:98%;'),
		//'message'=>array('type'=>'text','name'=>l('Comentariu'),"style"=>'width:98%;height:200px;'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		1=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Etichete - 640 x 480px'),'folder'=>$this->folder)
		);
	}
	function json_list() {
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		if (!empty($row['image']))
		$row['image']='<img src="'.$gd->url('resize','../static/i/imagini-etichete/'.$row['image'],'200x150').'" alt="'.$row['title'].'" />';
		return $row;
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		if ($date_saved['image']&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/'.$this->folder.'/i/'.$date_saved['image'];
		} else $date_saved['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		//unset($_POST,$data['file']);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/i/'.$this->folder,''));
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {

				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
			die();
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new label_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>