<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class rooms2_module{
	var $module='rooms2';
	var $date='25-08-2009';
	var $table='xp_room_types';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function rooms2_module() {
		$this->name=l('Tipuri de camere Omega');
		$this->title=l('Tipuri de camere Omega');
		$this->description=l('Tipuri de camere  Omega');

		$this->type=array(
		'like'=>array('_id','title','name','owner','order'),
		'date'=>array('date'),
		'equal'=>array('status','id_city','max_adults')
		);

		$this->folder='imagini-pagini';

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume'),'width'=>300),
		'_id'=>array('name'=>l('Cod'),'width'=>100,'align'=>'left'),
		'max_adults'=>array('name'=>l('Max adulti'),'width'=>60,'align'=>'center'),
		'order'=>array('name'=>l('Pozitia'),'width'=>50,'align'=>'center'),
		'owner'=>array('name'=>l('Adaugat de'),'width'=>100,'align'=>'center'),
		//'email'=>array('name'=>l('Email'),'width'=>120),
		//'telephone'=>array('name'=>l('Phone'),'width'=>120),
		//'website'=>array('name'=>l('Website'),'width'=>120),
		//'rating'=>array('name'=>l('Stele'),'width'=>50,'align'=>'center'),
		//'address'=>array('name'=>l('Adresa'),'width'=>150),
		//'id_city'=>array('name'=>l('Oras'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		//'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_promo'=>array('name'=>l('promovat index cu poza'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_header'=>array('name'=>l('In header'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_footer'=>array('name'=>l('In footer'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_info'=>array('name'=>l('informatie utila'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_navigator'=>array('name'=>l('apare in navigator'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'text'=>'','info'=>'','valid'=>'empty,min_2,max_100','style'=>'width:70%;','auto'=>'header_title,meta_description,meta_keywords,name_seo','lang'=>true),
		
		//'name_seo'=>array('type'=>'input','name'=>l('Link url'),'style'=>'width:70%;','lang'=>true),
		'max_adults'=>array('type'=>'input','name'=>l('Nr. maxim adulti'),'valid'=>'empty','style'=>'width:70%;'),
		'id'=>array('type'=>'input','name'=>l('Cod'),'valid'=>'empty','style'=>'width:70%;'),
		//'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:70%;'),
		//'class'=>array('type'=>'input','name'=>l('Clasa'),'style'=>'width:70%;'),
		//'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//'is_promo'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('promovat index cu poza')),
		//'is_footer'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('In footer')),
//		'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('informatie utila')),
		//'is_navigator'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in navigator')),
		//'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in index text')),
	//	'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true,'style'=>'width:70%;height:200px;'),
		//1=>'show_categories',
	//	'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		//2=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine buton'),'text'=>'170x30','folder'=>$this->folder),
		//'big_image'=>array('type'=>'image','name'=>l('Imagine mare'),'text'=>'770x110','folder'=>$this->folder)
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this,true);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

		//$row['id_city']=$db->fetch_one("select `name` from `xp_cities_data` WHERE `_id`={$row['id_city']} ");
		
		//$row['actions']="<a class=\"action\" href=\"".BASEHREF."admin/image.php?id_product=".$row['id']."\">Galerie foto</a><br/><br/>".$row['actions'];
		
		$row['order'] = '<input size="2" name="order['.$row['id'].']" value="'.$row['order'].'" style="text-align:center;width:30px;" onchange="set_product_order(this.value,\''.$row['id'].'\');reload_grid(\''.$this->module.'\');" />';
		
		if ($row['owner']=='Omega') {
		$row['actions']='<a class="action edit" onclick="do_edit(\''.$row['_id'].'\',\'rooms\');" >'.l('edit').'</a>';
		} else {
		$row['actions']='<a class="action edit" onclick="do_edit(\''.$row['_id'].'\',\'rooms\');" >'.l('edit').'</a>&nbsp;&nbsp;<a class="action delete" onclick="do_delete(\''.$row['_id'].'\',\'rooms\',\'\');" >'.l('Delete').'</a>';	
		}
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete4($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Oras')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px">
				<?php $this->json_categories($date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_brand=0) {
		global $db;

		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `xp_cities` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_cities_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_admin = lang_fetch3($this->table,"`id`='".$id."'");
		}
//		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//		} else $date_admin['image']='';
//
//		if ($date_admin['big_image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['big_image'])){
//			$date_admin['big_image']='static/i/'.$this->folder.'/'.$date_admin['big_image'];
//		} else $date_admin['big_image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica pagini site'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`='".$id."'");
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;

		foreach(
			$data['nss_languages'] as
			$lang => $d
		){
			$data['nss_languages'][$lang]['name_seo'] = url($data['nss_languages'][$lang]['name'], $lang);
			//$data['nss_languages'][$lang]['owner'] = 'Mondial';
		}

		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'],'-'),'static/i/'.$this->folder,''));
//			$data['big_image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['big_image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'].' big','-'),'static/i/'.$this->folder,''));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				lang_update2($this->table, $data," `id`='".$id."'");
				print_alerta('a fost updatat');
			} else {
				foreach(
					$data['nss_languages'] as
					$lang => $d
				){
					//$data['nss_languages'][$lang]['name_seo'] = url($data['nss_languages'][$lang]['name'], $lang);
					$data['nss_languages'][$lang]['owner'] = 'Mondial';
				}
				$data['status']=1;
				lang_insert2($this->table, $data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new rooms2_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>