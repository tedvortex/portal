<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class tags4_module {
	var	$module='tags4';
	var $date='27-08-2009';
	var $table='xp_summar_tags';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function tags4_module() {

		$this->name=l('Tag-uri zona 4');
		$this->title=l('Tag-uri zona 4');
		$this->description=l('Tag-uri zona 4');

		$this->type=array(
		'like'=>array('id_summar','link','order'),
		'like2'=>array('name'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'id_summar'=>array('name'=>l('Zona'),'width'=>100),
		'name'=>array('name'=>l('Name'),'width'=>100),
		'link'=>array('name'=>l('Link'),'width'=>200),
		'order'=>array('name'=>l('Pozitia'),'width'=>50,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>30,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		1=>'show_categories2',
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:92%;'),
		'link'=>array('type'=>'input','name'=>l('Link'),'style'=>'width:92%;'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:92%;'),
		'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'style'=>'width:92%;height:150px;'),

		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
	
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'furnizori_images'),
		);
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function set_product_order2(max_adults,id) {
$.ajax( {
		data :"ch_max_adults=1&id="+ id+"&max_adults="+max_adults,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function show_categories2($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_summar"><?=l('Zona')?></label>
			</dt>
			<dd>
				<select name="id_summar" id="id_summar" style="width:200px">
				<?php $this->json_categories2($date['id_summar']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories2($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		//$categories=$db->fetch_all("select * from `xp_pages_data` where `id_parent`=".$id_parent." ORDER BY `name` ASC");
		$categories=$db->fetch_all("SELECT * FROM `xp_summar` where `type`=3 and `id_parent`=".$id_parent." ORDER BY `name` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories2($id_category,$linie['id'],$deep+1);
		}
	}
	function json_list() {
		$new_sql="select sql_calc_found_rows
					p.*,pd.`name` as zona

				from
					`{$this->table}` p
					inner join `xp_summar` pd
								on p.`id_summar`=pd.`id`
					
					
					where pd.`type`=3";

		json_list($this,false,$new_sql);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		
		$row['order'] = '<input size="2" name="order['.$row['id'].']" value="'.$row['order'].'" style="text-align:center;width:30px;" onchange="set_product_order(this.value,\''.$row['id'].'\');reload_grid(\''.$this->module.'\');" />';
		$row['id_summar']=$db->fetch_one("select `name` from `xp_summar` where `id`=".$row['id_summar']);
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
		function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`='".$id."'");
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);

			
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new tags4_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	{
		$new_sql="select sql_calc_found_rows
					p.*,pd.`name` as zona

				from
					`{$module->table}` p
					inner join `xp_summar` pd
								on p.`id_summar`=pd.`id`
					
					
					where pd.`type`=3";

		json_list($module,false,$new_sql);
}
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
else {
	print_header();
	print_content($module);
	//$module->new_a();
	print_footer();
}
?>