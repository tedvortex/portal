<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class customers_module {
	var $module='customers';
	var $date='27-08-2009';
	var $table=TABLE_CUSTOMERS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function customers_module() {

		$this->name=l('Clienti');
		$this->title=l('Clienti');
		$this->description=l('customers_modul_description');

		$this->type=array(
		'like'=>array('phone','email','first_name','last_name','status','username'),
		'date'=>array('date','last_login'),
		'equal'=>array('type','status')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'username'=>array('name'=>l('User'),'width'=>100),
		'first_name'=>array('name'=>l('first name'),'width'=>100),
		'last_name'=>array('name'=>l('last name'),'width'=>100),
		//'credits'=>array('name'=>l('Puncte'),'width'=>50,'align'=>'center'),
		'phone'=>array('name'=>l('phone'),'width'=>120),
		'email'=>array('name'=>l('email'),'width'=>200),
		//'id_group'=>array('name'=>l('grup'),'width'=>70),
		'status'=>array('name'=>l('status'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:All;1:Activ;0:Inactiv')),
		'date'=>array('name'=>l('Date'),'width'=>80,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>150,'align'=>'center','sortable'=>false)
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Date personale'),
		2=>l('Modifica parola'),
		3=>l('Modifica email'),
		//4=>l('Adauga editeaza adrese'),
		//5=>l('Adauga editeaza firme')
		),
		0=>array('tab'=>1),
		//'username'=>array('type'=>'input','name'=>l('User'),'style'=>'width:70%;', 'valid'=>'empty,min_2,max_100'),
		'first_name'=>array('type'=>'input','name'=>l('first name'),'style'=>'width:70%;', 'valid'=>'empty,min_1,max_100'),
		'last_name'=>array('type'=>'input','name'=>l('last name'),'style'=>'width:70%;','valid'=>'empty,min_1,max_100'),
		//'cnp'=>array('type'=>'input','name'=>l('cnp')),
		//'seria_ci'=>array('type'=>'input','name'=>l('seria ci')),
		//'number_ci'=>array('type'=>'input','name'=>l('number ci')),
		//'base_zip_code'=>array('type'=>'input','name'=>l('zip code'),'valid'=>'empty'),
		//'birthdate'=>array('type'=>'date','name'=>l('birthdate'),'valid'=>'date'),
		'phone'=>array('type'=>'input','name'=>l('Telefon'),'valid'=>'empty,min_4,max_100'),
		//'base_city'=>array('type'=>'input','name'=>l('city'),'valid'=>'empty,min_4,max_100'),
		'city'=>array('type'=>'input','name'=>l('Oras'),'valid'=>'empty,min_2,max_100'),
		'state'=>array('type'=>'input','name'=>l('Judet'),'valid'=>'empty,min_2,max_100'),
		'address'=>array('type'=>'text','name'=>l('Adresa'),'style'=>'width:70%','valid'=>'empty,min_4,max_100'),
		//'ban_reason'=>array('type'=>'text','name'=>l('Motiv banare'),'style'=>'width:70%'),
		'note'=>array('type'=>'text','name'=>l('Observatii'),'style'=>'width:70%'),
		'status'=>array('type'=>'radio','options'=>array('1'=>l('activ'),'0'=>l('Inactiv'),'2'=>l('Banat')),'name'=>l('status'),'value'=>1),
		///1=>'list_groups',
		10=>array('tab'=>2),
		'password'=>array('type'=>'password','name'=>l('password'),'valid'=>'empty,min_6,max_14'),
		'password_2'=>array('type'=>'password','name'=>l('password ver'),'valid'=>'empty,min_6,max_14,same_password'),
		11=>array('tab'=>3),
		'email'=>array('type'=>'input','name'=>l('email'),'style'=>'width:70%','valid'=>'empty,unique,email'),
		//20=>array('tab'=>4),
		//21=>'list_addresses',
		//30=>array('tab'=>5),
		//31=>'list_companies',
		);
		$this->form_mail=array(
		//1=>'select_letter',
		'subject'=>array('type'=>'input','name'=>l('Subiect email'),'style'=>'width:98%;','valid'=>'empty,min_10,max_200'),
		'emails'=>array('type'=>'text','name'=>l('Email'),'style'=>'width:98%; height:140px;','valid'=>'empty,min_10,max_200'),
		//'logs'=>array('type'=>'checkbox','name'=>l('Salveaza loguri'),'value'=>1),
		//'letter'=>array('type'=>'checkbox','name'=>l('Salveaza scrisoare'),'value'=>1),
		'link'=>array('type'=>'checkbox','name'=>l('Link dezabonare'),'value'=>1,'text'=>l('pentru userii abonati')),
		'message'=>array('type'=>'editor','name'=>l('Mesaj'),'width'=>732),
		);
	}
	function css() {
		?>
		.module_menu li.email_new {
			background-image:url('../../images/icons/newsletter_emails_new.png');
		}
		.module_menu li.email_all {
			background-image:url('../../images/icons/newsletter_emails_all.png');
		}
		.module_menu li.update_multistatus { background-image:url('../../images/icons/update_multistatus.png'); }
		<?php
	}
	function json_list() {
		json_list($this);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function list_addresses($data){
		global $db;
		unset($_GET['action']);
		$module_html=true;
		include(dirname(__FILE__)."/customers_addresses.php");
		$module_html=false;
		?>
		<script type="text/javascript">
		add_all_url['customers_addresses']='&id_customer=<?=$data['id']?>';
		grid_width['customers_addresses']=848;
		<?php $module->js($data['id']); ?>
		</script>
		<?php
	}
	function list_companies($data){
		global $db;
		unset($_GET['action']);
		$module_html=true;
		include(dirname(__FILE__)."/customers_companies.php");
		$module_html=false;
		?>
		<script type="text/javascript">
		add_all_url['customers_addresses']='&id_customer=<?=$data['id']?>';
		grid_width['customers_companies']=848;
		<?php $module->js($data['id']); ?>
		</script>
		<?php
	}
	function json_list_row($row) {
		global $db;
		//$row['id_group']=(string)$db->fetch_one("SELECT `name` FROM `".TABLE_CUSTOMERS_GROUPS."` WHERE `id`=".$row['id_group']);
		//$row['company']=(string)$db->fetch_one("SELECT `name` FROM `".TABLE_CUSTOMERS_COMPANIES."` WHERE `id_customer`=".$row['id']);
		$row ['credits'] = '<input size="5" name="credite['.$row ['id'].']" value="'.$row ['credits'].'" style="text-align:center" onchange="set_customer_credite(this.value,'.$row ['id'].')" />';
		return $row;
	}
	function list_groups($date){
		global $db;
		?>
		
		<dl>
		<dt>
			<label for="customer_tra_select"><?=l('Tara')?></label>
		</dt>
		<dd>
		<select id="customer_tra_select" name="country">
			<?php
			$groups=$db->fetch_all("SELECT  * FROM `xp_countries` order by `name` asc " );
			foreach ($groups as $group) { 
			?>
			<option value="<?=$group['code']?>" <?=(empty($date['country'])?(($group['access']==1)?'selected':''):(($group['code']==$date['country'])?'selected':''))?> ><?=$group['name']?></option>
			<?php } ?>
			</select>
		</dd>
		</dl>
		
		<dl>
		<dt>
			<label for="customer_group_select"><?=l('Group')?></label>
		</dt>
		<dd>
		<select id="customer_group_select" name="id_group">
			<?php
			$groups=$db->fetch_all("SELECT  * FROM `".TABLE_CUSTOMERS_GROUPS."` WHERE status=1 order by `name` asc " );
			foreach ($groups as $group) { 
			?>
			<option value="<?=$group['name']?>" <?=(empty($date['id_group'])?(($group['access']==1)?'selected':''):(($group['id']==$date['id_group'])?'selected':''))?> ><?=$group['name']?></option>
			<?php } ?>
			</select>
		</dd>
		</dl>
	<?php
	}
	function js() {
		$after='$("#gbox_list_'.$this->module.'").css({"borderRight":"0px none"}).removeClass("ui-corner-all"); ';
		$loadComplete='';
		set_grid($this,array('multiselect'=>'true','sortname' => 'date','sortorder'=>'desc','loadComplete'=>$loadComplete),'',$after);
		//set_grid($this,array('multiselect'=>'true','sortname' => 'date','sortorder'=>'desc'));
		?>
		var global_window=true;
		var window_width=900;
		function autocomplete_letter(id) {
		if (id!=0) {
			$("#letter").attr("checked",false);
			$("#letter").parent().parent().hide();
				$.ajax( {
					data :"autocomplete_letter=1&id="+ id,
					type :"GET",
					url :'module/<?=$this->module?>.php',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) { eval(r);	}
				});
			} else {
				$("#letter").attr("checked",true);
				$("#letter").parent().parent().show()
			}
		}
		function set_customer_credite(credite,id) {
			$.ajax( {
			data :"ch_credite=1&id="+ id+"&credite="+credite,
			type :"GET",
			url :'module/<?=$this->module?>.php',
			timeout :45000,
			error : function() {
				console.log("Failed to submit - ");
			},
			success : function(r) {	}
			});
		}
		$(document).ready(function(){
			$(".module_menu .email_new").click(function(){
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=email_new&to='+gr,800, function () {

				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				},true);
			});
			$(".module_menu .update_multistatus").click(function(){
				var window_add_edit_name='<?=l('Baneaza clienti')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_multistatus&ids='+gr,600, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .email_all").click(function(){
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=email_new&to=all',800, function () {

				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				},true);
			});
		});
	<?php }
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
//			$date_saved['base_zip_code']=$date_saved['zip_code'];
//			$date_saved['base_phone']=$date_saved['phone'];
//			$date_saved['base_city']=$date_saved['city'];
//			$date_saved['base_state']=$date_saved['state'];
//			$date_saved['base_address']=$date_saved['address'];
			unset($date_saved['password']);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form($this->form,$this,$date_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		
		//$data['birthdate']=strtotime($data['birthdate']);

		unset(
		$data['id_customer'],
		$data['cui'],
		$data['nr_reg_com'],
		$data['company_city'],
		$data['company_state'],
		$data['company_address'],
		$data['company_zip_code'],
		$data['bank'],
		$data['iban_bank'],
		$data['company_phone'],
		$data['base_zip_code'],
		$data['base_phone'],
		$data['base_city'],
		$data['base_state'],
		$data['base_address'],
		$data['name'],
		$data['id']
		);
		
	//print_r($data);

		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['password_2']);
			if (empty($data['password'])) {
				unset($data['password']);
			} else {
				$data['password']=md7($data['password']);
			}
			if (!empty($id)) {
				$db->update($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function ch_credite($id,$credite) {
		global $db;
		$db->update($this->table,array('credits'=>$credite),"`id`=".(int)$id);
	}
	function autocomplete_letter($id){
		global $db;
		$letter=$db->fetch("SELECT * FROM `".TABLE_NEWSLETTER_LETTERS."` WHERE id=".(int)$id);
//		print_a($letter);
//		die();
		?>
		$("#subject").val('<?=str_replace("'","\'",$letter['subject'])?>');
		$("#message").val('<?=str_replace("\n","<br />",str_replace("'","\'",$letter['message']))?>');
		<?php
	}
	function select_letter($date) {
		global $db;
		$all_letters=$db->fetch_all("SELECT `id`,`subject` FROM `".TABLE_NEWSLETTER_LETTERS."`");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Scrisori predefinite')?></label>
			</dt>
			<dd>
			<select name="id_letter_template" onchange="autocomplete_letter(this.value);">
			<option value="0"> <?=l('Selectati o scrisoare')?> </option>
				<?php foreach ($all_letters as $letter) { ?>
				<option value="<?=$letter['id']?>"><?=ehtml($letter['subject'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<?php
	}
	function mail($id=0) {
		global  $db,$main_buttons;
		//if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".TABLE_NEWSLETTER_LETTERS."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=send&id=<?=$id?>" method="POST">
		<?php
		$ids=explode(",",$_GET['to']);
		$emails="";
		$emailsAddress=$db->fetch_all("SELECT * FROM `".$this->table."` WHERE status=1");
		foreach ($emailsAddress as $email) {
			if (preg_match('/^[^@]+@[a-zA-Z0-9-_.-]+?\.[a-zA-Z0-9-]{2,4}$/',$email['email'])) {
				if ($_GET['to']=='all') {
					$emails.=htmlentities("\r\n".$email['email']);
				} elseif (in_array($email['id'],$ids)) {
					$emails.=htmlentities("\r\n".$email['email']);
				}
			}
		}
		$this->form_mail['emails']['value']=$emails;
		print_form_header(l('New Email'));
		print_form($this->form_mail,$this,$date_saved);
		print_form_footer();
		unset($main_buttons['continue']);
		$main_buttons['save']=array('type'=>'submit','value'=>l('Trimite'));
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function send() {
		global $db,$config;
		$table=$this->table;
		if (isset($_POST['letter']) and $_POST['letter']==1) {
			$newNumber=$db->fetch_one("SELECT MAX(number) FROM `".TABLE_NEWSLETTER_LETTERS."`");
			$newNumber++;
			$values=array(
			'number'=>$newNumber,
			'subject'=>$_POST['subject'],
			'message'=>$_POST['message'],
			'date'=>time(),
			);
			$db->insert(TABLE_NEWSLETTER_LETTERS,$values);
			$idLetter=mysql_insert_id();
		}
		$swift =& new Swift(new Swift_Connection_Sendmail());
		$_POST['content']=preg_replace('/(href="|src="|action=")(?!#|[a-zA-Z]+:|")\/?([^"]+)(")/i','$1'.$config['SITE_WEB_ROOT'].'$2$3',$_POST['message']);
		//verificam daca punem link de dezabonare sau nu
		if ($_POST['link']==1) {
			$_POST['message'].='<div style="text-align:right;margin-top:10px;"><a target="_blank" href="'.$config['SITE_WEB_ROOT'].'newsletter.html?tip=del&id={USERID}&cod={RANDNUMBER}">Click aici pentru a va dezabona.</a></div>';
		}
		//end
		$message =& new Swift_Message($_POST['subject'], $_POST['message'], "text/html");
		$message->setReplyTo($config['REPLY_EMAIL']);
		/*	if ($_FILES['fisier']['tmp_name']!='') {
		$message->attach(new Swift_Message_Part($_POST['message'],"text/html"));
		$attachment =& new Swift_Message_Attachment(file_get_contents($_FILES['fisier']['tmp_name']),$_FILES['fisier']['name']);
		//		$file =& new Swift_File($_FILES['fisier']['tmp_name']);
		//		$attachment =& new Swift_Message_Attachment($file);
		$message->attach($attachment);
		}*/
		$recipients =& new Swift_RecipientList();
		$arrmails=explode("\n",$_POST['emails']);
		print_a($arrmails);
		$arrreplacements=array();
		foreach ($arrmails as $email) {
			$recipients->addTo(trim($email));
			$randNumber=mt_rand();
			$email=trim($email);
			if ($_POST['link']==1) {
				$upd=mysql_query("UPDATE ".$table." SET code='$randNumber' WHERE email='$email'");
				$id=mysql_fetch_array(mysql_query("SELECT id AS id FROM ".$table." WHERE email='$email'"));
				$arrreplacements[$email]=array('{USERID}'=>$id['id'], '{RANDNUMBER}'=>$randNumber);
			}
			//pastram logurile
			if ($_POST['logs']==1) {
				$verExistCustomer=$db->fetch("SELECT * FROM ".TABLE_CUSTOMERS." WHERE email='$email'");
				if ($verExistCustomer) {
					$valuesLogs=array(
					'id_letter'=>$idLetter,
					'id_customer'=>$verExistCustomer['id'],
					'subject'=>$_POST['subject'],
					'message'=>$_POST['message'],
					'date'=>time(),
					);
					$db->insert('xp_newsletter_logs',$valuesLogs);
				}
			}
			//end pastrare loguri
		}
		$swift->attachPlugin(new Swift_Plugin_Decorator($arrreplacements), "decorator");
		$thename=($_POST['fromName']!=''?$_POST['fromName']:$config['EMAIL_NAME']);
		$theemail=($_POST['fromEmail']!=''?$_POST['fromEmail']:$config['NEWS_EMAIL']);

		if ($swift->batchSend($message, $recipients,new Swift_Address($theemail,$thename))) {
			print_alerta(l('a fost trimis'));
		} else {
			print_alerta(l('nu a putut fi trimis'));
		}

		?>

			<script type="text/javascript">
			$('#window_<?=$this->module?>_email').dialog( 'close' );
			</script>
		<?php
		$swift->disconnect();
	}
	function print_records() {
		$new_icons=array(
		'email_new'=>l('Trimite la selectati'),
		'email_all'=>l('Trimite la toti'),
		'update_multistatus'=>l('Baneaza clienti')
		);
		print_content($this,$new_icons);
	}
	function update_multistatus($ids) {
		global $db;
		if (!empty($ids)) {
			$ids=explode(',',$ids);
			foreach ($ids as $id) {
//				$id_customer=$db->fetch_one("SELECT `id_customer` FROM `".$this->table."`  WHERE `id`=".$id);
			$username=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$id);
			$db->update(TABLE_CUSTOMERS,array('status'=>2),'`id`='.$id);?>
				<h3 style="text-align:center;">Utilizatorul <?=$username?> a fost banat</h3>
		<?} }
	}
}
$module=new customers_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['autocomplete_letter']==1) $module->autocomplete_letter(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['ch_credite']==1) $module->ch_credite(fget('id'),fget('credite'));
elseif ($_GET['action']=='send') $module->send();
elseif ($_GET['action']=='email_new') $module->mail(fget('id'));
elseif ($_GET['action']=='update_multistatus') $module->update_multistatus(fget('ids'));
else {
	print_header();
	//$module->print_records();
	print_content($module,array('email_new'=>l('Trimite la selectati'),'email_all'=>l('Trimite la toti'),'update_multistatus'=>l('Baneaza clienti')));
	print_footer();
}
?>