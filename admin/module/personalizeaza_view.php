<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class personalizeaza_view_module {
	var $module='personalizeaza_view';
	var $date='27-08-2009';
	var $table='prefix_products_views';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $folder='';
	var $title='';
	var $description='';
	function personalizeaza_view_module() {

		$this->name=l('Produse personalizare');
		$this->title=l('Produse personalizare');
		$this->description=l('Produse personalizare');
		$this->folder='files';

		$this->type=array(
		'like'=>array('name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume vedere')),
		'id_product'=>array('name'=>l('Produs'),'width'=>200,'stype'=>'none'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume vedere')),
		1=>'select_product'
		);



	}
	function css() {
		?>

		<?php
	}
	function select_product($date) {
		?>
		<dl>
			<dt>
				<label for="id_product"><?=l('Tip produs')?></label>
			</dt>
			<dd>
				<select name="id_product" id="id_product" style="width:200px">
				<?php $this->json_select_product($date['id_product']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_product($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this,false);
	}
	function js() {
		set_grid($this,array('multiselect'=>'true'));
		?>
		$(document).ready(function(){
			$(".module_menu .insert").click(function(){
			
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=insert',900, function () {
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				init_upload();
				},true);
			});
		});
		<?php
	}


	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function insert() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_import" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		?>
		<dl>
			<dt><label for="code">Fisier CSV:</label></dt>
		<dd>
		 	<input type="hidden" class="file" value="" name="code" id="code" rel="csv,<?=TMP_IMG?>,0" />
		</dd>
		</dl>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		
		</form>
		<?php
	}
	function json_list_row($row) {
		global $db;

		$row['id_product']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products` WHERE `id`=".$row['id_product']);
		return $row;
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_import(){
		global $db;
		if (!empty($_POST['code'])) {

			$handle = fopen(SITEROOT."/".$_POST['code'], "rb");
			$i=0;
			while (!feof($handle)) {
				$i++;
				$line = fgets($handle);
				$cols=explode(",",$line);
				if (empty($cols[1])) continue;
				if(!$verSerial=$db->fetch_one("select `id` from `".TABLE_PREPAID."` where `serial`='".trim($cols[0])."'")) {
					$db->insert($this->table,array(
					'code'=>trim($cols[1]),
					'serial'=>trim($cols[0]),
					'credits'=>(int)$cols[2],
					'date'=>time(),
					));
				}
			}
		?>
	<script type="text/javascript">
	$('#window_<?=$this->module?>_email').dialog( 'close' );
	</script>
<?php
print_alerta(l('Au fost importate'));
		} else print_alerta(l('Adaugati fisier'));

	}
	function save($id){
		global $db;
		$data=$_POST;

		//$data['name_seo']=escapeIlegalChars($data['name']," ");
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new personalizeaza_view_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='insert') $module->insert();
elseif ($_GET['action']=='save_import') $module->save_import();
else {
	print_header();
	//print_content($module, array(), '',  '',array('new'));
	print_content($module);
	print_footer();
}
?>