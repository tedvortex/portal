<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class variatii_produse_module {
	var	$module='variatii_produse';
	var $date='27-08-2009';
	var $table='ev_variation_entity';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function variatii_produse_module() {

		$this->name=l('Produse cu variatii');
		$this->title=l('Produse cu variatiie');
		$this->description=l('Produse cu variatii');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id_product')
		);
		$this->folder='producatori';
		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,

		'id_product'=>array('name'=>l('Produs'),'width'=>200),
		'order'=>array('name'=>l('Pozitia'),'width'=>80),

		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
			'tabs'=>array(
				1=>l('Detalii')
			),
			
			0=>array('tab'=>1),
				3=>'show_product',
				//'name'=>array('type'=>'input','name'=>l('nume'),'valid'=>'empty,unique,min_1,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
				//'name_seo'=>array('type'=>'input','name'=>l('Nume seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
				//4=>'show_categories',
				'order'=>array('type'=>'input','name'=>l('Pozitia')),
				'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),


		);
	}
	function json_list() {
		$new_sql="select *,`id_variation_entity` as `id`
						from `ev_variation_entity`";
		json_list($this,false,$new_sql);
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function show_product($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_product"><?=l('Produse')?></label>
			</dt>
			<dd>
				<select name="id_product" id="id_product" style="width:200px">
				<?php $this->json_product($date['id_product']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_product($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Selectati produsul</option><?php
		$resursa=$db->query("SELECT * FROM `xp_products_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id_product']?>" <?=($linie['id_product']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		$row['id_product']=$db->fetch_one("select `name` from `xp_products_data` WHERE `id_product`={$row['id_product']} ");
		$row['actions']='<a class="action edit" onclick="do_edit('.$row['id_variation_entity'].',\'variatii_produse\');" >'.l('edit').'</a>&nbsp;<a onclick="do_delete('.$row['id_variation_entity'].',\'variatii_produse\',\'\');" class="action delete">Sterge</a>';
		return $row;
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_variation"><?=l('Variatii')?></label>
			</dt>
			<dd>
				<select name="id_variation" id="id_variation" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_variation']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id_variation) FROM `ev_variation` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `ev_variation` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id_variation']?>" <?=($linie['id_variation']==$id_provider)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE `id_variation_entity`=".$id);

		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$data['name_seo']=escapeIlegalChars($data['name']," ");
		//$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			//$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/i/'.$this->folder,''));
			unset($data['type_op']);
			//if (!isset($data['image'])) $data['image']='';
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id_variation_entity=".$id);
				print_alerta('a fost updatat');
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new variatii_produse_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module);
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['json_product']==1) $module->json_product(fget('id'));
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>