<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class advantages_module {
	var $module='advantages';
	var $date='27-08-2009';
	var $table='xp_advantages';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $dimensiuni=array();
	function advantages_module() {
		global $config;
		$this->name=l('Banere');
		$this->title=l('Banere');
		$this->description=l('Banere');

		$this->type=array(
		'like'=>array('link','name'),
		'date'=>array('date'),
		);
		$this->folder='avantaje';
		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
//		'image'=>array('name'=>l('Banner'),'width'=>380,'align'=>'center'),
		'name'=>array('name'=>l('Titlu'),'width'=>100,'align'=>'center'),
		//'link'=>array('name'=>l('Link'),'width'=>300),
		'description'=>array('name'=>l('Descriere'),'width'=>100),
		'status'=>array('name'=>l('Status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Titlu'),'lang'=>true,"style"=>'width:98%;'),
		'link'=>array('type'=>'input','name'=>l('Link'),'lang'=>true,"style"=>'width:98%;'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),"style"=>'width:98%;'),
		'description'=>array('type'=>'text','name'=>l('Descriere'),'lang'=>true,"style"=>'width:98%;height:100px;'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),

		2=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder),
		//'image_index'=>array('type'=>'image','name'=>l('Imagine mica '),'folder'=>$this->folder)

		);
	}
	function css() {}
	function json_list() {
		json_list($this,true);
	}
	function json_list_row($row) {
		global $gd;
		$row['image']='<img src="'._static.'i/avantaje/'.$row['image'].'" alt="'.$row['image'].'" width="80%" />';
		//$row['image_inde']='<img src="'._static.'i/advantages/'.$row['image'].'" alt="'.$row['image'].'" width="80%" />';

		return $row;
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=lang_fetch($this->table,"`id`=".$id);
		}
		if ($date_saved['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/i/'.$this->folder.'/'.$date_saved['image'];
		} else $date_saved['image']='';

		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Add/Edit'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		//$data['lang']=LANG;
		//unset($data['image_1']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
//			if ($id!=0) {
//
//				if($img=$db->fetch_one("select `image` from ".$this->table." where id=".$id)) {
//					@unlink(SITEROOT.'/static/i/advantages/290/'.$img);
//				}
//
//			}
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages'][LANG]['name'],'-'),'static/i/'.$this->folder,''));
			//$data['image_index']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image_index'],escapeIlegalChars($data['nss_languages']['ro']['name'].' '.'index','-'),'static/i/'.$this->folder,'770x340'));
			unset($data['type_op']);
			if (!empty($id)) {
				lang_update($this->table, $data," `id`=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta(l('a fost inserat'));
			}
			die();
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new advantages_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1)  $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>