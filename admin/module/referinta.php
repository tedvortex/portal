<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class referinta_module {
	var $module='referinta';
	var $date='25-08-2009';
	var $table=TABLE_REFERINTA;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function referinta_module() {
		
		$this->name=l('Referinte');
		$this->title=l('Referinte');
		$this->description=l('Referinte');
		
		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);
		
		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'content'=>array('name'=>l('Referinta'),'width'=>300),
		'name'=>array('name'=>l('Nume'),'width'=>100),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
		
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:82%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'content'=>array('type'=>'text','name'=>l('Continut referinta'),'valid'=>'empty'),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this);
	}
	function json_list_row($row) {
		global $gd;
		if (!empty($row['image']))
		$row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Date referinta'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$languages=get_languages();
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table);
		//print_a($data);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new referinta_module();
if ($module_info) $this_module=$module;
elseif ($module_js) {
	?>
	var global_window=true;
	<?
	set_grid($module,array('multiselect'=>true,'sortorder'=>'desc'));
}
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module,false);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>