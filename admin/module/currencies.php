<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class currencies_module {
	var	$module='currencies';
	var $date='27-08-2009';
	var $table=TABLE_CURRENCIES;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function currencies_module() {

		$this->name=l('Curs Valutar');
		$this->title=l('Curs Valutar');
		$this->description=l('Curs Valutar');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Name'),'width'=>120),
		'code'=>array('name'=>l('code'),'width'=>80,'align'=>'center'),
		'country'=>array('name'=>l('country'),'width'=>160),
		'exchange_rate'=>array('name'=>l('exchange rate'),'width'=>100),
		//'location'=>array('name'=>l('location'),'width'=>80),
		//'decimal_token'=>array('name'=>l('decimal token'),'width'=>80),
		//'thousands_token'=>array('name'=>l('thousands token'),'width'=>80),
		'token'=>array('name'=>l('token'),'width'=>80,'align'=>'center'),
		//'decimal_places'=>array('name'=>l('decimal places'),'width'=>80),
		//'default'=>array('name'=>l('default'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume moneda'),'valid'=>'empty,unique,min_3,max_140'),
		'code'=>array('type'=>'input','name'=>l('Cod moneda'),'valid'=>'empty,unique,min_1,max_4'),
		'country'=>array('type'=>'input','name'=>l('Tara'),'valid'=>'empty,unique,min_2,max_140'),
		'exchange_rate'=>array('type'=>'input','name'=>l('Rata de schimb')),
		'token'=>array('type'=>'input','name'=>l('token')),
		'decimal_token'=>array('type'=>'input','name'=>l('decimal token')),
		'thousands_token'=>array('type'=>'input','name'=>l('thousands token')),
		'decimal_places'=>array('type'=>'input','name'=>l('decimal places')),
		'location'=>array('type'=>'radio','options'=>array(1=>l('Stanga'),0=>l('Dreapta')),'name'=>l('Locatie')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'default'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('default')),
		);
	}
	function json_list(){
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		if ($row['default']==1) {
			$row['default']='<img class="ch_default_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;" />';
		} else $row['default']='<img class="ch_default_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica curs'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);

				//calculate_promo();

				print_alerta('a fost updatat');
			}

			else {
				$db->insert($this->table,$data);

				calculate_promo();

				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new currencies_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module);
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>