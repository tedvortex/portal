<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class admins_module {
	var	$module='admins';
	var $date='27-08-2009';
	var $table=TABLE_ADMIN_USERS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function admins_module() {

		$this->name=l('admins');
		$this->title=l('admins');
		$this->description=l('admins');

		$this->type=array(
		'like'=>array('username','email','first_name','last_name','status','last_ip','module'),
		'date'=>array('date','last_login'),
		'equal'=>array('type','status')
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Add Edit Administrator'),
		2=>l('Module disponibile'),
		),
		1=>array('tab'=>1),
		'username'=>array('type'=>'input','name'=>l('username'),'text'=>'','info'=>'','valid'=>'empty,unique,'),
		'password'=>array('type'=>'password','name'=>l('password'),'valid'=>'empty,min_6,max_14'),
		'password_2'=>array('type'=>'password','name'=>l('password ver'),'valid'=>'empty,min_6,max_14,same_password'),
		'email'=>array('type'=>'input','name'=>l('email'),'valid'=>'empty,email'),
		'first_name'=>array('type'=>'input','name'=>l('first name'),'valid'=>'empty,min_4,max_100'),
		'last_name'=>array('type'=>'input','name'=>l('last name'),'valid'=>'empty,min_4,max_100'),
		'ip_access'=>array('type'=>'input','name'=>l('Acces numai de la IP')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		2=>array('tab'=>2),
		3=>'print_modules'
		);

		$this->grid=array(
		0=>array('order'=>'id asc','rows'=>100),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'username'=>array('name'=>l('Username'),'width'=>60),
		'email'=>array('name'=>l('email'),'width'=>120),
		'first_name'=>array('name'=>l('first_name'),'width'=>100),
		'last_name'=>array('name'=>l('last_name'),'width'=>100),
		'date'=>array('name'=>l('Date'),'width'=>130,'align'=>'center','stype'=>'date'),
		'last_login'=>array('name'=>l('last_login'),'width'=>130,'align'=>'center','stype'=>'date'),
		'last_ip'=>array('name'=>l('last_ip'),'width'=>100),
		//'module'=>array('name'=>l('module'),'width'=>100, 'sortable'=>false),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','options'=>array('_'=>l('Toate'),1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
	}
	function css() {}
	function json_list() {
		json_list($this);
	}
	function uninstall() {}
	function language() {}
	function js() {
		set_grid($this);
	}
	function grid_edit(){
		global_delete($this->table,false,'id_news');
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$form=$this->form;
		if (!empty($id)) {
			$date_admin=$db->fetch("SELECT * FROM `".TABLE_ADMIN_USERS."` WHERE id=".$id);
			$date_admin['module']=explode(",",$date_admin['module']);
			$date_admin['password']='';
		} else {
			$date_admin['module']=array();
		}
		if(empty($date['module'])) $date['module']=array();
		?>
		<form action="?mod=<?=$this->module?>&amp;action=save&amp;id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_admin);
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function print_modules($date_admin){
		global $db;
		$resursa=$db->query("SELECT * FROM `".TABLE_ADMIN_MODULE."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
			?>
			<dl>
			<dt>
			<label for="admin_module_<?=$linie['id']?>"><?=$linie['name']?> (<?=$linie['module']?>)</label>
			</dt>
			<dd>
			<input type="checkbox" id="admin_module_<?=$linie['id']?>"  name="module[]"  value="<?=$linie['module']?>" <?php if(in_array($linie['module'],$date_admin['module'])) echo "checked"; ?> /> &#160; -  &#160; <?=$linie['title']?> 
			</dd>
			</dl>
			<?php
		}
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			unset($data['password_2']);
			if (empty($data['password'])) {
				unset($data['password']);
			} else {
				$data['password']=md7($data['password']);
			}
			if (is_array($data['module'])) {
				$data['module']=implode(",",$data['module']);
			} else  $data['module']='';
			if (!empty($id)) {
				$db->qupdate(TABLE_ADMIN_USERS,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				$data['last_login']=time();
				$db->insert(TABLE_ADMIN_USERS,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new admins_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	?>
	
	<?php
	/*
	<div class="nss_grid">
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
	<th style="width:20px;">1</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	</tr>
	<tr>
	<th>1</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	<th>aaa</th>
	</tr>
	</thead>

	<tbody>
	<tr>
	<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>
	</tr>

	<tr>
	<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>
	</tr>

	<tr>
	<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>				<td>aa</td>
	</tr>


	</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" >
	<thead>
	<tr>
	<th class="th-foot-left">1</th>
	<th class="th-foot-center">1</th>
	<th class="th-foot-right">1</th>
	</tr>
	</thead>
	</table>
	</div>
	*/
	//include_once("includes/editor/editor.php");
	print_content($module);
	print_footer();
}
?>