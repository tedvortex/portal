<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class slide_module {
	var $module='slide';
	var $date='27-08-2009';
	var $table='xp_slide';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $dimensiuni=array();
	function slide_module() {
		global $config;
		$this->name=l('Slide prima pagina');
		$this->title=l('Slide prima pagina');
		$this->description=l('Slide prima pagina');

		$this->type=array(
		'like'=>array('link','name'),
		'date'=>array('date'),
		);
		$this->folder='slide';
		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'image'=>array('name'=>l('Banner'),'width'=>340,'align'=>'center'),
		'name'=>array('name'=>l('Titlu banner'),'width'=>100,'align'=>'center'),
		//'link'=>array('name'=>l('Link'),'width'=>300),
		'description'=>array('name'=>l('Descriere banner'),'width'=>100),
		'order'=>array('name'=>l('Pozitia'),'width'=>50,'align'=>'center'),
		'status'=>array('name'=>l('Status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Titlu banner'),'lang'=>true,"style"=>'width:98%;'),
		'link'=>array('type'=>'input','name'=>l('Link'),'lang'=>true,"style"=>'width:98%;'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),"style"=>'width:98%;'),
		'description'=>array('type'=>'text','name'=>l('Descriere banner'),'lang'=>true,"style"=>'width:98%;height:100px;'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),

		2=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Imagine 830x290'),'folder'=>$this->folder),
		//'image_small'=>array('type'=>'image','name'=>l('Imagine mica 290x80'),'folder'=>$this->folder)

		);
	}
	function css() {}
	function json_list() {
		$new_sql="select sql_calc_found_rows
					p.*,
					pd.`name`,
					pd.`link`,
					pd.`description`

				from
					`{$this->table}` p
					inner join `xp_slide_data` pd
								on p.`id`=pd.`_id`
				 where `lang`='".LANG."'";
		json_list($this,false,$new_sql);
	}
	function json_list_row($row) {
		global $gd;
		$row['order'] = '<input size="2" name="order['.$row['id'].']" value="'.$row['order'].'" style="text-align:center;width:30px;" onchange="set_product_order(this.value,\''.$row['id'].'\');reload_grid(\''.$this->module.'\');" />';
		$row['image']='<img src="'._static.'i/slide/'.$row['image'].'" alt="'.$row['image'].'" width="80%" />';
		//$row['image_inde']='<img src="'._static.'i/slide/'.$row['image'].'" alt="'.$row['image'].'" width="80%" />';

		return $row;
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function set_product_order2(max_adults,id) {
$.ajax( {
		data :"ch_max_adults=1&id="+ id+"&max_adults="+max_adults,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=lang_fetch($this->table,"`id`=".$id);
		}
		if ($date_saved['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/i/'.$this->folder.'/'.$date_saved['image'];
		} else $date_saved['image']='';
		if ($date_saved['image_small']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image_small'])){
			$date_saved['image_small']='static/i/'.$this->folder.'/'.$date_saved['image_small'];
		} else $date_saved['image_small']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Add/Edit Banner'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		unset($data['type']);
		//$data['lang']=LANG;
		//unset($data['image_1']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			if ($id!=0) {

				if($img=$db->fetch_one("select `image` from ".$this->table." where id=".$id)) {
					@unlink(SITEROOT.'/static/i/slide/290/'.$img);
				}

			}
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name'],'-'),'static/i/'.$this->folder,''));
			//$data['image_small']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image_small'],escapeIlegalChars($data['nss_languages']['ro']['name'].' '.'small','-'),'static/i/'.$this->folder,''));
			unset($data['type_op']);
			if (!empty($id)) {
				lang_update($this->table, $data," `id`=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				$data['type']=0;
				lang_insert($this->table, $data);
				print_alerta(l('a fost inserat'));
			}
			die();
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`='".$id."'");
	}
}
$module=new slide_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1)  $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>