<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
define(TABLE_PRODUCTS,'xp_products');
class promo_module {
	var $module='promo';
	var $date='27-08-2009';
	var $table='xp_products';
	var $folder='products_images';
	var $option_info;
	var $value_all_info;
	var $all_variations=array();
	var $all_variations_name=array();
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function promo_module() {
		
		global $db;
		if(
		$settings=
		$db->fetch_all("
			select  *
			from `config`
			where `status`=1
		")
		){
			foreach(
				$settings as
				$s
			){
				if (
					$s['type'] == 0
				) {
					$config2->$s['key'] = $s['value'] ;
				}
		
				elseif (
					$s['type'] == 1
				) {
					define(
						$s['key'],
						$s['value']
					);
		
					$$s['key'] = $s['value'];
				}
		
				elseif (
					$s['type'] == 2
				) {
					$t[ $s['key'] ] = $config2->prefix . $s['value'] ;
				}
			}
		}
				

		$this->name=l('Pachete promo');
		$this->title=l('Pachete promo');
		$this->description=l('Pachete promo');

		$this->type=array(
		'like'=>array('name','code','description','price','views','cadou_nume'),
		'date'=>array('date','date_start','date_end'),
		'equal'=>array('status','id_category','id_brand','id_currency','id_furnizor','id_main','id','is_default')
		);

		$this->folder='imagini-produse';
		$this->folder3='i/imagini-produse';
		$this->folder4='i/imagini-promotii';
		$this->folder2='static/fisiere-produse';

		$this->orders_status=array(
		1=>l('Activ'),
		0=>l('Inactiv'),
		);
		$this->promotion_type=array(
		//0=>l('Categorii'),
		2=>l('Produse')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image 1'),'width'=>60,'align'=>'center'),
		'name'=>array('name'=>l('Nume produs'),'width'=>100),
		//'description'=>array('name'=>l('Descriere'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie pachet'),'width'=>100,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		//'id_brand'=>array('name'=>l('Brand'),'width'=>80,'align'=>'center','stype'=>'select','options'=>'show_select_brands'),
		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>80,'align'=>'center','stype'=>'select','options'=>'show_select_furnizori'),
		'code'=>array('name'=>l('Cod'),'width'=>80),
		'price'=>array('name'=>l('Pret'),'width'=>50,'align'=>'right'),
		'id_currency'=>array('name'=>l('Moneda'),'width'=>50,'align'=>'left','stype'=>'select','options'=>'show_select_currencies'),
		'views'=>array('name'=>l('Views'),'width'=>40,'align'=>'center'),
		'stock'=>array('name'=>l('Stoc'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'is_multiple'=>array('name'=>l('La mai multe'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'is_new'=>array('name'=>l('Nou'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'is_best_seller'=>array('name'=>l('Top vandut'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'date'=>array('name'=>l('Date'),'width'=>120,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Detalii pachet'),
		2=>l('Info Produs'),
		8=>l('Avantaje'),
		6=>l('Produse pachet'),
		//3=>l('Produse asemanatoare')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:92%;','auto'=>'name_seo'),
		'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'style'=>'width:92%;height:150px;'),
		'price_description'=>array('type'=>'editor','name'=>l('Pret tabelar')),
		'description'=>array('type'=>'editor','name'=>l('Descriere')),
		1=>l('Alte detalii'),
		//2=>'show_furnizori',
		3=>'show_brands',
		4=>'show_categories',
		5=>'show_currencies',
		'price'=>array('type'=>'input','name'=>l('Pret'),'info'=>l('Pret')),
		'price_type'=>array('type'=>'radio','name'=>l('Tip pret'),'options'=>array(0=>l('Pret fix'),1=>l('Pret de la'))),
		'code'=>array('type'=>'input','name'=>l('Cod'),'info'=>l('Code')),
		
		
		'stock'=>array('type'=>'input','name'=>l('Nivelul de stoc'),'info'=>l('Nivelul stocului')),
		'stock_type'=>array('type'=>'radio','name'=>l('Tip stoc'),'options'=>array(0=>l('In functie de numar produse'),1=>l('Nelimitat'))),
		//'weight'=>array('type'=>'input','name'=>l('Greutate'),'info'=>l('Greutate produs')),
		//'tax_price'=>array('type'=>'input','name'=>l('Taxa livrare'),'info'=>l('Taxa livrare')),
		//'af_home'=>array('type'=>'radio','name'=>l('Vizibil pe homepage'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'has_cart'=>array('type'=>'radio','name'=>l('Buton "Cumpara"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'has_personalize'=>array('type'=>'radio','name'=>l('Buton "Personalizeaza"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'has_offer'=>array('type'=>'radio','name'=>l('Buton "Cere oferta"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		15=>l('Imagini'),
	//	'water'=>array('type'=>'radio','name'=>l('Aplica watermark'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'images'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder,'multiple'=>true,'images'=>'return_images'),
		17=>array('tab'=>2),
		'has_warranty'=>array('type'=>'radio','name'=>$config2->warranty,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'waranty_description'=>array('type'=>'text','name'=>'Info '.$config2->warranty,'style'=>'width:92%;height:100px;'),
		
		
		//'has_delivery'=>array('type'=>'radio','name'=>$config2->delivery,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'delivery_description'=>array('type'=>'text','name'=>'Info '.$config2->delivery,'style'=>'width:92%;height:100px;'),
		
		'has_transport'=>array('type'=>'radio','name'=>$config2->transport,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'transport_description'=>array('type'=>'text','name'=>'Info '.$config2->transport,'style'=>'width:92%;height:100px;'),
		
		'has_execution'=>array('type'=>'radio','name'=>$config2->execution,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'execution_description'=>array('type'=>'text','name'=>'Info '.$config2->execution,'style'=>'width:92%;height:100px;'),
		
		'has_points'=>array('type'=>'radio','name'=>$config2->points,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'points_description'=>array('type'=>'text','name'=>'Info '.$config2->points,'style'=>'width:92%;100px'),
		
		'has_economy'=>array('type'=>'radio','name'=>$config2->economy,'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'economy_description'=>array('type'=>'text','name'=>'Info '.$config2->economy,'style'=>'width:92%;height:100px;'),
		
		'has_now'=>array('type'=>'radio','name'=>l('Pe loc'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'now_description'=>array('type'=>'text','name'=>l('Info pe loc'),'style'=>'width:92%;height:100px;'),
		
		//18=>array('tab'=>3),
		//'related_products_checkbox'=>array('type'=>'checkbox','name'=>l('Produse asemanatoare'),'text'=>l('Gaseste si arata automat produse asemanatoare'),'info'=>l('Bifati aceasta casuta si o lista cu produse asemanatoare va fi aleasa si afisata in magazinul dvs. Debifati casuta pentru a nu arata produsele asemanatoare sau pentru a alege dumneavoastra produsele asemanatoare, folosind instructiunile:1. Alegeti o categorie si produsele din acea categorie vor fi afisate; 2. Faceti dublu-click pe un produs pentru a-l adauga ca produs asemanator; 3. Pentru a sterge un produs din lista de produse asemanatoare, faceti dublu-click pe el in josul boxului')),
		//18=>'list_related_products',
//		51=>array('tab'=>6),
//		'start_date'=>array('type'=>'date','name'=>l('Data start'),'style'=>'width:40%','text'=>'ex. 28-04-2011'),
//		'end_date'=>array('type'=>'date','name'=>l('Data final'),'style'=>'width:40%','text'=>'ex. 25-05-2011'),
//		53=>'list_product_banner',
		56=>array('tab'=>8),
		'advantage_1'=>array('type'=>'input','name'=>l('Avantaj 1'),'style'=>'width:92%;'),
		'advantage_2'=>array('type'=>'input','name'=>l('Avantaj 2'),'style'=>'width:92%;'),
		'advantage_3'=>array('type'=>'input','name'=>l('Avantaj 3'),'style'=>'width:92%;'),
		'advantage_4'=>array('type'=>'input','name'=>l('Avantaj 4'),'style'=>'width:92%;'),
		51=>array('tab'=>6),
		53=>'list_product_banner',
		);

	}
function list_product_banner($date) {
		global $db;
		if (isset($date['ids'])) $date['ids']=explode(",",$date['ids']);
		?>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="id_category"><?=l('Categoria')?></label>
			</dt>
				<select name="id_category2" id="id_category2" style="width:460px;" onchange="do_product_filter2();">
					<option value="0"><?=l('Toate')?></option>
					<?php $this->json_categories2() ?>
				</select>
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Numele produsului sau cod')?></label>
			</dt>
			<dd><input type="text" name="product_name" id="product_name" style="width:460px;" onkeyup="do_product_filter2();" />
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Products')?></label>
			</dt>
			<dd id="list_products_dd2">
				<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
					<?php $this->json_products2(); ?>
				</select>

			</dd>
		</dl>
		<div class="list_products_selected all_options_type options_type_2" style="margin-left:280px;">
				<div style="font-size:14px;padding:10px 0;">Produse pachet promo</div>
				<?php
					if (isset($date['ids'])) {
					foreach ($date['ids'] as $id_now) {

						if (!empty($id_now)) {
							$sql="select
										p.*,
										pd.*
									from
								`".TABLE_PRODUCTS."` p

								inner join `".TABLE_PRODUCTS."_data` pd
									on p.`id`=pd.`id_product`

								WHERE p.`id`=".$id_now;
							$product=$db->fetch($sql);
							$product['is_main']=$db->fetch_one("select `is_main` from `xp_products_to_pack` where `id_product`=".$id_now);
							
							?>
							
							<div style="padding:5px 0;">
								<input type="hidden" name="products[]" value="<?=$product['id']?>" />
								<img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> 
								<b><?=$product['name']?> <strong>cod: <?=$product['code']?></strong></b>
								<span style="margin-left:20px;">Produs de baza - <input type="checkbox" name="main[<?=$id_now?>]" value="1" <?=$product['is_main']==1?'checked="checked"':''?>/></span>
							</div><?php
						}
					}
				}
				?>
				</div>
		<script type="text/javascript">
		$(document).ready(function() {
			//$('#categories').toChecklist();

		});
		</script>
		<?php
	}
	function return_images($data) {
		return fa("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$data['id']." ORDER BY `id` ASC");
	}
	function show_select_currencies() {
		$_GET['from_grid']=1;
		$this->json_currencies();
	}
	function show_select_furnizori() {
		$_GET['from_grid']=1;
		$this->json_providers();
	}
	function show_select_brands() {
		$_GET['from_grid']=1;
		$this->json_brands();
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function json_list() {
		$new_sql="select sql_calc_found_rows
					p.*,
					pd.*

				from
					`{$this->table}` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`
					where p.`type`=1";
		json_list($this,false,$new_sql);
	}

	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		#uploaded_images img{ cursor:move; }
		.module_menu li.update_all { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.update_all_filters { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.port_categories { background-image:url('../../images/icons/products_port.png'); }
		.module_menu li.verify_codes { background-image:url('../../images/icons/products_verify_codes.png'); }
		.module_menu li.export_codes { background-image:url('../../images/icons/products_export_codes.png'); }
		.module_menu li.update_provider { background-image:url('../../images/icons/update_provider.png'); }
		.module_menu li.update_multistatus { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.product_clon { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.update_cadou { background-image:url('../../images/icons/update_cadou.png'); }
		<?php
	}

	function list_specifications($date) {
		global $db;
		?>
		<div id="list_specifications">
		<?php  $this->json_list_specifications($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function show_furnizori($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_furnizor"><?=l('Furnizor')?></label>
			</dt>
			<dd>
				<select name="id_furnizor" id="id_furnizor" style="width:200px">
				<option value="0">Fara furnizor</option>
						<?php

						$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
						while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$date['id_furnizor'])?'selected':''?> > <?=$linie['name']?> </option>
			<?php
						}
		 ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function json_list_specifications($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
			print_form_header ($filter['name']);
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $specification_info=$db->fetch("SELECT `value`,`front` FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".$id." AND `id_option`=".$option['id']);
		?>
		<dl>
		<dt>
			<label for="products_specification_<?=$option['id']?>"><?=$option['name']?></label>
		</dt>
		<dd>
			<input type="checkbox" name="specifications_show[<?=$option['id']?>]" value="1" align="absmiddle"  <?=(($specification_info['front']==1)?'checked':'')?> />
			<input type="text"  style="width:60%;" name="specifications[<?=$option['id']?>]" value="<?=$specification_info['value']?>" align="absmiddle" />

		</dd>
		</dl>
		<?php
			}
			print_form_footer();
		}
	}
	function json_list_filters($id_category=0,$ids=0) {?>
		<div id="list_filters">
		<?php $this->json_list_filter2($id_category,$ids); ?>
		</div>
		<?php
	}
	function list_filters($date) {
		?>
		<div id="list_filters">
		<?php $this->json_list_filter($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function json_list_filter($id_category=0,$id=0) {
		global $db;
		//echo a;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." order by `order` asc " );
		//echo "SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." order by `order` asc ";
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label>
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
				//				print_a("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function json_list_filter2($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label><br />
			<input type="checkbox" name="ignore[]" value="<?=$filter['id']?>" /> Ignora
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function list_variations_tab($date) {
		global $db;
		$all_variations=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_NAME."` WHERE status=1 ORDER BY `name` ASC");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Variation')?></label>
			</dt>
			<dd>
			<select name="id_variation" onchange="list_variations_options(this.value,<?=(int)$date['id']?>);">
			<option value="0"> <?=l('Choose a variation')?> </option>
				<?php foreach ($all_variations as $variation) { ?>
				<option value="<?=$variation['id']?>" <?=($date['id_variation']==$variation['id']?'selected':'')?>><?=ehtml($variation['name'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<br clear="all" />
		<div id="list_variations_options" class="product_have_variations">
		<?php
		if (!empty($date['id_variation'])) {
			$this->list_variations_options($date['id_variation'],$date['id']);
		}
		?>
		</div>
		<?php
	}
	function get_rec($j=0) {
		$variation='';
		$variation_name='';
		$k=0;
		$max=sizeof($this->option_info);
		foreach ($this->option_info as $option) {
			$k++;
			foreach ($this->value_all_info[$option['id']] as $value) {
				$variation_temp=$variation.$value['id'];
				if ($k<$max) $variation_temp.="-";
				if ((!in_array($variation_temp,$this->all_variations))) {
					$variation.=$value['id'];
					$variation_name.=$value['name'];
					if ($k<$max) {
						$variation.="-";
						$variation_name.=" - ";
					}
					$array_variation=explode("-",$variation);
					$end_array_variation=end($array_variation);
					if ((sizeof($array_variation)==$max ) && (!empty($end_array_variation))) {
						$this->all_variations_name[$j]=$variation_name;
					}
					$this->all_variations[$j]=$variation;
					break;
				}
			}
		}
		if (!empty($this->all_variations[$j])) $this->get_rec($j+1);
	}
	function list_variations_options($id_variation,$id_product) {
		global $db;
		if (!empty($id_variation)) {
			$this->option_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`='".$id_variation."' ORDER BY `id`");

			$this->value_all_info=array();
			foreach ($this->option_info as $option) {
				$value_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`='".$option['id']."'");
				$this->value_all_info[$option['id']]=$value_info;
			}
			$this->get_rec();
			$product_info_variations=array();
			if (!empty($id_product)) {
				$resursa=$db->sql("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product));
				while ($product_variations=$db->fetch($resursa)) {
					$product_info_variations[str_replace("-","_",$product_variations['variation'])]=$product_variations;
				}
			}
?>
<input type="hidden" name="variations[nss_all_variations]" id="nss_all_variations" />
<div style="width:856px;">
		<table cellpadding="0" cellspacing="0" border="0"  id="table_variations" >
			<thead>
			<tr>
				<th style="width:20px"><input type="checkbox" name="aaaaaa" /></th>
				<th><?=Variation_name?></th>
				<th style="width:72px"><?=Cod?></th>
				<th style="width:72px"><?=Stoc?></th>
				<th style="width:140px"><?=Pret?></th>
				<th style="width:140px"><?=Greutate?></th>
				<th style="width:200px;"><?=imagine?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($this->all_variations_name as $key=>$variation_name) {
				$var=str_replace("-","_",$this->all_variations[$key]);
			?>
			<tr id="tr_<?=$var?>">
				<td align="center" >
					<input type="checkbox" name="variations[<?=$var?>][variation]" value="<?=$var?>" <?=(isset($product_info_variations[$var])?'checked':'')?> />
				</td>
				<td >
					<?=$variation_name?>
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][code]" size="8" value="<?=$product_info_variations[$var]['code']?>" />
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][stock]" size="8" value="<?=$product_info_variations[$var]['stock']?>" />
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][price_type]">
					<?php $price_type=$product_info_variations[$var]['price_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($price_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($price_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($price_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($price_type==0)?'style="display: none;"':''?>>
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][price]" value="<?=$product_info_variations[$var]['price']?>" />
					</span>
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][weight_type]">
					<?php $weight_type=$product_info_variations[$var]['weight_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($weight_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($weight_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($weight_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($weight_type==0)?'style="display: none;"':''?> >
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][weight]"  value="<?=$product_info_variations[$var]['weight']?>" />
					</span>
				</td>
				<td>
					<?php if (!empty($product_info_variations[$var]['image'])) { ?>
						<div id="image_delete_<?=$var?>">
						<span style="display:none;"><img src="<?=$gd->url('resize','../'.$product_info_variations[$var]['image'],'260x260s-#f6f1f6')?>" alt="<?=$variation_name?>" /></span>
								<a href="#" class="toolTipInfoImg" title="<?=$variation_name?>"><?=$variation_name?></a>
								&nbsp; &raquo; &nbsp;
								<a class="Action" href="javascript: delete_variation_image('image_delete_<?=$var?>',<?=$product_info_variations[$var]['id']?>);"><?=Delete?></a>
						</div>
					<?php } else { ?>
						<input type="file" name="variations[<?=$var?>][image]" />
					<?php } ?>

				</td>
			</tr>

			<?php
			}
			?>
		</tbody>
		</table>
		</div>

		<!--	<tr id="tr_next_<?=$var?>" class="bodyTr" onmouseover="this.className='bodyTrHover'; $('#tr_<?=$var?>').attr('class','bodyTrHover');" onmouseout="this.className='bodyTr';$('#tr_<?=$var?>').attr('class','bodyTr');">
			<td colspan="7" style="padding-bottom:4px; border-bottom:1px solid #EFE2F0;"><input type="text" style="width:99%" name="short_description[]" value="<?=$product_info_variations[$var]['short_description']?>"/></td>
			</tr>-->
<?php }
	}


	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_brand"><?=l('Brand')?></label>
			</dt>
			<dd>
				<select name="id_brand" id="id_brand" style="width:200px">
				<?php $this->json_brands($date['id_brand']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga producator')?>" onclick="add_brand();" />
			</dd>
		</dl>
		<?php
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function json_categories2($ids=array(),$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories2($id_category,$linie['id'],$deep+1);
		}
	}
	function json_providers($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_FURNIZORI."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_provider)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_brands($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_BRANDS."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder' => 'desc','loadComplete'=>$loadComplete),'',$after);
?>
var global_window=true;
var window_width=900;
var window_add_edit_name_brand="<?=l('Add Brand')?>";
var window_add_edit_name_category="<?=l('Add Category')?>";
var window_add_edit_name_currency="<?=l('Add Currency')?>";
function do_product_filter(id,div) {
			var data="related_id_category="+$("#related_id_category").val()+"&related_product_name="+$("#related_product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
}});
}
		function export_applicanti(type,an) {
			var gr = nss_grids['<?=$this->module?>'].ids();
			window.location='module/<?=$this->module?>.php?action=do_export_excel&type='+type;
			$('#window_<?=$this->module?>_exporte2').dialog( 'close' );
		}
$(document).ready(function(){

				$(".module_menu .export_excel").click(function(){
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_exporte2",window_add_edit_name,'module/<?=$this->module?>.php?action=export_excel&to='+gr,600,function(){},true);
			});
		});
function do_product_filter2(id,div) {
			var data="id_category="+$("#id_category2").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter2=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd2').html(r);
					
					}
				});
		}
function select_product2(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected').append('<div ><input type="hidden" name="products[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b><span style="margin-left:20px;">Produs de baza - <input type="checkbox" name="main['+id_this.value+']" value="1" /></span></div>')
			}
		}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function list_variations_options(id_var,id){
	$.ajax({
		data :"",
		type :"POST",
		url :'module/<?=$this->module?>.php?list_variations_options=1&id_var='+id_var+'&id='+id,
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#list_variations_options').html(r);
			/*
			tableToGrid("#table_variations");
			$("#gbox_table_variations").removeClass("ui-corner-all");
			$("#gview_table_variations .ui-jqgrid-bdiv").attr("style","height:360px; overflow-y:scroll; overflow-x:hidden;");
			*/
		}});
}

function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function load_brands(){
	$("#id_brand").load('module/<?=$this->module?>.php?json_brands=1');
}
function load_providers(){
	$("#id_furnizor").load('module/<?=$this->module?>.php?json_providers=1');
}
function json_currencies(){
	$("#currency").load('module/<?=$this->module?>.php?json_currencies=1');
}
function add_currency(){
	var module_now='currencies';
	nss_win(module_now + '_new', window_add_edit_name_currency,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',json_currencies);
	});
}
function add_brand(){
	var module_now='brands';
	nss_win(module_now + '_new', window_add_edit_name_brand,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_brands);
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function update_all_specification(id_category){
	$("#update_all_specification").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category);
}
function update_all_filter(id_category,ids){
	$("#update_all_filter").load('module/<?=$this->module?>.php?json_list_filters=1&id_category='+id_category+'&ids='+ids);
}
function update_all_category(id_category,ids){
	$("#update_all_category").load('module/<?=$this->module?>.php?json_list_categories=1&id_category='+id_category+'&ids='+ids);
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
	$(document).ready(function(){
	
			$("#main").css('height','1300');
			$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();

				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);

			});

			$(".module_menu .update_all_filters").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all_filters&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .port_categories").click(function(){
				var window_add_edit_name='<?=l('Porteaza categorie')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=port_categories&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .verify_codes").click(function(){
				var window_add_edit_name='<?=l('Verify codes')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=verify_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .export_codes").click(function(){
				var window_add_edit_name='<?=l('Export codes')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=export_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_provider").click(function(){
				var window_add_edit_name='<?=l('Aloca provider')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_provider&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_cadou").click(function(){
				var window_add_edit_name='<?=l('Update cadou')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_cadou&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_multistatus").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();

				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_multistatus&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .product_clon").click(function(){
				var window_add_edit_name='<?=l('Cloneaza produs')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=product_clon&id='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});

	});
<?php
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function json_product_filter() {
		?>
		<select name="products" multiple="multiple" size="20" style="width:560px" id="products">
		<?php $this->json_products(array(),$_POST['related_id_category'],$_POST['related_product_name']) ?>
		</select>
	<?php
	}
	function json_products($ids=array(),$related_id_category=0,$related_product_name="") {
		global $db;
		$sql_filters=array();
		if (!empty($related_id_category)) $sql_filters[]=" `xp_products`.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$related_id_category})";
		if (!empty($related_product_name)) $sql_filters[]=" `xp_products`.`name` LIKE '%".$related_product_name."%' ";
		$sql="SELECT * FROM `xp_products`
			WHERE `xp_products`.`status`='1' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC";
		//echo $sql;
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) { $i++;
		if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
		}
		?>
		<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> > <?=$linie['name']?> </option>
		<?php
		}
	}
	function json_products2($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		if (!empty($id_category)) $sql_filters[]=" p.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$id_category})";
		if (!empty($product_name)) $sql_filters[]=" ( pd.`name` LIKE  '%".$product_name."%' OR pd.`code` LIKE  '%".$product_name."%' ) ";
		$sql="select		p.*,
							pd.*,
							c.`id_category`

						from
							`".TABLE_PRODUCTS."` p

							inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`

							inner join `".TABLE_PRODUCTS."_to_categories` c
								on p.`id`=c.`id_product`
							where  `status`<'2' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")."   ORDER BY pd.`name` ASC limit 50";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
				$i++;
				if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
				}
			?>
			<option value="<?=$linie['id']?>" <?=((in_array($linie['id'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function json_product_filter2() {
		?>
		<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
			<?php $this->json_products2(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_list_row($row) {
		$admin_info=adm_info();
		global $gd,$db;

		$row['id_category']=$db->fetch_all("select `name` from `xp_categories_data` WHERE `_id` in (select `id_category` from `xp_products_to_categories` where `id_product`={$row['id']}) ");
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Nu exista categorie').'</span>';
		}else{
			foreach($row['id_category'] as$r)
				$all_artists[]=$r['name'];
			$row['id_category']=implode(', ',$all_artists);
		}
		
		if ($row ['is_multiple'] == 1) {
			$row ['is_multiple'] = '<img class="ch_staus_img" rel="is_multiple" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_multiple'] = '<img class="ch_staus_img" rel="is_multiple" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		
		$row['id_currency']=$db->fetch_one("SELECT code FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['id_currency']);
		$row['price']=number_format($row['price'],3,',','.');



		return $row;
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') {
			global_delete($this->table);
			calculate_promo();
		}
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".$id);
			foreach ($images as $image) unlink(SITEROOT."/".$image);
			$db->delete("xp_products_data",'id_product='.$id);
			$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_PRODUCTS_IMAGES,TABLE_FILTERS_VALUES)," `id_product`=".$id);

		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$data_saved=
			$db->fetch("
				select
					p.*,
					pd.*,
					c.`id_category`,
					pr.`start_date`,
					pr.`end_date`,
					pr.`id_product`,
					pr.`ids`,
					pe.`image`,
					pr.`type`
				from
					`{$this->table}` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`
					inner join `{$this->table}_to_categories` c
						on c.`id_product`=p.`id`
					left join `xp_promotions` pr
								on pr.`id_product`=p.`id`
					left join `".TABLE_PRODUCTS."_previews` pe
								on p.`id`=pe.`id_product`
				where p.`id`={$id}
			");
			
			$ids=$db->fetch_all("select * from `xp_products_to_pack` where `id_pack`=".$id." order by `id`");
			foreach ($ids as $i) {
				$k[]=$i['id_product'];
			}
			$data_saved['ids']=implode(",",(array)$k);
		}
		//print_a($data_saved);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">

		<?php
		print_form($this->form,$this,$data_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">

		var reload_related_products=function() {
			if($('#related_products_checkbox').attr('checked')) {
				$(".related_products_select").hide();
			} else {
				$(".related_products_select").show();
			}
		}


		$(document).ready(function(){

			



			$('#related_products_checkbox').click(reload_related_products);
			//$('#have_variations input').click(reload_variations_product);
			reload_related_products();
			//reload_variations_product();
			

		});

		</script>
		<?php
	}

	function save($id){
		global $db,$config,$gd;
		$data=$_POST;
		//print_a($data['ids']);
		//die();
		if ($data['related_products_checkbox']!=1) {
			$data['related_products']=implode(",",(array)$data['products']);
		} else $data['related_products']="";
		$data_discount=$data['discount'];
		$data_images=$data['images'];
		$data_image=$data['image'];
		$data_filters=$data['filters'];
		$data_variations=$data['variations'];
		$data_specifications=$data['specifications'];
		$inser_categ=$data['id_category'];


		$start_date=strtotime ( $data['start_date'] );
		$end_date=strtotime ( $data['end_date'] );

		$ids=$data['products'];
		$main=$data['main'];
		//print_a($main);
		//$ids=implode(",",(array)$data['products']);


		unset($data['main']);
		unset($data['water']);
		unset($data['filters']);
		unset($data['products_3']);
		unset($data['products_2']);
		unset($data['products2']);
		unset($data['ids']);
		unset($data['id_category']);
		unset($data['id_category2']);
		unset($data['images']);
		unset($data['image']);
		unset($data['discount']);
		unset($data['related_id_category']);
		unset($data['related_product_name']);
		unset($data['products']);
		unset($data['products_2']);
		unset($data['start_date']);
		unset($data['end_date']);
		unset($data['product_name']);
		unset($data['related_products']);
		unset($data['have_variations']);
		unset($data['variations']);
		unset($data['specifications']);
		unset($data['specifications_show']);
		unset($data['related_products_checkbox']);
		//if (empty($data['weight'])) $data['weight']=$db->fetch_one("SELECT `weight` FROM `".TABLE_CATEGORIES."` WHERE `id`=".$data['id_category']);
		$data['name_seo']=escapeIlegalChars($data['name']." ".$data['code']," ");
			$data2=array(
					'code'=>$data['code'],
					'waranty_description'=>$data['waranty_description'],
					'delivery_description'=>$data['delivery_description'],
					'transport_description'=>$data['transport_description'],
					'execution_description'=>$data['execution_description'],
					'points_description'=>$data['points_description'],
					'economy_description'=>$data['economy_description'],
					'now_description'=>$data['now_description'],
					'advantage_1'=>$data['advantage_1'],
					'advantage_2'=>$data['advantage_2'],
					'advantage_3'=>$data['advantage_3'],
					'advantage_4'=>$data['advantage_4'],
					'name'=>$data['name'],
					'name_seo'=>$data['name_seo'],
					'short_description'=>$data['short_description']
					//'description'=>$data['description']
					);
		if (isset($data['price_description'])) $data2['price_description']=$data['price_description'];
		if (isset($data['description'])) $data2['description']=$data['description'];
		unset($data['code']);
		unset($data['warranty']);
		unset($data['waranty_description']);
		unset($data['delivery_description']);
		unset($data['transport_description']);
		unset($data['execution_description']);
		unset($data['points_description']);
		unset($data['economy_description']);
		unset($data['now_description']);
		unset($data['advantage_1']);
		unset($data['advantage_2']);
		unset($data['advantage_3']);
		unset($data['advantage_4']);
		unset($data['name']);
		unset($data['name_seo']);
		unset($data['description']);
		unset($data['price_description']);
		unset($data['short_description']);
		//print_a($data);
		//print_a($data2);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			if (!empty($id)) $cond="AND `id_product`<>".$id;
			$exist=$db->fetch_one("SELECT `id_product` FROM `xp_products_data` WHERE `code`='{$data2['code']}' {$cond}");
			if (!$exist) {
				close_window($this->module,$id);
				unset($data['type_op']);
				if (!empty($id)) {
					

					$db->update($this->table, $data," `id`=".$id);
					$data2['name_seo']=$data2['name_seo']." ".$id;
					$db->update($this->table.'_data', $data2," `id_product`=".$id);
					//echo $inser_categ;
					$db->update('xp_products_to_categories', array('id_category'=>$inser_categ)," `id_product`=".$id);
					print_alerta('a fost updatat');
				} else {
					$data['date']=time();
					$exist=$db->fetch_one("SELECT `id_product` FROM `xp_products_data` WHERE `code`='{$db->escape($data2['code'])}'");
					if (!$exist) {
						$data['type']=1;
						$db->insert($this->table,$data);
						$id=mysql_insert_id();
						$data2['id_product']=$id;
						
						$data2['name_seo']=$data2['name_seo']." ".$id;
						$db->insert($this->table.'_data',$data2);
						$db->insert('xp_products_to_categories', array('id_category'=>$inser_categ,'id_product'=>$id));
					}
					print_alerta('a fost inserat');
				}
				
				upload_images($data_images,escapeIlegalChars($data2['name_seo'],"-"),'static/'.$this->folder3,'0x0',$id,TABLE_PRODUCTS_IMAGES,'id_product',$water);

				refresh_images(TABLE_PRODUCTS_IMAGES,$id,'id_product','imagini-produse');



				if ($ids) {
					$db->delete("xp_products_to_pack","`id_pack`=".$id);
					foreach ($ids as $i) {
						$db->insert("xp_products_to_pack",array('id_product'=>$i,'is_main'=>(isset($main[$i])?1:0),'id_pack'=>$id));
					}
				} else $db->delete("xp_products_to_pack"," `id_pack`=".$id);
//				if ($ids) {
//					$db->insert("xp_promotions",array('ids'=>$ids,'type'=>3,'name'=>'produs cadou','status'=>1,'id_product'=>$id,'start_date'=>$start_date,'end_date'=>$end_date,'date'=>time()));
//				}
				
			} else print_alerta('pe site mai exista un pachet cu acelasi cod');
		} else {
			print_form_errors($errors,$form);
		}
	}
	function update_all($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_specification(this.value);">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_all_specification"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_provider($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_providers&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Furnizor'));
		?>
		<dl>
		<dt>
			<label for="update_furnizor"><?=l('Furnizor')?></label>
		</dt>
		<dd>
		<select name="provider" id="update_furnizor" style="width:300px" onchange="update_furnizor(this.value);">
			<?php $this->json_providers() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_furnizor"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_cadou($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_cadou&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Update cadou'));
		print_form($this->cadou,array(),array());
		?>
		<!--<dl>
		<dt>
		<label for="update_cadou"><?=l('Nume cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="nume_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Pret cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="pret_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Text cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="text_cadou" value="" align="absmiddle" />
		</dd>
		</dl>-->
		<?php
		print_form_footer();
		?>
		<!--<div id="update_cadou"></div>-->
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_multistatus($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_multistatus&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Statut'));
		?>
		<dl>
		<dt>
			<label for="update_multistatus"><?=l('Statut')?></label>
		</dt>
		<dd>
		<select name="statusx" id="update_multistatus" style="width:300px">
			<?php
			foreach ($this->orders_status as $key=>$status) {
				?>
				<option value="<?=$key?>"><?=$status?></option>
				<?php
			}
			?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_multistatus"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_all_filters($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all_filters&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_filter(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_filter"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function port_categories($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_port_categories&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_category(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_category"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function verify_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=return_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="verify_codes"><?=l('Insert codes with "," delimiter')?></label>
		</dt>
		<dd>
			<textarea name="verify_codes" id="return_codes"></textarea>
		</dd>
		</dl>
		<div id="return_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Check codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function export_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=brand_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Brand'));
		?>
		<dl>
			<dt>
				<label for="brand_codes"><?=l('Select brand to export codes')?></label>
			</dt>
			<dd>
				<select name="brand_codes">
				<?=$this->json_brands();?>
				</select>
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="select_status"><?=l('Select status of products')?></label>
			</dt>
			<dd>
				<select name="select_status">
					<option value="1"><?=l('Active')?></option>
					<option value="0"><?=l('Inactive')?></option>
					<option value="x"><?=l('Ignore')?></option>
				</select>
			</dd>
		</dl>
		<div id="brand_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Export codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function save_all(){
		global $db;
		$data_specifications=$_POST['specifications'];
		$ids=explode(",",r('ids'));
		foreach ($ids as $key=>$id) {
			$exist=$db->fetch_all("SELECT `id` FROM `xp_products` WHERE `id`=".$id." AND `id_category`=".r('category'));
			if (empty($exist)) unset($ids[$key]);
		}
		close_window($this->module);
		if (is_array($data_specifications)) {
			//$db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_product` IN (".implode(",",$ids).")");
			foreach ($data_specifications as $key=>$value) {
				foreach ($ids as $id) {
					$array_values=array(
					'front'=>$_POST['specifications_show'][$key],
					);
					if (!empty($value)) $array_values['value']=$value;
					$db->update(TABLE_SPECIFICATIONS_VALUES,$array_values," `id_product`=".$id." AND `id_option`=".$key);
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function save_providers(){
		global $db;
		$data_provider=$_POST['provider'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `xp_products` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('id_furnizor'=>$data_provider),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_cadou(){
		global $db;
		//print_a($_POST);
		//die();
		$ids=explode(",",r('ids'));
		$ceva=$_POST['nss_languages'][LANG];
		//print_a($ceva);
		//die();
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `xp_products` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS.TABLE_EXTEND,$ceva,"id_main=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_multistatus(){
		global $db;
		$data_multistatus=$_POST['statusx'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `xp_products` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('status'=>$data_multistatus),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_port_categories(){
		global $db;
		$data_category=$_POST['category'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) foreach ($ids as $key=>$id) $db->update(TABLE_PRODUCTS."_to_categories",array('id_category'=>$data_category),"`id_product`=$id");
		print_alerta('a fost updatat');
	}
	function return_codes(){
		global $db;
		$ids=explode(",",r('verify_codes'));
		if (!empty($ids)) foreach ($ids as $k=>$code) {
			$exist=$db->fetch_one("SELECT `id` FROM `xp_products` WHERE `code`='{$code}'");
			if ($exist) $exists[]=$code; else $inexist[]=$code;
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile inexistente in baza de date sunt: <br />'.implode('<br />',$inexist).'<br /></div><div>codurile existe sunt: <br />'.implode('<br />',$exists).'<br /></div>');
	}
	function brand_codes(){
		global $db;
		$id_brand=r('brand_codes');
		$status=r('select_status');
		if (!empty($id_brand)) {
			if ($status!='x') $cond=" AND `status`={$status}"; else $cond='';
			$products=$db->fetch_all("SELECT `code` FROM `xp_products` WHERE `id_brand`={$id_brand} {$cond} ORDER BY `id_category` ASC");
			foreach ($products as $p) {
				$codes.=", ".$p['code'];
			}
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile existente in baza de date sunt: </div><div>'.ehtml(substr($codes,1)).'</div>');
	}
	function save_all_filters(){
		global $db;
		$data_filters=$_POST['filters'];
		$ignore=$_POST['ignore'];
		$ids=explode(",",r('ids'));

		foreach ($ids as $id) $db->delete(TABLE_FILTERS_VALUES,"`id_product`=".$id.(!empty($ignore)?" AND `id_filter` NOT IN (".implode(",",(array)$ignore).")":""));
		close_window($this->module);
		if (is_array($data_filters)) {
			foreach ($data_filters as $key=>$value) {
				foreach ($ids as $id) {
					if (!in_array($key,(array)$ignore)) {
						$db->delete(TABLE_FILTERS_VALUES," `id_product`={$id} AND `id_filter`={$key}");
						if (!empty($value)) {
							$array_values=array(
							'id_option'=>$value,
							'id_product'=>$id,
							'id_filter'=>$key,
							);
							$db->insert(TABLE_FILTERS_VALUES,$array_values);
						}
					}
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`=".(int)$id);
	}
	function do_export_excel() {
		global $db;
		//$countries=$db->fetch_all("SELECT * FROM `".TABLE_COUNTRIES."` ORDER by `name` ASC");
		if ($_GET['type']==3) $where=" WHERE ( p.`id` BETWEEN 20001 and 40000 ) and p.`status`='1'";
		if ($_GET['type']==2) $where=" WHERE ( p.`id` BETWEEN 10001 and 20000 ) and p.`status`='1'";
		if ($_GET['type']==1) $where=" WHERE ( p.`id` BETWEEN 1 and 10000 ) and p.`status`='1'";
		//ini_set("memory_limit","1024M");
		$products=$db->fetch_all("select
					p.*,
					pd.*,
					c.`id_category`

				from
					`{$this->table}` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

					inner join `{$this->table}_to_categories` c
						on c.`id_product`=p.`id` ".$where."");
//		echo "select
//					p.*,
//					pd.*,
//					c.`id_category`
//
//				from
//					`{$this->table}` p
//					inner join `xp_products_data` pd
//								on p.`id`=pd.`id_product`
//
//					inner join `{$this->table}_to_categories` c
//						on c.`id_product`=p.`id` ".$where."";
//		die();
		//header('Content-type: application/xls');
		//header("Content-type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="Nomenclator-produse.xls"');
		header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
		//header("Pragma: no-cache");
		//header("Expires: 0");
		echo '
		<html xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:x="urn:schemas-microsoft-com:office:excel"
		xmlns="http://www.w3.org/TR/REC-html40">

		<head>
		<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
		<meta name=ProgId content=Excel.Sheet>
		<!--[if gte mso 9]><xml>
		 <o:DocumentProperties>
		  <o:LastAuthor>ExpertVision.ro</o:LastAuthor>
		  <o:LastSaved>{$date}</o:LastSaved>
		  <o:Version>10.2625</o:Version>
		 </o:DocumentProperties>
		 <o:OfficeDocumentSettings>
		  <o:DownloadComponents/>
		 </o:OfficeDocumentSettings>
		</xml><![endif]-->
		<style>
		<!--table
		{mso-displayed-decimal-separator:"\.";mso-displayed-thousand-separator:"\,";}
		@page{margin:1.0in .75in 1.0in .75in;mso-header-margin:.5in;mso-footer-margin:.5in;}
		tr{mso-height-source:auto;}
		col{mso-width-source:auto;}
		br{mso-data-placement:same-cell;}
		.style0{mso-number-format:General;text-align:general;vertical-align:bottom;white-space:nowrap;mso-rotate:0;mso-background-source:auto;mso-pattern:auto;color:windowtext;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Arial;mso-generic-font-family:auto;mso-font-charset:0;border:none;mso-protection:locked visible;mso-style-name:Normal;mso-style-id:0;}
		td{mso-style-parent:style0;padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:windowtext;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Arial;mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;border:none;mso-background-source:auto;mso-pattern:auto;mso-protection:locked visible;white-space:nowrap;mso-rotate:0;}
		.xl24{mso-style-parent:style0;white-space:normal;}
		-->
		</style>
		<!--[if gte mso 9]><xml>
		 <x:ExcelWorkbook>
		  <x:ExcelWorksheets>
		   <x:ExcelWorksheet>
			<x:Name>Registration</x:Name>
			<x:WorksheetOptions>
			 <x:Selected/>
			 <x:ProtectContents>False</x:ProtectContents>
			 <x:ProtectObjects>False</x:ProtectObjects>
			 <x:ProtectScenarios>False</x:ProtectScenarios>
			</x:WorksheetOptions>
		   </x:ExcelWorksheet>
		  </x:ExcelWorksheets>
		  <x:WindowHeight>10005</x:WindowHeight>
		  <x:WindowWidth>10005</x:WindowWidth>
		  <x:WindowTopX>120</x:WindowTopX>
		  <x:WindowTopY>135</x:WindowTopY>
		  <x:ProtectStructure>False</x:ProtectStructure>
		  <x:ProtectWindows>False</x:ProtectWindows>
		 </x:ExcelWorkbook>
		</xml><![endif]-->
		</head>

		<body link=blue vlink=purple>
		<table x:str border=0 cellpadding=0 cellspacing=0 style="border-collapse: collapse;table-layout:fixed;">';
		echo "<tr>
			<th class=xl24 width=300><b>Cod produs</b></td>
			<th class=xl24 width=300><b>Cod EAN</b></td>
			<th class=xl24 width=300><b>Nume produs</b></td>
			<th class=xl24 width=300><b>Categorie</b></td>
			<th class=xl24 width=300><b>Subcategorie</b></td>
			<th class=xl24 width=300><b>Producator</b></td>
			<th class=xl24 width=300><b>Garantia</b></td>
			<th class=xl24 width=300><b>Descriere</b></td></tr>";
		//echo 'An$Prenume$Nume$Firma$Telefon$Inaltime$Greutate$Nota';
		foreach ($products as $product) {
					$categories='';
					$categories1='';
					$brand='';
					$categories=$db->fetch("select `name`,`id_parent` from `".TABLE_CATEGORIES."` WHERE `id`={$product['id_category']}");
					//$cid=$db->fetch_one("select `name` from `".TABLE_CATEGORIES."` WHERE `id`={$product['id_category']}");
					if ($categories['id_parent']>0) $categories1=$db->fetch_one("select `name` from `".TABLE_CATEGORIES."` WHERE `id`={$categories['id_parent']}");
					$brand=$db->fetch_one("select `name` from `".TABLE_BRANDS."` WHERE `id`={$product['id_brand']}");
					if ($brand=='') $brand='Fara Producator';


			echo "<tr>
				<td class=xl24 width=300>".$product['code']."</td>
				<td class=xl24 width=300>".$product['code_ean']."</td>
				<td class=xl24 width=300>{$product['name']}</td>
				<td class=xl24 width=300>".($categories1==''?$categories['name']:$categories1)."</td>
				<td class=xl24 width=300>".($categories1==''?'':$categories['name'])."</td>
				<td class=xl24 width=300>".$brand."</td>
				<td class=xl24 width=300>".$product['warranty']."</td>
				<td class=xl24 width=300>".strip_tags($product['description'])."</td></tr>";
			ob_flush();
		}
		 echo "</table></body></html>";
	}
	function export_excel($to='') {
		global $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			if (!file_exists(SITEROOT.'/'.$date_saved['image'])) $date_saved['image']='';
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Exporta excel'));
		?>
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 1')?>" onclick="export_applicanti(1);" />
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 2')?>" onclick="export_applicanti(2);" />
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 3')?>" onclick="export_applicanti(3);" />

		<br /><br />
	 <?php
	 print_form_footer();
			?>
			</form>
			<?php

		}
}
$module=new promo_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) {
	$new_sql="select sql_calc_found_rows
					p.*,
					pd.*

				from
					`{$module->table}` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`
					where p.`type`=1";
		json_list($module,false,$new_sql);
}
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_categories2']==1) $module->json_categories2(fget('id'));
elseif ($_GET['json_providers']==1) $module->json_providers(fget('id'));
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_currencies']==1) $module->json_currencies(fget('id'));
elseif ($_GET['json_list_filter']==1) $module->json_list_filter(fget('id_category'),fget('id'));
elseif ($_GET['json_list_specifications']==1) $module->json_list_specifications(fget('id_category'),fget('id'));
elseif ($_GET['json_list_filters']==1) $module->json_list_filters(fget('id_category'),fget('ids'));
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
elseif ($_GET['json_product_filter2']==1) $module->json_product_filter2();
elseif ($_GET['list_variations_options']==1) $module->list_variations_options(fget('id_var'),fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='product_clon') $module->new_a_clone(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='update_all') $module->update_all(fget('ids'));
elseif ($_GET['action']=='update_provider') $module->update_provider(fget('ids'));
elseif ($_GET['action']=='update_cadou') $module->update_cadou(fget('ids'));
elseif ($_GET['action']=='update_multistatus') $module->update_multistatus(fget('ids'));
elseif ($_GET['action']=='update_all_filters') $module->update_all_filters(fget('ids'));
elseif ($_GET['action']=='port_categories') $module->port_categories(fget('ids'));
elseif ($_GET['action']=='verify_codes') $module->verify_codes(fget('ids'));
elseif ($_GET['action']=='export_codes') $module->export_codes(fget('ids'));
elseif ($_GET['action']=='save_all') $module->save_all();
elseif ($_GET['action']=='save_providers') $module->save_providers();
elseif ($_GET['action']=='save_cadou') $module->save_cadou();
elseif ($_GET['action']=='save_multistatus') $module->save_multistatus();
elseif ($_GET['action']=='save_all_filters') $module->save_all_filters();
elseif ($_GET['action']=='save_port_categories') $module->save_port_categories();
elseif ($_GET['action']=='return_codes') $module->return_codes();
elseif ($_GET['action']=='brand_codes') $module->brand_codes();
elseif ($_GET['action']=='export_excel') $module->export_excel(fget('to'));
elseif ($_GET['action']=='do_export_excel') $module->do_export_excel();
else {

	print_header();
	$admin_info=adm_info();
	//if ($admin_info['username']=='admin') {
	print_content($module);
//	} else {
//		print_content($module,array("port_categories"=>l('Porteaza categorie'),"verify_codes"=>l('Verificare coduri'),"export_codes"=>l('Exporta coduri'),"update_provider"=>l('Aloca provider'),"update_multistatus"=>l('Update statut')),'','',array('delete'));
//	}
	//$module->new_a(fget('id'));
	print_footer();
}
?>