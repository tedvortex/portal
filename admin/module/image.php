<?php
require_once(dirname(dirname(__FILE__))."/includes/init.php");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="robots" content="index,nofollow" />
	<title><?=l('ADMINISTRATOR ZONE').' ('.$config['SITE_NAME'].') - Mod: ('.strip_tags($_GET['mod']).')'?></title>
	<link rel="stylesheet" type="text/css" href="includes/jquery/css/cupertino/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="includes/css/base.css" media="screen" />
<!--	<link rel="stylesheet" type="text/css" href="../includes/jquery/plugins/toChecklist/jquery.toChecklist.css" media="screen">-->
	
	<link rel="stylesheet" type="text/css" href="includes/jquery/jquery_ui_datepicker/timepicker_plug/css/style.css">
	



	<script type="text/javascript" src="includes/jquery/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery.ui.dialog.patch.js"></script>
	<!--<script type="text/javascript" src="includes/jquery/plugins/toChecklist/jquery.toChecklist.js"></script>-->
	<script type="text/javascript" src="includes/tiny/jscripts/tiny_mce/tiny_mce.js"></script>

	<script src="includes/jquery/jquery_ui_datepicker/timepicker_plug/timepicker.js" type="text/javascript"></script>

	



</head>
<body>
<div id="main">
	<div id="header">
		<div class="logo">
			<a href="index.php" rel="external follow"><img src="images/logo.png" alt="ExpertVision.ro" /></a>
		</div>
		<div class="titleAdmin" style="position:relative;">
			<?=l(ADMINISTRATOR_ZONE)?>
			<div style="position:absolute;font-size:16px;left:250px;width:400px;top:0;">
			   Aveti <b style="color:#FFA24F;font-size:16px;"><?=$totalComenzi?></b> <?=($totalComenzi==1)?'comanda in derulare':'comenzi in derulare'?>
			   <br/>Aveti <b style="color:#FF5FC7;font-size:16px;"><?=$totalComenzi2?></b> <?=($totalComenzi2==1)?'comanda in asteptare':'comenzi in asteptare'?>
			</div>
		</div>
		<div class="right_menu">
			<ul>
				<li><a href="index.php?action=index" ><img src="images/li_home.png" alt="home" /><?=l(ADMIN_HOME)?></a></li>
				<li><a href="<?=BASEHREF?>" target="_blank"><img src="images/li_site.png" alt="view" /><?=l(ADMIN_VIEW_WEBSITE)?></a></li>
				<li><a href="index.php?action=logout" ><img src="images/li_log.png" alt="logout" /><?=l(ADMIN_LOGOUT)?></a></li>
			</ul>
			<div id="languageDiv" style="height:80px;">
				<div id="flags">
					<?php
					$languages = get_languages ();
					//echo LANG;
					foreach ( $languages as $lang ) {
						if (LANG==$lang['prefix']) {
							$classImg='imgLimbaAct';
						} else {
							$classImg='imgLimba';
						}
					?>
					<a href="index.php?mod=<?=$mod?>&amp;lang=<?=$lang['prefix']?>" title="<?=$lang['name']?>"><img src="images/flags/<?=$lang['prefix']?>.png" width="22" height="16" alt="<?=$lang['name']?>" class="<?=$classImg?>" style="margin-left:5px;"/></a>
					<?}?>
				</div>
				<div class="clear"></div>
			</div>
			<?php
			$admin_info=adm_info();
			?>
			<div id="auth">
				<strong><?=l(AUTENTIFICAT_CA)?></strong> : <?=$admin_info['first_name']." ".$admin_info['last_name']?> <br />(<a href="mailto:<?=$admin_info['email']?>&amp;subject=From%20Owner:%20(<?=SITE_NAME?>)"><?=$admin_info['email']?></a>)
			</div>
		</div>
	</div>
	<?=print_menu()?>
	<div id="content">
	
	

	</div>
		<div id="footer">Powered by ExpertVision.ro &copy; 2004-<?=date('Y')?></div>
	</div>
</body>
</html>