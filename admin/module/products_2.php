<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class products_module {
	var $module='products';
	var $date='27-08-2009';
	var $table=TABLE_PRODUCTS;
	var $folder='imagini-produse';
	var $option_info;
	var $value_all_info;
	var $all_variations=array();
	var $all_variations_name=array();
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function products_module() {
		$this->name=l('Produse');
		$this->title=l('Produse');
		$this->description=l('Produse');
		$this->type=array(
		'like'=>array('name','id','code','description','price','views','cadou_nume'),
		'date'=>array('date'),
		'equal'=>array('status','id_category','id_brand','id_currency','id_furnizor')
		);
		$this->orders_status=array(
		1=>l('Activ'),
		0=>l('Inactiv'),
		);
		$this->grid=array( 
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Image 1'),'width'=>50,'align'=>'center','stype'=>'none'),
		'name'=>array('name'=>l('Nume produs'),'width'=>200),
		//'description'=>array('name'=>l('Descriere'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie produs'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		//'id_model'=>array('name'=>l('Marca/Model'),'width'=>80,'align'=>'center','stype'=>'select','options'=>'json_brands_filter'),
		//'id_motorizare'=>array('name'=>l('Motorizare'),'width'=>60,'align'=>'center','stype'=>'select','options'=>'json_brands_filter'),
		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>80,'align'=>'center','stype'=>'select'),
		'code'=>array('name'=>l('Cod'),'width'=>60),
		'price'=>array('name'=>l('Pret'),'width'=>50,'align'=>'right'),
		'currency'=>array('name'=>l('Moneda'),'width'=>50,'align'=>'left','stype'=>'select','options'=>'show_select_currencies'),
		'views'=>array('name'=>l('Views'),'width'=>40,'align'=>'center'),
		//'stock'=>array('name'=>l('Stoc'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'af_home'=>array('name'=>l('Home'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_promo'=>array('name'=>l('Promotia zilei'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'cadou_nume'=>array('name'=>l('Nume cadou'),'width'=>70,'align'=>'center'),
		'date'=>array('name'=>l('Date'),'width'=>80,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Detalii produs'),
	//	2=>l('Produse asemanatoare'),
	//	3=>l('Campuri configurabile'),
	//	4=>l('Valori filtre'),
	//	5=>l('Variatii produs'),
		//6=>l('Reguli discount'),
		//7=>l('Produs Cadou')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','lang'=>true,'name'=>l('Nume'),'valid'=>'empty,min_3','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('header title'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Name seo'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		1=>l('Alte detalii'),
		//2=>'show_furnizori',
		//3=>'show_brands',
		4=>'show_categories',
		5=>'show_currencies',
		'price'=>array('type'=>'input','name'=>l('Pret'),'info'=>l('Pret')),
		'code'=>array('type'=>'input','name'=>l('Cod'),'info'=>l('Code')),
		'stock'=>array('type'=>'input','name'=>l('Nivelul de stoc'),'valid'=>'numeric','info'=>l('Nivelul stocului')),
		//'warranty'=>array('type'=>'input','name'=>l('Garantie'),'lang'=>true),
		//		'low_stock'=>array('type'=>'input','name'=>l('Nivel mic de stoc'),'valid'=>'numeric','info'=>l('Nivelul stocului')),
		//		'rating'=>array('type'=>'input','name'=>l('rating'),'valid'=>'empty','info'=>l('rating')),
		//		'votes'=>array('type'=>'input','name'=>l('votes'),'valid'=>'numeric','info'=>l('votes')),
		//'weight'=>array('type'=>'input','name'=>l('Greutate'),'info'=>l('Greutate produs')),
		//'tax_price'=>array('type'=>'input','name'=>l('Taxa livrare'),'info'=>l('Taxa livrare')),
		'is_homepage'=>array('type'=>'radio','name'=>l('Vizibil pe homepage'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		15=>l('Imagini'),
		'images'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder,'multiple'=>true,'images'=>'return_images'),
	//	17=>array('tab'=>2),
	//	'related_products_checkbox'=>array('type'=>'checkbox','name'=>l('Produse asemanatoare'),'text'=>l('Gaseste si arata automat produse asemanatoare'),'info'=>l('Bifati aceasta casuta si o lista cu produse asemanatoare va fi aleasa si afisata in magazinul dvs. Debifati casuta pentru a nu arata produsele asemanatoare sau pentru a alege dumneavoastra produsele asemanatoare, folosind instructiunile:1. Alegeti o categorie si produsele din acea categorie vor fi afisate; 2. Faceti dublu-click pe un produs pentru a-l adauga ca produs asemanator; 3. Pentru a sterge un produs din lista de produse asemanatoare, faceti dublu-click pe el in josul boxului')),
	//	18=>'list_related_products',
		//20=>array('tab'=>3),
		//21=>'list_specifications',
		//25=>array('tab'=>4),
		//26=>'list_filters',
		//30=>array('tab'=>5),
		//'have_variations'=>array('type'=>'radio','name'=>l('status'),'options'=>array(0=>l('Will not have any variations in my_store'),1=>l('Will use a product variation created '))),
	//	32=>'list_variations_tab',
		//40=>array('tab'=>6),
	//	50=>'list_discount_rules',
	//	51=>array('tab'=>7),
	//	'cadou_nume'=>array('type'=>'input','lang'=>true,'name'=>l('Nume produs'),'style'=>'width:92%;'),
	//	'cadou_pret'=>array('type'=>'input','name'=>l('Pret produs cadou'),'lang'=>true,'style'=>'width:92%;'),
	//	'cadou_description'=>array('type'=>'editor','name'=>l('Descriere produs cadou'),'lang'=>true)
		);
	//	$this->cadou=array('cadou_nume'=>array('type'=>'input','lang'=>true,'name'=>l('Nume produs'),'style'=>'width:92%;'),
	//	'cadou_pret'=>array('type'=>'input','name'=>l('Pret produs cadou'),'lang'=>true,'style'=>'width:92%;'),
	//	'cadou_description'=>array('type'=>'editor','name'=>l('Descriere produs cadou'),'lang'=>true)
	//	);
	}
	function return_images($data) {
		return fa("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$data['id']." ORDER BY `id` DESC");
	}
	function json_list() {
		json_list($this,true);
	}
	function css() {
		?>
		#uploaded_images img{ cursor:move; }
		.module_menu li.update_all { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.update_all_filters { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.port_categories { background-image:url('../../images/icons/products_port.png'); }
		.module_menu li.verify_codes { background-image:url('../../images/icons/products_verify_codes.png'); }
		.module_menu li.export_codes { background-image:url('../../images/icons/products_export_codes.png'); }
		.module_menu li.update_provider { background-image:url('../../images/icons/update_provider.png'); }
		.module_menu li.update_multistatus { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.product_clon { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.update_cadou { background-image:url('../../images/icons/update_cadou.png'); }
		<?php
	}
	function list_specifications($date) {
		?>
		<div id="list_specifications">
		<?php $this->json_list_specifications($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function show_furnizori($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_furnizor"><?=l('Furnizor')?></label>
			</dt>
			<dd>
				<select name="id_furnizor" id="id_furnizor" style="width:200px">
				<option value="0">Fara furnizor</option>
						<?php

						$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
						while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$date['id_furnizor'])?'selected':''?> > <?=$linie['name']?> </option>
			<?php
						} 
		 ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function json_list_specifications($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
			print_form_header ($filter['name']);
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $specification_info=$db->fetch("SELECT `value`,`front` FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".$id." AND `id_option`=".$option['id']);
		?>
		<dl>
		<dt>
			<label for="products_specification_<?=$option['id']?>"><?=$option['name']?></label>
		</dt>
		<dd>
			<input type="checkbox" name="specifications_show[<?=$option['id']?>]" value="1" align="absmiddle"  <?=(($specification_info['front']==1)?'checked':'')?> />
			<input type="text"  style="width:60%;" name="specifications[<?=$option['id']?>]" value="<?=$specification_info['value']?>" align="absmiddle" />

		</dd>
		</dl>
		<?php
			}
			print_form_footer();
		}
	}
	function json_list_filters($id_category=0,$ids=0) {?>
		<div id="list_filters">
		<?php $this->json_list_filter2($id_category,$ids); ?>
		</div>
		<?php
	}
	function list_filters($date) {
		?>
		<div id="list_filters">
		<?php $this->json_list_filter($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function json_list_filter($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label>
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
				?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function json_list_filter2($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label><br />
			<input type="checkbox" name="ignore[]" value="<?=$filter['id']?>" /> Ignora
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function list_variations_tab($date) {
		global $db;
		$all_variations=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_NAME."` WHERE status=1 ORDER BY `name` ASC");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Variation')?></label>
			</dt>
			<dd>
			<select name="id_variation" onchange="list_variations_options(this.value,<?=(int)$date['id']?>);">
			<option value="0"> <?=l('Choose a variation')?> </option>
				<?php foreach ($all_variations as $variation) { ?>
				<option value="<?=$variation['id']?>" <?=($date['id_variation']==$variation['id']?'selected':'')?>><?=ehtml($variation['name'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<br clear="all" />
		<div id="list_variations_options" class="product_have_variations">
		<?php
		if (!empty($date['id_variation'])) {
			$this->list_variations_options($date['id_variation'],$date['id']);
		}
		?>
		</div>
		<?php
	}
	function get_rec($j=0) {
		$variation='';
		$variation_name='';
		$k=0;
		$max=sizeof($this->option_info);
		foreach ($this->option_info as $option) {
			$k++;
			foreach ($this->value_all_info[$option['id']] as $value) {
				$variation_temp=$variation.$value['id'];
				if ($k<$max) $variation_temp.="-";
				if ((!in_array($variation_temp,$this->all_variations))) {
					$variation.=$value['id'];
					$variation_name.=$value['name'];
					if ($k<$max) {
						$variation.="-";
						$variation_name.=" - ";
					}
					$array_variation=explode("-",$variation);
					$end_array_variation=end($array_variation);
					if ((sizeof($array_variation)==$max ) && (!empty($end_array_variation))) {
						$this->all_variations_name[$j]=$variation_name;
					}
					$this->all_variations[$j]=$variation;
					break;
				}
			}
		}
		if (!empty($this->all_variations[$j])) $this->get_rec($j+1);
	}
	function list_variations_options($id_variation,$id_product) {
		global $db;
		if (!empty($id_variation)) {
			$this->option_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`='".$id_variation."' ORDER BY `id`");

			$this->value_all_info=array();
			foreach ($this->option_info as $option) {
				$value_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`='".$option['id']."'");
				$this->value_all_info[$option['id']]=$value_info;
			}
			$this->get_rec();
			$product_info_variations=array();
			if (!empty($id_product)) {
				$resursa=$db->sql("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product));
				while ($product_variations=$db->fetch($resursa)) {
					$product_info_variations[str_replace("-","_",$product_variations['variation'])]=$product_variations;
				}
			}
?>
<input type="hidden" name="variations[nss_all_variations]" id="nss_all_variations" />
<div style="width:856px;">
		<table cellpadding="0" cellspacing="0" border="0"  id="table_variations" >
			<thead>
			<tr>
				<th style="width:20px"><input type="checkbox" name="aaaaaa" /></th>
				<th><?=Variation_name?></th>
				<th style="width:72px"><?=Cod?></th>
				<th style="width:72px"><?=Stoc?></th>
				<th style="width:140px"><?=Pret?></th>
				<th style="width:140px"><?=Greutate?></th>
				<th style="width:200px;"><?=imagine?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($this->all_variations_name as $key=>$variation_name) {
				$var=str_replace("-","_",$this->all_variations[$key]);
			?>
			<tr id="tr_<?=$var?>">
				<td align="center" >
					<input type="checkbox" name="variations[<?=$var?>][variation]" value="<?=$var?>" <?=(isset($product_info_variations[$var])?'checked':'')?> />
				</td>
				<td >
					<?=$variation_name?>
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][code]" size="8" value="<?=$product_info_variations[$var]['code']?>" />
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][stock]" size="8" value="<?=$product_info_variations[$var]['stock']?>" />
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][price_type]">
					<?php $price_type=$product_info_variations[$var]['price_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($price_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($price_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($price_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($price_type==0)?'style="display: none;"':''?>>
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][price]" value="<?=$product_info_variations[$var]['price']?>" />
					</span>
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][weight_type]">
					<?php $weight_type=$product_info_variations[$var]['weight_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($weight_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($weight_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($weight_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($weight_type==0)?'style="display: none;"':''?> >
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][weight]"  value="<?=$product_info_variations[$var]['weight']?>" />
					</span>
				</td>
				<td>
					<?php if (!empty($product_info_variations[$var]['image'])) { ?>
						<div id="image_delete_<?=$var?>">
						<span style="display:none;"><img src="<?=$gd->url('resize','../'.$product_info_variations[$var]['image'],'260x260s-#f6f1f6')?>" alt="<?=$variation_name?>" /></span>
								<a href="#" class="toolTipInfoImg" title="<?=$variation_name?>"><?=$variation_name?></a>
								&nbsp; &raquo; &nbsp;
								<a class="Action" href="javascript: delete_variation_image('image_delete_<?=$var?>',<?=$product_info_variations[$var]['id']?>);"><?=Delete?></a>
						</div>
					<?php } else { ?>
						<input type="file" name="variations[<?=$var?>][image]" />
					<?php } ?>

				</td>
			</tr>

			<?php
			}
			?>
		</tbody>
		</table>
		</div>

		<!--	<tr id="tr_next_<?=$var?>" class="bodyTr" onmouseover="this.className='bodyTrHover'; $('#tr_<?=$var?>').attr('class','bodyTrHover');" onmouseout="this.className='bodyTr';$('#tr_<?=$var?>').attr('class','bodyTr');">
			<td colspan="7" style="padding-bottom:4px; border-bottom:1px solid #EFE2F0;"><input type="text" style="width:99%" name="short_description[]" value="<?=$product_info_variations[$var]['short_description']?>"/></td>
			</tr>-->
<?php }
	}
	function product_images($date) {
		global $db;
		?>
		<div id="uploaded_images" class="ui-helper-clearfix">
				<?php
				if (!empty($date['id'])) {
					$product_images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$date['id']);
					foreach ($product_images as $image) {
						if (!file_exists(SITEROOT."/".$image['image'])) {
							$db->delete(TABLE_PRODUCTS_IMAGES,"`id`=".$image['id']);
							continue;
						}
					?>
					<div style="padding:2px;position:relative;float:left;">
						<input type="hidden" name="images[]" value="<?=ehtml ( $image ['image'] )?>" />
						<img align="absmiddle" src="../?sgd&mode=resize&file=<?=$image['image']?>&args=100x100&<?=time()?>" height="100" style="border:1px solid #9F9F9F;" />
						<img src="images/icons/file_remove.png" style="cursor:pointer;position:absolute;top:5px;right:4px;" onclick="delete_uploaded('<?=$image['image']?>',this);" />
						<!--<input type="radio" name="image" value="<?=$image['id']?>" <?=(($image['id']==$date['image'])?'checked':'')?> style="cursor:pointer;position:absolute;top:6px;left:6px;"  />-->
					</div>
					<?php
					}
				}
				?>
		</div>
		<input type="file" id="file_upload_images" />
	<?php
	}
	function list_discount_rules($date) {
		global $db;
		if (!empty($date)) $product_discount=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_DISCOUNT_RULES."` WHERE `id_product`=".(int)$date['id']);
		if (empty($product_discount)) $product_discount=array(1);
		foreach ($product_discount as $discount) {
		?>
		<div style="padding:2px;">
			<?=l('Clienti care au cumparat intre')?>
			<input type="text" name="discount[quantity_from][]" value="<?=$discount['quantity_from']?>" size="4" style="text-align:center;" /> -
			<input type="text" name="discount[quantity_to][]" value="<?=$discount['quantity_to']?>" size="4" style="text-align:center;" />
			<?=l('articolele primesc')?>
			<select name="discount[type][]" align="absmiddle" style="width:140px">
				<option value="0" <?=($discount['type']==0?'selected':'')?>><?=l('Pret discount')?></option>
				<option value="1" <?=($discount['type']==1?'selected':'')?>><?=l('Discount in procente')?></option>
				<option value="2" <?=($discount['type']==2?'selected':'')?>><?=l('Pret fix')?></option>
			</select>
			<?=l('discount')?>
			<input type="text" name="discount[discount][]" value="<?=$discount['discount']?>" size="4" style="text-align:center;" />
			<?=l('pentru fiecare articol')?>
			<img src="images/addicon.gif" align="absmiddle" class="add" class="pointer" />
		 	<img src="images/delicon.gif" align="absmiddle" class="rem" class="pointer" />
		</div>
		<?php
		}
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produs')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_brand"><?=l('Brand')?></label>
			</dt>
			<dd>
				<select name="id_brand" id="id_brand" style="width:200px">
				<?php $this->json_brands($date['id_brand']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga producator')?>" onclick="add_brand();" />
			</dd>
		</dl>
		<?php
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function show_select_category() {
	$_GET['from_grid']=1;
		$this->json_categories();
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$categories=lang_fetch_all(TABLE_CATEGORIES," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function json_providers($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_FURNIZORI."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_provider)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_brands_filter() {
		//$this->json_brands(0,true);
	}
	function json_brands($id_brand=0,$from_grid=false) {
		global $db;
		if ($id_brand==0  && !$from_grid) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($from_grid) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_BRANDS."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function js() {
		$after='$("#gbox_list_'.$this->module.'").css({"borderRight":"0px none"}).removeClass("ui-corner-all"); ';
		$loadComplete='
			$("#gs_id_category").load("module/'.$this->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
			$("#gs_id_brand").load("module/'.$this->module.'.php?json_brands=1&from_grid=1&id="+$("#gs_id_brand").val());
		';
		set_grid($this,array('multiselect'=>'true','sortname' => 'date','sortorder'=>'desc','loadComplete'=>$loadComplete),'',$after);
?>
var global_window=true;
var window_width=900;
var window_add_edit_name_brand="<?=l('Add Brand')?>";
var window_add_edit_name_category="<?=l('Add Category')?>";
var window_add_edit_name_currency="<?=l('Add Currency')?>";
function do_product_filter(id,div) {
			var data="related_id_category="+$("#related_id_category").val()+"&related_product_name="+$("#related_product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
				$("#list_products_dd select").toChecklist();
}});
}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function list_variations_options(id_var,id){
	$.ajax({
		data :"",
		type :"POST",
		url :'module/<?=$this->module?>.php?list_variations_options=1&id_var='+id_var+'&id='+id,
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#list_variations_options').html(r);
			/*
			tableToGrid("#table_variations");
			$("#gbox_table_variations").removeClass("ui-corner-all");
			$("#gview_table_variations .ui-jqgrid-bdiv").attr("style","height:360px; overflow-y:scroll; overflow-x:hidden;");
			*/
		}});
}
function add_currency(){
	var module_now='currencies';
	nss_win(module_now + '_new', window_add_edit_name_currency,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',json_currencies);
	});
}
function add_brand(){
	var module_now='brands';
	nss_win(module_now + '_new', window_add_edit_name_brand,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_brands);
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function update_all_specification(id_category){
	$("#update_all_specification").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category);
}
function update_all_filter(id_category,ids){
	$("#update_all_filter").load('module/<?=$this->module?>.php?json_list_filters=1&id_category='+id_category+'&ids='+ids);
}
function update_all_category(id_category,ids){
	$("#update_all_category").load('module/<?=$this->module?>.php?json_list_categories=1&id_category='+id_category+'&ids='+ids);
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
	$(document).ready(function(){
			$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_all_filters").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all_filters&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .port_categories").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=port_categories&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .verify_codes").click(function(){
				var window_add_edit_name='<?=l('Verify codes')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=verify_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .export_codes").click(function(){
				var window_add_edit_name='<?=l('Export codes')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=export_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_provider").click(function(){
				var window_add_edit_name='<?=l('Aloca provider')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_provider&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_cadou").click(function(){
				var window_add_edit_name='<?=l('Update cadou')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_cadou&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_multistatus").click(function(){
				var window_add_edit_name='<?=l('Update statut')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_multistatus&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .product_clon").click(function(){
				var window_add_edit_name='<?=l('Cloneaza produs')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=product_clon&id='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});

	});
<?php
	}
	
	function show_select_currencies() {
		$_GET['from_grid']=1;
		$this->json_currencies();
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function json_product_filter() {
	?>
	<select name="products" multiple="multiple" size="20" style="width:560px" id="products">
	<?php $this->json_products(array(),$_POST['related_id_category'],$_POST['related_product_name']) ?>
	</select>
	<?php
	}
	function json_products($ids=array(),$related_id_category=0,$related_product_name="") {
		global $db;
		$sql_filters=array();
		if (!empty($related_id_category)) $sql_filters[]=" `".TABLE_PRODUCTS."`.`id_category`=".$related_id_category;
		if (!empty($related_product_name)) $sql_filters[]=" `".TABLE_PRODUCTS.TABLE_EXTEND."`.`name` LIKE '%".$related_product_name."%' ";
		$sql="SELECT * FROM `".TABLE_PRODUCTS."`,`".TABLE_PRODUCTS.TABLE_EXTEND."`
			WHERE `".TABLE_PRODUCTS."`.`id`=`".TABLE_PRODUCTS.TABLE_EXTEND."`.`id_main`
			AND `".TABLE_PRODUCTS.TABLE_EXTEND."`.`lang`='".LANG."' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC";
		//echo $sql;
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) { $i++;
		if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
		}
		?>
		<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> > <?=$linie['name']?> </option>
		<?php
		}
	}
	function list_related_products($date) {
		global $db;
		$date['related_products']=explode(",",$date['related_products']);
	?>
	<dl class="related_products_select">
	<dt>
	<label for="products"><?=l('Categorie produs')?></label>
	</dt>
	<dd>
	<select name="related_id_category" id="related_id_category" style="width:560px" onchange="do_product_filter();">
	<option value="0"><?=l('Toate')?></option>
	<?php $this->json_categories() ?>
	</select>
	</dd>
	</dl>
	<dl class="related_products_select">
	<dt>
	<label for="products"><?=l('Nume produs')?></label>
	</dt>
	<dd><input type="text" name="related_product_name" id="related_product_name" style="width:560px" onkeyup="do_product_filter();" />
	</dd>
	</dl>
	<dl class="related_products_select">
	<dt>
	<label for="products"><?=l('Produse')?></label>
	</dt>
	<dd id="list_products_dd">
	<select name="products" multiple="multiple" size="20" style="width:560px">
	<?php $this->json_products($date['related_products']); ?>
	</select>
	</dd>
	</dl>
		<?php
	}
	function json_list_row($row) {
		$admin_info=adm_info();
		global $gd,$db;
		global $gd,$db;
		$row['name']="<a href=".lang_item_link(TABLE_CATEGORIES,$row)." target='_blank'>".$row['name']."</a>";
		$row['id_category']=lang_recurent_categ_title(TABLE_CATEGORIES,$row['id_category'],'','','');
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Categoria nu mai exista').'</span>';
		}
		//$row['id_brand']=$db->fetch_one("SELECT `name` FROM `".TABLE_BRANDS."` WHERE `id`=".$row['id_brand']);
		if ($row['id_brand']==false) {
			$row['id_brand']='<span style="color:red;">'.l('Producatorul nu mai exista').'</span>';
		}
		//$row['id_furnizor']=$db->fetch_one("SELECT `name` FROM `".TABLE_FURNIZORI."` WHERE `id`=".$row['id_furnizor']);
		if ($row['id_furnizor']==false) {
			$row['id_furnizor']='<span style="color:red;">'.l('Furnizorul nu mai exista').'</span>';
		}
		if ($row ['af_home'] == 1) {
			$row ['af_home'] = '<img class="ch_staus_img" rel="af_home" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['af_home'] = '<img class="ch_staus_img" rel="af_home" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		if ($row ['is_promo'] == 1) {
			$row ['is_promo'] = '<img class="ch_staus_img" rel="is_promo" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_promo'] = '<img class="ch_staus_img" rel="is_promo" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		$row ['order'] = '<input size="2" name="order['.$row ['id'].']" value="'.$row ['order'].'" style="text-align:center" onchange="set_product_order(this.value,'.$row ['id'].')" />';


		$row['image']='<img src="'.image_link(lang_item_image(TABLE_PRODUCTS_IMAGES,$row,'imagini-produse'),40).'" alt="'.$row['title'].'" />';

		//$number=$row['price'];
		$row['id_currency']=$db->fetch_one("SELECT code FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['id_currency']);
		$row['price']=number_format($row['price'],2,',','.');
		//$row['price']=$price_new;
		//$row['currency']=$currencies['code'];

		return $row;
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table,true);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".$id);
			foreach ($images as $image) unlink(SITEROOT."/".$image);
			$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_VARIATIONS,TABLE_PRODUCTS_DISCOUNT_RULES,TABLE_PRODUCTS_IMAGES,TABLE_FILTERS_VALUES)," `id_product`=".$id);
		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$data_saved=lang_fetch($this->table,"`id`=".$id);
			if (!empty($data_saved['id_variation'])) $data_saved['have_variations']=1;
			if (empty($data_saved['related_products'])) $data_saved['related_products_checkbox']=1;
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">

		<?php
		print_form($this->form,$this,$data_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">

		var reload_related_products=function() {
			if($('#related_products_checkbox').attr('checked')) {
				$(".related_products_select").hide();
			} else {
				$(".related_products_select").show();
			}
		}
		var reload_variations_product=function() {
			var var_name = $("#have_variations input:checked").val();
			if(var_name==1) {
				$(".product_have_variations").show();
			} else {
				$(".product_have_variations").hide();
			}
		}

		$(document).ready(function(){

			tableToGrid("#table_variations");
			$("#gbox_table_variations").removeClass("ui-corner-all");
			$("#gview_table_variations .ui-jqgrid-bdiv").attr("style","height:360px; overflow-y:scroll; overflow-x:hidden;");

			$("#table_variations").setGridParam({
				onSelectRow: function(id){ $("#nss_all_variations").val($("#table_variations").getGridParam('selarrrow')); }
			});



			$('#related_products_checkbox').click(reload_related_products);
			$('#have_variations input').click(reload_variations_product);
			reload_related_products();
			reload_variations_product();
			$('#list_products_dd select').toChecklist();
			$("#uploaded_images").sortable({opacity: 0.7}) ;

			// 'sizeLimit':<?=(1024 * 1024 * 2)?>,
			var ts=setTimeout(function() {

				$("#file_upload_images").uploadify({
				'uploader':'includes/jquery-plugins/uploadify/uploadify.swf',
				'script':'includes/jquery-plugins/uploadify/uploadify.php',
				'folder':'<?=$this->folder?>',
				'checkScript':'includes/jquery-plugins/uploadify/check.php',
				'auto':true,
				'fileDesc':'<?=l ( 'Sunt acceptate doar imagini' )?>',
				'fileExt':'*.jpg;*.gif;*.jpeg;*.png',
				'sizeLimit':<?=(1024 * 1024 * 2)?>,
				'buttonText':'<?=l('Upload more')?>',
				'onComplete':function(event,queueID,fileObj,response,data){
					$("#uploaded_images").html($("#uploaded_images").html()+"<div style=\"padding:2px;position:relative;float:left;\"><input type=\"hidden\" name=\"images[]\" value=\"<?=$this->folder?>/"+fileObj.name+"\" /><img src=\"../?sgd&mode=resize&file=<?=$this->folder?>/"+fileObj.name+"&args=100x100s-#FFFFFF\" height=\"100\" style=\"border:1px solid #9F9F9F;\" /><img src=\"images/icons/file_remove.png\" style=\"cursor:pointer;position:absolute;top:5px;right:4px;\" onclick=\"delete_uploaded('<?=$this->folder?>/"+fileObj.name+"',this);\" /></div> ");
					return true;
				},
				'cancelImg':'includes/jquery-plugins/uploadify/cancel.png'
				});


			},600);

		});

		</script>
		<?php
	}
	function new_a_clone($id){
		global $db,$config,$gd;
		$product=$db->fetch("SELECT * FROM `".$this->table."` WHERE `id`=".(int)$id,MYSQL_ASSOC);
		unset(
		$product['code'],
		$product['id']
		);
		$product['date']=time();
		$product['views']=0;

		$db->insert($this->table,$product);
		$id_product=$db->insert_id();
		$product_desc=$db->fetch("SELECT * FROM `".$this->table.TABLE_EXTEND."` WHERE `id_main`=".(int)$id,MYSQL_ASSOC);
		$product_desc['id_main']=$id_product;
		unset($product_desc['id']);
		$product_desc['name']=$product_desc['name']." clona";
		$product_desc['name_seo']=$product_desc['name_seo']." clona";
		$db->insert($this->table.TABLE_EXTEND,$product_desc);

		$data_discount=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_DISCOUNT_RULES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_discount)) {
			foreach ($data_discount as $key=>$discount) {
				if (!empty($discount['discount'])) {
					$array_values=array(
					'id_product'=>$id_product,
					'quantity_from'=>$discount['quantity_from'],
					'quantity_to'=>$discount['quantity_to'],
					'discount'=>$discount['discount'],
					'type'=>$discount['type'],
					);
					$db->insert(TABLE_PRODUCTS_DISCOUNT_RULES,$array_values);
				}
			}
		}
		$data_specifications=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_specifications)) {
			foreach ($data_specifications as $key=>$value) {
				$array_values=array(
				'id_product'=>$id_product,
				'id_option'=>$value['id_option'],
				'value'=>$value['value'],
				'front'=>$value['front'],
				);
				$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_values);
			}
		}
		$data_specifications=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_filters)) {
			foreach ($data_filters as $key=>$filters) {
				foreach ($filters as $filter) {
					$array_values=array(
					'id_product'=>$id_product,
					'id_filter'=>$filters['id_filter'],
					'id_option'=>$filters['id_option']
					);
					$db->insert(TABLE_FILTERS_VALUES,$array_values);
				}
			}
		}

		$this->new_a($id_product);
	}
		function save($id){
		global $db,$config,$gd;
		$data=$_POST;
		/*if ($data['related_products_checkbox']!=1) {
		$data['related_products']=implode(",",(array)$data['products']);
		} else $data['related_products']="";*/
		$data_discount=$data['discount'];
		$data_images=$data['images'];
		$data_filters=$data['filters'];
		$data_variations=$data['variations'];
		$data_specifications=$data['specifications'];
		unset($data['filters'],$data['images'],$data['discount'],$data['related_id_category'],$data['related_product_name'],$data['products'],$data['have_variations'],$data['variations'],$data['specifications'],$data['specifications_show'],$data['related_products_checkbox']);
		//		if (empty($data['weight'])) $data['weight']=$db->fetch_one("SELECT `weight` FROM `".TABLE_CATEGORIES."` WHERE `id`=".$data['id_category']);
		foreach ($data['nss_languages'] as $lang=>$vars) $data['nss_languages'][$lang]['name_seo']=escapeIlegalChars($data['nss_languages'][$lang]['name']," ");
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			if (!empty($id)) $cond="AND `id_main`<>".$id;
			$exist=$db->fetch_one("SELECT `id_main` FROM ".$this->table.TABLE_EXTEND." WHERE `name_seo`='{$data['nss_languages'][$lang]['name_seo']}' {$cond}");
			if (!$exist) {
				if (!empty($id)) {
					$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_VARIATIONS,TABLE_PRODUCTS_DISCOUNT_RULES,TABLE_FILTERS_VALUES),"`id_product`=".$id);
					lang_update($this->table, $data," `id`=".$id);
				} else {
					$data['date']=time();
					$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='{$data['code']}'");
					if (!$exist) $id=lang_insert($this->table, $data);
				}
				//print_a($data_images);
				upload_images($data_images,escapeIlegalChars($data['nss_languages'][LANG]['name_seo'],"-"),'static/'.$this->folder,'0x0',$id,TABLE_PRODUCTS_IMAGES,'id_product',true);
				if (is_array($data_discount)) {
					foreach ($data_discount as $key=>$discount) {
						if (!empty($discount['discount'])) {
							$array_values=array(
							'id_product'=>$id,
							'quantity_from'=>$discount['quantity_from'],
							'quantity_to'=>$discount['quantity_to'],
							'discount'=>$discount['discount'],
							'type'=>$discount['type'],
							);
							print_a($discount);
							$db->insert(TABLE_PRODUCTS_DISCOUNT_RULES,$array_values);
						}
					}
				}
				if (is_array($data_specifications)) {
					foreach ($data_specifications as $key=>$value) {
						$array_values=array(
						'id_product'=>$_GET['id'],
						'id_option'=>$key,
						'value'=>$value,
						'front'=>$_POST['specifications_show'][$key],
						);
						$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_values);
					}
				}
				if (is_array($data_filters)) {
					foreach ($data_filters as $key=>$filters) {
						foreach ($filters as $filter) {
							$array_values=array(
							'id_product'=>$_GET['id'],
							'id_filter'=>$key,
							'id_option'=>$filter
							);
							$db->insert(TABLE_FILTERS_VALUES,$array_values);
						}
					}
				}
				if (is_array($data_variations)) {
					$data_variations['nss_all_variations']=explode(",",$data_variations['nss_all_variations']);
					foreach ($data_variations['nss_all_variations'] as $key) {
						$variation=$data_variations[$key];
						$variation['id_product']=$id;
						$variation['variation']=str_replace("_","-",$key);
						$db->insert(TABLE_VARIATIONS,$variation);
					}
				}
				close_window($this->module,$id);
				print_alerta('a fost inserat/updatat');
			} else print_alerta('pe site mai exista un produs cu acelasi nume');
		} else {
			print_form_errors($errors,$form);
		}
	}
	function update_all($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_specification(this.value);">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_all_specification"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_provider($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_providers&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Furnizor'));
		?>
		<dl>
		<dt>
			<label for="update_furnizor"><?=l('Furnizor')?></label>
		</dt>
		<dd>
		<select name="provider" id="update_furnizor" style="width:300px" onchange="update_furnizor(this.value);">
			<?php $this->json_providers() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_furnizor"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_cadou($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_cadou&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Update cadou'));
		print_form($this->cadou,array(),array());
		?>
		<!--<dl>
		<dt>
		<label for="update_cadou"><?=l('Nume cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="nume_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Pret cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="pret_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Text cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="text_cadou" value="" align="absmiddle" />
		</dd>
		</dl>-->
		<?php
		print_form_footer();
		?>
		<!--<div id="update_cadou"></div>-->
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_multistatus($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_multistatus&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Statut'));
		?>
		<dl>
		<dt>
			<label for="update_multistatus"><?=l('Statut')?></label>
		</dt>
		<dd>
		<select name="statusx" id="update_multistatus" style="width:300px">
			<?php
			foreach ($this->orders_status as $key=>$status) {
				?>
				<option value="<?=$key?>"><?=$status?></option>
				<?php
			}
			?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_multistatus"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_all_filters($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all_filters&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_filter(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_filter"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function port_categories($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_port_categories&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_category(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_category"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function verify_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=return_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="verify_codes"><?=l('Insert codes with "," delimiter')?></label>
		</dt>
		<dd>
			<textarea name="verify_codes" id="return_codes"></textarea>
		</dd>
		</dl>
		<div id="return_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Check codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function export_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=brand_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Brand'));
		?>
		<dl>
			<dt>
				<label for="brand_codes"><?=l('Select brand to export codes')?></label>
			</dt>
			<dd>
				<select name="brand_codes">
				<?=$this->json_brands();?>
				</select>
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="select_status"><?=l('Select status of products')?></label>
			</dt>
			<dd>
				<select name="select_status">
					<option value="1"><?=l('Active')?></option>
					<option value="0"><?=l('Inactive')?></option>
					<option value="x"><?=l('Ignore')?></option>
				</select>
			</dd>
		</dl>
		<div id="brand_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Export codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function save_all(){
		global $db;
		$data_specifications=$_POST['specifications'];
		$ids=explode(",",r('ids'));
		foreach ($ids as $key=>$id) {
			$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id." AND `id_category`=".r('category'));
			if (empty($exist)) unset($ids[$key]);
		}
		close_window($this->module);
		if (is_array($data_specifications)) {
			//$db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_product` IN (".implode(",",$ids).")");
			foreach ($data_specifications as $key=>$value) {
				foreach ($ids as $id) {
					$array_values=array(
					'front'=>$_POST['specifications_show'][$key],
					);
					if (!empty($value)) $array_values['value']=$value;
					$db->update(TABLE_SPECIFICATIONS_VALUES,$array_values," `id_product`=".$id." AND `id_option`=".$key);
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function save_providers(){
		global $db;
		$data_provider=$_POST['provider'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('id_furnizor'=>$data_provider),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_cadou(){
		global $db;
		//print_a($_POST);
		//die();
		$ids=explode(",",r('ids'));
		$ceva=$_POST['nss_languages'][LANG];
		//print_a($ceva);
		//die();
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS.TABLE_EXTEND,$ceva,"id_main=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_multistatus(){
		global $db;
		$data_multistatus=$_POST['statusx'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('status'=>$data_multistatus),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_port_categories(){
		global $db;
		$data_category=$_POST['category'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) foreach ($ids as $key=>$id) $db->update(TABLE_PRODUCTS,array('id_category'=>$data_category),"`id`=$id");
		print_alerta('a fost updatat');
	}
	function return_codes(){
		global $db;
		$ids=explode(",",r('verify_codes'));
		if (!empty($ids)) foreach ($ids as $k=>$code) {
			$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='{$code}'");
			if ($exist) $exists[]=$code; else $inexist[]=$code;
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile inexistente in baza de date sunt: <br />'.implode('<br />',$inexist).'<br /></div><div>codurile existe sunt: <br />'.implode('<br />',$exists).'<br /></div>');
	}
	function brand_codes(){
		global $db;
		$id_brand=r('brand_codes');
		$status=r('select_status');
		if (!empty($id_brand)) {
			if ($status!='x') $cond=" AND `status`={$status}"; else $cond='';
			$products=$db->fetch_all("SELECT `code` FROM `".TABLE_PRODUCTS."` WHERE `id_brand`={$id_brand} {$cond} ORDER BY `id_category` ASC");
			foreach ($products as $p) {
				$codes.=", ".$p['code'];
			}
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile existente in baza de date sunt: </div><div>'.ehtml(substr($codes,1)).'</div>');
	}
	function save_all_filters(){
		global $db;
		$data_filters=$_POST['filters'];
		$ignore=$_POST['ignore'];
		$ids=explode(",",r('ids'));
		/*	foreach ($ids as $key=>$id) {
		if (!empty($id)) {
		$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id." AND `id_category`=".r('category'));
		if (empty($exist)) unset($ids[$key]);
		}
		}*/
		foreach ($ids as $id) $db->delete(TABLE_FILTERS_VALUES,"`id_product`=".$id.(!empty($ignore)?" AND `id_filter` NOT IN (".implode(",",(array)$ignore).")":""));
		close_window($this->module);
		if (is_array($data_filters)) {
			foreach ($data_filters as $key=>$value) {
				foreach ($ids as $id) {
					if (!in_array($key,(array)$ignore)) {
						$db->delete(TABLE_FILTERS_VALUES," `id_product`={$id} AND `id_filter`={$key}");
						if (!empty($value)) {
							$array_values=array(
							'id_option'=>$value,
							'id_product'=>$id,
							'id_filter'=>$key,
							);
							$db->insert(TABLE_FILTERS_VALUES,$array_values);
						}
					}
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`=".(int)$id);
	}
}
$module=new products_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module,true);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_providers']==1) $module->json_providers(fget('id'));
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_currencies']==1) $module->json_currencies(fget('id'));
elseif ($_GET['json_list_filter']==1) $module->json_list_filter(fget('id_category'),fget('id'));
elseif ($_GET['json_list_specifications']==1) $module->json_list_specifications(fget('id_category'),fget('id'));
elseif ($_GET['json_list_filters']==1) $module->json_list_filters(fget('id_category'),fget('ids'));
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
elseif ($_GET['list_variations_options']==1) $module->list_variations_options(fget('id_var'),fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='product_clon') $module->new_a_clone(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='update_all') $module->update_all(fget('ids'));
elseif ($_GET['action']=='update_provider') $module->update_provider(fget('ids'));
elseif ($_GET['action']=='update_cadou') $module->update_cadou(fget('ids'));
elseif ($_GET['action']=='update_multistatus') $module->update_multistatus(fget('ids'));
elseif ($_GET['action']=='update_all_filters') $module->update_all_filters(fget('ids'));
elseif ($_GET['action']=='port_categories') $module->port_categories(fget('ids'));
elseif ($_GET['action']=='verify_codes') $module->verify_codes(fget('ids'));
elseif ($_GET['action']=='export_codes') $module->export_codes(fget('ids'));
elseif ($_GET['action']=='save_all') $module->save_all();
elseif ($_GET['action']=='save_providers') $module->save_providers();
elseif ($_GET['action']=='save_cadou') $module->save_cadou();
elseif ($_GET['action']=='save_multistatus') $module->save_multistatus();
elseif ($_GET['action']=='save_all_filters') $module->save_all_filters();
elseif ($_GET['action']=='save_port_categories') $module->save_port_categories();
elseif ($_GET['action']=='return_codes') $module->return_codes();
elseif ($_GET['action']=='brand_codes') $module->brand_codes();
else {
	print_header();
	$admin_info=adm_info();
	if ($admin_info['username']=='admin') {
		print_content($module);
	} else {
		print_content($module,array(),'','',array('delete'));
	}
	//$module->new_a(fget('id'));
	print_footer();
}
?>