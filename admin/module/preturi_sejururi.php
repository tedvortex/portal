<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
//error_reporting(-1);




$b = BASEHREF.'admin/index.php';




print_header();
?>

<div id="availability-container">
	<h2>Disponibilitate camere</h2>
	<form id="change-dates" method="get" action="">
		<input type="hidden" name="mod" value="availability" />
		<input type="text" name="start" value="<?php echo $start ?>" />
		<input type="text" name="end" value="<?php echo $end ?>" />
		<button type="submit">Schimba interval</button>
	</form>

	<table id="select-month" cellpadding="0" cellspacing="0">
	<?php
	
	$cont = 1;
	for(
		$i = 1;
		$i <= 3;
		$i++
	){
	?>
		<tr>
			<td><?php echo $y?></td>
			<?php
			for(
				$j = 1;
				$j <= 12;
				$j++
			){
			?>
			<td>
				<?php

				if(
					(
						(
							date('m',$interval['first']) >= date('m',time()) &&
							date('Y',$interval['first']) == date('Y',time())
						) ||
						date('Y',$interval['first']) > date('Y',time())
					) &&
					$cont <= 24
				){
					echo '<a href="'.$b.'?mod=availability&start='.$interval['first_date'].'&end='.$interval['last_date'].'" >'.$months[$j].'</a>';
					$cont++;
				}
				else{
					echo '-';
				}
				?>
			</td>
			<?php
			}
			?>
		</tr>
	<?php
		$y++;
	}
	?>
	</table>

	<?php
	if(
		$table
	){
	?>
	<table id="change-quantity" cellpadding="0" cellspacing="0">
		<tr>
			<th class="day no-col">Zi</th>
			<th class="date no-col">Data</th>
			<?php
			foreach(
				$table['heading'] as
				$th
			){
				echo "<th>".$th."</th>";
			}
			?>
		</tr>
		<?php
		foreach(
			$table['rooms'] as
			$poz => $r
		){
		?>
		<tr class="<?php echo ($poz%2 ? 'even' : 'odd')?>">
			<td class="day"><?php echo $r['day']?></td>
			<td class="date"><?php echo $r['date_complete']?></td>
			<?php
			if(
				$r['list']
			){
				foreach(
					$r['list'] as
					$l
				){
				?>
			<td>
				<input class="availability" type="text" name="room-<?php echo $l['id']?>-date-<?php echo $r['id_date']?>" value = "<?php echo $l['quantity'] ?>" data-orders="<?php echo $l['q_orders']?>" data-date="<?php echo $r['id_date']?>" data-room="<?php echo $l['id']?>" />
				<input class="price" type="text" name="room-<?php echo $l['id']?>-price-<?php echo $r['id_date']?>" value = "<?php echo $l['price'] ?>" data-date="<?php echo $r['id_date']?>" data-room="<?php echo $l['id']?>" />
			</td>
				<?php
				}
			}
			?>
		</tr>
		<?php
		}
		?>
	</table>
	<?php
	}
	?>
</div>

<?php
print_footer();