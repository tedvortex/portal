<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class foto_module {
	var $module='foto';
	var $date='25-08-2009';
	var $table=TABLE_FOTO;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function foto_module() {
		$this->name=l('Galerie foto clienti');
		$this->title=l('Galerie foto clienti');
		$this->description=l('Galerie foto clienti');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status','id_category')
		);
		$this->folder='galerie-foto';
		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'image'=>array('name'=>l('Foto'),'width'=>200,'align'=>'center'),
		'name'=>array('name'=>l('Nume foto'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		//'content'=>array('name'=>l('Taguri foto'),'width'=>300),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>120,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume foto'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2,max_100','style'=>'width:70%;'),
		//'content'=>array('type'=>'text','name'=>l('Taguri foto'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2,max_100','style'=>'width:70%;'),
		//		'name'=>array('type'=>'input','name'=>l('Nume pagina'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2,max_100','style'=>'width:70%;','auto'=>'name_seo,header_title,title,meta_keywords,meta_description'),
		//		'title'=>array('type'=>'input','name'=>l('Title, Alt'),'lang'=>true,'valid'=>'empty,min_2,max_200','style'=>'width:70%;'),
		//		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:70%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		1=>'show_categories',
		//		'content'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		'image'=>array('type'=>'image','name'=>l('Imagine - 800px x 600px'),'folder'=>$this->folder)
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this,true);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=900;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() + 
		'&videoCategory=' + $('#video_categ').val() + 
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db,$gd;
		$row['id_category']=$db->fetch_one("SELECT `name` FROM `xp_categoriesf_data` WHERE `_id`=".$row['id_category']);
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Nicio categorie').'</span>';
		}
		$row['image']='<img src="../static/i/'.$this->folder.'/80/'.$row['image'].'" width="80" />';
		if ($row['type']==0) {
			$row['type']='<img src="images/tick.gif"  width="16" height="16" alt="tick"  />';
		} else {
			$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
			$row['actions']='<a class="action edit" id="id_line_'.$row['id'].'">'.l('edit').'</a>';
		}
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIESF."` ");
		$categories=lang_fetch_all(TABLE_CATEGORIESF," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}

		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=lang_fetch($this->table,"`id`=".$id);
		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
		} else $date_admin['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica foto'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
		}
		function save($id){
			global $db,$gd;
			$admin_info=adm_info();
			$languages=get_languages();
			$data=$_POST;
			$errors=form_validation($data,$this->form,$this->table);
			if(empty($errors)) {
				$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages'][LANG]['name'].' '.time(),"-"),'static/i/'.$this->folder,'800x600'));
				close_window($this->module);
				if (!empty($id)) {
					//unset($data['sursa']);
					lang_update($this->table, $data," `id`=".$id);
					print_alerta('a fost updatat');
				} else {
					$data['date']=time();
					lang_insert($this->table, $data);
					print_alerta('a fost inserat');
				}
			} else {
				print_form_errors($errors,$this->form);
			}
		}

	}
	$module=new foto_module();
	if ($module_info) $this_module=$module;
	elseif ($module_js) $module->js();
	elseif ($module_css) $module->css();
	elseif ($_GET['json_list']==1) json_list($module,true);
	elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
	elseif ($_GET['grid_edit']==1) $module->grid_edit();
	elseif ($_GET['action']=='new') $module->new_a(fget('id'));
	elseif ($_GET['action']=='save') $module->save(fget('id'));
	else {
		print_header();
		print_content($module);
		print_footer();
	}
?>