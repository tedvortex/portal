<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");

//$CompaniiAerieneLinie = array("Adria Airways", "Aegean", "Aeroflot", "AeroGal", "Aerolineas Argentinas", "Aeromexico", "Aeropelican", "Aerosvit", "Aer Lingus", "Afriqiyah Airways", "Airlinair", "Air Algerie", "Air Alps", "Air Astana", "Air Austral", "Air Berlin", "Air Botswana", "Air Canada", "Air China", "Air Creebec", "Air Dolomiti", "Air Europa", "Air France", "Air Greenland", "Air India", "Air Italy", "Air Ivoire", "Air Jamaica", "Air Macau", "Air Madagascar", "Air Malawi", "Air Malta", "Air Maroc", "Air Mauritius", "Air Moldova", "Air Namibia", "Air New Zealand", "Air Nigeria", "Air One", "Air Seychelles", "Air Sinai", "Air Sunshine", "Air Tahiti", "Air Transat", "Air Vanuatu", "Air Zimbabwe", "Alaska Airlines", "Albanian Airlines", "Alitalia", "All Nippon Airways", "Alpine Air Express", "American Airlines", "Arik Air", "Arkia", "Armavia", "Asiana", "Atlantic Airways", "Atlasjet", "Aurigny", "Austrian", "Aviacsa", "Avianca", "Azerbaijan Airlines", "Bahamasair", "Belavia", "Bemidji Airlines", "Bering Air", "Berjaya Air", "Binter Canarias", "Blue1", "BMI", "British Airways", "Brussels Airlines", "Bulgaria Air", "Canadian North", "Cape Air", "Carpatair", "Caspian Airlines", "Cathay Pacific", "Cayman Airways", "Central Mountain Air", "China Airlines", "China Eastern", "China Southern", "Condor", "Continental Airlines", "Copa", "Corsair", "Croatia Airlines", "Cyprus Airways", "Czech Airlines", "Danish Air", "Delta Airlines", "Direktflyg", "Dnieproavia", "Donbassaero", "Dragonair", "Druk Air", "Egyptair", "El Al", "Emirates", "Eritrean Airlines", "Estonian Air", "Ethiopian Airlines", "Etihad Airways", "ExpressJet Airlines", "Finnair", "Flybaboo Airline", "Georgian Airways", "GMG Airlines", "Gulf Air", "Hahn Air", "Hainan Airlines", "Hawaiian Airlines", "Iberia", "Icelandair", "InterSky", "Iran Air", "JAL", "Jat Airways", "Jet Airways", "Kal Star", "Kavminvodyavia", "Kenya Airways", "Kingfisher Airlines", "KLM", "Korean Air", "Kuwait Airways", "Lacsa", "LAM", "LAN Airlines", "LAN Peru", "Lauda Air", "LIAT", "Línea Aérea Amaszonas", "LOT", "Lufthansa", "Luxair", "Malaysia Airlines", "Malev", "Mesa Airlines", "MIAT", "Middle East Airlines", "Moldavian Airlines", "Montenegro Airlines", "Myanmar Airways", "Northwest Airlines", "North American Airlines", "Nouvelair", "Olympic Air", "Oman Air", "Philippine Airlines", "Precision Air", "Qantas", "Qatar Airways", "Rossiya Airlines", "S7 Airlines", "SAS", "Saudi Arabian Airlines", "Shanghai Airlines", "Sichuan Airlines", "Singapore Airlines", "Southwest Airlines", "South African Airways", "Spanair", "SriLankan Airlines", "Suckling Airways", "Swiss", "TACA", "TAP", "Tarom", "Thai Airways", "Tianjin Airlines", "Transaero", "Turkish Airlines", "UM Airlines", "United Airlines", "US Airways", "Varig Brasil", "Vietnam Airlines", "Virgin America", "Virgin Atlantic", "Virgin Australia", "VLM Airlines", "Westjet", "Wideroe", "Wind Jet", "World Airways", "Yemenia");
//
//$CompaniiAerieneLowCost = array("Aer Arann", "AirAsia", "AirTran Airways", "Air Baltic", "Antilles Express", "Blu-Express", "Blue Air", "bmibaby", "CanJet", "Cimber Sterling", "EasyFly", "easyJet", "Flybe", "Flydubai", "Germania", "Germanwings", "GOL", "Helvetic", "Hewa Bora Airways", "Iceland Express", "IndiGo Airlines", "Israir Airlines", "Jet2.com", "Jetairfly", "JetBlue", "Jetstar Airways", "Lithuanian Airlines", "Meridiana", "Monarch Airlines", "Nas Air", "NextJet", "Niki", "Norwegian", "Ryanair", "Smart Wings", "Spirit Airlines", "Sun Country Airlines", "Sun Express", "Thomas Cook Airlines", "Tiger Airways", "Transavia", "TUIfly", "Vueling", "Wizzair");
//
//// Aici in baza de date ar mai trebui un filed numit "CodIata" .Toate folosesc doar doua caractere 
//// De ex. Pentru Adria Airways codul Iata este "JP"
//
//$CompaniiAerieneLinie2 = array(
//					array("Adria Airways", "JP", "Linie"), 
//					array("Aegean", "A3", "Linie"), 
//					array("Aeroflot","SU", "Linie")
//					);
//
////$customers=$db->fetch_all("select * from `xp_orders`");
//foreach ($CompaniiAerieneLowCost as $c){
//	//print_a($c);
//	  	$db->insert('airlines_codes',array('name'=>$c,'type'=>1));
//	  
//
//}
//die();
//

$this_module=array();

class config_module {
	var $module='config';
	var $date='14-03-2010';
	var $table='config';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function config_module() {
		global $db;
		$this->name=l('Config');
		$this->title=l('Config');
		$this->description=l('Config');

		$this->type=array(
			'like'=>array('name','id','key','value'),
			'equal'=>array('status','id')
		);

		$this->grid=array(
			'_nr'=>true,
			'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
			'key'=>array('name'=>l('Cheie'),'width'=>300),
			'value'=>array('name'=>l('Valoare'),'width'=>300),
			'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
			'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
			'key'=>array('name'=>l('Cheie'),'width'=>300),
			'value'=>array('type'=>'input','name'=>l('cuvant'),'style'=>'width:90%;'),
			'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
			'type'=>array('type'=>'radio','name'=>l('tip cheie'),'options'=>array(0=>l('config'),1=>l('define'),2=>l('tabela'))),
		);
	}
	function css() {}
	function json_list() {
		$new_sql="select 
					*
					from
					`{$this->table}`
					where `show`=1";
		json_list($this,false,$new_sql);
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function json_list_row($row) {
		if ($row['default']==1) {
			$row['default']='<img class="ch_default_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;" />';
		} else $row['default']='<img class="ch_default_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Editeaza cheie'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['show']=1;
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}

$module=new config_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>