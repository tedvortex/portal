<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
//$customers=$db->fetch_all("select * from `xp_articles_data` ");
//foreach ($customers as $c){
//	  	$c['description']=str_replace('thftryuuiryrre','',$c['description']);
////	  	$c['image_thumb']=str_replace('imagini-producatori/','',$c['image_thumb']);
////	  	$c['image_thumb2']=str_replace('imagini-producatori/','',$c['image_thumb2']);
//	  	$db->update('xp_articles_data',array('description'=>$c['description']),"`id`=".$c['id']);
//		
//}
//die();
class news_module {
	var $module='news';
	var $date='25-08-2009';
	var $table='xp_articles';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function news_module() {

		$this->name=l('Articole');
		$this->title=l('Articole');
		$this->description=l('Articole');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);
		$this->folder='imagini-articole';
		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'name'=>array('name'=>l('Titlu noutate'),'width'=>200),
		//'title'=>array('name'=>l('Title'),'width'=>300),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume stire'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2','style'=>'width:82%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		//'link'=>array('type'=>'input','name'=>l('Link'),'text'=>'','lang'=>true,'style'=>'width:82%;'),
		//'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:82%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//'email_news'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('Trimite email la abonati')),
		'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'style'=>'width:75%;height:250px;','lang'=>true,'valid'=>'empty'),
		'description'=>array('type'=>'editor','name'=>l('description'),'lang'=>true),
		1=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Imagine '),'text'=>'Dimensiune 640 x 480 sau raport 4/3',$this->folder),
		//'image2'=>array('type'=>'image','name'=>l('Imagine'),'text'=>'Dimensiune 294x130',$this->folder),
		//'image3'=>array('type'=>'image','name'=>l('Imagine'),'text'=>'Dimensiune 640x480',$this->folder),
		);
	}
	function css() {}
	function json_list() {
		json_list($this,true);
	}
	function json_list_row($row) {
		global $gd;
		if (!empty($row['image']))
		$row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=lang_fetch($this->table,"`id`=".$id);
		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
		} else $date_admin['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Date noutate'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		unset($data['email_news']);
		foreach ($data['nss_languages'] as $lang=>$vars) {

			$data['nss_languages'][$lang]['name_seo']=escapeIlegalChars($data['nss_languages'][$lang]['name']," ");
		}
		$errors=form_validation($data,$this->form,$this->table);
		
		if(empty($errors)) {
			close_window($this->module);
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo'],'-'),'static/i/'.$this->folder,''));

			//$data['image']=upload_images($data['image'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo'],"-"),$this->folder,'81x53');
			//$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			//$data['image3']=upload_images($data['image3'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 3","-"),$this->folder,'640x480');
			if (!empty($id)) {
				lang_update($this->table, $data," `id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new news_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortorder'=>'desc'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list($module,true);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>