<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class emails_module {
	var	$module='emails';
	var $date='27-08-2009';
	var $table=TABLE_EMAILS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function emails_module() {

		$this->name=l('emails');
		$this->title=l('emails');
		$this->description=l('emails');

		$this->type=array(
		'like'=>array('id','template','sender','subject'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'template'=>array('name'=>l('template'),'width'=>120),
		'sender'=>array('name'=>l('sender'),'width'=>200),
		'sender_name'=>array('name'=>l('sender_name'),'width'=>200),
		'subject'=>array('name'=>l('subject'),'width'=>240),
		'date'=>array('name'=>l('Data modificare'),'width'=>100,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>100,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'sender'=>array('type'=>'input','name'=>l('sender'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_6,max_100','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo,title'),
		'sender_name'=>array('type'=>'input','name'=>l('sender_name'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_6,max_100','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo,title'),
		'subject'=>array('type'=>'input','name'=>l('subject'),'lang'=>true,'valid'=>'empty,min_6,max_200','style'=>'width:92%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'content'=>array('type'=>'editor','name'=>l('description'),'lang'=>true,'style'=>'width:92%;'),
		);

	}
	function css() {}
	function json_list(){
		json_list($this,true);
	}
	function json_list_row($row) {
		//$row ['actions'] = '<a class="action edit" id="id_line_' . $row ['id'] . '">' . l ( 'edit' ) . '</a>';
		$row ['actions'] = '<a class="action edit" onclick="do_edit(\''.$row['id'].'\',\'emails\');">' . l ( 'edit' ) . '</a>';
		return $row;
	}
	function print_records() {
		print_content($this,array(),'','',array('new','delete'));
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=lang_fetch($this->table,"`id`=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Info vars'));
		echo $date_saved['vars_use'];
		print_form_footer();
		print_form_header(l('Email text'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		//$languages=get_languages();
		$data=$_POST;
		foreach ($data['nss_languages'] as $lang=>$vars) {
			if (isset($data['nss_languages'][$lang]['content'])){
				$data['nss_languages'][$lang]['content']=str_replace('../','http://www.clickandgo.ro/',$data['nss_languages'][$lang]['content']);
			}
		}
		$errors=form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
			$data['date']=time();
			lang_update($this->table, $data," `id`=".$id);
			print_alerta('a fost updatat');
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new emails_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>