<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class furnizori_module {
	var	$module='furnizori';
	var $date='27-08-2009';
	var $table=TABLE_FURNIZORI;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function furnizori_module() {

		$this->name=l('Furnizori');
		$this->title=l('Furnizori');
		$this->description=l('Furnizori');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'name'=>array('name'=>l('Name'),'width'=>200),
		'email'=>array('name'=>l('Email'),'width'=>120),
		'phone'=>array('name'=>l('Telefon'),'width'=>80),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Date personale'),
		2=>l('Modifica parola')
		//4=>l('Adauga editeaza adrese'),
		//5=>l('Adauga editeaza firme')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','name'=>l('Compania'),'valid'=>'empty,unique,min_2,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'phone'=>array('type'=>'input','name'=>l('Telefon'),'valid'=>'empty,min_4,max_100'),
		//'base_city'=>array('type'=>'input','name'=>l('city'),'valid'=>'empty,min_4,max_100'),
		'city'=>array('type'=>'input','name'=>l('Tara'),'valid'=>'empty,min_2,max_100'),
		'state'=>array('type'=>'input','name'=>l('Oras'),'valid'=>'empty,min_2,max_100'),
		'address'=>array('type'=>'text','name'=>l('Adresa'),'style'=>'width:70%','valid'=>'empty,min_4,max_100'),
		'email'=>array('type'=>'input','name'=>l('email'),'style'=>'width:70%','valid'=>'empty,unique,email'),
		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		10=>array('tab'=>2),
		'password'=>array('type'=>'password','name'=>l('password'),'valid'=>'empty,min_6,max_14'),
		'password_2'=>array('type'=>'password','name'=>l('password ver'),'valid'=>'empty,min_6,max_14,same_password'),
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'furnizori_images'),
		);
	}
	function json_list() {
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd;
		if (!empty($row['image']))
		$row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			
			unset($date_saved['password']);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['password_2']);
			if (empty($data['password'])) {
				unset($data['password']);
			} else {
				$data['password']=md7($data['password']);
			}
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new furnizori_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortname'=>'name','sortorder' => 'asc'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	//$module->new_a();
	print_footer();
}
?>