<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class specifications_module {
	var $module='specifications';
	var $date='3-10-2009';
	var $table=TABLE_SPECIFICATIONS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function specifications_module() {

		$this->name=l('Specificatii produse');
		$this->title=l('Specificatii produse');
		$this->description=l('specifications');

		$this->type=array(
		'like'=>array('name'),
		'equal'=>array('id_category','status','id')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Categorie specificatii'),'width'=>300),
		'id_category'=>array('name'=>l('Categorii produse'),'width'=>180,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume specificatie'),'valid'=>'empty','style'=>'width:92%;'),
		'order'=>array('type'=>'input','name'=>l('Ordinea'),'valid'=>'empty','style'=>'width:12%;'),
		1=>'show_categories',
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		2=>l('Specificatii')
		);
	}
	function json_list() {
		json_list($this,false);
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		.drag_all_top span {
			cursor:move;
			font-size:14px;
			background-image:url('../../images/grid_move.gif');
		}
		.drag_all_top div.opt_value{
			padding:2px;
			padding-left:4px;

		}
		<?php
	}
	function json_list_row($row) {
		global $db;
		
		if ($row['id_category']==0) {
			$row['id_category']='<span style="color:red;">'.l('Toate categoriile').'</span>';
		} else {
			$row['id_category']=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."_data` WHERE `_id`=".$row['id_category']." ");
		}
		return $row;
	}
	function js() {
		?>
		var window_add_edit_name_category="<?=l('Add Category')?>";
		function add_category(){
			var module_now='categories';
			nss_win(module_now + '_new', window_add_edit_name_category,
			'module/' + module_now + '.php?action=new', 800,
			function() {
				$("#window_" + module_now + "_new form").bind('submit',
				function() {
					return false;
				});
				after_window_load(module_now, 'new',load_categories);
			});
		}
		function load_categories(){
			$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');

		}
		<?php
		$loadComplete='
			$("#gs_id_category").load("module/'.$this->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
		';
		set_grid($this,array('multiselect'=>true,'sortorder' => 'asc','sortname' => 'order','loadComplete'=>$loadComplete));
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<option value="0">Toate categoriile</option>
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fost('id'));
		foreach ($ids as $id) if(!empty($id)) {
			$ids_options=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$id);
			foreach ($ids_options as $id_option) $db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_option`=".$id);
			$db->delete(TABLE_SPECIFICATIONS_OPTIONS,"`id_specification`=".$id);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		?>
		<div class="drag_all_top">
		<?php
		if (!empty($id)) $specifications_options=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE `id_specification`=".$id." ORDER BY `order` ASC");
		$specifications_options=array_merge((array)$specifications_options,array(1=>array('id'=>'new')));
		foreach ($specifications_options as $linie) {
			?>
			<div class="opt_value"><span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<input type="hidden" name="order_2[]" value="<?=$linie['id']?>">
				<input type="text" name="value[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:60%;" value="<?=$linie['name']?>" align="absmiddle"  />
				<?php if($linie['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="add" />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="rem"  />
			</div>
			<?php
		}
		?>
		</div>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">
			$(".drag_all_top").sortable({opacity: 0.7,revert: true,scroll: true ,handle:$(".drag_all_top span")}) ;
		</script>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			$array_insert=array('name'=>fpost('name'),'id_category'=>fpost('id_category'),'order'=>fpost('order'));
			if (!empty($id)) {
				$db->qupdate($this->table,$array_insert," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$array_insert['status']=1;
				$db->insert($this->table,$array_insert);
				$id=$db->insert_id();
				print_alerta('a fost inserat');
			}
			if (is_array($_POST['value'])) {
				$all_ids=array();
				//print_a($_POST['order_2']);
				foreach ($_POST['order_2'] as $order=>$key) {
					$value=$_POST['value'][$key];
					if (is_numeric($key) && $key!='new') {
						$all_ids[]=$key;
						if (!empty($value)) {
							$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_SPECIFICATIONS_OPTIONS."`  WHERE `name`='".trim($value)."' AND `id_specification`=".$id." AND `id`<>".$key);
							if (empty($exist)) {
								$array_insert=array('name'=>trim($value));
								$array_insert['id_specification']=$id;
								$array_insert['order']=$order;
								//echo $value."-";
								$db->update(TABLE_SPECIFICATIONS_OPTIONS,$array_insert," `id`=".$key);
							}
						}
					} else {
						foreach ($value as $value_ins) {
							if (!empty($value_ins)) {
								$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_SPECIFICATIONS_OPTIONS."`  WHERE `name`='".trim($value_ins)."' AND `id_specification`=".$id);
								if (empty($exist)) {
									$array_insert=array('name'=>trim($value_ins));
									$array_insert['id_specification']=$id;
									$array_insert['order']=$order++;
									$db->insert(TABLE_SPECIFICATIONS_OPTIONS,$array_insert);
									$all_ids[]=$db->insert_id();
								}
							}
						}
					}
				}
				if (empty($all_ids)) $all_ids=array(0);
				$options=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE  `id` NOT IN (".implode(",",$all_ids).") AND `id_specification`=".$id."");
				foreach ($options as $option) $db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_option`=".$option['id']);
				$db->delete(TABLE_SPECIFICATIONS_OPTIONS,"   `id` NOT IN (".implode(",",$all_ids).") AND `id_specification`=".$id."");
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new specifications_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>