<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class onlineusers_module {
	var $module='onlineusers';
	var $date='31-10-2009';
	var $table=TABLE_ONLINEUSERS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function onlineusers_module() {
		
		$this->name=l('Onlineusers');
		$this->title=l('Onlineusers');
		$this->description=l('Onlineusers');
		
		$this->type=array(
		'like'=>array('browser','id','ip'),
		'date'=>array('time'),
		'equal'=>array('status','id')
		);
		
		$this->grid=array(
		'_nr'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'id_user'=>array('name'=>l('ID User'),'width'=>80,'align'=>'center'),
		'ip'=>array('name'=>l('IP'),'width'=>100,'align'=>'center'),
		'time'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'browser'=>array('name'=>l('Browser'),'width'=>200,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
	}
	function css() {}
	function json_list(){
		json_list($this);
	}
	function print_records() {
		print_content($this,array(),'','',array('new','edit'));
	}
	function json_list_row($row) {
		global $db;
		$row ['actions'] = '<a class="action delete" id="id_line_' . $row ['id'] . '">' . l ( 'Sterge' ) . '</a>';
		$row['id_user']=$db->fetch("SELECT `username`,`first_name`,`last_name` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_user']);
		if (empty($row['id_user'])) $row['id_user']='<span style="color:red;">'.l("Utilizator nelogat").'</span>';
		else $row['id_user']=$row['id_user']['username']." (".$row['id_user']['first_name']." ".$row['id_user']['last_name'].")";
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
}
$module=new onlineusers_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortorder'=>'desc','sortname'=>'id_user'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>