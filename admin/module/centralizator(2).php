<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class centralizator_module {
	var $module='centralizator';
	var $date='27-08-2009';
	var $table='xp_summar';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $dimensiuni=array();
	function centralizator_module() {
		global $config;
		$this->name=l('Centralizator oferte - zona 4');
		$this->title=l('Centralizator oferte - zona 4');
		$this->description=l('Centralizator oferte - zona 4');

		$this->type=array(
		'like'=>array('link','name'),
		'date'=>array('date'),
		);
		$this->folder='promoted';
		$this->grid=array(
		0=>array('order'=>'id`,`order asc'),
		'_nr'=>true,
		'_cb'=>true,
		'image'=>array('name'=>l('Banner'),'width'=>240,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>200,'align'=>'center'),
		'price'=>array('name'=>l('Pret'),'width'=>60,'align'=>'right'),
		'link'=>array('name'=>l('Link'),'width'=>200),
		'order'=>array('name'=>l('Pozitia'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('Status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),"style"=>'width:98%;'),
		'link'=>array('type'=>'input','name'=>l('Link'),"style"=>'width:98%;'),
		'price'=>array('type'=>'input','name'=>l('Pret de la '),"style"=>'width:98%;'),
		'price'=>array('type'=>'input','name'=>l('Pret de la '),"style"=>'width:98%;'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),"style"=>'width:98%;'),
		//'description'=>array('type'=>'text','name'=>l('Descriere banner'),'lang'=>true,"style"=>'width:98%;height:100px;'),
		'color'=>array('type'=>'radio','name'=>l('Culoare'),'options'=>array('violet'=>l('violet'),'lightgreen'=>l('lightgreen'),'orange'=>l('orange'),'blue'=>l('blue'))),
		//'type'=>array('type'=>'radio','name'=>l('Tip'),'options'=>array(0=>l('830x168'),1=>l('410x168'),2=>l('200x268'),3=>l('200x148'))),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),

		2=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Imagine200x148'),'folder'=>$this->folder),
		//'image_small'=>array('type'=>'image','name'=>l('Imagine mica 290x80'),'folder'=>$this->folder)

		);
	}
	function css() {}
	function json_list() {
		$new_sql="select sql_calc_found_rows
					*

				from
					`{$this->table}` 
				
				 where `id_parent`>0";
		json_list($this,false,$new_sql);
	}
	function json_list_row($row) {
		global $gd;
		$row['image']='<img src="'._static.'i/promoted/'.$row['image'].'" alt="'.$row['image'].'" width="200" />';
		//$row['image_inde']='<img src="'._static.'i/slide/'.$row['image'].'" alt="'.$row['image'].'" width="80%" />';

		return $row;
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		if ($date_saved['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/i/'.$this->folder.'/'.$date_saved['image'];
		} else $date_saved['image']='';
	
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Add/Edit Banner'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		unset($data['type']);
		//$data['lang']=LANG;
		//unset($data['image_1']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name'],'-'),'static/i/'.$this->folder,''));
			//$data['image_small']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image_small'],escapeIlegalChars($data['nss_languages']['ro']['name'].' '.'small','-'),'static/i/'.$this->folder,''));
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['type']=3;
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
			die();
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new centralizator_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1)  $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>