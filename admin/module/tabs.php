<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class tabs_module {
	var $module='tabs';
	var $date='15-09-2009';
	var $table='xp_tabs';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function tabs_module() {

		$this->name=l('Taburi oferte');
		$this->title=l('Taburi oferte');
		$this->description=l('Taburi oferte');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','position','order','id_category')
		);

		$this->grid=array(
		0=>array('order'=>'position`,`order asc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie'),'width'=>80,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		'position'=>array('name'=>l('Tip header'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';0:'.l('Header').';1:'.l('Footer'))),
		'order'=>array('name'=>l('Ordinea'),'width'=>40,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>140,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty','style'=>'width:80%;'),
		4=>'show_categories',
		'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:80%;'),
		'position'=>array('type'=>'radio','options'=>array(0=>l('Header').' &nbsp; ',1=>l('Footer')),'name'=>l('Tip taburi')),
		

		);


	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." and `lang`='".LANG."' ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function json_list() {
		json_list($this,false);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		.module_menu li.back_zones {
			background-image:url('../../images/icons/shipping_methods_back.png');
		}
		.all_div_tax {
			display:none;
		}
		<?php
	}
	function print_records() {
		if (!empty($_GET['id_zone'])) {
		?>
		<script type="text/javascript">
		add_all_url['<?=$this->module?>']="&id_zone=<?=fget('id_zone')?>";
		</script>
		<?php
		}
		$new_icons=array(
		'nss_position'=>'left',
		'back_zones'=>l('Shipping zones'),
		);
		print_content($this,$new_icons);
	}
	function json_list_row($row) {
		global $db;
		$row['id_category']=$db->fetch_one("select `name` from `xp_categories_data` WHERE `_id`={$row['id_category']} ");
		if ($row['position']==0) {
			$row['position']='Header';
		} else $row['position']='Footer';
		return $row;
	}
	function js() {
		$before='$(".back_zones").click(function(){set_location(\'?mod=shipping_zones\')});';
		set_grid($this,array('multiselect'=>true),$before);
	}
	function json_list_2($id_zone){
		global $db;

		$new_sql= "SELECT * FROM `" . $this->table . "` WHERE `id_zone`=".(int)$id_zone." ";
		json_list($this,false,$new_sql);
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function show_shipping_motods_type($date){
	?>
		<dl>
			<dt>
				<label for="shipping_zone_type"><?=l('Metoda de livrare')?></label>
			</dt>
			<dd>
				<select name="type" onchange="$('.all_div_tax').hide(); $('.div_tax_'+this.value).show();">
					<option value="0"><?=l('Select')?></option>
					<?php foreach ($this->method_type as $key=>$name) { ?>					
						<option value="<?=$key?>" <?=(($date['type']==$key)?'selected':'')?>><?=$name?></option>
					<?php } ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new tabs_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>