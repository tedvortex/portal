<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
//include_once(dirname(dirname(dirname(__FILE__)))."/includes/functions.php");
class orders_module {
	var $module='orders';
	var $date='27-08-2009';
	var $table=TABLE_ORDERS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $orders_status=array();
	var $name='Comenzi';
	var $title='Comenzi';
	var $description='Comenzi';
	function orders_module() {

		$this->type=array(
		'like'=>array('id','id_customer','no_order','contact_person','contact_person_email',
		'buyer_cnp','buyer_address','buyer_city','buyer_phone','buyer_last_name','buyer_first_name',
		'payment_method','shipping_method','order_amount'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->orders_status=array(
		0=>l('In asteptare'),
		1=>l('In procesare'),
		//2=>l('Respinsa'),
		3=>l('Expediata'),
		4=>l('Completa'),
		5=>l('Rambursata'),
		6=>l('Anulata'),
		);


		$status_comanda=array(0=>'_:'.l('All'));
		foreach ($this->orders_status as $key=>$status) $status_comanda[]=$key.":".$status;
		$edit_options=array('value'=>implode(";",$status_comanda));

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'show_info'=>array('name'=>l('Info'),'width'=>40,'align'=>'center'),
		'no_order'=>array('name'=>l('Nr. comanda'),'width'=>40,'align'=>'center'),
		//'client'=>array('name'=>l('Nume'),'width'=>158,'align'=>'left'),
		'buyer_last_name'=>array('name'=>l('Last Name'),'width'=>80),
		'buyer_first_name'=>array('name'=>l('First Name'),'width'=>80),
		//'company'=>array('name'=>l('Compania'),'width'=>100),
		'buyer_email'=>array('name'=>l('Email'),'width'=>150),
		'buyer_city'=>array('name'=>l('Oras'),'width'=>60),
		'buyer_phone'=>array('name'=>l('Telefon Client'),'width'=>100),
		//'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'left','stype'=>'select','editoptions'=>$edit_options),
		'date'=>array('name'=>l('Date'),'width'=>70,'align'=>'center'),
		'shipping_method'=>array('name'=>l('Metoda livrare'),'width'=>100),
		'amount'=>array('name'=>l('Total'),'width'=>60,'align'=>'right'),
		'actions'=>array('name'=>l('Actions'),'width'=>250,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Cumparator'),
		2=>l('Firma'),
		3=>l('Detalii livrare'),
		4=>l('Alte detalii'),
		5=>l('Produse comandate'),
		),
		0=>array('tab'=>1),
		'buyer_first_name'=>array('type'=>'input','name'=>l('Prenume')),
		'buyer_last_name'=>array('type'=>'input','name'=>l('Nume')),
		'buyer_email'=>array('type'=>'input','name'=>l('Email')),
		'buyer_cnp'=>array('type'=>'input','name'=>l('CNP ')),
		'buyer_ci_serie'=>array('type'=>'input','name'=>l('Serie C.I.')),
		'buyer_address'=>array('type'=>'input','name'=>l('Adresa')),
		'buyer_city'=>array('type'=>'input','name'=>l('Oras')),
		'buyer_state'=>array('type'=>'input','name'=>l('Judet')),
		'buyer_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		'buyer_phone'=>array('type'=>'input','name'=>l('Telefon')),
		2=>array('tab'=>2),
		'company'=>array('type'=>'input','name'=>l('Firma')),
		'company_cui'=>array('type'=>'input','name'=>l('CUI')),
		'company_nr_reg_com'=>array('type'=>'input','name'=>l('Nr. Reg. Com.')),
		'company_bank'=>array('type'=>'input','name'=>l('Banca')),
		'company_bank_account'=>array('type'=>'input','name'=>l('Contul bancar')),
		'company_address'=>array('type'=>'input','name'=>l('Adresa')),
		'company_city'=>array('type'=>'input','name'=>l('Oras')),
		'company_state'=>array('type'=>'input','name'=>l('Judet')),
		'company_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		'company_phone'=>array('type'=>'input','name'=>l('Telefon')),
		3=>array('tab'=>3),
		'shipping_address'=>array('type'=>'input','name'=>l('Adresa livrare')),
		'shipping_city'=>array('type'=>'input','name'=>l('Oras livrare')),
		'shipping_state'=>array('type'=>'input','name'=>l('Judet livrare')),
		'shipping_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		4=>array('tab'=>4),
		'shipping_method'=>array('type'=>'input','name'=>l('Metoda livrare')),
		'shipping_tax'=>array('type'=>'input','name'=>l('Taxe livrare')),
		'handling_fee'=>array('type'=>'input','name'=>l('Taxa ramburs')),
		'comment'=>array('type'=>'text','name'=>l('Comentarii')),
		'ip'=>array('type'=>'input','name'=>l('IP')),
		5=>array('tab'=>5),
		6=>'show_order_products',
		7=>l('Adauga produs nou'),
		8=>'add_new_product',
		//9=>l('Update statut'),
		//'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>$this->orders_status[1],2=>$this->orders_status[2],5=>$this->orders_status[5])),
		//'status_mail_textarea',
		//'update_notification'=>array('type'=>'radio','name'=>l('Expediaza mail notificare update statut'),'options'=>array(1=>l('Da'),0=>l('Nu')))
		);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this);
	}
	function add_new_product($date) {
		global $db;

		?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categoria')?></label>
			</dt>
				<select name="id_category" id="id_category" style="width:460px;" onchange="do_product_filter();">
					<option value="0"><?=l('Toate')?></option>
					<?php $this->json_categories() ?>
				</select>
			</dd>
		</dl>
		<dl >
			<dt>
				<label for="products"><?=l('Numele produsului sau codul')?></label>
			</dt>
			<dd><input type="text" name="product_name" id="product_name" style="width:460px;" onkeyup="do_product_filter();" />
			</dd>
		</dl>
		<dl >
			<dt>
				<label for="products"><?=l('Produs')?></label>
			</dt>
			<dd>
				 <div id="list_products_dd" >
					<?php $this->json_products(); ?>
				</div>

			</dd>
		</dl>
		<?php
	}
	function json_categories($ids=array(),$id_parent=0,$deep=0) {
		global $db;
		$categories=lang_fetch_all(TABLE_CATEGORIES," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		foreach ($categories as $linie) {
			?>
			<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp;  &nbsp; ";
			?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($ids,$linie['id'],$deep+1);
		}
	}
	function json_product_filter() {
		$this->json_products(array(),$_POST['id_category'],$_POST['product_name']);
	}
	function json_products($ids=array(),$related_id_category=0,$related_product_name="") {
		global $db;
		$sql_filters=array();
		if (!empty($related_id_category)) $sql_filters[]=" `".TABLE_PRODUCTS."`.`id_category`=".$related_id_category;
		if (!empty($related_product_name)) $sql_filters[]=" `".TABLE_PRODUCTS.TABLE_EXTEND."`.`name` LIKE '%".$related_product_name."%' ";
		$sql="SELECT * FROM `".TABLE_PRODUCTS."`,`".TABLE_PRODUCTS.TABLE_EXTEND."`
			WHERE `".TABLE_PRODUCTS."`.`id`=`".TABLE_PRODUCTS.TABLE_EXTEND."`.`id_main`
			AND `".TABLE_PRODUCTS.TABLE_EXTEND."`.`lang`='".LANG."' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC";
		//echo $sql;
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) { $i++;
		if ($i>100) {
					?>
					<div> <?=l('And more ...')?> </div>
					<?php
					break;
		}
		?>
		<div style="white-space:nowrap;padding:1px;"> <img src="images/addicon.gif" onclick="add_order_product('<?=$linie['id_main']?>','<?=$linie['code']?>','<?=$linie['name']?>','<?=$linie['price']?>');" align="absmiddle"  class="pointer" /> <?=$linie['name']?> </div>
		<?php
		}
	}
	function json_list_row_before($row) {
		$row['status_comanda']=$row['status'];
		return $row;
	}
	function json_list_row($row) {
		$row['show_info']='<img src="images/list-add.png" class="pointer"  width="16" height="16" alt="Info" onclick="insert_grid_line(this,'.$row['id'].',load_order_info);" />';
		switch ($row['status_comanda']) {
			case 0:$col='#FD8014';break;
			case 1:$col='#000';break;
			case 3:$col='#149531';break;
			case 4:$col='#c00';break;
			case 5:$col='#f57cc9';break;
			default:$col='';break;
		}
		$row['status']='<b style="color:'.$col.'">'.$this->orders_status[$row['status_comanda']].'</b>';
		$row['amount']='<b>'.number_format($row['amount'],2,'.','').'</b>';
		$row['client']=$row['buyer_first_name'].' '.$row['buyer_last_name'];
		$row['actions']="<span onclick='detalii_comanda2(".$row['id'].");' style='cursor:pointer;'>Printeaza eticheta colet</span>&nbsp;&nbsp;&nbsp;<span onclick='detalii_comanda(".$row['id'].");' style='cursor:pointer;'>Printeaza comanda</span>&nbsp;&nbsp;&nbsp; ".$row['actions'];
		return $row;
	}
	function js() {
		?>
		function add_order_product(id_product,code,name,price){
			var text='';
			var quantity=prompt('<?=l('introduceti cantiatea')?>');
			text+='<dl>';
			text+='	<input type="hidden" name="products[id_product][]" value="'+id_product+'" />';
			text+='<input type="hidden" name="products[code][]" value="'+code+'" />';
			text+='	<input type="hidden" name="products[product][]" value="'+name+'" />';
			text+='	<input type="hidden" name="products[price][]" value="'+price+'" />';
			text+='	<img src="images/delicon.gif" align="absmiddle" class="rem left" class="pointer" />';
			text+='	<dt style="width:500px;">';
			text+='		<label for="order_product_'+id_product+'">'+name+' ('+code+' )</label>';
			text+='	</dt>';
			text+='	<dd style="margin-left:500px;">';
			text+='	<input type="text" style="width:40px;text-align:center;" name="products[quantity][]" value="'+quantity+'" /> <strong>'+price+' Lei</strong>';
			text+='	</dd>';
			text+='</dl>';
			$('#order_products_list').append(text);

			$("img.rem").click(function(){
		if (($(this).parent().find(".add").size())==0) $(this).parent().remove();
		if (($(this).parent().parent().find(".add").size())>1) $(this).parent().remove();
	});
		}
		function do_product_filter(id,div) {
			var data="id_category="+$("#id_category").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);

					}
				});
		}
		function load_order_info(id) {
			$.ajax({
				data :"id="+id,
				type :"POST",
				url :"module/"+module+".php?show_info=1",
				timeout :45000,
				error : function() {
					console.log("Failed to submit - ");
				},
				success : function(r) {
					$('#show_info_'+id).html(r);
				}
			});
		}
		$(document).ready(function(){
			$(".module_menu .update_status").click(function(){
				var window_add_edit_name='<?=l('Schimba statut')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();

				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_status&ids='+gr,600, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);

			});

				$(".module_menu .facturi").click(function(){
				var window_add_edit_name='<?=l('Link`uri facturi')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=facturi',800, function () {
				after_window_load('<?=$this->module?>','edit');
				$('#startDate').datepicker('option', 'altField', '#unixstartDate');
				$('#endDate').datepicker('option', 'altField', '#unixendDate');
				$('#startDate,#endDate').datepicker('option', 'altFormat', '@');
				$('#startDate,#endDate').datepicker('option', 'maxDate', '+0s');
				$('#startDate,#endDate').datepicker('option', 'minDate', new Date(2007, 12 - 1, 1));
				$('.buttons #Afiseaza').click(function(){
					$('#facturi_div').load('ajax/facturi.php?start='+$('#unixstartDate').val()+'&end='+$('#unixendDate').val());
				});
				},true);
			});


			$(".module_menu .necessary").click(function(){
				var window_add_edit_name='<?=l('Necesar marfa')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=necessary',1000, function () {
				after_window_load('<?=$this->module?>','edit');
				$('#startDate').datepicker('option', 'altField', '#unixstartDate');
				$('#endDate').datepicker('option', 'altField', '#unixendDate');
				$('#startDate,#endDate').datepicker('option', 'altFormat', '@');
				$('#startDate,#endDate').datepicker('option', 'maxDate', '+0s');
				$('#startDate,#endDate').datepicker('option', 'minDate', new Date(2007, 12 - 1, 1));
				$('.buttons #Afiseaza').click(function(){
					$('#raport').load('ajax/raport.php?start='+$('#unixstartDate').val()+'&end='+$('#unixendDate').val());
				});
				},true);
			});
			$(".module_menu .reports").click(function(){
				var window_add_edit_name='<?=l('Rapoarte')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=reports',1000, function () {
				after_window_load('<?=$this->module?>','edit');
				$('#startDate').datepicker('option', 'altField', '#unixstartDate');
				$('#endDate').datepicker('option', 'altField', '#unixendDate');
				$('#startDate,#endDate').datepicker('option', 'altFormat', '@');
				$('#startDate,#endDate').datepicker('option', 'maxDate', '+0s');
				$('#startDate,#endDate').datepicker('option', 'minDate', new Date(2007, 12 - 1, 1));
				$('.buttons #Afiseaza').click(function(){
					$('#raport').load('ajax/rapoarte.php?start='+$('#unixstartDate').val()+'&end='+$('#unixendDate').val()+'&filter='+$('#filterType').val());
				});
				$('.filterType input[type=radio]').change(function(){
					$('#filterType').val($(this).val());
				});
				},true);
			});
		});
		<?php
		set_grid($this,array('multiselect'=>true,'sortorder' => 'desc' ));
	}
	function css() {
		?>
		#list_products_dd {
			background-color:#FFFFFF;
			width:460px;
			border:1px solid #B8B5CF;
			height:160px;
			overflow-y:scroll;
			overflow-x:hidden;
		}
		#list_products_dd>div:hover {
			background-color:#E2E0EF;
		}
		.order_hr {
			height:18px;
			clear:both;
		}
		.order_info_col {
			float:left;
			width:310px;
			border-left:3px solid #696C8B;
			padding:8px;
		}
		.order_info_col_none {
			border-left:0 none;
		}
		.order_info_col b {
			font-size:1.2em;
			display:block;
			margin-bottom:4px;
			border-bottom:1px solid #696C8B;
		}
		.order_info_col div {
			padding:1px;
		}
		.order_info_col div:hover {
			background-color:#FFF1A0;
		}
		.order_info_col div span{
			display:block;
			float:right;
			font-weight:bold;
			width:200px;
			font-size:12px;
			padding-left:4px;
			/*border-left:1px solid #696C8B;*/
		}
		dl dd input[type="text"], dl dd textarea  {
			width:80%;
		}
		.module_menu li.update_status {
			background-image:url('../../images/icons/orders_update_status.png');
		}
		.module_menu li.necessary {
			background-image:url('../../images/icons/orders_update_status.png');
		}
		.module_menu li.reports {
			background-image:url('../../images/icons/orders_update_status.png');
		}
		.module_menu li.facturi {
			background-image:url('../../images/icons/orders_update_status.png');
		}
		<?php
	}
	function print_records() {
		print_content($this,array('update_status'=>l('Schimba statut')),'','',array('new'));
	}

	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$this->return_stock($id);
			$db->delete(TABLE_ORDERS_PRODUCTS," `id_order`=".$id);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function show_order_products($date){
		global $db;
		$order_product=$db->fetch_all("SELECT * FROM `".TABLE_ORDERS_PRODUCTS."` WHERE `id_order`=".$date['id']);
		?>
		<div id="order_products_list">
		<?php
		foreach ($order_product as $product) {
		?>
		<dl>
			<input type="hidden" name="products[id][]" value="<?=$product['id']?>" />
			<input type="hidden" name="products[id_product][]" value="<?=$product['id_product']?>" />
			<input type="hidden" name="products[code][]" value="<?=$product['code']?>" />
			<input type="hidden" name="products[product][]" value="<?=$product['product']?>" />
			<input type="hidden" name="products[base_price][]" value="<?=number_format($product['price'],3)?>" />
			<img src="images/delicon.gif" align="absmiddle" class="rem left" class="pointer" />
			<dt style="width:500px;">
				<label for="order_product_<?=$product['id']?>"><?=$product['product']?> ( <?=$product['code']?> ) </label>
			</dt>
			<dd style="margin-left:500px;">
				<input type="text" style="width:40px;text-align:center;" name="products[quantity][]" value="<?=$product['quantity']?>" /> <strong><?=$product['price']?> Lei</strong>
			</dd>
			<dt style="width:516px;">
				<label for="order_product_delivered_<?=$product['id']?>">&nbsp;&nbsp;&nbsp;&nbsp;---&nbsp;&nbsp;&nbsp;&nbsp;Cantitate livrata</label>
			</dt>
			<dd style="margin-left:510px;">
				<input type="text" style="width:40px;text-align:center;" name="products[quantity_delivered][]" value="<?=$product['quantity_delivered']?>" /> <strong><?=$product['price_delivered']?> Lei</strong>
			</dd>
		</dl>
		<?php
		}
		?>
		</div>
		<?php
	}
	function update_status($ids){
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_status&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header();
		?>
		<dl class="related_products_select">
			<dt>
				<label for="status_comanda"><?=l('Status')?></label>
			</dt>
			<dd>
			<select name="status_comanda" id="status_comanda" >
			<?php
			foreach ($this->orders_status as $key=>$status) {
				?>
				<option value="<?=$key?>"><?=$status?></option>
				<?php
			}
			?>
			</select>
			</dd>
		</dl>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function status_mail_textarea($id=0) {
		global $db;?>
		<script type="text/javascript">
		//var emails=["comanda_in_derulare","comanda_aprobata","comanda_livrata","comanda_in_asteptare","comanda_livrata_partial","comanda_anulata"];
		var emails=["comanda_in_derulare","comanda_aprobata","comanda_livrata","comanda_in_asteptare","comanda_livrata_partial","comanda_anulata"];
		$('#status input[type="radio"]').change(function(){
			$('#update_notification_1').attr('checked','checked');
			$('#update_status_mail').text($('#'+emails[$(this).val()]).val());
			$('#template').val(emails[$(this).val()]);
			$('#emailsubject').val(emails[$(this).val()]);
		});
		</script>
		<?$emails=$db->fetch_all("SELECT * FROM `".TABLE_EMAILS."`,`".TABLE_EMAILS.TABLE_EXTEND."` WHERE `".TABLE_EMAILS."`.`id`=`".TABLE_EMAILS.TABLE_EXTEND."`.`id_main` AND `template` LIKE 'comanda%'");
		foreach ($emails as $e) {
			echo "<input type=\"hidden\" id=\"".$e['template']."\" value=\"".htmlentities($e['content'])."\" />";
		}?>
		<input type="hidden" name="template" id="template" value="" />
		<input type="hidden" name="emailsubject" id="emailsubject" value="" />
		<textarea id="update_status_mail" name="update_status_mail" style="border:1px solid #ccc;width:90%;min-height:100px;padding:5px 10px;">
			Mail template
		</textarea>
		<!--<script type="text/javascript">
		$('#update_status_mail').click(function(){
			$('#template').val('changed');
		});
		</script>-->
	<?}
	function facturi(){
		global $main_buttons;
		$buttons=array('Afiseaza'=>array('type'=>'button','value'=>'Afiseaza'));
		//die(print_a($main_buttons));
		?>
		<form action="?mod=<?=$this->module?>&action=necessary" method="POST" id="necessary">
		<?php
		print_form_header();
		?>
		<style type="text/css">
		@media screen{
			#print { padding: 10px;color:#333;font-weight:bold; }
			#print a{ cursor:pointer; }
			#print a:hover { font-size:1.2em;text-decoration:underline; }
		}
		</style>

		<dl>
			<dt><label for="startDate"><?=l('De la data')?></label></dt>
			<dd><input type="text" name="startDate" id="startDate" style="width:100px;" class="datepicker" value="<?=date('d-m-Y',time()-2592000)?>" /><input type="hidden" name="unixstartDate" id="unixstartDate" /></dd>
		</dl>
		<dl>
			<dt><label for="endDate"><?=l('Pana la')?></label></dt>
			<dd><input type="text" name="endDate" id="endDate"  style="width:100px;" class="datepicker" value="<?=date('d-m-Y',time())?>" /><input type="hidden" name="unixendDate" id="unixendDate" /></dd>
		</dl>
		<div id="facturi_div"></div>

		<?php
		print_form_footer();
		print_form_buttons($buttons);
		?>
		</form>
		<?php
	}
	function necessary(){
		global $main_buttons;
		$buttons=array('Afiseaza'=>array('type'=>'button','value'=>'Afiseaza'));
		//die(print_a($main_buttons));
		?>
		<form action="?mod=<?=$this->module?>&action=necessary" method="POST" id="necessary">
		<?php
		print_form_header();
		?>
		<style type="text/css">
		@media screen{
			#print { padding: 10px;color:#333;font-weight:bold; }
			#print a{ cursor:pointer; }
			#print a:hover { font-size:1.2em;text-decoration:underline; }
		}
		</style>
		<dl>
			<dt><label for="startDate"><?=l('De la data')?></label></dt>
			<dd><input type="text" name="startDate" id="startDate" class="datepicker" value="<?=date('d-m-Y',time()-2592000)?>" /><input type="hidden" name="unixstartDate" id="unixstartDate" /></dd>
		</dl>
		<dl>
			<dt><label for="endDate"><?=l('Pana la')?></label></dt>
			<dd><input type="text" name="endDate" id="endDate" class="datepicker" value="<?=date('d-m-Y',time())?>" /><input type="hidden" name="unixendDate" id="unixendDate" /></dd>
		</dl>
		<div id="raport"></div>
		<?php
		print_form_footer();
		print_form_buttons($buttons);
		?>
		</form>
		<?php
	}
	function reports(){
		global $main_buttons;
		$buttons=array('Afiseaza'=>array('type'=>'button','value'=>'Afiseaza'));
		//die(print_a($main_buttons));
		?>
		<form action="?mod=<?=$this->module?>&action=reports" method="POST" id="reports">
		<?php
		print_form_header();
		?>
		<style type="text/css">
		@media screen{
			#print { padding: 10px;color:#333;font-weight:bold; }
			#print a{ cursor:pointer; }
			#print a:hover { font-size:1.2em;text-decoration:underline; }
		}
		</style>
		<dl>
			<dt><label for="startDate"><?=l('De la data')?></label></dt>
			<dd><input type="text" name="startDate" id="startDate" class="datepicker" value="<?=date('d-m-Y',time()-2592000)?>" /><input type="hidden" name="unixstartDate" id="unixstartDate" /></dd>
		</dl>
		<dl>
			<dt><label for="endDate"><?=l('Pana la')?></label></dt>
			<dd><input type="text" name="endDate" id="endDate" class="datepicker" value="<?=date('d-m-Y',time())?>" /><input type="hidden" name="unixendDate" id="unixendDate" /></dd>
		</dl>
		<dl>
			<dt><label for="filterType"><?=l('Tip')?></label></dt>
			<dd class="filterType">
				<input type="radio" name="filterType_s" id="filterType_0" checked="checked" value="0" />
				Clienti
				<input type="radio" name="filterType_s" id="filterType_1" value="1" />
				Luna si an
				<input type="radio" name="filterType_s" id="filterType_2" value="2" />
				Producatori
				<input type="hidden" name="filterType" id="filterType" value="0" />
			</dd>
		</dl>
		<!--<dl>
			<dt><label for="filterUser"><?=l('Utilizator')?></label></dt>
			<dd>
				<input type="text" name="filterUser" id="filterUser" style="width:40%;" />
				<select name="filterUserResults" style="display:none;width:40%;margin-top:5px;" size="10">
					<option value="toti">toti</option>
				</select>
			</dd>
		</dl>-->
		<!--<dl>
			<dt>Utilizator selectat:</dt>
			<dd><input type="text" id="filterUserSelected" style="display:none;width:40%;margin-top:5px;" /></dd>
		</dl>-->
		<div id="raport"></div>
		<script type="text/javascript">
		//			$(document).ready(function(){
		//				$('#filterUser').keyup(function(){
		//					$('select[name="filterUserResults"]').css('display','block').load('ajax/usersdb.php?user='+$('#filterUser').val());
		//				});
		//				$('select[name="filterUserResults"]').change(function(){
		//					$('#filterUserSelected').css('display','block').val($(this).val());
		//					$('#filterUserSelected').html(
		//						$('option[value="'+$(this).val()+'"]').text()
		//					);
		//				});
		//			});
		</script>

		<?php
		print_form_footer();
		print_form_buttons($buttons);
		?>
		</form>
		<?php
	}
	function return_stock($id){
		global $db;
		$products=$db->fetch_all("SELECT * FROM `".TABLE_ORDERS_PRODUCTS."` WHERE `id_order`=".$id);
		foreach ($products as $p) {
			if (!empty($p['variation'])) {
				$db->sql("UPDATE `".TABLE_VARIATIONS."` SET stock=stock+".$p['quantity']." WHERE `variation`='".$p['variation']."' AND id_product=".$p['id_product']);
			} else {
				$db->sql("UPDATE `".TABLE_PRODUCTS."` SET stock=stock+".$p['quantity']." WHERE `id`=".$p['id_product']);
			}
		}
	}
	function save_status($ids){
		global $db;
		$ids=explode(",",$ids);
		foreach ($ids as $id) if (!empty($id)) {
			$db->update($this->table,array("status"=>r('status_comanda'))," id=".$id);
			if (r('status_comanda')==5) {
				$this->return_stock($id);
			}
			$statusEmail=$db->fetch("SELECT * FROM `".TABLE_ORDERS."` WHERE `id`=".$id);
			$array_vars=array(
			'name'=>$statusEmail['buyer_first_name'].' '.$statusEmail['buyer_last_name'],
			'email'=>$statusEmail['buyer_email'],
			'comment'=>$statusEmail['comment'],
			'no_order'=>$statusEmail['no_order'],
			//'order_products'=>$statusEmail['products'],
			'order_total'=>$statusEmail['amount'],
			//'nr_order'=>$statusEmail$id,
			);
			template_mail('comanda_'.str_replace(' ','_',$this->orders_status[$statusEmail['status']]),$statusEmail['buyer_email'],$array_vars);
		}
		close_window($this->module);
		print_alerta(l('Statusul a fost modificat'));
	}
	function save($id){
		global $db,$config;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			//die(print_a($data));
			close_window($this->module);
			$products=$data['products'];
			//print_a($products);
			//die();
			$mailsend=$data['update_notification'];
			$array_subject=array('comanda_livrata'=>'Comanda livrata','comanda_aprobata'=>'Comanda dvs. a fost acceptata','comanda_anulata'=>'Comanda anulata');
			$emailsubject=$array_subject[$data['template']];
			$template=$data['template'];
			$emailcontent=$data['update_status_mail'];
			unset($data['type_op'],$data['products'],$data['product_name'],$data['id_category'],$data['update_notification'],$data['template'],$data['update_status_mail'],$data['emailsubject']);
					$db->delete(TABLE_ORDERS_PRODUCTS," `id_order`=".$id);
			$total=0;
			foreach ($products['id_product'] as $key=>$id_product) {
				$array_insert=array(
				'id_order'=>$id,
				'id_product'=>$id_product,
				'product'=>$products['product'][$key],
				'quantity'=>$products['quantity'][$key],
				'price'=>$products['base_price'][$key],
				'code'=>$products['code'][$key],
				);

				$total+=$products['base_price'][$key]*$products['quantity'][$key];
				if ($data['status']==5) { $db->sql("UPDATE `".TABLE_PRODUCTS."` SET stock=stock+".$products['quantity'][$key]." WHERE id=".$id_product); }
				unset($products['base_price'][$key]);

				$db->insert(TABLE_ORDERS_PRODUCTS,$array_insert);
			}
			$data['amount']=$total;
			//die(print_r($data));
			//+$data['shipping_tax']+$data['handling_fee'];
			$db->update($this->table,$data," id=".$id);
			//$indisponibile=implode(' ',$indisponibile);
			//if ($data['status']!=0 && $mailsend==1) {
			if ($data['status']!=0 && $mailsend==1) {
				$array_vars=array(
				'name'=>$data['buyer_first_name'].' '.$data['buyer_last_name'],
				'email'=>$data['buyer_email'],
				'comment'=>$data['comment'],
				'no_order'=>$array_insert['no_order'],
				'order_products'=>$products,
				'order_total'=>$data['amount'],
				'indisponibile'=>$indisponibile,
				);
				sendHTMLemail($config['SITE_EMAIL'],$config['SITE_NAME'],$array_vars['email'],$emailsubject,nl2br($emailcontent),'');
				//if ($template=='changed') sendHTMLemail($config['SITE_EMAIL'],$config['SITE_NAME'],$array_vars['email'],$emailsubject,nl2br($emailcontent),'');
				//else template_mail('comanda_'.str_replace(' ','_',strtolower($this->orders_status[$data['status']])),$data['buyer_email'],$array_vars);
			}
			print_alerta('a fost updatat');

		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function show_info($id=0) {
		global $db;
		$orders_status=$this->orders_status;
		$order=$db->fetch("SELECT * FROM `".TABLE_ORDERS."` WHERE `id`=".$id);
		?>
		<div class="order_info_col order_info_col_none">
			<b><?=l('Detalii client')?></b>
			<div> <?=l('Nume')?><span> <?=$order['buyer_first_name']?></span></div>
			<div> <?=l('Prenume')?><span> <?=$order['buyer_last_name']?></span></div>
			<div> <?=l('Email')?><span> <?=$order['buyer_email']?></span></div>
			<div> <?=l('Cnp')?><span> <?=$order['buyer_cnp']?></span></div>
			<div> <?=l('Serie C.I.')?><span> <?=$order['buyer_ci_serie']?> <?=$order['buyer_ci_number']?></span></div>
			<div> <?=l('Oras')?><span> <?=$order['buyer_city']?></span></div>
			<div> <?=l('Judet')?><span> <?=$db->fetch_one("select `name` from xp_states where id=".$order['buyer_state'])?></span></div>
			<div> <?=l('Cod postal')?><span> <?=$order['buyer_zip_code']?></span></div>
			<div> <?=l('Adresa')?><span> <?=$order['buyer_address']?></span></div>
			<div> <?=l('Telefon')?><span> <?=$order['buyer_phone']?></span></div>
		</div>
		<div class="order_info_col">
			<b><?=l('Detalii firma')?></b>
			<div> <?=l('Nume')?><span> <?=$order['company']?></span></div>
			<div> <?=l('CUI')?><span> <?=$order['company_cui']?></span></div>
			<div> <?=l('Nr. Reg. Com.')?><span> <?=$order['company_nr_reg_com']?></span></div>
			<div> <?=l('Banca')?><span> <?=$order['company_bank']?></span></div>
			<div> <?=l('Cont')?><span> <?=$order['company_bank_account']?></span></div>
			<div> <?=l('Oras')?><span> <?=$order['company_city']?></span></div>
			<div> <?=l('Judet')?><span> <?=$order['company_state']?></span></div>
			<div> <?=l('Adresa')?><span> <?=$order['company_address']?></span></div>
			<div> <?=l('Cod postal')?><span> <?=$order['company_zip_code']?></span></div>
			<div> <?=l('Telefon')?><span> <?=$order['company_phone']?></span></div>
		</div>
		<div class="order_info_col">
			<b><?=l('Detalii livrare')?></b>
			<div> <?=l('Metoda de livrare')?><span> <?=$order['shipping_method']?></span></div>
			<br />
			<b><?=l('Detalii adresa')?></b>
			<div> <?=l('Oras')?><span> <?=$order['shipping_city']?></span></div>
			<div> <?=l('Judet')?><span> <?=$order['shipping_state']!=''?$db->fetch_one("select `name` from xp_states where id=".$order['shipping_state']):''?></span></div>
			<div> <?=l('Adresa')?><span> <?=$order['shipping_address']?></span></div>
			<div> <?=l('Cod postal')?><span> <?=$order['shipping_zip_code']?></span></div>
		</div>
		<div class="order_hr"></div>
		<div class="order_info_col order_info_col_none" style="overflow:hidden;">
			<b><?=l('Detalii securitate')?></b>
			<div> <?=l('Data')?> <span><?=date('d-m-Y H:m:s',$order['date'])?></span></div>
			<div> <?=l('Adresa ip')?> <span><?=$order['ip']?></span></div>
			<div> <?=l('Statut')?> <span><?=$orders_status[$order['status']]?></span></div>
			<b><?=l('Observatii')?></b>
			<div> <?=$order['comment']?> </div>
		</div>
		<div class="order_info_col" style="width:620px;">
			<script type="text/javascript">
			$(document).ready(function(){
				$('.order_info_col a').click(function(){
					window.open(this.href);
				});
			});
			</script>
			<b><?=l('Produse comandate')?></b>
			<?php
			$products=$db->fetch_all("SELECT * FROM `".TABLE_ORDERS_PRODUCTS."` WHERE `id_order`=".$id,MYSQL_ASSOC);
			foreach ($products as $product) {
				$product['id_category']=$db->fetch_one("SELECT `id_category` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$product['id_product']."",MYSQL_ASSOC);
				$product['name_seo']=$db->fetch_one("SELECT `name_seo` FROM `".TABLE_PRODUCTS.TABLE_EXTEND."` WHERE `id_main`=".$product['id_product']."",MYSQL_ASSOC);
				//$product['id_brand']=$db->fetch_one("SELECT `id_brand` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$product['id_product']."",MYSQL_ASSOC);
				//$brand=$db->fetch_one("SELECT `name` FROM `".TABLE_BRANDS."` WHERE `id`=".$product['id_brand']);?>
			<div style="padding-top:5px;"> <?=$product['quantity']?> X <?=$product['product']?> - <strong><?=$product['code']?></strong> <span style="text-align:right;"><?=number_format($product['price'],2,'.','')?></span></div>
			<?php } ?>
			<div style="border-top:2px solid #000;border-bottom:2px solid #000;margin:10px 0;"> <?=l('Subtotal comanda')?><span style="text-align:right;"><?=number_format($order['amount'],2,'.','')?></span></div>
			<div> <?=l('Taxa de livrare')?><span style="text-align:right;"><?=number_format($order['shipping_tax'],2,'.','')?></span></div>
			<div style="border-top:2px solid #000;border-bottom:2px solid #000;margin:10px 0;"> <?=l('Total comanda')?><span style="text-align:right;"><?=number_format(($order['amount']+$order['shipping_tax']+$order['handling_fee']),2,'.','')?></span></div>

		</div>
		<div class="clear"></div>
		<?php
	}
}
$module=new orders_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['show_info']==1) $module->show_info(fpost('id'));
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='update_status') $module->update_status(fget('ids'));
elseif ($_GET['action']=='necessary') $module->necessary();
elseif ($_GET['action']=='facturi') $module->facturi();
elseif ($_GET['action']=='reports') $module->reports();
elseif ($_GET['action']=='save_status') $module->save_status(fget('ids'));
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>