<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class shipping_methods_module {
	var $module='shipping_methods';
	var $date='15-09-2009';
	var $table=TABLE_SHIPPING_METHODS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function shipping_methods_module() {

		$this->name=l('Metode de livrare');
		$this->title=l('Metode de livrare');
		$this->description=l('Metode de livrare');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>200),
		'type'=>array('name'=>l('Tip'),'width'=>100),
		'default'=>array('name'=>l('Default'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>140,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,unique,min_2,max_240','style'=>'width:80%;'),
		1=>'show_shipping_motods_type',
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ').' &nbsp; ',0=>l('Inactiv')),'name'=>l('status')),
		'default'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('default')),
		2=>l('Costuri de transport'),
		);

		$this->method_type=array(
		1=>l('Plata ramburs'),
		);
	}
	function json_list() {
		json_list($this,false);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		.module_menu li.back_zones {
			background-image:url('../../images/icons/shipping_methods_back.png');
		}
		.all_div_tax {
			display:none;
		}
		<?php
	}
	function print_records() {
		if (!empty($_GET['id_zone'])) {
		?>
		<script type="text/javascript">
		add_all_url['<?=$this->module?>']="&id_zone=<?=fget('id_zone')?>";
		</script>
		<?php
		}
		$new_icons=array(
		'nss_position'=>'left',
		'back_zones'=>l('Shipping zones'),
		);
		print_content($this,$new_icons);
	}
	function json_list_row($row) {
		if ($row['default']==1) {
			$row['default']='<img class="ch_default_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;" />';
		} else $row['default']='<img class="ch_default_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;" />';
		$row['type']=$this->method_type[$row['type']];
		return $row;
	}
	function js() {
		$before='$(".back_zones").click(function(){set_location(\'?mod=shipping_zones\')});';
		set_grid($this,array('multiselect'=>true),$before);
	}
	function json_list_2($id_zone){
		global $db;

		$new_sql= "SELECT * FROM `" . $this->table . "` WHERE `id_zone`=".(int)$id_zone." ";
		json_list($this,false,$new_sql);
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			$date_saved['method']=unserialize($date_saved['method']);
			$_GET['id_zone']=$date_saved['id_zone'];
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<input type="hidden" name="id_zone" value="<?=fget('id_zone')?>" />
		<?php 
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		?>		
		<dl>
			<dt>
				<label for="shipping_zone_type"><?=l('Cost de baza')?></label>	
			</dt>
			<dd>
				<input type="text" name="method[default_tax]" value="<?=$date_saved['method']['default_tax']?>" /> 
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="shipping_zone_type" ><?=l('Taxa per item')?></label>	
			</dt>
			<dd>
				<input type="text" name="method[per_item_tax]" value="<?=$date_saved['method']['per_item_tax']?>" /> 
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="fan_courier_percent"><?=l('Procent din comanda')?></label>
			</dt>
			<dd>
				<input type="text" name="method[percent_tax]" id="fan_courier_percent" value="<?=$date_saved['method']['percent_tax']?>" /> % 
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="shipping_zone_type"><?=l('Taxa pe valoare comanda')?></label>
			</dt>
			<dd>
			<?php 
			if (empty($date_saved['method']['total_tax_value'])) {
				$date_saved['method']['total_from_value']=array(0);
				$date_saved['method']['total_to_value']=array(0);
				$date_saved['method']['total_tax_value']=array(0);
			}
			foreach ((array)$date_saved['method']['total_from_value'] as $key=>$from_value) {
			?>
				<div style="padding:2px;"> 
					<input type="text" name="method[total_from_value][]" value="<?=(double)$date_saved['method']['total_from_value'][$key]?>" size="6" /> - 
					<input type="text" name="method[total_to_value][]" value="<?=(double)$date_saved['method']['total_to_value'][$key]?>" size="6" /> 
					<?=l('taxa transport')?> : <input type="text" name="method[total_tax_value][]"  value="<?=(double)$date_saved['method']['total_tax_value'][$key]?>" align="absmiddle" /> 
					<img src="images/addicon.gif" align="absmiddle" onclick="$(this).parent().clone(true).insertAfter($(this).parent());"  class="pointer"  /> 
				 	<img src="images/delicon.gif" align="absmiddle" onclick="$(this).parent().remove();"  class="pointer"  />
			 	</div>
			 	<?php } ?>
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="shipping_zone_type"><?=l('Taxa pe greutate')?></label>
			</dt>
			<dd>
			<?php
			if (empty($date_saved['method']['weight_tax_value'])) {
				$date_saved['method']['weight_from_value']=array(0);
				$date_saved['method']['weight_to_value']=array(0);
				$date_saved['method']['weight_tax_value']=array(0);
			}
			foreach ((array)$date_saved['method']['weight_from_value'] as $key=>$from_value) {
			?>
				<div style="padding:2px;"> 
					<input type="text" name="method[weight_from_value][]" value="<?=(double)$date_saved['method']['weight_from_value'][$key]?>" size="6" /> - 
					<input type="text" name="method[weight_to_value][]" value="<?=(double)$date_saved['method']['weight_to_value'][$key]?>" size="6" /> 
					<?=l('taxa transport')?> : <input type="text" name="method[weight_tax_value][]"  value="<?=(double)$date_saved['method']['weight_tax_value'][$key]?>" align="absmiddle" /> 
					<img src="images/addicon.gif" align="absmiddle" onclick="$(this).parent().clone(true).insertAfter($(this).parent());"  class="pointer"  /> 
				 	<img src="images/delicon.gif" align="absmiddle" onclick="$(this).parent().remove();"  class="pointer"  />
			 	</div>
			 	<?php } ?>
			</dd>
		</dl>
		<dl class="all_div_tax div_tax_1">
			<dt>
				<label for="fan_courier_first_kg"><?=l('Primul Kg')?></label>
			</dt>
			<dd>
				<input type="text" name="method[fan_first_kg]" id="fan_courier_first_kg" value="<?=$date_saved['method']['fan_first_kg']?>" /> 
			</dd>
		</dl>
		<dl class="all_div_tax div_tax_1">
			<dt>
				<label for="fan_courier_extra_kg"><?=l('Km extra')?></label>
			</dt>
			<dd>
				<input type="text" name="method[fan_extra_kg]" id="fan_courier_extra_kg" value="<?=$date_saved['method']['fan_extra_kg']?>" /> 
			</dd>
		</dl>
		<dl class="all_div_tax div_tax_1">
			<dt>
				<label for="fan_courier_ramburs"><?=l('Plata Ramburs')?></label>
			</dt>
			<dd>
				<input type="text" name="method[ramburs_tax]" id="fan_courier_ramburs" value="<?=$date_saved['method']['ramburs_tax']?>" /> 
			</dd>
		</dl>
		<dl class="all_div_tax div_tax_1">
			<dt>
				<label for="ramburs_on_invoice"><?=l('Show Separately')?></label>
			</dt>
			<dd>
				<input type="checkbox" name="method[ramburs_on_invoice]" id="ramburs_on_invoice" value="1" <?=(( $date_saved['method']['ramburs_on_invoice'] == 1 )? 'checked' : '')?>" /> <?=l('Arata separat taxa ramburs plus procentul din total')?>
			</dd>
		</dl>
		
		
	
		
		<script type="text/javascript">
		$('.all_div_tax').hide();
		$('.div_tax_<?=$date_saved['type']?>').show();
		</script>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function show_shipping_motods_type($date){
	?>
		<dl>
			<dt>
				<label for="shipping_zone_type"><?=l('Metoda de livrare')?></label>
			</dt>
			<dd>
				<select name="type" onchange="$('.all_div_tax').hide(); $('.div_tax_'+this.value).show();">
					<option value="0"><?=l('Select')?></option>
					<?php foreach ($this->method_type as $key=>$name) { ?>					
						<option value="<?=$key?>" <?=(($date['type']==$key)?'selected':'')?>><?=$name?></option>
					<?php } ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			$data['method']=serialize($data['method']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new shipping_methods_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list_2(fget('id_zone'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>