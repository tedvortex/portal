<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class offers_module {
	var $module='offers';
	var $date='25-08-2009';
	var $table=TABLE_MESSAGES;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function offers_module() {

		$this->name=l('Oferte');
		$this->title=l('Oferte');
		$this->description=l('Oferte');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'id_product'=>array('name'=>l('Anunt'),'width'=>120,'align'=>'left'),
		'id_sender'=>array('name'=>l('User sender'),'width'=>120,'align'=>'left'),
		'id_receiver'=>array('name'=>l('User receiver'),'width'=>120,'align'=>'left'),
		'subject'=>array('name'=>l('Subiect mesaj'),'width'=>200),
		'description'=>array('name'=>l('Continut mesaj'),'width'=>300),
		//'title'=>array('name'=>l('Title'),'width'=>300),
		//'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'subject'=>array('type'=>'input','name'=>l('Titlu mesaj'),'text'=>'','info'=>'','valid'=>'empty,min_2','style'=>'width:82%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
//		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
//		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
//		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
//		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		//'link'=>array('type'=>'input','name'=>l('Link'),'text'=>'','lang'=>true,'style'=>'width:82%;'),
		//'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:82%;'),
//		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
//		'email_news'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('email news')),
		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true,'valid'=>'empty'),
		'description'=>array('type'=>'editor','name'=>l('description')),
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'ニュース'),
		);
	}
	function css() {}
	function json_list() {
		json_list($this,false);
	}
	function json_list_row($row) {
		global $gd,$db;
		//$name=$db->fetch_one("SELECT `name` FROM `".TABLE_ADS.TABLEEXTEND."` WHERE `id`=".$row['id_receiver']);
		$ad=lang_item_info(TABLE_ADS,$row['id_product']);
		$row['id_product']="<a href=".lang_item_link(TABLE_CATEGORIES,$ad)." targer='_blank'>".(($ad['name']!='')?$ad['name']:'Anuntul nu mai exista in db')."</a>";
		$row['id_sender']=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_sender']);
		if ($row['id_sender']==false) {
			$row['id_sender']='<span style="color:red;">'.l('Anonim').'</span>';
		}
		$row['id_receiver']=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_receiver']);
		if ($row['id_receiver']==false) {
			$row['id_receiver']='<span style="color:red;">'.l('Userul nu mai exista in db').'</span>';
		}
		if (!empty($row['image']))
		$row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		 global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Oferta'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		unset($data['email_news']);
		$errors=form_validation($data,$this->form,$this->table);
		//print_a($data);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new offers_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list($module,false);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>