<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class filters5_module {
	var $module='filters5';
	var $date='3-10-2009';
	var $table='xp_filters';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function filters5_module() {

		$this->name=l('Particularitati tarife croaziere');
		$this->title=l('Particularitati tarife croaziere');
		$this->description=l('Particularitati tarife croaziere');

		$this->type=array(
		'like'=>array('name','id'),
		'equal'=>array('id_category','status','id')
		);

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Nume filtru'),'width'=>300),
		//'id_category'=>array('name'=>l('Categorii oferte'),'width'=>180,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		'is_boxes'=>array('name'=>l('Afiseaza in box'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,min_2,max_140','style'=>'width:92%;'),
		'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:12%;'),
		//1=>'show_categories',
		'is_boxes'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('Afiseaza in box filtrare')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		2=>l('optiuni')
		);
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		.drag_all_top span {
			cursor:move;
			font-size:14px;
			background-image:url('../../images/grid_move.gif');
		}
		.drag_all_top div.opt_value{

			padding:2px;
			padding-left:4px;

		}
		<?php
	}
	function json_list() {

		$new_sql="select sql_calc_found_rows
					*
				from
					`{$this->table}` 
					where `id_category`=3";

		json_list($this,false,$new_sql);
	}
	function json_list_row($row) {
		global $db;
		
		if ($row['is_boxes']==1) {
			$row['is_boxes']='<img id="id_line_'.$row['id'].'" rel="is_boxes" class="ch_staus_img" src="images/tick.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		} else {
			$row['is_boxes']='<img id="id_line_'.$row['id'].'" rel="is_boxes" class="ch_staus_img" src="images/cross.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		}
		
//		if ($row['id_category']==0) {
//			$row['id_category']='<span style="color:red;">'.l('Toate categoriile').'</span>';
//		} else {
//			$row['id_category']=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."_data` WHERE `_id`=".$row['id_category']."");
//		}
		return $row;
	}
	function js() {
		?>
		var window_add_edit_name_category="<?=l('Add Category') ?>";
		function add_category(){
			var module_now='categories';
			nss_win(module_now + '_new', window_add_edit_name_category,
			'module/' + module_now + '.php?action=new', 800,
			function() {
				$("#window_" + module_now + "_new form").bind('submit',
				function() {
					return false;
				});
				after_window_load(module_now, 'new',load_categories);
			});
		}
		function load_categories(){
			$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
		}		

		<?php
		$loadComplete='
			$("#gs_id_category").load("module/'.$this->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
		';
		set_grid($this,array('multiselect'=>true,'sortorder' => 'asc','sortname' => 'order','loadComplete'=>$loadComplete));
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fost('id'));
		foreach ($ids as $id) if(!empty($id)) {
			$ids_options=$db->fetch_all("SELECT * FROM `xp_filters_options` WHERE id_variation=".$id);
			foreach ($ids_options as $id_option) $db->delete('fibula_hotels_filters_values',"`id_option`=".$id);
			$db->delete('xp_filters_options',"`id_variation`=".$id);
		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		?>
		<div class="drag_all_top">
		<?php
		if (!empty($id)) $filters_options=$db->fetch_all("SELECT * FROM `xp_filters_options` WHERE `id_filter`=".$id." ORDER BY `order` ASC");
		$filters_options=array_merge((array)$filters_options,array(1=>array('id'=>'new')));
		foreach ($filters_options as $k=>$linie) {
			?>
			<div class="opt_value"><span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<input type="hidden" name="order_2[]" value="<?=$linie['id']?>">
				<input type="text" name="value[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" style="width:60%;" value="<?=$linie['name']?>" align="absmiddle"  />
				<strong>Activ</strong>
				<input type="hidden" class="hid" name="status[<?=$linie['id']?>]<?=($linie['id']=='new')?'[]':''?>" value="<?php echo $linie['status']; ?>"/>
				<input type="checkbox"  value="1" onclick="var $val=0; if(this.checked) $val=1; $(this).parent().find('.hid').val($val);" <?php echo (($linie['status'])==1?'checked="checked"':'')?> style="vertical-align:middle;" />
				<?php if($linie['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="add" />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="rem"  />
			</div>
			<?php
		}
		?>
		</div>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
	?>
		</form>
		<script type="text/javascript">
			$(".drag_all_top").sortable({opacity: 0.7,revert: true,scroll: true ,handle:$(".drag_all_top span")}) ;
		</script>
		<?php
	}
	function save($id){
		global $db;
		
		
		
		$data=$_POST;
		
	
		
		///$data['name_seo']=escapeIlegalChars($data['name']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			$array_insert=array('name'=>fpost('name'),'name_seo'=>escapeIlegalChars(fpost('name'),' '),'id_category'=>3,'order'=>fpost('order'),'is_boxes'=>fpost('is_boxes'));
			if (!empty($id)) {
				$db->qupdate($this->table,$array_insert," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$array_insert['status']=1;
				$exist=$db->fetch_one("SELECT `id` FROM `{$this->table}` WHERE `name`='".fpost('name')."' AND `id_category`=0");
				if (!$exist) {
					$db->insert($this->table,$array_insert);
					$id=$db->insert_id();
					print_alerta('a fost inserat');
				}
			}
			
			//print_a($_POST);
			
			if (is_array($_POST['value']) && !$exist) {
				$all_ids=array();
				foreach ($_POST['order_2'] as $order=>$key) {
					if (is_numeric($key) && $key!='new') {
						$value=$_POST['value'][$key];
						$all_ids[]=$key;
						if (!empty($value)) {
							$exist=$db->fetch_one("SELECT `id` FROM `xp_filters_options`  WHERE `name`='".$value."' AND `id_filter`=".$id." AND `id`<>".$key);
							if (empty($exist)) {
								$status=$_POST['status'][$key];
								$array_insert=array('name'=>$value);
								$array_insert['name_seo']=escapeIlegalChars($array_insert['name'],' ');
								$array_insert['id_filter']=$id;
								$array_insert['order']=$order;
								$array_insert['status']=$status;
								$db->update('xp_filters_options',$array_insert, " `id`=".$key);

							}
						}
					} else {
						$value=$_POST['value']['new'];
						foreach ($value as $key2=>$value_ins) {
							if (!empty($value_ins)) {
								$exist=$db->fetch_one("SELECT `id` FROM `xp_filters_options`  WHERE `name`='".$value_ins."' AND `id_filter`=".$id);
								if (empty($exist)) {
									$status=$_POST['status']['new'][$key2];
									$array_insert=array('name'=>$value_ins);
									$array_insert['name_seo']=escapeIlegalChars($array_insert['name'],' ');
									$array_insert['id_filter']=$id;
									$array_insert['order']=$order;
									$array_insert['status']=$status;
									$db->insert('xp_filters_options',$array_insert);
									$all_ids[]=$db->insert_id();
								}
							}
						}
					}
				}
				if (empty($all_ids)) $all_ids=array(0);
				$filters=$db->fetch_all("SELECT * FROM `xp_filters_options` WHERE  `id` NOT IN (".implode(",",$all_ids).") AND `id_filter`=".$id." ");
				foreach ($filters as $filter) $db->delete('fibula_hotels_filters_values',"`id_option`=".$filter['id']);
				$db->delete('xp_filters_options'," `id` NOT IN (".implode(",",$all_ids).") AND `id_filter`=".$id." ");
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}

$module=new filters5_module();
if ($module_info) $this_module=$module;

elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) {
		$new_sql="select sql_calc_found_rows
					*
				from
					`{$module->table}` 
					where `id_category`=3";

		json_list($module,false,$new_sql);
}
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>