<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class promotions_module {
	var $module='promotions';
	var $date='15-09-2009';
	var $table=TABLE_PROMOTIONS;
	var $table_b=TABLE_PROMOTIONS_BANNERS;
	var $grid=array();
	var $form=array();
	var $form_b=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function promotions_module() {

		$this->name=l('Promotii');
		$this->title=l('Promotii');
		$this->description=l('Promotii');

		$this->type=array(
		'like'=>array('name','code'),
		'date'=>array('date','last_login'),
		'equal'=>array('type','status')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Name'),'width'=>100),
		'code'=>array('name'=>l('Cod produs'),'width'=>100),
		'type'=>array('name'=>l('Type'),'width'=>120),
		'discount_type'=>array('name'=>l('Tip discount'),'width'=>60),
		'discount'=>array('name'=>l('Discount'),'width'=>80),
		//'currencie'=>array('name'=>l('Moneda'),'width'=>60),
		'date'=>array('name'=>l('Date'),'width'=>90,'align'=>'center'),
		'start_date'=>array('name'=>l('Start date'),'width'=>60,'align'=>'center'),
		'end_date'=>array('name'=>l('End date'),'width'=>60,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>60,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>200,'align'=>'center','sortable'=>false),
		);

		$this->discount_type=array(
		0=>l('Pret discount'),
		//1=>l('Discount in procente'),
		2=>l('Pret fix')
		);

		$this->promotion_type=array(
		0=>l('Categorii'),
		2=>l('Produse')
		);

		$this->form=array(
		0=>l('Adauga/modifica promotie'),
		'name'=>array('type'=>'input','name'=>l('Nume promotie'),'valid'=>'empty,unique,min_4,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'discount_type'=>array('type'=>'radio','options'=>$this->discount_type,'name'=>l('Tip promotie')),
		'discount'=>array('type'=>'input','name'=>l('Discount'),'style'=>'width:140px;'),
		1=>'list_currencie',
		2=>l('Articole promotie'),
		'type'=>array('type'=>'radio','options'=>$this->promotion_type,'name'=>l('Articole din')),
		3=>'list_categ_brand_prod',
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status'))
		);

		$this->form_b=array(
		0=>l('Add edit banner'),
		'name'=>array('type'=>'input','name'=>l('nume'),'lang'=>true,'valid'=>'empty','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		1=>'list_show_on_page',
		'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'banner_promotions'),
		'location'=>array('type'=>'radio','options'=>array(0=>l('Sus'),1=>l('Jos')),'name'=>l('location')),
		'type'=>array('type'=>'radio','options'=>array(1=>l('Fix'),0=>l('carusel')),'name'=>l('type')),
		'order'=>array('type'=>'input','name'=>l('Ordinea')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'content'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		2=>l('Link product'),
		3=>'list_product_banner',
		);

	}
	function css() {
		?>
		dd.checkbox_page label {
			width:120px;
			display:block;
			float:left;
		}
		.list_products_selected > div {
			padding:2px;
		}
		<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		$row['type']=$this->promotion_type[$row['type']];
		$row['discount_type']=$this->discount_type[$row['discount_type']];
		#$row['code']=$db->fetch_one("SELECT `code` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$row['ids']);
		//$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PROMOTIONS_BANNERS."` WHERE `id_promotion`=".$row['id']);
//		if (!empty($exist)) {
//			$text_button=l ( 'Editeaza banner' ) ;
//		} else {
//			$text_button=l ( 'Creaza banner' ) ;
//		}
//		$row['actions']='<a class="action browse" href="javascript: edit_promotions_banner('.$row['id'].');">' .$text_button . '</a>  &nbsp;&nbsp; '.$row['actions'];
		$row['currencie']=$db->fetch_one("SELECT `code` FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['currencie']);
		$row['start_date']=date('d-m-Y',$row['start_date']);
		$row['end_date']=date('d-m-Y',$row['end_date']);
		return $row;
	}
	function js() {
		$module=$this->module;
		?>
		var global_window=true;
		datepicker_add=", #gs_start_date, #gs_end_date";
		function do_product_filter(id,div) {
			var data="id_category="+$("#id_category").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
					//	$("#list_products_dd select").toChecklist();
					}
				});
		}

		function edit_promotions_banner(id) {
			var module='<?=$this->module?>';
			var window_add_edit_name='<?=l('add edit banner')?>';
			$("#window_" + module + '_edit').html('');
			nss_win(module + '_edit', window_add_edit_name, 'module/' + module
			+ '.php?action=new_b&id=' + id, window_width, function() {
				$("#window_" + module + "_edit form").bind('submit', function() {
					return false;
				});
				after_window_load(module, 'edit');
			}, true);
		}
		function select_product(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected').append('<div ><input type="hidden" name="products[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}
		<?php
		set_grid($this,array('multiselect'=>true,'sortorder'=>'desc'));
	}

	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$db->delete(TABLE_PROMOTIONS_BANNERS," `id_product`=".$id);
		}
	}
	function list_show_on_page($date) {
		$showOnPages=explode(",",$date['show_on_page']);
		?>
		<dl>
			<dt>
				<label for="promotions_date"><?=l('Show on page')?></label>
			</dt>
			<dd class="checkbox_page ui-helper-clearfix">
				<label><input type="checkbox" name="show_on_page[]" value="0" <?=(in_array('0',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Home page')?></label>
				<label><input type="checkbox" name="show_on_page[]" value="1" <?=(in_array('1',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Category page')?></label>
				<label><input type="checkbox" name="show_on_page[]" value="2" <?=(in_array('2',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Products page')?></label>
				<br />
				<label><input type="checkbox" name="show_on_page[]" value="3" <?=(in_array('3',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Product page')?></label>
				<label><input type="checkbox" name="show_on_page[]" value="4" <?=(in_array('4',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Brand page')?></label>
				<label><input type="checkbox" name="show_on_page[]" value="5" <?=(in_array('5',$showOnPages)?'checked="checked"':'')?> style="vertical-align:middle" /> <?=l('Search page')?></label>
			</dd>
		</dl>
		<?php
	}
	function list_currencie($date) {
		global $db;
		?>
	<!--	<dl>
			<dt>
				<label for="promotions_currencie"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="currencie" id="promotions_currencie" style="width:142px;">
					<?php
					$currencies=$db->fetch_all("SELECT * FROM `".TABLE_CURRENCIES."`");
					 foreach ($currencies as $currencie) { ?>
						<option value="<?=$currencie['id']?>" <?=(($date['currencie']==$currencie['id'])?'selected':'')?>><?=$currencie['name']?></option>
					<?php } ?>
				</select>
			</dd>
		</dl>-->
		<dl>
			<dt>
				<label for="promotions_date"><?=l('Valabilitate')?></label>
			</dt>
			<dd>
				<input type="text" name="start_date" class="datepicker" size="12" value="<?=date(DATE_FORMAT,$date['start_date'])?>" /> -
				<input type="text" name="end_date" class="datepicker" size="12" value="<?=date(DATE_FORMAT,$date['end_date'])?>"  />
			</dd>
		</dl>
		<?php
	}
	function list_product_banner($date) {
		global $db;
		//print_a($date['id_product']);
		if (isset($date['id_product'])) $date['ids']=explode(",",$date['id_product']);
		?>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="id_category"><?=l('Categoria')?></label>
			</dt>
				<select name="id_category" id="id_category" style="width:460px;" onchange="do_product_filter();">
					<option value="0"><?=l('Toate')?></option>
					<?php $this->json_categories() ?>
				</select>
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Numele produsului sau cod')?></label>
			</dt>
			<dd><input type="text" name="product_name" id="product_name" style="width:460px;" onkeyup="do_product_filter();" />
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Products')?></label>
			</dt>
			<dd id="list_products_dd">
				<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product(this);">
					<?php $this->json_products(); ?>
				</select>

			</dd>
		</dl>
		<div class="list_products_selected all_options_type options_type_2">
				<?php
				if ($date['type']==2) {
					foreach ($date['ids'] as $id_now) {

						if (!empty($id_now)) {
							$sql="select
										p.*,
										pd.*
									from
								`".TABLE_PRODUCTS."` p

								inner join `".TABLE_PRODUCTS."_data` pd
									on p.`id`=pd.`id_product`
							
								WHERE p.`id`=".$id_now;
							$product=$db->fetch($sql);

							?><div ><input type="hidden" name="products[]" value="<?=$product['id']?>" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b><?=$product['name']?> - <?=$product['code']?></b></div><?php
						}
					}
				}
				?>
				</div>
		<script type="text/javascript">
		$(document).ready(function() {
			//$('#categories, #brands, #list_products_dd select').toChecklist();
			$('#categories').toChecklist();

		});
		</script>
		<?php
	}
	function list_categ_brand_prod($date) {
		global $db;
		$date['ids']=explode(",",$date['ids']);
		?>
		<dl class="all_options_type options_type_0">
			<dt>
				<label for="categories"><?=l('Categoria')?></label>
			</dt>
			<dd>
				<select name="categories"  multiple="multiple"  size="12"  id="categories" style="width:460px;">
				<?php $this->json_categories($date['ids']) ?>
				</select>

			</dd>
		</dl>
		<?php $this->list_product_banner($date); ?>
		<script type="text/javascript">
		var reload_free_shipping=function() {
			var var_name = $("input[name='type']:checked").val();
			$(".all_options_type").hide();
			$(".options_type_"+var_name).show();
		}
		$('#type').click(reload_free_shipping);
		$(document).ready(function() {
			reload_free_shipping();
		});
		</script>
		<?php
	}
	function json_categories($ids=array(),$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		//print_a($categories);
		foreach ($categories as $linie) {
			?>
			<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp;  &nbsp; ";
			?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($ids,$linie['id'],$deep+1);
		}
	}
	function json_product_filter() {
		?>
		<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product(this);">
			<?php $this->json_products(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_products($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		if (!empty($id_category)) $sql_filters[]=" p.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$id_category})";
		if (!empty($product_name)) $sql_filters[]=" ( pd.`name` LIKE  '%".$product_name."%' OR pd.`code` LIKE  '%".$product_name."%' ) ";
		$sql="select		p.*,
							pd.*,
							c.`id_category`

						from
							`".TABLE_PRODUCTS."` p

							inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`

							inner join `".TABLE_PRODUCTS."_to_categories` c
								on p.`id`=c.`id_product`
							where  `status`='1' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")."   ORDER BY pd.`name` ASC";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
				$i++;
				if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
				}
			?>
			<option value="<?=$linie['id']?>" <?=((in_array($linie['id'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function new_b($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		$exist=$db->fetch_one("SELECT `id` FROM `".$this->table_b."` WHERE `id_promotion`=".$id);
		if (!empty($exist)) $date_saved=lang_fetch($this->table_b," `id_promotion`=".$id );

		?>
		<form action="?mod=<?=$this->module?>&action=save_b&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form_b,$this,$date_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_b($id){
		global $db;
		$data=$_POST;
		$data['id_product']=implode(",",(array)$data['products']);
		$data['show_on_page']=implode(",",(array)$data['show_on_page']);
		$data['id_promotion']=$id;
		unset($data['id_category']);
		unset($data['product_name']);
		unset($data['products']);
		unset($data['products_2']);

		//		print_a($data);

		$exist=$db->fetch_one("SELECT `id` FROM `".$this->table_b."` WHERE `id_promotion`=".$id);
		if (empty($exist)) $id=0;
		$errors=form_validation($data,$this->form_b,$this->table_b,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				lang_update($this->table_b, $data," `id_promotion`=".$id);
				print_alerta('a fost updatat');
			} else {
				lang_insert($this->table_b,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form_b);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT `start_date`,`end_date`,`ids`,`type`,`name`,`discount_type`,`discount`,`currencie`,`date`,`status` FROM `".$this->table."` WHERE id=".$id);
		if ($date_saved['start_date']<=0) $date_saved['start_date']=time();
		if ($date_saved['end_date']<=0) $date_saved['end_date']=time()+(60*60*24*7);
		//print_a($date_saved);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		if ($data['type']==0) { $data['ids']=implode(",",(array)$data['categories']); }
		if ($data['type']==1) { $data['ids']=implode(",",(array)$data['brands']); }
		if ($data['type']==2) { $data['ids']=implode(",",(array)$data['products']); }
		unset($data['id_category']);
		unset($data['product_name']);
		unset($data['brands']);
		unset($data['categories']);
		unset($data['products']);
		unset($data['products_2']);
		$data['start_date']=strtotime ( $data['start_date'] );
		$data['end_date']=strtotime ( $data['end_date'] );
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);

				calculate_promo();

				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);

				calculate_promo();

				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function json_list() {
		$new_sql="select 
					*			
				from 
					`{$this->table}` 
					where `type`<>3";
		json_list($this,false,$new_sql);
	}
}
$module=new promotions_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='new_b') $module->new_b(fget('id'));
elseif ($_GET['action']=='save_b') $module->save_b(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>