<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");

class countries2_module {
	var $module='countries2';
	var $date='27-08-2009';
	var $table='fibula_countries';
	var $folder='categories_images';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function countries2_module() {

		$this->name=l('Tari - regiuni');
		$this->title=l('Tari - regiuni');
		$this->description=l('Tari - regiuni');

		$this->type=array(
		'like'=>array('name','id'),
		'equal'=>array('status')
		);
		$this->folder='imagini-categorii';

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Titlu'),'valid'=>'empty,min_1,max_200','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),

		'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;'),

		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),

		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere')),
		1=>'show_categories',

		);

		$this->form_update=array(
		'name'=>array('type'=>'input','name'=>l('Titlu'),'valid'=>'empty,min_1,max_200','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),

		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),

		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere')),
		//1=>'show_categories',


		);
	}
	function return_images($data) {
		return fa("SELECT * FROM `ev_category_image` WHERE `id_category`=".(int)$data['id_category']." ORDER BY `id` ASC");
	}
	function css() {}

	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;

		//$categories=$db->fetch_all("select *,ifnull(id_parent,0) id_parent from `ev_category`,`ev_category_data` where `ev_category`.`id_category`=`ev_category_data`.`id_category`  and ".($id_parent==0?'isnull(`id_parent`)':$id_parent)." and `status`=1 ORDER BY `order`,`name`");
		if ($id_parent == 0 && $deep == 0) {
			$id_id = 'isnull(';
		} else {
            $id_id = ($nr ? 'c.id =' : 'c.id_parent =');
        }

		$categories = $db->fetch_all("
			select cd.*,c.*,ifnull(id_parent,0) id_parent
			from
				`fibula_countries` c
				inner join `fibula_countries_data` cd
					on c.`id` = cd.`_id`
			where c.`status`=1 and
				{$id_id} ".($id_parent==0 && $deep==0?'`id_parent`)':$id_parent)."
			ORDER BY cd.`name` ASC
		");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected':''?> style="padding-left:<?=($deep*20)?>px">
			<?php
//			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Tari / Regiuni')?></label>
			</dt>
			<dd>
				<select name="id_parent" id="id_parent" style="width:200px">
				<option value="NULL"><?=l('Niciun parinte')?></option>
				<?=$this->json_categories($date['id_parent'])?>
				</select>
			</dd>
		</dl>
		<?
	}
	function js() {}
	function print_records() {
	?>
	<h2><?=l('Tari / Regiuni')?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="nss_grid">
		<table cellspacing="0" cellpadding="0" border="0"  >
		<thead>
			<tr role="rowheader" class="ui-jqgrid-labels">
				<th><?=l('nume')?></th>
				<th style="width: 60px;"><?=l('vizibil')?></th>
				<th style="width: 140px;"><?=l('Actions')?></th>
			</tr>
		</thead>
		</table>
	</div>
	<div id="nss_list_record">
	<?php  $this->reg_get_categs((int)r('cid')); ?>
	</div>
	<br />
	<?php
	}

	function reg_get_categs($id_parent=0,$content="",$nr=0) {
		global $db;

		if ($nr>8) return false;

		if ($id_parent == 0 && $nr == 0) {
			$id_id = 'isnull(';
		} else {
            $id_id = ($nr ? 'c.id =' : 'c.id_parent =');
        }

		$categories = $db->fetch_all("
			select cd.*,c.*,ifnull(id_parent,0) id_parent
			from
				`".$this->table."` c
				inner join `".$this->table."_data` cd
					on c.`id` = cd.`_id`
			where
				{$id_id} ".($id_parent==0 && $nr==0?'`id_parent`)':$id_parent)."
			ORDER BY cd.`name` ASC
		");

        ob_start();
		if (empty($categories)) {
			?>
			<ul style="list-style-type:none;	margin:0;		padding:0;" class="connectedSortable">

			<?php
			?>
				<li id="ele-0">
						<div style="padding:2px; ">
						<a href="?mod=<?=$this->module?>&cid=0" onclick="add_all_url['<?=$this->module?>']='&cid=0'; reload_list_record('<?=$this->module?>'); return false; ">
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l('Tari / Regiuni')?></span>
						</a>
					</div>
				</li>
			<?=$content?>
			</ul>
			<?php
		}

		else {
			$exist_p=fo("SELECT `id` FROM `".$this->table."` WHERE `id`=".(int)$categories[0]['id_parent']);
			?>
			<ul  style="list-style-type:none;	margin:0;	 padding:0; <?php if ($exist_p) { ?>margin-left:20px; <?php } ?> " <?php if ($nr==0)  { ?> class="list_sort" <?php } ?> >
			<?php
			$i=0;
			foreach ($categories as $category) {
				$i++;
				//$nr_products=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_ADS."` WHERE `id_category`=".$category['id']);
				//$nr_products=product_count(','.$category['id_main'].categ_children($category['id_main']));
				if ($category ['status'] == 1) {
					$category ['status'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['_id'] . '" onclick="ajax_ch_status(\''.$this->module.'\',this);" />';
				} else {
					$category ['status'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['_id'] . '"  onclick="ajax_ch_status(\''.$this->module.'\',this);"  />';
				}

				if ($category ['is_criteria_level'] == 1) {
					$category ['is_criteria_level'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['_id'] . '" onclick="ajax_ch_status2(\''.$this->module.'\',this);" />';
				} else {
					$category ['is_criteria_level'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['_id'] . '"  onclick="ajax_ch_status2(\''.$this->module.'\',this);"  />';
				}

				$exist=fo("SELECT `id` FROM `".$this->table."` WHERE `id_parent`=".$category['id']); ?>
					<li id="ele-<?=$category['id']?>">
						<div style="float:left;">
						<?php if ($nr==0 /*&& $i!=sizeof($categories)*/)  {
							?>
							<img src="images/icons/branch.gif" style="vertical-align:middle;" />
							<?php
						} else { ?>
						<img src="images/icons/branch2.gif" style="vertical-align:middle;" />
						<?php  } if (!empty($exist)) { ?>
						<a href="?mod=<?=$this->module?>&cid=<?=$category['_id']?>" onclick="add_all_url['<?=$this->module?>']='&cid=<?=$category['_id']?>'; reload_list_record('<?=$this->module?>'); return false; ">
						<?php } ?>
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=$category['name']?></span>
						<?php if (!empty($exist)) { ?>
						</a>
						<?php } ?>
						</div>
						<div style="width: 140px;float:right; padding:2px; padding-top:3px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="delete_record(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 60px;float:right; padding:1px 2px; text-align:center;"><?=$category['status']?></div>

						<!--<div style="width: 60px;float:right; padding:3px 2px; text-align:center;"><?=$nr_products?></div>-->
						<div class="clear"></div>
				</li>
				<?php

				$id_parent=$category['id_parent'];
			}

			echo $content;
			?>
			</ul>
		    <?php
		    $file_data = ob_get_contents();
		    ob_end_clean();

		    $this->reg_get_categs($id_parent,$file_data,$nr+1);
		}
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fpost('id'));
		function delete_recursive($id,$table) {
			if(!empty($id)) {
				$childs=$db->fetch_all("SELECT * FROM `".$table."` WHERE id_parent=".$id);
				if (empty($childs)) {
					$db->delete($table," id=".$id);
					$db->delete($table.TABLE_EXTEND," id_main=".$id);
				} else {
					foreach ($childs as $child) {
						delete_recursive($child['id'],$table);
					}
				}
			} else return false;
		}
		foreach ($ids as $id) delete_recursive($id,$this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;

		if (!empty($id)) {
			//$idmain=get_primary_id($this->table);
//			$date_saved=$db->fetch("SELECT *
//				FROM `" . $this->table . "` t
//				 inner join `" . $this->table . TABLE_EXTEND . "` td
//				 	on t.`{$idmain}`=td.`{$idmain}`
//				 	 WHERE t.`{$idmain}`=".$id);

		$date_saved = $db->fetch("
				select
					p.*,
					pd.*
				from
					`fibula_countries` p
					inner join `fibula_countries_data` pd
								on p.`id`=pd.`_id`


				where p.`id`={$id}
			");
		}



		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica categorie'));


		print_form($this->form,$this,$date_saved);

		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;



		$errors=form_validation($data,$this->form,$this->table,$id);
		if ($id==0) {
			$data['code']='mondial-' . escapeIlegalChars($data['name_seo'],"-");
		}
// 		$data2['image']=$data['image'];


		//$data['id_parent'] = ($data['id_parent'] ? $data['id_parent'] : 'NULL');
		$data2['name'] = $data['name'];
		$data2['lang'] = 'ro';
// 		$data2['type']=$data['type'];
// 		$data2['header_title']=$data['header_title'];
// 		$data2['meta_keywords']=$data['meta_keywords'];
// 		$data2['meta_description']=$data['meta_description'];
		if (isset($data['description'])) $data2['description']=$data['description'];
		unset($data['name']);
		unset($data['type']);
		unset($data['header_title']);
		unset($data['meta_keywords']);
		unset($data['meta_description']);
		unset($data['name_seo']);
		unset($data['image']);
		unset($data['description']);

		if ($data['id_parent']=='NULL') {
			unset($data['id_parent']);
		}

		if(empty($errors)) {

			close_window($this->module);
			if (!empty($id)) {
				$data2['name_seo']=escapeIlegalChars($data2['name'], " ");
				$db->update($this->table, $data," `id`=".$id);
				$db->update($this->table.'_data', $data2," `_id`=".$id);
				print_alerta('a fost updatat');
			} else {

				$db->insert($this->table,$data);
				$id=mysql_insert_id();
				$data2['_id']=$id;
				$db->insert($this->table.'_data',$data2);
				print_alerta('a fost inserat');
			}

			?>
			<script type="text/javascript">
			reload_list_record('<?=$this->module?>');
			</script>
			<?
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function save_order() {
		global_save_order2($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
}
$module=new countries2_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->reg_get_categs((int)r('cid'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>