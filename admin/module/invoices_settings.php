<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class invoices_settings_module {
	var	$module='invoices_settings';
	var $date='15-09-2009';
	var $table=TABLE_INVOICES_SETTINGS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function invoices_settings_module() {
		
		$this->name=l('invoices_settings');
		$this->title=l('invoices_settings');
		$this->description=l('invoices_settings');
		
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('name'),'valid'=>'empty,min_3,max_200','style'=>'width:92%;','valid'=>'empty'),
		'address'=>array('type'=>'input','name'=>l('address'),'style'=>'width:92%;','valid'=>'empty'),
		'city'=>array('type'=>'input','name'=>l('city'),'style'=>'width:60%;','valid'=>'empty'),
		'country'=>array('type'=>'input','name'=>l('country'),'style'=>'width:60%;','valid'=>'empty'),
		'state'=>array('type'=>'input','name'=>l('state'),'style'=>'width:60%;','valid'=>'empty'),
		'zip_code'=>array('type'=>'input','name'=>l('zip code'),'style'=>'width:60%;','valid'=>'empty'),
		'nr_reg_com'=>array('type'=>'input','name'=>l('nr reg com'),'style'=>'width:60%;','valid'=>'empty'),
		'cui'=>array('type'=>'input','name'=>l('cui'),'style'=>'width:60%;','valid'=>'empty'),
		'bank'=>array('type'=>'input','name'=>l('bank'),'style'=>'width:60%;','valid'=>'empty'),
		'account'=>array('type'=>'input','name'=>l('account'),'style'=>'width:60%;','valid'=>'empty'),
		'email'=>array('type'=>'input','name'=>l('email'),'style'=>'width:60%;','valid'=>'empty'),
		'web'=>array('type'=>'input','name'=>l('web'),'style'=>'width:60%;','valid'=>'empty'),
		'phone'=>array('type'=>'input','name'=>l('phone'),'style'=>'width:60%;','valid'=>'empty'),
		'fax'=>array('type'=>'input','name'=>l('fax'),'style'=>'width:60%;','valid'=>'empty'),
		'no_invoice'=>array('type'=>'input','name'=>l('no invoice'),'style'=>'width:10%;','valid'=>'empty,numeric','text'=>l('Numarul de start la facturi')),
		2=>l('Imagini'),
		'logo_small'=>array('type'=>'image','name'=>l('Imagine 1'),'folder'=>'invoice_images'),
		'logo_big'=>array('type'=>'image','name'=>l('Imagine 2'),'folder'=>'invoice_images'),
		);
	}
	function css() {}
	function js() {
		?>
		$(document).ready(function() {
			after_window_load('<?=$this->module?>');
		});
		
		<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` LIMIT 1");
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Edit Invoice settings'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new invoices_settings_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	?>
	<h2><?=$module->title?></h2>
	<?php
	$module->new_a(1);
	print_footer();
}
?>