<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class products_module {
	var $module='products';
	var $date='27-08-2009';
	var $table=TABLE_PRODUCTS;
	var $folder='imagini-produse';
	var $option_info;
	var $value_all_info;
	var $all_variations=array();
	var $all_variations_name=array();
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function products_module() {

		$this->name=l('Produse');
		$this->title=l('Produse');
		$this->description=l('Produse');

		$this->folder='imagini-produse/';
		$this->folder3='imagini-produse';
		$this->folder2='static/fisiere-produse';

		$this->type=array(
		'like'=>array('name','id','code','promo','views','price'),
		'date'=>array('date'),
		'equal'=>array('status','id_category','id_brand','stock')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Image 1'),'width'=>50,'align'=>'center','stype'=>'none'),
		'name'=>array('name'=>l('Nume produs'),'width'=>200),
		//'description'=>array('name'=>l('Descriere'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie produs'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		//'id_model'=>array('name'=>l('Marca/Model'),'width'=>80,'align'=>'center','stype'=>'select','options'=>'json_brands_filter'),
		//'id_motorizare'=>array('name'=>l('Motorizare'),'width'=>60,'align'=>'center','stype'=>'select','options'=>'json_brands_filter'),
		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>80,'align'=>'center','stype'=>'select'),
		'code'=>array('name'=>l('Cod'),'width'=>60),
		'price'=>array('name'=>l('Pret'),'width'=>50,'align'=>'right'),
		'currency'=>array('name'=>l('Moneda'),'width'=>50,'align'=>'left','stype'=>'select','options'=>'show_select_currencies'),
		//'views'=>array('name'=>l('Views'),'width'=>40,'align'=>'center'),
		//'stock'=>array('name'=>l('Stoc'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'af_home'=>array('name'=>l('Home'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'is_promo'=>array('name'=>l('Promotia zilei'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'cadou_nume'=>array('name'=>l('Nume cadou'),'width'=>70,'align'=>'center'),
		'date'=>array('name'=>l('Date'),'width'=>80,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Detalii produs'),
		//2=>l('Produse asemanatoare'),
		//3=>l('Campuri configurabile'),
		//4=>l('Valori filtre'),
		//5=>l('Variatii produs'),
		//6=>l('Reguli discount')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,min_3','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'SEO!!!!'),
		'link'=>array('type'=>'input','name'=>l('Link produs'),'style'=>'width:92%;'),
		'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'style'=>'width:92%;height:150px;'),
		'description'=>array('type'=>'editor','name'=>l('Descriere')),
		//'filenamepdf'=>array('type'=>'input','name'=>l('Titlu fisier'),'style'=>'width:92%;'),
	//	'file'=>array('type'=>'file','name'=>l('Fisier'),'folder'=>$this->folder2),
		1=>l('Alte detalii'),
		2=>'show_furnizori',
		3=>'show_brands',
		4=>'show_categories',
		5=>'show_currencies',
		'price'=>array('type'=>'input','name'=>l('Pret'),'info'=>l('Pret')),
		'code'=>array('type'=>'input','name'=>l('Cod'),'info'=>l('Code')),
		//'stock'=>array('type'=>'input','name'=>l('Nivelul de stoc')),
	//	'minimum_quantity'=>array('type'=>'input','name'=>l('Cantitate minima')),
		//'length'=>array('type'=>'input','name'=>l('Lungime')),
		//'width'=>array('type'=>'input','name'=>l('Latime')),
		//'height'=>array('type'=>'input','name'=>l('Inaltime')),
		//'weight'=>array('type'=>'input','name'=>l('Greutate')),
		//'rim_diameter'=>array('type'=>'input','name'=>l('Diametru')),
		//'rim_width'=>array('type'=>'input','name'=>l('Latime')),
		//'rim_studs'=>array('type'=>'input','name'=>l('Prezoane')),
		//'warranty'=>array('type'=>'input','name'=>l('Garantie')),
		//		'low_stock'=>array('type'=>'input','name'=>l('Nivel mic de stoc'),'valid'=>'numeric','info'=>l('Nivelul stocului')),
		//		'rating'=>array('type'=>'input','name'=>l('rating'),'valid'=>'empty','info'=>l('rating')),
		//		'votes'=>array('type'=>'input','name'=>l('votes'),'valid'=>'numeric','info'=>l('votes')),
		//'weight'=>array('type'=>'input','name'=>l('Greutate'),'info'=>l('Greutate produs')),
		//'tax_price'=>array('type'=>'input','name'=>l('Taxa livrare'),'info'=>l('Taxa livrare')),
		//'is_homepage'=>array('type'=>'radio','name'=>l('Vizibil pe homepage'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'package_type'=>array('type'=>'radio','name'=>l('Tip pachet'),'options'=>array(0=>l('Punguta'),1=>l('Sir'))),
		//'package_quantity'=>array('type'=>'input','name'=>l('Cantitate pachet')),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		15=>l('Imagini'),
		'images'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder,'multiple'=>true,'images'=>'return_images'),
		//'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:32%;'),
		//	15=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'products_images','text'=>'<img src="images/addicon.gif" align="absmiddle" class="add" class="pointer" /> 		 	<img src="images/delicon.gif" align="absmiddle" class="rem" class="pointer" />'),
		//		16=>'product_images',
		//	16=>'show_uploader2',
		//	17=>array('tab'=>2),
		//'related_products_checkbox'=>array('type'=>'checkbox','name'=>l('Produse asemanatoare'),'text'=>l('Gaseste si arata automat produse asemanatoare'),'info'=>l('Bifati aceasta casuta si o lista cu produse asemanatoare va fi aleasa si afisata in magazinul dvs. Debifati casuta pentru a nu arata produsele asemanatoare sau pentru a alege dumneavoastra produsele asemanatoare, folosind instructiunile:1. Alegeti o categorie si produsele din acea categorie vor fi afisate; 2. Faceti dublu-click pe un produs pentru a-l adauga ca produs asemanator; 3. Pentru a sterge un produs din lista de produse asemanatoare, faceti dublu-click pe el in josul boxului')),
		//18=>'list_related_products',
		//	20=>array('tab'=>3),
		//	21=>'list_specifications',
		//	25=>array('tab'=>4),
		//	26=>'list_filters',
		//30=>array('tab'=>5),
		//'have_variations'=>array('type'=>'radio','name'=>l('status'),'options'=>array(0=>l('Will not have any variations in my_store'),1=>l('Will use a product variation created '))),
		//32=>'list_variations_tab',
		/*		40=>array('tab'=>6),
		50=>'list_discount_rules',*/
		);
	}
	function return_images($data) {
		return fa("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$data['id']." ORDER BY `id` DESC");
	}
	function show_select_currencies() {
		$_GET['from_grid']=1;
		$this->json_currencies();
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		#uploaded_images img{
			cursor:move;
		}
		.module_menu li.update_all {
			background-image:url('../../images/icons/products_update_all.png');
		}
		.module_menu li.price_all {
			background-image:url('../../images/icons/products_update_all.png');
		}
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		#wrapper{width:110px;height:30px;position:relative;}
		#wrapper .button{cursor:pointer;display:block;height:30px;width:110px;background:transparent url('../../images/browse.png') no-repeat;border:0;}
		#wrapper .button:hover,#wrapper .button:focus{outline:none;}
		#files ol{cursor:auto;list-style:none;marker-offset:auto;position:relative;padding:10px 0 0 0;}
		#files li{padding:90px 0 0 0;position:relative;width:110px;float:left;font-size:11px;text-shadow:0 0 0;margin-right:8px;text-align:center;color:#5196ea;}
		#files span{cursor:pointer;color:red;font-size:12px;font-weight:bold;text-decoration:underline;}
		em {color:#000;font-size:12px;font-weight:bold;}
		#files li .img{position:absolute;top:0;left:0;width:110px;height:88px;border:1px solid #ccc;overflow:hidden;}
		#files p{margin:7px 0;}
		#files em{font:normal 11px "Tahoma";}
		body{position:relative;}

		.expanding{display:none;padding:5px 0 0 15px;margin-bottom:10px;border-left:1px solid #333;}
		#main-expander{list-style:none;}
		<?php
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function list_specifications($date) {
		?>
		<div id="list_specifications">
		<?php $this->json_list_specifications($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function json_list_specifications($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
			print_form_header ($filter['name']);
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $specification_info=$db->fetch("SELECT `value`,`front` FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".$id." AND `id_option`=".$option['id']);
		?>
		<dl>
		<dt>
			<label for="products_specification_<?=$option['id']?>"><?=$option['name']?></label>
		</dt>
		<dd>
			<input type="checkbox" name="specifications_show[<?=$option['id']?>]" value="1" align="absmiddle"  <?=(($specification_info['front']==1)?'checked':'')?> />
			<input type="text"  style="width:60%;" name="specifications[<?=$option['id']?>]" value="<?=$specification_info['value']?>" align="absmiddle" />

		</dd>
		</dl>
		<?php
			}
			print_form_footer();
		}
	}
	function list_filters($date) {
		?>
		<div id="list_filters">
		<?php $this->json_list_filter($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function json_list_filter($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label>
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." AND `id_product`=".$id." AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function list_variations_tab($date) {
		global $db;
		$all_variations=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_NAME."` WHERE status=1 ORDER BY `name` ASC");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Variation')?></label>
			</dt>
			<dd>
			<select name="id_variation" onchange="list_variations_options(this.value,<?=(int)$date['id']?>);">
			<option value="0"> <?=l('Choose a variation')?> </option>
				<?php foreach ($all_variations as $variation) { ?>
				<option value="<?=$variation['id']?>" <?=($date['id_variation']==$variation['id']?'selected':'')?>><?=ehtml($variation['name'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<br clear="all" />
		<div id="list_variations_options" class="product_have_variations">
		<?php
		if (!empty($date['id_variation'])) {
			$this->list_variations_options($date['id_variation'],$date['id']);
		}
		?>
		</div>
		<?php
	}
	function get_rec($j=0) {
		$variation='';
		$variation_name='';
		$k=0;
		$max=sizeof($this->option_info);
		foreach ($this->option_info as $option) {
			$k++;
			foreach ($this->value_all_info[$option['id']] as $value) {
				$variation_temp=$variation.$value['id'];
				if ($k<$max) $variation_temp.="-";
				if ((!in_array($variation_temp,$this->all_variations))) {
					$variation.=$value['id'];
					$variation_name.=$value['name'];
					if ($k<$max) {
						$variation.="-";
						$variation_name.=" - ";
					}
					$array_variation=explode("-",$variation);
					$end_array_variation=end($array_variation);
					if ((sizeof($array_variation)==$max ) && (!empty($end_array_variation))) {
						$this->all_variations_name[$j]=$variation_name;
					}
					$this->all_variations[$j]=$variation;
					break;
				}
			}
		}
		if (!empty($this->all_variations[$j])) $this->get_rec($j+1);
	}
	function list_variations_options($id_variation,$id_product) {
		global $db;
		if (!empty($id_variation)) {
			/*	$this->option_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`='".$id_variation."' ORDER BY `order`  ASC");

			$this->value_all_info=array();
			foreach ($this->option_info as $option) {
			$value_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`='".$option['id']."'  ORDER BY `order` ASC");
			$this->value_all_info[$option['id']]=$value_info;
			}
			$this->get_rec();
			$product_info_variations=array();
			if (!empty($id_product)) {
			$resursa=$db->sql("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product));
			while ($product_variations=$db->fetch($resursa)) {
			$product_info_variations[str_replace("-","_",$product_variations['variation'])]=$product_variations;
			}
			}*/

			$all_var=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product)." ORDER BY `order`");
			$all_var=array_merge($all_var,array(0=>array('id'=>'new')));

?>
<!--<input type="hidden" name="variations[nss_all_variations]" id="nss_all_variations"  />-->
<div style="width:856px;">
		<table cellpadding="0" cellspacing="0" border="0"  id="table_variations" >
			<thead>
			<tr>
				<th style="width:20px"><input type="checkbox" name="aaaaaa" /></th>
				<th><?=Variation_name?></th>
				<th style="width:72px"><?=Order?></th>
				<th style="width:72px"><?=Cod?></th>
		<!--		<th style="width:72px"><?=Stoc?></th>-->
				<th style="width:140px"><?=Pret?></th>
				<th style="width:140px"><?=Greutate?></th>
				<!--<th style="width:200px;"><?=imagine?></th>-->

			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($all_var as $_temp_nw) {
				if($_temp_nw['id']=='new') $after_for_new="[]"; else $after_for_new="";
				$var=$_temp_nw['id'];
			?>
			<tr id="tr_<?=$var?>">
				<td align="center" >
					<input type="checkbox" name="variations[<?=$var?>][variation]<?=$after_for_new?>" value="<?=$var?>" <?=(isset($_temp_nw)?'checked':'')?> />
				</td>
				<td >	<input type="text" name="variations[<?=$var?>][variation_name]<?=$after_for_new?>" size="60" value="<?=$_temp_nw['variation_name']?>" />

					<?php if($_temp_nw['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="pointer" onclick="$(this).parent().parent().clone(true).insertAfter($(this).parent().parent());"  />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="pointer" onclick="$(this).parent().find('input').val('');" />

				</td>
					<td>
					<input type="text" name="variations[<?=$var?>][order]<?=$after_for_new?>" size="4" value="<?=$_temp_nw['order']?>" />
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][code]<?=$after_for_new?>" size="8" value="<?=$_temp_nw['code']?>" />
				</td>
<!--				<td>
					<input type="text" name="variations[<?=$var?>][stock]" size="8" value="<?=$_temp_nw['stock']?>" />
				</td>-->
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][price_type]<?=$after_for_new?>">
					<?php $price_type=$_temp_nw['price_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($price_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($price_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($price_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($price_type==0)?'style="display: none;"':''?>>
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][price]<?=$after_for_new?>" value="<?=$_temp_nw['price']?>" />
					</span>
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][weight_type]<?=$after_for_new?>">
					<?php $weight_type=$_temp_nw['weight_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($weight_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($weight_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($weight_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($weight_type==0)?'style="display: none;"':''?> >
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][weight]<?=$after_for_new?>"  value="<?=$_temp_nw['weight']?>" />
					</span>
				</td>

			</tr>

			<?php
			}
			?>
		</tbody>
		</table>

		</div>

		<!--	<tr id="tr_next_<?=$var?>" class="bodyTr" onmouseover="this.className='bodyTrHover'; $('#tr_<?=$var?>').attr('class','bodyTrHover');" onmouseout="this.className='bodyTr';$('#tr_<?=$var?>').attr('class','bodyTr');">
			<td colspan="7" style="padding-bottom:4px; border-bottom:1px solid #EFE2F0;"><input type="text" style="width:99%" name="short_description[]" value="<?=$_temp_nw['short_description']?>"/></td>
			</tr>-->
<?php }
	}
	function product_images($date) {
		global $db;
		?>
		<div id="uploaded_images" class="ui-helper-clearfix">
				<?php
				if (!empty($date['id'])) {
					$product_images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$date['id']." ORDER by `id` ASC");
					foreach ($product_images as $image) {
						if (!file_exists(SITEROOT."/".$image['image'])) {
							$db->delete(TABLE_PRODUCTS_IMAGES,"`id`=".$image['id']);
							continue;
						}
					?>
					<div style="padding:2px;position:relative;float:left;">
						<input type="hidden" name="images[]" value="<?=ehtml ( $image ['image'] )?>" />
						<img align="absmiddle" src="../?sgd&mode=resize&file=<?=$image['image']?>&args=100x100" height="100" style="border:1px solid #9F9F9F;" />
						<img src="images/icons/file_remove.png" style="cursor:pointer;position:absolute;top:5px;right:4px;" onclick="delete_uploaded('<?=$image['image']?>',this);" />
						<!--<input type="radio" name="image" value="<?=$image['id']?>" <?=(($image['id']==$date['image'])?'checked':'')?> style="cursor:pointer;position:absolute;top:6px;left:6px;"  />-->
					</div>
					<?php
					}
				}
				?>
		</div>
		<input type="file" id="file_upload_images" />
	<?php
	}
	function list_discount_rules($date) {
		global $db;
		if (!empty($date)) $product_discount=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_DISCOUNT_RULES."` WHERE `id_product`=".(int)$date['id']);
		if (empty($product_discount)) $product_discount=array(1);
		foreach ($product_discount as $discount) {
		?>
		<div style="padding:2px;">
			<?=l('Clienti care au cumparat intre')?>
			<input type="text" name="discount[quantity_from][]" value="<?=$discount['quantity_from']?>" size="4" style="text-align:center;" /> -
			<input type="text" name="discount[quantity_to][]" value="<?=$discount['quantity_to']?>" size="4" style="text-align:center;" />
			<?=l('articolele primesc')?>
			<select name="discount[type][]" align="absmiddle" style="width:140px">
				<option value="0" <?=($discount['type']==0?'selected':'')?>><?=l('Pret discount')?></option>
				<option value="1" <?=($discount['type']==1?'selected':'')?>><?=l('Discount in procente')?></option>
				<option value="2" <?=($discount['type']==2?'selected':'')?>><?=l('Pret fix')?></option>
			</select>
			<?=l('discount')?>
			<input type="text" name="discount[discount][]" value="<?=$discount['discount']?>" size="4" style="text-align:center;" />
			<?=l('pentru fiecare articol')?>
			<img src="images/addicon.gif" align="absmiddle" class="add" class="pointer" />
		 	<img src="images/delicon.gif" align="absmiddle" class="rem" class="pointer" />
		</div>
		<?php
		}
	}
	/*function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produs')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}*/
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_brand"><?=l('Brand')?></label>
			</dt>
			<dd>
				<select name="id_brand" id="id_brand" style="width:200px">
				<?php $this->json_brands($date['id_brand']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga producator')?>" onclick="add_brand();" />
			</dd>
		</dl>
		<?php
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$categories=$db->fetch_all("select * from `".TABLE_CATEGORIES."` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function json_brands($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT * FROM `".TABLE_BRANDS."` ORDER by `name` ASC");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_BRANDS."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}

	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT * FROM `".TABLE_CURRENCIES."` ORDER by `name` DESC");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function js() {
		$after='$("#gbox_list_'.$this->module.'").css({"borderRight":"0px none"}).removeClass("ui-corner-all"); ';
		$loadComplete='
			$("#gs_id_category").load("module/'.$this->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
			$("#gs_id_brand").load("module/'.$this->module.'.php?json_brands=1&from_grid=1&id="+$("#gs_id_brand").val());
		';
		set_grid($this,array('multiselect'=>'true','sortname' => 'date','sortorder'=>'desc','loadComplete'=>$loadComplete),'',$after);
?>
$('#main-expander a').live('click',function(){$(this).next().slideToggle();});


var global_window=true;
var window_width=900;
var window_add_edit_name_brand="<?=l('Add Brand')?>";
var window_add_edit_name_category="<?=l('Add Category')?>";
var window_add_edit_name_currency="<?=l('Add Currency')?>";
function do_product_filter(id,div) {
			var data="related_id_category="+$("#related_id_category").val()+"&related_product_name="+$("#related_product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
				$("#list_products_dd select").toChecklist();
}});
}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function list_variations_options(id_var,id){
	$.ajax({
		data :"",
		type :"POST",
		url :'module/<?=$this->module?>.php?list_variations_options=1&id_var='+id_var+'&id='+id,
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#list_variations_options').html(r);
			set_variations_grid('table_variations');


		}});
}

function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function load_brands(){
	$("#id_brand").load('module/<?=$this->module?>.php?json_brands=1');
}
function json_currencies(){
	$("#currency").load('module/<?=$this->module?>.php?json_currencies=1');
}
function add_currency(){
	var module_now='currencies';
	nss_win(module_now + '_new', window_add_edit_name_currency,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',json_currencies);
	});
}
function add_brand(){
	var module_now='brands';
	nss_win(module_now + '_new', window_add_edit_name_brand,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_brands);
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function update_all_specification(id_category){
	$("#update_all_specification").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category);
}
	$(document).ready(function(){
			$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Muta produse')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
			$(".module_menu .price_all").click(function(){
				var window_add_edit_name='<?=l('Update pret produse')?>';
				//var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=price_all',800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
	});

	function set_product_order(order,id) {
			$.ajax( {
				data :"ch_order=1&id="+ id+"&order="+order,
				type :"GET",
				url :'module/<?=$this->module?>.php',
				timeout :45000,
				error : function() {
					console.log("Failed to submit - ");
				},
				success : function(r) {	}
			});
		}

<?php
	}
	function json_product_filter() {
	?>
	<select name="products" multiple="multiple" size="20" style="width:560px" id="products">
	<?php $this->json_products(array(),$_POST['related_id_category'],$_POST['related_product_name']) ?>
	</select>
	<?php
	}
	function json_products($ids=array(),$related_id_category=0,$related_product_name="") {
		global $db;
		$sql_filters=array();
		if (!empty($related_id_category)) $sql_filters[]=" `".TABLE_PRODUCTS."`.`id_category`=".$related_id_category;
		if (!empty($related_product_name)) $sql_filters[]=" `".TABLE_PRODUCTS.TABLE_EXTEND."`.`name` LIKE '%".$related_product_name."%' ";
		$sql="SELECT * FROM `".TABLE_PRODUCTS."`,`".TABLE_PRODUCTS.TABLE_EXTEND."`
			WHERE `".TABLE_PRODUCTS."`.`id`=`".TABLE_PRODUCTS.TABLE_EXTEND."`.`id_main`
			AND `".TABLE_PRODUCTS.TABLE_EXTEND."`.`lang`='".LANG."' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC";
		//echo $sql;
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) { $i++;
		if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
		}
		?>
		<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> > <?=$linie['name']?> </option>
		<?php
		}
	}

	function json_list_row_before($row) {
		//$row['status_comanda']=$row['status'];
		$row['date2']=date('d-m-Y H:i:s',$row['date']);
		return $row;
	}
	function json_list_row($row) {
		$admin_info=adm_info();
		global $gd,$db;
		//$row['name']="<a href=".lang_item_link(TABLE_CATEGORIES,$row)." target='_blank'>".$row['name']."</a>";
		if (!empty($row['id_category'])) $row['id_category']=$db->fetch_one("select `name` from `".TABLE_CATEGORIES."` WHERE `id`={$row['id_category']}");
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Nu exista categoria').'</span>';
		}else{
			foreach($row['id_category'] as$r)
				$all_artists[]=$r['name'];
			$row['id_category']=implode(', ',$all_artists);
		}
		//$row['id_brand']=$db->fetch_one("SELECT `name` FROM `".TABLE_BRANDS."` WHERE `id`=".$row['id_brand']);
		if ($row['id_brand']==false) {
			$row['id_brand']='<span style="color:red;">'.l('Producatorul nu mai exista').'</span>';
		}
		//$row['id_furnizor']=$db->fetch_one("SELECT `name` FROM `".TABLE_FURNIZORI."` WHERE `id`=".$row['id_furnizor']);
		if ($row['id_furnizor']==false) {
			$row['id_furnizor']='<span style="color:red;">'.l('Furnizorul nu mai exista').'</span>';
		}
		if ($row ['af_home'] == 1) {
			$row ['af_home'] = '<img class="ch_staus_img" rel="af_home" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['af_home'] = '<img class="ch_staus_img" rel="af_home" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		if ($row ['is_promo'] == 1) {
			$row ['is_promo'] = '<img class="ch_staus_img" rel="is_promo" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_promo'] = '<img class="ch_staus_img" rel="is_promo" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		$row ['order'] = '<input size="2" name="order['.$row ['id'].']" value="'.$row ['order'].'" style="text-align:center" onchange="set_product_order(this.value,'.$row ['id'].')" />';


		$image=$db->fetch_one("SELECT `image` FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$row['id']." ORDER BY `id` ASC",MYSQL_ASSOC);
		//print_a($image);
		$row['image']='<img src="'._static.'imagini-produse/72/'.$image.'" alt="'.$row['title'].'" />';

		//$number=$row['price'];
		$row['currency']=$db->fetch_one("SELECT `code` FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['id_currency']);
		$row['price']=number_format($row['price'],2,',','.');
		//$row['price']=$price_new;
		//$row['currency']=$currencies['code'];

		return $row;
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".$id);
			foreach ($images as $image) unlink(SITEROOT."/".$image);
			$db->delete(array(TABLE_PRODUCTS_IMAGES)," `id_product`=".$id);
		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$data_saved=$db->fetch("select * from `{$this->table}` where `id`=".$id);
			if (!empty($data_saved['id_variation'])) $data_saved['have_variations']=1;
			if (empty($data_saved['related_products'])) $data_saved['related_products_checkbox']=1;
		}// else $data_saved['order']=$db->fetch_one("SELECT MAX(`order`) FROM `{$this->table}`")+1;
//		if ($data_saved['file']!=''&&file_exists(SITEROOT.'/static/'.$this->folder2.'/'.$data_saved['file'])){
//			$data_saved['file']='static/'.$this->folder2.'/'.$data_saved['file'];
//		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">

		<?php
		print_form($this->form,$this,$data_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">

		var reload_related_products=function() {
			if($('#related_products_checkbox').attr('checked')) {
				$(".related_products_select").hide();
			} else {
				$(".related_products_select").show();
			}
		}
		var reload_variations_product=function() {
			var var_name = $("#have_variations input:checked").val();
			if(var_name==1) {
				$(".product_have_variations").show();
			} else {
				$(".product_have_variations").hide();
			}
		}

		$(document).ready(function(){




			$('#related_products_checkbox').click(reload_related_products);
			$('#have_variations input').click(reload_variations_product);
			reload_related_products();
			reload_variations_product();
			$('#list_products_dd select').toChecklist();
			$("#uploaded_images").sortable({opacity: 0.7}) ;



			$("#file_upload_images").uploadify({
			'uploader':'includes/jquery-plugins/uploadify/uploadify.swf',
			'script':'includes/jquery-plugins/uploadify/uploadify.php',
			'folder':'<?=$this->folder?>',
			'checkScript':'includes/jquery-plugins/uploadify/check.php',
			'auto':true,
			'fileDesc':'<?=l ( 'Sunt acceptate doar imagini' )?>',
			'fileExt':'*.jpg;*.gif;*.jpeg;*.png',
			'sizeLimit':<?=(1024 * 1024 * 2)?>,
			'buttonText':'<?=l('Upload more')?>',
			'onComplete':function(event,queueID,fileObj,response,data){
				$("#uploaded_images").html($("#uploaded_images").html()+"<div style=\"padding:2px;position:relative;float:left;\"><input type=\"hidden\" name=\"images[]\" value=\"<?=$this->folder?>/"+fileObj.name+"\" /><img src=\"../?sgd&mode=resize&file=<?=$this->folder?>/"+fileObj.name+"&args=100x100s-#FFFFFF\" height=\"100\" style=\"border:1px solid #9F9F9F;\" /><img src=\"images/icons/file_remove.png\" style=\"cursor:pointer;position:absolute;top:5px;right:4px;\" onclick=\"delete_uploaded('<?=$this->folder?>/"+fileObj.name+"',this);\" /></div> ");
				return true;
			},
			'cancelImg':'includes/jquery-plugins/uploadify/cancel.png'
			});

		});

		</script>
		<?php
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;
		//$data['file']=str_replace($this->folder2.'/','',upload_file($data,'file',$this));
		/*if ($data['related_products_checkbox']!=1) {
		$data['related_products']=implode(",",(array)$data['products']);
		} else $data['related_products']="";*/
		$data_discount=$data['discount'];
		$data_images=$data['images'];
		$data_filters=$data['filters'];
		$data_variations=$data['variations'];
		$data_specifications=$data['specifications'];
		$data_to_categories=$data['id_category'];
		$data['id_category']=0;
		unset($data['filters'],$data['images'],$data['discount'],$data['related_id_category'],$data['related_product_name'],$data['products'],$data['have_variations'],$data['variations'],$data['specifications'],$data['specifications_show'],$data['related_products_checkbox']);
		//		if (empty($data['weight'])) $data['weight']=$db->fetch_one("SELECT `weight` FROM `".TABLE_CATEGORIES."` WHERE `id`=".$data['id_category']);

		//print_a($data);


		if (empty($id)) $nsid=next_auto_increment($config['SQL_DB'],$this->table);
		else $nsid=$id;

		foreach ($data['nss_languages'] as $lang=>$vars) $data['nss_languages'][$lang]['name_seo']=escapeIlegalChars($data['nss_languages'][$lang]['name']," ");

		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			//if (!isset($data['file'])) $data['file']='';
			//if (!empty($id)) $cond="AND `id_main`<>".$id;
			//$exist=$db->fetch_one("SELECT `code` FROM ".$this->table." WHERE `code`='{$data['code']}' {$cond}");
			if (!$exist) {
				if (!empty($id)) {
					#$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_PRODUCTS_DISCOUNT_RULES,TABLE_FILTERS_VALUES,$this->table.'_to_categories'),"`id_product`=".$id);
					$db->update($this->table, $data," `id`=".$id);
				} else {
					$data['date']=time();
					//$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='{$data['code']}'");
					//if (!$exist) $id=lang_insert($this->table, $data);
					//$id=lang_insert($this->table, $data);
					$db->insert($this->table, $data);
				}
				upload_images($data_images,escapeIlegalChars($data['nss_languages'][LANG]['name_seo'],"-"),'static/'.$this->folder3,'',$id,TABLE_PRODUCTS_IMAGES,'id_product',true);

				if(is_array($data_to_categories))
					foreach($data_to_categories as$c)
						$db->insert($this->table.'_to_categories',array('id_product'=>$id,'id_category'=>$c));

				if (is_array($data_discount)) {
					foreach ($data_discount as $key=>$discount) {
						if (!empty($discount['discount'])) {
							$array_values=array(
							'id_product'=>$id,
							'quantity_from'=>$discount['quantity_from'],
							'quantity_to'=>$discount['quantity_to'],
							'discount'=>$discount['discount'],
							'type'=>$discount['type'],
							);
							$db->insert(TABLE_PRODUCTS_DISCOUNT_RULES,$array_values);
						}
					}
				}
				/*if (is_array($data_specifications)) {
					foreach ($data_specifications as $key=>$value) {
						$array_values=array(
						'id_product'=>$_GET['id'],
						'id_option'=>$key,
						'value'=>$value,
						'front'=>$_POST['specifications_show'][$key],
						);
						$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_values);
					}
				}
				if (is_array($data_filters)) {
					foreach ($data_filters as $key=>$filters) {
						foreach ($filters as $filter) {
							$array_values=array(
							'id_product'=>$_GET['id'],
							'id_filter'=>$key,
							'id_option'=>$filter
							);
							$db->insert(TABLE_FILTERS_VALUES,$array_values);
						}
					}
				}

				if (is_array($data_variations)) {
					foreach ($data_variations as $key=>$variation) {
						$variation['id_product']=$id;
						if (is_numeric($key) && $key!='new') {
							if (!empty($variation['variation_name'])) {
								$variation['variation']=$key;
								$db->update(TABLE_VARIATIONS,$variation,"`id`=".$key);
							} else {
								$db->delete(TABLE_VARIATIONS,"`id`=".$key);
							}
						} else {
							foreach ($variation['variation_name'] as $key2=>$variation_2) {
								if (!empty($variation_2)) {
									$variation_ins=array(
									'id_product'=>$id,
									'variation_name'=>$variation_2,
									'order'=>$variation['order'][$key2],
									'code'=>$variation['code'][$key2],
									'price_type'=>$variation['price_type'][$key2],
									'price'=>$variation['price'][$key2],
									'weight_type'=>$variation['weight_type'][$key2],
									'weight'=>$variation['weight'][$key2],
									);
									$db->insert(TABLE_VARIATIONS,$variation_ins);
									$id_ins=mysql_insert_id();
									$db->update(TABLE_VARIATIONS,array('variation'=>$id_ins),"`id`=".$id_ins);
								}
							}
						}
					}
				}*/

				close_window($this->module,$id);
				print_alerta('a fost inserat/updatat');
			} else print_alerta('pe site mai exista un produs cu acelasi cod produs');
		} else {
			print_form_errors($errors,$form);
		}
	}

	function update_all($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_specification(this.value);">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_all_specification"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function price_all() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=price_save_all" method="POST">
		<?php
		print_form_header (l('Modifica pret procentual'));
		?>
		<dl>
			<dt><label for="price"><?=l('Introdu procent')?></label></dt>
			<dd><input type="text" name="price" value="" /> %</dd>
		</dl>
		<dl>
			<dt>
				<label for="id_category"><?=l('Producator')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
					<option value="all" selected="selected">toate</option>
					<?php $this->json_brands(0) ?>
				</select>
			</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_all(){
		global $db;
		$data_category=$_POST['category'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) foreach ($ids as $key=>$id) $db->update(TABLE_PRODUCTS,array('id_category'=>$data_category),"`id`=$id");
		print_alerta('a fost updatat');
		//		close_window($this->module);
		//		if (is_array($data_specifications)) {
		//			//$db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_product` IN (".implode(",",$ids).")");
		//			foreach ($data_specifications as $key=>$value) {
		//				foreach ($ids as $id) {
		//					$array_values=array(
		//					'front'=>$_POST['specifications_show'][$key],
		//					);
		//					if (!empty($value)) $array_values['value']=$value;
		//					$db->update(TABLE_SPECIFICATIONS_VALUES,$array_values," `id_product`=".$id." AND `id_option`=".$key);
		//				}
		//			}
		//		}
	}
	function price_save_all(){
		global $db;
		if ($_SESSION['last_discount']+10<time()) {
			//print_a($_SESSION['last_discount']=$_POST['price'].$_POST['id_category']);
			if (is_numeric($_POST['price'])) {
				if ($_POST['id_category']!='all') {
					$cond=" AND `id_brand`={$_POST['id_category']}";
					$cond2=" AND `id_product` IN (SELECT `id` FROM `".$this->table."` WHERE `id_brand`={$_POST['id_category']})";
				}
				$db->sql("UPDATE `{$this->table}` SET `price`=CEIL(`price`*".(1+$_POST['price']/100).") WHERE `price`<>0 ".$cond);
				$db->sql("UPDATE `{$this->table}_variations` SET `price`=CEIL(`price`*".(1+$_POST['price']/100).") WHERE `price`<>0 ".$cond2);
			}
			//unset($_SESSION['last_discount']);
			$_SESSION['last_discount']=time();
			print_alerta('a fost updatat');
			close_window($this->module);
		} else {
			print_alerta('Puteti face inca un update de pret peste 10 secunde');
			close_window($this->module);
		}
	}
	function ch_order($id,$order) {
		global $db;
		$product=$db->fetch("SELECT `id_category`,`order` FROM `{$this->table}` WHERE `id`=".(int)$id,MYSQL_ASSOC);
		$id_category=$product['id_category'];
		$order_pozitie=$product['order'];
		if ($order_pozitie>$order) $db->query("UPDATE `".$this->table."` SET `order`=`order`+1 WHERE `order`>={$order} AND `order`<=".$order_pozitie." AND `id_category`={$id_category} AND `order`>0 AND `id`<>".$id);
		else $db->query("UPDATE `".$this->table."` SET `order`=`order`-1 WHERE `order`>={$order_pozitie} AND `order`<=".$order."  AND `id_category`={$id_category} AND `order`>0 AND `id`<>".$id);
		$db->query("UPDATE `".$this->table."` SET `order`={$order} WHERE `id`=".$id);
	}
	function show_uploader1($date) {
		global $db,$gd;
		$ceva=time();
	?>
	<dl>
		<dt>
			<label for="files"><?=l('Imagine articol')?></label>
		</dt>
		<dd id="uploader-1-<?=$ceva?>">
			<div class="files">
				<div class="wrapper"><a class="button">&nbsp;</a></div>
				<ol>
					<?
					if (!empty($date['image'])) {
						if (file_exists(SITEROOT.'/'.$date['image'])) {
							echo "
								<li>
								 	<div class=\"img\" style=\"background:url('".BASEHREF.$gd->url('resize',$date['image'],'110x88s-#eff7e6')."');\"></div>
								 	<span onclick=\"delete_file('".$date['image']."',this)\">".l('Sterge poza')."</span>
									<input type=\"hidden\" name=\"image[]\" value=\"{$date['image']}\"/>
									<input type=\"text\" name=\"text[]\" value=\"{$date['image_text']}\"/>
								</li>
							";
						}
					}
					?>
				</ol>
				<div class="clear"></div>
			</div>
		</dd>
	</dl>
	<script type="text/javascript">
	$(document).ready(function () {activate_uploader('uploader-1-<?=$ceva?>','<?=$this->folder?>',1,'jpg|jpeg|gif|png','image'); });
	</script>
	<?
	}
	function show_uploader2($date) {
		//print_a($date);
		global $db,$gd;
		$ceva=time();
	?>
	<dl>
		<dt>
			<label for="files"><?=l('Imagini produs')?></label>
		</dt>
		<dd id="uploader-2-<?=$ceva?>">
			<div class="files">
				<div class="wrapper"><a class="button">&nbsp;</a></div>
				<ol>
					<?
					if (!empty($date['id'])) {
						$images=$db->fetch_all("SELECT * FROM `{$this->table}_images` WHERE `id_product`={$date['id']} ORDER BY `image` ASC");
						if (!empty($images)) {
							foreach ($images as $image) {
								if (file_exists(SITEROOT.'/'.$image['image'])) {
									echo "
									<li>
									 	<div class=\"img\" style=\"background:url('".BASEHREF.$gd->url('resize',$image['image'],'110x88s-#eff7e6')."');\"></div>
									 	<span onclick=\"delete_file('".$image['image']."',this)\">".l('Sterge poza')."</span>
										<input type=\"hidden\" name=\"image[]\" value=\"{$image['image']}\"/>
										<input type=\"text\" name=\"text[]\" value=\"{$image['alt']}\"/>
									</li>
								";
								} else {
									$db->delete(TABLE_PRODUCTS_IMAGES,"`id`=".$image['id']);
								}
							}
						}
					}
					?>
				</ol>
				<div class="clear"></div>
			</div>
		</dd>
	</dl>
	<script type="text/javascript">
	$(document).ready(function () {activate_uploader('uploader-2-<?=$ceva?>','<?=$this->folder?>',10,'jpg|jpeg|gif|png','image',2); });
	</script>
	<?
	}
	function show_editor_jmeker($date) {
	?>
	<dl>
		<dt><label for="description"><?=l('Description')?></label><dt><dd><br /><br /></dd>
		<script type="text/javascript">
		$(document).ready(function(){
			if (typeof(WPro) == 'undefined' || typeof(WPro.editors['description']) == 'undefined') {
				$("#description").load("<?=BASEHREF?>admin/module/editor.php",{'name':'description','id':'<?=$date['id']?>','table':'<?=$this->table?>'});
			} else {
				WPro.deleteEditor('description');
				$("#description").load("<?=BASEHREF?>admin/module/editor.php",{'name':'description','id':'<?=$date['id']?>','table':'<?=$this->table?>'});
			}
		});
		</script>
		<div id="description"></div>
	</dl>
	<?
	}
	function json_list() {
		json_list($this);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories_tree($id_parent=0,$all_ids=array()){
		global $db;
		$return='';
		$expander=($id_parent?'class="expanding"':'id="main-expander"');

		if($categories=lang_fetch_all(TABLE_CATEGORIES,"`id_parent`={$id_parent} order by `order`",LANG)){
			foreach($categories as$c){
				$sub_link=$this->json_categories_tree($c['id_main'],$all_ids);
				if(!$sub_link)
					$return.="<li><input type=\"checkbox\" name=\"id_category[]\" value=\"{$c['id_main']}\" id=\"category-{$c['id_main']}\" ".(in_array($c['id_main'],(array)$all_ids)?'checked="checked"':'')."/> <label for=\"category-{$c['id_main']}\">{$c['name']}</label></li>";
				else
					$return.="<li><a>{$c['name']}</a>{$sub_link}</li>";
			}

			return "<ul {$expander}>".$return.'</ul>';
		}else
			return false;
	}

}
$module=new products_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_categories_tree']==1) $module->json_categories_tree();
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_currencies']==1) $module->json_currencies(fget('id'));
elseif ($_GET['json_list_filter']==1) $module->json_list_filter(fget('id_category'),fget('id'));
elseif ($_GET['json_list_specifications']==1) $module->json_list_specifications(fget('id_category'),fget('id'));
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
elseif ($_GET['list_variations_options']==1) $module->list_variations_options(fget('id_var'),fget('id'));
elseif ($_GET['show_quantity']==1) $module->show_quantity(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='update_all') $module->update_all(fget('ids'));
elseif ($_GET['action']=='price_all') $module->price_all(fget('ids'));
elseif ($_GET['action']=='save_all') $module->save_all();
elseif ($_GET['action']=='price_save_all') $module->price_save_all();
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
else {
	print_header();
	print_content($module,array("update_all"=>l('Muta produse'),"price_all"=>l('Update pret %')));
	print_footer();
}
?>