<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class sejur_price_module{
	var $module='sejur_price';
	var $date='25-08-2009';
	var $table='xp_sejur_cost';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function sejur_price_module() {
		$this->name=l('Seteaza preturi adult/copii/sejur');
		$this->title=l('Seteaza preturi adult/copii/sejur');
		$this->description=l('Seteaza preturi adult/copii/sejur');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status','id_city','id_sejur','id_board_type','id_room_type','is_standard_price')
		);

		//$this->folder='imagini-pagini';

		$this->grid=array(
		0=>array('order'=>'id_sejur asc','extra'=>array(
		'id_sejur'=>array('name'=>l('Nume sejur'),'width'=>200,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		'id_board_type'=>array('name'=>l('Tip sejur'),'width'=>80,'align'=>'left','stype'=>'select','options'=>'show_select_board'),
		'id_room_type'=>array('name'=>l('Tip camera'),'width'=>80,'align'=>'left','stype'=>'select','options'=>'show_select_room'),
		)),
		'_nr'=>true,
		'_cb'=>true,

		
		//'id_confirmation_type'=>array('name'=>l('Tip confirmare'),'width'=>60),
		
		'nights'=>array('name'=>l('Nr. nopti'),'width'=>60,'align'=>'center'),
		'price'=>array('name'=>l('Pret'),'width'=>80,'align'=>'center'),
		
		'date_start'=>array('name'=>l('Data plecarii'),'width'=>80,'align'=>'center'),
		'availability_start'=>array('name'=>l('Perioda vanzare de la'),'width'=>80,'align'=>'center'),
		'availability_end'=>array('name'=>l('Perioda vanzare pana la'),'width'=>80,'align'=>'center'),
		
		//'adults'=>array('name'=>l('Adulti'),'width'=>120),
		//'children'=>array('name'=>l('Copii'),'width'=>120),
		//'telephone'=>array('name'=>l('Phone'),'width'=>120),
		//'website'=>array('name'=>l('Website'),'width'=>120),
		//'rating'=>array('name'=>l('Stele'),'width'=>50,'align'=>'center'),
		//'address'=>array('name'=>l('Adresa'),'width'=>150),
		//'id_city'=>array('name'=>l('Oras'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		//'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'is_standard_price'=>array('name'=>l('Tip oferta'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';0:'.l('De la').';1:'.l('Fix').';1:'.l('SPO'))),
		//'is_header'=>array('name'=>l('In header'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_footer'=>array('name'=>l('In footer'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_info'=>array('name'=>l('informatie utila'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_navigator'=>array('name'=>l('apare in navigator'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		//'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:92%;'),
		4=>'show_sejururi',
		5=>'show_board',
		//'adults'=>array('type'=>'input','name'=>l('Adulti/camera'),'style'=>'width:70%;'),

		//'children'=>array('type'=>'input','name'=>l('Copii/camera'),'style'=>'width:70%;'),
		
		6=>'show_room',
		7=>'show_confirm',
		
		'date_start'=>array('type'=>'date','name'=>l('De la data')),
		//'date_end'=>array('type'=>'date','name'=>l('Pana la data')),
		
		'price'=>array('type'=>'input','name'=>l('Pret'),'style'=>'width:70%;'),
		8=>'show_currencies',
		
		'type'=>array('type'=>'radio','name'=>l('Tip pret'),'options'=>array(1=>l('Pe adult'),0=>l('Pe camera'))),
		'max_adults'=>array('type'=>'input','name'=>l('Max. Adulti'),'style'=>'width:70%;'),	
		);
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_sejururi();
	}
	function show_select_board() {
		$_GET['from_grid']=1;
		$this->json_board();
	}
	function show_select_room() {
		$_GET['from_grid']=1;
		$this->json_room();
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function show_sejururi($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_sejur"><?=l('Nume sejur')?></label>
			</dt>
			<dd>
				<select name="id_sejur" id="id_sejur" style="width:400px">
				<?php $this->json_sejururi($date['id_sejur']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_sejururi($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
			$resursa=$db->query("SELECT
								pd.`name`,p.`id`

								 FROM
								`xp_sejur` p
							inner join  `xp_sejur_data` pd
						 		on p.`id`=pd.`_id` where  p.`id_category`=1 ORDER BY p.`date` desc");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_board($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_board_type"><?=l('Tip masa')?></label>
			</dt>
			<dd>
				<select name="id_board_type" id="id_board_type" style="width:400px">
				<?php $this->json_board($date['id_board_type']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_board($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_board_types_data` where `lang`='".LANG."' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_room($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_room_type"><?=l('Tip camera')?></label>
			</dt>
			<dd>
				<select name="id_room_type" id="id_room_type" style="width:400px">
				<?php $this->json_room($date['id_room_type']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_room($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_room_types_data` where `lang`='".LANG."' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_confirm($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_confirmation_type"><?=l('Tip confirmare')?></label>
			</dt>
			<dd>
				<select name="id_confirmation_type" id="id_confirmation_type" style="width:400px">
				<?php $this->json_confirm($date['id_confirmation_type']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_confirm($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_confirmation_type_data` where `lang`='".LANG."' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

		$row['id_sejur']=$db->fetch_one("select `name` from `xp_sejur_data` WHERE `_id`={$row['id_sejur']} ");
		if ($row['is_standard_price']==0) {
			$row['is_standard_price']='De la';
		}elseif ($row['is_standard_price']==1) {
			$row['is_standard_price']='Fix';
		}  else $row['is_standard_price']='<b style="color:red;">SPO</b>';
		//$row['id_board_type']=$db->fetch_one("select `name` from `xp_board_types_data` WHERE `id`={$row['id_board_type']} ");
		//$row['id_room_type']=$db->fetch_one("select `name` from `xp_room_types_data` WHERE `id`={$row['id_room_type']} ");
		
		//$row['actions']="<a class=\"action\" href=\"".BASEHREF."admin/image.php?id_product=".$row['id']."\">Galerie foto</a><br/><br/>".$row['actions'];
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Oras')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px">
				<?php $this->json_categories($date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_brand=0) {
		global $db;

		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `xp_cities` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_cities_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_admin = $db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			
			if ($date_admin['date_start']=='') $date_admin['date_start']=time();
			//if ($date_admin['date_end']=='') $date_admin['date_end']=time();
		}
		
		if ($date_admin['date_start']=='') {
			$date_admin['date_start']=time();
		} else $date_admin['date_start']=strtotime($date_admin['date_start']);
//		if ($date_admin['date_end']=='') {
//			$date_admin['date_end']=time();
//		} else $date_admin['date_end']=strtotime($date_admin['date_end']);
//		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//		} else $date_admin['image']='';
//
//		if ($date_admin['big_image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['big_image'])){
//			$date_admin['big_image']='static/i/'.$this->folder.'/'.$date_admin['big_image'];
//		} else $date_admin['big_image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;

		$data['date_start']=strtotime($data['date_start']);
		//$data['date_end']=strtotime($data['date_end']);
		$data['date_start']=date('Y-m-d',$data['date_start']);
		//$data['date_end']=date('Y-m-d',$data['date_end']);


		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'],'-'),'static/i/'.$this->folder,''));
//			$data['big_image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['big_image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'].' big','-'),'static/i/'.$this->folder,''));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new sejur_price_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>