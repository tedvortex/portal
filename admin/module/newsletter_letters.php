<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class newsletter_letters_module {
	var $module='newsletter_letters';
	var $date='27-08-2009';
	var $table=TABLE_NEWSLETTER_LETTERS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function newsletter_letters_module() {

		$this->name=l('Scrisori newsletter');
		$this->title=l('Scrisori newsletter');
		$this->description=l('Scrisori newsletter');

		$this->type=array(
		'like'=>array('subject'),
		'date'=>array('date'),
		'equal'=>array('number','status')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'number'=>array('name'=>l('Numar'),'width'=>80,'align'=>'center'),
		'subject'=>array('name'=>l('Subiect'),'width'=>300),
//		'status'=>array('name'=>l('Status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'subject'=>array('type'=>'input','name'=>l('Subiect'),'valid'=>'empty,min_4,max_200',"style"=>'width:98%;'),
//		'number'=>array('type'=>'input','name'=>l('Numar'),'valid'=>'empty,unique','edit'=>true),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		'message'=>array('type'=>'editor','name'=>l('Mesaj')),
		//51=>'show_editor_jmeker',
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function json_list() {
		json_list($this,false);
	}
	function print_records() {
		print_content($this->module);
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica scrisoare'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		$data['message']=str_replace('src="http:/','src="http://www.bricotop.ro/',$data['message']);
		$data['message']=str_replace('href="','href="http://www.bricotop.org',$data['message']);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				$newNumber=$db->fetch_one("SELECT MAX(number) FROM `".TABLE_NEWSLETTER_LETTERS."`");
				$newNumber++;
				$data['number']=$newNumber;
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function show_editor_jmeker($date) {
	?>
	<dl>
		<dt><label for="description"><?=l('Description')?></label><dt><dd><br /><br /></dd>
		<script type="text/javascript">
		$(document).ready(function(){
			if (typeof(WPro) == 'undefined' || typeof(WPro.editors['description']) == 'undefined') {
				$("#description").load("<?=BASEHREF?>admin/module/editor.php",{'name':'message','id':'<?=$date['id']?>','table':'<?=$this->table?>','lang':'false'});
			} else {
				WPro.deleteEditor('description');
				$("#description").load("<?=BASEHREF?>admin/module/editor.php",{'name':'message','id':'<?=$date['id']?>','table':'<?=$this->table?>','lang':'false'});
			}
		});
		</script>
		<div id="description"></div>
	</dl>
	<?
	}
}
$module=new newsletter_letters_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>