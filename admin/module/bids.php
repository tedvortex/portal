<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class bids_module {
	var	$module='bids';
	var $date='27-08-2009';
	var $table='xp_bids';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function bids_module() {

		$this->name=l('Biduri licitatii');
		$this->title=l('Biduri licitatii');
		$this->description=l('Biduri licitatii');

		$this->type=array(
		'like'=>array('name','id','auction','id_user'),
		'date'=>array('date','end_date','start_date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'auction'=>array('name'=>l('Licitatie'),'width'=>100,'align'=>'center'),
		'id_user'=>array('name'=>l('User'),'width'=>100,'align'=>'center'),
		'date'=>array('name'=>l('data'),'width'=>100,'align'=>'center'),
		'ammount'=>array('name'=>l('Pret'),'width'=>100,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

	}


	function json_list_row($row) {
		global $db;
		$idp=$db->fetch_one("SELECT `id_product` FROM `".TABLE_AUCTIONS."` WHERE `id`=".$row['id_auction']);
		//echo "SELECT `id_product` FROM `".TABLE_AUCTIONS."` WHERE `id`=".$row['id_auction'];
		if ($idp) $row['auction']=$db->fetch_one("SELECT `name` FROM `".TABLE_PRODUCTS.TABLE_EXTEND."` WHERE `id_main`=".$idp." AND lang='".LANG."'");
		$row['id_user']=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_user']);
		return $row;
	}

	function css() {}
	function json_list() {
		json_list($this,false);
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table,false);
	}
}
$module=new bids_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortname'=>'start_date','sortorder' => 'desc'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	$module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>