<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class auctions_bids_module {
	var	$module='auctions_bids';
	var $date='27-08-2009';
	var $table='xp_bids';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function auctions_bids_module() {

		$this->name=l('Biduri licitatii');
		$this->title=l('Biduri licitatii');
		$this->description=l('Biduri licitatii');

		$this->type=array(
		'like'=>array('name'),
		'date'=>array('date','end_date','start_date'),
		'equal'=>array('default','status')
		);

		$this->grid=array(
			'_nr'=>true,
		'_cb'=>true,
		'id_user'=>array('name'=>l('User'),'width'=>100,'align'=>'center'),
		'date'=>array('name'=>l('data'),'width'=>100,'align'=>'center'),
		'ammount'=>array('name'=>l('Pret'),'width'=>100,'align'=>'center'),
		);

	}


	function json_list_row($row) {
		global $db;

		$row['id_user']=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_user']);
		return $row;
	}

	function css() {

	?>
	#list_<?=$this->module?>_container {
		width:580px;
	}
	<?php
	}
	function js($id_customer=0) {


		$id_lic=$id_customer*-1;
		?>
		
			$(".module_menu .back_bid").live('click',function(){
				var window_add_edit_name='<?=l('back bid')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=back_bid&id=<?=$id_lic?>&ids='+gr,600, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});


		<?php
		set_grid($this,array('multiselect'=>true,'sortname'=>'ammount','sortorder' => 'desc'));
	}
	function json_list() {
		global $db;
		$id_customer=$db->escape($_GET['id_customer']);
		if ($id_customer<0) {
			$id_customer=-$id_customer;
			$new_sql="SELECT *,count(`id`) as `ammount` FROM `" . $this->table . "` WHERE `id_auction`=".$id_customer." GROUP BY `id_user`";
		} else{
			$new_sql="SELECT * FROM `" . $this->table . "` WHERE `id_auction`=".$id_customer;
		}
		//	echo $new_sql;
		json_list($this,false,$new_sql);
	}
}
$module=new auctions_bids_module();
if ($module_info) $this_module=$module;
elseif ($module_js)  $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['action']=='back_bid') 	{
	$id_customer=$db->escape($_GET['id']);
	$exist=$db->fetch_one("SELECT `back_bid` FROM `" . TABLE_AUCTIONS. "` WHERE `id`=".$id_customer);
	if ($exist==1) {
?>
		<center><b>Creditele au mai fost returnate la aceasta licitatie</b></center>
		<?php 
	} else {
		$all_auctions=$db->fetch_all("SELECT *,count(`id`) as `ammount` FROM `" . $module->table . "` WHERE `id_auction`=".$id_customer." GROUP BY `id_user`");
		$ids=explode(",",$_GET['ids']);
		foreach ($all_auctions as $all_auctions_nw)
		if (in_array($all_auctions_nw['id'],$ids)) {
			$db->sql("UPDATE `".TABLE_CUSTOMERS."` SET `credite`=`credite`+".$all_auctions_nw['ammount']." WHERE `id`=".$all_auctions_nw['id_user']);
		}
		$db->update(TABLE_AUCTIONS,array('back_bid'=>1),"`id`=".$id_customer);
		?>
		<center><b>Creditele au fost returnate</b></center>
		<?php 
	}
}elseif ($_GET['json_list']==1) 	{

	$module->json_list();

}
else {
	/*	if ($_GET['back_bid']==1) {
	$id_customer=$db->escape($_GET['id'])*-1;
	$exist=$db->fetch_one("SELECT `back_bid` FROM `" . TABLE_AUCTIONS. "` WHERE `id`=".$id_customer);
	if ($exist==1) {
	?>
	<center><b>Creditele au mai fost returnate la aceasta licitatie dupa cum urmeaza</b></center>
	<?php
	} else {
	$all_auctions=$db->fetch_all("SELECT *,count(`id`) as `ammount` FROM `" . $module->table . "` WHERE `id_auction`=".$id_customer." GROUP BY `id_user`");
	foreach ($all_auctions as $all_auctions_nw)
	$db->update(TABLE_CUSTOMERS,array('credite'=>$all_auctions_nw['ammount']),"`id`=".$all_auctions_nw['id_user']);
	$db->update(TABLE_AUCTIONS,array('back_bid'=>1),"`id`=".$id_customer);
	?>
	<center><b>Creditele au fost returnate dupa cum urmeaza</b></center>
	<?php
	}
	}*/
	$id_customer=$db->escape($_GET['id']);
	if ($id_customer<0) $array_add=array('back_bid'=>l('Returneaza credite'));
	else $array_add=array();
	if ($id_customer<0 ) $module->grid['ammount']['name']='Bids';
	print_content($module,$array_add,'','',array('new','edit','delete'));

}
?>