<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class servicii_module {
	var $module='servicii';
	var $date='25-08-2009';
	var $table=TABLE_SERVICII;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function servicii_module() {

		$this->name=l('Servicii');
		$this->title=l('Servicii');
		$this->description=l('Servicii');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		//'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'name'=>array('name'=>l('Titlu stire'),'width'=>200),
		//'title'=>array('name'=>l('Title'),'width'=>300),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume stire'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2','style'=>'width:82%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:75%;','info'=>l('warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!')),
		//'link'=>array('type'=>'input','name'=>l('Link'),'text'=>'','lang'=>true,'style'=>'width:82%;'),
		//'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:82%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//'email_news'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('email news')),
		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true,'valid'=>'empty'),
		'description'=>array('type'=>'editor','name'=>l('description'),'lang'=>true),
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'ニュース'),
		);
	}
	function css() {}
	function json_list() {
		json_list($this,true);
	}
	function json_list_row($row) {
		global $gd;
		if (!empty($row['image']))
		$row['image']='<img src="../'.$gd->url('resize',$row['image'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=lang_fetch($this->table,"`id`=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Date stire'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		unset($data['email_news']);
		$errors=form_validation($data,$this->form,$this->table);
		//print_a($data);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				lang_update($this->table, $data," `id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				if ($_POST['email_news']==1) {
					$_GET['action']='send';
					$_POST['link']=1;
					$_POST['message']=l('Newsletter').' - '.$data['nss_languages'][LANG]['description'];
					$_POST['subject']=$data['nss_languages'][LANG]['name'];
					$_POST['emails']="";
					foreach ( $db->fetch_all("SELECT * FROM `".TABLE_NEWSLETTER_EMAILS."` WHERE status=1") as $email){
						$_POST['emails'].=htmlentities("\n".$email['email']);
					}
					if (!empty($_POST['emails'])) include_once(dirname(__FILE__)."/newsletter_emails.php");

					print_alerta(l('A fost inserat si trimis newsletter'));
				} else print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new servicii_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortorder'=>'desc'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list($module,true);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>