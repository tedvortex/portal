<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");

class links_module {
	var	$module='links';
	var $date='27-08-2009';
	var $table='xp_links';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function links_module() {

		$this->name=l('Link-uri oferte home - dreapta');
		$this->title=l('Link-uri oferte home - dreapta');
		$this->description=l('Link-uri oferte home - dreapta');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id_shop')
		);
		$this->folder='producatori';
		$this->grid=array(
		0=>array('order'=>'id asc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>200,'align'=>'center'),
		'link'=>array('name'=>l('Link'),'width'=>200,'align'=>'center'),


		//'is_promo'=>array('name'=>l('Promo'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume buton'),'valid'=>'empty,min_1,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'link'=>array('type'=>'input','name'=>l('Link'),'valid'=>'empty,min_1,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'name_seo'=>array('type'=>'input','name'=>l('Nume seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'id_shop'=>array('type'=>'radio','options'=>array(1=>l('Crap'),2=>l('Stationar'),3=>l('Rapitor')),'name'=>l('Categorie')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		
		);
	}
	function json_list() {
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd;
		
		
		$row['actions']='<a class="action edit" onclick="do_edit('.$row['id'].',\'links\');" >'.l('edit').'</a>';
		
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
	
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		

		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);



			unset($data['type_op']);
			//if (!isset($data['image'])) $data['image']='';
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new links_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module);
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>