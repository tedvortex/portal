<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class products_module {
	var $module='products';
	var $date='27-08-2009';
	var $table=TABLE_PRODUCTS;
	var $folder='products_images';
	var $option_info;
	var $value_all_info;
	var $all_variations=array();
	var $all_variations_name=array();
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function products_module() {
		global $db;


		$this->name=l('Produse');
		$this->title=l('Produse');
		$this->description=l('Produse');

		$this->type=array(
		'like'=>array('name','code','description','price','views','cadou_nume'),
		'date'=>array('date','date_start','date_end'),
		'equal'=>array('status','id_brand','id_currency',
		'id_furnizor','id_main','id','is_default','is_new_crap','is_promo_crap','is_clearance_crap',
		'is_new_stationary','is_promo_stationary','is_clearance_stationary',
		'is_new_predator','is_promo_predator','is_clearance_predator'),
		'in'=>array('id_category')
		);

		$this->folder='imagini-produse';
		$this->folder3='i/imagini-produse';
		$this->folder4='i/imagini-promotii';
		$this->folder2='static/fisiere-produse';

		$this->orders_status=array(
		1=>l('Activ'),
		0=>l('Inactiv'),
		);
		$this->promotion_type=array(
		//0=>l('Categorii'),
		2=>l('Produse')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>25,'align'=>'center'),
		'image'=>array('name'=>l('Image 1'),'width'=>75,'align'=>'center'),
		'name'=>array('name'=>l('Nume produs'),'width'=>100),
		//'description'=>array('name'=>l('Descriere'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie produs'),'width'=>80,'align'=>'left','stype'=>'select','options'=>'show_select_category'),
		'id_brand'=>array('name'=>l('Producator'),'width'=>60,'align'=>'center','stype'=>'select','options'=>'show_select_brands'),
		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>80,'align'=>'center','stype'=>'select','options'=>'show_select_furnizori'),
		//'code'=>array('name'=>l('Cod'),'width'=>40),
		'price'=>array('name'=>l('Pret'),'width'=>40,'align'=>'right'),
		'pricev'=>array('name'=>l('Pret vechi'),'width'=>40,'align'=>'right'),
		//'id_currency'=>array('name'=>l('Moneda'),'width'=>50,'align'=>'left','stype'=>'select','options'=>'show_select_currencies'),
		'views'=>array('name'=>l('Views'),'width'=>20,'align'=>'center'),
		//'stock'=>array('name'=>l('Stoc'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>30,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'is_new_crap'=>array('name'=>l('Noutati<br/>crap'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_promo_crap'=>array('name'=>l('Promo<br/>crap'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_clearance_crap'=>array('name'=>l('Lichidari<br/>crap'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_new_stationary'=>array('name'=>l('Noutati<br/>stationar'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_promo_stationary'=>array('name'=>l('Promo<br/>stationar'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_clearance_stationary'=>array('name'=>l('Lichidari<br/>stationar'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_new_predator'=>array('name'=>l('Noutati<br/>rapitor'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_promo_predator'=>array('name'=>l('Promo<br/>rapitor'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'is_clearance_predator'=>array('name'=>l('Lichidari<br/>rapitor'),'width'=>40,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'is_new'=>array('name'=>l('Noutati crap'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		//'is_best_seller'=>array('name'=>l('Top vandut'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'date'=>array('name'=>l('Date'),'width'=>120,'align'=>'center','stype'=>'date'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Detalii produs'),
		//2=>l('Categorie Produs'),
		//8=>l('Avantaje'),
		//4=>l('Specificatii produse'),
		//5=>l('Valori filtre'),
		//6=>l('Aloca pachet promo'),
		//7=>l('Aloca tip personalizare'),
		9=>l('Variatii produse'),
		10=>l('Galerie video youtube'),
	//	11=>l('Etichete')
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:92%;','auto'=>'name_seo'),
		'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'style'=>'width:92%;height:150px;'),
		//'price_description'=>array('type'=>'editor','name'=>l('Pret tabelar')),
		'description'=>array('type'=>'editor','name'=>l('Descriere')),
		1=>l('Alte detalii'),
		//2=>'show_furnizori',

		3=>'show_brands',
		4=>'show_categories2',
		//5=>'show_currencies',
		'price'=>array('type'=>'input','name'=>l('Pret'),'info'=>l('Pret')),
		'pricev'=>array('type'=>'input','name'=>l('Pret vechi'),'info'=>l('Pret vechi')),
		//'discount'=>array('type'=>'input','name'=>l('Discount %'),'info'=>l('Discount in procente')),
		//'price_type'=>array('type'=>'radio','name'=>l('Tip pret'),'options'=>array(0=>l('Pret fix'),1=>l('Pret de la'))),
		'code'=>array('type'=>'input','name'=>l('Cod'),'info'=>l('Code')),

		'stock'=>array('type'=>'radio','name'=>l('Stoc'),'options'=>array(1=>l('In stoc'),0=>l('Stoc indisponibil'))),
		//'stock'=>array('type'=>'input','name'=>l('Nivelul de stoc'),'info'=>l('Nivelul stocului')),
		//'stock_type'=>array('type'=>'radio','name'=>l('Tip stoc'),'options'=>array(0=>l('In functie de numar produse'),1=>l('Nelimitat'),2=>l('La comanda'))),
		//'weight'=>array('type'=>'input','name'=>l('Greutate'),'info'=>l('Greutate produs')),
		//'tax_price'=>array('type'=>'input','name'=>l('Taxa livrare'),'info'=>l('Taxa livrare')),
		//'af_home'=>array('type'=>'radio','name'=>l('Vizibil pe homepage'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'is_customizable'=>array('type'=>'radio','name'=>l('Produs customizabil'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'customization_text'=>array('type'=>'text','name'=>l('Text customizare'),'style'=>'width:92%;height:100px;'),

		//'has_cart'=>array('type'=>'radio','name'=>l('Buton "Cumpara"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'has_personalize'=>array('type'=>'radio','name'=>l('Buton "Personalizeaza"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'has_offer'=>array('type'=>'radio','name'=>l('Buton "Cere oferta"'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'water'=>array('type'=>'radio','name'=>l('Aplica watermark'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		'fb'=>array('type'=>'radio','name'=>l('FB discount'),'options'=>array(0=>l('Fara discount'),1=>l('-5%'),2=>l('-10%')),'value'=>0),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		//15=>l('Imagini'),
		//'water'=>array('type'=>'radio','name'=>l('Aplica watermark'),'options'=>array(1=>l('Da'),0=>l('Nu'))),
		//'images'=>array('type'=>'image','name'=>l('Imagine - 600px x 450px'),'folder'=>$this->folder,'multiple'=>true,'images'=>'return_images'),

		//58=>array('tab'=>9),
		//'related_products_checkbox'=>array('type'=>'checkbox','name'=>l('Produse asemanatoare'),'text'=>l('Gaseste si arata automat produse asemanatoare'),'info'=>l('Bifati aceasta casuta si o lista cu produse asemanatoare va fi aleasa si afisata in magazinul dvs. Debifati casuta pentru a nu arata produsele asemanatoare sau pentru a alege dumneavoastra produsele asemanatoare, folosind instructiunile:1. Alegeti o categorie si produsele din acea categorie vor fi afisate; 2. Faceti dublu-click pe un produs pentru a-l adauga ca produs asemanator; 3. Pentru a sterge un produs din lista de produse asemanatoare, faceti dublu-click pe el in josul boxului')),
		//59=>'list_related_products',
//		20=>array('tab'=>4),
//		21=>'list_specifications',
//		25=>array('tab'=>5),
//		26=>'list_filters',
//		51=>array('tab'=>6),
//		'start_date'=>array('type'=>'date','name'=>l('Data start'),'style'=>'width:40%','text'=>'ex. 28-04-2011'),
//		'end_date'=>array('type'=>'date','name'=>l('Data final'),'style'=>'width:40%','text'=>'ex. 25-05-2011'),
//		53=>'list_product_banner',
		//30=>array('tab'=>2),
		//59=>'list_related_products',
		31=>array('tab'=>9),
		'have_variations'=>array('type'=>'radio','name'=>l('status'),'options'=>array(0=>l('Produsul nu are variatii'),1=>l('Produs cu variatii'))),
		32=>'list_variations_tab',
		33=>array('tab'=>10),
		'youtube_1'=>array('type'=>'input','name'=>l('Link youtube 1'),'style'=>'width:92%;'),
		'youtube_2'=>array('type'=>'input','name'=>l('Link youtube 2'),'style'=>'width:92%;'),
		'youtube_3'=>array('type'=>'input','name'=>l('Link youtube 3'),'style'=>'width:92%;'),
		'youtube_4'=>array('type'=>'input','name'=>l('Link youtube 4'),'style'=>'width:92%;'),
		'youtube_5'=>array('type'=>'input','name'=>l('Link youtube 5'),'style'=>'width:92%;'),
		'youtube_6'=>array('type'=>'input','name'=>l('Link youtube 6'),'style'=>'width:92%;'),
		'youtube_7'=>array('type'=>'input','name'=>l('Link youtube 7'),'style'=>'width:92%;'),
		'youtube_8'=>array('type'=>'input','name'=>l('Link youtube 8'),'style'=>'width:92%;'),
		);

		$this->form_facebook=array(
		'fb'=>array('type'=>'radio','name'=>l('FB discount'),'options'=>array(0=>l('Fara discount'),1=>l('-5%'),2=>l('-10%'))),
		);
	}
	function list_variations_tab($date) {
		global $db;
		$all_variations=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_NAME."` WHERE status=1 ORDER BY `name` ASC");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Variatie')?></label>
			</dt>
			<dd>
			<select name="id_variation" onchange="list_variations_options(this.value,<?=(int)$date['id']?>);">
			<option value="0"> <?=l('Alege o variatie')?> </option>
				<?php foreach ($all_variations as $variation) { ?>
				<option value="<?=$variation['id']?>" <?=($date['id_variation']==$variation['id']?'selected':'')?>><?=ehtml($variation['name'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<br clear="all" />
		<div id="list_variations_options" class="product_have_variations">
		<?php
		if (!empty($date['id_variation'])) {
			$this->list_variations_options($date['id_variation'],$date['id']);
		}
		?>
		</div>
		<?php
	}
	function list_variations_options($id_variation,$id_product) {
		global $db;
		if (!empty($id_variation)) {
			/*	$this->option_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`='".$id_variation."' ORDER BY `order`  ASC");

			$this->value_all_info=array();
			foreach ($this->option_info as $option) {
			$value_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`='".$option['id']."'  ORDER BY `order` ASC");
			$this->value_all_info[$option['id']]=$value_info;
			}
			$this->get_rec();
			$product_info_variations=array();
			if (!empty($id_product)) {
			$resursa=$db->sql("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product));
			while ($product_variations=$db->fetch($resursa)) {
			$product_info_variations[str_replace("-","_",$product_variations['variation'])]=$product_variations;
			}
			}*/

			$all_var=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product)." ORDER BY `order`");
			$all_var=array_merge($all_var,array(0=>array('id'=>'new')));

?>
<!--<input type="hidden" name="variations[nss_all_variations]" id="nss_all_variations"  />-->
<div style="width:928px;">
		<table cellpadding="0" cellspacing="0" border="0"  id="table_variations" >
			<thead>
			<tr>
				<th style="width:20px"><input type="checkbox" name="aaaaaa" /></th>
				<th>Nume variatie</th>
				<th style="width:72px">Pozitia</th>
				<th style="width:72px">Cod variatie</th>
		<!--		<th style="width:72px"><?=Stoc?></th>-->
				<th style="width:72px">Pret vechi</th>
				<th style="width:140px">Pret</th>
				
				<th style="width:140px">Greutate</th>
				<!--<th style="width:200px;"><?=imagine?></th>-->

			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($all_var as $_temp_nw) {
				if($_temp_nw['id']=='new') $after_for_new="[]"; else $after_for_new="";
				$var=$_temp_nw['id'];
			?>
			<tr id="tr_<?=$var?>">
				<td align="center" >
					<input type="checkbox" name="variations[<?=$var?>][variation]<?=$after_for_new?>" value="<?=$var?>" <?=(isset($_temp_nw)?'checked':'')?> />
				</td>
				<td >	<input type="text" name="variations[<?=$var?>][variation_name]<?=$after_for_new?>" size="60" value="<?=$_temp_nw['variation_name']?>" />

					<?php if($_temp_nw['id']=='new') { ?> <img src="images/addicon.gif" align="absmiddle"  class="pointer" onclick="$(this).parent().parent().clone(true).insertAfter($(this).parent().parent());"  />  <?php } ?>
				<img src="images/delicon.gif" align="absmiddle"  class="pointer" onclick="$(this).parent().find('input').val('');" />

				</td>
					<td>
					<input type="text" name="variations[<?=$var?>][order]<?=$after_for_new?>" size="4" value="<?=$_temp_nw['order']?>" />
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][code]<?=$after_for_new?>" size="8" value="<?=$_temp_nw['code']?>" />
				</td>
<!--				<td>
					<input type="text" name="variations[<?=$var?>][stock]" size="8" value="<?=$_temp_nw['stock']?>" />
				</td>-->
				<td>
					<input type="text" name="variations[<?=$var?>][pricev]<?=$after_for_new?>" size="8" value="<?=$_temp_nw['pricev']?>" />
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][price_type]<?=$after_for_new?>">
					<?php $price_type=$_temp_nw['price_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($price_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($price_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($price_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($price_type==0)?'style="display: none;"':''?>>
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][price]<?=$after_for_new?>" value="<?=$_temp_nw['price']?>" />
					</span>
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][weight_type]<?=$after_for_new?>">
					<?php $weight_type=$_temp_nw['weight_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($weight_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($weight_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($weight_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($weight_type==0)?'style="display: none;"':''?> >
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][weight]<?=$after_for_new?>"  value="<?=$_temp_nw['weight']?>" />
					</span>
				</td>


			</tr>

			<?php
			}
			?>
		</tbody>
		</table>

		</div>

		<!--	<tr id="tr_next_<?=$var?>" class="bodyTr" onmouseover="this.className='bodyTrHover'; $('#tr_<?=$var?>').attr('class','bodyTrHover');" onmouseout="this.className='bodyTr';$('#tr_<?=$var?>').attr('class','bodyTr');">
			<td colspan="7" style="padding-bottom:4px; border-bottom:1px solid #EFE2F0;"><input type="text" style="width:99%" name="short_description[]" value="<?=$_temp_nw['short_description']?>"/></td>
			</tr>-->
<?php }
	}
	function list_related_products_sample($date) {
		global $db;
		if (isset($date['related_products2'])) $date['related_products2']=explode(",",$date['related_products2']);
		//if (isset($date['id_category_related'])) $date['id_category_related']=explode(",",$date['id_category_related']);
		//print_a($date['id_category_related']);
		?>
		<dl class="related_products_select">
		<dt>
		<label for="products"><?=l('Categorie produs')?></label>
		</dt>
		<dd>
		<select name="related_id_category2" id="related_id_category2" style="margin-left:8px;width:560px" onchange="do_product_filter5();">
		<option value="0"><?=l('Toate')?></option>
		<?php $this->json_categories() ?>
		</select>
		</dd>
		<dl class="related_products_select">
		<dt>
		<label for="products"><?=l('Nume produs')?></label>
		</dt>
		<dd><input type="text" name="related_product_name" id="related_product_name" style="width:560px" onkeyup="do_product_filter5();" />
		</dd>
		</dl>
		<dl class="related_products_select">
		<dt>
		<label for="products"><?=l('Produse')?></label>
		</dt>
		<dd id="list_products_dd5">
		<select name="products5" multiple="multiple"  size="12" style="width:460px;" onchange="select_product5(this);">
		<?php $this->json_products5($date['related_products2']); ?>
		</select>
		</dd>
		</dl>
		<div class="list_products_selected5 all_options_type options_type_2">
				<?php
				if (!empty($date['related_products2'])) {
					foreach ($date['related_products2'] as $id_now) {

						if (!empty($id_now)) {
							$sql="select
										p.*,
										pd.*
									from
								`xp_products` p

								inner join `xp_products_data` pd
									on p.`id`=pd.`id_product`

								WHERE p.`id`=".$id_now;
							$product=$db->fetch($sql);
							//print_a($product);
							?><div ><input type="hidden" name="products5[]" value="<?=$product['id_product']?>" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b><?=$product['code']?> - <?=$product['name']?></b></div><?php
						}
					}
				}
				?>
				</div>

		<?php
	}

	function list_labels($date) {
		global $db,$gd;
		//print_a($date);
		//if (isset($date['related_products'])) $date['related_products']=explode(",",$date['related_products']);
		//if (isset($date['id_category_related'])) $date['id_category_related']=explode(",",$date['id_category_related']);
		//print_a($date['id_category_related']);
		?>

		<dl class="related_products_select">
		<dt style="font-size:16px;padding-top:70px;font-weight:bold;">
		Nicio eticheta
		</dt>
		</dt>
		<dd >
		<div style="text-align:center;width:110px;height:75px;padding-top:35px;">
		<input type="radio" name="id_label" value="0" <?=($date['id_label']==0)?'checked="checked"':''?> /> <br/>
		</div>
		</dd>
		<?php
		$resursa=$db->query("SELECT * FROM `xp_label` where `status`=1 ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<dt style="font-size:16px;padding-top:35px;font-weight:bold;">
			<?php echo $linie['name']?>
			</dt>
			<dd >
			<div style="text-align:center;width:110px;">
			<input type="radio" name="id_label" value="<?=$linie['id']?>" <?=($linie['id']==$date['id_label'])?'checked="checked"':''?>/> <br/><img src="<?=$gd->url('resize','../static/i/imagini-etichete/'.$linie['image'],'100x75')?>"  />
			</div>
			</dd>
			<?php
		}
		?>
		</dl>
		<?php
	}
	function list_related_products($date) {
		global $db;
		//if (isset($date['related_products'])) $date['related_products']=explode(",",$date['related_products']);


		?>
		<dl class="related_products_select">

		<dt><label for="id_category_related"><?=l('Categorie')?></label></dt>
		<dl>

				<dd><?=$this->json_categories_tree(0,$date['id_category_related']);?></dd>
		</dl>

		</dl>
		<?php
	}
	function show_categories2($date) {
		global $db;

		$adm=adm_info();
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produs')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<optgroup label="alege">
						<option value="0"> Alege o categorie </option>
				</optgroup>
				<?php $this->json_categories2($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category_to_product();" />
			</dd>
			<dt>&nbsp;</dt>
			<dd id="selected-categories">&nbsp;<br/>
				<? if($date['id']!=''){
					$date['id_category']=explode(',',$date['id_category']);
					foreach($date['id_category'] as$k=>$v){
					$category=$db->fetch("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `status`=1 and `_id`={$v}",MYSQL_ASSOC);?>
				<div class="value-<?=$category['_id']?>">
					<input type="hidden" name="id_category[]" value="<?=$category['_id']?>"/><?=$category['name']?>&nbsp;<a style="color:#036;">sterge</a>
				</div>
				<?}}?>
			</dd>
		</dl>
		<?php
	}
	function json_categories2($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$adm=adm_info();

		if($anvelope=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `status`=1 and `id_parent`=0 ",MYSQL_ASSOC)){
			foreach($anvelope as$anvelopa){
				if($anv=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `status`=1 and `id_parent`={$anvelopa['_id']} ",MYSQL_ASSOC)){?>
				<optgroup label="<?=$anvelopa['name']?>">
					<?foreach($anv as$a){?>
					<option value="<?=$a['_id']?>"><?=$a['name']?></option>
					<?php $this->json_categories3($a['_id']) ?>
					<?}?>
				</optgroup>
				<?}else{?>
				<option value="<?=$anvelopa['_id']?>"><?=($deep?'&nbsp;&nbsp;&nbsp;':$anvelopa['name'])?></option>
				<?php $this->json_categories3($anvelopa['_id']) ?>
				<?}
			}
		}
	}

	function json_categories3($id_category=0) {
		global $db;
		echo "select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `status`=1 and `id_parent`=".$id_category;
		if($anvelope=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `status`=1 and `id_parent`=".$id_category,MYSQL_ASSOC)){
			foreach($anvelope as$anvelopa){ ?>

				<option value="<?=$anvelopa['_id']?>">&nbsp;&nbsp;&nbsp<?=$anvelopa['name']?></option>
				<?php $this->json_categories3($anvelopa['_id']) ?>
				<?
			}
		}
	}

	function show_categories_tree($date) {
		global $db;
		$categories='';

		if(isset($date['id']))
			if($item2categories=$db->fetch_all("select * from `{$this->table}_to_categories` where `id_product`=".$date['id']))
				foreach($item2categories as$c)
					$categories[]=$c['id_category'];
		?>
		<dl>
			<dt><label for="id_category_related"><?=l('Categorie')?></label></dt>
			<dd><?=$this->json_categories_tree(0,$categories);?></dd>
		</dl>
		<?
	}
	function json_categories_tree($id_parent=0,$all_ids=array()){
		global $db;
		$return='';
		$expander=($id_parent?'class="expanding"':'class="expanding"');

		if($categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC")){
			print_a($categories);
			foreach($categories as$c){
				$sub_link=$this->json_categories_tree($c['_id'],$all_ids);
				if(!$sub_link)
					$return.="<li><input type=\"checkbox\" name=\"id_category_related[]\" value=\"{$c['_id']}\" id=\"category-{$c['_id']}\" ".(in_array($c['_id'],(array)$all_ids)?'checked="checked"':'')."/> <label for=\"category-{$c['_id']}\">{$c['name']}</label></li>";
				else
					$return.="<li><a>{$c['name']}</a>{$sub_link}</li>";
			}

			return "<ul {$expander}>".$return.'</ul>';
		}else
			return false;
	}
	function list_product_banner($date) {
		global $db;
		if (isset($date['ids'])) $date['ids']=explode(",",$date['ids']);
		?>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Cauta pachet dupa nume')?></label>
			</dt>
			<dd><input type="text" name="product_name" id="product_name" style="width:460px;" onkeyup="do_product_filter2();" />
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Pachet promo')?></label>
			</dt>
			<dd id="list_products_dd2">
				<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
					<?php $this->json_products2(); ?>
				</select>

			</dd>
		</dl>
		<div class="list_products_selected all_options_type options_type_2">
				<?php

					foreach ($date['ids'] as $id_now) {

						if (!empty($id_now)) {
							$sql="select
										p.*,
										pd.*
									from
								`xp_products` p

								inner join `xp_products_data` pd
									on p.`id`=pd.`id_product`

								WHERE p.`id`=".$id_now;
							$product=$db->fetch($sql);
							//print_a($product);
							?><div ><input type="hidden" name="products[]" value="<?=$product['id_product']?>" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b><?=$product['code']?> - <?=$product['name']?></b></div><?php
						}
					}

				?>
				</div>
		<?php
	}


	function list_product_banner2($date) {
		global $db;
		//if (isset($date['id_personalize'])) $date['id_personalize']=explode(",",$date['ids']);
		?>

		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Tipuri de personalizare')?></label>
			</dt>
			<dd id="list_products_dd3">
				<select name="products_3"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product3(this);">
					<?php $this->json_products3(); ?>
				</select>

			</dd>
		</dl>
		<div class="list_products_selected2 all_options_type options_type_2">
				<?php



						if ($date['id_personalize']>0) {
							$sql="select
										p.*
									from
								`prefix_products` p


								WHERE p.`id`=".$date['id_personalize'];
							$product=$db->fetch($sql);
							//print_a($product);
							?><div ><input type="hidden" name="products2[]" value="<?=$product['id']?>" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b><?=$product['name']?></b></div><?php
						}


				?>
				</div>
		<?php
	}

	function return_images($data) {
		return fa("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$data['id']." ORDER BY `id` ASC");
	}
	function show_select_currencies() {
		$_GET['from_grid']=1;
		$this->json_currencies();
	}
	function show_select_furnizori() {
		$_GET['from_grid']=1;
		$this->json_providers();
	}
	function show_select_brands() {
		$_GET['from_grid']=1;
		$this->json_brands();
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function json_list() {
		if (isset($_GET['idprod'])) {
			$new_sql="select sql_calc_found_rows
					p.*,
					pd.*,
					(select group_concat(concat_ws(',',id_category)) from `xp_products_to_categories` where `id_product`=pd.`id_product`) id_category

				from
					`{$this->table}` p
					inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`

							where p.`id`=".$_GET['idprod'];
	} else{

		$new_sql="select sql_calc_found_rows
					p.*,
					pd.*,
					(select group_concat(concat_ws(',',id_category)) from `xp_products_to_categories` where `id_product`=pd.`id_product`) id_category

				from
					`{$this->table}` p
					inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`
					where 1";
	}

		//print_a($new_sql);
		json_list($this,false,$new_sql);
	}

	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		#uploaded_images img{ cursor:move; }
		.module_menu li.update_all { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.update_all_filters { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.port_categories { background-image:url('../../images/icons/products_port.png'); }
		.module_menu li.verify_codes { background-image:url('../../images/icons/products_verify_codes.png'); }
		.module_menu li.export_codes { background-image:url('../../images/icons/products_export_codes.png'); }
		.module_menu li.update_provider { background-image:url('../../images/icons/update_provider.png'); }
		.module_menu li.update_multistatus { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.product_clon { background-image:url('../../images/icons/update_multistatus.png'); }
		.module_menu li.update_cadou { background-image:url('../../images/icons/update_cadou.png'); }

		#content,#menu { width:98%;margin:0 1%; }
		#wrapper{width:110px;height:30px;position:relative;}
		#wrapper .button{cursor:pointer;display:block;height:30px;width:110px;background:transparent url('../../images/browse.png') no-repeat;border:0;}
		#wrapper .button:hover,#wrapper .button:focus{outline:none;}
		#files ol{cursor:auto;list-style:none;marker-offset:auto;position:relative;padding:10px 0 0 0;}
		#files li{padding:90px 0 0 0;position:relative;width:110px;float:left;font-size:11px;text-shadow:0 0 0;margin-right:8px;text-align:center;color:#5196ea;}
		#files span{cursor:pointer;color:red;font-size:12px;font-weight:bold;text-decoration:underline;}
		em {color:#000;font-size:12px;font-weight:bold;}
		#files li .img{position:absolute;top:0;left:0;width:110px;height:88px;border:1px solid #ccc;overflow:hidden;}
		#files p{margin:7px 0;}
		#files em{font:normal 11px "Tahoma";}
		body{position:relative;}



		body{position:relative;}
		.expanding{padding:5px 0 0 15px;margin-bottom:10px;border-left:1px solid #333;}
		#main-expander{list-style:none;}
		.nss_grid table th a { height:40px }

		<?php
	}

	function list_specifications($date) {
		global $db;
		?>
		<div id="list_specifications">
		<?php  $this->json_list_specifications($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function show_furnizori($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_furnizor"><?=l('Furnizor')?></label>
			</dt>
			<dd>
				<select name="id_furnizor" id="id_furnizor" style="width:200px">
				<option value="0">Fara furnizor</option>
						<?php

						$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
						while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$date['id_furnizor'])?'selected':''?> > <?=$linie['name']?> </option>
			<?php
						}
		 ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function json_list_specifications($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `xp_categories` ");
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `id_category`,`order` asc " );
		foreach ($filters as $filter) {
			print_form_header ($filter['name']);
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $specification_info=$db->fetch("SELECT `value`,`front` FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".$id." AND `id_option`=".$option['id']);
		?>
		<dl>
		<dt>
			<label for="products_specification_<?=$option['id']?>"><?=$option['name']?></label>
		</dt>
		<dd>
			<input type="checkbox" name="specifications_show[<?=$option['id']?>]" value="1" align="absmiddle"  <?=(($specification_info['front']==1)?'checked':'')?> />
			<input type="text"  style="width:60%;" name="specifications[<?=$option['id']?>]" value="<?=$specification_info['value']?>" align="absmiddle" />

		</dd>
		</dl>
		<?php
			}
			print_form_footer();
		}
	}
	function json_list_filters($id_category=0,$ids=0) {?>
		<div id="list_filters">
		<?php $this->json_list_filter2($id_category,$ids); ?>
		</div>
		<?php
	}
	function list_filters($date) {
		?>
		<div id="list_filters">
		<?php $this->json_list_filter($date['id_category'],$date['id']); ?>
		</div>
		<?php
	}
	function json_list_filter($id_category=0,$id=0) {
		global $db;
		//echo a;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `xp_categories` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." order by `order` asc " );
		//echo "SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." order by `order` asc ";
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label>
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
				//				print_a("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function json_list_filter2($id_category=0,$id=0) {
		global $db;
		if (empty($id_category)) $id_category=$db->fetch_one("SELECT MAX(id) FROM `xp_categories` ");
		if ($id!=0) if (strpos(',',$id)!==0) $id_product="AND `id_product` IN ({$id})";
		if (empty($id_product)) $id_product="AND `id_product`={$id}";
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {
		?>
		<dl>
		<dt>
			<label for="products_filter_<?=$filter['id']?>"><?=$filter['name']?></label><br />
			<input type="checkbox" name="ignore[]" value="<?=$filter['id']?>" /> Ignora
		</dt>
		<dd>
			<?php
			$options=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS_OPTIONS."` WHERE id_filter=".$filter['id']." order by `order` asc " );
			foreach ($options as $option) {
				if (!empty($id)) $exist=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_VALUES."` WHERE id_filter=".$filter['id']." {$id_product} AND `id_option`=".$option['id']);
			?>
			<div><input type="checkbox" name="filters[<?=$filter['id']?>][]" value="<?=$option['id']?>" <?=($exist?'checked':'')?> /> <?=$option['name']?></b></div>
			<?php } ?>
		</dd>
		</dl>
	<?php }
	}
	function list_variations_tab2($date) {
		global $db;
		$all_variations=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_NAME."` WHERE status=1 ORDER BY `name` ASC");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Variation')?></label>
			</dt>
			<dd>
			<select name="id_variation" onchange="list_variations_options(this.value,<?=(int)$date['id']?>);">
			<option value="0"> <?=l('Choose a variation')?> </option>
				<?php foreach ($all_variations as $variation) { ?>
				<option value="<?=$variation['id']?>" <?=($date['id_variation']==$variation['id']?'selected':'')?>><?=ehtml($variation['name'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<br clear="all" />
		<div id="list_variations_options" class="product_have_variations">
		<?php
		if (!empty($date['id_variation'])) {
			$this->list_variations_options2($date['id_variation'],$date['id']);
		}
		?>
		</div>
		<?php
	}
	function get_rec($j=0) {
		$variation='';
		$variation_name='';
		$k=0;
		$max=sizeof($this->option_info);
		foreach ($this->option_info as $option) {
			$k++;
			foreach ($this->value_all_info[$option['id']] as $value) {
				$variation_temp=$variation.$value['id'];
				if ($k<$max) $variation_temp.="-";
				if ((!in_array($variation_temp,$this->all_variations))) {
					$variation.=$value['id'];
					$variation_name.=$value['name'];
					if ($k<$max) {
						$variation.="-";
						$variation_name.=" - ";
					}
					$array_variation=explode("-",$variation);
					$end_array_variation=end($array_variation);
					if ((sizeof($array_variation)==$max ) && (!empty($end_array_variation))) {
						$this->all_variations_name[$j]=$variation_name;
					}
					$this->all_variations[$j]=$variation;
					break;
				}
			}
		}
		if (!empty($this->all_variations[$j])) $this->get_rec($j+1);
	}
	function list_variations_options2($id_variation,$id_product) {
		global $db;
		if (!empty($id_variation)) {
			$this->option_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`='".$id_variation."' ORDER BY `id`");

			$this->value_all_info=array();
			foreach ($this->option_info as $option) {
				$value_info=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`='".$option['id']."'");
				$this->value_all_info[$option['id']]=$value_info;
			}
			$this->get_rec();
			$product_info_variations=array();
			if (!empty($id_product)) {
				$resursa=$db->sql("SELECT * FROM `".TABLE_VARIATIONS."` WHERE `id_product`=".$db->escape($id_product));
				while ($product_variations=$db->fetch($resursa)) {
					$product_info_variations[str_replace("-","_",$product_variations['variation'])]=$product_variations;
				}
			}
?>
<input type="hidden" name="variations[nss_all_variations]" id="nss_all_variations" />
<div style="width:856px;">
		<table cellpadding="0" cellspacing="0" border="0"  id="table_variations" >
			<thead>
			<tr>
				<th style="width:20px"><input type="checkbox" name="aaaaaa" /></th>
				<th><?=Variation_name?></th>
				<th style="width:72px"><?=Cod?></th>
				<th style="width:72px"><?=Stoc?></th>
				<th style="width:140px"><?=Pret?></th>
				<th style="width:140px"><?=Greutate?></th>
				<th style="width:200px;"><?=imagine?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($this->all_variations_name as $key=>$variation_name) {
				$var=str_replace("-","_",$this->all_variations[$key]);
			?>
			<tr id="tr_<?=$var?>">
				<td align="center" >
					<input type="checkbox" name="variations[<?=$var?>][variation]" value="<?=$var?>" <?=(isset($product_info_variations[$var])?'checked':'')?> />
				</td>
				<td >
					<?=$variation_name?>
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][code]" size="8" value="<?=$product_info_variations[$var]['code']?>" />
				</td>
				<td>
					<input type="text" name="variations[<?=$var?>][stock]" size="8" value="<?=$product_info_variations[$var]['stock']?>" />
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][price_type]">
					<?php $price_type=$product_info_variations[$var]['price_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($price_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($price_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($price_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($price_type==0)?'style="display: none;"':''?>>
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][price]" value="<?=$product_info_variations[$var]['price']?>" />
					</span>
				</td>
				<td>
					<select onchange="if(this.selectedIndex>0) { $(this).parent().find('span').show(); $(this).parent().find('span input').focus(); $(this).parent().find('span input').select(); } else { $(this).parent().find('span').hide(); } " name="variations[<?=$var?>][weight_type]">
					<?php $weight_type=$product_info_variations[$var]['weight_type']; ?>
						<option value="0" >No Change</option>
						<option value="1" <?=($weight_type==1)?'selected':''?>>Add</option>
						<option value="-1" <?=($weight_type==-1)?'selected':''?>>Subtract</option>
						<option value="2" <?=($weight_type==2)?'selected':''?>>Fixed</option>
					</select>
					<span <?=($weight_type==0)?'style="display: none;"':''?> >
						<input type="text" style="width: 40px;" name="variations[<?=$var?>][weight]"  value="<?=$product_info_variations[$var]['weight']?>" />
					</span>
				</td>
				<td>
					<?php if (!empty($product_info_variations[$var]['image'])) { ?>
						<div id="image_delete_<?=$var?>">
						<span style="display:none;"><img src="<?=$gd->url('resize','../'.$product_info_variations[$var]['image'],'260x260s-#f6f1f6')?>" alt="<?=$variation_name?>" /></span>
								<a href="#" class="toolTipInfoImg" title="<?=$variation_name?>"><?=$variation_name?></a>
								&nbsp; &raquo; &nbsp;
								<a class="Action" href="javascript: delete_variation_image('image_delete_<?=$var?>',<?=$product_info_variations[$var]['id']?>);"><?=Delete?></a>
						</div>
					<?php } else { ?>
						<input type="file" name="variations[<?=$var?>][image]" />
					<?php } ?>

				</td>
			</tr>

			<?php
			}
			?>
		</tbody>
		</table>
		</div>

		<!--	<tr id="tr_next_<?=$var?>" class="bodyTr" onmouseover="this.className='bodyTrHover'; $('#tr_<?=$var?>').attr('class','bodyTrHover');" onmouseout="this.className='bodyTr';$('#tr_<?=$var?>').attr('class','bodyTr');">
			<td colspan="7" style="padding-bottom:4px; border-bottom:1px solid #EFE2F0;"><input type="text" style="width:99%" name="short_description[]" value="<?=$product_info_variations[$var]['short_description']?>"/></td>
			</tr>-->
<?php }
	}


	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_brand"><?=l('Producator')?></label>
			</dt>
			<dd>
				<select name="id_brand" id="id_brand" style="width:200px">
				<?php $this->json_brands($date['id_brand']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga producator')?>" onclick="add_brand();" />
			</dd>
		</dl>
		<?php
	}
	function show_personalizare($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_personalize"><?=l('Tip personalizare')?></label>
			</dt>
			<dd>
				<select name="id_personalize" id="id_personalize" style="width:200px">
				<option value="0" > Nicio personalizare </option>
				<?php $this->json_personalizare($date['id_personalize']) ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}

	function json_providers($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_FURNIZORI."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_provider)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_brands($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_BRANDS."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder' => 'desc','loadComplete'=>$loadComplete),'',$after);
?>
$('#main-expander a').live('click',function(){$(this).next().slideToggle();});

function add_category_to_product(){
	var $new_val=$('#id_category').val();
	var $new_name=$('select#id_category option[value="'+$new_val+'"]').html();
	if($('#selected-categories').find('input[value="'+$new_val+'"]').length==0){
		$('#selected-categories').append('<div class="value-'+$new_val+'"><input type="hidden" name="id_category[]" value="'+$new_val+'"/>'+$new_name+'&nbsp;<a style="color:#036;">sterge</a></div>');
	}
}
$('#selected-categories a').live('click',function(){
	$(this).parent().remove();
});

var global_window=true;
var window_width=1050;
var window_add_edit_name_brand="<?=l('Add Brand')?>";
var window_add_edit_name_category="<?=l('Add Category')?>";
var window_add_edit_name_currency="<?=l('Add Currency')?>";
function do_product_filter(id,div) {
			var data="related_id_category="+$("#related_id_category").val()+"&related_product_name="+$("#related_product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
}});
}
		function export_applicanti(type,an) {
			var gr = nss_grids['<?=$this->module?>'].ids();
			window.location='module/<?=$this->module?>.php?action=do_export_excel&type='+type;
			$('#window_<?=$this->module?>_exporte2').dialog( 'close' );
		}
$(document).ready(function(){

				$(".module_menu .export_excel").click(function(){
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_exporte2",window_add_edit_name,'module/<?=$this->module?>.php?action=export_excel&to='+gr,600,function(){},true);
			});
		});
function do_product_filter2(id,div) {
			var data="id_category="+$("#id_category2").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter2=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd2').html(r);

					}
				});
		}
function do_product_filter3(id,div) {
			var data="id_category="+$("#id_category2").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter3=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd3').html(r);

					}
				});
		}
function do_product_filter5(id,div) {
			var data="id_category="+$("#related_id_category2").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter5=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd5').html(r);

					}
				});
		}
function select_product2(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected').append('<div ><input type="hidden" name="products[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}

function select_product3(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected2').append('<div ><input type="hidden" name="products2[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}
function select_product4(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected3').append('<div ><input type="hidden" name="products4[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}
function select_product5(id_this) {

			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected5').append('<div ><input type="hidden" name="products5[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function list_variations_options(id_var,id){
	$.ajax({
		data :"",
		type :"POST",
		url :'module/<?=$this->module?>.php?list_variations_options=1&id_var='+id_var+'&id='+id,
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#list_variations_options').html(r);
			/*
			tableToGrid("#table_variations");
			$("#gbox_table_variations").removeClass("ui-corner-all");
			$("#gview_table_variations .ui-jqgrid-bdiv").attr("style","height:360px; overflow-y:scroll; overflow-x:hidden;");
			*/
		}});
}

function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function load_brands(){
	$("#id_brand").load('module/<?=$this->module?>.php?json_brands=1');
}
function load_providers(){
	$("#id_furnizor").load('module/<?=$this->module?>.php?json_providers=1');
}
function json_currencies(){
	$("#currency").load('module/<?=$this->module?>.php?json_currencies=1');
}
function add_currency(){
	var module_now='currencies';
	nss_win(module_now + '_new', window_add_edit_name_currency,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',json_currencies);
	});
}
function add_brand(){
	var module_now='brands';
	nss_win(module_now + '_new', window_add_edit_name_brand,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_brands);
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function update_all_specification(id_category){
	$("#update_all_specification").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category);
}
function update_all_filter(id_category,ids){
	$("#update_all_filter").load('module/<?=$this->module?>.php?json_list_filters=1&id_category='+id_category+'&ids='+ids);
}
function update_all_category(id_category,ids){
	$("#update_all_category").load('module/<?=$this->module?>.php?json_list_categories=1&id_category='+id_category+'&ids='+ids);
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
	$(document).ready(function(){

			$("#main").css('height','1300');
			$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();

				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);

			});

			$(".module_menu .update_all_filters").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all_filters&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .port_categories").click(function(){
				var window_add_edit_name='<?=l('Porteaza categorie')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=port_categories&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .verify_codes").click(function(){
				var window_add_edit_name='<?=l('Verify codes')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=verify_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .export_codes").click(function(){
				var window_add_edit_name='<?=l('Export codes')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=export_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_provider").click(function(){
				var window_add_edit_name='<?=l('Aloca provider')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_provider&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_cadou").click(function(){
				var window_add_edit_name='<?=l('Update cadou')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_cadou&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_multistatus").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();

				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_multistatus&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .product_clon").click(function(){
				var window_add_edit_name='<?=l('Cloneaza produs')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=product_clon&id='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
						$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Muta produse')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
			$(".module_menu .upgrade").click(function(){
				var window_add_edit_name='<?=l('Cloneaza produs')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=upgrade&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .price_all").click(function(){
				var window_add_edit_name='<?=l('Update pret produse')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=price_all&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
			$(".module_menu .keywords").click(function(){
				var window_add_edit_name='<?=l('Keywords')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=keywords&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
			$(".module_menu .facebook").click(function(){
				var window_add_edit_name='<?=l('Aplica discount FB')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=facebook&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});
			$(".module_menu .reset_all").click(function(){
				var window_add_edit_name='<?=l('Reset pret produse')?>';
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=reset_all&ids='+gr,800, function () {

				after_window_load('<?=$this->module?>','edit');

				},true);
			});

	});
<?php
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function json_product_filter() {
		?>
		<select name="products" multiple="multiple" size="20" style="width:560px" id="products">
		<?php $this->json_products(array(),$_POST['related_id_category'],$_POST['related_product_name']) ?>
		</select>
	<?php
	}
	function json_products($ids=array(),$related_id_category=0,$related_product_name="") {
		global $db;
		$sql_filters=array();
		if (!empty($related_id_category)) $sql_filters[]=" `".TABLE_PRODUCTS."`.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$related_id_category})";
		if (!empty($related_product_name)) $sql_filters[]=" `".TABLE_PRODUCTS."`.`name` LIKE '%".$related_product_name."%' ";
		$sql="SELECT * FROM `".TABLE_PRODUCTS."`
			WHERE `".TABLE_PRODUCTS."`.`status`='1' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC";
		//echo $sql;
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) { $i++;
		if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
		}
		?>
		<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> > <?=$linie['name']?> </option>
		<?php
		}
	}
	function json_products2($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		if (!empty($product_name)) $sql_filters[]=" ( pd.`name` LIKE  '%".$product_name."%' OR pd.`code` LIKE  '%".$product_name."%' ) ";
		$sql="select		p.*,
							pd.*

						from
							`xp_products` p

							inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

							where  `type`=1 ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")."   ORDER BY pd.`name` ASC limit 50";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
				$i++;
				if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
				}
			?>
			<option value="<?=$linie['id_product']?>" <?=((in_array($linie['id_product'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}

	function json_products4($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		if (!empty($product_name)) $sql_filters[]=" ( pd.`name` LIKE  '%".$product_name."%' OR pd.`code` LIKE  '%".$product_name."%' ) ";
		$sql="select		p.*,
							pd.*

						from
							`xp_products` p

							inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

							where  `type`=0 ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")."   ORDER BY pd.`name` ASC limit 50";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
				$i++;
				if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
				}
			?>
			<option value="<?=$linie['id_product']?>" <?=((in_array($linie['id_product'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function json_products5($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		if (!empty($id_category)) $sql_filters[]=" p.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$id_category})";
		if (!empty($product_name)) $sql_filters[]=" ( pd.`name` LIKE  '%".$product_name."%' OR pd.`code` LIKE  '%".$product_name."%' ) ";
		$sql="select		p.*,
							pd.*

						from
							`xp_products` p

							inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

							where  `status`=1 and `type`=0 ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")."   ORDER BY pd.`name` ASC limit 50";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
				$i++;
				if ($i>100) {
					?>
					<option value="0" > <?=l('And more ...')?> </option>
					<?php
					break;
				}
			?>
			<option value="<?=$linie['id_product']?>" <?=((in_array($linie['id_product'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function json_products3($ids=array(),$id_category=0,$product_name="") {
		global $db;


		$sql="select		p.*

						from
							`prefix_products` p


							   ORDER BY p.`name` ASC limit 100";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
			?>
			<option value="<?=$linie['id']?>" <?=((in_array($linie['id'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function json_personalizare($id_brand=0) {
			global $db;
			if ($_GET['from_grid']==1) { ?>
				<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
				<?php
			}
			$resursa=$db->query("SELECT * FROM `prefix_products` ORDER BY `name` ASC");
			while ($linie=$db->fetch($resursa)) {
			?>
				<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
				<?php
			}
	}
	function json_product_filter2() {
		?>
		<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
			<?php $this->json_products2(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_product_filter3() {
		?>
		<select name="products_3"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product3(this);">
			<?php $this->json_products3(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_product_filter4() {
		?>
		<select name="products4"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product4(this);">
			<?php $this->json_products4(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_product_filter5() {
		?>
		<select name="products5"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product5(this);">
			<?php $this->json_products5(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function json_list_row($row) {
		$admin_info=adm_info();
		global $gd,$db;
		$row['id_parent']=$db->fetch_one("select `id_parent` from `xp_categories` where `id` in (".$row['id_category'].")");
//		$row['id_category']=$db->fetch_all("select `name` from `xp_categories_data` WHERE `_id` in (select `id_category` from `xp_products_to_categories` where `id_product`={$row['id']}) ");
//		if ($row['id_category']==false) {
//			$row['id_category']='<span style="color:red;">'.l('Nu exista categorie').'</span>';
//		}else{
//			foreach($row['id_category'] as$r)
//				$all_artists[]=$r['name'];
//			$row['id_category']=implode(', ',$all_artists);
//		}

//		$sc['cat_link'] = categories_link($sc['id_parent']);
//		$sc['link'] = lnk( 'category', $sc );

		$row['id_category']=$db->fetch_all("select `name` from `xp_categories_data` WHERE `_id` in ({$row['id_category']}) AND `lang`='".LANG."'");
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Nu exista categorie').'</span>';
		}else{
			foreach($row['id_category'] as$r)
				$all_artists[]=$r['name'];
			$row['id_category']=implode(', ',$all_artists);
		}
		$row['id_brand']=$db->fetch_one("SELECT `name` FROM `".TABLE_BRANDS."` WHERE `id`=".$row['id_brand']);
		if ($row['id_brand']==false) {
			$row['id_brand']='<span style="color:red;">'.l('Producatorul nu mai exista').'</span>';
		}
		if (!empty($row['id_furnizor'])) $row['id_furnizor']=$db->fetch_one("SELECT `name` FROM `".TABLE_FURNIZORI."` WHERE `id`=".$row['id_furnizor']);
		if ($row['id_furnizor']==false) {
			$row['id_furnizor']='<span style="color:red;">'.l('Furnizorul nu mai exista').'</span>';
		}
		if ($row ['is_new_crap'] == 1) {
			$row ['is_new_crap'] = '<img class="ch_staus_img" rel="is_new_crap" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_new_crap'] = '<img class="ch_staus_img" rel="is_new_crap" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		if ($row ['is_promo_crap'] == 1) {
			$row ['is_promo_crap'] = '<img class="ch_staus_img" rel="is_promo_crap" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_promo_crap'] = '<img class="ch_staus_img" rel="is_promo_crap" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		if ($row ['is_clearance_crap'] == 1) {
			$row ['is_clearance_crap'] = '<img class="ch_staus_img" rel="is_clearance_crap" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_clearance_crap'] = '<img class="ch_staus_img" rel="is_clearance_crap" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';


		if ($row ['is_new_stationary'] == 1) {
			$row ['is_new_stationary'] = '<img class="ch_staus_img" rel="is_new_stationary" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else	$row ['is_new_stationary'] = '<img class="ch_staus_img" rel="is_new_stationary" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		if ($row ['is_promo_stationary'] == 1) {
			$row ['is_promo_stationary'] = '<img class="ch_staus_img" rel="is_promo_stationary" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else	$row ['is_promo_stationary'] = '<img class="ch_staus_img" rel="is_promo_stationary" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		if ($row ['is_clearance_stationary'] == 1) {
			$row ['is_clearance_stationary'] = '<img class="ch_staus_img" rel="is_clearance_stationary" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else 	$row ['is_clearance_stationary'] = '<img class="ch_staus_img" rel="is_clearance_stationary" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';


		if ($row ['is_new_predator'] == 1) {
			$row ['is_new_predator'] = '<img class="ch_staus_img" rel="is_new_predator" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else	$row ['is_new_predator'] = '<img class="ch_staus_img" rel="is_new_predator" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		if ($row ['is_promo_predator'] == 1) {
			$row ['is_promo_predator'] = '<img class="ch_staus_img" rel="is_promo_predator" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else	$row ['is_promo_predator'] = '<img class="ch_staus_img" rel="is_promo_predator" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		if ($row ['is_clearance_predator'] == 1) {
			$row ['is_clearance_predator'] = '<img class="ch_staus_img" rel="is_clearance_predator" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else	$row ['is_clearance_predator'] = '<img class="ch_staus_img" rel="is_clearance_predator" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		$image=$db->fetch_one("SELECT `image` FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$row['id']." ORDER BY `id` ASC");
		if ($image) {
		$row['image']='<img src="'._static.'i/imagini-produse/78/'.$image.'" alt="'.$row['title'].'" />';
		} else {
			$row['image']='Fara imagine';
		}


		//$row['id_currency']=$db->fetch_one("SELECT code FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['id_currency']);
		$row['price']=number_format($row['price'],3,',','.');

		$row['cat_link'] = categories_link($row['id_parent']);
		$link = lnk( 'product', $row );
		$row['actions']="<a class=\"action\" href=\"".BASEHREF."admin/image.php?id_product=".$row['id']."\">Galerie foto</a><br/><br/><a class=\"action\" href=\"".$link."\" target=\"_blank\">Vezi produs</a><br><br/>".$row['actions'];


		return $row;
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') {
			global_delete($this->table);
		}
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".$id);
			foreach ($images as $image) unlink(SITEROOT."/".$image);
			$db->delete("xp_products_data",'id_product='.$id);
			$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_PRODUCTS_IMAGES,TABLE_FILTERS_VALUES)," `id_product`=".$id);

		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$data_saved=
			$db->fetch("
				select
					p.*,
					pd.*
				from
					`{$this->table}` p
					inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`


				where p.`id`={$id}
			");

			if (!empty($data_saved['id_variation'])) $data_saved['have_variations']=1;
			if (empty($data_saved['related_products'])) $data_saved['related_products_checkbox']=1;
			if ($data_saved['start_date']=='') $data_saved['start_date']=time();
			if ($data_saved['end_date']=='') $data_saved['end_date']=time()+(60*60*24*7);

			$ids2=$db->fetch_all("select * from `xp_products_to_categories` where `id_product`=".$id." order by `id`");
			foreach ($ids2 as $i) {
				$k2[]=$i['id_category'];
			}
			$data_saved['id_category']=implode(",",(array)$k2);
		}
		if ($data_saved['image']!=''&&file_exists(SITEROOT.'/static/'.$this->folder4.'/'.$data_saved['image'])){
			$data_saved['image']='static/'.$this->folder4.'/'.$data_saved['image'];
		} else $data_saved['image']='';
		//print_a($data_saved);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">

		<?php
		print_form($this->form,$this,$data_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">

		var reload_related_products=function() {
			if($('#related_products_checkbox').attr('checked')) {
				$(".related_products_select").hide();
			} else {
				$(".related_products_select").show();
			}
		}


		$(document).ready(function(){





			$('#related_products_checkbox').click(reload_related_products);
			//$('#have_variations input').click(reload_variations_product);
			reload_related_products();
			//reload_variations_product();


		});

		</script>
		<?php
	}
	function new_a_clone($id){
		global $db,$config,$gd;
		$product=$db->fetch("SELECT * FROM `".$this->table."` WHERE `id`=".(int)$id,MYSQL_ASSOC);
		unset(
		$product['code'],
		$product['id']
		);
		$product['date']=time();
		$product['views']=0;

		$db->insert($this->table,$product);
		$id_product=$db->insert_id();
		$product_desc=$db->fetch("SELECT * FROM `".$this->table.TABLE_EXTEND."` WHERE `id_main`=".(int)$id,MYSQL_ASSOC);
		$product_desc['id_main']=$id_product;
		unset($product_desc['id']);
		$product_desc['name']=$product_desc['name']." clona";
		$product_desc['name_seo']=$product_desc['name_seo']." clona";
		$db->insert($this->table.TABLE_EXTEND,$product_desc);

		$data_discount=$db->fetch_all("SELECT * FROM `".TABLE_PRODUCTS_DISCOUNT_RULES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_discount)) {
			foreach ($data_discount as $key=>$discount) {
				if (!empty($discount['discount'])) {
					$array_values=array(
					'id_product'=>$id_product,
					'quantity_from'=>$discount['quantity_from'],
					'quantity_to'=>$discount['quantity_to'],
					'discount'=>$discount['discount'],
					'type'=>$discount['type'],
					);
					$db->insert(TABLE_PRODUCTS_DISCOUNT_RULES,$array_values);
				}
			}
		}
		$data_specifications=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_specifications)) {
			foreach ($data_specifications as $key=>$value) {
				$array_values=array(
				'id_product'=>$id_product,
				'id_option'=>$value['id_option'],
				'value'=>$value['value'],
				'front'=>$value['front'],
				);
				$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_values);
			}
		}
		$data_specifications=$db->fetch_all("SELECT * FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`=".(int)$id);
		if (is_array($data_filters)) {
			foreach ($data_filters as $key=>$filters) {
				foreach ($filters as $filter) {
					$array_values=array(
					'id_product'=>$id_product,
					'id_filter'=>$filters['id_filter'],
					'id_option'=>$filters['id_option']
					);
					$db->insert(TABLE_FILTERS_VALUES,$array_values);
				}
			}
		}

		$this->new_a($id_product);
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;


		if ($data['id_category']==0) {
			print_error(l('Alege o categorie!'));
			exit();
		}
//		print_a($data);
//		die();
//		if ($data['related_products_checkbox']!=1) {
//			$data['related_products']=implode(",",(array)$data['products']);
//		} else $data['related_products']="";
		$data_discount=$data['discount'];
		$data_images=$data['images'];
		$data_image=$data['image'];
		$data_filters=$data['filters'];
		$data_variations=$data['variations'];
		$data_specifications=$data['specifications'];
		//$inser_categ=$data['id_category'];


		$start_date=strtotime ( $data['start_date'] );
		$end_date=strtotime ( $data['end_date'] );
		//print_a($data);
		$ids=$data['products'];
		$ids_related_category=$data['id_category'];
		$ids_related_products=$data['products4'];
		$ids_sample=$data['products5'];
//		if ($data['products_3']>0) {
//			$data['id_personalize']=$data['products_3'];
//		} else $data['id_personalize']=0;
		//$ids=implode(",",(array)$data['products']);
		$water=$data['water'];
		//print_a($ids);
		unset($data['water']);
		unset($data['id_category_related']);
		unset($data['related_id_category2']);
		unset($data['products4']);
		unset($data['filters']);
		unset($data['products_3']);
		unset($data['products4']);
		unset($data['products5']);
		unset($data['products_2']);
		unset($data['products2']);
		unset($data['ids']);
		unset($data['id_category']);
		unset($data['id_category2']);
		unset($data['images']);
		unset($data['image']);
		unset($data['discount']);
		unset($data['related_id_category']);
		unset($data['related_product_name']);
		unset($data['products']);
		unset($data['products_2']);
		unset($data['start_date']);
		unset($data['end_date']);
		unset($data['product_name']);
		unset($data['related_products']);
		unset($data['have_variations']);
		unset($data['variations']);
		unset($data['specifications']);
		unset($data['specifications_show']);
		unset($data['related_products_checkbox']);
		//if (empty($data['weight'])) $data['weight']=$db->fetch_one("SELECT `weight` FROM `xp_categories` WHERE `id`=".$data['id_category']);

		if ($id) {
			$data['name_seo']=escapeIlegalChars($data['name_seo']," ");
		} else {
			$data['name_seo']=escapeIlegalChars($data['name_seo']," ");
		}
			$data2=array(
					//'code'=>$data['code'],
					'youtube_1'=>$data['youtube_1'],
					'youtube_2'=>$data['youtube_2'],
					'youtube_3'=>$data['youtube_3'],
					'youtube_4'=>$data['youtube_4'],
					'youtube_5'=>$data['youtube_5'],
					'youtube_6'=>$data['youtube_6'],
					'youtube_7'=>$data['youtube_7'],
					'youtube_8'=>$data['youtube_8'],
					'name'=>$data['name'],
					'name_seo'=>$data['name_seo'],
					'short_description'=>$data['short_description']
					//'description'=>$data['description']
					);
		if (isset($data['description'])) $data2['description']=$data['description'];
		unset($data['code']);
		unset($data['youtube_1']);
		unset($data['youtube_2']);
		unset($data['youtube_3']);
		unset($data['youtube_4']);
		unset($data['youtube_5']);
		unset($data['youtube_6']);
		unset($data['youtube_7']);
		unset($data['youtube_8']);
		unset($data['warranty']);
		unset($data['water']);
		unset($data['canonical']);
		unset($data['waranty_description']);
		unset($data['delivery_description']);
		unset($data['transport_description']);
		unset($data['execution_description']);
		unset($data['points_description']);
		unset($data['customization_text']);
		unset($data['economy_description']);
		unset($data['buy_bubble']);
		unset($data['offer_bubble']);
		unset($data['personalize_bubble']);
		unset($data['price_bubble']);
		unset($data['now_description']);
		unset($data['advantage_1']);
		unset($data['advantage_2']);
		unset($data['advantage_3']);
		unset($data['advantage_4']);
		unset($data['name']);
		unset($data['name_seo']);
		unset($data['description']);
		unset($data['info']);
		unset($data['price_description']);
		unset($data['short_description']);
		//print_a($data);
		//print_a($data2);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			if (!empty($id)) $cond="AND `id_product`<>".$id;
			//$exist=$db->fetch_one("SELECT `id_product` FROM `".TABLE_PRODUCTS."_data` WHERE `code`='{$data2['code']}' {$cond}");
			if (!$exist) {
				close_window($this->module,$id);
				unset($data['type_op']);
				if (!empty($id)) {
					if (!empty($array_remove)) {
						foreach ($array_remove as $k=>$v)  {
							$db->delete(TABLE_PRODUCTS_IMAGES," image='".$v."'");

							delete_image_vars(true,$images);
						}
					}
					$db->delete(array(TABLE_SPECIFICATIONS_VALUES,TABLE_FILTERS_VALUES),"`id_product`=".$id);
					//print_a($data);
					//die();
					$db->update($this->table, $data," `id`=".$id);
					//$data2['name_seo']=$data2['name_seo']." ".$id;
					$db->update($this->table.'_data', $data2," `id_product`=".$id);
					//echo $inser_categ;
					//$db->update('xp_products_to_categories', array('id_category'=>$inser_categ)," `id_product`=".$id);
					print_alerta('a fost updatat');
				} else {
					$data['date']=time();
					$data['id_currency'] = 1;
					//$exist=$db->fetch_one("SELECT `id_product` FROM `".TABLE_PRODUCTS."_data` WHERE `code`='{$db->escape($data2['code'])}'");
//					if (!$exist) {
						$db->insert($this->table,$data);
						$id=mysql_insert_id();
						$data2['id_product']=$id;
						$data2['name_seo']=$data2['name_seo']." ".$id;
						$db->insert($this->table.'_data',$data2);
						//$db->insert('xp_products_to_categories', array('id_category'=>$inser_categ,'id_product'=>$id));
					//} else print_alerta('pe site mai exista un produs cu acelasi cod');
					print_alerta('a fost inserat');
				}
//				if ($data_image) {
//					$data_image=str_replace('static/'.$this->folder4.'/','',upload_images($data_image,escapeIlegalChars($data2['name'],"-"),'static/'.$this->folder4,''));
//					$exist=$db->fetch_one("SELECT `id_product` FROM `".TABLE_PRODUCTS."_previews` WHERE `id_product`='{$id}'");
//					if ($exist) {
//						$db->update($this->table.'_previews', array('image'=>$data_image)," `id_product`=".$id);
//					} else $db->insert('xp_products_previews', array('image'=>$data_image,'id_product'=>$id));
//				} else {
//					$db->delete(array(TABLE_PRODUCTS.'_previews')," `id_product`=".$id);
//				}
//

//				if ($ids) {
//					$db->delete("xp_products_to_promo","`id_product`=".$id);
//					foreach ($ids as $i) {
//						$db->insert("xp_products_to_promo",array('id_product'=>$id,'id_pack'=>$i));
//					}
//				} else $db->delete("xp_products_to_promo"," `id_product`=".$id);
//
				if ($ids_related_category) {
					$db->delete("xp_products_to_categories","`id_product`=".$id);
					foreach ($ids_related_category as $i) {
						$db->insert("xp_products_to_categories",array('id_product'=>$id,'id_category'=>$i));
					}
				} else $db->delete("xp_products_to_categories"," `id_product`=".$id);
//

				if (is_array($data_variations)) {
					foreach ($data_variations as $key=>$variation) {
						$variation['id_product']=$id;
						if (is_numeric($key) && $key!='new') {
							if (!empty($variation['variation_name'])) {
								$variation['variation']=$key;
								$db->update(TABLE_VARIATIONS,$variation,"`id`=".$key);
							} else $db->delete(TABLE_VARIATIONS,"`id`=".$key);
						} else {
							foreach ($variation['variation_name'] as $key2=>$variation_2) {
								if (!empty($variation_2)) {
									$variation_ins=array(
									'id_product'=>$id,
									'variation_name'=>$variation_2,
									'order'=>$variation['order'][$key2],
									'code'=>$variation['code'][$key2],
									'price_type'=>$variation['price_type'][$key2],
									'price'=>$variation['price'][$key2],
									'weight_type'=>$variation['weight_type'][$key2],
									'weight'=>$variation['weight'][$key2],
									);
									//print_a($variation_ins);
									$db->insert(TABLE_VARIATIONS,$variation_ins);
									$id_ins=$db->insert_id();
									$db->update(TABLE_VARIATIONS,array('variation'=>$id_ins),"`id`=".$id_ins);
								}
							}
						}
					}
				}
//				if ($ids_related_products) {
//					$db->delete("xp_products_to_recommended","`id_recommended`=".$id);
//					foreach ($ids_related_products as $i) {
//						$db->insert("xp_products_to_recommended",array('id_recommended'=>$id,'id_product'=>$i));
//					}
//				} else $db->delete("xp_products_to_recommended"," `id_recommended`=".$id);
//
//
//				//print_a($ids_sample);
//				if ($ids_sample) {
//					$db->delete("xp_products_to_sampler","`id_sample`=".$id);
//					foreach ($ids_sample as $i) {
//						//echo $i;
//						$db->insert("xp_products_to_sampler",array('id_sample'=>$id,'id_product'=>$i));
//					}
//				} else $db->delete("xp_products_to_sampler"," `id_sample`=".$id);


				//upload_images($data_images,escapeIlegalChars($data2['name_seo'],"-"),'static/'.$this->folder3,'600x450',$id,TABLE_PRODUCTS_IMAGES,'id_product',$water);

				refresh_images(TABLE_PRODUCTS_IMAGES,$id,'id_product','imagini-produse');


//				if ($ids) {
//					$exist=$db->fetch_one("SELECT `id_product` FROM `xp_promotions` WHERE `id_product`='{$id}'");
//					if ($exist) {
//						$db->update("xp_promotions", array('ids'=>$ids,'type'=>3,'name'=>'produs cadou','status'=>1,'start_date'=>$start_date,'end_date'=>$end_date,)," `id_product`=".$id);
//					} else $db->insert("xp_promotions",array('ids'=>$ids,'type'=>3,'name'=>'produs cadou','status'=>1,'id_product'=>$id,'start_date'=>$start_date,'end_date'=>$end_date,'date'=>time()));
//				} else $db->delete("xp_promotions","`type`=3 and `id_product`=".$id);
//				if ($ids) {
//					$db->insert("xp_promotions",array('ids'=>$ids,'type'=>3,'name'=>'produs cadou','status'=>1,'id_product'=>$id,'start_date'=>$start_date,'end_date'=>$end_date,'date'=>time()));
//				}
//				if (is_array($data_specifications)) {
//					foreach ($data_specifications as $key=>$value) {
//					 if (trim($value)!=''&&$id!=0) {
//
//						$array_values=array(
//						'id_product'=>$id,
//						'id_option'=>$key,
//						'value'=>$value,
//						'front'=>$_POST['specifications_show'][$key],
//						);
//						$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_values);
//					 }
//					}
//				}
//				$spec=calculate_spec($id);
//				calculate_promo();
//				$db->update(TABLE_PRODUCTS."_data",$update=array('specifications'=>$spec),'`id_product`='.$id);
//				if (is_array($data_filters)) {
//					foreach ($data_filters as $key=>$filters) {
//						foreach ($filters as $filter) {
//						 if ($id!=0) {
//							$array_values=array(
//							'id_product'=>$id,
//							'id_filter'=>$key,
//							'id_option'=>$filter
//							);
//							$db->insert(TABLE_FILTERS_VALUES,$array_values);
//						 }
//						}
//					}
//				}
			} else print_alerta('pe site mai exista un produs cu acelasi cod');
		} else {
			print_form_errors($errors,$form);
		}
	}
	function update_all($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_specification(this.value);">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_all_specification"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_provider($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_providers&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Furnizor'));
		?>
		<dl>
		<dt>
			<label for="update_furnizor"><?=l('Furnizor')?></label>
		</dt>
		<dd>
		<select name="provider" id="update_furnizor" style="width:300px" onchange="update_furnizor(this.value);">
			<?php $this->json_providers() ?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_furnizor"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_cadou($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_cadou&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Update cadou'));
		print_form($this->cadou,array(),array());
		?>
		<!--<dl>
		<dt>
		<label for="update_cadou"><?=l('Nume cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="nume_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Pret cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="pret_cadou" value="" align="absmiddle" />
		</dd>
		<dt>
			<label for="update_cadou"><?=l('Text cadou')?></label>
		</dt>
		<dd>
				<input type="text"  style="width:60%;" name="text_cadou" value="" align="absmiddle" />
		</dd>
		</dl>-->
		<?php
		print_form_footer();
		?>
		<!--<div id="update_cadou"></div>-->
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_multistatus($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_multistatus&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Statut'));
		?>
		<dl>
		<dt>
			<label for="update_multistatus"><?=l('Statut')?></label>
		</dt>
		<dd>
		<select name="statusx" id="update_multistatus" style="width:300px">
			<?php
			foreach ($this->orders_status as $key=>$status) {
				?>
				<option value="<?=$key?>"><?=$status?></option>
				<?php
			}
			?>
		</select>
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<div id="update_multistatus"></div>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_all_filters($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_all_filters&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_filter(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_filter"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function port_categories($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_port_categories&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="update_all_category"><?=l('Category')?></label>
		</dt>
		<dd>
		<select name="category" id="update_all_category" style="width:300px" onchange="update_all_category(this.value,'<?=$ids?>');">
			<?php $this->json_categories() ?>
		</select>
		</dd>
		</dl>
		<div id="update_all_category"></div>
		<?print_form_footer();
		print_form_buttons($main_buttons);?>
		</form>
		<?php
	}
	function verify_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=return_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Category'));
		?>
		<dl>
		<dt>
			<label for="verify_codes"><?=l('Insert codes with "," delimiter')?></label>
		</dt>
		<dd>
			<textarea name="verify_codes" id="return_codes"></textarea>
		</dd>
		</dl>
		<div id="return_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Check codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function export_codes($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=brand_codes&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Brand'));
		?>
		<dl>
			<dt>
				<label for="brand_codes"><?=l('Select brand to export codes')?></label>
			</dt>
			<dd>
				<select name="brand_codes">
				<?=$this->json_brands();?>
				</select>
			</dd>
		</dl>
		<dl>
			<dt>
				<label for="select_status"><?=l('Select status of products')?></label>
			</dt>
			<dd>
				<select name="select_status">
					<option value="1"><?=l('Active')?></option>
					<option value="0"><?=l('Inactive')?></option>
					<option value="x"><?=l('Ignore')?></option>
				</select>
			</dd>
		</dl>
		<div id="brand_codes"></div>
		<?print_form_footer();
		print_form_buttons(array('save'=>array('type'=>'submit','value'=>l('Export codes')),'reset'=>array('type'=>'reset','value'=>l('Reset')),'cancel'=>array('type'=>'button','value'=>l('Cancel'))));?>
		</form>
		<?php
	}
	function save_all(){
		global $db;
		$data_specifications=$_POST['specifications'];
		$ids=explode(",",r('ids'));
		foreach ($ids as $key=>$id) {
			$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id." AND `id_category`=".r('category'));
			if (empty($exist)) unset($ids[$key]);
		}
		close_window($this->module);
		if (is_array($data_specifications)) {
			//$db->delete(TABLE_SPECIFICATIONS_VALUES,"`id_product` IN (".implode(",",$ids).")");
			foreach ($data_specifications as $key=>$value) {
				foreach ($ids as $id) {
					$array_values=array(
					'front'=>$_POST['specifications_show'][$key],
					);
					if (!empty($value)) $array_values['value']=$value;
					$db->update(TABLE_SPECIFICATIONS_VALUES,$array_values," `id_product`=".$id." AND `id_option`=".$key);
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function save_providers(){
		global $db;
		$data_provider=$_POST['provider'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('id_furnizor'=>$data_provider),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_cadou(){
		global $db;
		//print_a($_POST);
		//die();
		$ids=explode(",",r('ids'));
		$ceva=$_POST['nss_languages'][LANG];
		//print_a($ceva);
		//die();
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS.TABLE_EXTEND,$ceva,"id_main=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_multistatus(){
		global $db;
		$data_multistatus=$_POST['statusx'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) {
			foreach ($ids as $key=>$id) {
				$exist=$db->fetch_all("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `id`=".$id);
				if (!empty($exist)) $db->update(TABLE_PRODUCTS,array('status'=>$data_multistatus),"id=".$id);
			}
			close_window($this->module);
			print_alerta('a fost updatat');
		} else {
			close_window($this->module);
			print_alerta('Nu ai selectat produse!');
		}

	}
	function save_port_categories(){
		global $db;
		$data_category=$_POST['category'];
		$ids=explode(",",r('ids'));
		if (!empty($ids)) foreach ($ids as $key=>$id) $db->update(TABLE_PRODUCTS."_to_categories",array('id_category'=>$data_category),"`id_product`=$id");
		print_alerta('a fost updatat');
	}
	function return_codes(){
		global $db;
		$ids=explode(",",r('verify_codes'));
		if (!empty($ids)) foreach ($ids as $k=>$code) {
			$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='{$code}'");
			if ($exist) $exists[]=$code; else $inexist[]=$code;
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile inexistente in baza de date sunt: <br />'.implode('<br />',$inexist).'<br /></div><div>codurile existe sunt: <br />'.implode('<br />',$exists).'<br /></div>');
	}
	function brand_codes(){
		global $db;
		$id_brand=r('brand_codes');
		$status=r('select_status');
		if (!empty($id_brand)) {
			if ($status!='x') $cond=" AND `status`={$status}"; else $cond='';
			$products=$db->fetch_all("SELECT `code` FROM `".TABLE_PRODUCTS."` WHERE `id_brand`={$id_brand} {$cond} ORDER BY `id_category` ASC");
			foreach ($products as $p) {
				$codes.=", ".$p['code'];
			}
		}
		close_window($this->module);
		print_alerta('<div style="color:#c00;">codurile existente in baza de date sunt: </div><div>'.ehtml(substr($codes,1)).'</div>');
	}
	function save_all_filters(){
		global $db;
		$data_filters=$_POST['filters'];
		$ignore=$_POST['ignore'];
		$ids=explode(",",r('ids'));

		foreach ($ids as $id) $db->delete(TABLE_FILTERS_VALUES,"`id_product`=".$id.(!empty($ignore)?" AND `id_filter` NOT IN (".implode(",",(array)$ignore).")":""));
		close_window($this->module);
		if (is_array($data_filters)) {
			foreach ($data_filters as $key=>$value) {
				foreach ($ids as $id) {
					if (!in_array($key,(array)$ignore)) {
						$db->delete(TABLE_FILTERS_VALUES," `id_product`={$id} AND `id_filter`={$key}");
						if (!empty($value)) {
							$array_values=array(
							'id_option'=>$value,
							'id_product'=>$id,
							'id_filter'=>$key,
							);
							$db->insert(TABLE_FILTERS_VALUES,$array_values);
						}
					}
				}
			}
			print_alerta('a fost updatat');
		}
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`=".(int)$id);
	}
	function do_export_excel() {
		global $db;
		//$countries=$db->fetch_all("SELECT * FROM `".TABLE_COUNTRIES."` ORDER by `name` ASC");
		if ($_GET['type']==3) $where=" WHERE ( p.`id` BETWEEN 20001 and 40000 ) and p.`status`='1'";
		if ($_GET['type']==2) $where=" WHERE ( p.`id` BETWEEN 10001 and 20000 ) and p.`status`='1'";
		if ($_GET['type']==1) $where=" WHERE ( p.`id` BETWEEN 1 and 10000 ) and p.`status`='1'";
		//ini_set("memory_limit","1024M");
		$products=$db->fetch_all("select
					p.*,
					pd.*,
					c.`id_category`

				from
					`{$this->table}` p
					inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`

					inner join `{$this->table}_to_categories` c
						on c.`id_product`=p.`id` ".$where."");
//		echo "select
//					p.*,
//					pd.*,
//					c.`id_category`
//
//				from
//					`{$this->table}` p
//					inner join `".TABLE_PRODUCTS."_data` pd
//								on p.`id`=pd.`id_product`
//
//					inner join `{$this->table}_to_categories` c
//						on c.`id_product`=p.`id` ".$where."";
//		die();
		//header('Content-type: application/xls');
		//header("Content-type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="Nomenclator-produse.xls"');
		header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
		//header("Pragma: no-cache");
		//header("Expires: 0");
		echo '
		<html xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:x="urn:schemas-microsoft-com:office:excel"
		xmlns="http://www.w3.org/TR/REC-html40">

		<head>
		<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
		<meta name=ProgId content=Excel.Sheet>
		<!--[if gte mso 9]><xml>
		 <o:DocumentProperties>
		  <o:LastAuthor>ExpertVision.ro</o:LastAuthor>
		  <o:LastSaved>{$date}</o:LastSaved>
		  <o:Version>10.2625</o:Version>
		 </o:DocumentProperties>
		 <o:OfficeDocumentSettings>
		  <o:DownloadComponents/>
		 </o:OfficeDocumentSettings>
		</xml><![endif]-->
		<style>
		<!--table
		{mso-displayed-decimal-separator:"\.";mso-displayed-thousand-separator:"\,";}
		@page{margin:1.0in .75in 1.0in .75in;mso-header-margin:.5in;mso-footer-margin:.5in;}
		tr{mso-height-source:auto;}
		col{mso-width-source:auto;}
		br{mso-data-placement:same-cell;}
		.style0{mso-number-format:General;text-align:general;vertical-align:bottom;white-space:nowrap;mso-rotate:0;mso-background-source:auto;mso-pattern:auto;color:windowtext;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Arial;mso-generic-font-family:auto;mso-font-charset:0;border:none;mso-protection:locked visible;mso-style-name:Normal;mso-style-id:0;}
		td{mso-style-parent:style0;padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:windowtext;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Arial;mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;border:none;mso-background-source:auto;mso-pattern:auto;mso-protection:locked visible;white-space:nowrap;mso-rotate:0;}
		.xl24{mso-style-parent:style0;white-space:normal;}
		-->
		</style>
		<!--[if gte mso 9]><xml>
		 <x:ExcelWorkbook>
		  <x:ExcelWorksheets>
		   <x:ExcelWorksheet>
			<x:Name>Registration</x:Name>
			<x:WorksheetOptions>
			 <x:Selected/>
			 <x:ProtectContents>False</x:ProtectContents>
			 <x:ProtectObjects>False</x:ProtectObjects>
			 <x:ProtectScenarios>False</x:ProtectScenarios>
			</x:WorksheetOptions>
		   </x:ExcelWorksheet>
		  </x:ExcelWorksheets>
		  <x:WindowHeight>10005</x:WindowHeight>
		  <x:WindowWidth>10005</x:WindowWidth>
		  <x:WindowTopX>120</x:WindowTopX>
		  <x:WindowTopY>135</x:WindowTopY>
		  <x:ProtectStructure>False</x:ProtectStructure>
		  <x:ProtectWindows>False</x:ProtectWindows>
		 </x:ExcelWorkbook>
		</xml><![endif]-->
		</head>

		<body link=blue vlink=purple>
		<table x:str border=0 cellpadding=0 cellspacing=0 style="border-collapse: collapse;table-layout:fixed;">';
		echo "<tr>
			<th class=xl24 width=300><b>Cod produs</b></td>
			<th class=xl24 width=300><b>Cod EAN</b></td>
			<th class=xl24 width=300><b>Nume produs</b></td>
			<th class=xl24 width=300><b>Categorie</b></td>
			<th class=xl24 width=300><b>Subcategorie</b></td>
			<th class=xl24 width=300><b>Producator</b></td>
			<th class=xl24 width=300><b>Garantia</b></td>
			<th class=xl24 width=300><b>Descriere</b></td></tr>";
		//echo 'An$Prenume$Nume$Firma$Telefon$Inaltime$Greutate$Nota';
		foreach ($products as $product) {
					$categories='';
					$categories1='';
					$brand='';
					$categories=$db->fetch("select `name`,`id_parent` from `xp_categories` WHERE `id`={$product['id_category']}");
					//$cid=$db->fetch_one("select `name` from `xp_categories` WHERE `id`={$product['id_category']}");
					if ($categories['id_parent']>0) $categories1=$db->fetch_one("select `name` from `xp_categories` WHERE `id`={$categories['id_parent']}");
					$brand=$db->fetch_one("select `name` from `".TABLE_BRANDS."` WHERE `id`={$product['id_brand']}");
					if ($brand=='') $brand='Fara Producator';


			echo "<tr>
				<td class=xl24 width=300>".$product['code']."</td>
				<td class=xl24 width=300>".$product['code_ean']."</td>
				<td class=xl24 width=300>{$product['name']}</td>
				<td class=xl24 width=300>".($categories1==''?$categories['name']:$categories1)."</td>
				<td class=xl24 width=300>".($categories1==''?'':$categories['name'])."</td>
				<td class=xl24 width=300>".$brand."</td>
				<td class=xl24 width=300>".$product['warranty']."</td>
				<td class=xl24 width=300>".strip_tags($product['description'])."</td></tr>";
			ob_flush();
		}
		 echo "</table></body></html>";
	}
	function export_excel($to='') {
		global $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			if (!file_exists(SITEROOT.'/'.$date_saved['image'])) $date_saved['image']='';
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Exporta excel'));
		?>
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 1')?>" onclick="export_applicanti(1);" />
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 2')?>" onclick="export_applicanti(2);" />
		<br />			<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Fisier 3')?>" onclick="export_applicanti(3);" />

		<br /><br />
	 <?php
	 print_form_footer();
			?>
			</form>
			<?php

		}

	function reset_all() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=reset_save_all&ids=<?=$_GET['ids']?>" method="POST">
		<?php
		print_form_header (l('Resetare preturi'));
		?>
		<dl>
			<dt><label for="price"><?=l('Vor fi resetate preturile la care au fost facute discounturi!')?></label></dt>

		</dl>

		<?php
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
		function reset_save_all(){
		global $db;
		if ($_SESSION['last_discount']+10<time()) {
			//print_a($_SESSION['last_discount']=$_POST['price'].$_POST['id_category']);

			if (!empty($_GET['ids'])) {


				//$cond=" AND `id_brand`={$_POST['id_category']}";
				$cond=" AND `id` IN ({$_GET['ids']})";
				$cond2=" AND `id_product` IN ({$_GET['ids']})";


				$db->sql("UPDATE `{$this->table}` SET `price`=`pricev` WHERE `price`<>0 ".$cond);
				$db->sql("UPDATE `{$this->table}_variations` SET `price`=`pricev` WHERE `price`<>0 ".$cond2);



			//unset($_SESSION['last_discount']);
			$_SESSION['last_discount']=time();
			print_alerta('a fost updatat');
			close_window($this->module);
		} else {
			print_alerta('Nu ati selectat niciun produs');
			close_window($this->module);
		}
		}
		else {
			print_alerta('Puteti face inca un update de pret peste 10 secunde');
			close_window($this->module);
		}
	}


	function facebook() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=facebook_save_all&ids=<?=$_GET['ids']?>" method="POST">
		<?php
		print_form_header (l('Aplica FB discount'));
		print_form($this->form_facebook,$this,$data_saved);
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}

	function facebook_save_all(){
		global $db;
		if ($_SESSION['last_discount']+10<time()) {
			//print_a($_SESSION['last_discount']=$_POST['price'].$_POST['id_category']);

			if (!empty($_GET['ids'])) {


				//$cond=" AND `id_brand`={$_POST['id_category']}";


				$db->sql("UPDATE `xp_products` SET `fb`='".$_POST['fb']."' WHERE `id` IN ({$_GET['ids']})");



			//unset($_SESSION['last_discount']);
			$_SESSION['last_discount']=time();
			print_alerta('a fost updatat');
			close_window($this->module);
		} else {
			print_alerta('Nu ati selectat niciun produs');
			close_window($this->module);
		}
		}
		else {
			print_alerta('Puteti face inca un update de pret peste 10 secunde');
			close_window($this->module);
		}
	}

	function price_all() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=price_save_all&ids=<?=$_GET['ids']?>" method="POST">
		<?php
		print_form_header (l('Modifica pret procentual'));
		?>
		<dl>
			<dt><label for="price"><?=l('Introduceti procent')?></label></dt>
			<dd><input type="text" name="price" value="" /> %</dd>
		</dl>

		<?php
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function price_save_all(){
		global $db;
		if ($_SESSION['last_discount']+5<time()) {
			//print_a($_SESSION['last_discount']=$_POST['price'].$_POST['id_category']);

			if (!empty($_GET['ids'])) {
			if (is_numeric($_POST['price'])) {

				//$cond=" AND `id_brand`={$_POST['id_category']}";
				$cond=" AND `id` IN ({$_GET['ids']})";
				$cond2=" AND `id_product` IN ({$_GET['ids']})";
				if ($_POST['price']>0) {

					$db->sql("UPDATE `{$this->table}` SET `pricev`=0 WHERE `price`<>0 ".$cond);
					$db->sql("UPDATE `{$this->table}_variations` SET `pricev`=0 WHERE `price`<>0 ".$cond2);
				} else {

					$db->sql("UPDATE `{$this->table}` SET `pricev`=`price` WHERE `price`<>0 ".$cond);
					$db->sql("UPDATE `{$this->table}_variations` SET `pricev`=`price` WHERE `price`<>0 ".$cond2);
				}

				$db->sql("UPDATE `{$this->table}` SET `price`=`price`*".(1+$_POST['price']/100)." WHERE `price`<>0 ".$cond);
				$db->sql("UPDATE `{$this->table}_variations` SET `price`=`price`*".(1+$_POST['price']/100)." WHERE `price`<>0 ".$cond2);

			}
			//unset($_SESSION['last_discount']);
			$_SESSION['last_discount']=time();
			print_alerta('a fost updatat');
			close_window($this->module);
		} else {
			print_alerta('Nu ati selectat niciun produs');
			close_window($this->module);
		}
		}
		else {
			print_alerta('Puteti face inca un update de pret peste 10 secunde');
			close_window($this->module);
		}
	}
	function keywords() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=keywords_save_all&ids=<?=$_GET['ids']?>" method="POST">
		<?php
		print_form_header (l('Cuvinte cheie'));
		?>
		<dl>
			<dt><label for="price"><?=l('Introduceti cuvintele cheie')?></label></dt>
			<dd><input type="text" name="price" value="" /></dd>
		</dl>

		<?php
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function keywords_save_all(){
		global $db;
		if ($_SESSION['last_discount']+10<time()) {
			//print_a($_SESSION['last_discount']=$_POST['price'].$_POST['id_category']);

			if (!empty($_GET['ids'])) {
			if ($_POST['price']!='') {

				//$cond=" AND `id_brand`={$_POST['id_category']}";


				$db->sql("UPDATE `xp_products_description` SET `meta_keywords`='".$_POST['price']."' WHERE `id_main` IN ({$_GET['ids']})");


			}
			//unset($_SESSION['last_discount']);
			$_SESSION['last_discount']=time();
			print_alerta('a fost updatat');
			close_window($this->module);
		} else {
			print_alerta('Nu ati selectat niciun produs');
			close_window($this->module);
		}
		}
		else {
			print_alerta('Puteti face inca un update de pret peste 10 secunde');
			close_window($this->module);
		}
	}

}
$module=new products_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) {
	if (isset($_GET['idprod'])) {
			$new_sql="select sql_calc_found_rows
							p.*,
							pd.*,
							(select group_concat(concat_ws(',',id_category)) from `xp_products_to_categories` where `id_product`=pd.`id_product`) id_category

						from
							`{$module->table}` p
							inner join `".TABLE_PRODUCTS."_data` pd
										on p.`id`=pd.`id_product`

							where p.`id`=".$_GET['idprod'];
	} else{

		$new_sql="select sql_calc_found_rows
					p.*,
					pd.*,
					(select group_concat(concat_ws(',',id_category)) from `xp_products_to_categories` where `id_product`=pd.`id_product`) id_category

				from
					`{$module->table}` p
					inner join `".TABLE_PRODUCTS."_data` pd
								on p.`id`=pd.`id_product`

					where 1";
	}
		json_list($module,false,$new_sql);
}
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_categories2']==1) $module->json_categories2(fget('id'));
elseif ($_GET['json_categories3']==1) $module->json_categories3(fget('id'));
elseif ($_GET['json_categories_tree']==1) $module->json_categories_tree();
elseif ($_GET['json_providers']==1) $module->json_providers(fget('id'));
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_personalizare']==1) $module->json_personalizare(fget('id'));
elseif ($_GET['json_currencies']==1) $module->json_currencies(fget('id'));
elseif ($_GET['json_list_filter']==1) $module->json_list_filter(fget('id_category'),fget('id'));
elseif ($_GET['json_list_specifications']==1) $module->json_list_specifications(fget('id_category'),fget('id'));
elseif ($_GET['json_list_filters']==1) $module->json_list_filters(fget('id_category'),fget('ids'));
elseif ($_GET['json_product_filter']==1) $module->json_product_filter();
elseif ($_GET['json_product_filter2']==1) $module->json_product_filter2();
elseif ($_GET['json_product_filter3']==1) $module->json_product_filter3();
elseif ($_GET['json_product_filter5']==1) $module->json_product_filter5();
elseif ($_GET['list_variations_options']==1) $module->list_variations_options(fget('id_var'),fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='product_clon') $module->new_a_clone(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));

elseif ($_GET['action']=='update_provider') $module->update_provider(fget('ids'));
elseif ($_GET['action']=='update_cadou') $module->update_cadou(fget('ids'));
elseif ($_GET['action']=='update_multistatus') $module->update_multistatus(fget('ids'));
elseif ($_GET['action']=='update_all_filters') $module->update_all_filters(fget('ids'));
elseif ($_GET['action']=='port_categories') $module->port_categories(fget('ids'));
elseif ($_GET['action']=='verify_codes') $module->verify_codes(fget('ids'));
elseif ($_GET['action']=='export_codes') $module->export_codes(fget('ids'));

elseif ($_GET['action']=='save_providers') $module->save_providers();
elseif ($_GET['action']=='save_cadou') $module->save_cadou();
elseif ($_GET['action']=='save_multistatus') $module->save_multistatus();
elseif ($_GET['action']=='save_all_filters') $module->save_all_filters();
elseif ($_GET['action']=='save_port_categories') $module->save_port_categories();
elseif ($_GET['action']=='return_codes') $module->return_codes();
elseif ($_GET['action']=='brand_codes') $module->brand_codes();
elseif ($_GET['action']=='export_excel') $module->export_excel(fget('to'));
elseif ($_GET['action']=='do_export_excel') $module->do_export_excel();

elseif ($_GET['action']=='update_all') $module->update_all(fget('ids'));
elseif ($_GET['action']=='price_all') $module->price_all(fget('ids'));
elseif ($_GET['action']=='keywords') $module->keywords(fget('ids'));
elseif ($_GET['action']=='facebook') $module->facebook(fget('ids'));
elseif ($_GET['action']=='reset_all') $module->reset_all(fget('ids'));
elseif ($_GET['action']=='save_all') $module->save_all();
elseif ($_GET['action']=='upgrade') $module->_upgrade(fget('ids'));
elseif ($_GET['action']=='do_upgrade') $module->do_upgrade();
elseif ($_GET['action']=='price_save_all') $module->price_save_all();
elseif ($_GET['action']=='keywords_save_all') $module->keywords_save_all();
elseif ($_GET['action']=='facebook_save_all') $module->facebook_save_all();
elseif ($_GET['action']=='reset_save_all') $module->reset_save_all();
else {

	print_header();
	$admin_info=adm_info();
	//if ($admin_info['username']=='admin') {
		print_content($module,array("update_all"=>l('Muta produse'),"price_all"=>l('Update pret %'),"reset_all"=>l('Reset pret'),'upgrade'=>l('Cloneaza produs'),"keywords"=>l('Keywords'),"facebook"=>l('Discount FB')));
//	} else {
//		print_content($module,array("port_categories"=>l('Porteaza categorie'),"verify_codes"=>l('Verificare coduri'),"export_codes"=>l('Exporta coduri'),"update_provider"=>l('Aloca provider'),"update_multistatus"=>l('Update statut')),'','',array('delete'));
//	}
	//$module->new_a(fget('id'));
	print_footer();
}
?>