<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class personalizeaza_var_module {
	var $module='personalizeaza_var';
	var $date='27-08-2009';
	var $table='prefix_products_variations';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $folder='';
	var $title='';
	var $description='';
	var $_id_product=0;
	function personalizeaza_var_module() {

		$this->name=l('Variatii produse personalizare');
		$this->title=l('Variatii produse personalizare');
		$this->description=l('Variatii produse personalizare');
		$this->folder='files';

		$this->type=array(
		'like'=>array('name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume variatie')),
		'id_product'=>array('name'=>l('Produs'),'width'=>200,'stype'=>'none'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume variatie')),
		1=>'select_product',
		2=>'select_var',
		'type'=>array('type'=>'radio','options'=>array(0=>l('Radio'),1=>l('Select'),2=>l('Textura'),3=>l('Icon')),'name'=>l('Tip variatie')),
//		'calcul'=>array('type'=>'radio','options'=>array(1=>l('Vizibil'),0=>l('Nu apare')),'name'=>l('Tab calcul')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'order'=>array('type'=>'input','name'=>l('Ordine')),
		);



	}
	function css() {
		?>

		<?php
	}
	function select_var_json($id_option=0) {
		global $db;
		$resursa=$db->query("SELECT * FROM `prefix_products_variations` WHERE `id_product`=".$this->_id_product." ORDER BY `name` ASC ");
		while ($linie=$db->fetch($resursa)) {

		?>
		  <optgroup label="<?=$linie['name']?>">
		  <?php

		  $resursa_2=$db->query("SELECT * FROM `prefix_products_variations_options` WHERE `id_variation`=".(int)$linie['id']." ORDER BY `name` ASC ");
		  while ($linie_2=$db->fetch($resursa_2)) {

					?>
					<option value="<?=$linie_2['id']?>" <?=($linie_2['id']==$id_option)?'selected':''?> style="padding-left:20px;" > <?=$linie_2['name']?> </option>
					<?php
		  }
		  ?>
		  </optgroup>
			
			<?php
		}
	}
	function select_var($date) {
		$id_option=$date['is_variation'];
		if (empty($id_option)) {
			$id_option="0";
		}

		if (!empty($date['id_product'])) {
			$this->_id_product=$date['id_product'];
		}
		?>
		<dl>
			<dt>
				<label for="id_option"><?=l('Optiune care da dependenta')?></label>
			</dt>
			<dd>
			<?php foreach (explode(",",$id_option) as $id_option){ if ($id_option=="") continue;  ?>
			<div style="padding:2px;">
				<select name="id_option[]" id="id_option" style="width:200px">
				<option value="0" selected> <?=l('Aceasta variatie este independenta')?> </option>
				<?php $this->select_var_json($id_option); ?>
				</select>

			<img src="images/addicon.gif" align="absmiddle" class="add" class="pointer" />
		 	<img src="images/delicon.gif" align="absmiddle" class="rem" class="pointer" />
		 	</div>
		 	
		 	<?php }  ?>
				</dd>
				</dl>
				<?php
	}
	function select_product($date) {
		?>
		<dl>
			<dt>
				<label for="id_product"><?=l('Tip produs')?></label>
			</dt>
			<dd>
				<select name="id_product" id="id_product" style="width:200px" onchange="$('#id_option').load('module/<?=$this->module?>.php?action=json_var&id_product='+this.value);">
				<?php $this->json_select_product($date['id_product']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_product($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1){
			$id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products` ");
			$this->_id_product=$id_brand;
		}
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this,false);
	}
	function js() {
		set_grid($this,array('multiselect'=>'true'));
		?>
		$(document).ready(function(){
			$(".module_menu .insert").click(function(){
			
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=insert',900, function () {
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				init_upload();
				},true);
			});
		});
		<?php
	}


	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function insert() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_import" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		?>
		<dl>
			<dt><label for="code">Fisier CSV:</label></dt>
		<dd>
		 	<input type="hidden" class="file" value="" name="code" id="code" rel="csv,<?=TMP_IMG?>,0" />
		</dd>
		</dl>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		
		</form>
		<?php
	}
	function json_list_row($row) {
		global $db;

		$row['id_product']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products` WHERE `id`=".$row['id_product']);
		return $row;
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_import(){
		global $db;
		if (!empty($_POST['code'])) {

			$handle = fopen(SITEROOT."/".$_POST['code'], "rb");
			$i=0;
			while (!feof($handle)) {
				$i++;
				$line = fgets($handle);
				$cols=explode(",",$line);
				if (empty($cols[1])) continue;
				if(!$verSerial=$db->fetch_one("select `id` from `".TABLE_PREPAID."` where `serial`='".trim($cols[0])."'")) {
					$db->insert($this->table,array(
					'code'=>trim($cols[1]),
					'serial'=>trim($cols[0]),
					'credits'=>(int)$cols[2],
					'date'=>time(),
					));
				}
			}
		?>
	<script type="text/javascript">
	$('#window_<?=$this->module?>_email').dialog( 'close' );
	</script>
<?php
print_alerta(l('Au fost importate'));
		} else print_alerta(l('Adaugati fisier'));

	}
	function save($id){
		global $db;
		$data=$_POST;
		//print_a($data);
		$data['is_variation']=",".implode(",",$data['id_option']).",";
		unset($data['id_option']);

		//$data['name_seo']=escapeIlegalChars($data['name']," ");
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new personalizeaza_var_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='insert') $module->insert();
elseif ($_GET['action']=='save_import') $module->save_import();
elseif ($_GET['action']=='json_var'){
	$module->_id_product=fget('id_product');
	$module->select_var_json();
} else {
	print_header();
	print_content($module);
	print_footer();
}
?>