<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
$this_module=array();
class languages_module {
	var $module='languages';
	var $date='25-08-2009';
	var $table=TABLE_LANGUAGES;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function languages_module() {

		$this->name=l('Limbi site');
		$this->title=l('Limbi site');
		$this->description=l('Limbi site');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Name'),'width'=>300),
		'prefix'=>array('name'=>l('prefix'),'width'=>80,'align'=>'center'),
		'default'=>array('name'=>l('default'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('nume'),'valid'=>'empty,unique,min_4,max_140'),
		'prefix'=>array('type'=>'input','name'=>l('prefix'),'valid'=>'empty,unique,min_2,max_4'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		'default'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('default')),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this);
	}
	function json_list_row($row) {
		if ($row['default']==1) {
			$row['default']='<img class="ch_default_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;" />';
		} else $row['default']='<img class="ch_default_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Add Edit Language'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new languages_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('rownumbers'=>'false'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>