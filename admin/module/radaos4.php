<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class radaos4_module {
	var	$module='radaos4';
	var $date='27-08-2009';
	var $table='xp_croaziera_transport_tax';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function radaos4_module() {

		$this->name=l('Taxe');
		$this->title=l('Taxe');
		$this->description=l('Taxe');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('id_furnizor','status','id','id_brand','id_category','type','is_adult')
		);

		$this->grid=array(
		0=>array('order'=>'id desc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),

		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>140,'stype'=>'select','options'=>'show_select_furnizori'),
		//'id_brand'=>array('name'=>l('Producator'),'width'=>140,'stype'=>'select','options'=>'show_select_brands'),
		//'id_category'=>array('name'=>l('Categorie'),'width'=>140,'stype'=>'select','options'=>'show_select_category'),
		'ruta'=>array('name'=>l('Plecare din'),'width'=>120),
		'id_region'=>array('name'=>l('Intoarcere din'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select2'),
		'id_country'=>array('name'=>l('Tara'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select2'),
		'price'=>array('name'=>l('Pret'),'width'=>70),
		'type'=>array('name'=>l('Tip'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';0:'.l('Dus').';1:'.l('Intors'))),
		'is_adult'=>array('name'=>l('Adult / Copil'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';0:'.l('Adult').';1:'.l('Copil'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',

		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//1=>'show_furnizori',
		2=>'show_brands',
		//3=>'show_categories',
		5=>'show_state',
		6=>'show_city',
		'price'=>array('type'=>'input','name'=>l('Pret'),'style'=>'width:20%;'),
		'type'=>array('type'=>'radio','options'=>array(0=>l('Dus'),1=>l('Intors')),'name'=>l('Tip')),
		'is_adult'=>array('type'=>'radio','options'=>array(1=>l('Adult'),0=>l('Copil')),'name'=>l('Adult / Copil')),
		'age_start'=>array('type'=>'input','name'=>l('Varsta de la'),'style'=>'width:20%;'),
		'age_end'=>array('type'=>'input','name'=>l('Varsta pana la'),'style'=>'width:20%;'),
		//'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:20%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'radaos_images'),
		);
	}
	function json_list() {
		json_list($this);
	}
	function show_select_furnizori() {
		$_GET['from_grid']=1;
		$this->json_providers();
	}
	function show_select_brands() {
		$_GET['from_grid']=1;
		$this->json_brands();
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function generate_select2() {
		$_GET['from_grid']=1;
		$this->json_state();
	}
	function json_providers($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_FURNIZORI."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
			$t=$db->fetch_one("select `name` from `xp_transport_data` WHERE `_id`={$linie['id_transport']} ");
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_provider)?'selected':''?> > <?=$linie['name'].' '.$t?> </option>
			<?php
		}
	}
	function show_state($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_country"><?=l('Tara')?></label>
			</dt>
			<dd>
				<select name="id_country" id="id_country" style="width:200px" onchange="json_list_filter2(this.value,<?=(int)$date['id_country']?>);">
				<?php $this->json_state($date['id_country']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function show_city($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_region"><?=l('Regiunea')?></label>
			</dt>
			<dd>
				<select name="id_region" id="id_region" style="width:200px" onchange="json_list_filter3(this.value,<?=(int)$date['id_region']?>);">
				<?php $this->json_city($date['id_country'],$date['id_region']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_state($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Selectati tara</option><?php
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `fibula_countries` p
							inner join `fibula_countries_data` pd
							 on p.`id`=pd.`_id` WHERE isnull(p.`id_parent`) ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_city($id_state,$id_brand=0) {
		global $db;

		if (empty($id_state)) {
			?><option>Selectati regiunea</option><?php
			return false;
		}
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `fibula_countries` p
							inner join `fibula_countries_data` pd
							 on p.`id`=pd.`_id` WHERE p.`id_parent`=".$id_state." ORDER BY `name` ASC");
		
	
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
//		if ($row['id_category']==0) {
//			$row['id_category']='<span style="color:red;">'.l('Toate categoriile').'</span>';
//		} else {
//			$row['id_category']=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."_data` WHERE `_id`=".$row['id_category']."");
//		}


		if ($row['id_region']) $row['id_region']=$db->fetch_one("select `name` from `fibula_countries_data` WHERE `_id`={$row['id_region']} ");
		if ($row['id_country']) $row['id_country']=$db->fetch_one("select `name` from `fibula_countries_data` WHERE `_id`={$row['id_country']} ");
		
		$t=$db->fetch("select * from `xp_croaziera_transport` WHERE `id`={$row['id_croaziera_transport']} ");
		$p=$db->fetch_one("select `name` from `xp_transport_data` WHERE `_id`={$t['id_transport']} and `lang`='ro' ");
		$row['ruta']=$t['name'].' - '.$p;
		if($row['type']==0) {
			$row['type']='Dus';
		} else {
			$row['type']='Intors';
		}
		if($row['is_adult']==1) {
			$row['is_adult']='Adult';
		} else {
			$row['is_adult']='Copil';
		}
		
		return $row;
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function json_list_filter2(id_country,id){
		var id_city=0;
		$("#id_region").load('module/<?=$this->module?>.php?json_city=1&id_country='+id_country+'&id='+id);
		//$("#id_city").load('module/<?=$this->module?>.php?json_city=1&id_country='+id_country+'&id='+id);
		$("#id_hotel").load('module/<?=$this->module?>.php?json_hotel=1&id_region='+id_city+'&id='+id);

		}
	function json_list_filter3(id_region,id){

		$("#id_hotel").load('module/<?=$this->module?>.php?json_hotel=1&id_region='+id_region+'&id='+id);

		}
		
		function json_list_filter4(id_region,id){

		$("#id_city").load('module/<?=$this->module?>.php?json_city=1&id_region='+id_region+'&id='+id);

		}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}

	function show_furnizori($date) {
		global $db;
	?>
			<dl>
			<dt>
				<label for="id_furnizor"><?=l('Furnizor')?></label>
			</dt>
			<dd>
				<select name="id_furnizor" id="id_furnizor" style="width:200px">
				<?php $this->json_furnizori($date['id_furnizor']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_sejur_transport"><?=l('Transport')?></label>
			</dt>
			<dd>
				<select name="id_croaziera_transport" id="id_croaziera_transport" style="width:200px">
				<?php $this->json_brands($date['id_croaziera_transport']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_brands($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_croaziera_transport` ORDER BY `id` ASC");
		while ($linie=$db->fetch($resursa)) {
			$t=$db->fetch_one("select `name` from `xp_transport_data` WHERE `_id`={$linie['id_transport']} ");
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name'].' - '.$t?> </option>
			<?php
		}
	}
	
	function json_furnizori($id_furnizor=0) {
		global $db;
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_furnizor=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_furnizor)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		if ($data['type']==0) {
			unset($data['type']);
			unset($data['id_country']);
			unset($data['id_region']);
		}
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new radaos4_module();
if ($module_info) $this_module=$module;
elseif ($module_css) $module->css();
elseif ($module_js) $module->js();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_state']==1) $module->json_state(fget('id'));
elseif ($_GET['json_city']==1) $module->json_city(fget('id_country'),fget('id'));
elseif ($_GET['json_furnizori']==1) $module->json_furnizori(fget('id'));
else {
	print_header();
	print_content($module);
	//$module->new_a();
	print_footer();
}
?>