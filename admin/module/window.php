<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class window_module{
	var $module='window';
	var $date='25-08-2009';
	var $table='xp_window';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function window_module() {
		$this->name=l('Mesaje site');
		$this->title=l('Mesaje site');
		$this->description=l('Mesaje site');

		$this->type=array(
		'like'=>array('id','constructor','name'),
		'date'=>array('date'),
		'equal'=>array('status')
		);




		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Titlu'),'style'=>'width:70%;','lang'=>true),
		'constructor'=>array('type'=>'input','name'=>l('Module'),'style'=>'width:70%;'),
	
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		
		1=>'show_categories',
		'description'=>array('type'=>'text','name'=>l('Descriere'),'lang'=>true,'style'=>'width:70%;height:150px;'),
		
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

			//$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
			//$row['actions']='<a class="action edit" onclick="do_edit('.$row['_id'].',\'pages\');" >'.l('edit').'</a>';
		
		return $row;
	}
	function print_records() {
	?>
	<h2><?=l('Mesaje site')?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="nss_grid">
		<table cellspacing="0" cellpadding="0" border="0"  >
		<thead>
			<tr role="rowheader" class="ui-jqgrid-labels">
				<th><?=l('nume')?></th>
				<!--<th style="width: 60px;"><?=l('Anunturi')?></th>-->
				<th style="width: 60px;"><?=l('vizibil')?></th>
				<th style="width: 140px;"><?=l('Actions')?></th>
			</tr>
		</thead>
		</table>
	</div>
	<div id="nss_list_record">
	<?php  $this->reg_get_categs((int)r('cid')); ?>
	</div>
	<br />
	<?php
	}

	function reg_get_categs($id_parent=0,$content="",$nr=0) {
		global $db;
		if ($nr>4) die();
		if ($nr==0) {
			$id_id='id_parent';
		} else $id_id='c.id';

		$categories =
		$db->fetch_all("
			select cd.*,c.*
			from
				`".$this->table."` c
				inner join `".$this->table."_data` cd
					on c.`id` = cd.`_id`
			where
				{$id_id} = {$id_parent} and
				`lang` = '".LANG."'
			ORDER BY `order` ASC
		");

		if (empty($categories)) {
			?>
			<ul style="list-style-type:none;	margin:0;		padding:0;" class="connectedSortable">

			<?php
			?>
				<li id="ele-0">
						<div style="padding:2px; ">
						<a href="?mod=<?=$this->module?>&cid=0" onclick="add_all_url['<?=$this->module?>']='&cid=0'; reload_list_record('<?=$this->module?>'); return false; ">
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l('Mesaje site')?></span>
						</a>
					</div>
				</li>
			<?=$content?>
			</ul>
			<?php
		} else {


			ob_start();
			$exist_p=fo("SELECT `id` FROM `".$this->table."` WHERE `id`=".(int)$categories[0]['id_parent']);

			?>
			<ul  style="list-style-type:none;	margin:0;	 padding:0;
			 <?php if ($exist_p) { ?>			margin-left:20px; <?php } ?> "
			<?php if ($nr==0)  { ?> class="list_sort" <?php } ?> >
			<?php
			$i=0;
			foreach ($categories as $category) {
				$i++;
				//$nr_products=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_ADS."` WHERE `id_category`=".$category['id']);
				//$nr_products=product_count(','.$category['id_main'].categ_children($category['id_main']));
				if ($category ['status'] == 1) {
					$category ['status'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '" onclick="ajax_ch_status(\''.$this->module.'\',this);" />';
				} else {
					$category ['status'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '"  onclick="ajax_ch_status(\''.$this->module.'\',this);"  />';
				}
				$exist=fo("SELECT `id` FROM `".$this->table."` WHERE `id_parent`=".$category['id']);
			?>
					<li id="ele-<?=$category['id']?>">
						<div style="float:left;">
						<?php if ($nr==0 /*&& $i!=sizeof($categories)*/)  {
							?>
							<img src="images/icons/branch.gif" style="vertical-align:middle;" />
							<?php
						} else { ?>
						<img src="images/icons/branch2.gif" style="vertical-align:middle;" />
						<?php  } if (!empty($exist)) { ?>
						<a href="?mod=<?=$this->module?>&cid=<?=$category['id']?>" onclick="add_all_url['<?=$this->module?>']='&cid=<?=$category['id']?>'; reload_list_record('<?=$this->module?>'); return false; ">
						<?php } ?>
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=$category['constructor']?></span>
						<?php if (!empty($exist)) { ?>
						</a>
						<?php } ?>
						</div>
						<div style="width: 140px;float:right; padding:2px; padding-top:3px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="delete_record(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 60px;float:right; padding:1px 2px; text-align:center;"><?=$category['status']?></div>
						<!--<div style="width: 60px;float:right; padding:3px 2px; text-align:center;"><?=$nr_products?></div>-->
						<div class="clear"></div>
				</li>
				<?php
				$id_parent=$category['id_parent'];
			}
			echo $content;
			?>
			</ul>
		<?php
		$file_data = ob_get_contents();
		ob_end_clean();
		$this->reg_get_categs($id_parent,$file_data,$nr+1);
		}
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_parent"><?=l('Tip mesaj')?></label>
			</dt>
			<dd>
				<select name="id_parent" id="id_parent" style="width:200px">
				<?php $this->json_categories($date['id_parent']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;

		$categories=lang_fetch_all('xp_window'," `id_parent`=".$id_parent." and `id_parent`=0 ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		if ($id_parent==0 ) { ?>
			<option value="0"><?=l('Niciuna')?></option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['constructor']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_admin = lang_fetch($this->table,"`id`=".$id);
		}
//		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//		} else $date_admin['image']='';

		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_order() {
		global_save_order($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;

		
		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['en']['name_seo'],'-'),'static/i/'.$this->folder,'240x120'));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				lang_update($this->table, $data," `id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new window_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->reg_get_categs((int)r('cid'));
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>