<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
$db2=new simpleDB_mysql($config['SQL_HOST'],$config['SQL_USER'],$config['SQL_PASS'],'clickgo_newsletter');
$db2->query("SET NAMES utf8");
class newsletter_emails_module {
	var $module='newsletter_emails';
	var $date='27-08-2009';
	var $table='email_list_subscribers';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $folder='';
	var $title='';
	var $description='';
	function newsletter_emails_module() {
		$this->name=l('Emailuri newsletter');
		$this->title=l('Emailuri newsletter');
		$this->description=l('Emailuri newsletter');
		$this->folder='files';
		$this->type=array(
		'like'=>array('email'),
		'date'=>array('requestdate'),
		'equal'=>array('status')
		);
		$this->grid=array(
		0=>array('order'=>'requestdate desc'),
		'_nr'=>true,
		//'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'emailaddress'=>array('name'=>l('Email'),'width'=>300),
		//'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'requestdate'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
		$this->form=array(
		'email'=>array('type'=>'input','name'=>l('email'),'valid'=>'empty,unique,email'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv')))
		);
		
		$this->form_mails=array(
		'file'=>array('type'=>'file','name'=>l('Fisier'),'folder'=>'tmp_img'),
		);
		$this->form_mail=array(
		//1=>'select_letter',
		//'sender'=>array('type'=>'input','name'=>l('Email expeditor'),'style'=>'width:98%;','valid'=>'empty,min_10,max_200'),
		'subject'=>array('type'=>'input','name'=>l('Subiect email'),'style'=>'width:98%;','valid'=>'empty,min_10,max_200'),
		//'emails'=>array('type'=>'text','name'=>l('Email'),'style'=>'width:98%; height:140px;','valid'=>'empty,min_10,max_200'),
		//'logs'=>array('type'=>'checkbox','name'=>l('Salveaza loguri'),'value'=>1),
		//'letter'=>array('type'=>'checkbox','name'=>l('Salveaza scrisoare'),'value'=>0,'text'=>'Da, salveaz!'),
		//'link'=>array('type'=>'checkbox','name'=>l('Link dezabonare'),'value'=>1,'text'=>l('pentru userii abonati')),
		//'message'=>array('type'=>'editor','name'=>l('Mesaj'),'style'=>'width:98%;'),
		51=>array('tab'=>2),
		53=>'list_product_banner',
		);
	}
		function list_product_banner($date) {
		global $db;
		if (isset($date['ids'])) $date['ids']=explode(",",$date['ids']);
		?>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="id_category"><?=l('Categoria')?></label>
			</dt>
				<select name="id_category2" id="id_category2" style="width:460px;" onchange="do_product_filter2();">
					<option value="0"><?=l('Toate')?></option>
					<?php $this->json_categories2() ?>
				</select>
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Numele produsului sau cod')?></label>
			</dt>
			<dd><input type="text" name="product_name" id="product_name" style="width:460px;" onkeyup="do_product_filter2();" />
			</dd>
		</dl>
		<dl  class="all_options_type options_type_2">
			<dt>
				<label for="products"><?=l('Produse')?></label>
			</dt>
			<dd id="list_products_dd2">
				<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
					<?php $this->json_products2(); ?>
				</select>
			</dd>
		</dl>
		<div class="list_products_selected all_options_type options_type_2">
				<?php
				if ($date['type']==3) {
					//print_a($date['ids']);
					foreach ($date['ids'] as $id_now) {
						if (!empty($id_now)) {
							$sql="select
										p.*,
										pd.*
									from
								`".TABLE_PRODUCTS."` p
								inner join `".TABLE_PRODUCTS."_data` pd
									on p.`id`=pd.`id_product`
									
								WHERE p.`id`=".$id_now;
							$product=$db->fetch($sql);
							//print_a($product);
							?><div ><input type="hidden" name="products[]" value="<?=$product['id']?>" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b><?=$product['code']?> - <?=$product['name']?></b></div><?php
						}
					}
				}
				?>
				</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#categories').toChecklist();
		});
		</script>
		<?php
	}
	function json_products2($ids=array(),$id_category=0,$product_name="") {
		global $db;
		$sql_filters=array();
		if (empty($ids)) $ids=array();
		//if (!empty($id_category)) $sql_filters[]=" p.`id` in (select `id_product` from `xp_products_to_categories` where `id_category`={$id_category})";
		if (!empty($product_name)) $sql_filters[]="  p.`name` LIKE  '%".$product_name."%'  ";
		$sql="select sql_calc_found_rows
					p.*,
					pd.*,
					c.`id_category`

				from
					`xp_products` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

					left join `xp_products_to_categories` c
						on c.`id_product`=p.`id`
				order by p.`date` desc";
		$products=$db->fetch_all($sql);
		$i=0;
		foreach ($products as $linie) {
			if (!empty($linie['name'])) {
			?>
			<option value="<?=$linie['id']?>" <?=((in_array($linie['id'],$ids))?'selected':'')?> ><?=$linie['name']?></option>
			<?php
			}
		}
	}
	function json_categories2($ids=array(),$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		//print_a($categories);
		foreach ($categories as $linie) {
			?>
			<option value="<?=$linie['id']?>" <?=(in_array($linie['id'],$ids))?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp;  &nbsp; ";
			?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories2($ids,$linie['id'],$deep+1);
		}
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list2($this,false);
	}
	function json_list_row($row) {
		$row['actions']="<a onclick=\"do_delete('{$row['subscriberid']}','newsletter_emails','');\" class=\"action delete\">Sterge</a>";
		return $row;
	}
	function js() {
		$after='$("#gbox_list_'.$this->module.'").css({"borderRight":"0px none"}).removeClass("ui-corner-all");';
		set_grid($this,array('multiselect'=>'true'),'',$after);
		?>
		$(document).ready(function(){
			$(".module_menu .import_email").click(function(){
			
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=import_email',900, function () {
				init_upload();
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				},true);
			});
			$(".module_menu .email_new").click(function(){
				var gr = nss_grids['<?=$this->module?>'].ids();
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=email_new&to='+gr,900, function () {
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				},true);
			});
			$(".module_menu .email_all").click(function(){
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=email_new&to=all',900, function () {
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				},true);
			});
		});
		function autocomplete_letter(id) {
		if (id!=0) {
			$("#letter").attr("checked",false);
			$("#letter").parent().parent().hide();
				$.ajax( {
					data :"autocomplete_letter=1&id="+ id,
					type :"GET",
					url :'module/<?=$this->module?>.php',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) { eval(r);	}
				});
			} else {
				$("#letter").attr("checked",true);
				$("#letter").parent().parent().show()
			}
		}
		function do_product_filter2(id,div) {
			var data="id_category="+$("#id_category2").val()+"&product_name="+$("#product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter2=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd2').html(r);
					//	$("#list_products_dd select").toChecklist();
					}
				});
		}
		function select_product2(id_this) {
			var name=id_this.options[id_this.selectedIndex].text;
			if(id_this.value>0) {
				$('.list_products_selected').append('<div ><input type="hidden" name="products[]" value="'+id_this.value+'" /><img src="images/delicon.gif" align="absmiddle"  class="rem" onclick="$(this).parent().remove();"  /> <b>'+name+'</b></div>')
			}
		}
		<?php
	}
	function print_records() {
		$new_icons=array(
		'email_new'=>l('Creaza newsletter'),
		//'email_all'=>l('Trimite la toti'),
		'import_email'=>l('Importa email-uri')
		);
		print_content($this,$new_icons);
	}
	function json_product_filter2() {
		?>
		<select name="products_2"  multiple="multiple"  size="12" style="width:460px;" onchange="select_product2(this);">
			<?php $this->json_products2(array(),$_POST['id_category'],$_POST['product_name']) ?>
		</select>
		<?php
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete5($this->table);
	}
	function select_letter($date) {
		global $db;
		$all_letters=$db->fetch_all("SELECT `id`,`subject` FROM `".TABLE_NEWSLETTER_LETTERS."`");
		?>
		<dl class="product_have_variations">
			<dt>
				<label for="id_category"><?=l('Scrisori predefinite')?></label>
			</dt>
			<dd>
			<select name="id_letter_template" onchange="autocomplete_letter(this.value);">
			<option value="0"> <?=l('Selectati o scrisoare')?> </option>
				<?php foreach ($all_letters as $letter) { ?>
				<option value="<?=$letter['id']?>"><?=ehtml($letter['subject'])?></option>
				<?php } ?>
			</select>
			</dd>
		</dl>
		<?php
	}
	function import_email_save() {
		global $db2;
		$file=SITEROOT.'/'.$_POST['file'];
		$file_content= @file_get_contents($file);
		$file_content=explode("\r",$file_content);
		foreach ($file_content as $email_nw) {
			$email_nw=str_replace(array("\n","\r","\r\n"),"",trim($email_nw));
			//print_a($email_nw);
			$verEmail=$db2->fetch_one("SELECT * FROM email_list_subscribers WHERE emailaddress='".$email_nw."'");
				if (!$verEmail) {
					$db2->insert($this->table,array('emailaddress'=>$email_nw,'confirmed'=>1,'listid'=>1,'format'=>'h','requestdate'=>time(),'confirmdate'=>time(),'subscribedate'=>time(),'confirmcode'=>md5(time().rand(1000,90000))));
				}
		}
		@unlink($file);
		print_alerta(l('Emailurile au fost importate'));
		?>
			<script type="text/javascript">
			$('#window_<?=$this->module?>_email').dialog( 'close' );
			</script>
			<?php
	}
	function import_email() {
		global  $db2,$main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=import_email_save" method="POST">
		<?php
		print_form_header(l('Importa Emailuri'));
		?>
		<div style="color:red;font-weight:bold;font-size:16px;">Puteti importa emailuri din fisier txt care sunt separate prin virgula!</div>
		<?
		print_form($this->form_mails,$this);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function new_a($id=0) {
		global  $db2,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE subscriberid=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica email'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function mail($id=0) {
		global  $db,$main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=send" method="POST">
		<?php
		print_form_header(l('Creaza newsletter'));
		print_form($this->form_mail,$this,$date_saved);
		print_form_footer();
		unset($main_buttons['continue']);
		$main_buttons['save']=array('type'=>'submit','value'=>l('Trimite'));
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db2;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			unset($data['id_letter_template']);
			if (!empty($id)) {
				$db2->qupdate($this->table,$data," subscriberid=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				//$data['date']=time();
				//$data['code']=md5(time().rand(1000,90000));
				$db2->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function send() {
		global $db,$config,$db2;
		$table=$this->table;
		$data=$_POST;
		
		foreach ($data['products'] as $p) {
			$x=$db->fetch("select 
					p.*,
					pd.*,
					c.`id_category`

				from
					`xp_products` p
					inner join `xp_products_data` pd
								on p.`id`=pd.`id_product`

					left join `xp_products_to_categories` c
						on c.`id_product`=p.`id`
		where
			p.`id`={$p}");
			$products[]=$x;
			
		}					 
		foreach ($products as $k=>$p){
			
			
			
			if($p['id_parent']>0) {
				$p['id_parent']=$db->fetch_one("select `id_parent` from `xp_categories` where `id`=".$p['id_category']);
			} else $p['id_parent']=$p['id_category'];
			
			$p['cat_link'] = categories_link($p['id_parent']);
			$p['link'] = lnk( 'product', $p );
					
			$image=$db->fetch_one("SELECT `image` FROM `".TABLE_PRODUCTS_IMAGES."` WHERE `id_product`=".(int)$p['id']." ORDER BY `order` ASC",MYSQL_ASSOC);	
			//$p['link'] = lnk('products',$p);
			$p['image'] = _static.'i/imagini-produse/' . ( $image ? $image : 'no-image.jpg') ;
			$message.='
			<tr>
				<td></td>
				<td width="10"></td>
				<td width="738" style="color:#fff;border-bottom:1px solid #808080"><br>
					<a style="text-decoration:none" href="'.$p['link'].'" title="'.ehtml($p['name']).'" target="_blank">
						<img width="250" height="187" style="float:left;margin-right:14px;margin-bottom:14px" src="'.$p['image'].'" alt="'.ehtml($p['name']).'"/>
					</a>
					<a style="text-decoration:none" href="'.$p['link'].'" title="'.ehtml($p['name']).'" target="_blank">
						<strong style="font-size:15px"><span style="color:#333;">'.ehtml($p['name']).'</span></strong>
					</a>
					<p style="font-size:14px;color:#333;margin-top:5px">'.nl2br(strip_tags($p['description'])).'</p>
					<p></p>
					<p style="font-size:14px;font-weight:bold;color:#333;margin-top:5px">Pret: '.$p['price'].' lei</p>
					

				</td>
				<td width="10"></td>
				<td></td>
			</tr>
				';
		}
		
		
		$mesaj=preg_replace('/(href="|src="|action=")(?!#|[a-zA-Z]+:|")\/?([^"]+)(")/i','$1'.$config['SITE_WEB_ROOT'].'$2$3',file_get_contents('http://pantofinicolett.ro/static/newsletter.html'));
		
		$mesaj=str_replace(array('{code}','{continut}','{continut2}'),array($randNumber,$message,$message2),$mesaj);
		$array_insert = 
		array(
			'name'=>(!empty($_POST['subject'])?$_POST['subject']:'newsletter'),
			'htmlbody'=>$mesaj,
			'createdate'=>time(),
			'active' =>1,
			'format' =>'h',
			'isglobal' =>1
		);
		
		$db2->insert(
			'email_templates',
			$array_insert
		);
		
		print_alerta('a fost creat');
		
		?>
			<script type="text/javascript">
			$('#window_<?=$this->module?>_email').dialog( 'close' );
			</script>
			<?php
	}
}
$module=new newsletter_emails_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list2($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['autocomplete_letter']==1) $module->autocomplete_letter(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['json_categories2']==1) $module->json_categories2(fget('id'));
elseif ($_GET['json_products2']==1) $module->json_products2(fget('id'));
elseif ($_GET['json_product_filter2']==1) $module->json_product_filter2();
elseif ($_GET['action']=='email_new') $module->mail(fget('id'));
elseif ($_GET['action']=='import_email') $module->import_email();
elseif ($_GET['action']=='import_email_save') $module->import_email_save();
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='send') $module->send();
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>