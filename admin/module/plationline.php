<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class plationline_module {
	var	$module='plationline';
	var $date='27-08-2009';
	var $table=TABLE_PLATIONLINE;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $orders_status=array();
	var $name='Vezi comenzi credit card';
	var $title='';
	var $description='';
	function plationline_module() {

		$this->type=array(
		'like'=>array('id','po_id','last_name','first_name','amount','credite'),
		'date'=>array('date'),
		'equal'=>array('status')
		);

		$this->plationline_status=array(
		0=>l('In derulare'),
		1=>l('Aprobata'),
		2=>l('Respinsa'),
		3=>l('Livrata'),
		4=>l('Anulata'),
		);

		$status_comanda=array(0=>'_:'.l('All'));
		foreach ($this->plationline_status as $key=>$status) $status_comanda[]=$key.":".$status;
		$edit_options=array('value'=>implode(";",$status_comanda));

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'show_info'=>array('name'=>l('Info'),'width'=>40,'align'=>'center'),
		'po_id'=>array('name'=>l('Nr comanda'),'width'=>78,'align'=>'center'),
		'last_name'=>array('name'=>l('Last Name'),'width'=>130,'align'=>'left'),
		'first_name'=>array('name'=>l('First Name'),'width'=>130,'align'=>'left'),
		'email'=>array('name'=>l('Email'),'width'=>200,'align'=>'left'),
		'credits'=>array('name'=>l('Credite'),'width'=>100,'align'=>'right'),
		'amount'=>array('name'=>l('Suma platita'),'width'=>100,'align'=>'right'),
		'status'=>array('name'=>l('status'),'width'=>120,'align'=>'center','stype'=>'select','editoptions'=>$edit_options),
		'date'=>array('name'=>l('Date'),'width'=>110,'align'=>'center'),
		//'shipping_method'=>array('name'=>l('shipping method'),'width'=>210),
		'actions'=>array('name'=>l('Actions'),'width'=>150,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>l('Cumparator'),
		'buyer_first_name'=>array('type'=>'input','name'=>l('Nume')),
		'buyer_last_name'=>array('type'=>'input','name'=>l('Prenume')),
		'buyer_email'=>array('type'=>'input','name'=>l('Email')),
		'buyer_cnp'=>array('type'=>'input','name'=>l('Cnp')),
		'buyer_ci_serie'=>array('type'=>'input','name'=>l('Serie CI')),
		'buyer_city'=>array('type'=>'input','name'=>l('Oras')),
		'buyer_state'=>array('type'=>'input','name'=>l('Judet')),
		'buyer_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		'buyer_address'=>array('type'=>'input','name'=>l('Adresa')),
		'buyer_phone'=>array('type'=>'input','name'=>l('Telefon')),
		1=>l('Firma'),
		'company'=>array('type'=>'input','name'=>l('Nume')),
		'company_cui'=>array('type'=>'input','name'=>l('CUI')),
		'company_nr_reg_com'=>array('type'=>'input','name'=>l('Nr reg com')),
		'company_bank'=>array('type'=>'input','name'=>l('Banca')),
		'company_bank_account'=>array('type'=>'input','name'=>l('Cont')),
		'company_city'=>array('type'=>'input','name'=>l('Oras')),
		'company_state'=>array('type'=>'input','name'=>l('Judet')),
		'company_address'=>array('type'=>'input','name'=>l('Adresa')),
		'company_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		'company_phone'=>array('type'=>'input','name'=>l('Telefon')),
		2=>l('Livrare'),
		'shipping_city'=>array('type'=>'input','name'=>l('Oras')),
		'shipping_state'=>array('type'=>'input','name'=>l('Judet')),
		'shipping_address'=>array('type'=>'input','name'=>l('Adresa')),
		'shipping_zip_code'=>array('type'=>'input','name'=>l('Cod postal')),
		3=>l('Alte detalii'),
		'shipping_method'=>array('type'=>'input','name'=>l('Metoda de livrare')),
		'shipping_tax'=>array('type'=>'input','name'=>l('Taxa de livrare')),
		'handling_fee'=>array('type'=>'input','name'=>l('Taxa de manipulare')),
		'comment'=>array('type'=>'text','name'=>l('Observatii')),
		'ip'=>array('type'=>'input','name'=>l('Adresa de ip')),
		'status'=>array('type'=>'input','name'=>l('Statut')),
		);
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row_before($row) {
		$row['status_comanda']=$row['status'];
		return $row;
	}
	function json_list() {
		json_list($this);
	}
	function json_list_row($row) {
		$row['show_info']='<img src="images/list-add.png" class="pointer"  width="16" height="16" alt="Info" onclick="insert_grid_line(this,'.$row['id'].',load_order_info);" />';
		$row['status']='<b>'.$this->plationline_status[$row['status_comanda']].'</b>';
		$row['amount']='<b>'.number_format($row['amount'],2,'.','').' lei</b>';
		//$row['date']='<b>'.date('d M Y H:i:s',$row['date']).'</b>';
		return $row;
	}
	function js() {
		?>
		function load_order_info(id) {
			$.ajax({
				data :"id="+id,
				type :"POST",
				url :"module/"+module+".php?show_info=1",
				timeout :45000,
				error : function() {
					console.log("Failed to submit - ");
				},
				success : function(r) {
					$('#show_info_'+id).html(r);
				}
			});
		}
		$(document).ready(function(){
			$(".module_menu .update_status").click(function(){
				var window_add_edit_name='<?=l('Update status')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_status&ids='+gr,600, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
		});
		<?php
		set_grid($this,array('multiselect'=>true,'sortorder' => 'desc' ));
	}
	function css() {
		?>
		.order_hr {
			height:18px;
			clear:both;
		}
		.order_info_col {
			float:left;
			width:310px;
			border-left:3px solid #696C8B;
			padding:8px;
		}
		.order_info_col_none {
			border-left:0 none;
		}
		.order_info_col b {
			font-size:1.2em;
			display:block;
			margin-bottom:4px;
			border-bottom:1px solid #696C8B;
		}
		.order_info_col div {
			padding:1px;
		}
		.order_info_col div:hover {
			background-color:#FFF1A0;
		}
		.order_info_col div span{
			display:block;
			float:right;
			font-weight:bold;
			width:160px;
			padding-left:4px;
			border-left:1px solid #696C8B;
		}
		dl dd input, dl dd textarea  {
			width:80%;
		}
		.module_menu li.update_status {
			background-image:url('../../images/icons/orders_update_status.png');
		}
		<?php
	}
	function print_records() {
		print_content($this,array('update_status'=>l('Update status')),'','',array('new'));
	}

	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$db->delete(TABLE_ORDERS_PRODUCTS," `id_order`=".$id);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Editeaza comanda'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function update_status($ids){
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_status&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header();
		?>
		<dl class="related_products_select">
			<dt>
				<label for="status_comanda"><?=l('Status')?></label>
			</dt>
			<dd>
			<select name="status_comanda" id="status_comanda" >
			<?php
			foreach ($this->plationline_status as $key=>$status) {
				?>
				<option value="<?=$key?>"><?=$status?></option>
				<?php
			}
			?>
			</select>
			</dd>
		</dl>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save_status($ids){
		global $db;
		$ids=explode(",",$ids);
		foreach ($ids as $id) if (!empty($id)) $db->update($this->table,array("status"=>r('status_comanda'))," id=".$id);
		close_window($this->module);
		print_alerta(l('Statusul a fost modificat'));
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->update($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function show_info($id=0) {
		global $db;
		$orders_status=$this->plationline_status;
		$order=$db->fetch("SELECT * FROM `".TABLE_PLATIONLINE."` WHERE `id`=".$id);
		?>
		<div class="order_info_col order_info_col_none">
			<b><?=l('Detalii client')?></b>
			<div> <?=l('Nume')?><span> <?=$order['buyer_first_name']?></span></div>
			<div> <?=l('Prenume')?><span> <?=$order['buyer_last_name']?></span></div>
			<div> <?=l('Email')?><span> <?=$order['buyer_email']?></span></div>
			<div> <?=l('Oras')?><span> <?=$order['buyer_city']?></span></div>
			<div> <?=l('Judet')?><span> <?=$order['buyer_state']?></span></div>
			<div> <?=l('Adresa')?><span> <?=$order['buyer_address']?></span></div>
			<div> <?=l('Telefon')?><span> <?=$order['buyer_phone']?></span></div>
		</div>
		<div class="order_hr"></div>
		<div class="order_info_col order_info_col_none" style="overflow:hidden;">
			<b><?=l('Detalii securitate')?></b>
			<div> <?=l('Data')?> <span><?=date(DATE_TIME_FORMAT,$order['date'])?></span></div>
			<div> <?=l('Adresa ip')?> <span><?=$order['ip']?></span></div>
			<div> <?=l('Statut')?> <span><?=$orders_status[$order['status']]?></span></div>
			<b><?=l('Observatii')?></b>
			<div> <?=$order['comment']?> </div>
		</div>
		<div class="order_info_col" style="width:620px;">
			<b><?=l('Pachet credite')?></b>
			<?php
			$products=$db->fetch_all("SELECT * FROM `".TABLE_ORDERS_PRODUCTS."` WHERE `id_order`=".$id);
			foreach ($products as $product) { ?>
			<div> <?=$product['quantity']?> X <?=$product['product']?> - <strong><?=$product['code']?></strong><span><?=number_format($product['price'],2,'.','')?></span></div>
			<?php } ?>
			<div> <?=l('Total comanda')?><span><?=number_format(($order['amount']+$order['shipping_tax']+$order['handling_fee']),2,'.','')?></span></div>

		</div>
		<div class="clear"></div>
		<?php
	}
}
$module=new plationline_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['show_info']==1) $module->show_info(fpost('id'));
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='update_status') $module->update_status(fget('ids'));
elseif ($_GET['action']=='save_status') $module->save_status(fget('ids'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>