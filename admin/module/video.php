<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class video_module {
	var $module='video';
	var $date='10-08-2010';
	var $table='xp_video';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function video_module() {
		$this->name=l('Video');
		$this->title=l('Video');
		$this->description=l('Video');

		$this->type=array(
			'like'=>array('id','title','name'),
			'date'=>array('date'),
			'equal'=>array('status','id_shop')
		);

		$this->folder='video';
		$this->folder2='static/i/video';

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume video'),'width'=>200),
		//'id_category'=>array('name'=>l('Categorie'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'id_shop'=>array('name'=>l('Categorie'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Crap').';2:'.l('Stationar').';3:'.l('Rapitor'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume video'),'text'=>'','info'=>'','style'=>'width:70%;','auto'=>'name_seo'),
		//'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:70%;'),
		//'image'=>array('type'=>'image','name'=>l('Imagine 160px x 120px'),$this->folder),
		'description'=>array('type'=>'text','name'=>l('Descriere video'),'style'=>'width:92%;height:250px;'),
		//'file'=>array('type'=>'file','name'=>l('Video file'),'text'=>'Format mp4','folder'=>$this->folder2),
		'link'=>array('type'=>'input','name'=>l('Link youtube'),'style'=>'width:70%;'),
		//5=>'show_categories',
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		);
	}
	
	
	function css() {}
	function json_list() {
		json_list($this);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=900;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){

nss_win('<?=$this->module?>_video', '<?=l('Upload YouTube video')?>',
		'module/<?=$this->module?>.php?create_upload_form=1&video_title='+ $('#ro_name').val()+
		'&video_desc=' + $('#ro_content').val() +
		'&video_tags=' + $('#ro_name').val(), 560);

}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;
		//$row['id_category']=0;
//		$row['id_category']=$db->fetch_one("SELECT `name` FROM `xp_video_categories_data` WHERE `_id`=".$row['id_category']);
//		if ($row['id_category']==false) {
//			$row['id_category']='<span style="color:red;">'.l('Nicio categorie').'</span>';
//		}

		if ($row['id_shop']==1) {
				$row['id_shop']='Crap';
			} elseif ($row['id_shop']==2) {
				$row['id_shop']='Stationar';
			} elseif ($row['id_shop']==3) {
				$row['id_shop']='Rapitor';
			}

//		if ($row['type']==0) {
//			$row['type']='<img src="images/tick.gif"  width="16" height="16" alt="tick"  />';
//		} else {
//			$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
//			$row['actions']='<a class="action edit" id="id_line_'.$row['id'].'">'.l('edit').'</a>';
//		}

		if ($row ['is_featured'] == 1) {
			$row ['is_featured'] = '<img class="ch_staus_img" rel="is_featured" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';
		} else
		$row ['is_featured'] = '<img class="ch_staus_img" rel="is_featured" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;vertical-align:middle;"  id="id_line_' . $row ['id'] . '" />';

		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie video')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:400px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `xp_video_categories` ");
		$categories=lang_fetch_all('xp_video_categories'," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)){
			$date_admin=$db->fetch("select
					*
				from
					`{$this->table}` 
					 where `id`=".$id);

//			if(!empty($date_admin['file'])){
//				$date_admin['file']='static/'.$this->folder.'/'.$date_admin['file'];
//			}
//			if ($date_admin['file']!=''&&file_exists(SITEROOT.'/'.$this->folder2.'/'.$date_admin['file'])){
//				$date_admin['file']=$this->folder2.'/'.$date_admin['file'];
//			} else $date_admin['file']='';
//			if(!empty($date_admin['image'])&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//				$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//			}
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica video site'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$poza=escapeIlegalChars(ro_to_en($data['name'])," ");
		$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($poza,"-"),'static/i/'.$this->folder,''));
		//$data['name_seo']=escapeIlegalChars(ro_to_en($data['name'])," ");
//		if (!isset($data['file']))
//			$data['file']='';

		//$this->folder='static/i/'.$this->folder;

//		$data['file']=str_replace($this->folder2.'/','',upload_file($data,'file',$this));
//		$data2=array(
//					'name'=>$data['name'],
//					'name_seo'=>$data['name_seo'],
//					'file'=>$data['file'],
//					'description'=>$data['description'],
//					'youtube'=>$data['youtube'],
//					'image'=>$data['image'],
//
//					//'description'=>$data['description']
//					);
//		unset($data['name']);
//		unset($data['description']);
//		unset($data['file']);
//		unset($data['name_seo']);
//		unset($data['youtube']);
//		unset($data['image']);
		#print_a($data);

		$errors=form_validation($data,$this->form,$this->table);

		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				//$data['date']=time();
				$db->update($this->table, $data," `id`=".$id);
				//$db->update($this->table.'_data', $data2," `_id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				$id=mysql_insert_id();
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}

}
$module=new video_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) {
		json_list($module);
}
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['create_upload_form']==1) $module->create_upload_form();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>