<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class order_discount_module {
	var	$module='order_discount';
	var $date='27-08-2009';
	var $table=TABLE_ORDER_DISCOUNT;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function order_discount_module() {

		$this->name=l('Discount total comanda');
		$this->title=l('Discount total comanda');
		$this->description=l('Discount total comanda');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);
		
		$this->grid=array(
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'price_from'=>array('name'=>l('De la'),'width'=>200,'align'=>'center'),
		'price_to'=>array('name'=>l('Pana la'),'width'=>200,'align'=>'center'),
		'percent'=>array('name'=>l('Discount'),'width'=>200,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
		
		$this->form=array(
		0=>'',
		'price_from'=>array('type'=>'input','name'=>l('Frome la'),'valid'=>'empty,unique,numeric'),
		'price_to'=>array('type'=>'input','name'=>l('Pana la'),'valid'=>'empty,unique,numeric'),
		'percent'=>array('type'=>'input','name'=>l('Discount'),'valid'=>'empty,numeric'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		$row['discount']=$row['discount']."%";
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function json_list(){
		json_list($this);
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new order_discount_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module);
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>