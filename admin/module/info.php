<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class info_module {
	var $module='info';
	var $date='27-08-2009';
	var $table='xp_products_info';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	var $dimensiuni=array();
	function info_module() {
		global $config;
		$this->name=l('Info produse');
		$this->title=l('Info produse');
		$this->description=l('Info produse');

		$this->type=array(
		'like'=>array('link','name','username','email'),
		'date'=>array('date'),
		'equal'=>array('status','id_product')
		);

		//$this->folder='static/banners';

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		//'image'=>array('name'=>l('Banner'),'width'=>570,'align'=>'center'),
		'name'=>array('name'=>l('Nume'),'width'=>80,'align'=>'left'),

		'order'=>array('name'=>l('Pozitia'),'width'=>100,'align'=>'left'),
		
		'status'=>array('name'=>l('Status'),'width'=>60,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'date'=>array('name'=>l('Date'),'width'=>100,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),"style"=>'width:98%;'),

		'order'=>array('type'=>'input','name'=>l('Pozitia'),"style"=>'width:98%;'),
		//'email'=>array('type'=>'input','name'=>l('Email'),"style"=>'width:98%;'),
		//'website'=>array('type'=>'input','name'=>l('Website'),"style"=>'width:98%;'),
		//'link'=>array('type'=>'input','name'=>l('Link'),"style"=>'width:98%;'),
		//'message'=>array('type'=>'text','name'=>l('Comentariu'),"style"=>'width:98%;height:200px;'),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		);
	}
	function json_list() {
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		//$row['id_product']=$db->fetch_one("select `name` from `xp_products_data` where `id_product`=".$row['id_product']);
		//$row['id_user']=$db->fetch_one("select `email` from `xp_customers` where `id`=".$row['id_user']);
		return $row;
	}
	function js() {
		set_grid($this,array('multiselect'=>'true','sortorder'=>'desc'));
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		//unset($_POST,$data['file']);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			//if (!isset($data['image'])) $data['image']='';
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {

				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
			die();
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new info_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>