<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class slide_module {
	var $module='slide';
	var $date='25-08-2009';
	var $table=TABLE_SLIDE;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function slide_module() {
		$this->name=l('Slide prima pagina');
		$this->title=l('Slide prima pagina');
		$this->description=l('Slide prima pagina');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status','id_category')
		);
		$this->folder='slide';
		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		'image'=>array('name'=>l('Imagine mare'),'width'=>150),
		'name'=>array('name'=>l('Nume promotie'),'width'=>150),
//		'thumb'=>array('name'=>l('Imagine mica'),'width'=>150),
//		'image2'=>array('name'=>l('Imagine dreptunghi'),'width'=>150),
//		'name'=>array('name'=>l('Nume foto'),'width'=>100),
//		'id_category'=>array('name'=>l('Categoria'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		'order'=>array('name'=>l('Pozitia'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'text'=>'','info'=>'','valid'=>'empty,min_2,max_100','style'=>'width:70%;'),
		'link'=>array('type'=>'input','name'=>l('Link'),'style'=>'width:70%;'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:20%;','info'=>''),
//		'content'=>array('type'=>'input','name'=>l('Descriere'),'style'=>'width:70%;'),
//		'name'=>array('type'=>'input','name'=>l('Nume pagina'),'text'=>'','info'=>'','lang'=>true,'valid'=>'empty,min_2,max_100','style'=>'width:70%;','auto'=>'name_seo,header_title,title,meta_keywords,meta_description'),
//		'title'=>array('type'=>'input','name'=>l('Title, Alt'),'lang'=>true,'valid'=>'empty,min_2,max_200','style'=>'width:70%;'),
//		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
//		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
//		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:70%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
//		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:70%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
//		1=>'show_categories',
//		'content'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		'image'=>array('type'=>'image','name'=>l('Imagine mare'),'text'=>'620x280','folder'=>$this->folder),
		'image_small'=>array('type'=>'image','name'=>l('Imagine mica'),'text'=>'290x80','folder'=>$this->folder),
		//'thumb'=>array('type'=>'image','name'=>l('Imagine mica'),'text'=>'172x117','folder'=>$this->folder),
		//'image2'=>array('type'=>'image','name'=>l('Imagine dreptunghi'),'text'=>'340x117','folder'=>$this->folder)
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this,false);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=900;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() + 
		'&videoCategory=' + $('#video_categ').val() + 
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db,$gd;
//		if ($row['type']==0) {
//			$row['type']='<img src="images/tick.gif"  width="16" height="16" alt="tick"  />';
//		} else {
//			$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
//			$row['actions']='<a class="action edit" id="id_line_'.$row['id'].'">'.l('edit').'</a>';
//		}
		$row['image']='<img src="'.$gd->url('resize','../static/imagini-slide/'.$row['image'],'321x118').'" alt="'.$row['title'].'" />';
		//$row['image2']='<img src="../'.$gd->url('resize','static/imagini-slide/'.$row['image2'],'340x117').'" alt="'.$row['title'].'" />';
		//$row['thumb']='<img src="../'.$gd->url('resize','static/imagini-slide/'.$row['thumb'],'200x150').'" alt="'.$row['title'].'" />';
		return $row;
	}
	function grid_edit(){
		global_delete($this->table);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie site')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIESF."` ");
		$categories=lang_fetch_all(TABLE_CATEGORIESF," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		if ($id_parent==0 ) { ?>
			<option value="0"><?=l('Niciuna')?></option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_admin=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		if (($date_admin['image']!='')&&file_exists(SITEROOT.'/static/'.$this->folder.'/'.$date_admin['image'])){
					$date_admin['image']='static/'.$this->folder.'/'.$date_admin['image'];
		} else $date_admin['image']='';
		if (($date_admin['image_small']!='')&&file_exists(SITEROOT.'/static/'.$this->folder.'/'.$date_admin['image_small'])){
					$date_admin['image_small']='static/'.$this->folder.'/'.$date_admin['image_small'];
		} else $date_admin['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica slide'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$gd;
		$admin_info=adm_info();
		$languages=get_languages();
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table);
		//print_a(TABLE_SLIDE);
		//die();
		if(empty($errors)) {
			close_window($this->module);
			$data['image']=str_replace('static/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/'.$this->folder,''));
			$data['image_small']=str_replace('static/'.$this->folder.'/','',upload_images($data['image_small'],escapeIlegalChars($data['name'].' small',"-"),'static/'.$this->folder,''));
			//$data['image2']=str_replace('static/'.$this->folder.'/','',upload_images($data['image2'],escapeIlegalChars($data['name'].' dr',"-"),'static/'.$this->folder,''));
			//$data['thumb']=str_replace('static/'.$this->folder.'/','',upload_images($data['thumb'],escapeIlegalChars($data['name'].' tn',"-"),'static/'.$this->folder,''));
			//print_a($data);
			//die();
			if (!empty($id)) {
				//unset($data['sursa']);
				//print_a(TABLE_SLIDE);
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}

}
$module=new slide_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module,false);
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>