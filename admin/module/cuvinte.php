<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
$this_module=array();
class cuvinte_module {
	var $module='cuvinte';
	var $date='14-03-2010';
	var $table=TABLE_CUVINTE;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function cuvinte_module() {
		global $db;
		$this->name=l('Define-uri');
		$this->title=l('Define-uri');
		$this->description=l('Define-uri');

		$this->type=array(
		'like'=>array('name','id','key','cuvant'),
		'equal'=>array('lang','id')
		);


		$limbi_instalate=$db->fetch_all("SELECT * FROM `".TABLE_LANGUAGES."`");
		$status_comanda=array(0=>'_:'.l('All'));
		foreach ($limbi_instalate as $limba_now) {
			$status_comanda[]=$limba_now['prefix'].":".$limba_now['name'];
		}
		$edit_options=array('value'=>implode(";",$status_comanda));

		$this->grid=array(
		'_nr'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'key'=>array('name'=>l('Define'),'width'=>300),
		'cuvant'=>array('name'=>l('Traducere'),'width'=>300),
		'lang'=>array('name'=>l('Limba'),'width'=>80,'stype'=>'select','align'=>'center','editoptions'=>$edit_options),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'cuvant'=>array('type'=>'input','name'=>l('cuvant'),'valid'=>'empty','style'=>'width:90%;'),
		);
	}
	function css() {}
	function json_list(){ 
		json_list($this);
	}
	function json_list_row($row) {
		if ($row['default']==1) {
			$row['default']='<img class="ch_default_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;" />';
		} else $row['default']='<img class="ch_default_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Editeaza define'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new cuvinte_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('rownumbers'=>'false'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module,array(),'','',array('new'));
	print_footer();
}
?>