<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class ads_module {
	var $module='ads';
	var $date='27-08-2009';
	var $table=TABLE_ADS;
	var $folder='画像のアップロード';
	var $option_info;
	var $value_all_info;
	var $all_variations=array();
	var $all_variations_name=array();
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function ads_module() {
		$this->name=l('Anunturi');
		$this->title=l('Anunturi');
		$this->description=l('Anunturi');

		$this->type=array(
		'like'=>array('name','id','code','description','price','views','id_user'),
		'date'=>array('date'),
		'equal'=>array('status','id_category','id_brand','currency')
		);

		$this->folder='画像のアップロード';

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID Site'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Imagine'),'width'=>60,'align'=>'center'),
		'name'=>array('name'=>l('Titlu anunt'),'width'=>200),
		'id_category'=>array('name'=>l('Categorie anunt'),'width'=>120,'align'=>'left','stype'=>'select','editoptions'=>array('value'=>'0:'.l('All').';')),
		'id_user'=>array('name'=>l('User'),'width'=>120,'align'=>'left'),
		'price'=>array('name'=>l('Pret'),'width'=>50,'align'=>'right'),
		//'currency'=>array('name'=>l('Moneda'),'align'=>'left','stype'=>'select','editoptions'=>array('value'=>'0:'.l('All').';'),'width'=>40),
		'views'=>array('name'=>l('Vizualizari'),'width'=>40,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>80,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'tabs'=>array(
		1=>l('Detalii anunt'),
		),
		0=>array('tab'=>1),
		'name'=>array('type'=>'input','lang'=>true,'name'=>l('Tiltu anunt'),'valid'=>'empty,min_3','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'header_title'=>array('type'=>'input','lang'=>true,'name'=>l('Meta title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'meta_keywords'=>array('type'=>'input','lang'=>true,'name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'meta_description'=>array('type'=>'input','lang'=>true,'name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'name_seo'=>array('type'=>'input','lang'=>true,'name'=>l('Link url'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		1=>l('Alte detalii'),
		'location'=>array('type'=>'input','name'=>l('Locatia produsului'),'style'=>'width:92%;','info'=>l('Locatia produsului')),
		2=>l('Pret'),
		4=>'show_categories',
		'price'=>array('type'=>'input','name'=>l('Pret'),'info'=>l('Pret')),
		//5=>'show_currencies',
		'condition'=>array('type'=>'radio','name'=>l('Conditie'),'options'=>array(1=>l('Used'),0=>l('New'))),
		'is_negociable'=>array('type'=>'radio','name'=>l('Negociabil'),'options'=>array(0=>l('Da'),1=>l('Nu'))),
		'is_refundable'=>array('type'=>'radio','name'=>l('Returnabil'),'options'=>array(0=>l('Da'),1=>l('Nu'))),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		15=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder,'multiple'=>true,'images'=>'return_images')
		//16=>'product_images',
		);
	}
	function return_images($data) {
		return fa("SELECT * FROM `".TABLE_ADS_IMAGES."` WHERE `id_product`=".(int)$data['id']." ORDER BY `id` DESC");
	}
	function json_list() {
		json_list($this,true);
	}
	function css() {
		?>
		#uploaded_images img{ cursor:move; }
		.module_menu li.update_all { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.update_all_filters { background-image:url('../../images/icons/products_update_all.png'); }
		.module_menu li.port_categories { background-image:url('../../images/icons/products_port.png'); }
		.module_menu li.verify_codes { background-image:url('../../images/icons/products_verify_codes.png'); }
		.module_menu li.export_codes { background-image:url('../../images/icons/products_export_codes.png'); }
		.module_menu li.update_provider { background-image:url('../../images/icons/update_provider.png'); }
		.module_menu li.price_all { background-image:url('../../images/icons/update_multistatus.png'); }
		<?php
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii anunturi')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px" onchange="json_list_filter(this.value,<?=(int)$date['id']?>);">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga categorie')?>" onclick="add_category();" />
			</dd>
		</dl>
		<?php
	}
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="currency" id="currency" style="width:200px">
				<?php $this->json_currencies($date['currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		if ($id_category==0 && $_GET['from_grid']!=1) $id_category=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CATEGORIES."` ");
		$categories=lang_fetch_all(TABLE_CATEGORIES," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function js() {
		$after='$("#gbox_list_'.$this->module.'").css({"borderRight":"0px none"}).removeClass("ui-corner-all"); ';
		$loadComplete='
			$("#gs_id_category").load("module/'.$this->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
			$("#gs_id_brand").load("module/'.$this->module.'.php?json_brands=1&from_grid=1&id="+$("#gs_id_brand").val());
			$("#gs_currency").load("module/'.$this->module.'.php?json_currencies=1&from_grid=1&id="+$("#gs_currency").val());
		';
		set_grid($this,array('multiselect'=>'true','sortorder' => 'desc','loadComplete'=>$loadComplete),'',$after);
?>
var global_window=true;
var window_width=900;
var window_add_edit_name_brand="<?=l('Add Brand')?>";
var window_add_edit_name_category="<?=l('Add Category')?>";
var window_add_edit_name_currency="<?=l('Add Currency')?>";
function do_product_filter(id,div) {
			var data="related_id_category="+$("#related_id_category").val()+"&related_product_name="+$("#related_product_name").val();
				$.ajax({
					data :data,
					type :"POST",
					url :'module/<?=$this->module?>.php?json_product_filter=1',
					timeout :45000,
					error : function() {
						console.log("Failed to submit - ");
					},
					success : function(r) {
						$('#list_products_dd').html(r);
				$("#list_products_dd select").toChecklist();
}});
}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function list_variations_options(id_var,id){
	$.ajax({
		data :"",
		type :"POST",
		url :'module/<?=$this->module?>.php?list_variations_options=1&id_var='+id_var+'&id='+id,
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#list_variations_options').html(r);
			/*
			tableToGrid("#table_variations");
			$("#gbox_table_variations").removeClass("ui-corner-all");
			$("#gview_table_variations .ui-jqgrid-bdiv").attr("style","height:360px; overflow-y:scroll; overflow-x:hidden;");
			*/
		}});
}

function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function load_brands(){
	$("#id_brand").load('module/<?=$this->module?>.php?json_brands=1');
}
function json_currencies(){
	$("#currency").load('module/<?=$this->module?>.php?json_currencies=1');
}
function add_currency(){
	var module_now='currencies';
	nss_win(module_now + '_new', window_add_edit_name_currency,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',json_currencies);
	});
}
function add_brand(){
	var module_now='brands';
	nss_win(module_now + '_new', window_add_edit_name_brand,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_brands);
	});
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function update_all_specification(id_category){
	$("#update_all_specification").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category);
}
function update_all_filter(id_category,ids){
	$("#update_all_filter").load('module/<?=$this->module?>.php?json_list_filters=1&id_category='+id_category+'&ids='+ids);
}
function update_all_category(id_category,ids){
	$("#update_all_category").load('module/<?=$this->module?>.php?json_list_categories=1&id_category='+id_category+'&ids='+ids);
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
	$(document).ready(function(){
			$(".module_menu .update_all").click(function(){
				var window_add_edit_name='<?=l('Update all products')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_all_filters").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_all_filters&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .port_categories").click(function(){
				var window_add_edit_name='<?=l('Update all filters')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=port_categories&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .verify_codes").click(function(){
				var window_add_edit_name='<?=l('Verify codes')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=verify_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .export_codes").click(function(){
				var window_add_edit_name='<?=l('Export codes')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=export_codes&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .update_provider").click(function(){
				var window_add_edit_name='<?=l('Aloca provider')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=update_provider&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
			$(".module_menu .price_all").click(function(){
				var window_add_edit_name='<?=l('Promote ads')?>';
				var gr = jQuery("#list_<?=$this->module?>").getGridParam('selarrrow');
				nss_win("<?=$this->module?>_edit",window_add_edit_name,'module/<?=$this->module?>.php?action=price_all&ids='+gr,800, function () {
				after_window_load('<?=$this->module?>','edit');
				},true);
			});
	});
<?php
	}
	function json_list_row($row) {
		$admin_info=adm_info();
		global $gd,$db;
		$row['id_category']=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES.TABLE_EXTEND."` WHERE `id_main`=".$row['id_category']." AND `lang`='".LANG."'");
		if ($row['id_category']==false) {
			$row['id_category']='<span style="color:red;">'.l('Categoria nu mai exista').'</span>';
		}
		$row['id_user']=$db->fetch_one("SELECT `username` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_user']);
		if ($row['id_user']==false) {
			$row['id_user']='<span style="color:red;">'.l('Userul nu mai exista in db').'</span>';
		}
		$row['name']="<a href=".lang_item_link(TABLE_CATEGORIES,lang_item_info($this->table,$row['id']))." targer='_blank'>".$row['name']."</a>";
		$row['image']='<img src="../'.$gd->url('resize',(string)lang_item_image($this->table.'_images',$row),'48x48').'&<?=time()?>" alt="'.$row['title'].'" />';
		$row['id_currency']=$db->fetch_one("SELECT code FROM `".TABLE_CURRENCIES."` WHERE `id`=".$row['id_currency']);
		return $row;
	}
	function price_all($ids) {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=price_save_all&ids=<?=$ids?>" method="POST">
		<?php
		print_form_header (l('Promote adds'));
		?>
		<dl>
		<dt>
			<label for="price"><?=l('Introdu valabilitate plata')?></label>
		</dt>
		<dd>
			<input type="text" name="price" value="" />
		</dd>
		</dl>
		<?php
		print_form_footer();
		?>
		<?php
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function grid_edit(){
		global $db,$array_size_img;
		if ($_POST['oper']=='del') global_delete($this->table,true);
		$ids=explode(",",fpost('id'));
		foreach ($ids as $id) {
			$images=$db->fetch_all("SELECT * FROM `".TABLE_ADS_IMAGES."` WHERE `id_product`=".$id);
			foreach ($images as $image) {
				foreach ($array_size_img as $size) {
					@unlink(SITEROOT."/".$size."-".$image);
				}
				@unlink(SITEROOT."/".$image);
			}
			$db->delete(array(TABLE_ADS_IMAGES)," `id_product`=".$id);
		}
	}
	function new_a($id=0) {
		global $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$data_saved=lang_fetch($this->table,"`id`=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$data_saved);
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;
		$data_images=$data['image'];
		unset($data['image']);
		foreach ($data['nss_languages'] as $lang=>$vars) $data['nss_languages'][$lang]['name_seo']=escapeIlegalChars($data['nss_languages'][$lang]['name']." ".$data['code']," ");
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			if (!empty($id)) $cond="AND `id_main`<>".$id;
			$exist=$db->fetch_one("SELECT `id_main` FROM ".$this->table.TABLE_EXTEND." WHERE `name_seo`='{$data['nss_languages'][LANG]['name_seo']}' {$cond} AND `lang`='".LANG."'");
			if (!$exist) {
				if (!empty($id)) {
					lang_update($this->table, $data," `id`=".$id);
				} else {
					$data['date']=time();
					$id=lang_insert($this->table, $data);
				}
				upload_images($data_images,$data['nss_languages'][LANG]['name_seo'],$this->folder,"",$id,TABLE_ADS_IMAGES,'id_product');
				close_window($this->module,$id);
				print_alerta('a fost inserat/updatat');
			} else print_alerta('pe site mai exista un produs cu acelasi nume');
		} else {
			print_form_errors($errors,$form);
		}
	}

	function price_save_all(){
		global $db;
		$ids=(r('ids'));
		if (!empty($ids) && is_numeric($_POST['price'])) $db->sql("UPDATE `{$this->table}` SET `payment_valability`=".$_POST['price'].",`payment_date`=".time().",`is_paid`=1 WHERE `id` IN ({$ids})");
		print_alerta('a fost updatat');
	}
}
$module=new ads_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_currencies']==1) $module->json_currencies(fget('id'));
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='price_all') $module->price_all(fget('ids'));
elseif ($_GET['action']=='price_save_all') $module->price_save_all();
else {
	print_header();
	$admin_info=adm_info();
	/*if ($admin_info['username']=='admin') print_content($module,array());
	else print_content($module,array(),'','',array('delete'));*/
	print_content($module,array("price_all"=>l('Promote')));
	print_footer();
}
?>