<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class customers_companies_module {
	var $module='customers_companies';
	var $date='11-10-2009';
	var $name='Companii client';
	var $title='Modificare companie client';
	var $description='customers_companies_modul_description';
	var $table=TABLE_CUSTOMERS_COMPANIES;
	function customers_companies_module() {
		$this->type=array(
		'like'=>array('phone','email','first_name','last_name','status'),
		'date'=>array('date','last_login'),
		'equal'=>array('type','status')
		);
		$this->grid=array(
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'id_customer'=>array('name'=>l('Nume client'),'width'=>140),
		'name'=>array('name'=>l('Nume companie'),'width'=>140),
		'cui'=>array('name'=>'CUI','width'=>100),
		'nr_reg_com'=>array('name'=>l('Nr. Reg. Com.'),'width'=>100),
		'company_city'=>array('name'=>l('Oras'),'width'=>100),
		'company_state'=>array('name'=>l('Judet'),'width'=>100),
		'company_address'=>array('name'=>l('Adresa'),'width'=>200),
		'company_zip_code'=>array('name'=>l('Cod postal'),'width'=>80),
		'bank'=>array('name'=>l('Nume banca'),'width'=>100),
		'iban_bank'=>array('name'=>l('IBAN'),'width'=>100),
		'company_phone'=>array('name'=>l('Telefon'),'width'=>100),
		'actions'=>array('name'=>l('Actions'),'width'=>140,'align'=>'center','sortable'=>false)
		);
		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume companie'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'cui'=>array('type'=>'input','name'=>'CUI','valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'nr_reg_com'=>array('type'=>'input','name'=>'Nr. Reg. Com.','valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'company_city'=>array('type'=>'input','name'=>l('city'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'company_state'=>array('type'=>'input','name'=>l('Judet'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'company_address'=>array('type'=>'text','name'=>l('Adresa'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'company_zip_code'=>array('type'=>'input','name'=>l('zip code'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'bank'=>array('type'=>'input','name'=>l('Banca'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'iban_bank'=>array('type'=>'input','name'=>'IBAN','valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		'company_phone'=>array('type'=>'input','name'=>l('phone'),'valid'=>'empty,min_4,max_100','style'=>'width:92%;'),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;
		$row['id_customer']=(string)$db->fetch_one("SELECT `first_name` FROM `".TABLE_CUSTOMERS."` WHERE `id`=".$row['id_customer']);
		return $row;
	}
	function list_json(){
		$id_customer=r('id_customer');
		if (!empty($id_customer)) {
			$new_sql="SELECT * FROM `" . $this->table . "` WHERE `id_customer`=".$id_customer;
			json_list($this,false,$new_sql);
		} else json_list($this);
	}
	function js($id_customer=0) {
		?>
		var window_width=600;
		<?php if (!empty($id_customer)) { ?>		
		add_all_url['<?=$this->module?>']='&id_customer=<?=$id_customer?>';
		<?php	
		}
		set_grid($this);
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="post">
		<input type="hidden" name="id_customer" value="<?=(int)fget('id_customer')?>" />
		<?php
		print_form_header(l('Adauga/modifica companie client'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$form);
		}
	}
}
$module=new customers_companies_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($module_html) print_content($module);
elseif ($_GET['json_list']==1) $module->list_json();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module,array(),'','',array('new','edit'));
	print_footer();
}
?>