<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class pages_module{
	var $module='pages';
	var $date='25-08-2009';
	var $table=TABLE_PAGES;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function pages_module() {
		$this->name=l('Pagini site');
		$this->title=l('Pagini site');
		$this->description=l('Pagini site');

		$this->type=array(
		'like'=>array('id','title','name'),
		'date'=>array('date'),
		'equal'=>array('status','id_category')
		);

		$this->folder='imagini-pagini';

		$this->grid=array(
		0=>array('order'=>'order asc'),
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume pagina'),'width'=>200),
//		'id_category'=>array('name'=>l('Categorie'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_promo'=>array('name'=>l('promovat index cu poza'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'is_header'=>array('name'=>l('In header'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'is_footer'=>array('name'=>l('In footer'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_info'=>array('name'=>l('informatie utila'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_navigator'=>array('name'=>l('apare in navigator'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume pagina'),'text'=>'','info'=>'','valid'=>'empty,min_2,max_100','style'=>'width:70%;','auto'=>'header_title,meta_description,meta_keywords,name_seo','lang'=>true),
		'header_title'=>array('type'=>'input','name'=>l('Titlu pagina'),'style'=>'width:70%;','lang'=>true),
		'meta_description'=>array('type'=>'input','name'=>l('Meta descriere'),'style'=>'width:70%;','lang'=>true),
		'meta_keywords'=>array('type'=>'input','name'=>l('Meta cuvinte cheie'),'style'=>'width:70%;','lang'=>true),
		'name_seo'=>array('type'=>'input','name'=>l('Link intern'),'style'=>'width:70%;','lang'=>true),
		'url'=>array('type'=>'input','name'=>l('Link extern'),'style'=>'width:70%;','lang'=>true),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:70%;'),
		//'class'=>array('type'=>'input','name'=>l('Clasa'),'style'=>'width:70%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//'is_promo'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('promovat index cu poza')),
		//'is_footer'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('In footer')),
//		'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('informatie utila')),
		//'is_navigator'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in navigator')),
		//'is_info'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('apare in index text')),
		'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true,'style'=>'width:70%;height:200px;'),
		//1=>'show_categories',
		'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		//2=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine buton'),'text'=>'170x30','folder'=>$this->folder),
		//'big_image'=>array('type'=>'image','name'=>l('Imagine mare'),'text'=>'770x110','folder'=>$this->folder)
		);
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this,true);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

		if ($row['is_header']==1) {
			$row['is_header']='<img id="id_line_'.$row['id'].'" rel="is_header" class="ch_staus_img" src="images/tick.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		} else {
			$row['is_header']='<img id="id_line_'.$row['id'].'" rel="is_header" class="ch_staus_img" src="images/cross.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		}

		if ($row['is_footer']==1) {
			$row['is_footer']='<img id="id_line_'.$row['id'].'" rel="is_footer" class="ch_staus_img" src="images/tick.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		} else {
			$row['is_footer']='<img id="id_line_'.$row['id'].'" rel="is_footer" class="ch_staus_img" src="images/cross.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		}

		if ($row['is_info']==1) {
			$row['is_info']='<img id="id_line_'.$row['id'].'" rel="is_info" class="ch_staus_img" src="images/tick.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		} else {
			$row['is_info']='<img id="id_line_'.$row['id'].'" rel="is_info" class="ch_staus_img" src="images/cross.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		}

		if ($row['constructor']=='') {
			$row['type']='<img src="images/tick.gif"  width="16" height="16" alt="tick"  />';
		} else {
			$row['type']='<img src="images/cross.gif" width="16" height="16" alt="cross"  />';
			$row['actions']='<a class="action edit" onclick="do_edit('.$row['_id'].',\'pages\');" >'.l('edit').'</a>';
		}
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie site')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;

		$categories=lang_fetch_all('xp_pages_categories'," `id_parent`=".$id_parent." ORDER BY `order` ASC",LANG );
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		if ($id_parent==0 ) { ?>
			<option value="0"><?=l('Niciuna')?></option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_admin = lang_fetch($this->table,"`id`=".$id);
		}
//		if ($date_admin['image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['image'])){
//			$date_admin['image']='static/i/'.$this->folder.'/'.$date_admin['image'];
//		} else $date_admin['image']='';
//
//		if ($date_admin['big_image']!=''&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_admin['big_image'])){
//			$date_admin['big_image']='static/i/'.$this->folder.'/'.$date_admin['big_image'];
//		} else $date_admin['big_image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica pagini site'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;

		foreach(
			$data['nss_languages'] as
			$lang => $d
		){
			$data['nss_languages'][$lang]['name_seo'] = url($data['nss_languages'][$lang]['name_seo'], $lang);
		}

		if (isset($data['nss_languages']['ro']['description'])) {
		$data['nss_languages']['ro']['description']=str_replace('<iframe','<iframe border="0" frameborder="0" style="border:0;"',$data['nss_languages']['ro']['description']);
		}
		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'],'-'),'static/i/'.$this->folder,''));
//			$data['big_image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['big_image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'].' big','-'),'static/i/'.$this->folder,''));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				lang_update($this->table, $data," `id`=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new pages_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>