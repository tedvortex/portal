<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");

class categoriesblog_module {
	var $module='categoriesblog';
	var $date='27-08-2009';
	var $table='xp_news_categories';
	var $folder='categories_images';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function categoriesblog_module() {

		$this->name=l('Categorii Blog');
		$this->title=l('Categorii Blog');
		$this->description=l('Categorii Blog');

		$this->type=array(
		'like'=>array('name','id'),
		'equal'=>array('status')
		);
		$this->folder='imagini-categorii';
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Titlu'),'valid'=>'empty,min_1,max_200','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo','lang'=>true),
		'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','lang'=>true),
		'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','lang'=>true),
		'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','lang'=>true),
		'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','lang'=>true),
		'status'=>array('type'=>'radio','name'=>l('status'),'options'=>array(1=>l('activ'),0=>l('Inactiv'))),
		//'description'=>array('type'=>'editor','name'=>l('Descriere'),'lang'=>true),
		//'short_description'=>array('type'=>'text','name'=>l('Scurta descriere'),'lang'=>true),
		#1=>'show_categories',
		#2=>l('Imagini'),
		#'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>$this->folder)
		);
	}
	function css() {}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
			$categories=$db->fetch_all("select * from `".$this->table."` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorii')?></label>
			</dt>
			<dd>
				<select name="id_parent" id="id_parent" style="width:200px">
				<option value="0"><?=l('Niciun parinte')?></option>
				<?=$this->json_categories($date['id_parent'])?>
				</select>
			</dd>
		</dl>
		<?
	}
	function js() {}
	function print_records() {
	?>
	<h2><?=l('Categorii Blog')?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="nss_grid">
		<table cellspacing="0" cellpadding="0" border="0"  >
		<thead>
			<tr role="rowheader" class="ui-jqgrid-labels">
				<th><?=l('nume')?></th>
				<!--<th style="width: 60px;"><?=l('Anunturi')?></th>-->
				<th style="width: 60px;"><?=l('vizibil')?></th>
				<th style="width: 140px;"><?=l('Actions')?></th>
			</tr>
		</thead>
		</table>
	</div>
	<div id="nss_list_record">
	<?php  $this->reg_get_categs((int)r('cid')); ?>
	</div>
	<br />
	<?php
	}

	function reg_get_categs($id_parent=0,$content="",$nr=0) {
		global $db;
		if ($nr>4) die();
		if ($nr==0) {
			$id_id='id_parent';
		} else $id_id='c.id';

		$categories =
		$db->fetch_all("
			select cd.*,c.*
			from
				`".$this->table."` c
				inner join `".$this->table."_data` cd
					on c.`id` = cd.`_id`
			where
				{$id_id} = {$id_parent} and
				`lang` = '".LANG."'
			ORDER BY `order` ASC
		");

		if (empty($categories)) {
			?>
			<ul style="list-style-type:none;	margin:0;		padding:0;" class="connectedSortable">

			<?php
			?>
				<li id="ele-0">
						<div style="padding:2px; ">
						<a href="?mod=<?=$this->module?>&cid=0" onclick="add_all_url['<?=$this->module?>']='&cid=0'; reload_list_record('<?=$this->module?>'); return false; ">
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l('Categorii Blog')?></span>
						</a>
					</div>
				</li>
			<?=$content?>
			</ul>
			<?php
		} else {


			ob_start();
			$exist_p=fo("SELECT `id` FROM `".$this->table."` WHERE `id`=".(int)$categories[0]['id_parent']);

			?>
			<ul  style="list-style-type:none;	margin:0;	 padding:0;
			 <?php if ($exist_p) { ?>			margin-left:20px; <?php } ?> "
			<?php if ($nr==0)  { ?> class="list_sort" <?php } ?> >
			<?php
			$i=0;
			foreach ($categories as $category) {
				$i++;
				//$nr_products=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_ADS."` WHERE `id_category`=".$category['id']);
				//$nr_products=product_count(','.$category['id_main'].categ_children($category['id_main']));
				if ($category ['status'] == 1) {
					$category ['status'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '" onclick="ajax_ch_status(\''.$this->module.'\',this);" />';
				} else {
					$category ['status'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '"  onclick="ajax_ch_status(\''.$this->module.'\',this);"  />';
				}
				$exist=fo("SELECT `id` FROM `".$this->table."` WHERE `id_parent`=".$category['id']);
			?>
					<li id="ele-<?=$category['id']?>">
						<div style="float:left;">
						<?php if ($nr==0 /*&& $i!=sizeof($categories)*/)  {
							?>
							<img src="images/icons/branch.gif" style="vertical-align:middle;" />
							<?php
						} else { ?>
						<img src="images/icons/branch2.gif" style="vertical-align:middle;" />
						<?php  } if (!empty($exist)) { ?>
						<a href="?mod=<?=$this->module?>&cid=<?=$category['id']?>" onclick="add_all_url['<?=$this->module?>']='&cid=<?=$category['id']?>'; reload_list_record('<?=$this->module?>'); return false; ">
						<?php } ?>
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=$category['name']?></span>
						<?php if (!empty($exist)) { ?>
						</a>
						<?php } ?>
						</div>
						<div style="width: 140px;float:right; padding:2px; padding-top:3px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="delete_record(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 60px;float:right; padding:1px 2px; text-align:center;"><?=$category['status']?></div>
						<!--<div style="width: 60px;float:right; padding:3px 2px; text-align:center;"><?=$nr_products?></div>-->
						<div class="clear"></div>
				</li>
				<?php
				$id_parent=$category['id_parent'];
			}
			echo $content;
			?>
			</ul>
		<?php
		$file_data = ob_get_contents();
		ob_end_clean();
		$this->reg_get_categs($id_parent,$file_data,$nr+1);
		}
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fost('id'));
		function delete_recursive($id,$table) {
			if(!empty($id)) {
				$childs=$db->fetch_all("SELECT * FROM `".$table."` WHERE id_parent=".$id);
				if (empty($childs)) {
					$db->delete($table," id=".$id);
					$db->delete($table.TABLE_EXTEND," id_main=".$id);
				} else {
					foreach ($childs as $child) {
						delete_recursive($child['id'],$table);
					}
				}
			} else return false;
		}
		foreach ($ids as $id) delete_recursive($id,$this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;

		if (!empty($id)){
			$date_saved = lang_fetch($this->table,"`id`=".$id);
		}

		if ($date_saved['image']!=''&&file_exists(SITEROOT.'/static/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/'.$this->folder.'/'.$date_saved['image'];
		} else $date_saved['image']='';

		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica categorie meniu'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db,$config,$gd;
		$data=$_POST;

		foreach(
			$data['nss_languages'] as
			$lang => $d
		){
			$data['nss_languages'][$lang]['name_seo'] = url($data['nss_languages'][$lang]['name_seo'], $lang);
		}

		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			//$data['image']=str_replace('static/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/'.$this->folder,''));
			close_window($this->module);
			if (!empty($id)) {
				lang_update($this->table, $data," `id`=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				$data['date']=time();
				lang_insert($this->table, $data);
				print_alerta(l('a fost inserat'));
			}
			?>
			<script type="text/javascript">
			reload_list_record('<?=$this->module?>');
			</script>
			<?
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function save_order() {
		global_save_order($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
}
$module=new categoriesblog_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->reg_get_categs((int)r('cid'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>