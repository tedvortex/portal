<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class plecari3_module{
	var $module='plecari3';
	var $date='25-08-2009';
	var $table='xp_croaziera_transport';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function plecari3_module() {
		$this->name=l('Seteaza plecari');
		$this->title=l('Seteaza plecari');
		$this->description=l('Seteaza plecari');

		$this->type=array(
		'like'=>array('id','title','name'),
		//'date'=>array('date'),
		'equal'=>array('status','id_city','id_croaziera','id_croaziera_transport')
		);

		//$this->folder='imagini-pagini';

		$this->grid=array(
		0=>array('order'=>'id asc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>60),
		//'id_sejur'=>array('name'=>l('Nume sejur'),'width'=>100),
		'name'=>array('name'=>l('Nume'),'width'=>100),
		'id_transport'=>array('name'=>l('Tip transport'),'width'=>100),
		//'id_board_type'=>array('name'=>l('Tip sejur'),'width'=>100),
		//'id_room_type'=>array('name'=>l('Tip camera'),'width'=>100),
		//'id_confirmation_type'=>array('name'=>l('Tip confirmare'),'width'=>100),
		'price'=>array('name'=>l('Taxe'),'width'=>120),
		//'adults'=>array('name'=>l('Adulti'),'width'=>120),
		//'children'=>array('name'=>l('Copii'),'width'=>120),
		//'telephone'=>array('name'=>l('Phone'),'width'=>120),
		//'website'=>array('name'=>l('Website'),'width'=>120),
		//'rating'=>array('name'=>l('Stele'),'width'=>50,'align'=>'center'),
		//'address'=>array('name'=>l('Adresa'),'width'=>150),
		//'id_city'=>array('name'=>l('Oras'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select'),
		//'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_promo'=>array('name'=>l('promovat index cu poza'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_header'=>array('name'=>l('In header'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_footer'=>array('name'=>l('In footer'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_info'=>array('name'=>l('informatie utila'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		//'is_navigator'=>array('name'=>l('apare in navigator'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'id_city'=>array('name'=>l('Oras'),'width'=>120,'align'=>'left','stype'=>'select','options'=>'generate_select2'),
		
		'order'=>array('name'=>l('Pozitia'),'width'=>50,'align'=>'center'),
		//'date'=>array('name'=>l('Date'),'width'=>60,'align'=>'center'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'style'=>'width:92%;'),
		//4=>'show_sejururi',
		//5=>'show_board',
		//6=>'show_room',
		5=>'show_state',
		6=>'show_city',
		7=>'show_confirm',
		
		'date'=>array('type'=>'date','name'=>l('Data plecarii')),
		//'date_end'=>array('type'=>'date','name'=>l('Pana la data')),
		
		//'price'=>array('type'=>'input','name'=>l('Total taxe'),'style'=>'width:70%;'),
		//8=>'show_currencies',
		//'adults'=>array('type'=>'input','name'=>l('Adulti'),'style'=>'width:70%;'),
		'description'=>array('type'=>'text','name'=>l('Descriere'),'style'=>'width:70%;height:100px'),
		//'children'=>array('type'=>'input','name'=>l('Copii'),'style'=>'width:70%;'),
		
		//'type'=>array('type'=>'radio','name'=>l('Tip pret'),'options'=>array(1=>l('Pe adult'),0=>l('Pe camera'))),
		//'max_adults'=>array('type'=>'input','name'=>l('Max. Adulti'),'style'=>'width:70%;'),	
		);
	}
	
	function generate_select2() {
		$_GET['from_grid']=1;
		$this->json_city2();
	}
	
	function show_city2($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Oras')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px">
				<?php $this->json_city($date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_city2($id_brand=0) {
		global $db;

		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `xp_cities` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_cities_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	
	function show_state($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_country"><?=l('Tara')?></label>
			</dt>
			<dd>
				<select name="id_country" id="id_country" style="width:200px" onchange="json_list_filter2(this.value,<?=(int)$date['id_country']?>);">
				<?php $this->json_state($date['id_country']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function show_city($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Orasul')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px" onchange="json_list_filter3(this.value,<?=(int)$date['id_city']?>);">
				<?php $this->json_city($date['id_country'],$date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	
	function json_state($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Selectati tara</option><?php
		$resursa=$db->query("SELECT * FROM `xp_countries_data`  ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_city($id_state,$id_brand=0) {
		global $db;

		if (empty($id_state)) {
			?><option value="NULL">Selectati orasul</option><?php
			return false;
		}
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT
								p.* ,pd.*
							FROM `xp_cities` p
							inner join `xp_cities_data` pd
							 on p.`id`=pd.`_id` WHERE p.`id_country`=".$id_state." ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	
	
	function show_currencies($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="currency"><?=l('Moneda')?></label>
			</dt>
			<dd>
				<select name="id_currency" id="id_currency" style="width:200px">
				<?php $this->json_currencies($date['id_currency']) ?>
				</select>
				<input type="button" class="ui-state-default ui-corner-all padding_2" value="<?=l('Adauga moneda')?>" onclick="add_currency();" />
			</dd>
		</dl>
		<?php
	}
	function json_currencies($id_currency=0) {
		global $db;
		if ($id_currency==0  && $_GET['from_grid']!=1) $id_currency=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_CURRENCIES."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_currency=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_CURRENCIES."` ORDER BY `code` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_currency)?'selected':''?> > <?=$linie['code']?> </option>
			<?php
		}
	}
	function show_sejururi($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_sejur"><?=l('Nume sejur')?></label>
			</dt>
			<dd>
				<select name="id_sejur" id="id_sejur" style="width:400px">
				<?php $this->json_sejururi($date['id_sejur']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_sejururi($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
			$resursa=$db->query("SELECT
								pd.`name`,p.`id`

								 FROM
								`xp_croaziera` p
							inner join  `xp_croaziera_data` pd
						 		on p.`id`=pd.`_id` where  p.`id_category`=1 ORDER BY p.`date` desc");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_board($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_board_type"><?=l('Tip sejur')?></label>
			</dt>
			<dd>
				<select name="id_board_type" id="id_board_type" style="width:400px">
				<?php $this->json_board($date['id_board_type']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_board($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_board_types_data` where `lang`='en' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_room($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_room_type"><?=l('Tip camera')?></label>
			</dt>
			<dd>
				<select name="id_room_type" id="id_room_type" style="width:400px">
				<?php $this->json_room($date['id_room_type']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_room($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_room_types_data` where `lang`='en' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function show_confirm($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_transport"><?=l('Tip transport')?></label>
			</dt>
			<dd>
				<select name="id_transport" id="id_transport" style="width:400px">
				<?php $this->json_confirm($date['id_transport']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_confirm($id_brand=0) {
		global $db;
		//if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		?><option>Select</option><?php
		$resursa=$db->query("SELECT * FROM `xp_transport_data` where `lang`='".LANG."' ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected="selected"':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {
		?>
		#main { width:100%; }
		#content,#menu { width:98%;margin:0 1%; }
		<?php
	}
	function json_list() {
		json_list($this);
	}
	function generate_select() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function js() {

		set_grid($this);

?>
var window_width=875;
var window_add_edit_name_category="<?=l('Add Category')?>";

function json_list_filter2(id_country,id){
		var id_city=0;
		$("#id_city").load('module/<?=$this->module?>.php?json_city=1&id_country='+id_country+'&id='+id);
		$("#id_hotel").load('module/<?=$this->module?>.php?json_hotel=1&id_city='+id_city+'&id='+id);

		}
function json_list_filter(id_category,id){
	$("#list_filters").load('module/<?=$this->module?>.php?json_list_filter=1&id_category='+id_category+'&id='+id);
	$("#list_specifications").load('module/<?=$this->module?>.php?json_list_specifications=1&id_category='+id_category+'&id='+id);
}
function load_categories(){
	$("#id_category").load('module/<?=$this->module?>.php?json_categories=1');
}
function add_category(){
	var module_now='categories';
	nss_win(module_now + '_new', window_add_edit_name_category,
	'module/' + module_now + '.php?action=new', 800,
	function() {
		$("#window_" + module_now + "_new form").bind('submit',
		function() {
			return false;
		});
		after_window_load(module_now, 'new',load_categories);
	});
}
function set_product_order(order,id) {
$.ajax( {
		data :"ch_order=1&id="+ id+"&order="+order,
		type :"GET",
		url :'module/<?=$this->module?>.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {	}
	});
}
function create_upload_form(){
		$.ajax( {
		data :'operation=create_upload_form&videoTitle='+ $('#video_title').val()+
		'&videoDescription=' + $('#video_desc').val() +
		'&videoCategory=' + $('#video_categ').val() +
		'&videoTags=' + $('#video_tags').val(),
		type :"POST",
		url :'operations.php',
		timeout :45000,
		error : function() {
			console.log("Failed to submit - ");
		},
		success : function(r) {
			$('#div_return_new_video').html(r);
		}
		});
}
<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $db;

		//$row['id_sejur']=$db->fetch_one("select `name` from `xp_croaziera_data` WHERE `_id`={$row['id_sejur']} ");
		$row['id_transport']=$db->fetch_one("select `name` from `xp_transport_data` WHERE `_id`={$row['id_transport']} ");
		$row['order'] = '<input size="2" name="order['.$row['id'].']" value="'.$row['order'].'" style="text-align:center;width:30px;" onchange="set_product_order(this.value,\''.$row['id'].'\');reload_grid(\''.$this->module.'\');" />';
		if ($row['id_city']>0) {
			$row['id_city']=$db->fetch_one("select `name` from `xp_cities_data` WHERE `_id`={$row['id_city']} ");
		} else {
			$row['id_city']='Plecare Individuala';
		}
		
		//$row['id_board_type']=$db->fetch_one("select `name` from `xp_board_types_data` WHERE `id`={$row['id_board_type']} ");
		//$row['id_room_type']=$db->fetch_one("select `name` from `xp_room_types_data` WHERE `id`={$row['id_room_type']} ");
		$row['actions']='<a class="action edit" onclick="do_edit('.$row['id'].',\'plecari3\');" >'.l('edit').'</a>&nbsp;&nbsp;<a class="action delete" onclick="do_delete(\''.$row['id'].'\',\'plecari3\',\'\');" >'.l('Delete').'</a>';
		//$row['actions']="<a class=\"action\" href=\"".BASEHREF."admin/image.php?id_product=".$row['id']."\">Galerie foto</a><br/><br/>".$row['actions'];
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table,true);
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_city"><?=l('Oras')?></label>
			</dt>
			<dd>
				<select name="id_city" id="id_city" style="width:200px">
				<?php $this->json_categories($date['id_city']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_categories($id_brand=0) {
		global $db;

		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `xp_cities` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `xp_cities_data` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_admin = $db->fetch("
							SELECT * FROM 
							`".$this->table."` p
							left join `xp_cities` pd 
							  on p.`id_city`=pd.`id` 
							where p.`id`=".$id);
			
			if ($date_admin['date']=='') $date_admin['date']=time();

		}
		
		if ($date_admin['date']=='') {
			$date_admin['date']=time();
		} else $date_admin['date']=strtotime($date_admin['date']);
		
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_admin);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function ch_order($id,$order) {
		global $db;
		$db->update($this->table,array('order'=>$order),"`id`='".$id."'");
	}
	function save($id){
		global $db,$gd;
		$data=$_POST;

		$data['date']=strtotime($data['date']);

		$data['date']=date('Y-m-d',$data['date']);


		if ($data['id_city']=='NULL') {
			unset($data['id_city']);
		}

		unset($data['id_country']);
		$errors = form_validation($data,$this->form,$this->table);
		if(empty($errors)) {
			close_window($this->module);
//			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'],'-'),'static/i/'.$this->folder,''));
//			$data['big_image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['big_image'],escapeIlegalChars($data['nss_languages']['ro']['name_seo'].' big','-'),'static/i/'.$this->folder,''));
//			$data['image2']=upload_images($data['image2'],escapeIlegalChars($data['nss_languages'][LANG]['name_seo']." 2","-"),$this->folder,'294x130');
			if (!empty($id)) {
				//unset($data['sursa']);
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new plecari3_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['json_city']==1) $module->json_city(fget('id_country'),fget('id'));
elseif ($_GET['ch_order']==1) $module->ch_order(fget('id'),fget('order'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>