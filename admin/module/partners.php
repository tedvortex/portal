<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class partners_module {
	var	$module='partners';
	var $date='27-08-2009';
	var $table='xp_partners';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function partners_module() {

		$this->name=l('Parteneri');
		$this->title=l('Parteneri');
		$this->description=l('Parteneri');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);
		$this->folder='parteneri';
		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Image'),'width'=>250,'align'=>'center'),
		'name'=>array('name'=>l('Name'),'width'=>200),
		'link'=>array('name'=>l('Link'),'width'=>200),
		//'type'=>array('name'=>l('Categorie'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';0:'.l('Crap').';1:'.l('Rapitor'))),
		//'is_promo'=>array('name'=>l('Promo'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Da').';0:'.l('Nu'))),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('nume'),'valid'=>'empty,min_1,max_140','style'=>'width:92%;','auto'=>'meta_keywords,meta_description,header_title,name_seo'),
		'link'=>array('type'=>'input','name'=>l('Link'),'style'=>'width:92%;',),
		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		'order'=>array('type'=>'input','name'=>l('Pozitia'),'style'=>'width:92%;'),
		//'type'=>array('type'=>'radio','options'=>array(0=>l('Crap'),1=>l('Rapitor')),'name'=>l('Categorie')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		1=>l('Imagini'),
		'image'=>array('type'=>'image','name'=>l('Baner - 340px width'),'folder'=>$this->folder)
		);
	}
	function json_list() {
		json_list($this);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd;
		
		if ($row['is_promo']==1) {
			$row['is_promo']='<img id="id_line_'.$row['id'].'" rel="is_promo" class="ch_staus_img" src="images/tick.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		} else {
			$row['is_promo']='<img id="id_line_'.$row['id'].'" rel="is_promo" class="ch_staus_img" src="images/cross.gif" style="cursor:pointer;" width="16" height="16" alt="tick"  />';
		}

		
		if (!empty($row['image']))
		$row['image']='<img src="../static/i/parteneri/'.$row['image'].'" width="300" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		if ($date_saved['image']&&file_exists(SITEROOT.'/static/i/'.$this->folder.'/'.$date_saved['image'])){
			$date_saved['image']='static/i/'.$this->folder.'/'.$date_saved['image'];
		} else $date_saved['image']='';
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
//		if ($data['type']==0) {
//			$tip='crap';
//		} elseif ($data['type']==1) {
//			$tip='rapitor';
//		}
		//$data['name_seo']=escapeIlegalChars($data['name'].' '.$tip," ");
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			$data['image']=str_replace('static/i/'.$this->folder.'/','',upload_images($data['image'],escapeIlegalChars($data['name'],"-"),'static/i/'.$this->folder,''));
			unset($data['type_op']);
			//if (!isset($data['image'])) $data['image']='';
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new partners_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module);
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>