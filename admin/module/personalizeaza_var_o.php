<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class personalizeaza_var_o_module {
	var $module='personalizeaza_var_o';
	var $date='14-10-2012';
	var $table='prefix_products_variations_options';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $folder='';
	var $title='';
	var $description='';
	var $_id_product=0;
	function personalizeaza_var_o_module() {

		$this->name=l('Optiuni variatii produse personalizare');
		$this->title=l('Optiuni variatii produse personalizare');
		$this->description=l('Optiuni variatii produse personalizare');
		$this->folder='.options';

		$this->type=array(
		'like'=>array('name'),
		'equal'=>array('status','id_product','id_variation')
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'name'=>array('name'=>l('Nume optiune')),
		'id_product'=>array('name'=>l('Tip produs'),'width'=>200,'stype'=>'select','options'=>'show_select_product'),
		'id_variation'=>array('name'=>l('Nume variatie'),'width'=>200,'stype'=>'select','options'=>'show_select_var'),
		'price'=>array('name'=>l('Pret'),'width' =>100),
		'status'=>array('name'=>l('status'),'width'=>50,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume optiune')),
		1=>'select_product',
		2=>'select_var',
	//	3=>'select_var_child',
		'price'=>array('type'=>'input','name'=>l('Pret')),
		'new_pid'=>array('type'=>'input','name'=>l('ID tip produs nou'),'info'=>' * Se completeza cu ID daca este nevoie sa sara in alt produs. <br /> * Ramane necompletat pentru optiuni normale'),
		'color'=>array('type'=>'input','name'=>l('ID culoare text - cod hex')),
		'bold'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('Bold')),
		'um'=>array('type'=>'input','name'=>l('Unitate de masura')),
		'img_bg_edit'=>array('type'=>'image','name'=>l('Imagine fundal 2D'),'folder'=>$this->folder),
		'icon'=>array('type'=>'image','name'=>l('Icon'),'folder'=>$this->folder,'info'=>'Numai pentru campurile textura si icon. <br /> Dimensiune pentru Textura: 120x30 <br /> Dimensiune pentru Icon: 50x50'),
		'calcul'=>array('type'=>'radio','options'=>array(1=>l('Vizibil'),0=>l('Nu apare')),'name'=>l('Tab calcul')),
		'calcul_text'=>array('type'=>'text','name'=>l('Text calcul')),
		'default'=>array('type'=>'radio','options'=>array(1=>l('Da'),0=>l('Nu')),'name'=>l('Default')),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('Status')),
		'order'=>array('type'=>'input','name'=>l('Ordine')),
		
		//'size'=>array('type'=>'input','name'=>l('Dimensiune')),
		);
 


	}
	function css() {
		?>

		<?php
	}
	function show_select_var() {
		$_GET['from_grid']=1;
		$this->json_select_var();
	}
	function show_select_product() {
		$_GET['from_grid']=1;
		$this->json_select_product();
	}
	function select_product($date) {
		?>
		<dl>
			<dt>
				<label for="id_product"><?=l('Tip produs')?></label>
			</dt>
			<dd>
				<select name="id_product" id="id_product" style="width:200px" onchange="$('#id_variation').load('module/<?=$this->module?>.php?action=json_var&id_product='+this.value);">
				<?php $this->json_select_product($date['id_product']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_product($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) {
			$id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products` ");
			$this->_id_product=$id_brand;
		}
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function select_var_child($date) {
		if (empty($date['id_variation_child'])) {$date['id_variation_child']='_';}
		?>
		<dl>
			<dt>
				<label for="id_variation_child"><?=l('Alta variatie dependenta de optiune')?></label>
			</dt>
			<dd>
				<select name="id_variation_child" id="id_variation_child" style="width:200px">
				<option value="0" selected> <?=l('Aceasta optiune nu genereaza alte variatii')?> </option>
				<?php $this->json_select_var($date['id_variation_child']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function select_var($date) {  
		?>
		<dl>
			<dt>
				<label for="id_variation"><?=l('Variatie de care apartine optiunea')?></label>
			</dt>
			<dd>
				<select name="id_variation" id="id_variation" style="width:200px">
				<?php $this->json_select_var($date['id_variation']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_var($id_brand=0) {
		global $db;
		echo $id_brand;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products_variations` WHERE `id_product`=".(int)$this->_id_product);
		//echo $id_brand;  
		/*if (empty($id_brand)) { ?>
		<option value="_" > <?=l('Selectati un produs')?> </option>
		<?php
		}*/
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products_variations` WHERE `id_product`=".(int)$this->_id_product." ORDER BY `name` ASC ");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this,false);
	}
	function js() {
		$after="
		
		$('select[name=filter_id_product]').change(function(){
			$('select[name=filter_id_variation]').load('module/".$this->module.".php?action=json_var&id_product='+this.value);
		});
				";
		set_grid($this,array('multiselect'=>'true') ,'', $after );
		?>
		$(document).ready(function(){
			$(".module_menu .insert").click(function(){
			
				nss_win("<?=$this->module?>_email",window_add_edit_name,'module/<?=$this->module?>.php?action=insert',900, function () {
				$('#window_<?=$this->module?>_email form').submit( function() {
					var form_id=this;
					var t=setTimeout(function() {
						post_form(form_id);
					},200);
					return false;
				});
				$('input[name=cancel]').click(function(){ $('#window_<?=$this->module?>_email').dialog( 'close' ); });
				init_upload();
				},true);
			});
			
			
		});
		<?php
	}


	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function insert() {
		global $main_buttons;
		?>
		<form action="?mod=<?=$this->module?>&action=save_import" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		?>
		<dl>
			<dt><label for="code">Fisier CSV:</label></dt>
		<dd>
		 	<input type="hidden" class="file" value="" name="code" id="code" rel="csv,<?=TMP_IMG?>,0" />
		</dd>
		</dl>
		<?php
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		
		</form>
		<?php
	}
	function json_list_row($row) {
		global $db;
		$row['id_variation']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products_variations` WHERE `id`=".$row['id_variation']);
		$row['id_product']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products` WHERE `id`=".$row['id_product']);
		return $row;
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			$this->_id_product=$db->fetch_one("SELECT `id_product` FROM `prefix_products_variations` WHERE `id`=".$date_saved['id_variation']);
			$date_saved['id_product']=$this->_id_product;
			if ($date_saved['img_bg_edit']!=''&&file_exists(SITEROOT.'/static/i/preview/'.$this->folder.'/'.$date_saved['img_bg_edit'])){
				$date_saved['img_bg_edit']='static/i/preview/'.$this->folder.'/'.$date_saved['img_bg_edit'];
			} else $date_saved['img_bg_edit']='';
			if ($date_saved['icon']!=''&&file_exists(SITEROOT.'/static/i/preview/'.$this->folder.'/'.$date_saved['icon'])){
				$date_saved['icon']='static/i/preview/'.$this->folder.'/'.$date_saved['icon'];
			} else $date_saved['icon']='';
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		//unset($data['id_product']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
//			if (!empty($data['img_bg_edit'])) {
//				$_new_name=escapeIlegalChars($data['name']." ".$id,"-").".jpg";
//				$new_file=SITEROOT."/static/".$this->folder."/".$_new_name;
//				if (strpos($data['img_bg_edit'],TMP_IMG)===false){
//					$file=SITEROOT."/static/".$this->folder."/".$data['img_bg_edit'];
//				} else {
//
//					$file=SITEROOT."/".$data['img_bg_edit'];
//				}
//				rename($file,$new_file);
//				$data['img_bg_edit']=$_new_name;
//			}
//			if (!empty($data['icon'])) { 
//				$_new_name=escapeIlegalChars($data['name']." icon ".$id,"-").".jpg";
//				$new_file=SITEROOT."/static/".$this->folder."/".$_new_name;
//				if (strpos($data['icon'],TMP_IMG)===false){
//					$file=SITEROOT."/static/".$this->folder."/".$data['icon'];
//				} else {
//
//					$file=SITEROOT."/".$data['icon'];
//				}
//				rename($file,$new_file);
//				$data['icon']=$_new_name;
//			}
			unset($data['type_op']);
			$data['img_bg_edit']=str_replace('static/i/preview/'.$this->folder.'/','',upload_images($data['img_bg_edit'],escapeIlegalChars($data['name'].' '.$id,'-'),'static/i/preview/'.$this->folder,''));
			$data['icon']=str_replace('static/i/preview/'.$this->folder.'/','',upload_images($data['icon'],escapeIlegalChars($data['name'].' icon','-'),'static/i/preview/'.$this->folder,''));
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}  
}
$module=new personalizeaza_var_o_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module); 
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='insert') $module->insert();
elseif ($_GET['action']=='json_var') {
	$module->_id_product=fget('id_product');
	$module->json_select_var();
} else {
	print_header();
	print_content($module);
	print_footer();
}
?>