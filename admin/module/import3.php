<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class import3_module {
	var $module='import3';
	var $date='24-10-2009';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function import3_module() {

		$this->name=l('Import/Export');
		$this->title=l('Import/Export');
		$this->description=l('Import/Export');

	}
	function css() {}
	function js() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function new_a($id=0) {
		global  $db,$main_buttons;
		print_form_header(l('Import produse XLS'));
		?>

		
		<br />
	
		
		<hr />
		
		
		<?php 
		$_furnizori=array(
		//'nanjing'=>'NANJING',
		'croaziere'=>'CROAZIERE',
		//'sejururi_promo'=>'SEJURURI SPO',
		//'stoc'=>'STOC',
		//'jante'=>'IMPORT',
		);
		foreach ($_furnizori as $key=>$_temp_nw) { ?>
		<br />
		<form action="../admin/xml/<?=$key?>.php" method="post" target="iframe"  enctype="multipart/form-data">
			<dl>
			<dt>
				<label for="id_category">Fisier tarife <?=$_temp_nw?></label>
			</dt>
			<dd>
				<input type="file" name="products" />
				&nbsp; &nbsp; &nbsp;
				<input type="submit" class="ui-state-default ui-corner-all padding_2" value="<?=('Trimite')?>" />
			
			</dd>
		</dl>
		</form>
		
		<?php
		}
		print_form_footer();
		?>

		<iframe style="width:100%;height:500px;border:1px solid #000;" id="iframe" name="iframe"></iframe>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `".TABLE_CATEGORIES."` where `id_parent`=".$id_parent." ORDER BY `order` ASC");
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_category)?'selected':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['id'],$deep+1);
		}
	}
	function load_specifications($id_category){
		global $db;
		$filters=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
		foreach ($filters as $filter) {?>
		<dl>
			<dt>
				<input type="checkbox" id="specification_header_<?=$filter['id']?>" name="specifications_header[]" value="1" align="absmiddle" onclick="$(this).parent().parent().find('dd input').attr('checked', $(this).attr('checked')).attr('disabled', !$(this).attr('checked')); " />
				<label for="specification_header_<?=$filter['id']?>"  align="absmiddle"  >
					<?=$filter['name']?>
				</label>
			</dt>
			<dd>
				<?$options=$db->fetch_all("SELECT  * FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".$filter['id']." order by `order` asc " );
				foreach ($options as $option) {?>
					<input type="checkbox" name="specifications[<?=$option['id']?>]" value="1"  id="specification_<?=$option['id']?>"   />
					<label for="specification_<?=$option['id']?>"  align="absmiddle" >
						<?=$option['name']?>
					</label>
					<br />
				<?}?>
			</dd>
		</dl>
		<?}?>
		<dl>
			<dt>
				<input type="checkbox" id="filters_show" name="filters_header[]" value="1" align="absmiddle" checked onclick="$(this).parent().parent().find('dd input').attr('checked', $(this).attr('checked')).attr('disabled', !$(this).attr('checked')); " />
				<label for="filters_show"  align="absmiddle"  >
					<?=l('Filtre')?>
				</label>
			</dt>
			<dd>
				<?php
				$filters=$db->fetch_all("SELECT  * FROM `".TABLE_FILTERS."`  WHERE `id_category`=".(int)$id_category." OR  `id_category`=0 order by `order` asc " );
				foreach ($filters as $filter) {?>
					<input type="checkbox" name="filters[<?=$filter['id']?>]" value="1"  id="filter_<?=$filter['id']?>" checked  />
					<label for="filter_<?=$filter['id']?>"  align="absmiddle" >
						<?=$filter['name']?>
					</label>
					<br />
				<?}?>
			</dd>
		</dl>
		<?php

	}
	function update_filtre(){
		global $db,$config,$gd;
		$category_id=$_POST['id_category'];
		if (empty($_FILES['products']['name'] )) {
			$error=l('Select a file');
		}elseif (empty($category_id)) {
			$error=l('Select a caregory');
		} else {
			$handle = fopen($_FILES['products']['tmp_name'], "rb");
			$i=0;
			while (!feof($handle)) {
				$i++;
				$line = fgets($handle);
				$cols=explode("$",$line);
				if ($i==1) $header=$cols;
				$id_product=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='".trim($cols[0])."'");
				echo $id_product;
				if ($i>1 && !empty($cols[0])&&!empty($id_product)) {
					for ($j=1;$j<=sizeof($cols);$j++) {
						if (!empty($cols[$j])) {
							$id_filter=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS."` WHERE `id_category`=".$category_id." AND `name`='".trim($header[$j])."'");
							$option_temp=explode("|",$cols[$j]);
							$db->delete(TABLE_FILTERS_VALUES," `id_product`=".$id_product." AND `id_filter`=".$id_filter);
							foreach ($option_temp as $option_nw) {
								$id_option=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_OPTIONS."` WHERE `name`='".trim($option_nw)."' and `id_filter`=".$id_filter);
								$array_insert_f=array(
								'id_product'=>$id_product,
								'id_filter'=>$id_filter,
								'id_option'=>$id_option,
								);
								$db->insert(TABLE_FILTERS_VALUES,$array_insert_f);
							}
						}
					}
				}
			}
			$error=l('Filtrele produselor au fost updatate');
		}
		if (!empty($error)) {
		?>
		<script type="text/javascript">
		alert('<?=$error?>');
		</script>
		<?php
		}
	}
	function update_specifications(){
		global $db,$config,$gd;
		$category_id=$_POST['id_category'];
		if (empty($_FILES['products']['name'] )) {
			$error=l('Select a file');
		}elseif (empty($category_id)) {
			$error=l('Select a caregory');
		} else {
			$handle = fopen($_FILES['products']['tmp_name'], "rb");
			$i=0;
			while (!feof($handle)) {
				$i++;
				$line = fgets($handle);
				$cols=explode("$",$line);
				if ($i==1) $header=$cols;
				$id_product=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='".trim($cols[0])."'");
				echo $id_product;
				if ($i>1 && !empty($cols[0])&&!empty($id_product)) {
					for ($j=1;$j<=sizeof($cols);$j++) {
						$id_option=$db->fetch_one("SELECT `".TABLE_SPECIFICATIONS_OPTIONS."`.`id` FROM
							`".TABLE_SPECIFICATIONS_OPTIONS."`,`".TABLE_SPECIFICATIONS."` WHERE
							`".TABLE_SPECIFICATIONS_OPTIONS."`.`id_specification`=`".TABLE_SPECIFICATIONS."`.`id`
							AND `".TABLE_SPECIFICATIONS."`.`id_category`=".$category_id."
							AND `".TABLE_SPECIFICATIONS_OPTIONS."`.`name`='".trim($header[$j])."'
							");
						$array_insert_c=array(
						'id_product'=>$id_product,
						'id_option'=>$id_option,
						'value'=>$cols[$j],
						);
						//						print_a($array_insert_c);
						$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_insert_c);
					}
				}
			}
			$error=l('Specificatiile produselor au fost updatate');
		}
		if (!empty($error)) {
		?>
		<script type="text/javascript">
		alert('<?=$error?>');
		</script>
		<?php
		}
	}
	function update(){
		global $db;
		if (empty($_FILES['products']['name'] )) {
			$error=l('Select a file');
		} else {
			$handle = fopen($_FILES['products']['tmp_name'], "rb");
			$i=0;
			while (!feof($handle)) {
				$i++;
				$line = fgets($handle);
				$cols=explode("$",$line);
				if (trim($cols[4])=='') {
					if ($i>1 && !empty($cols[0])) {
						$id_currencie=$db->fetch_one("SELECT `id` FROM `".TABLE_CURRENCIES."` WHERE `code`='".trim($cols[4])."'");
						$array_update=array(
						'price'=>$cols[1],
						'stock'=>(int)$cols[2],
						'status'=>(int)$cols[3],
						'id_furnizor'=>(int)$cols[5],
						'currency'=>$id_currencie,
						);
						//$db->qupdate(TABLE_PRODUCTS,$array_update,'');
						$db->qupdate(TABLE_PRODUCTS,$array_update," `code`='".trim($cols[0])."'");
					}
				} else {
					if ($i>1 && !empty($cols[0])) {
						$id_currencie=$db->fetch_one("SELECT `id` FROM `".TABLE_CURRENCIES."` WHERE `code`='".trim($cols[4])."'");
						//print_a($cols);
						//die();
						$array_update=array(
						'price'=>$cols[1],
						'stock'=>(int)$cols[2],
						'status'=>(int)$cols[3],
						'currency'=>$id_currencie,
						'id_furnizor'=>(int)$cols[5],
						);
						//$db->qupdate(TABLE_PRODUCTS,$array_update,'');
						$db->qupdate(TABLE_PRODUCTS,$array_update," `code`='".trim($cols[0])."'");
					}
				}
			}
			$error=l('Produsele au fost updatate');
		}
		if (!empty($error)) {
		?>
		<script type="text/javascript">
		alert('<?=$error?>');
		</script>
		<?php
		}
	}
	function import(){
		global $db,$config,$gd;
		$category_id=$_POST['id_category'];
		if (empty($_FILES['products']['name'] )) {
			$error=l('Select a file');
		}elseif (empty($category_id)) {
			$error=l('Select a caregory');
		} else {
			$handle = fopen($_FILES['products']['tmp_name'], "rb");
			$c=0;
			//print_r($handle);
			while (!feof($handle)) {
				$line = return_utf8(fgets($handle));
				$cols=explode("$",$line);
				//print_r($line);
				//die();
				if ($c==0) $header=$cols;
				if ($c>0 && !empty($cols[0]) && !empty($cols[1])) {
					if (empty($cols[2])) $status=0; else $status=1;
					$exist=$db->fetch_one("SELECT `id` FROM `".TABLE_PRODUCTS."` WHERE `code`='".trim($cols[1])."'");
					//print_r($cols[1]);
					if (!empty($exist)) continue;
					$id_brand=$db->fetch_one("SELECT `id` FROM `".TABLE_BRANDS."` WHERE `name`='".trim($cols[4])."'");
					$id_currencie=$db->fetch_one("SELECT `id` FROM `".TABLE_CURRENCIES."` WHERE `code`='".trim($cols[3])."'");
					$array_insert=array(
					'code'=>$cols[1],
					'currency'=>$id_currencie,
					'id_brand'=>$id_brand,
					'id_category'=>$category_id,
					'price'=>$cols[2],
					'stock'=>$cols[5],
					'link'=>$cols[9],
					'pdf'=>$cols[10],
					'id_furnizor'=>(int)$cols[11],
					'status'=>$status,
					'date'=>time(),
					);
					$db->insert(TABLE_PRODUCTS,$array_insert);
					$id_product=$db->insert_id();
					$images=explode("|",$cols[8]);
					$j=0;

					//print_r($images);
					//die();
					foreach ($images as $image_link) {
						if (!empty($image_link)) {
							$j++;
							$image=SITEROOT."/products_images/tmp_image-".time().".".file_ext($image_link);
							$image_content=file_get_contents($image_link);
							$image_file= fopen($image, 'w');
							fwrite($image_file, $image_content);
							fclose($image_file);
							$new_name="products_images/".escapeIlegalChars($cols[1]."-".$j).".".file_ext($image);
							$gd->move_upload($image,SITEROOT."/".$new_name,'UM4MBR'.'400x400'.'sl-#FFFFFF');
							@unlink(SITEROOT."/products_images/".$image);
							if (file_exists(SITEROOT."/".$new_name)) {
								$array_insert_img=array('id_product'=>$id_product,'image'=>$new_name);
								$db->insert(TABLE_PRODUCTS_IMAGES,$array_insert_img);
							}
						}
					}
					$array_insert_lang=array(
					'id_main'=>$id_product,
					'name'=>$cols[0],
					'short_description'=>$cols[0],
					'description'=>$cols[6],
					'warranty'=>$cols[7],
					'header_title'=>$cols[0],
					'meta_keywords'=>$cols[0],
					'meta_description'=>$cols[0],
					'name_seo'=>escapeIlegalChars($cols[0]." ".$cols[1]," "),
					'lang'=>LANG,
					);
					$db->insert(TABLE_PRODUCTS.TABLE_EXTEND,$array_insert_lang);

					for ($i=11;$i<=sizeof($cols);$i++) {
						$header_temp=explode(":",$header[$i]);
						if (trim($header_temp[0])=='C') {
							$id_option=$db->fetch_one("SELECT `".TABLE_SPECIFICATIONS_OPTIONS."`.`id` FROM
							`".TABLE_SPECIFICATIONS_OPTIONS."`,`".TABLE_SPECIFICATIONS."` WHERE
							`".TABLE_SPECIFICATIONS_OPTIONS."`.`id_specification`=`".TABLE_SPECIFICATIONS."`.`id`
							AND `".TABLE_SPECIFICATIONS."`.`id_category`=".$category_id."
							AND `".TABLE_SPECIFICATIONS_OPTIONS."`.`name`='".trim($header_temp[1])."'
							");
							$array_insert_c=array(
							'id_product'=>$id_product,
							'id_option'=>$id_option,
							'value'=>$cols[$i],
							);
							$db->insert(TABLE_SPECIFICATIONS_VALUES,$array_insert_c);

						} elseif (trim($header_temp[0])=='F') {
							$id_filter=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS."` WHERE `id_category`=".$category_id." AND `name`='".trim($header_temp[1])."'");
							//							echo "SELECT `id` FROM `".TABLE_FILTERS."` WHERE `id_category`=".$category_id." AND `name`='".trim($header_temp[1])."'";
							//							die();
							if (!empty($cols[$i])) {
								$option_temp=explode("|",$cols[$i]);
								foreach ($option_temp as $option_nw) {
									$id_option=$db->fetch_one("SELECT `id` FROM `".TABLE_FILTERS_OPTIONS."` WHERE `name`='".trim($option_nw)."' and `id_filter`=".$id_filter);

									if (empty($id_option)) {

										$array_insert=array('name'=>trim($option_nw));
										$array_insert['id_filter']=$id_filter;
										$array_insert['order']=0;
										$db->insert(TABLE_FILTERS_OPTIONS,$array_insert);
										$id_option=$db->insert_id();
									}
									$array_insert_f=array(
									'id_product'=>$id_product,
									'id_filter'=>$id_filter,
									'id_option'=>$id_option,
									);
									$db->insert(TABLE_FILTERS_VALUES,$array_insert_f);
								}
							}
						}
					}
				}
				$c++;
			}
			$error=l('Produsele au fost importate');
		}
		if (!empty($error)) {
		?>
		<script type="text/javascript">
		alert('<?=$error?>');
		</script>
		<?php
		}
	}
	function export_update(){
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="Update Produse.csv"');
		echo l('Code').'$'
		.l('Price').'$'
		.l('Stock').'$'
		.l('Status').'$'
		.l('Moneda').'$'
		.l('Id Furnizor');
	}
	function export(){
		global $db;
		$category_id=$_POST['id_category'];
		$where=$_POST['export_products_yes'];
		$specifications_add_pref="";
		if ($where=='products') $is_products=true; else $is_products=false;
		$category_name=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."` WHERE `id`=".$category_id." AND `lang`='".LANG."'");
		if ($is_products) {
			$specifications_add_pref="C: ";
			$filters_value=array();
			if (!empty($_POST['filters'])) {
				foreach ($_POST['filters'] as $id_filter=>$value) {
					if (!empty($value)) {
						$filters_value[]="F: ".$db->fetch_one("SELECT  `name` FROM `".TABLE_FILTERS."` WHERE id=".$id_filter);
					}
				}
			}
		}
		$options_value=array();
		if (!empty($_POST['specifications'])) {
			foreach ($_POST['specifications'] as $id_specifications=>$value) {
				if (!empty($value)) {
					$options_value[]=$specifications_add_pref.$db->fetch_one("SELECT  `name` FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id=".$id_specifications);
				}
			}
		}

		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="Produse ' . $category_name . '.csv"');
		if ($is_products) {
			echo l('Name').'$'
			.l('Code').'$'
			.l('Price').'$'
			.l('Currency ').'$'
			.l('Brand').'$'
			.l('Stock').'$'
			.l('Description').'$'
			.l('Garantie').'$'
			.l('Images').'$'
			.l('Link').'$'
			.l('PDF').'$'
			.l('ID Furnizor').'$';
			if (!empty($options_value)) echo implode("$",$options_value).'$';
			if (!empty($filters_value)) echo implode("$",$filters_value);
		} else {
			echo l('Code').'$';
			if (!empty($options_value)) echo implode("$",$options_value);
		}
	}
	function export_all(){
		global $db;
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="All products.csv"');

		$products=$db->lfetch_all(TABLE_PRODUCTS," ",LANG);
		foreach ($products as $product) {
			$product['currency_code']=$db->fetch_one("SELECT `code` FROM  `".TABLE_CURRENCIES."` WHERE `id`=".$product['currency']);
			$product['main_id_categ']=$db->fetch_one("SELECT `id_parent` FROM  `".TABLE_CATEGORIES."` WHERE `id`=".$product['id_category']);
			$product['main_categ']=$db->fetch_one("SELECT `name` FROM  `".TABLE_CATEGORIES.TABLE_EXTEND."` WHERE `id_main`=".$product['main_id_categ']);
			$product['sub_categ']=$db->fetch_one("SELECT `name` FROM  `".TABLE_CATEGORIES."` WHERE `id`=".$product['id_category']);
			echo "\n"
			.$product['main_categ']."$"
			.$product['sub_categ']."$"
			.$product['code']."$"
			.$product['name']."$"
			.number_format($product['price'],2,'.','')."$"
			.$product['currency_code']."$"
			.$product['id_furnizor']."$"
			.$product['status'];
		}
	}
	function export_products(){
		global $db;
		if (!empty($_POST['export_type'])) $export_type=$_POST['export_type'];
		else $export_type='null';
		$category_id=$_POST['id_category'];
		$category_name=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."` WHERE `id`=".$category_id." AND `lang`='".LANG."'");

		$filters_value=array();
		if (!empty($_POST['filters'])) {
			foreach ($_POST['filters'] as $id_option=>$value) {
				if (!empty($value)) {
					$filters_value[]=$db->fetch_one("SELECT  `name` FROM `".TABLE_FILTERS."` WHERE id=".$id_option);
				}
			}
		}
		$specifications_value=array();
		if (!empty($_POST['specifications'])) {
			foreach ($_POST['specifications'] as $id_option=>$value) {
				if (!empty($value)) {
					$specifications_value[]=$db->fetch_one("SELECT  `name` FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id=".$id_option);
				}
			}
		}
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="Produse ' . $category_name . '.csv"');
		echo l('Code').'$'.implode("$",$filters_value).'$'.implode("$",$specifications_value);
		$products=$db->lfetch_all(TABLE_PRODUCTS," `id_category`=".$category_id,LANG);
		foreach ($products as $product) {
			$i++;
			if (empty($product['id_main'])) continue;
			$filtre_count=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_FILTERS_VALUES."` WHERE `id_product`={$product['id_main']}");
			if ($export_type['filtre_cu']==1 XOR $export_type['filtre_fara']==1) {
				if ($export_type['filtre_fara']==1 && $filtre_count>0 && $export_type['filtre_cu']!=1) continue;
				if ($export_type['filtre_cu']==1 && $filtre_count<1 && $export_type['filtre_fara']!=1) continue;
			}
			$filters_value=array();
			if (!empty($_POST['filters'])) {
				foreach ($_POST['filters'] as $id_filter=>$value) {
					if (!empty($value)) {
						$id_option=$db->fetch_one("SELECT  `id_option` FROM `".TABLE_FILTERS_VALUES."` WHERE `id_filter`=".$id_filter." AND `id_product`=".$product['id_main']);
						$filters_value[]=preg_replace("/[\n\r]/","",$db->fetch_one("SELECT  `name` FROM `".TABLE_FILTERS_OPTIONS."` WHERE id=".(int)$id_option));
					}
				}
			}
			$specificatii_count=$db->fetch_one("SELECT COUNT(`id`) FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_product`={$product['id_main']}");
			if ($export_type['specificatii_cu']==1 XOR $export_type['specificatii_fara']==1) {
				if ($export_type['specificatii_fara']==1 && $specificatii_count>0 && $export_type['specificatii_cu']!=1) continue;
				if ($export_type['specificatii_cu']==1 && $specificatii_count<1 && $export_type['specificatii_fara']!=1) continue;
			}
			$specifications_value=array();
			if (!empty($_POST['specifications'])) {
				foreach ($_POST['specifications'] as $id_specification=>$value) {
					if (!empty($value)) {
						//$id_option=$db->fetch_one("SELECT  `id_option` FROM `".TABLE_SPECIFICATIONS_VALUES."` WHERE `id_option`=".$id_specification." AND `id_product`=".$product['id_main']);
						$specifications_value[]=preg_replace("/[\n\r]/","",$db->fetch_one("SELECT `value` FROM ".TABLE_SPECIFICATIONS_VALUES." WHERE `id_option`=".(int)$id_specification." AND `id_product`=".$product['id_main']));
						//$specifications_value[]=preg_replace("/[\n\r]/","",$db->fetch_one("SELECT  `name` FROM `".TABLE_SPECIFICATIONS_OPTIONS."` WHERE id_specification=".(int)$id_specification));
					}
				}
			}
			echo "\n".$product['code']."$".implode("$",$filters_value)."$".implode("$",$specifications_value);
		}

	}
}
$module=new import3_module();

if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['action']=='import') $module->import();
elseif ($_GET['action']=='update') $module->update();
elseif ($_GET['action']=='export') $module->export();
elseif ($_GET['action']=='export_update') $module->export_update();
elseif ($_GET['action']=='export_products') $module->export_products();
elseif ($_GET['action']=='export_all') $module->export_all();
elseif ($_GET['action']=='update_specifications') $module->update_specifications();
elseif ($_GET['action']=='update_filtre') $module->update_filtre();
elseif ($_GET['load_specifications']==1) $module->load_specifications(fget('id_category'));
else {
	print_header();
	$module->new_a(1);
	print_footer();
}
?>