<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class radaos_module {
	var	$module='radaos';
	var $date='27-08-2009';
	var $table='xp_pricing';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function radaos_module() {

		$this->name=l('Discount / Comision');
		$this->title=l('Discount / Comision');
		$this->description=l('Discount / Comision');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('id_furnizor','status','id','id_brand','id_category')
		);

		$this->grid=array(
		0=>array('order'=>'id desc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),

		//'id_furnizor'=>array('name'=>l('Furnizor'),'width'=>140,'stype'=>'select','options'=>'show_select_furnizori'),
		//'id_brand'=>array('name'=>l('Producator'),'width'=>140,'stype'=>'select','options'=>'show_select_brands'),
		'id_category'=>array('name'=>l('Categorie'),'width'=>140,'stype'=>'select','options'=>'show_select_category'),
		'price'=>array('name'=>l('Valoare'),'width'=>70),
		'is_fixed'=>array('name'=>l('Tip discount'),'width'=>70),
		'is_adult'=>array('name'=>l('Adult / Copil'),'width'=>70),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		0=>'',

		//'header_title'=>array('type'=>'input','name'=>l('header title'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_keywords'=>array('type'=>'input','name'=>l('meta keywords'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'meta_description'=>array('type'=>'input','name'=>l('meta description'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//'name_seo'=>array('type'=>'input','name'=>l('Name seo'),'style'=>'width:92%;','info'=>'warning!!! warning!!! <br /> Doar pentru specialisti seo!!!!'),
		//1=>'show_furnizori',
		//2=>'show_brands',
		3=>'show_categories',
		'price'=>array('type'=>'input','name'=>l('Valoare'),'style'=>'width:20%;'),
		'is_fixed'=>array('type'=>'radio','options'=>array(1=>l('Fix'),0=>l('Procent')),'name'=>l('Tip')),
		'is_adult'=>array('type'=>'radio','options'=>array(1=>l('Adult'),0=>l('Copil')),'name'=>l('Adult / Copil')),
		'age_start'=>array('type'=>'input','name'=>l('Varsta de la'),'style'=>'width:20%;'),
		'age_end'=>array('type'=>'input','name'=>l('Varsta pana la'),'style'=>'width:20%;'),
		'order'=>array('type'=>'input','name'=>l('Ordinea'),'style'=>'width:20%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		//1=>l('Imagini'),
		//'image'=>array('type'=>'image','name'=>l('Imagine'),'folder'=>'radaos_images'),
		);
	}
	function json_list() {
		json_list($this);
	}
	function show_select_furnizori() {
		$_GET['from_grid']=1;
		$this->json_providers();
	}
	function show_select_brands() {
		$_GET['from_grid']=1;
		$this->json_brands();
	}
	function show_select_category() {
		$_GET['from_grid']=1;
		$this->json_categories();
	}
	function json_providers($id_provider=0) {
		global $db;
		if ($id_provider==0  && $_GET['from_grid']!=1) $id_provider=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_FURNIZORI."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_provider=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_provider)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list_row($row) {
		global $gd,$db;
		if ($row['id_category']==0) {
			$row['id_category']='<span style="color:red;">'.l('Toate categoriile').'</span>';
		} else {
			$row['id_category']=$db->fetch_one("SELECT `name` FROM `".TABLE_CATEGORIES."_data` WHERE `_id`=".$row['id_category']."");
		}
		if($row['is_fixed']==1) {
			$row['is_fixed']='Fix';
		} else {
			$row['is_fixed']='Procentual';
		}
		if($row['is_adult']==1) {
			$row['is_adult']='Adult';
		} else {
			$row['is_adult']='Copil';
		}
		
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}

	function show_furnizori($date) {
		global $db;
	?>
			<dl>
			<dt>
				<label for="id_furnizor"><?=l('Furnizor')?></label>
			</dt>
			<dd>
				<select name="id_furnizor" id="id_furnizor" style="width:200px">
				<?php $this->json_furnizori($date['id_furnizor']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function show_categories($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_category"><?=l('Categorie produse')?></label>
			</dt>
			<dd>
				<select name="id_category" id="id_category" style="width:200px">
				<?php $this->json_categories($date['id_category']) ?>
				</select>

			</dd>
		</dl>
		<?php
	}
	function json_categories($id_category=0,$id_parent=0,$deep=0) {
		global $db;
		$categories=$db->fetch_all("select * from `xp_categories`,`xp_categories_data` where `xp_categories`.`id`=`xp_categories_data`.`_id` and `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0 && $_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_category=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		foreach ($categories as $linie) {
		?>
			<option value="<?=$linie['_id']?>" <?=($linie['_id']==$id_category)?'selected="selected"':''?> >
			<?php
			for ($i=0;$i<$deep;$i++) echo " &nbsp; &nbsp; ";
		?>
			<?=$linie['name']?>
			</option>
			<?php
			$this->json_categories($id_category,$linie['_id'],$deep+1);
		}
	}
	function show_brands($date) {
		global $db;
	?>
		<dl>
			<dt>
				<label for="id_brand"><?=l('Brand')?></label>
			</dt>
			<dd>
				<select name="id_brand" id="id_brand" style="width:200px">
				<?php $this->json_brands($date['id_brand']) ?>
				</select>
			</dd>
		</dl>
		<?php
	}
	function json_brands($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `".TABLE_BRANDS."` ");
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_BRANDS."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	
	function json_furnizori($id_furnizor=0) {
		global $db;
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_furnizor=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `".TABLE_FURNIZORI."` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_furnizor)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new radaos_module();
if ($module_info) $this_module=$module;
elseif ($module_js) {
	$loadComplete='
			$("#gs_id_category").load("module/'.$module->module.'.php?json_categories=1&from_grid=1&id="+$("#gs_id_category").val());
			$("#gs_id_brand").load("module/'.$module->module.'.php?json_brands=1&from_grid=1&id="+$("#gs_id_brand").val());
			$("#gs_id_furnizor").load("module/'.$module->module.'.php?json_furnizori=1&from_grid=1&id="+$("#gs_id_furnizor").val());
		';

	set_grid($module,array('multiselect'=>true,'sortname'=>'id_furnizor','sortorder' => 'asc','loadComplete'=>$loadComplete));
}
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['json_categories']==1) $module->json_categories(fget('id'));
elseif ($_GET['json_brands']==1) $module->json_brands(fget('id'));
elseif ($_GET['json_furnizori']==1) $module->json_furnizori(fget('id'));
else {
	print_header();
	print_content($module);
	//$module->new_a();
	print_footer();
}
?>