<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class personalizeaza_var_o_v_module {
	var $module='personalizeaza_var_o_v';
	var $date='14-10-2012';
	var $table='prefix_products_variations_options_views';
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $folder='';
	var $title='';
	var $description='';
	var $_id_product=0;
	var $_id_variation=0;
	function personalizeaza_var_o_v_module() {

		$this->name=l('Vederi optiuni variatii produse personalizare');
		$this->title=l('Vederi optiuni variatii produse personalizare');
		$this->description=l('Vederi optiuni variatii produse personalizare');
		$this->folder='i/preview/.options-views';

		$this->type=array(
		'like'=>array(),
		'date'=>array(),
		'equal'=>array()
		);

		$this->grid=array(
		'_nr'=>true,
		'_cb'=>true,
		'id_product'=>array('name'=>l('Produs'),'width'=>200,'stype'=>'none'),
		'id_variation'=>array('name'=>l('Variatie'),'width'=>200,'stype'=>'none'),
		'id_variation_option'=>array('name'=>l('Optiune'),'width'=>200,'stype'=>'none'),
		'id_view'=>array('name'=>l('Vedere'),'width'=>200,'stype'=>'none'),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);

		$this->form=array(
		1=>'select_product',
		2=>'select_var',
		3=>'select_o',
		4=>'select_view',
		'img_filter'=>array('type'=>'image','name'=>l('Filtru vedere 3D'),'folder'=>$this->folder),
		'pos_filter'=>array('type'=>'radio','options'=>array(0=>l('Fara filtru'),1=>l('Peste vedere'),2=>l('Dedesubt')),'name'=>l('status')),
		);



	}
	function select_product($date) {
		?>
		<dl>
			<dt>
				<label for="id_product"><?=l('Tip produs')?></label>
			</dt>
			<dd>
				<select name="id_product" id="id_product" style="width:200px" onchange="$('#id_variation').load('module/<?=$this->module?>.php?action=json_var&id_product='+this.value);$('#id_view').load('module/<?=$this->module?>.php?action=json_view&id_product='+this.value);">
				<?php $this->json_select_product($date['id_product']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_product($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) {
			$id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products` ");
			$this->_id_product=$id_brand;
		}
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products` ORDER BY `name` ASC");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function select_o($date) {
		?>
		<dl>
			<dt>
				<label for="id_variation_option"><?=l('Optiune')?></label>
			</dt>
			<dd>
				<select name="id_variation_option" id="id_variation_option" style="width:200px">
				<?php $this->json_select_o($date['id_variation_option']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function select_view($date) {
		?>
		<dl>
			<dt>
				<label for="id_view"><?=l('Vedere')?></label>
			</dt>
			<dd>
				<select name="id_view" id="id_view" style="width:200px">
				<?php $this->json_select_view($date['id_view']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function select_var($date) {
		?>
		<dl>
			<dt>
				<label for="id_variation"><?=l('Variatie')?></label>
			</dt>
			<dd>
				<select name="id_variation" id="id_variation" style="width:200px"  onchange="$('#id_variation_option').load('module/<?=$this->module?>.php?action=json_o&id_product=<?=$this->_id_product?>&id_variation='+this.value);">
				<?php $this->json_select_var($date['id_variation']) ?>
				</select>
				
			</dd>
		</dl>
		<?php
	}
	function json_select_o($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products_variations_options` WHERE `id_variation`=".(int)$this->_id_variation);

		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products_variations_options` WHERE `id_variation`=".(int)$this->_id_variation." ORDER BY `name` ASC ");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_select_view($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) $id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products_views` WHERE `id_product`=".(int)$this->_id_product);
		/*if (empty($id_brand)) { ?>
		<option value="_" > <?=l('Selectati un produs')?> </option>
		<?php
		}*/
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products_views` WHERE `id_product`=".(int)$this->_id_product." ORDER BY `name` ASC ");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function json_select_var($id_brand=0) {
		global $db;
		if ($id_brand==0  && $_GET['from_grid']!=1) {
			$id_brand=$db->fetch_one("SELECT MAX(id) FROM `prefix_products_variations` WHERE `id_product`=".(int)$this->_id_product);
			$this->_id_variation=$id_brand;
		}
		/*if (empty($id_brand)) { ?>
		<option value="_" > <?=l('Selectati un produs')?> </option>
		<?php
		}*/
		if ($_GET['from_grid']==1) { ?>
			<option value="_" <?=($id_brand=='_')?'selected':''?> > <?=l('All')?> </option>
			<?php
		}
		$resursa=$db->query("SELECT * FROM `prefix_products_variations` WHERE `id_product`=".(int)$this->_id_product." ORDER BY `name` ASC ");
		while ($linie=$db->fetch($resursa)) {
		?>
			<option value="<?=$linie['id']?>" <?=($linie['id']==$id_brand)?'selected':''?> > <?=$linie['name']?> </option>
			<?php
		}
	}
	function css() {
		?>

		<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function json_list() {
		json_list($this,false);
	}
	function js() {
		set_grid($this,array('multiselect'=>'true'));
	}


	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function json_list_row($row) {
		global $db;
		$_info_o=$db->fetch("SELECT `name`,`id_variation` FROM `prefix_products_variations_options` WHERE `id`=".$row['id_variation_option']);
		$row['id_variation_option']=(string)$_info_o['name'];
		$_info=$db->fetch("SELECT `name`,`id_product` FROM `prefix_products_variations` WHERE `id`=".$_info_o['id_variation']);
		$row['id_variation']=(string)$_info['name'];
		$row['id_product']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products` WHERE `id`=".$_info['id_product']);
		$row['id_view']=(string)$db->fetch_one("SELECT `name` FROM `prefix_products_views` WHERE `id`=".$row['id_view']);
		return $row;
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) {
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
			$this->_id_variation=$db->fetch_one("SELECT `id_variation` FROM `prefix_products_variations_options` WHERE `id`=".$date_saved['id_variation_option']);
			$date_saved['id_variation']=$this->_id_variation;
			$this->_id_product=$db->fetch_one("SELECT `id_product` FROM `prefix_products_variations` WHERE `id`=".$this->_id_variation);
			$date_saved['id_product']=$this->_id_product;
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form_header(l('Adauga/modifica'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}

	function save($id){
		global $db;
		$data=$_POST;
		unset($data['id_product']);
		unset($data['id_variation']);
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			if (!empty($data['img_filter'])) {
				$data['name']="filtru ".$data['id_variation_option']." ".$data['id_view'];
				$_new_name=escapeIlegalChars($data['name']." ".$id,"-").".jpg";
				$new_file=SITEROOT."/static/".$this->folder."/".$_new_name;
				if (strpos($data['img_filter'],TMP_IMG)===false){
					$file=SITEROOT."/static/".$this->folder."/".$data['img_filter'];
				} else {

					$file=SITEROOT."/".$data['img_filter'];
				}
				rename($file,$new_file);
				$data['img_filter']=$_new_name;
				unset($data['name']);
			}
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta(l('a fost updatat'));
			} else {
				//$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta(l('a fost inserat'));
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new personalizeaza_var_o_v_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
elseif ($_GET['action']=='insert') $module->insert();
elseif ($_GET['action']=='save_import') $module->save_import();
elseif ($_GET['action']=='json_var') {
	$module->_id_product=fget('id_product');
	$module->json_select_var();
} elseif ($_GET['action']=='json_view') {
	$module->_id_product=fget('id_product');
	$module->json_select_view();
} elseif ($_GET['action']=='json_o') {
	$module->_id_product=fget('id_product');
	$module->_id_variation=fget('id_variation');
	$module->json_select_o();
} else {
	print_header();
	print_content($module);
	print_footer();
}
?>