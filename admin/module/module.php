<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class module_module {
	var	$module='module';
	var $date='27-08-2009';
	var $table=TABLE_ADMIN_MODULE;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function module_module() {

		$this->name=l('modules');
		$this->title=l('modules');
		$this->description=l('modules');

		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,min_3,max_40','text'=>'','info'=>'','auto'=>'title,description'),
		'title'=>array('type'=>'input','name'=>l('Titlu'),'valid'=>'empty,min_3,max_100'),
		'module'=>array('type'=>'input','name'=>l('Module'),'valid'=>'empty'),
		'description'=>array('type'=>'text','name'=>l('Descriere'),'valid'=>'empty,max_255'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function js() {
		?>
		var window_add_edit_name="<?=$this->title?>";
		<?php
	}
	function print_records() {
	?>
	<h2><?=$module_info['title']?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="nss_grid">
		<table cellspacing="0" cellpadding="0" border="0"  >
		<thead>
			<tr role="rowheader" class="ui-jqgrid-labels">
				<th><?=l('nume')?></th>
				<th style="width: 200px;"><?=l('Modul')?></th>
				<th style="width: 60px;"><?=l('vizibil')?></th>
				<th style="width: 140px;"><?=l('Actions')?></th>
			</tr>
		</thead>
		</table>
	</div>
	<div id="nss_list_record">
	<?php  $this->reg_get_categs((int)r('cid')); ?>
	</div>
	<br />
	<?php
	}

	function reg_get_categs($id_parent=0,$content="",$nr=0) {
		global $db;
		if ($nr>4) die();
		if ($nr==0) {
			$id_id='id_parent';
		} else $id_id='id';

		$categories=$db->fetch_all(" SELECT * FROM `".$this->table."` WHERE `".$id_id."`=".$id_parent." ORDER BY `order` ASC");
		//print_a($categories);
		//echo "<br/> `".$id_id."`=".$id_parent." ORDER BY `order` ASC";
		if (empty($categories)) {
			?>
			<ul style="list-style-type:none;	margin:0;		padding:0;" class="connectedSortable">
			
			<?php
			?>
				<li id="ele-0">
						<div style="padding:2px; ">
						<a href="?mod=<?=$this->module?>&cid=0" onclick="add_all_url['<?=$this->module?>']='&cid=0'; reload_list_record('<?=$this->module?>'); return false; ">
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l('Module')?></span>
						</a>
					</div>
				</li>
			<?=$content?>
			</ul>
			<?php
		} else {


			ob_start();
			$exist_p=fo("SELECT `id` FROM `".$this->table."` WHERE `id`=".(int)$categories[0]['id_parent']);

			?>
			<ul  style="list-style-type:none;	margin:0;	 padding:0; 
			 <?php if ($exist_p) { ?>			margin-left:20px; <?php } ?> " 
			<?php if ($nr==0)  { ?> class="list_sort" <?php } ?> >
			<?php
			$i=0;
			foreach ($categories as $category) {
				$i++;
				if ($category ['status'] == 1) {
					$category ['status'] = '<img class="ch_staus_img" src="images/tick.gif" width="16" height="16" alt="tick" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '" onclick="ajax_ch_status(\''.$this->module.'\',this);" />';
				} else {
					$category ['status'] = '<img class="ch_staus_img" src="images/cross.gif" width="16" height="16" alt="cross" style="cursor:pointer;"  id="id_line_' . $category ['id'] . '"  onclick="ajax_ch_status(\''.$this->module.'\',this);"  />';
				}
				$exist=fo("SELECT `id` FROM `".$this->table."` WHERE `id_parent`=".$category['id']);
			?>
					<li id="ele-<?=$category['id']?>">
						<div style="float:left;">
						<?php if ($nr==0 /*&& $i!=sizeof($categories)*/)  {
							?>
							<img src="images/icons/branch.gif" style="vertical-align:middle;" />
							<?php 
						} else { ?>
						<img src="images/icons/branch2.gif" style="vertical-align:middle;" />
						<?php  } if (!empty($exist)) { ?>
						<a href="?mod=<?=$this->module?>&cid=<?=$category['id']?>" onclick="add_all_url['<?=$this->module?>']='&cid=<?=$category['id']?>'; reload_list_record('<?=$this->module?>'); return false;">
						<?php } ?>
						<img src="images/icons/folder.gif" style="vertical-align:middle;" /> <span><?=l($category['name'])?></span>
						<?php if (!empty($exist)) { ?>
						</a>
						<?php } ?>
						</div>
						
								
						<div style="width: 140px;float:right; padding:2px; padding-top:3px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="do_delete(<?=$category['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 60px;float:right; padding:1px 2px; text-align:center;"><?=$category['status']?></div>
						<div style="width: 200px;float:right; padding:2px;  text-align:center;"><?=$category['module']?></div>
						<div class="clear"></div>
				</li>
				<?php
				$id_parent=$category['id_parent'];
			}
			echo $content;
			?>
			</ul>
		<?php
		$file_data = ob_get_contents();
		ob_end_clean();
		$this->reg_get_categs($id_parent,$file_data,$nr+1);
		}
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table,true);
		$ids=explode(",",fost('id'));
		function delete_recursive($id,$table) {
			if(!empty($id)) {
				$childs=$db->fetch_all("SELECT * FROM `".$table."` WHERE id_parent=".$id);
				if (empty($childs)) {
					$db->delete($table," id=".$id);
					$db->delete($table.TABLE_EXTEND," id_main=".$id);
					$db->delete(TABLE_ADMIN_MODULE_CONFIG,"`id_module`=".$id);
				} else {
					foreach ($childs as $child) {
						delete_recursive($child['id'],$table);
					}
				}
			} else return false;
		}
		foreach ($ids as $id) delete_recursive($id,$this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_selected=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Add Edit Module'));
		print_form($this->form,$this,$date_selected);
			?>
			<dl>
			<dt>
			<label for="id_parent_module"><?=l('Modul parinte')?></label>
			</dt>
			<dd>
			<select name="id_parent" id="id_parent">
			<option value="0">Niciunul (modul principal) </option>
			<?php	$resursa=$db->query("SELECT * FROM `".TABLE_ADMIN_MODULE."`  WHERE `id_parent`=0 ORDER BY `order` ASC");
			while ($linie=$db->fetch($resursa)) {
				?>
				<option value="<?=$linie['id']?>" <?=($linie['id']==$date_selected['id_parent'])?'selected':''?> ><?=$linie['name']?> (<?=$linie['module']?>)</option>
				<?php
			}
			?>
			</select>
			</dd>
			</dl>
			<?php
			print_form_footer();
			print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
			?>
			<script type="text/javascript">
			reload_list_record('<?=$this->module?>');
			</script>
			<?php
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function save_order() {
		global_save_order($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
}
$module=new module_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1)$module->reg_get_categs((int)r('cid'));
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>