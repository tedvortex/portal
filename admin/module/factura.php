<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class factura_module {
	var	$module='factura';
	var $date='07-11-2009';
	var $table=TABLE_INVOICES_SETTINGS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function factura_module() {
		
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume firma'),'style'=>'width:92%;'),
		'address'=>array('type'=>'input','name'=>l('Adresa'),'style'=>'width:92%;'),
		'zip_code'=>array('type'=>'input','name'=>l('Cod Postal'),'style'=>'width:92%;'),
		'city'=>array('type'=>'input','name'=>l('Oras'),'style'=>'width:92%;'),
		'state'=>array('type'=>'input','name'=>l('Judet'),'style'=>'width:92%;'),
		'country'=>array('type'=>'input','name'=>l('Tara'),'style'=>'width:92%;'),
		'nr_reg_com'=>array('type'=>'input','name'=>l('Registrul Comertului'),'style'=>'width:92%;'),
		'cui'=>array('type'=>'input','name'=>l('CIF'),'style'=>'width:92%;'),
		'bank'=>array('type'=>'input','name'=>l('Banca'),'style'=>'width:92%;'),
		'account'=>array('type'=>'input','name'=>l('Contul'),'style'=>'width:92%;'),
		'email'=>array('type'=>'input','name'=>l('Email'),'style'=>'width:92%;'),
		'web'=>array('type'=>'input','name'=>l('Site web'),'style'=>'width:92%;'),
		'phone'=>array('type'=>'input','name'=>l('Telefon'),'style'=>'width:92%;'),
		'fax'=>array('type'=>'input','name'=>l('Fax'),'style'=>'width:92%;'),
		'no_invoice'=>array('type'=>'input','name'=>l('Nr. Factura'),'style'=>'width:92%;'),
		);
	}
	function css() {}
	function js() {
		?>
		$(document).ready(function() {
			after_window_load('<?=$this->module?>');
		});
		<?php
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function new_a() {
		global  $db,$main_buttons;
		$date_saved=$db->fetch("SELECT * FROM `".$this->table."` LIMIT 1");
		?>
		<form action="?mod=<?=$this->module?>&amp;action=save" method="POST">
		<?php 
		print_form_header(l('Editeaza setari fatura'));
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save(){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,1);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			$db->qupdate($this->table,$data,'');
			print_alerta(l('a fost updatat'));
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new factura_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='save') $module->save();
else {
	print_header();
	?>
	<h2><?=$module->title?></h2>
	<?php
	$module->new_a();
	print_footer();
}
?>