<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class variations_module {
	var	$module='variations';
	var $date='27-08-2009';
	var $table=TABLE_VARIATIONS_NAME;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function variations_module() {
		
		$this->name=l('Variatii produse');
		$this->title=l('Variatii produse');
		$this->description=l('Variatii produse');
		
		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date'),
		'equal'=>array('default','status','id')
		);
		
		$this->grid=array(
		0=>array('order'=>'name asc'),
		'_nr'=>true,
		'_cb'=>true,
		'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'name'=>array('name'=>l('Nume variatie'),'width'=>300),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);
		
		$this->form=array(
		0=>'',
		'name'=>array('type'=>'input','name'=>l('Nume variatie'),'valid'=>'empty,unique,min_4,max_140','style'=>'width:92%;'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
	//	1=>l('optiuni')
		);
		
	}
	function install() {}
	function uninstall() {}
	function language() {}
	function css() {
		?>
		.drag_all_top span {
			cursor:move;
			font-size:14px;
			background-image:url('../../images/grid_move.gif');
		}
		.value_container div.opt_value{
			padding:2px;
			padding-left:4px;
			
		}
		img.rem_parent, img.add_parent {
			cursor:pointer;
		}
		<?php
	}
	function json_list_row($row) {
		global $gd;
		return $row;
	}


	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
		$ids=explode(",",fost('id'));
		foreach ($ids as $id) if(!empty($id)) {
			$ids_options=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE id_variation=".$id);
			foreach ($ids_options as $id_option) $db->delete(TABLE_VARIATIONS_VALUES,"`id_option`=".$id);
			$db->delete(TABLE_VARIATIONS_OPTIONS,"`id_variation`=".$id);
		}
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		
		<?php 
		print_form($this->form,$this,$date_saved);
		if (1==2) {
			
		
		?>
		<div class="drag_all_top">
		<?php
		if ($edit) $variations_options=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE `id_variation`=".$id." ORDER BY `id` ASC");
		if (empty($variations_options)) $variations_options=array(1);
		foreach ($variations_options as $linie) {
			?>
			<dl>
			<dt> <span>&nbsp;&nbsp;&nbsp;&nbsp;</span> 
				<input type="text" name="option[]"  value="<?=$linie['name']?>" style="width:70%" align="absmiddle" />
				<input type="hidden" name="value[]" value="=-=-=" /> 
				<img src="images/addicon.gif" align="absmiddle"  class="add_parent" /> 
				<img src="images/delicon.gif" align="absmiddle"  class="rem_parent"  /> 
			</dt>
			<dd class="value_container ui-helper-clearfix">		
			<?php
			if ($edit) $variations_values=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_VALUES."` WHERE `id_option`=".(int)$linie['id']." ORDER BY `id` ASC");
			if (empty($variations_values)) $variations_values=array(1);
			foreach ($variations_values as $linie_2) {
				?>
				<div class="opt_value"><span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<input type="text" name="value[]" style="width:60%;"  value="<?=$linie_2['name']?>" align="absmiddle"  /> 
					<img src="images/addicon.gif" align="absmiddle"  class="add" /> 
					<img src="images/delicon.gif" align="absmiddle"  class="rem"  /> 
				</div>
				<?php
			}
			?>	
			</dd>
			</dl>
			<?php
		}
		?>
		</div>
		<?php
		}
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<script type="text/javascript">
		load_w=function() {
			<?php if ($edit) { ?>
			//alerta('<?=l('Daca aceasta variatie de produs este definita la unele produse variatiile lor vor fi sterse Este indicat ca modificarile finale sa se faca inaintea setarii variatiilor la prosuse')?>');
			<?php } ?>
			$(".value_container,.drag_all_top").sortable({opacity: 0.7,revert: true,scroll: true ,handle:$(".drag_all_top span")}) ;
			$("img.add_parent").click(function(){
				$(this).parent().parent().clone(true).insertAfter($(this).parent().parent());
			});
			$("img.rem_parent").click(function(){
				if (($(this).parent().parent().parent().children().size())>1) $(this).parent().parent().remove();
			});
		}
		</script>
		<?php
	}
	function save($id){
		global $db;
		$errors=form_validation($_POST,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module);
			$array_insert=array('name'=>$_POST['name']);
			if (!empty($id)) {
				$ids_options=$db->fetch_all("SELECT * FROM `".TABLE_VARIATIONS_OPTIONS."` WHERE id_variation=".$id);
				foreach ($ids_options as $id_option) $db->delete(TABLE_VARIATIONS_VALUES,"`id_option`=".$id);
				$db->delete(TABLE_VARIATIONS_OPTIONS,"`id_variation`=".$id);
				$db->qupdate($this->table,$array_insert," id=".$id);
				//print_alerta('a fost updatat');
			} else {
				$array_insert['status']=1;
				$db->insert($this->table,$array_insert);
				$id=$db->insert_id();
				//print_alerta('a fost inserat');
			}
			if (is_array($_POST['value'])) {
				$i=0;
				foreach ($_POST['value'] as $key_id_value=>$value) {
					if ($value=='=-=-=') {
						$array_insert=array('name'=>$_POST['option'][$i]);
						$array_insert['id_variation']=$id;
						$db->insert(TABLE_VARIATIONS_OPTIONS,$array_insert);
						$id_option=$db->insert_id();
						$i++;
					} else {
						if (!empty($value)) {
							$array_insert=array('name'=>$value);
							$array_insert['id_option']=$id_option;
							$db->insert(TABLE_VARIATIONS_VALUES,$array_insert);
						}
					}

				}
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function json_list() {
		json_list($this);
	}
}
$module=new variations_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->json_list();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	print_footer();
}
?>