<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class auctions2_module {
	var	$module='auctions2';
	var $date='27-08-2009';
	var $table=TABLE_AUCTIONS;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function auctions2_module() {

		$this->name=l('Modifica licitatii in curs');
		$this->title=l('Modifica licitatii in curs');
		$this->description=l('Modifica licitatii in curs');

		$this->type=array(
		'like'=>array('name','id'),
		'date'=>array('date','end_date','start_date'),
		'equal'=>array('default','status','id')
		);

		$this->grid=array(
		0=>array('order'=>'date desc'),
		'_nr'=>true,
		'_cb'=>true,
		//'id'=>array('name'=>l('ID'),'width'=>40,'align'=>'center'),
		'image'=>array('name'=>l('Image'),'width'=>50,'align'=>'center'),
		'auction_name'=>array('name'=>l('Nume licitatie'),'width'=>150),
		'name'=>array('name'=>l('Nume produs'),'width'=>200),
		'last_bid'=>array('name'=>l('Ultimul licitator'),'width'=>100,'align'=>'center'),
		'start_date'=>array('name'=>l('Data de start'),'width'=>100,'align'=>'center'),
		'end_date'=>array('name'=>l('Data de final'),'width'=>100,'align'=>'center'),
		'price'=>array('name'=>l('Pret'),'width'=>50,'align'=>'center'),
		'status'=>array('name'=>l('status'),'width'=>80,'align'=>'center','stype'=>'select','editoptions'=>array('value'=>'_:'.l('All').';1:'.l('Activ').';0:'.l('Inactiv'))),
		'actions'=>array('name'=>l('Actions'),'width'=>130,'align'=>'center','sortable'=>false),
		);






			$this->form=array(
			0=>'',
			'auction_name'=>array('type'=>'input','name'=>l('Numele licitatie'),'valid'=>'empty,min_2,max_140','style'=>'width:200px'),
			//1=>'form_product_select',
			//'start_date'=>array('type'=>'date','name'=>l('Data de incepere'),'valid'=>'empty,min_2,max_140','style'=>'width:80px;','text'=>$text),
			//'time_add'=>array('type'=>'input','name'=>l('Durata licitatiei'),'valid'=>'empty,min_2,max_140','style'=>'width:80px;','text'=>' zz : hh : mm : ss'),
			//2=>'form_product_moneda',
			//'start_price'=>array('type'=>'input','name'=>l('Pret pornire'),'valid'=>'empty,min_2,max_140','style'=>'width:200px;'),
			'price'=>array('type'=>'input','name'=>l('Pret actual'),'valid'=>'empty,min_2,max_140','style'=>'width:200px;'),
			//'price_add'=>array('type'=>'input','name'=>l('Pret minim de adaugat'),'valid'=>'empty,min_2,max_140','style'=>'width:200px;'),
			'last_bid'=>array('type'=>'input','name'=>l('Ultimul licitator'),'style'=>'width:200px;'),
			//'re_reset'=>array('type'=>'input','name'=>l('Criteriu restart'),'valid'=>'empty','style'=>'width:200px;','text'=>'ex: 1000=20;1200=30'),
			//'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status'))
			);
	}
	function css() {}
	function json_list() {
		json_list($this,false);
	}
	function form_product_select($date_selected) {
		global $db;
		?>
			<dl>
			<dt>
			<label for="id_product_module"><?=l('Produs')?>:</label>
			</dt>
			<dd>
			<select name="id_product" id="id_product">
			<option value="0"> Selectati produsul</option>
			<?php

			$sql="SELECT * FROM `".TABLE_PRODUCTS."`,`".TABLE_PRODUCTS.TABLE_EXTEND."`
			WHERE `".TABLE_PRODUCTS."`.`id`=`".TABLE_PRODUCTS.TABLE_EXTEND."`.`id_main`
			AND `".TABLE_PRODUCTS.TABLE_EXTEND."`.`lang`='".LANG."' ".(!empty($sql_filters)?" AND ".implode(" AND ",$sql_filters):"")." ORDER BY `name` ASC LIMIT 100";
//			echo $sql;
			$products=$db->fetch_all($sql);

			foreach ($products as $linie) {
				?>
				<option value="<?=$linie['id_main']?>" <?=($linie['id_main']==$date_selected['id_product'])?'selected':''?> ><?=limit_string($linie['name'],60)?> </option>
				<?php
			}
			?>
			</select>
			</dd>
			</dl>
			<?php

	}
	function form_product_moneda(){
		global $db;
		?>
			<dl>
			<dt>
			<label for="id_product_module"><?=l('Moneda')?>:</label>
			</dt>
			<dd>
			<select name="id_moneda" id="id_moneda">
			<option value="0"> Selectati moneda</option>
				<?php

			$sql="SELECT * FROM `".TABLE_CURRENCIES."` WHERE `status`=1 ";
//			echo $sql;
			$products=$db->fetch_all($sql);

			foreach ($products as $linie) {
				?>
				<option value="<?=$linie['id']?>" <?=($linie['id']==$date_selected['id_product'])?'selected':''?> ><?=limit_string($linie['name'],60)?> </option>
				<?php
			}
			?>
			</select>
			</dd>
			</dl>
			<?php
	}
	function json_list_row($row) {
		global $gd,$db;
		//echo LANG;
		$row['name']=$db->fetch_one("SELECT `name` FROM `".TABLE_PRODUCTS.TABLE_EXTEND."` WHERE `lang`='".LANG."' AND  `id_main`=".$row['id_product']);
		$row['image']=(string)$db->fetch_one("SELECT `image` FROM `".TABLE_PRODUCTS."` WHERE  `id`=".$row['id_product']);
		$row['time_add']=implode(":",return_date($row['time_add']));
		//$row['reset_time']=implode(":",return_date($row['reset_time']));
		if ($row['end']==1) {
			$status_licitatie='Terminata';
		} else {

			if ($row['start_date']<time()) {
				$status_licitatie='Inceputa';
			} else $status_licitatie='Neinceputa';
		}
		$row['end']='<b>'.$status_licitatie.'</b>';
		if (!empty($row['image2']))	 $row['image2']='<img src="../'.$gd->url('resize',$row['image2'],'48x48s-#f6f1f6').'" alt="'.$row['name'].'" />';
		return $row;
	}
	function grid_edit(){
		if ($_POST['oper']=='del') global_delete($this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		if (!empty($id)) $date_saved=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php
		print_form($this->form,$this,$date_saved);
		print_form_footer();
		print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {

			close_window($this->module);


			//$start_date=explode('.',$_POST['start_date']);
			//$starOra=explode(':',$_POST['ora']);
			//$start_date=mktime((int)$_POST['ora'],(int)$_POST['minutul'],(int)$_POST['secunda'],$start_date[1],$start_date[0],$start_date[2]);

			//$data['time_add']=explode(':',$data['time_add']);
			//$data['reset_time']=explode(':',$data['reset_time']);

			//$data['price']=$data['start_price'];
			//$data['time_add']=(int)$data['time_add'][0]*3600*24+(int)$data['time_add'][1]*3600+(int)$data['time_add'][2]*60+(int)$data['time_add'][3];
			//$data['reset_time']=(int)$data['reset_time'][0]*3600*24+(int)$data['reset_time'][1]*3600+(int)$data['reset_time'][2]*60+(int)$data['reset_time'][3];

			//$startDate=explode('-',$data['start_date']);
			//$data['start_date']=mktime((int)$data['ora'],(int)$data['minutul'],(int)$data['secunda'],$startDate[1],$startDate[0],$startDate[2]);
			//$data['end_date']=$data['start_date']+$data['time_add'];

			unset($data['type_op'],$data['ora'],$data['minutul'],$data['secunda'],$data['prices'],$data['sec'],$data['order_2']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
				$id=mysql_insert_id();
			}
		} else {
			print_form_errors($errors,$this->form);
		}
	}
}
$module=new auctions2_module();
if ($module_info) $this_module=$module;
elseif ($module_js) set_grid($module,array('multiselect'=>true,'sortname'=>'start_date','sortorder' => 'desc'));
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) 	json_list($module);
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	print_content($module);
	//$module->new_a();
	print_footer();
}
?>