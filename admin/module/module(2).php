<?php
include_once(dirname(dirname(__FILE__))."/includes/init.php");
class module_module {
	var	$module='module';
	var $date='27-08-2009';
	var $table=TABLE_ADMIN_MODULE;
	var $grid=array();
	var $form=array();
	var $type=array();
	var $name='';
	var $title='';
	var $description='';
	function module_module() {
		
		$this->name=l('modules');
		$this->title=l('modules');
		$this->description=l('modules');
		
		$this->form=array(
		'name'=>array('type'=>'input','name'=>l('Nume'),'valid'=>'empty,min_3,max_40','text'=>'','info'=>'','auto'=>'title,description'),
		'title'=>array('type'=>'input','name'=>l('Titlu'),'valid'=>'empty,min_3,max_100'),
		'module'=>array('type'=>'input','name'=>l('Module'),'valid'=>'empty'),
		'description'=>array('type'=>'text','name'=>l('Descriere'),'valid'=>'empty,max_255'),
		'status'=>array('type'=>'radio','options'=>array(1=>l('activ'),0=>l('Inactiv')),'name'=>l('status')),
		);
	}
	function css() {}
	function install() {}
	function uninstall() {}
	function language() {}
	function js() {
		?>
		var window_add_edit_name="<?=$this->title?>";
		<?php
	}
	function print_records() {
	?>
	<h2><?=$this->title?></h2>
	<ul class="module_menu">
		<li class="new" onclick="menu_new_click('<?=$this->module?>');"><?=l('new')?></li>
		<li class="save" onclick="save_sort('<?=$this->module?>');"><?=l('save')?></li>
	</ul>
	<div class="ui-jqgrid ui-widget ui-widget-content" style="border-right:0 none;">
		<div class="ui-jqgrid-view">
			<div class="ui-jqgrid-hbox" style="padding-right:0;">
				<table cellspacing="0" cellpadding="0" border="0"  style="width:100%" class="ui-jqgrid-htable">
				<thead>
					<tr role="rowheader" class="ui-jqgrid-labels">
						<th class="ui-state-default ui-th-column" role="columnheader" >
							<div id="jqgh_rn" style="padding-top:3px;"> <?=l('nume')?> </div>
						</th>
						</th>
							<th class="ui-state-default ui-th-column" role="columnheader" style="width: 140px;">
							<div id="jqgh_rn" style="padding-top:3px;"> <?=l('Modul')?>  </div>
						</th>
							<th class="ui-state-default ui-th-column" role="columnheader" style="width: 140px;">
							<div id="jqgh_rn" style="padding-top:3px;"> <?=l('Actions')?>  </div>
						</th>
					</tr>
				</thead>
				</table>
			</div>
			<br clear="all" />
		</div>
	</div>	
	<div id="nss_list_record">
		<?php $this->reg_get_categs(); ?>
	</div>
	<br />
	<?php
	}
	function reg_get_categs($id_parent=0) {
		global $db;
		$module_s=$db->fetch_all(" SELECT * FROM `".$this->table."` WHERE `id_parent`=".$id_parent." ORDER BY `order` ASC");
		if ($id_parent==0) {
			?>
			<ul id="list-container" style="list-style-type:none;margin:0;padding:0;">
			<?php	
		} else  {
			?>
			<ul style="list-style-type:none;margin:0;padding:0;">
			<?php
		}
		foreach ($module_s as $module_nw) {
				?>
					<li class="li-accepted-class" id="ele-<?=$module_nw['id']?>">
					<div class="ui-widget-content jqgrow" style="margin-top:-1px;" >
						<div style="float:left;padding:4px 2px; padding-left:10px;">   <?=$module_nw['name']?> ( <?=$module_nw['title']?>) </div>
						<div style="width: 140px;float:right; padding:3px 2px; padding-top:5px;  text-align:center;" class="th_action">
							<a class="action edit" onclick="do_edit(<?=$module_nw['id']?>,'<?=$this->module?>')" ><?=l ( 'edit' ) ?></a>
							<a class="action delete" onclick="do_delete(<?=$module_nw['id']?>,'<?=$this->module?>')" ><?=l ( 'Delete' )?></a>
						</div>
						<div style="width: 140px;float:right; padding:2px 2px; text-align:center;"><?=$module_nw['module']?></div>
						<div class="clear"></div>
					</div>
				<?php
				$this->reg_get_categs($module_nw['id'],$this->module);
			?>
			</li>
			<?php
		}
		?>
		</ul>
		<?php
	}
	function grid_edit(){
		global $db;
		if ($_POST['oper']=='del') global_delete($this->table,true);
		$ids=explode(",",fost('id'));
		function delete_recursive($id,$table) {
			if(!empty($id)) {
				$childs=$db->fetch_all("SELECT * FROM `".$table."` WHERE id_parent=".$id);
				if (empty($childs)) {
					$db->delete($table," id=".$id);
					$db->delete($table.TABLE_EXTEND," id_main=".$id);
					$db->delete(TABLE_ADMIN_MODULE_CONFIG,"`id_module`=".$id);
				} else {
					foreach ($childs as $child) {
						delete_recursive($child['id'],$table);
					}
				}
			} else return false;
		}
		foreach ($ids as $id) delete_recursive($id,$this->table);
	}
	function new_a($id=0) {
		global  $db,$main_buttons;
		$edit=false;
		if (!empty($id)) {
			$edit=true;
			$date_selected=$db->fetch("SELECT * FROM `".$this->table."` WHERE id=".$id);
		}
		?>
		<form action="?mod=<?=$this->module?>&action=save&id=<?=$id?>" method="POST">
		<?php 
		print_form_header(l('Add Edit Module'));
		print_form($this->form,$this,$date_selected);
			?>
			<dl>
			<dt>
			<label for="id_parent_module"><?=l('Modul parinte')?></label>
			</dt>
			<dd>
			<select name="id_parent" id="id_parent">
			<option value="0">Niciunul (modul principal) </option>
			<?php	$resursa=$db->query("SELECT * FROM `".TABLE_ADMIN_MODULE."`  WHERE `id_parent`=0 ORDER BY `order` ASC");
			while ($linie=$db->fetch($resursa)) {
				?>
				<option value="<?=$linie['id']?>" <?=($linie['id']==$date_selected['id_parent'])?'selected':''?> ><?=$linie['name']?> (<?=$linie['module']?>)</option>
				<?php
			}
			?>
			</select>
			</dd>
			</dl>
			<?php
			print_form_footer();
			print_form_buttons($main_buttons);
		?>
		</form>
		<?php
	}
	function save($id){
		global $db;
		$data=$_POST;
		$errors=form_validation($data,$this->form,$this->table,$id);
		if(empty($errors)) {
			close_window($this->module,$id);
			unset($data['type_op']);
			if (!empty($id)) {
				$db->qupdate($this->table,$data," id=".$id);
				print_alerta('a fost updatat');
			} else {
				$data['date']=time();
				$db->insert($this->table,$data);
				print_alerta('a fost inserat');
			}
			?>
			<script type="text/javascript">
			reload_list_record('<?=$this->module?>');
			</script>
			<?php
		} else {
			print_form_errors($errors,$this->form);
		}
	}
	function save_order() {
		global_save_order($_POST,$this->table);
		print_alerta(l('A fost salvat'));
	}
}
$module=new module_module();
if ($module_info) $this_module=$module;
elseif ($module_js) $module->js();
elseif ($module_css) $module->css();
elseif ($_GET['json_list']==1) $module->reg_get_categs();
elseif ($_GET['grid_edit']==1) $module->grid_edit();
elseif ($_GET['save_order']==1) $module->save_order();
elseif ($_GET['action']=='new') $module->new_a(fget('id'));
elseif ($_GET['action']=='save') $module->save(fget('id'));
else {
	print_header();
	$module->print_records();
	print_footer();
}
?>