<?include(dirname(dirname(__FILE__)).'/admin/includes/init.php');

if( isset($_GET['id']) && is_numeric($_GET['id']) ){
	
	$id_order = $_GET['id'];

	$order = $db->fetch("
		select *
		from `xp_orders`
		where `id` = {$id_order} "
	);

	$order['cancellationPolicyOmega'] = unserialize($order['cancellationPolicyOmega']);

	// 	print_a($order);

	$continue = true;
	
	#options parameters for SoapClient() request
	$option = array(
		'compression' => 1,
		'trace' => 1,
		'connection_timeout' => 180
	);

	#try to connect with SoapClient
	try {
		$Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
	}
	catch (Exception $e) {
		$continue = false;
	}


	#if SoapClient() connection succeeded check the reservation to see the current status
	#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
	if( $continue ){
	
		$checkReservation = array(
			'PartnerCode' => 'mondialvoyages',
			'PlatformName' => 'mondialvoyages',
			'Username' => 'mondialvoyages',
			'Password' => 'gr$sd45Ds8vr',
			'Version' => 1,
			'ReservationReference' => $order['reservationReferenceOmega']
		);
		
		try {
			$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
		}
		catch (SoapFault $Exception) {
			$continue = false;
		}
	}

	#verify if CheckReservesion() method returned an error or status of reservation is not Requires confirmation
	if( $continue && (
		isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ||
		! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status)
		)
	){
		$continue = false;
	}
	else {

		$order_status = $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status;

		if( $order['statusOmega'] != $order_status ){
	
			$db->query("
				update `xp_orders`
				set `statusOmega` = '{$order_status}'
				where `id` = {$id_order} "
			);
		}
	}

	$cancellation_policy = false;
	
	if( $continue ){

		if( $order_status == 'Confirmed' ){
	
			if( $order['cancellationPolicyOmega'] ){
				foreach( $order['cancellationPolicyOmega'] as $c ){
					if( $c->Price > 0 ){
		
						$from_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $c->FromDate );
						$to_date = DateTime::createFromFormat( 'Y-m-d H:i:s', $c->ToDate );
		
						if( time() >= $from_date->format('U') && time() <= $to_date->format('U') ){
								
							$continue = false;
							$cancellation_policy = $c;
									
							break;
						}
					}		
				}
			}
		}
		elseif( in_array($order_status, array('Canceled', 'Pending cancelation')) ){
			$continue = false;
		}
	}

	if( $continue ){

		$cancelReservation = array(
			'PartnerCode' => 'mondialvoyages',
			'PlatformName' => 'mondialvoyages',
			'Username' => 'mondialvoyages',
			'Password' => 'gr$sd45Ds8vr',
			'Version' => 1,
			'ReservationReference' => $order['reservationReferenceOmega']
		);

		try {
			$cancelReservationResultOmega = $Client->CancelReservation($cancelReservation);
		}
		catch (SoapFault $Exception) {
			$continue = false;
		}

		$db->query("
			update `xp_orders_products`
			set `cancelReservationParametersOmega` = '".serialize($cancelReservation)."', `cancelReservationResultOmega` = '".(isset($cancelReservationResultOmega) ? serialize($cancelReservationResultOmega) : '')."'
			where `id_order` = {$id_order} "
		);

		if( isset($cancelReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){

			$db->query("
				update `xp_orders`
				set `errorMessageCancelReservationOmega` = '{$cancelReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage}'
				where `id` = {$id_order} "
			);
		}

		elseif( ! isset($cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->Status) ){
			
			$db->query("
				update `xp_orders`
				set `errorMessageCancelReservationOmega` = 'Clickandgo Error: Cancellation could not be performed. Omega did not return any error or status.'
				where `id` = {$id_order} "
			);
		}

		elseif( isset($cancelReservationResultOmega) ){
		
			$db->query("
				update `xp_orders`
				set `statusOmega` = '{$cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->Status}'
				where `id` = {$id_order} "
			);
				
			$db->query("
				update `xp_orders_products`
				set `canceledTotalOmega` = '{$cancelReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseNewVersion->ReservationResponseTotalAmount->Total}'
				where `id_order` = {$id_order} "
			);
		}
	}
	
}
die('Finalizat');