<?php
$adaos = 0;

if (! empty($array_adaos[$id_brand])) {
    if (! empty($array_adaos[$id_brand][0])) {
        $adaos = $array_adaos[$id_brand][0];
    }

    $id_categ = $product['id_category'];

    while ($id_categ) {
        if (! empty($array_adaos[$id_brand][$id_categ])) {
            $adaos = $array_adaos[$id_brand][$id_categ];
        }

        $id_categ = $db->fetchOne("
        		select `id_parent`
        		from `xp_categories`
        		where `id` = ?",
                array($id_categ));
    }

    if (! empty($array_adaos[$id_brand][$product['id_category']])) {
        $adaos = $array_adaos[$id_brand][$product['id_category']];
    }

    if ($adaos == 0) {
        $adaos ++;
    }
}

else {
    $adaos = 1;
}

$price = $price * 1.24 * $adaos;