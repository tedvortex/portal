<?php
$brand_name = $_producatori_name[$brand_xml];
$id_category = $categ_ext[$brand_xml];
$id_currency = $array_exportc[$currency];
$id_brand = $db->fetchOne("
        select `id`
        from `xp_brands`
        where `name` = ?",
        array($brand_name));

$product = $db->fetchRow("
        select sql_no_cache
                c.`id_category`,
                p.`id`, p.`price`, p.`id_currency`, p.`id_furnizor`,
                pd.`code`
        from
                `xp_products` p
            	inner join `xp_products_data` pd
            		on p.`id` = pd.`id_product`
            	inner join `xp_products_to_categories` c
                    on p.`id` = c.`id_product`
        where
                pd.`code` = ?",
        array($code));