<?php
$product_to_furnizor = array(
        'id_currency' => $id_currency,
        'price' => $price,
        'stock' => $stock,
        'date' => date('Y-m-d H:i:s')
);

$exist = $db->fetchOne("
                    select `id_product`
                    from `xp_products_to_furnizori`
                    where
                        `id_furnizor` = ? and
                        `code` = ?",
        array($id_furnizor, $code));

if ($exist) {
    $db->update(
            'xp_products_to_furnizori',
            $product_to_furnizor,
            array(
                    $db->quoteInto('`id_product` = ?', $exist),
                    $db->quoteInto('`id_furnizor` = ?', $id_furnizor)));

    write($code);
}

else {
    if ($product) {
        $db->insert(
                'xp_products_to_furnizori',
                array_merge(
                        $product_to_furnizor,
                        array(
                                'id_furnizor' => $id_furnizor,
                                'id_product' => $product['id'],
                                'status' => 1,
                                'code' => $code
                        )
                ));

        write($product['code']);
    }

    else {
        $insert = array(
                'id_brand' => $id_brand,
                'price' => $price,
                'id_currency' => $id_currency,
                'stock' => $stock,
                'status' => 0,
                'id_furnizor' => $id_furnizor,
                'date' => time()
        );

        $insert_extend = array(
                'code' => $code,
                'code_ean' => $code_ean,
                'name' => $name,
                'short_description' => strip_tags($description),
                'description' => $description,
                'warranty' => $warranty,
                'name_seo' => $name_seo
        );

        $insert_categ = array(
                'id_category' => $id_category
        );

        $db->insert('xp_products', $insert);

        $insert_extend['id_product'] = $insert_categ['id_product'] = $db->lastInsertId();

        $db->insert('xp_products_data', $insert_extend);

        $db->insert('xp_products_to_categories', $insert_categ);

        $db->insert(
                'xp_products_to_furnizori',
                array_merge(
                        $product_to_furnizor,
                        array(
                                'id_furnizor' => $id_furnizor,
                                'id_product' => $insert_extend['id_product'],
                                'status' => 1,
                                'code' => $code
                        )
                ));

        write($product['code'], 0);
    }
}