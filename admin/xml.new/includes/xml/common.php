<?php
function write ($code, $type = 1)
{
    global $log;

    $types = array(
            'insert',
            'update'
    );

    $log['products'][] = array(
            'type' => $types[$type],
            'code' => $code
    );
}

$time = date('Y-m-d H:i:s');

$log = array(
        'created' => $time,
        'feed' => $feed,
        'products' => array()
);

$_producatori = $array_adaos = array();

$currencies = $db->fetchAll("
		select *
		from `xp_currencies`
		where `status`=1");

foreach ($currencies as $c) {
    $array_exportc[$c['code']] = $c['id'];
    $array_exportp[$c['id']] = $c;
}

$x = xml2array(file_get_contents("feed/{$feed}.xml"));

foreach ($db->fetchAll("
		select `brand_name`,`name`,`id`
		from `xp_categories`
		where `id_parent` = ?",
        array($id_categorie)) as $v
) {
    $_producatori[] = $v['name'];
    $_producatori_name[$v['name']] = $v['brand_name'];
}

foreach ($_producatori as $v) {
    $categ_ext[$v] = $db->fetchOne("
		select `id`
		from `xp_categories`
		where
			`id_parent` = ? and
			`name` = ?",
            array($id_categorie, $v)
    );
}