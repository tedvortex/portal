<?php
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ro" xml:lang="ro">

<head>
	<title>tavi</title>
	<meta name="description" content=""/>
	<meta name="keywords" content=""/>
	<meta charset="UTF-8"/>

	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>

	<div id="chart">
		<h1>Grafic:</h1>

	<?php
	if(
		$file = file_get_contents( 'New Text Document.txt' )
	){
	?>
		<ul>
		<?php
		$chart = array();
		$max = array(
			'Lumina' => 0,
			'Apa' => 0,
			'Gaz' => 0,
		);

		foreach(
			preg_split( "%(\r?\n)%", $file ) as
			$key => $line
		){
			if(
				$key != 0
			){
				$chart[] = $line = preg_split( '%\s+%', $line );
				if(
					$line[0] > $max['Lumina']
				){
					$max['Lumina'] = $line[0];
				}
				if(
					$line[1] > $max['Apa']
				){
					$max['Apa'] = $line[1];
				}
				if(
					$line[2] > $max['Gaz']
				){
					$max['Gaz'] = $line[2];
				}
			}
		}

		foreach(
			$chart as
			$c
		){
			?>
			<li>
				<div title="curent: <?php echo $c[0]?> / max: <?php echo $max['Lumina']?>" class="chart lumina" style="height:<?php echo $c[0] / $max['Lumina'] * 100?>px"></div>
				<div title="curent: <?php echo $c[1]?> / max: <?php echo $max['Apa']?>" class="chart apa" style="height:<?php echo $c[1] / $max['Apa'] * 100?>px"></div>
				<div title="curent: <?php echo $c[2]?> / max: <?php echo $max['Gaz']?>" class="chart gaz" style="height:<?php echo $c[2] / $max['Gaz'] * 100?>px"></div>

				<span><?php echo $c[3]?></span>
			</li>
		<?php
		}
		?>
		</ul>
	<?php
	}
	?>
	</div>
</body>