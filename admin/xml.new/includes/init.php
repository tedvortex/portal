<?php
define('i', r . 'includes/');
define('init', i . '/init/');
define('t', r . '_template/');
define('c', r . '_content/');

set_include_path( /*get_include_path() . PATH_SEPARATOR .*/ i);

setlocale(LC_TIME, "ro_RO");

require_once (init . 'config.php');
require_once (init . 'session.php');
require_once (init . 'db.php');
require_once (init . 'template.php');
require_once (init . 'user.php');
require_once (init . 'xml.php');
require_once (init . 'excel.php');

require_once (i . 'functions.php');

$config->setReadOnly();