<?php
require_once ('Zend/Db.php');
require_once ('Zend/Db/Table.php');
require_once ('Zend/Registry.php');

$Zend_Config = new Zend_Config_Ini(r . 'config.ini', 'production');

$db = Zend_Db::factory($Zend_Config->database);

Zend_Db_Table_Abstract::setDefaultAdapter($db);