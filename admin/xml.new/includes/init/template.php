<?php
require_once(i.'sfTemplate/sfTemplateAutoloader.php');
sfTemplateAutoloader::register();

$loader = new sfTemplateLoaderFilesystem(t.'%name%.php');

$engine = new sfTemplateEngine($loader);