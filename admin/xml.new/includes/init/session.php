<?php
session_name($config->session->name);
session_save_path(r.$config->session->save_path);
session_set_cookie_params(
	$config->session->lifetime,
	$config->session->path,
	$config->session->domain,
	false
);

// session_start();

date_default_timezone_set('Europe/Bucharest');