<?php
/**
 * prefix + md5(string) +suffix
 *
 * @param string $string
 * @param string $prefix
 * @param string $suffix
 * @return md5
 */
function md7(
	$string,
	$prefix='licenta.',
	$suffix='.t4v1'
){
	return md5( $prefix . $string . $suffix );
}

/**
 * <pre> $string </pre>
 *
 * @param string $string
 */
function print_a(
	$string
) {
	echo "<pre class=\"code\"><code>";
	print_r($string);
	echo "</code></pre>";
}

function removeAccents(
	$str
){
	$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
	$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	return str_replace($a, $b, $str);
}

function url(
	$string,
	$lang = null,
	$replace = ' ',
	$regexp = null,
	$encode = false
){
	global $languages;

	$lang = ( $lang == null && !empty($_SESSION['lang']) ? $_SESSION['lang'] : $lang );

	$regexp = ( $regexp == null && isset($languages[$lang]['regexp']) ? $languages[$lang]['regexp'] : $regexp );

	$string = mb_strtolower( preg_replace("%[^[:alnum:]\p{L}{$regexp}]+%ui", $replace, trim(removeAccents($string))));
	$string = mb_ereg_replace( "{$replace}$" , '', $string );
	$string = mb_ereg_replace( "^{$replace}", '', $string );

	if(
		$encode
	){
		$string = rawurlencode( $string );
	}

	return $string;
}

function beautify(
	$string,
	$separator = 'em',
	$direction = 1
){
	$regexp = array( '^(\w+)', '(\w+)$', '^((?:\w+\s)+)', '((?:\s\w+)+)$' );

	return mb_ereg_replace( $regexp[$direction], "<{$separator}>\\1</{$separator}>", $string);
}

function query(
	$statement,
	$dbid
){
	$quid = trans_sql_open($statement, $dbid);

	return $quid;
}

function bulk_insert(
	$info = array(),
	$data = array(),
	$fk = true
){
	global $db;

	$insert = array();
	$table = ( $fk ? 'main' : 'extend' );
	$name = $info['name'] . ( ! $fk ? '_Data' : '' );

	$data['STATUS'] = 1;

	if(
		isset($info[$table])
	){
		foreach(
			$info[$table]['cols'] as
			$key
		){
			$key = mb_strtoupper( $key );

			if(
				isset( $data[ $key ] )
			){
				$insert[ $key ] = $data[ $key ];
			}
		}

		if(
			!empty( $insert )
		){
			$db->insert(
				'ev_' . mb_strtolower( $name ),
				$insert
			);

			if(
				$fk
			){
				if(
					isset( $data['NAME'] )
				){
					$data['HEADER_TITLE'] =
					$data['META_KEYWORDS'] =
					$data['META_DESCRIPTION'] =
					$data['NAME'];

					$data['NAME_SEO'] = url( $data['NAME'] );
					$data['ID_LANGUAGE'] = 7;
				}

				bulk_insert( $info, $data, false );
			}

			elseif(
				! $fk || !isset( $info[ $table . '_Data' ] )
			){
				#echo $db->lastInsertId() . ' ';
			}
		}
	}
}

function generate_info(
	$table
){
	$tableExtend = $table.'_Data';

	if(
		class_exists( $table )
	){
		$tbClass = new $table;

		$info = array(
			'name' => $table,
			'main' => $tbClass->info(),
		);

		if(
			class_exists( $tableExtend )
		){
			$tbClassExtend = new $tableExtend;
			$info['extend'] = $tbClassExtend->info();
		}

		return $info;
	}

	else{
		exit( 'error loading class <strong>' . $table . '</strong>' );
	}
}

function lower_keys ($array) {
	$arr = array();

	if (is_array($array)) {
		foreach ($array as $array_k => $array_v) {
			$arr[ mb_strtolower($array_k) ] = (string) $array_v;
		}
	}

	return $arr;
}

function loadModel ($modelName, $constructor) {
    global $$modelName;

    require_once r . 'script/model/' . mb_strtolower($modelName) . '.php';

    if (class_exists($modelName)) {
        $reflection_class = new ReflectionClass($modelName);
        $$modelName = $reflection_class->newInstanceArgs($constructor);

//         $$modelName = new $modelName($constructor);

        return true;
    }

    return false;
}

function haltMessage ($message, $value) {
    global $error;

    $error = true;

    print_a($message . ' - "' . $value . '"');
}

if (! function_exists('apache_request_headers')) {
    function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();

                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val)
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return ($arh);
    }
}

function generateCost ($priceModifiers, $value, $hotelEntity, $children) {
    if (isset($priceModifiers['Comision'][1]['price']) && $priceModifiers['Comision'][1]['price'] > 0) {
        $value += $hotelEntity['adults'] * $priceModifiers['Comision'][1]['price'];
    }

    if (is_array($children)) {
        foreach ($children as $c) {
            if ($c['age_end'] < 2) {
                if (isset($priceModifiers['Comision'][3]['price']) && $priceModifiers['Comision'][3]['price'] > 0) {
                    $value += $c['count'] * $priceModifiers['Comision'][3]['price'];
                }
            } else {
                if (isset($priceModifiers['Comision'][2]['price']) && $priceModifiers['Comision'][2]['price'] > 0) {
                    $value += $c['count'] * $priceModifiers['Comision'][2]['price'];
                }
            }
        }
    }

    if (isset($priceModifiers['Discount'][1]['price']) && $priceModifiers['Discount'][1]['price'] > 0) {
        $value -= $hotelEntity['adults'] * $priceModifiers['Discount'][1]['price'];
    }

    if (is_array($children)) {
        foreach ($children as $c) {
            if ($c['age_end'] < 2) {
                if (isset($priceModifiers['Discount'][3]['price']) && $priceModifiers['Discount'][3]['price'] > 0) {
                    $value -= $c['count'] * $priceModifiers['Discount'][3]['price'];
                }
            } else {
                if (isset($priceModifiers['Discount'][2]['price']) && $priceModifiers['Discount'][2]['price'] > 0) {
                    $value -= $c['count'] * $priceModifiers['Discount'][2]['price'];
                }
            }
        }
    }

    return sprintf('%.2f', $value);
}

function regenerateCost ($id_category, $cost) {
    global $pricingData;

    if (isset($pricingData[$id_category]) && is_array($pricingData[$id_category])) {
        $price = $cost['price'];

        foreach ($pricingData[$id_category] as $pricing) {
            if ($pricing['is_fixed']) {
                if ($pricing['is_adult']) {
                    $price += $cost['adults'] * $pricing['price'];
                }
            } else {
                $price += $pricing['price'] * $price;
            }
        }
    }
}