<?php
loadModel('Room', array($db));
loadModel('Board', array($db));
loadModel('Sejur_cost', array($db));

$inputFileType = 'Excel5';
$inputFileName = r . '../../' . (isset($_GET['file']) ? $_GET['file'] : /* 'feed/susesi.7.eur.xls' */ '');

if (preg_match('%([0-9]+)\.([^\.]+)\.xls$%is', $inputFileName, $files)) {
    $sejur = $db->fetchRow($db->select()
                              ->from('xp_sejur')
                              ->where('`id` = ?', $files[1]));

    if (! $sejur) {
        exit('Sejur inexistent');
    }

    $id_currency = $db->fetchOne($db->select()
                                    ->from(array('c' => 'xp_currencies'))
                                    ->where('c.`token` = ?', $files[2]));

    if (! $id_currency) {
        exit('Moneda inexistenta');
    }

    $objReader = PHPExcel_IOFactory::createReader($inputFileType);

    $objSheets = $objReader->listWorksheetNames($inputFileName);

    $objPHPExcel = $objReader->load($inputFileName);

    foreach ($objSheets as $sheet) {
        if (is_numeric($sheet)) {
            $objWorksheet = $objPHPExcel->getSheetByName($sheet);

            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();

            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            for ($row = 1; $row <= $highestRow; $row++) {
                for ($col = 0; $col <= $highestColumnIndex; $col++) {
                    $cellValue = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue());

                    if ($col == 0) {
                        $columnHeader = false;

                        if (preg_match('%^([\p{L}\p{N}\p{Z}]+)\(([\p{L}\p{N}\p{Z}\p{P}]+)\)(?:[^\#]+\#)([\p{L}\p{N}\p{Z}]+)\(([\p{L}\p{N}\p{Z}\p{P}]+)\)$%ism', $cellValue, $matches)) {
                            if ($matches) {
                                $columnHeader = true;

                                $_room = (object) array(
                                    'Code' => trim($matches[2]),
                                    'Name' => trim($matches[1]),
                                    'LanguageCode' => 'ro',
                                    'Status' => 'Active',
                                    'Owner' => 'Mondial',
                                );

                                $Room->insertOrUpdate($_room);

                                $_board = (object) array(
                                    'Code' => trim($matches[4]),
                                    'Name' => trim($matches[3]),
                                    'LanguageCode' => 'ro',
                                    'Status' => 'Active',
                                );

                                $Board->insertOrUpdate($_board);

                                $cost = array(
                                    'id_sejur' => $sejur['id'],
                                    'id_room_type' => $_room->Code,
                                    'id_board_type' => $_board->Code,
                                    'dates' => array(),
                                );
                            }
                        } else {
                            if (preg_match('%^(\p{N})\s*(?:Adu?lt?)%is', $cellValue, $adults)) {
                                if (isset($adults[1]) && is_numeric($adults[1])) {
                                    $childrenCount = 0;
                                    $_children = array();

                                    $select = $db->select()
                                                 ->from(array('he' => 'xp_hotels_entities'))
                                                 ->where('he.`id_hotel` = ?', $sejur['id_hotel'])
                                                 ->where('he.`adults` = ?', $adults[1]);

                                    $params = array();

                                    if (preg_match_all('%\+(\p{N})\s*(?:Ch(?:ild)?d(?:ren)?)\(([\p{N}\.]+)\-([\p{N}\.]+)\)%is', $cellValue, $children)) {
                                        foreach ($children[1] as $key => $child) {
                                            $childrenCount += $children[1][$key];

                                            $select->joinInner(array('hec' . $key => 'xp_hotels_entities_children'),
                                                               'hec' . $key . '.`id_hotel_entity` = he.`id`
                                                                and hec' . $key . '.`count` = :hec' . $key . 'count
                                                                and hec' . $key . '.`age_start` = :hec' . $key . 'age_start
                                                                and hec' . $key . '.`age_end` = :hec' . $key . 'age_end');

                                            $params['hec' . $key . 'count'] = $children[1][$key];
                                            $params['hec' . $key . 'age_start'] = $children[2][$key];
                                            $params['hec' . $key . 'age_end'] = $children[3][$key];

                                            $_children[$key] = array(
                                                'count' => $children[1][$key],
                                                'age_start' => $children[2][$key],
                                                'age_end' => $children[3][$key],
                                                'code' => $children[1][$key] . ' Chd(' . $children[2][$key] . '-' . $children[3][$key] . ')',
                                            );
                                        }
                                    }

                                    $select->where('he.`children` = ?', $childrenCount);

                                    $id_hotel_entity = $db->fetchOne($select, $params);

                                    if (! $id_hotel_entity) {
                                        $_entity = array(
                                            'id_hotel' => $sejur['id_hotel'],
                                            'status' => 1,
                                            'adults' => $adults[1],
                                            'children' => $childrenCount,
                                            'code' => $cellValue
                                        );

                                        $db->insert('xp_hotels_entities', $_entity);

                                        $id_hotel_entity = $db->lastInsertId();

                                        if (is_array($_children) && $id_hotel_entity) {
                                            foreach ($_children as $c) {
                                                $c['id_hotel_entity'] = $id_hotel_entity;

                                                $db->insert('xp_hotels_entities_children', $c);
                                            }
                                        }
                                    }

                                    $cost['id_hotel_entity'] = $id_hotel_entity;
                                }
                            }
                        }
                    } else {
                        if ($columnHeader) {
                            if (preg_match_all('%^([0-9]{1,2})\.([0-9]{1,2})\s*\-\s*([0-9]{1,2})\.([0-9]{1,2})$%is', $cellValue, $dates)) {
                                foreach ($dates[0] as $key => $date) {
                                    if (isset($dates[1][$key]) && isset($dates[2][$key]) && isset($dates[3][$key]) && isset($dates[4][$key])) {
                                        $cost['dates'][$col]['start'] = date('Y-') . sprintf('%02d', $dates[2][$key]) . '-' . sprintf('%02d', $dates[1][$key]);
                                        $cost['dates'][$col]['end'] = date('Y-') . sprintf('%02d', $dates[4][$key]) . '-' . sprintf('%02d', $dates[3][$key]);
                                    }
                                }
                            }
                        } else {
                            if (isset($cost['dates'][$col])) {
                                $_sejur_cost = array(
                                    'id_sejur' => $cost['id_sejur'],
                                    'id_room_type' => $cost['id_room_type'],
                                    'id_board_type' => $cost['id_board_type'],
                                    'id_hotel_entity' => $cost['id_hotel_entity'],
                                    'id_currency' => $id_currency,
                                    'id_confirmation_type' => 1,
                                    'date_start' => $cost['dates'][$col]['start'],
                                    'date_start' => $cost['dates'][$col]['end'],
                                    'price' => sprintf('%.2f', $cellValue),
                                    'nights' => $sheet,
                                );

                                $Sejur_cost->insertOrUpdate($_sejur_cost);
                            }
                        }
                    }
                }
            }
        }
    }
}