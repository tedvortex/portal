<?php
try {
    $result = $Client->GetLanguages($parameters);
} catch (SoapFault $Exception) {
    print_a($Exception);

    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
}

$table = new Zend_Db_Table('xp_languages');

if ($result) {
    if (is_array($result->GetLanguagesResponseLanguages->GetLanguagesResponseLanguage)) {
        foreach ($result->GetLanguagesResponseLanguages->GetLanguagesResponseLanguage as $p) {
            $row = $table->fetchRow($table->select()
                                          ->where('token = ?', mb_strtolower($p->Code)));

            if (! $row) {
                $data = array(
                    'default' => 0,
                    'status' => 1,
                    'token' => mb_strtolower($p->Code),
                    'name' => $p->EnglishName,
                    'original_name' => $p->Name,
                    'locale' => mb_strtolower($p->Code) . '_' . mb_strtoupper($p->Code) . '.UTF-8'
                );

                $table->insert($data);
            }
        }
    }
} else {
    print_a('Empty result set');
}