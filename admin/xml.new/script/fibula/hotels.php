<?php
require_once 'hotelstatic.php';

$file = file_get_contents(r . 'feed/hotels.xml');

$xml = xml2array($file, 1, 'hotel');

$countryData22 =  $db->select()
                    ->from(array('cd' => 'fibula_countries_data'),
                            array('name', 'id' => '_id'))
                    ->joinInner(array('c' => 'fibula_countries'),
                                '`c`.`id` = `cd`.`_id`',
                                array('id_parent'));
                              
$countryData= array();

foreach ($db->fetchAll($countryData22) as $c3) {
		$countryData[mb_strtoupper($c3['name'])]=$c3;
}



//print_a($priceModifiers);
////
//die();


if (isset($xml['hotels']['hotel'])) {
    $updateDate = new DateTime();
    $updateDate->setTime(0, 0, 0)->modify('+1 day');

    loadModel('Hotel2', array($db));
    loadModel('Sejur_cost', array($db));

    $table = new Zend_Db_Table('xp_sejur');
    $tableData = new Zend_Db_Table('xp_sejur_data');
    $tableCost = new Zend_Db_Table('xp_sejur_cost');
    $tableTransport = new Zend_Db_Table('xp_sejur_transport');
    $tableImages = new Zend_Db_Table('fibula_hotels_images');
    $tableEntities = new Zend_Db_Table('fibula_hotels_entities');
    $tableEntitiesChildren = new Zend_Db_Table('fibula_hotels_entities_children');

    $rows = 0;

    foreach ($xml['hotels']['hotel'] as $hotel) {
        dif('setup ' . $hotel['attr']['hotelcode']);

// comment Costin
       if ($hotel['attr']['hotelcode']=='VOYBOD') {
        $hotelTime = microtime(true);

        $selectSejur = $table->select()
                             ->where('`code` = ?', 'fibula-' . $hotel['attr']['hotelcode']);

        $exist = $db->fetchRow($selectSejur);

        if (isset($_GET['hotel'])) {
            if ($hotel['attr']['hotelcode'] != $_GET['hotel']) {
                continue;
            }
        } else {
            if ($exist) {
                $dateRow = DateTime::createFromFormat('Y-m-d H:i:s', $exist['lastSync']);

                if ($updateDate->format('U') <= $dateRow->format('U')) {
                    dif("Hotel {$hotel['attr']['hotelcode']} a fost deja actualizat azi");

                    continue;
                }
            }
        }

        $_hotel = array_merge($hotel['attr'], array('description' => $hotel['description']['value']));

        $_hotel['id_city'] = null;

        if ($_hotel['description'] == 'N/A') {
            $_hotel['description'] = '';
        }
        
        
	//       print_a($_hotel);
//        die();
//      

        if (isset($cityData[$_hotel['location']])) {
            $_hotel['id_city'] = $cityData[$_hotel['location']]['id'];
            $_hotel['id_region'] = $cityData[$_hotel['location']]['id_country'];
        } else {
        	$_hotel['location']=trim($_hotel['location']);
        	print_a($_hotel['location']);
            if (isset($countryData[$_hotel['location']])) {
                $_hotel['id_region'] = $countryData[$_hotel['location']]['id'];
            } else {
                dif("Regiunea {$_hotel['location']} nu exista in baza de date");

                continue;
            }
        }

        if (isset($cityData[$_hotel['airport']])) {
            $_hotel['id_airport'] = $cityData[$_hotel['airport']]['id'];
        } else {
            if (isset($countryData[$_hotel['airport']])) {
                $_hotel['id_airport'] = $countryData[$_hotel['airport']]['id'];
            } else {
                dif("Aeroportul {$_hotel['airport']} nu exista in baza de date");

                continue;
            }
        }

        if (isset($countryId[$_hotel['id_region']])) {
            $_hotel['id_country'] = $countryId[$_hotel['id_region']]['id_parent'];
        } else {
            dif("Tara atribuita acestei regiuni [{$_hotel['location']}] nu exista in baza de date");

            continue;
        }
        
       // print_a($_hotel);

        $id = $Hotel2->insertOrUpdate((object)$_hotel);

        if (isset($hotel['images']['image']) && is_array($hotel['images']['image'])) {
            foreach ($hotel['images']['image'] as $k => $image) {
                $image = $image['attr']['url'];

                if (preg_match('%([a-z]+___([0-9]+)\.[a-z]{3,4})$%is', $image, $matches)) {
                    $file = r . '../../static/i/hotel-fibula/' . $matches[1];

                    if (! is_file($file)) {
                        $page = get_web_page($image);

                        if ($page['errno'] == 0 && $page['http_code'] == '200') {
                            file_put_contents($file, $page['content']);

                            $data = array(
                                'id_hotel' => $id,
                                'image' => $matches[1],
                                'order' => ($hotel['attr']['defaultimage'] == $matches[2] ? -1 : $k)
                            );

                            if (! $tableImages->fetchRow($tableImages->select()
                                              ->where('id_hotel = ?', $id)
                                              ->where('image = ?', $matches[1])))
                            {
                                $tableImages->insert($data);
                            }
                        }
                    }
                }
            }
        }


		
        $data = array(
            'id_country' => $_hotel['id_country'],
            'id_region' => $_hotel['id_region'],
            'id_city' => $_hotel['id_city'],
            'id_airport' => $_hotel['id_airport'],
            'id_category' => 1,
            'id_hotel' => $id,
            'id_currency' => 3,
            'price' => $_hotel['bestprice'],
            'stock' => 0,
            'status' => 1,
            'order' => 0,
            'code' => 'fibula-' . $_hotel['hotelcode'],
            'is_promo' => 0,
            'id_furnizor' => 42,
        );

        $needed = array(
            'code',
        );

        $select = $table->select();

        foreach ($needed as $v) {
            if (isset($data[$v])) {
                $select->where('`' . $v . '` = ?', $data[$v]);
            }
        }

        $row = $db->fetchRow($select);

        if (! $row) {
            $row['id'] = $table->insert($data);
        }

        $dataExtend = array(
            '_id' => $row['id'],
            'lang' => 'ro',
            'name' => $_hotel['hotelname'],
            'header_title' => $_hotel['hotelname'],
            'meta_keywords' => $_hotel['hotelname'],
            'meta_description' => $_hotel['hotelname'],
            'name_seo' => url($_hotel['hotelname']),
        );

        $select = $tableData->select()
                            ->where('`_id` = ?', $dataExtend['_id'])
                            ->where('`lang` = ?', $dataExtend['lang']);

        $rowData = $db->fetchOne($select);

        if (! $rowData) {
            $tableData->insert($dataExtend);
        }

        $db->beginTransaction();
        $db->query("set unique_checks = 0");
        $db->query("set foreign_key_checks = 0");

        $urls = array(
		  // trebuie pus primul pt ca rezolve problema cand nu are copii
        	   'http://resurse.fibula.ro/parteneri/xmlprices.php?chdno=99&hotelcode='	. $hotel['attr']['hotelcode'],
            'http://resurse.fibula.ro/parteneri/xmlprices.php?hotelcode=' 				. $hotel['attr']['hotelcode'],
        );

        dif('setup end ' . $hotel['attr']['hotelcode']);

        foreach ($urls as $url) {
            $prices = get_web_page($url);
            
           // print_a($prices);

            $cost = xml2array($prices['content'], 1, 'price');
            
            //print_a($cost);
			$bestprice=$cost['hotels']['hotel']['attr']['bestprice'];
			
			$priceModifiers['Comision'][1]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_adult'");
			$priceModifiers['Comision'][2]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_child'");
			$priceModifiers['Comision'][3]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_infant'");
			
			$priceModifiers['Discount'][1]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_adult'");
			$priceModifiers['Discount'][2]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_child'");
			$priceModifiers['Discount'][3]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_infant'");
					
			$_children20=array();
			$hotel_entity20['adults']=1;
			$bestprice = generateCost($priceModifiers, sprintf('%.2f', $bestprice), $hotel_entity20, $_children20);
            dif('download end price');

            $date_start_sejur = $date_end_sejur = null;
            $max_stay = $duration = 0;
            $date_start = $date_end = new DateTime();
            $firstWalk = true;

            if (isset($cost['hotels']['hotel']['prices']['price']) && is_array($cost['hotels']['hotel']['prices']['price'])) {
                $insertTransport = array();

                foreach ($cost['hotels']['hotel']['prices']['price'] as $c) {
                    $c = $c['attr'];
                    $c['depAirport'] = mb_strtolower($c['depAirport']);

                    $time = microtime(true);

                    if (isset($boardTypeId[$c['BoardCode']])) {
                        $board = $boardTypeId[$c['BoardCode']];
                    } else {
                        if (isset($boardTypeData[$c['board']])) {
                            $board = $boardTypeData[$c['board']];
                        } else {
                            dif("Camera {$c['board']} [{$c['BoardCode']}] nu exista in baza de date");

                            continue;
                        }
                    }

                    if (isset($roomTypeId[$c['RoomCode']])) {
                        $room = $roomTypeId[$c['RoomCode']];
                    } else {
                        if (isset($roomTypeData[$c['room']])) {
                            $room = $roomTypeData[$c['room']];
                        } else {
                            dif("Camera {$c['room']} [{$c['RoomCode']}] nu exista in baza de date");

                            continue;
                        }
                    }

                    if (! isset($airportCityData[$c['depAirport']])) {
                        dif("Aeroportul {$c['depAirport']} nu exista in baza de date");

                        continue;
                    }

                    $time = microtime(true);

                    // de aduagat cache static aici candva, in viitor
                    $select = $tableTransport->select()
                                             ->where('`id_transport` = ?', $transportData['air']['id'])
                                             ->where('`id_city` = ?', $airportCityData[mb_strtolower($c['depAirport'])]['id']);

                    $sejur_transport = $db->fetchRow($select);

                    if (! $sejur_transport) {
                        $sejur_transport_data = array(
                            'id_transport' => $transportData['air']['id'],
                            'id_city' => $airportCityData[mb_strtolower($c['depAirport'])]['id'],
                            'name' => $transportData['air']['name'] . ' - ' . $c['depAirport'],
                        );

                        try {
                            $sejur_transport['id'] = $tableTransport->insert($sejur_transport_data);
                        } catch (Exception $e) {
                            $db->rollBack();

                            dif($e->getMessage());
                        }
                    }

                    $hotel_entity = array(
                        'id' => (isset($entityData[$id][$c['adno']][$c['chdno']]) ? array_search($c['accommodation'], $entityData[$id][$c['adno']][$c['chdno']], true) : false)
                    );

                    $_children = array();
                    
                    if ($hotel_entity['id'] === false) {
                        $hotel_entity_data = array(
                            'id_hotel' => $id,
                            'status' => 1,
                            'adults' => $c['adno'],
                            'children' => $c['chdno'],
                            'code' => $c['accommodation'],
                        );

                        $hotel_entity['id'] = $tableEntities->insert($hotel_entity_data);

                        $entityData[$id][$c['adno']][$c['chdno']][$hotel_entity['id']] = $hotel_entity_data['code'];

                        if (preg_match_all('%\+(\p{N})\s*(?:Ch(?:ild)?d(?:ren)?)\(([\p{N}\.]+)\-([\p{N}\.]+)\)%is', $c['accommodation'], $children)) {
                            foreach ($children[1] as $key => $child) {
                                $_children[$key] = array(
                                    'count' => $children[1][$key],
                                    'age_start' => $children[2][$key],
                                    'age_end' => $children[3][$key],
                                    'code' => $children[1][$key] . ' Chd(' . $children[2][$key] . '-' . $children[3][$key] . ')',
                                );
                            }
                            
                            

                            if (is_array($_children) && $hotel_entity) {
                                foreach ($_children as $child) {
                                    $child['id_hotel_entity'] = $hotel_entity['id'];
                                    
									$tableEntitiesChildren->insert($child);
									
                                   /* for ($_i = 0; $_i < $child['count']; $_i++) {
                                        $_c = $child;

                                        $_c['count'] = 1;

                                        
                                    }*/
                                }
                            }
                        }

                        $db->commit();
                        $db->beginTransaction();
                    }
					$hotel_entity['adults']=$c['adno'];
					$_children2 = array();
					if (isset($c['adno'])&&$c['adno']>0) {
						
						 if (preg_match_all('%\+(\p{N})\s*(?:Ch(?:ild)?d(?:ren)?)\(([\p{N}\.]+)\-([\p{N}\.]+)\)%is', $c['accommodation'], $children3)) {
                            foreach ($children3[1] as $key => $child) {
                                $_children2[$key] = array(
                                    'count' => $children3[1][$key],
                                    'age_start' => $children3[2][$key],
                                    'age_end' => $children3[3][$key],
                                    'code' => $children3[1][$key] . ' Chd(' . $children3[2][$key] . '-' . $children3[3][$key] . ')',
                                );
                            }

                           
                        }
						
					}
					
					$priceModifiers['Comision'][1]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_adult'");
					$priceModifiers['Comision'][2]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_child'");
					$priceModifiers['Comision'][3]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='comision_sejur_infant'");
					
					$priceModifiers['Discount'][1]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_adult'");
					$priceModifiers['Discount'][2]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_child'");
					$priceModifiers['Discount'][3]['price']=$db->fetchOne("SELECT `value` FROM `config` where `key`='dicount_sejur_infant'");
					
										
					//print_a($_children2);
//                 dif('get entity values done for ukey ' . $c['ukey'], $time);
                    $c['price'] = generateCost($priceModifiers, sprintf('%.2f', $c['price']), $hotel_entity, $_children2);

//
//                    print_a($hotel_entity);
//                    print_a($_children2);
//                    print_a($c['price']);
                    
                    $_cost = array(
                        'id_sejur' => $row['id'],
                        'id_room_type' => $room['id'],
                        'id_board_type' => $board['id'],
                        'id_hotel_entity' => $hotel_entity['id'],
                        'id_sejur_transport' => $sejur_transport['id'],
                        'id_confirmation_type' => 2,
                        'max_adults' => 0,
                        'date_start' => $c['checkin'],
                        'availability_start' => $c['cachedate'],
                        'availability_end' => $c['SaleEndDate'],
                        'is_standard_price' => 0,
                        'price' => $c['price'],
                        'nights' => $c['night'],
                        'ukey' => $c['ukey'],
                    );

                    if ($firstWalk) {
                        $costKeys = array_keys($_cost);
                        $costKeysString = implode('`,`', $costKeys);

                        $firstWalk = false;
                    }

                    $db->query("
                        insert into `xp_sejur_cost`(`{$costKeysString}`)
                        values ('" . implode("','",$_cost) . "')
                        on duplicate key update
                            `date_start` = values(`date_start`),
                            `availability_start` = values(`availability_start`),
                            `price` = values(`price`)");

                    if ($_cost['nights'] > $max_stay) {
                        $max_stay = $_cost['nights'];
                    }

                    if ($duration == 0 || $_cost['nights'] < $duration) {
                        $duration = $_cost['nights'];
                    }

                    $date = DateTime::createFromFormat('Y-m-d', $_cost['date_start']);

                    if (intval($date->format('U')) <= intval($date_start->format('U'))) {
                        $date_start = DateTime::createFromFormat('Y-m-d', $_cost['date_start']);
                    }

                    if (intval($date->format('U')) >= intval($date_end->format('U'))) {
                        $date_end = DateTime::createFromFormat('Y-m-d', $_cost['date_start']);
                    }

                    if (! in_array($sejur_transport['id'], $insertTransport)) {
                        $db->query("
                            insert
                            into `xp_sejur_to_transport` (`id_sejur`, `id_sejur_transport`)
                            values (?, ?)
                            on duplicate key update `id_sejur_transport` = values(`id_sejur_transport`)", array($row['id'], $sejur_transport['id']));

                        $insertTransport[] = $sejur_transport['id'];
                    }

                    $rows++;

                    if ($rows % 1000 == 0) {
                        $rowTime = (isset($rowTime) ? $rowTime : $time);

                        dif('<b>' . sprintf('%05.d', $rows / 1000) . 'k</b> rows in <b>' . (microtime(true) - $rowTime) . '</b>');
                    }

//                     if ($rows % 10 == 0) {
//                         dif('<b>' . $c['ukey'] . '</b> ukey took <b>' . (microtime(true) - $time) . '</b> seconds');
//                     }
                }
            }
        }

        $update = array(
            'duration' => $duration,
            'max_stay' => $max_stay,
            'price' => (isset($bestprice)?$bestprice:''),
            'date_start' => $date_start->format('Y-m-d'),
            'availability_start' => $date_start->format('Y-m-d'),
            'date_end' => $date_end->format('Y-m-d'),
            'availability_end' => $date_end->format('Y-m-d'),
            'lastSync' => $updateDate->format('Y-m-d') . ' 00:00:00',
        );

        $where = $table->getAdapter()->quoteInto('`id` = ?', $row['id']);

        $table->update($update, $where);

//        $db->commit();
//
//        $db->query("set unique_checks = 1");
//        $db->query("set foreign_key_checks = 1");

        dif('commit end ' . $hotel['attr']['hotelcode'], $hotelTime);
// Comment Costin
   }
	 
	 
	 
	 
    }
}

dif('finished');