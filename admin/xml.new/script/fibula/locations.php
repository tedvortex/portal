<?php
$file = file_get_contents(r . 'feed/locations.xml');

$xml = xml2array($file, 1, 'Location');

$locations = array(
    1 => array(),
    2 => array(),
    3 => array(),
    4 => array(),
);

if (isset($xml['Lists']['Locations']['Locations']) && is_array($xml['Lists']['Locations']['Locations'])) {
    loadModel('Country2', array($db));
    loadModel('City2', array($db));

    foreach ($xml['Lists']['Locations']['Locations'] as $k => $loc) {
        $loc = $loc['attr'];

        $locations[$loc['type']][$loc['id']] = $loc;
    }

    unset($xml['Lists']['Locations']);

    foreach ($locations[1] as $loc) {
        $Country2->insertOrUpdate((object)$loc);
    }

    unset($locations[1]);

    foreach ($locations[2] as $loc) {
        $Country2->insertOrUpdate((object)$loc);
    }

    unset($locations[2]);

    foreach ($locations[3] as $loc) {
        $City2->insertOrUpdate((object)$loc);
    }

    unset($locations[3]);

    foreach ($locations[4] as $loc) {
        $City2->insertOrUpdate((object)$loc);
    }

    unset($locations[4]);
}

if (isset($xml['Lists']['Rooms']['Room']) && is_array($xml['Lists']['Rooms']['Room'])) {
    loadModel('Room2', array($db));

    foreach ($xml['Lists']['Rooms']['Room'] as $k => $room) {
        $room = array_merge($room['attr'], array(
            'languagecode' => 'ro',
            'status' => 1,
            'owner' => 'Fibula'
        ));

        $Room2->insertOrUpdate((object)$room);
    }
}

if (isset($xml['Lists']['Boards']['Board']) && is_array($xml['Lists']['Boards']['Board'])) {
    loadModel('Board2', array($db));

    foreach ($xml['Lists']['Boards']['Board'] as $k => $board) {
        $board = array_merge($board['attr'], array(
            'languagecode' => 'ro',
            'status' => 1,
            'owner' => 'Fibula'
        ));

        $Board2->insertOrUpdate((object)$board);
    }
}