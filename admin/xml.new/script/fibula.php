<?php
$headers = apache_request_headers();

if (isset($headers['X']) && $headers['X']) {
    require_once('fibula/download.php');
    require_once('fibula/locations.php');
    require_once('fibula/hotels.php');
}