<?php
$start = microtime(true);

function dif ($message, $start = 0) {
    if ($start == 0) {
        global $start;
    }

    echo sprintf('%010.4f', (microtime(true) - $start)) . ' - ' . $message . PHP_EOL . '<br/>';
}

$priceModifiers = array(
    'Comision' => array(
        1 => array(
            'name' => 'Adl',
            'price' => 0,
        ),
        2 => array(
            'name' => 'Chd',
            'price' => 0,
        ),
        3 => array(
            'name' => 'Inf',
            'price' => 0,
        ),
    ),
    'Discount' => array(
        1 => array(
            'name' => 'Adl',
            'price' => 0,
        ),
        2 => array(
            'name' => 'Chd',
            'price' => 0,
        ),
        3 => array(
            'name' => 'Inf',
            'price' => 0,
        ),
    )
);

$modules = array(
    'transport' => 'default',
    'country' => 'name',
    'city' => 'name',
    'boardType' => 'name',
    'roomType' => 'name',
    'airportCity' => 'name',
);

$sql = array(
    'transport' => $db->select()
                        ->from(array('t' => 'xp_transport'),
                                array('default', 'id'))
                        ->joinInner(array('td' => 'xp_transport_data'),
                                '`t`.`id` = `td`.`_id`',
                                array('name', 'name_seo'))
                        ->where('`t`.`default` <> ""'),

    'city' => $db->select()
                    ->from(array('cd' => 'fibula_cities_data'),
                            array('name', 'id' => '_id'))
                    ->joinInner(array('c' => 'fibula_cities'),
                                '`c`.`id` = `cd`.`_id`',
                                array('id_country')),

    'country' => $db->select()
                    ->from(array('cd' => 'fibula_countries_data'),
                            array('name', 'id' => '_id'))
                    ->joinInner(array('c' => 'fibula_countries'),
                                '`c`.`id` = `cd`.`_id`',
                                array('id_parent')),

    'boardType' => $db->select()
                      ->from(array('bd' => 'fibula_board_types_data'),
                             array('name', 'id' => '_id')),

    'roomType' => $db->select()
                     ->from(array('rd' => 'fibula_room_types_data'),
                            array('name', 'id' => '_id')),

    'airportCity' => $db->select()
                        ->from(array('rd' => 'xp_cities_data'),
                               array('name' => new Zend_Db_Expr('lower(`rd`.`name`)'), 'id' => '_id')),

    'entity' => $db->select()
                    ->from(array('he' => 'fibula_hotels_entities'),
                           array('*')),

    'comision_sejur' => $db->select()
                           ->from(array('c' => 'config'),
                                  array('*'))
                           ->where('`key` = ?', 'comision_sejur'),

    'discount_sejur' => $db->select()
                           ->from(array('c' => 'config'),
                                  array('*'))
                           ->where('`key` = ?', 'discount_sejur')
);

foreach ($modules as $m => $default) {
    $statement = $db->query($sql[$m]);

    if ($statement) {
        ${$m . 'Data'} = ${$m . 'Id'} = array();

        while ($row = $statement->fetch()) {
            ${$m . 'Data'}[$row[$default]] = $row;
            ${$m . 'Id'}[$row['id']] = $row;
        }
    }
}

$statement = $db->query($sql['entity']);

if ($statement) {
    $entityData = array();

    while ($row = $statement->fetch()) {
        $entityData[$row['id_hotel']][$row['adults']][$row['children']][$row['id']] = $row['code'];
    }
}

$statement = $db->query($sql['comision_sejur']);

if ($statement) {
    $pricingData = array();

    while ($row = $statement->fetch()) {
        if (preg_match('%(?:\s*Adl\s*\(\s*([^\)]*)\s*\)\s*\/?)(?:\s*Chd\s*\(\s*([^\)]*)\s*\)\s*\/?)?(?:\s*Inf\s*\(\s*([^\)]*)\s*\)\s*\/?)?%is', $row['value'], $matches)) {
            foreach ($matches as $k => $m) {
                if ($k > 0) {
                    $priceModifiers['Comision'][$k]['price'] = intval($m);

                    if (preg_match('%\%$%is', $m)) {
                        $priceModifiers['Comision'][$k]['type'] = 1;
                    } else {
                        $priceModifiers['Comision'][$k]['type'] = 0;
                    }
                }
            }
        }
    }
}

$statement = $db->query($sql['discount_sejur']);

if ($statement) {
    $pricingData = array();

    while ($row = $statement->fetch()) {
        if (preg_match('%(?:\s*Adl\s*\(\s*([^\)]*)\s*\)\s*\/?)(?:\s*Chd\s*\(\s*([^\)]*)\s*\)\s*\/?)?(?:\s*Inf\s*\(\s*([^\)]*)\s*\)\s*\/?)?%is', $row['value'], $matches)) {
            foreach ($matches as $k => $m) {
                if ($k > 0) {
                    $priceModifiers['Discount'][$k]['price'] = intval($m);

                    if (preg_match('%\%$%is', $m)) {
                        $priceModifiers['Discount'][$k]['type'] = 1;
                    } else {
                        $priceModifiers['Discount'][$k]['type'] = 0;
                    }
                }
            }
        }
    }
}