<?php
$microTime = microtime(true);

$priceModifiers = array(
    'Comision' => array(
        1 => array(
            'name' => 'Adl',
            'price' => 0,
        ),
        2 => array(
            'name' => 'Chd',
            'price' => 0,
        ),
        3 => array(
            'name' => 'Inf',
            'price' => 0,
        ),
    ),
    'Discount' => array(
        1 => array(
            'name' => 'Adl',
            'price' => 0,
        ),
        2 => array(
            'name' => 'Chd',
            'price' => 0,
        ),
        3 => array(
            'name' => 'Inf',
            'price' => 0,
        ),
    )
);

$isStandardPriceValues = array(
    'Promo' => 2,
    'Fix' => 1,
    'De la' => 0,
);

$inputFileType = 'Excel5';

$inputFileName = r . '../../' . (isset($_GET['file']) ? $_GET['file'] : null);

if (is_file($inputFileName)) {
    foreach ($models as $k => $m) {
        loadModel($m, array($db));
        $$k = $$m;
    }

    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objSheets = $objReader->listWorksheetNames($inputFileName);
    $objPHPExcel = $objReader->load($inputFileName);

    foreach ($objSheets as $sheet) {
        $error = false;

        print_a('<strong>' . $sheet . ': </strong>');

        $cost = array(
            'type' => 0,
            'max_adults' => 0,
        );

        $objWorksheet = $objPHPExcel->getSheetByName($sheet);

        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        /**
         *     row 1, column 1 - sejur name **informative** {string}
         * get row 2, column 1 - sejur id {integer}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 2)->getValue());

        $sejur = $db->fetchRow($db->select()
                                  ->from('xp_' . $sejurTableName)
                                  ->where('`id` = ?', $cellValue));

        if ($sejur) {
            $cost['id_circuit'] = $sejur['id'];
        } else {
            haltMessage('Sejur inexistent', $cellValue);
        }

        /**
         * get row 3, column 1 - entitate transport asociata
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 3)->getValue());

        $id_transport = $db->fetchOne($db->select()
                                        ->from(array('st' => 'xp_' . $sejurTableName . '_transport'),
                                               array('id'))
                                        ->where('st.`id` = ?', $cellValue));

        if ($id_transport) {
            $cost['id_sejur_transport'] = $id_transport;
        } else {
            haltMessage('Transport inexistent', $cellValue);
        }

        /**
         * get row 4, column 1 - sales validity interval (%date% - %date%) {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 4)->getValue());

        if (preg_match('%([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})\s*\-\s*([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})%ism', $cellValue, $matches)) {
            $date_start = DateTime::createFromFormat('Ymd', $matches[3] . sprintf('%02d', $matches[2]) . sprintf('%02d', $matches[1]));
            $date_end   = DateTime::createFromFormat('Ymd', $matches[6] . sprintf('%02d', $matches[5]) . sprintf('%02d', $matches[4]));

            if ($date_start && $date_end) {
                $cost['availability_start'] = $date_start->format('Y-m-d') . ' 00:00:00';
                $cost['availability_end'] = $date_end->format('Y-m-d') . ' 00:00:00';
            } else {
                haltMessage('Perioada de vanzare invalida', $cellValue);
            }
        } else {
            haltMessage('Perioada de vanzare invalida', $cellValue);
        }

        /**
         * get row 5, column 1 - board type (code) {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 5)->getValue());

        if (preg_match('%([\p{L}\p{N}\p{Z}]+)\(([\p{L}\p{N}\p{Z}\p{P}]+)\)%ism', $cellValue, $matches)) {
            $_board = (object) array(
                'Code' => trim($matches[2]),
                'Name' => trim($matches[1]),
                'LanguageCode' => 'ro',
                'Status' => 'Active',
            );

            $Board->insertOrUpdate($_board);

            $cost['id_board_type'] = $_board->Code;
        } else {
            haltMessage('Formatul mesei este invalid', $cellValue);
        }

        /**
         * get row 6, column 1 - number of nights {integer}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 6)->getValue());

        if (is_numeric($cellValue)) {
            $cost['nights'] = $cellValue;
        } else {
            haltMessage('Numarul de nopti este invalid', $cellValue);
        }

        /**
         * get row 7, column 1 - confirmation type name {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 7)->getValue());

        $_confirmation = (object) array(
            'Name' => $cellValue,
            'LanguageCode' => 'ro',
            'Status' => 'Active',
        );

        $cost['id_confirmation_type'] = $Confirmation->insertOrUpdate($_confirmation);

        /**
         * get row 8, column 1 - is standard price or not {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 8)->getValue());

		
        if (isset($isStandardPriceValues[$cellValue])) {
            $cost['is_standard_price'] = $isStandardPriceValues[$cellValue];
        } else {
            haltMessage('Tipul pretului este invalid', $cellValue);
        }

        /**
         * get row 9, column 1 - Comission modifer {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 9)->getValue());

        if ($cellValue && preg_match('%(?:\s*Adl\s*\(\s*([^\)]*)\s*\)\s*\/?)(?:\s*Chd\s*\(\s*([^\)]*)\s*\)\s*\/?)?(?:\s*Inf\s*\(\s*([^\)]*)\s*\)\s*\/?)?%is', $cellValue, $matches)) {
            foreach ($matches as $k => $m) {
                if ($k > 0) {
                    $priceModifiers['Comision'][$k]['price'] = intval($m);

                    if (preg_match('%\%$%is', $m)) {
                        $priceModifiers['Comision'][$k]['type'] = 1;
                    } else {
                        $priceModifiers['Comision'][$k]['type'] = 0;
                    }
                }
            }
        } else {
            haltMessage('Formatul comisionului este invalid', $cellValue);
        }

        /**
         * get row 10, column 1 - Discount modifer {string}
         */
        $cellValue = trim($objWorksheet->getCellByColumnAndRow(1, 10)->getValue());

        if ($cellValue && preg_match('%(?:\s*Adl\s*\(\s*([^\)]*)\s*\)\s*\/?)(?:\s*Chd\s*\(\s*([^\)]*)\s*\)\s*\/?)?(?:\s*Inf\s*\(\s*([^\)]*)\s*\)\s*\/?)?%is', $cellValue, $matches)) {
            foreach ($matches as $k => $m) {
                if ($k > 0) {
                    $priceModifiers['Discount'][$k]['price'] = intval($m);

                    if (preg_match('%\%$%is', $m)) {
                        $priceModifiers['Discount'][$k]['type'] = 1;
                    } else {
                        $priceModifiers['Discount'][$k]['type'] = 0;
                    }
                }
            }
        } else {
            haltMessage('Formatul comisionului este invalid', $cellValue);
        }

        if (! $error) {
            $insertTransport = true;

            for ($row = 11; $row <= $highestRow; $row++) {
                $db->beginTransaction();
                $db->query("set unique_checks = 0");
                $db->query("set foreign_key_checks = 0");

                for ($col = 0; $col <= $highestColumnIndex; $col++) {
                    $cellValue = trim($objWorksheet->getCellByColumnAndRow($col, $row)->getValue());

                    if ($cellValue) {
                        if ($col == 0) {
                            $columnHeader = false;

                            if (preg_match('%^([\p{L}\p{N}\p{Z}]+)\(([\p{L}\p{N}\p{Z}\p{P}]+)\)$%ism', $cellValue, $matches)) {
                                if ($matches) {
                                    $columnHeader = true;

                                    $_room = (object) array(
                                        'Code' => trim($matches[2]),
                                        'Name' => trim($matches[1]),
                                        'LanguageCode' => 'ro',
                                        'Status' => 'Active',
                                        'Owner' => 'Mondial',
                                    );

                                    $Room->insertOrUpdate($_room);

                                    $cost['id_room_type'] = $_room->Code;

                                    $_dates = array();
                                }
                            } else {
                                if (preg_match('%^(\p{N})\s*(?:Adu?lt?)%is', $cellValue, $adults)) {
                                    if (isset($adults[1]) && is_numeric($adults[1])) {
                                        $childrenCount = 0;
                                        $_children = array();

                                        $select = $db->select()
                                                     ->from(array('he' => $prefix . 'hotels_entities'))
                                                     ->where('he.`id_hotel` = ?', $sejur['id_hotel'])
                                                     ->where('he.`adults` = ?', $adults[1]);

                                        $params = array();

                                        if (preg_match_all('%\+(\p{N})\s*(?:Ch(?:ild)?d(?:ren)?)\(([\p{N}\.]+)\-([\p{N}\.]+)\)%is', $cellValue, $children)) {
                                            foreach ($children[1] as $key => $child) {
                                                $childrenCount += $children[1][$key];

                                                $select->joinInner(array('hec' . $key => $prefix . 'hotels_entities_children'),
                                                                   'hec' . $key . '.`id_hotel_entity` = he.`id`
                                                                   and hec' . $key . '.`count` = :hec' . $key . 'count
                                                                   and hec' . $key . '.`age_start` = :hec' . $key . 'age_start
                                                                   and hec' . $key . '.`age_end` = :hec' . $key . 'age_end',
                                                                   array(''));

                                                $params['hec' . $key . 'count'] = $children[1][$key];
                                                $params['hec' . $key . 'age_start'] = $children[2][$key];
                                                $params['hec' . $key . 'age_end'] = $children[3][$key];

                                                $_children[$key] = array(
                                                    'count' => $children[1][$key],
                                                    'age_start' => $children[2][$key],
                                                    'age_end' => $children[3][$key],
                                                    'code' => $children[1][$key] . ' Chd(' . $children[2][$key] . '-' . $children[3][$key] . ')',
                                                );
                                            }
                                        }

                                        $select->where('`he`.`children` = ?', $childrenCount);

                                        $hotel_entity = $db->fetchRow($select, $params);

                                        if (! $hotel_entity) {
                                            $_entity = array(
                                                'id_hotel' => $sejur['id_hotel'],
                                                'status' => 1,
                                                'adults' => $adults[1],
                                                'children' => $childrenCount,
                                                'code' => $cellValue
                                            );

                                            $db->insert($prefix . 'hotels_entities', $_entity);

                                            $hotel_entity = $_entity;
                                            $hotel_entity['id'] = $db->lastInsertId();

                                            if (is_array($_children) && $hotel_entity) {
                                                foreach ($_children as $c) {
                                                    $c['id_hotel_entity'] = $hotel_entity['id'];

                                                    for ($_i = 0; $_i < $c['count']; $_i++) {
                                                        $_c = $c;

                                                        $_c['count'] = 1;

                                                        $db->insert($prefix . 'hotels_entities_children', $c);
                                                    }
                                                }
                                            }
                                        }

                                        $cost['id_hotel_entity'] = $hotel_entity['id'];
                                    }
                                }
                            }
                        } else {
                            if ($columnHeader) {
                               if (preg_match_all('%([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})%ism', $cellValue, $dates)) {
                                    foreach ($dates[0] as $key => $date) {
                                        if (isset($dates[1][$key]) && isset($dates[2][$key])) {
                                            $_dates[$col][] = ($dates[3][$key] ? $dates[3][$key] : date('Y')) . '-' . sprintf('%02d', $dates[2][$key]) . '-' . sprintf('%02d', $dates[1][$key]);
                                        }
                                    }
                                }
                            } else {
                                if (isset($_dates[$col])) {
                                    foreach ($_dates[$col] as $date) {
                                        $cost['date_start'] = $date;
                                        $cost['ukey'] = 'MON-' . $cost['id_room_type'] . '-' . $cost['id_board_type']
                                                      . '-' . $cost['id_circuit'] . '-' . $cost['type'] . '-' . $cost['nights']
                                                      . '-' . $cost['id_hotel_entity'] . '-' . str_replace('-', '', $cost['date_start']);

                                        $cost['price'] = generateCost($priceModifiers, sprintf('%.2f', $cellValue), $hotel_entity, $_children);

                                        if ($insertTransport) {
                                            $costKeys = array_keys($cost);
                                            $costKeysString = implode('`,`', $costKeys);

                                            $insertTransport = false;
                                        }

                                        $db->query("
                                            insert into `xp_{$sejurTableName}_cost`(`{$costKeysString}`)
                                            values ('" . implode("','",$cost) . "')
                                            on duplicate key update
                                                `date_start` = values(`date_start`),
                                                `is_standard_price` = values(`is_standard_price`),
                                                `availability_start` = values(`availability_start`),
                                                `price` = values(`price`)");
                                        
                                    }
                                }
                            }
                        }
                    }
                }

                $db->commit();

                $db->query("set unique_checks = 1");
                $db->query("set foreign_key_checks = 1");
            }

            print_a('- Success !');
        } else {
            print_a('- Costurile din acest sheet nu au fost importate !');
        }
    }
} else {
    haltMessage('Fisierul uploadat nu este valid', $inputFileName);
}

print_a('Script execution time: ' . (microtime(true) - $microTime) . ' seconds');