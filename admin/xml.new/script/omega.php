<?php
@error_reporting(E_NONE);
$headers = apache_request_headers();

//if (isset($headers['X']) && $headers['X']) {
    $lastDate = $db->fetchOne("
            select min(`lastsync`)
            from `xp_hotels`
            where `is_api` = 1 ");

    $date = DateTime::createFromFormat('Y-m-d H:i:s', $lastDate);
    $updateDate = new DateTime();
    $updateDate->modify('-2 day');

    loadModel('Country', array($db));
    loadModel('City', array($db));
    loadModel('Hotel', array($db));

    $tableImages = new Zend_Db_Table('xp_hotels_images');

    $countryParams = $cityParams = array_merge(
            $parameters,
            array(
                'LanguageCode' => 'en',
                'LastSync' => $updateDate->format('Y-m-d 00:00:00'))
    );

    try {
        $resultCountries = $Client->GetCountries($countryParams);
    } catch (SoapFault $Exception) {
        print_a($Exception);

        echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
        echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
    }

    if ($resultCountries) {
        if (is_object($resultCountries->GetCountriesResponseCountries)
            && isset($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry)
            && is_array($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry))
        {
            foreach ($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry as $country)
            {
                $country->lastSync = $date->format('Y-m-d 00:00:00');
                $Country->insertOrUpdate($country);

                $cityParams['CountryCode'] = $country->Code;

                try {
                    $resultCities = $Client->GetCities($cityParams);
                } catch (SoapFault $Exception) {
                    print_a($Exception);

                    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
                    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
                }

                if ($resultCities) {
                    if (is_object($resultCities->GetCitiesResponseCities)
                        && isset($resultCities->GetCitiesResponseCities->GetCitiesResponseCity)
                        && is_array($resultCities->GetCitiesResponseCities->GetCitiesResponseCity))
                    {
                        foreach ($resultCities->GetCitiesResponseCities->GetCitiesResponseCity as $c) {
                            if ($c->Code) {
                                $parameters['CityCode'] = $c->Code;

                                $c->lastSync = $date->format('Y-m-d 00:00:00');
                                $City->insertOrUpdate($c);

                                try {
                                    $result = $Client->GetHotels($parameters);
                                } catch (SoapFault $Exception) {
                                    print_a($Exception);

                                    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
                                    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
                                }

                                if ($result) {
                                    if (is_object($result->GetHotelsResponseHotels)
                                        && isset($result->GetHotelsResponseHotels->GetHotelsResponseHotel)
                                        && is_array($result->GetHotelsResponseHotels->GetHotelsResponseHotel))
                                    {
                                        foreach ($result->GetHotelsResponseHotels->GetHotelsResponseHotel as $p) {
                                            echo "Oras: {$c->Code}, Hotel: {$p->Code}" . PHP_EOL;

                                            $p->lastSync = $updateDate->format('Y-m-d 00:00:00');
                                            @ob_flush();

                                            $Hotel->insertOrUpdate($p);

                                            if (isset($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto)
                                                && is_array($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto))
                                            {
                                                foreach ($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto as $r) {
                                                    if (preg_match('%http://xml\.omegahotels\.ro/[0-9]+/(.*)%is', $r->URL, $matches)) {
                                                        $file = r . '../../static/i/hotel/' . $matches[1];

                                                        if (! is_file($file)) {
                                                            $page = get_web_page($r->URL);

                                                            if ($page['errno'] == 0 && $page['http_code'] == '200') {
                                                                file_put_contents($file, $page['content']);

                                                                $data = array(
                                                                    'id_hotel' => $p->Code,
                                                                    'name' => $r->Name,
                                                                    'image' => $matches[1],
                                                                );

                                                                if (! $tableImages->fetchRow($tableImages->select()
                                                                                                         ->where('id_hotel = ?', $p->Code)
                                                                                                         ->where('image = ?', $matches[1])))
                                                                {
                                                                    $tableImages->insert($data);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data = array('has_hotels' => 1);
                                    $where = $db->quoteInto('id = ?', $c->Code);

                                    $City->table->update($data, $where);
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        echo 'Empty result set' . PHP_EOL;
    }

    $db->query("
            update `xp_hotels`
            set `lastsync` = ?
            where `is_api` = 1",
            array(date('Y-m-d 00:00:00')));
// }