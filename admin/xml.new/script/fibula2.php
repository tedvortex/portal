<?php
$headers = apache_request_headers();

if (isset($headers['X']) && $headers['X']) {
    require_once('fibula2/download.php');
    require_once('fibula2/locations.php');
    require_once('fibula2/hotels.php');
}