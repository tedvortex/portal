<?php
$countryParams = $cityParams = array_merge(
        $parameters,
        array('LanguageCode' => 'en')
);

try {
    $resultCountries = $Client->GetCountries($countryParams);
} catch (SoapFault $Exception) {
    print_a($Exception);

    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
}

$table = new Zend_Db_Table('xp_hotels_images');
$tableHotels = new Zend_Db_Table('xp_hotels');

if ($resultCountries) {
    if (is_object($resultCountries->GetCountriesResponseCountries)
        && isset($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry)
        && is_array($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry))
    {
        foreach ($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry as $country)
        {
            $cityParams['CountryCode'] = $country->Code;

            try {
                $resultCities = $Client->GetCities($cityParams);
            } catch (SoapFault $Exception) {
                print_a($Exception);

                echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
                echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
            }

            if ($resultCities) {
                if (is_object($resultCities->GetCitiesResponseCities)
                    && isset($resultCities->GetCitiesResponseCities->GetCitiesResponseCity)
                    && is_array($resultCities->GetCitiesResponseCities->GetCitiesResponseCity))
                {
                    foreach ($resultCities->GetCitiesResponseCities->GetCitiesResponseCity as $c) {
                        $parameters['CityCode'] = $c->Code;
//                         $parameters['LastSync'] = date('c');

                        try {
                            $result = $Client->GetHotels($parameters);
                        } catch (SoapFault $Exception) {
                            print_a($Exception);

                            echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
                            echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
                        }

                        if ($result) {
                            if (is_object($result->GetHotelsResponseHotels)
                                && isset($result->GetHotelsResponseHotels->GetHotelsResponseHotel)
                                && is_array($result->GetHotelsResponseHotels->GetHotelsResponseHotel))
                            {
                                foreach ($result->GetHotelsResponseHotels->GetHotelsResponseHotel as $p) {
                                    print_a($p->Code);
                                    @ob_flush();

                                    $processed = $tableHotels->fetchRow($tableHotels->select()
                                                                                    ->where('processed = 1 and `id`= ?', $p->Code));

                                    if ($processed) {
                                        break;
                                    }

                                    if (isset($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto)
                                        && is_array($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto))
                                    {
                                        foreach ($p->GetHotelsResponsePhotos->GetHotelsResponsePhoto as $r) {
                                            if (preg_match('%http://test\.xml\.omegahotels\.ro/[0-9]+/(.*)%is', $r->URL, $matches)) {
                                                $file = r . '../../static/i/hotel/' . $matches[1];
                                                $page = get_web_page($r->URL);

                                                if ($page['errno'] == 0 && $page['http_code'] == '200') {
                                                    file_put_contents($file, $page['content']);

                                                    $data = array(
                                                        'id_hotel' => $p->Code,
                                                        'name' => $r->Name,
                                                        'image' => $matches[1],
                                                    );

                                                    $table->insert($data);
                                                }
                                            }
                                        }
                                    }

                                    $data = array('processed' => 1);
                                    $where = $tableHotels->getAdapter()->quoteInto('id = ?', $p->Code);

                                    $tableHotels->update($data, $where);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} else {
    print_a('Empty result set');
}