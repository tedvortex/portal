<?php
class Hotel {
    public $adapter,
           $table,
           $tableData,
           $tableDirection;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_hotels');
        $this->tableData = new Zend_Db_Table('xp_hotels_data');
        $this->tableDirection = new Zend_Db_Table('xp_hotels_directions');
    }

    final public function insertOrUpdate($data) {
    	
    	
        if ($data->Code) {
            $_data = array(
                'id' => $data->Code,
                'code' => $data->Code,
                'is_api' => 1,
                'id_city' => $data->CityCode,
                'status' => (in_array($data->Status, array('Active', 'Modified')) ? 1 : 0),
                'rating' => $data->Rating,
                'latitude' => $data->Latitude,
                'longitude' => $data->Longitude,
                'lastsync' => $data->lastSync,
                'date' => $data->lastSync
            );

            $_data_extend = array(
                '_id' => $data->Code,
                'lang' => 'en',
                'name' => $data->Name,
                'name_seo' => url($data->Name),
                'address' => $data->Address,
                'postal_code' => $data->PostalCode,
                'telephone' => $data->Telephone,
                'fax' => $data->Fax,
                'website' => $data->Website,
                'email' => $data->Email,
            );

            $row = $this->adapter->fetchOne($this->table->select()
                                                        ->where('id = ?', $data->Code)/*
                                                        ->where('id_city = ?', $data->CityCode) */);

            if (! $row) {
                $this->table->insert($_data);
            } else {
                unset($_data['id']);

                $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->Code));
            }

            $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                                   ->where('_id = ?', $data->Code)
                                                                   ->where('lang = ?', mb_strtolower('en')));

            if (! $row_extend) {
                $this->tableData->insert($_data_extend);
            }

            if (isset($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections)
                && is_array($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections))
            {

                foreach ($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections as $r) {
                	if (isset($r->LanguageCode)) {
		                    $row_direction = $this->adapter->fetchRow($this->tableDirection->select()
		                                                                                   ->where('id_hotel = ?', $data->Code)
		                                                                                   ->where('lang = ?', $r->LanguageCode));
		
		                    if (! $row_direction) {
		                        $data_direction = array(
		                            'id_hotel' => $data->Code,
		                            'lang' => $r->LanguageCode,
		                            'description' => $r->Directions,
		                        );
		
		                        $this->tableDirection->insert($data_direction);
		                    }
		                }
              	  }
            }

            if (isset($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription))
            {
                if (is_array($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription)) {
                    foreach ($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription as $r) {
                    	if (isset($r->LanguageCode)) {
	                        if ($r->LanguageCode == 'en') {
	                            $this->adapter->query("
	                                    update `xp_hotels_data`
	                                    set `description` = ?
	                                    where
	                                        `lang` = ?
	                                        and `_id` = ?",
	                                    array($r->Description, $r->LanguageCode, $data->Code));
	                        } else {
	                            $data_extend['lang'] = $r->LanguageCode;
	                            $data_extend['description'] = $r->Description;
	                        }
                    	}
                    }
                }
            }

//             if (isset($data->GetHotelsResponsePhotos->GetHotelsResponsePhoto)
//                 && is_array($data->GetHotelsResponsePhotos->GetHotelsResponsePhoto))
//             {
//                 foreach ($$data->GetHotelsResponsePhotos->GetHotelsResponsePhoto as $r) {
//                     print_a($r);
//                 }
//             }

//             if (isset($data->GetHotelsResponseFacilities->GetHotelsResponseFacility)
//                 && is_object($data->GetHotelsResponseFacilities->GetHotelsResponseFacility))
//             {
//                 $facility = $p->GetHotelsResponseFacilities->GetHotelsResponseFacility;
//             }
        }
    }
}