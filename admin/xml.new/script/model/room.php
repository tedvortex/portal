<?php
class Room {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_room_types');
        $this->tableData = new Zend_Db_Table('xp_room_types_data');
    }

    final public function insertOrUpdate($data) {
        if ($data->Code) {
            $_data = array(
                'id' => $data->Code,
                'status' => ($data->Status == 'Active' ? 1 : 0),
            );

            $_data_extend = array(
                '_id' => $data->Code,
                'lang' => mb_strtolower($data->LanguageCode),
                'name' => $data->Name,
                'name_seo' => url($data->Name),
                'owner' => $data->Owner,
            );

            $row = $this->adapter->fetchOne($this->table->select()
                                                        ->where('id = ?', $data->Code));

            if (! $row) {
                $this->table->insert($_data);
            } else {
                unset($_data['id']);

                $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->Code));
            }

            $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                                   ->where('_id = ?', $data->Code)
                                                                   ->where('lang = ?', mb_strtolower($data->LanguageCode)));

            if (! $row_extend) {
                $this->tableData->insert($_data_extend);
            }
        }
    }
}