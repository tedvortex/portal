<?php
class City {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_cities');
        $this->tableData = new Zend_Db_Table('xp_cities_data');
    }

    final public function insertOrUpdate($data) {
        if ($data->Code) {
            $_data = array(
                'id' => $data->Code,
                'id_country' => $data->CountryCode,
                'status' => ($data->Status == 'Active' ? 1 : 0),
                'lastsync' => $data->lastSync,
                'date' => $data->lastSync
            );

            $_data_extend = array(
                '_id' => $data->Code,
                'lang' => mb_strtolower($data->LanguageCode),
                'name' => $data->Name,
                'name_seo' => url($data->Name),
            );

            $row = $this->adapter->fetchRow($this->table->select()
                                                        ->where('id = ?', $data->Code)
                                                        /* ->where('id_country = ?', $data->CountryCode) */);

            if (! $row) {
                $this->table->insert($_data);
            } else {
                unset($_data['id']);

                $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->Code));
            }

            $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                                   ->where('_id = ?', $data->Code)
                                                                   ->where('lang = ?', mb_strtolower($data->LanguageCode)));

            if (! $row_extend) {
                $this->tableData->insert($_data_extend);
            }
        }
    }
}