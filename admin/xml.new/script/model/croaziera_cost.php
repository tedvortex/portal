<?php
class Circuit_cost {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_croaziera_cost');
    }

    final public function insertOrUpdate($data) {
        $select = $this->table->select();

        foreach ($data as $k => $v) {
            $select->where('`' . $k . '` = ?', $v);
        }

        $row = $this->adapter->fetchOne($select);

        if (! $row) {
            $this->table->insert($data);
        }
    }
}