<?php
class Sejur_cost {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_sejur_cost');
    }

    final public function insertOrUpdate($data) {
        $select = $this->table->select()
                              ->where('`ukey` = ?', $data['ukey']);

        $row = $this->adapter->fetchOne($select);

        if (! $row) {
            $this->table->insert($data);
        }
    }
}