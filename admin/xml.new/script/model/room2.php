<?php
class Room2 {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('fibula_room_types');
        $this->tableData = new Zend_Db_Table('fibula_room_types_data');
    }

    final public function insertOrUpdate($data) {
        $dataKeep = array(
            'code' => 'Code',
            'name' => 'Name',
            'languagecode' => 'LanguageCode',
            'status' => 'Status',
            'owner' => 'Owner',
        );

        foreach ($dataKeep as $k => $v) {
            $data->$k = (isset($data->$k) ? $data->$k : $data->$v);
        }

        $_data = array(
            'id' => $data->code,
            'status' => 1,
//            'max_adults' => 3,
        );

        $_data_extend = array(
            '_id' => $data->code,
            'lang' => 'ro',
            'name' => $data->name,
            'name_seo' => url($data->name),
        );

        if (isset($data->owner)) {
            $_data_extend['owner'] = $data->owner;
        }

        $row = $this->adapter->fetchOne($this->table->select()
                                                    ->where('`id` = ?', $data->code));

        if (! $row) {
            $this->table->insert($_data);
        } else {
            unset($_data['id']);

            $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->code));
        }

        $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                               ->where('_id = ?', $data->code)
                                                               ->where('lang = ?', 'ro'));

        if (! $row_extend) {
            $this->tableData->insert($_data_extend);
        }
    }
}