<?php
class Confirmation {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('xp_confirmation_type');
        $this->tableData = new Zend_Db_Table('xp_confirmation_type_data');
    }

    final public function insertOrUpdate($data) {
        $row_extend = $this->adapter->fetchRow($this->tableData->select()
                                                               ->where('name = ?', $data->Name)
                                                               ->where('lang = ?', mb_strtolower($data->LanguageCode)));

        if ($row_extend) {
            return $row_extend['_id'];
        } else {
            $_data = array(
                'status' => ($data->Status == 'Active' ? 1 : 0),
            );

            $id = $this->table->insert($_data);

            $_data_extend = array(
                '_id' => $id,
                'lang' => mb_strtolower($data->LanguageCode),
                'name' => $data->Name,
                'name_seo' => url($data->Name),
            );

            $this->tableData->insert($_data_extend);

            return $_data_extend['_id'];
        }
    }
}