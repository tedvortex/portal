<?php
class Hotel2 {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('fibula_hotels');
        $this->tableData = new Zend_Db_Table('fibula_hotels_data');
    }

    final public function insertOrUpdate($data) {
        $_data = array(
            'code' => $data->hotelcode,
            'is_api' => 1,
            'id_country' => $data->id_country,
            'id_region' => $data->id_region,
            'status' => 1,
            'rating' => intval($data->stars),
            'price' => $data->bestprice,
            'latitude' => $data->Lat,
            'longitude' => $data->Long,
        );

        $_data_extend = array(
            'lang' => 'ro',
            'description' => $data->description,
            'name' => $data->hotelname,
            'name_seo' => url($data->hotelname),
        );

        $row = $this->adapter->fetchRow($this->table->select()
                                                    ->where('code = ?', $data->hotelcode)/*
                                                    ->where('id_city = ?', $data->CityCode) */);

        if (! $row) {
            $row['id'] = $this->table->insert($_data);
        } else {
            unset($_data['id']);

            $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->hotelcode));
        }

        $_data_extend['_id'] = $row['id'];

        $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                               ->where('_id = ?', $_data_extend['_id'])
                                                               ->where('lang = ?', mb_strtolower('ro')));

        if (! $row_extend) {
            $this->tableData->insert($_data_extend);
        } else {
            $this->tableData->update($_data_extend, array($this->adapter->quoteInto('`_id` = ?', $_data_extend['_id']),
                                                          $this->adapter->quoteInto('`lang` = ?', 'ro')));
        }

        if (isset($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections)
            && is_array($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections))
        {
            foreach ($data->GetHotelsResponseDirectionsList->GetHotelsResponseDirections as $r) {
                $row_direction = $this->adapter->fetchRow($this->tableDirection->select()
                                                                               ->where('id_hotel = ?', $data->Code)
                                                                               ->where('lang = ?', $r->LanguageCode));

                if (! $row_direction) {
                    $data_direction = array(
                        'id_hotel' => $data->Code,
                        'lang' => $r->LanguageCode,
                        'description' => $r->Directions,
                    );

                    $this->tableDirection->insert($data_direction);
                }
            }
        }

        if (isset($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription))
        {
            if (is_array($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription)) {
                foreach ($data->GetHotelsResponseDescriptions->GetHotelsResponseDescription as $r) {
                    if ($r->LanguageCode == 'en') {
                        $this->adapter->query("
                                update `xp_hotels_data`
                                set `description` = ?
                                where
                                    `lang` = ?
                                    and `_id` = ?",
                                array($r->Description, $r->LanguageCode, $data->Code));
                    } else {
                        $data_extend['lang'] = $r->LanguageCode;
                        $data_extend['description'] = $r->Description;
                    }
                }
            }
        }

        return $_data_extend['_id'];
    }
}