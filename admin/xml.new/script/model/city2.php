<?php
class City2 {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('fibula_cities');
        $this->tableData = new Zend_Db_Table('fibula_cities_data');
        $this->tableCountries = new Zend_Db_Table('fibula_countries');
    }

    final public function insertOrUpdate($data) {
        $_data = array(
            'id' => $data->id,
            'code' => $data->code,
            'id_country' => $data->parent,
            'status' => 1,
        );

        if (! $this->adapter->fetchOne($this->tableCountries->select()
                                                            ->where('`id` = ?', $data->parent))) {
            return false;
        }

        $_data_extend = array(
            '_id' => $data->id,
            'lang' => 'ro',
            'name' => $data->name,
            'name_seo' => url($data->name),
        );

        $row = $this->adapter->fetchOne($this->table->select()
                                                    ->where('id = ?', $data->id)
                                                    ->where('id_country = ?', $data->parent));

        if (! $row) {
            $this->table->insert($_data);
        } else {
            unset($_data['id']);

            $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->id));
        }

        $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                               ->where('_id = ?', $data->id)
                                                               ->where("lang = 'ro'"));

        if (! $row_extend) {
            $this->tableData->insert($_data_extend);
        }
    }
}