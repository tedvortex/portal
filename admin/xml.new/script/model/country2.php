<?php
class Country2 {
    public $adapter,
           $table,
           $tableData;

    final public function __construct($adapter) {
        $this->adapter = $adapter;
        $this->table = new Zend_Db_Table('fibula_countries');
        $this->tableData = new Zend_Db_Table('fibula_countries_data');
    }

    final public function insertOrUpdate($data) {
        $_data = array(
            'id' => $data->id,
            'code' => $data->code,
            'status' => 1,
        );

        if ($data->id != $data->parent) {
            $_data['id_parent'] = $data->parent;

            $row = $this->adapter->fetchRow($this->table->select()
                                                        ->where('`id` = ?', $data->parent));

            if (! $row) {
                return false;
            }
        }

        $_data_extend = array(
            '_id' => $data->id,
            'lang' => 'ro',
            'name' => $data->name,
            'name_seo' => url($data->name),
        );

        $row = $this->adapter->fetchOne($this->table->select()
                                                    ->where('`code` = ?', $data->code));

        if (! $row) {
            $data->id = $this->table->insert($_data);
        } else {
            $this->table->update($_data, $this->adapter->quoteInto('`id` = ?', $data->id));
        }

        $row_extend = $this->adapter->fetchOne($this->tableData->select()
                                                               ->where('_id = ?', $data->id)
                                                               ->where("lang = 'ro'"));

        if (! $row_extend) {
            $this->tableData->insert($_data_extend);
        }
    }
}