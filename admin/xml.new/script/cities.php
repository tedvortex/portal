<?php
$countryParams = array_merge(
        $parameters,
        array('LanguageCode' => 'en')
);

try {
    $resultCountries = $Client->GetCountries($countryParams);
} catch (SoapFault $Exception) {
    print_a($Exception);

    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
}

$table = new Zend_Db_Table('xp_cities');
$tableData = new Zend_Db_Table('xp_cities_data');

if ($resultCountries) {
    if (is_object($resultCountries->GetCountriesResponseCountries)
        && isset($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry)
        && is_array($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry))
    {
        foreach ($resultCountries->GetCountriesResponseCountries->GetCountriesResponseCountry as $p)
        {
            $parameters['CountryCode'] = $p->Code;

            try {
                $result = $Client->GetCities($parameters);
            } catch (SoapFault $Exception) {
                print_a($Exception);

                echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
                echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
            }

            if ($result) {
                if (is_object($result->GetCitiesResponseCities)
                    && isset($result->GetCitiesResponseCities->GetCitiesResponseCity)
                    && is_array($result->GetCitiesResponseCities->GetCitiesResponseCity))
                {
                    foreach ($result->GetCitiesResponseCities->GetCitiesResponseCity as $c) {
                        $row = $table->fetchRow($table->select()
                                                      ->where('id = ?', $c->Code)
                                                      ->where('id_country = ?', $c->CountryCode));

                        if (! $row) {
                            $data = array(
                                'id' => $c->Code,
                                'id_country' => $c->CountryCode,
                                'status' => (in_array($data->Status, array('Active', 'Modified')) ? 1 : 0),
                            );

                            $table->insert($data);
                        }

                        $row_extend = $tableData->fetchRow($tableData->select()
                                                                     ->where('_id = ?', $c->Code)
                                                                     ->where('lang = ?', mb_strtolower($c->LanguageCode)));

                        if (! $row_extend) {
                            $data_extend = array(
                                '_id' => $c->Code,
                                'lang' => mb_strtolower($c->LanguageCode),
                                'name' => $c->Name,
                                'name_seo' => url($c->Name),
                            );

                            $tableData->insert($data_extend);
                        }

//                         if (is_object($c->GetCitiesResponseAliases)
//                             && isset($c->GetCitiesResponseAliases->GetCitiesResponseAlias)
//                             && is_array($c->GetCitiesResponseAliases->GetCitiesResponseAlias))
//                         {
//                             foreach ($c->GetCitiesResponseAliases->GetCitiesResponseAlias as $d) {
//                                 $row_extend = $tableData->fetchRow($tableData->select()
//                                                                              ->where('_id = ?', $c->Code)
//                                                                              ->where('lang = ?', mb_strtolower($d->LanguageCode)));

//                                 if (! $row_extend) {
//                                     $data_extend = array(
//                                         '_id' => $c->Code,
//                                         'lang' => mb_strtolower($d->LanguageCode),
//                                         'name' => $d->Name,
//                                         'name_seo' => url($d->Name),
//                                     );

//                                     $tableData->insert($data_extend);
//                                 }
//                             }
//                         }
                    }
                }
            }
        }
    }
} else {
    print_a('Empty result set');
}