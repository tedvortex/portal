<?php
$parameters = array_merge(
    $parameters,
    array(
        'Version' => 1,
        'AvailabilityRequestItems' => array(
            'AvailabilityRequestHotel' => array(
                'ItemId' => 1,
                'AvailabilityRequestLocalization' => array(
                    'CityCode' => 257398 // Kemer
                ),
                'CheckInDate' => '2013-04-14',
                'Nights' => 4,
                'OnRequest' => true,
                'AvailabilityRequestRooms' => array(
                    'AvailabilityRequestRoom' => array(
                        'NumberOfRooms' => '1',
                        'Adults' => '2',
                        'Children' => '1',
                        'AvailabilityRequestChildrenAges' => array(
                            'AvailabilityRequestChildrenAge' => '5',
                            'AvailabilityRequestChildrenAge' => '6'
                        ),
                    ),
                ),
            ),
        ),
    )
);

$table = new Zend_Db_Table('xp_search_queries');
$tableItems = new Zend_Db_Table('xp_search_items');

try {
    $result = $Client->Availability($parameters);
} catch (SoapFault $Exception) {
    print_a($Exception);

    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
}

if (is_object($result->AvailabilityResponseItems)) {
    $row = $table->fetchRow($table->select()
                                  ->where('reference = ?', $result->AvailabilityResponseItems->SearchReference));

    if (! $row) {
        $data = array(
            'id_city' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestLocalization']['CityCode'],
            'reference' => $result->AvailabilityResponseItems->SearchReference,
            'nights' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['Nights'],
            'rooms' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['NumberOfRooms'],
            'adults' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Adults'],
            'children' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['AvailabilityRequestRooms']['AvailabilityRequestRoom']['Children'],
            'checkin' => $parameters['AvailabilityRequestItems']['AvailabilityRequestHotel']['CheckInDate'],
        );

        $id_query = $table->insert($data);

        if (is_array($result->AvailabilityResponseItems->AvailabilityResponseHotel)) {
            foreach ($result->AvailabilityResponseItems->AvailabilityResponseHotel as $hotel) {
                $data = array(
                    'id_search_query' => $id_query,
                    'id_item' => $hotel->ItemId,
                    'id_city' => $hotel->CityCode,
                    'id_request' => $hotel->RequestItemId,
                    'id_hotel' => $hotel->HotelCode,
                    'hotel_name' => $hotel->HotelName,
                    'city_name' => $hotel->CityName,
                    'description' => serialize($hotel->AvailabilityResponseHotelRooms)
                );

                $tableItems->insert($data);
            }
        }
    }
}

print_a($result);