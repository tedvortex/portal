<?php
try {
    $result = $Client->GetRoomTypes($parameters);
} catch (SoapFault $Exception) {
    print_a($Exception);

    echo 'Request : <br/><xmp>', $Client->__getLastRequest(), '</xmp><br/><br/>';
    echo 'Response : <br/><xmp>', $Client->__getLastResponse(), '</xmp><br/><br/>';
}

$table = new Zend_Db_Table('xp_room_types');
$tableData = new Zend_Db_Table('xp_room_types_data');

if ($result) {
    if (is_array($result->GetRoomTypesResponseRoomTypes->GetRoomTypesResponseRoomTypes)) {
        foreach ($result->GetRoomTypesResponseRoomTypes->GetRoomTypesResponseRoomTypes as $p) {
            $row = $table->fetchRow($table->select()
                                          ->where('id = ?', $p->Code));

            if (! $row) {
                $data = array(
                    'id' => $p->Code,
                    'status' => ($p->Status == 'Active' ? 1 : 0),
                );

                $table->insert($data);
            }

            $row_extend = $tableData->fetchRow($tableData->select()
                                                         ->where('_id = ?', $p->Code)
                                                         ->where('lang = ?', mb_strtolower($p->LanguageCode)));

            if (! $row_extend) {
                $data_extend = array(
                    '_id' => $p->Code,
                    'lang' => mb_strtolower($p->LanguageCode),
                    'name' => $p->Name,
                    'name_seo' => url($p->Name),
                );

                $tableData->insert($data_extend);
            }

            if (is_object($p->GetRoomTypesResponseAliases)
                && isset($p->GetRoomTypesResponseAliases->GetRoomTypesResponseAliases)
                && is_array($p->GetRoomTypesResponseAliases->GetRoomTypesResponseAliases))
            {
                foreach ($p->GetRoomTypesResponseAliases->GetRoomTypesResponseAliases as $d) {
                    $row_extend = $tableData->fetchRow($tableData->select()
                                                                 ->where('_id = ?', $p->Code)
                                                                 ->where('lang = ?', mb_strtolower($d->LanguageCode)));

                    if (! $row_extend) {
                        $data_extend = array(
                            '_id' => $p->Code,
                            'lang' => mb_strtolower($d->LanguageCode),
                            'name' => $d->Name,
                            'name_seo' => url($d->Name),
                        );

                        $tableData->insert($data_extend);
                    }
                }
            }
        }
    }
} else {
    print_a('Empty result set');
}