<?php
set_time_limit(0);
error_reporting(E_ALL);
//ob_implicit_flush(TRUE);
//ob_end_flush();

header('Content-Type: text/html; charset=UTF-8', true);

define('r', dirname(__FILE__) . '/');
require_once (r . 'includes/init.php');
setlocale(LC_ALL, 'en_GB.UTF-8');

$Zend_Config = new Zend_Config_Ini(r . 'config.ini', 'production');
$db = Zend_Db::factory($Zend_Config->database);

if (! isset($_GET['script'])) {
    $_GET['script'] = '';
}

set_include_path(i . PATH_SEPARATOR . i . 'xml/');

if (is_file(r . 'script/' . $_GET['script'] . '.php')) {
    require_once (r . 'script/script_base.php');

    require_once (r . 'script/' . $_GET['script'] . '.php');
}

exit('finished');