<?php

error_reporting(E_NONE);
require_once(dirname(__FILE__).'/includes/init.php');
//echo dirname(__FILE__);

//if (isset($_GET['status'])) {
//(isset($_GET['code']) ? $code = $_GET['code'] : $code = null);
//(isset($_GET['id']) ? $id = $_GET['id'] : $id = null);
//print uploadStatus($_GET['status'], $code, $id) ;
//die();
//}
if (isset($_GET['token'])) {
	$_SESSION['YT_TOKEN']=$_GET['token'];
}

//if (preg_match('/(?i)MSIE [1-6]/',$_SERVER['HTTP_USER_AGENT']) && !preg_match('/(?i)MSIE [7-8]/',$_SERVER['HTTP_USER_AGENT'])) {
//	header('Location: '.BASEHREF); die();
//}

if (adm_logat()) {
	ob_start();
	$mod=$_GET['mod'];
	if(empty($mod)) $mod='index';
	if (is_loaded($mod)) {
		include(ADMINROOT."/module/".$mod.".php");
	} else {
		print_header();
		?>
		<h1>Acces interzis </h1>
		<p>Nu aveti dreptul sa accesati acest modul</p>
		<?php
		print_footer();
	}
	$file_data = ob_get_contents();
	ob_end_clean();
	//echo str_replace(array("\n","\r","\t"),"",str_replace("  "," ",$file_data));
	echo $file_data;
}
function print_header() {
	global $db,$mod,$config,$alimba,$wproAjax;
	//$totalComenzi=$db->fetch_one("SELECT COUNT(*) FROM `".TABLE_ORDERS."` WHERE status=1");
	//$totalComenzi2=$db->fetch_one("SELECT COUNT(*) FROM `".TABLE_ORDERS."` WHERE status=0");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="robots" content="index,nofollow" />
	<title><?=l('ADMINISTRATOR ZONE').' ('.$config['SITE_NAME'].') - Mod: ('.strip_tags($_GET['mod']).')'?></title>
	<link rel="stylesheet" type="text/css" href="includes/jquery/css/cupertino/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="includes/css/style.php?mod=<?=$mod?>" media="screen" />
<!--	<link rel="stylesheet" type="text/css" href="../includes/jquery/plugins/toChecklist/jquery.toChecklist.css" media="screen">-->
	<link rel="stylesheet" type="text/css" href="includes/jquery/jquery_ui_datepicker/timepicker_plug/css/style.css">
	<script type="text/javascript" src="includes/jquery/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="includes/jquery/jquery.ui.dialog.patch.js"></script>
	<!--<script type="text/javascript" src="includes/jquery/plugins/toChecklist/jquery.toChecklist.js"></script>-->
	<script type="text/javascript" src="includes/tiny/jscripts/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript" src="includes/javascript/javascript.php?mod=<?=$mod?>"></script>
	<script src="includes/jquery/jquery_ui_datepicker/timepicker_plug/timepicker.js" type="text/javascript"></script>

	<script type="text/javascript">
	window.siteurl='<?php echo addslashes($config['SITE_WEB_ROOT']).'admin/'; ?>';
	if (window.location=='<?=str_replace("www." , "" ,addslashes($config['SITE_WEB_ROOT'].'admin/'))?>') {
	window.location=window.siteurl;
	}
	</script>
</head>
<body>
<div id="main">
	<div id="header">
		<div class="logo">
			<a href="index.php" rel="external follow"><img src="images/logo.png" alt="ExpertVision.ro" /></a>
		</div>
		<div class="titleAdmin" style="position:relative;">
			<?=l(ADMINISTRATOR_ZONE)?>
			<div style="position:absolute;font-size:16px;left:250px;width:400px;top:0;">
			  	Aveti <b style="color:red;font-size:18px;font-weight:bold;"><?=$totalComenzi2?></b> <?=($totalComenzi2==1)?'rezervare in asteptare':'rezervari in asteptare'?>
			</div>
		</div>
		<div class="right_menu">
			<ul>
				<li><a href="index.php?action=index" ><img src="images/li_home.png" alt="home" /><?=l(ADMIN_HOME)?></a></li>
				<li><a href="<?=BASEHREF?>" target="_blank"><img src="images/li_site.png" alt="view" /><?=l(ADMIN_VIEW_WEBSITE)?></a></li>
				<li><a href="index.php?action=logout" ><img src="images/li_log.png" alt="logout" /><?=l(ADMIN_LOGOUT)?></a></li>
			</ul>
			<div id="languageDiv" style="height:80px;">
				<div id="flags">
					<?php
					$languages = get_languages ();
					//echo LANG;
					foreach ( $languages as $lang ) {
						if (LANG==$lang['prefix']) {
							$classImg='imgLimbaAct';
						} else {
							$classImg='imgLimba';
						}
					?>
					<a href="index.php?mod=<?=$mod?>&amp;lang=<?=$lang['prefix']?>" title="<?=$lang['name']?>"><img src="images/flags/<?=$lang['prefix']?>.png" width="22" height="16" alt="<?=$lang['name']?>" class="<?=$classImg?>" style="margin-left:5px;"/></a>
					<?}?>
				</div>
				<div class="clear"></div>
			</div>
			<?php
			$admin_info=adm_info();
			?>
			<div id="auth">
				<strong><?=l(AUTENTIFICAT_CA)?></strong> : <?=$admin_info['first_name']." ".$admin_info['last_name']?> <br />(<a href="mailto:<?=$admin_info['email']?>&amp;subject=From%20Owner:%20(<?=SITE_NAME?>)"><?=$admin_info['email']?></a>)
			</div>
		</div>
	</div>
	<?=print_menu()?>
	<div id="content">
	
	
<?php
}
function print_footer() {
?>
		</div>
		<div id="footer">Powered by ExpertVision.ro &copy; 2004-<?=date('Y')?></div>
	</div>
</body>
</html>
<?php } ?>

<?php
function login_form($error) {
	global $config;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><?=l(ADMINISTRATOR_ZONE).' ('.$config['SITE_NAME'].')'?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="index,nofollow" />
<link rel="stylesheet" href="includes/css/.new.css" type="text/css" />
<!--[if lte IE 7]>
<style>
body{overflow-y:visible;}
</style>
<![endif]-->
	<script type="text/javascript">
	window.siteurl='<?php echo addslashes($config['SITE_WEB_ROOT']).'admin/'; ?>';
	if (window.location=='<?=str_replace("www." , "" ,addslashes($config['SITE_WEB_ROOT'].'admin/'))?>') {
	window.location=window.siteurl;
	}
	</script>

</head>
<body>
<img src="images/adminbg.jpg" width="100%" alt="" id="background" />
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" >
	<div id="login">
		<div id="logo">
		<img src="images/logo.png" alt="ExpertVision.ro" />
		</div>
		<div id="header">
			<h1><?=l(ADMINISTRATOR_ZONE)?></h1>
			<a href="<?=BASEHREF?>" target="_blank"><?=$config['SITE_NAME']?></a>
			<input type="hidden" name="action" value="login" />
		</div>
		<div id="error">
			<?=(isset($error)?l($error):"")?>
		</div>
		<div id="flags_login">
			<?php
			$languages = get_languages ();
			//echo LANG;
			foreach ( $languages as $lang ) {
				if (LANG==$lang['prefix']) {
					$classImg='imgLimbaAct';
				} else {
					$classImg='imgLimba';
				}
					?>
					<a href="index.php?mod=<?=$mod?>&amp;lang=<?=$lang['prefix']?>" title="<?=$lang['name']?>"><img src="images/flags/<?=$lang['prefix']?>.png" width="22" height="16" alt="<?=$lang['name']?>" class="<?=$classImg?>" /></a>
			<?}?>
		</div>
		<dl>
			<dt><?=l(ADMIN_USERNAME)?>:</dt>
			<dd><input id="username" type="text" tabindex="1" value="" name="username" /></dd>
			<dt><?=l(ADMIN_PASSWORD)?>:</dt>
			<dd><input id="password" type="password" tabindex="2" value="" name="password" /></dd>
		</dl>
		<p id="log"><?=l(ADMIN_YOUR_IP)?>: <span><?=$_SERVER['REMOTE_ADDR']?></span><br /><?=l(ADMIN_HAS_BEEN_LOGGED_FOR_SECURITY_PURPOSES)?></p>
		<button type="submit" tabindex="3"></button>
	</div>
</form>
</body>
</html>
<?php
}
?>