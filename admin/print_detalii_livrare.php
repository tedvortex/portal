<?php
include(dirname(dirname(__FILE__)).'/admin/includes/init.php');
//require(dirname(dirname(__FILE__)).'/config/config.php');
$info=$db->fetch("SELECT * FROM `".TABLE_ORDERS."` WHERE `id`=$_GET[id]",MYSQL_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Detalii livrare</title>
	<meta name="Description" content="Print packing slip" />
	<meta name="Keywords" content="Print packing slip" />
	<meta name="subjects" content="Print packing slip" />
</head>
<style type="text/css">
body {
	font-family:Verdana;
	font-size:12px;
}
</style>
<body style="margin:0px;padding:0px;" onload="window.resizeTo(684,426);"> 
<div style="margin:10px;">
	<table cellpadding="5" width="100%" cellspacing="5" border="0" style="border:2px solid #CACACA;">
		<tr>
			<td colspan="2" style="padding:5px;background-color:#000000;color:#ffffff;font-weight:bold;font-size:15px;">
				Detalii livrare pentru comanda #<?=$info['no_order']?>
			</td>
		</tr>
		<tr>
			<td width="50%">
				<div style="font-size:14px;font-weight:bold;padding:5px;">Destinatar</div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Nume:</b> <?=ucfirst($info['buyer_last_name']).' '.ucfirst($info['buyer_first_name'])?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Adresa:</b> <?=$info['buyer_address']?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Oras:</b> <?=ucfirst($info['buyer_city'])?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Judet:</b> <?=ucfirst($info['buyer_state'])?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Telefon:</b> <?=$info['buyer_phone']?></div>
				<? if ($info['shipping_address']) {?>
				<div style="font-size:14px;font-weight:bold;padding:5px;">Adresa livrare</div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Adresa:</b> <?=$info['shipping_address']?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Oras:</b> <?=ucfirst($info['shipping_city'])?></div>
				<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Judet:</b> <?=ucfirst($info['shipping_state'])?></div>
				<?}?>
			<!--	<div style="padding-left:5px;"><b style="width:80px;display:inline-block;">Tara:</b> Romania</div>-->
			</td>
		</tr>
		<tr>
			<td>
<!--				<div>
					<span style="padding-left:5px;font-weight:bold;width:80px;display:inline-block;">Comanda:</span> #<?=$info['no_order']?>
				</div>-->
				<div>
					<span style="padding-left:5px;font-weight:bold;width:80px;display:inline-block;">Livrare:</span> Ramburs
				</div>
				<div>
					<span style="padding-left:5px;font-weight:bold;width:80px;display:inline-block;">Data:</span> <?=date('d-m-Y',$info['date'])?>
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
</html>