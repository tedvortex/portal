<?php
//define('FPDF_FONTPATH','tcpdf/fonts/');
//echo FPDF_FONTPATH;
$ef=array(); //etichete de la box furnizori
$tf=array();//texte de la box furnizori
function ccc($str) {
	return $str;
	//return iconv('UTF-8', 'windows-1252//TRANSLIT', $str);
}
include('../admin/includes/tcpdf/config/lang/eng.php');
require('../admin/includes/tcpdf/tcpdf.php');
include('../admin/includes/invoice.php');
//echo $_GET['id_order'];
//print_r($invoiceConfig);
//die('dd');
class PDF extends TCPDF{
	public function __constructor($orientation='P',$unit='mm',$format='A4')
	{
		//Call parent constructor
		base::__constructor($orientation,$unit,$format);
		//Initialization
	}

	function WriteHTML_1($html)
	{
		//HTML parser
		$html=str_replace("\n",' ',$html);
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				//Text
				if($this->HREF)
				$this->PutLink($this->HREF,$e);
				else
				$this->Write(4,$e);
			}
			else
			{
				//Tag
				if($e{0}=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
				else
				{
					//Extract attributes
					$a2=explode(' ',$e);
					$tag=strtoupper(array_shift($a2));
					$attr=array();
					foreach($a2 as $v)
					if(ereg('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
					$attr[strtoupper($a3[1])]=$a3[2];
					$this->OpenTag($tag,$attr);
				}
			}
		}
	}

	function OpenTag($tag,$attr)
	{
		//Opening tag
		if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,true);
		if($tag=='A')
		$this->HREF=$attr['HREF'];
		if($tag=='BR')
		{$this->Ln(4);$this->setX(40);}
	}

	function CloseTag($tag)
	{
		//Closing tag
		if($tag=='B' or $tag=='I' or $tag=='U')
		$this->SetStyle($tag,false);
		if($tag=='A')
		$this->HREF='';
	}

	function SetStyle($tag,$enable)
	{
		//Modify style and select corresponding font
		$this->$tag+=($enable ? 1 : -1);
		$style='';
		foreach(array('B','I','U') as $s)
		if($this->$s>0)
		$style.=$s;
		$this->SetFont('',$style);
	}

	function PutLink($URL,$txt)
	{
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}
	function Header() {
		global $tipareste_header,$invoiceConfig,$config,$infoOrder,$facturainfo,$discount;
		if (!$tipareste_header) return false;
		$pdf=$this;
		$pdf->setXY(7,7);
		//if ($invoiceConfig['logo_small']!='') {
			$pdf->image('../static/images/logo.jpg',7,7,25);
		//}
		// ---- informatii despre furnizor (panoul stanga sus) ----
		$pdf->setXY(7,30);
		$pdf->SetFont('freeserif','',8);
		$pdf->cell(12,0,'Furnizor: ',0,0,'L');
		$pdf->SetFont('freeserif','B',8);
		$pdf->cell(50,0,ccc($invoiceConfig['name']),0,0,'L');
		$pdf->Ln(4.5);
		$pdf->SetFont('freeserif','',8);
		$pdf->cell(70,0,'C.U.I.: '.ccc($invoiceConfig['cui']),0,0,'L');$pdf->Ln(3.5);
		$pdf->cell(70,0,'Nr. ord. Reg. Com.: '.ccc($invoiceConfig['nr_reg_com']),0,0,'L');$pdf->Ln(3.5);
		$mcx=$pdf->GetStringWidth('Sediul: ');
		$pdf->cell(70,0,'Sediul: ',0,0,'L');
		$pdf->SetY($pdf->getY()-1);
		$pdf->SetX($mcx+8);
		$pdf->multicell(70-$mcx-8,3.5,ccc(ucfirst($invoiceConfig['address'])),0,'L');$pdf->Ln(2);
		$maxY=$pdf->getY();
		$pdf->cell(70,0,'IBAN: '.ccc($invoiceConfig['account']),0,0,'L');$pdf->Ln(3.5);
		$pdf->cell(70,0,'Banca: '.ccc($invoiceConfig['bank']),0,0,'L');$pdf->Ln(3.5);
		$pdf->cell(70,0,'Tel.: '.$invoiceConfig['phone'],0,0,'L');$pdf->Ln(3.5);
		$pdf->cell(70,0,'Fax: '.$invoiceConfig['fax'],0,0,'L');$pdf->Ln(3.5);
		$pdf->cell(70,0,'Email: '.$invoiceConfig['email'],0,0,'L');$pdf->Ln(3.5);

		// ---- informatii despre cumparator (panoul dreapta sus) ----
		$pdf->setXY(140,12);
		$pdf->SetFont('freeserif','',14);
		$pdf->cell(70,0,'Numar: DF '.$facturainfo['nr_factura'],0,0,'L');$pdf->Ln(8);
		$pdf->SetFont('freeserif','',18);
		$pdf->setX(70);
		$pdf->cell(70,0,strtoupper($facturainfo['tipf']),0,0,'L');
		$pdf->setXY(140,30);
		$pdf->SetFont('freeserif','',8);
		if ($infoOrder['buyer_type']==0) {
			$mcx1=$pdf->GetStringWidth('Cumparator: ');
			$pdf->cell(70,0,'Cumparator: ',0,0,'L');
			$pdf->SetFont('freeserif','B',0);
			$pdf->SetY($pdf->getY());
			$diffY=$pdf->getY()+3.5+2;
			$pdf->SetX(141+$mcx1);
			$pdf->multicell(80-$mcx1,3.5,ccc(ucfirst($infoOrder['buyer_last_name']).' '.ucfirst($infoOrder['buyer_first_name'])),0,'L');$pdf->Ln(2);
			$diffY-=$pdf->getY();
			$pdf->SetFont('freeserif','',8);
			$pdf->setX(140);
			$pdf->cell(35,0,'CNP: '.ccc($infoOrder['buyer_cnp']),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,('Nr. ord. Reg. Com.: '.ccc('')),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Adresa: '.ccc($infoOrder['buyer_address']),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Oras: '.ccc($infoOrder['buyer_city']),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Judet/Sector: '.ccc($infoOrder['buyer_state']),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'IBAN: '.ccc(''),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Banca: '.ccc(''),0,0,'L');$pdf->Ln(3.5);
		} else {
			$mcx1=$pdf->GetStringWidth('Cumparator: ');
			$pdf->cell(70,0,'Cumparator: ',0,0,'L');
			$pdf->SetFont('freeserif','B',0);
			$pdf->SetY($pdf->getY()-1);
			$diffY=$pdf->getY()+3.5+2;
			$pdf->SetX(140+$mcx1);
			$pdf->multicell(80-$mcx1,3.5,ccc($infoOrder['company']),0,'L');$pdf->Ln(2);
			$diffY-=$pdf->getY();
			$pdf->SetFont('freeserif','',8);
			$pdf->setX(140);
			$pdf->cell(35,0,'CNP: '.ccc(''),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,('Nr. ord. Reg. Com.: '.ccc($infoOrder['company_nr_reg_com'])),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Adresa: '.ccc(ucfirst($infoOrder['company_address'])),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Oras: '.ccc(ucfirst($infoOrder['company_city'])),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Judet/Sector: '.ccc(ucfirst($infoOrder['company_state'])),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'IBAN: '.ccc($infoOrder['company_bank_account']),0,0,'L');$pdf->Ln(3.5);
			$pdf->setX(140);
			$pdf->cell(35,0,'Banca: '.ccc(ucfirst($infoOrder['company_bank'])),0,0,'L');$pdf->Ln(3.5);
		}
		$pdf->rect(80,60,50,20);
		$pdf->setXY(82,64);
		$pdf->cell($pdf->getStringWidth('Nr. facturii: '),0,'Nr. facturii: ',0,0,'L');
		$pdf->SetFont('freeserif','B',8);
		$pdf->cell(50,0,(string)$facturainfo['nr_factura'],0,0,'L');
		$pdf->SetFont('freeserif','',8);
		$pdf->Ln(4.5);
		$pdf->setX(82);
		$pdf->cell($pdf->getstringWIdth('Data (ziua, luna, anul): '),0,'Data (ziua, luna, anul): ',0,0,'L');
		$pdf->SetFont('freeserif','B',8);
		$pdf->cell(50,0,$facturainfo['data'],0,0,'L');$pdf->Ln(4.5);
		$pdf->SetFont('freeserif','',8);
		$pdf->setX(82);
		$pdf->cell($pdf->getstringwidth('Nr. aviz insotire a marfii: '),0,'Nr. aviz insotire a marfii: ',0,0,'L');
		$pdf->SetFont('freeserif','B',8);
		$pdf->cell(50,0,$facturainfo['nr_aviz'],0,0,'L');$pdf->Ln(4);
		$pdf->SetFont('freeserif','',8);

		$pdf->setX(82);
		$pdf->SetFont('freeserif','',7);
		$pdf->cell(46,0,'(daca este cazul)',0,0,'C');
		$pdf->setXY(7,78);
		$pdf->SetFont('freeserif','',9);
		$pdf->cell(60,0,'Cota TVA: 24%',0,0,'L');

		$pdf->setXY(7,84);

		$startY=$this->getY();
		$this->SetFont('freeserif','',8);
		$this->cCell(7, $startY, 10, 10, 'Nr.|crt.',1,1,'C');
		$this->cCell(17, $startY, 70, 10, 'Denumirea produselor|sau a serviciilor',1,1,'C');
		$this->cCell(87, $startY, 10, 10, 'U.M.',1,1,'C');
		$this->cCell(97, $startY, 22, 10, 'Cantitatea',1,1,'C');
		$this->cCell(119, $startY, 27, 10, 'Pretul unitar|(fara T.V.A.)|- lei -',1,1,'C');
		$this->cCell(146, $startY, 27, 10, 'Valoarea|- lei -',1,1,'C');
		$this->cCell(173, $startY, 27, 10, 'Valoarea|T.V.A.|- lei -',1,1,'C');
		$startY+=10;
		$this->cCell(7, $startY, 10, 4, '0',1,1,'C');
		$this->cCell(17, $startY, 70, 4, '1',1,1,'C');
		$this->cCell(87, $startY, 10, 4, '2',1,1,'C');
		$this->cCell(97, $startY, 22, 4, '3',1,1,'C');
		$this->cCell(119, $startY, 27, 4, '4',1,1,'C');
		$this->cCell(146, $startY, 27, 4, '5(3x4)',1,1,'C');
		$this->cCell(173, $startY, 27, 4, '6',1,1,'C');
		global $tb_start_rows;$tb_start_rows=$startY+4;
		$this->setXY(80,$tb_start_rows+50);

		if ($invoiceConfig['logo_big']!='') {
			$this->image($invoiceConfig['logo_big'],40,$tb_start_rows+50,130);
		}
		$this->SetTextColor(130,130,130);
		$this->SetFont('freeserif','',20);
		$this->setXY(40,$tb_start_rows+90);
		$this->setXY(80,$tb_start_rows+50);
		$this->SetFont('freeserif','',8);
		$this->SetTextColor(0,0,0);
	}

	//Page footer
	function Footer(){
		//   global $fuckingdisablefooter;
		//	if ($fuckingdisablefooter) return;
		//$this->InFooter=false;

		//========= chitanta
		//$e_chitanta=true;
		global $tb_start_rows, $tbl_gata, $facturainfo, $totalFaraTva,$totalTva,$h,$infoOrder,$discount;
		$h=290-44-10.5-$tb_start_rows;
		if ($tbl_gata) {
			if ($e_chitanta) {
				$h=200-$tb_start_rows;
			}
		}


		$this->rect(7, $tb_start_rows, 10, $h);
		$this->rect(17, $tb_start_rows, 70, $h);
		$this->rect(87, $tb_start_rows, 10, $h);
		$this->rect(97, $tb_start_rows, 22, $h);
		$this->rect(119, $tb_start_rows, 27, $h);
		$this->rect(146, $tb_start_rows, 27, $h);
		$this->rect(173, $tb_start_rows, 27, $h);
		if ($tbl_gata) {
			$pdf=$this;

			$startY=$tb_start_rows+$h;
			if ($e_chitanta) {
				$startY2=$startY+54.5;
				$this->rect(7, $startY2, 193, 34);
				$this->line(96, $startY2, 96, $startY2+34);

				$pdf->SetFont('freeserif','',9);

				$htmlcontent2='
				<table border="1">
				<tr>
				<td width="40">Data </td>
				<td> = </td>
				</tr>
				<tr>
				<td  width="40"> Seria </td>
				<td> = </td>
				</tr>
				<tr>
				<td  width="40"> Numarul </td>
				<td> = </td>
				</tr>
				<tr>
				<td  width="40"> Furnizor </td>
				<td> = </td>  
				</tr>
				</table>
				';
				$pdf->writeHTMLCell(100,40,8, $startY2+3,$htmlcontent2, 0, 0, 0);
				//	$pdf->writeHTML($htmlcontent2, true, 0, true, 0);


				$htmlcontent3='
				<table  border="1">
				<tr>
				<td  width="50"> Adresa </td>
				<td>  = = = == = = == = 
				= == = = = == = = == = = = == = = = = = == = = = = == = 
				= == = = = == = = == = = = == = = = = = == = = = = == = 
				= == = = = == = = == = = = == = = = = = == = = = = == = 
				= </td>
				</tr>
				<tr>
				<td  width="50"> Suma </td>
				<td> = </td>
				</tr>
				<tr>
				<td  width="60"> Reprezentand </td>
				<td> = </td>
				</tr>
				</table>
				';
				$pdf->writeHTMLCell(140,40,98, $startY2+3,$htmlcontent3, 0, 0, 0);

			}
			$pdf->rect(7, $startY, 193, 17.5);
			$pdf->SetFont('freeserif','',7.5);
			$pdf->setXY(8, $startY+0.7);
			$pdf->multicell(191,3,ccc($facturainfo['msg']),0,'L');
			$startY=$startY+10.5;
			$pdf->rect(7, $startY+7, 193, 37);
			$pdf->line(39, $startY+7, 39, $startY+7+37);
			$pdf->line(119, $startY+7, 119, $startY+7+37);
			$pdf->SetFont('freeserif','',9);
			$pdf->setXY(8, $startY+9);
			$pdf->cell(10,3,'Aceasta factura circula',0,0,'L');$pdf->ln(5);
			$pdf->setX(8);
			$pdf->cell(10,3,'fara stampila si',0,0,'L');$pdf->ln(5);
			$pdf->setX(8);
			$pdf->cell(10,3,'semnatura conform',0,0,'L');$pdf->ln(5);
			$pdf->setX(8);
			$pdf->cell(10,3,'legii 571/2003',0,0,'L');$pdf->ln(5);
			$pdf->setX(8);
			$pdf->cell(10,3,'privind codul fiscal',0,0,'L');$pdf->ln(5);
			$pdf->setX(8);
			$pdf->cell(10,3,'articolul 155(6)',0,0,'L');$pdf->ln(5);
			// date privind expeditia
			$pdf->setXY(40, $startY+8);
			$pdf->SetFont('freeserif','',8.7);
			$pdf->writeHTMLCell(100,100,40,$startY+8,'Date privind expeditia <br /> Numele delegatului: <b>'.ccc($facturainfo['nume_delegat']).'</b><br>BI/CI seria <b>'.$facturainfo['bici_seria'].'</b> nr. <b>'.$facturainfo['bici_nr'].'</b> eliberat(a) <b>'.$facturainfo['bici_eliberat'].'</b><br>Mijlocul de transport <b>'.ccc($facturainfo['mjt']).'</b> nr. <b>'.$facturainfo['mjnr'].'</b><br>Expedierea s-a efectuat in prezenta noastra<br>la data de <b>'.$facturainfo['data'].'</b> ora <b>'.date('h:i',time()).'</b><br>Semnaturile: ', 0, 0, 0);
			$pdf->ln(4);
			$pdf->line(146, $startY+7, 146, $startY+7+37);
			$pdf->line(119, $startY+25, 200, $startY+25);
			$pdf->line(146, $startY+16, 200, $startY+16);
			$pdf->line(173, $startY+7, 173, $startY+25);
			$pdf->setXY(119+3,$startY+9);
			$pdf->multicell(20,4,'Total
din care:
accize',0,'L');
			$pdf->setXY(119+3,$startY+27);
			$pdf->multicell(20,4,'Semnatura
de primire',0,'L');
			$pdf->setXY(146+2,$startY+31);
			$pdf->multicell(20,4,'Total de plata
(col 5 + col 6)',0,'L');
			$pdf->setXY(146+2+27,$startY+33);
			$pdf->SetFont('freeserif','B',11);
			$pdf->cell(24,4,number_format($totalFaraTva+$totalTva,2,',','.').' lei',0,0,'C');
			$pdf->setXY(146+2,$startY+7+2);
			$pdf->SetFont('freeserif','',8.7);
			$pdf->cell(24,4,number_format($totalFaraTva,2,',','.'),0,0,'C');
			$pdf->setXY(146+2+27,$startY+7+2);
			$pdf->cell(24,4,number_format($totalTva,2,',','.'),0,0,'C');
			$pdf->setXY(146+2+27,$startY+19);
			$pdf->cell(24,4,'X',0,0,'C');


		}


		///====

		$pdf=$this;

		$startY=$tb_start_rows+$h;
		$pdf->rect(7, $startY, 193, 17.5);
		$pdf->SetFont('freeserif','',7.5);
		$pdf->setXY(8, $startY+0.7);
		$pdf->multicell(191,3,ccc($facturainfo['msg']),0,'L');
		$startY=$startY+10.5;
		$pdf->rect(7, $startY+7, 193, 37);
		$pdf->line(39, $startY+7, 39, $startY+7+37);
		$pdf->line(119, $startY+7, 119, $startY+7+37);
		$pdf->SetFont('freeserif','',9);
		$pdf->setXY(8, $startY+9);
		//de aici am sters faza cu stampila si semnautra
		$pdf->cell(10,3,'',0,0,'L');$pdf->ln(5);
		$pdf->setX(8);
		$pdf->cell(10,3,'',0,0,'L');$pdf->ln(5);
		$pdf->setX(8);
		$pdf->cell(10,3,'',0,0,'L');$pdf->ln(5);
		// date privind expeditia
		$pdf->setXY(40, $startY+8);
		$pdf->SetFont('freeserif','',8.7);
		$pdf->writeHTMLCell(100,100,40,$startY+8,'Date privind expeditia  <br /> Numele delegatului: <b>'.ccc($facturainfo['nume_delegat']).'</b><br>BI/CI seria <b>'.$facturainfo['bici_seria'].'</b> nr. <b>'.$facturainfo['bici_nr'].'</b> eliberat(a) <b>'.$facturainfo['bici_eliberat'].'</b><br>Mijlocul de transport <b>'.ccc($facturainfo['mjt']).'</b> nr. <b>'.$facturainfo['mjnr'].'</b><br>Expedierea s-a efectuat in prezenta noastra<br>la data de <b>'.$facturainfo['data'].'</b> ora <b>'.date('h:i',time()).'</b><br>Semnaturile: ', 0, 0, 0);$pdf->ln(4);
		$pdf->line(146, $startY+7, 146, $startY+7+37);
		$pdf->line(119, $startY+25, 200, $startY+25);
		$pdf->line(146, $startY+16, 200, $startY+16);
		$pdf->line(173, $startY+7, 173, $startY+25);
		$pdf->setXY(119+3,$startY+9);
		$pdf->multicell(20,4,'Total
din care:
accize',0,'L');
		$pdf->setXY(119+3,$startY+27);
		$pdf->multicell(20,4,'Semnatura
de primire',0,'L');
		$pdf->setXY(146+2,$startY+31);
		$pdf->multicell(20,4,'Total de plata
(col 5 + col 6)',0,'L');
		$pdf->setXY(146+2+27,$startY+33);
		$pdf->SetFont('freeserif','B',9);
		//	$pdf->cell(24,4,number_format($plata1+$plata2,2,',','.').' lei',0,0,'C');
		$pdf->setXY(146+2,$startY+7+2);
		$pdf->SetFont('freeserif','',8.7);
		//	$pdf->cell(24,4,number_format($plata1,2,',','.'),0,0,'C');
		$pdf->setXY(146+2+27,$startY+7+2);
		//	$pdf->cell(24,4,number_format($plata2,2,',','.'),0,0,'C');
		$pdf->setXY(146+2+27,$startY+19);
		//	$pdf->cell(24,4,'X',0,0,'C');

		///====



		//$this->InFooter=true;
	}
	function cCell($x=0,$y=0,$w=0,$h=0,$text='test',$ds=3,$dj=1,$aliniere='C'){
		$vlines=explode('|',$text);
		$nl=sizeof($vlines);
		$lh=3;
		$yl=$y+$h/2-$nl*$lh/2;
		$this->Rect($x,$y,$w,$h);
		for($i=0;$i<$nl;$i++){
			$yll=$yl+$lh*$i;
			$this->SetXY($x,$yll);
			$this->Cell($w,$lh,$vlines[$i],0,1,$aliniere);

		}
	}
	function nbCell($x=0,$y=0,$w=0,$h=0,$text='test',$aliniere='C') {
		if ($aliniere=='L') {
			$this->SetXY($x+1,$y);
			//$this->Cell($w-1,$h,$text,0,0,$aliniere);
			$this->multiCell($w-2,$h,$text,0,$aliniere);
			//$this->SetXY($x,$y);
			//$this->Cell($w,$h,$text,0,0,$aliniere);
		}
		elseif ($aliniere=='R') {
			$this->SetXY($x,$y);
			$this->Cell($w-1,$h,$text,0,0,$aliniere);
		} elseif ($aliniere=='J') {
			$this->SetXY($x+1,$y);
			$this->multiCell($w-2,$h,$text,0,$aliniere);
		} else {$this->SetXY($x,$y);$this->Cell($w,$h,$text,0,0,$aliniere);}
	}
	function eCell($x=0,$y=0,$w=0,$h=0,$eticheta,$text,$font=10,$lh=4,$proprietatifont='', $aliniere='L',$w1=0,$w2=0){
		$rez=$y;
		$text=trim($text);
		$eticheta=trim($eticheta);
		$lungEthicheta=(int) (strlen($eticheta))?($w1)?$w1:$this->GetStringWidth($eticheta)+1:0;
		$lungText=(int)  (strlen($text))?($w2)?$w2:$this->GetStringWidth($text):0;
		if($lungText+$lungEthicheta>$w){
			$nl=(int) ceil(($lungText+$lungEthicheta)/$w);
		}
		else{
			$nl=1;
		}
		$this->SetFont('freeserif',$proprietatifont,$font);
		$this->SetXY($x,$y);
		if($nl>1){
			if($h==0){
				$h=$nl*$lh;
			}
			$this->MultiCell($lungEthicheta,$lh,$eticheta,0,$aliniere);
			$this->SetXY($x+$lungEthicheta,$y);
			$this->MultiCell($w-$lungEthicheta,$lh,$text,0,$aliniere);
			$rez=$this->GetY();
		}
		else{
			$this->MultiCell($w,$lh,$eticheta.' '.$text,0,$aliniere);
		}
		$rez=$this->GetY();
		return $rez;
	}
	function dotline($x1,$x2,$y){
		for($x=$x1;$x<=$x2;$x+=0.5){
			$this->Line($x,$y,$x+0.1,$y);
		}
	}

	function uMC($w,$h,$text,$aliniere='C'){
		$this->MultiCell($w,$h,$text,0,$aliniere);
		$rez=$this->GetY();
		return $rez;
	}

}
////////////////////////////////////////////
// milimetri
//set general
$w=210-$stanga-$dreapta; // latimea facturii
$h=297-$top-$jos; // inaltimea facturii
$lh=4; //inaltime linie pentru font 10
//set header
$hhf=80-$top; //inaltime headerul facturii
$wf=70-$stanga; //latime furnizor
$sl=$stanga+$wf;//stanga logo
$wlgif=65;// width logogif
$hlgif=40; //inaltime logo gif
$sc=$sl+$wlgif; //stanga cumparator
$wc=$w-$wf-$wlgif;

$tipareste_header=false;

$pdf=new PDF('P','mm','A4');
$pdf->SetMargins(7,7,7);
$pdf->SetAutoPageBreak(false, 10);
$pdf->AddPage();
$tipareste_header=true;
//culoarea facturii
$pdf->SetDrawColor(0,0,0);
$pdf->SetTextColor(0,0,0);

//headerul facturii

//furnizor
$pozX=$stanga+1;
$pozY=$top+2;//pozitia returnata de eCell
$wfi=$wf-3;//width interior furnizor
$hhfi=$hhf-4;//height interior furnizor
//$pdf->Rect($stanga+2,$top+2,$wfi,$hhfi);

$pdf->header();
$i=0;
$startY=$tb_start_rows+1;
$totalFaraTva=0; $totalTva=0;
foreach ($orderProducts as $prodinfo) {$i++;
$pdf->SetFont('dejavusansmono','',8);
$pdf->nbCell(7, $startY, 10, 4, $i.'.','C');
$pdf->nbCell(17, $startY, 70, 3, ccc($prodinfo['name']),'L');
$pdf->setY($pdf->getY()+1);
$startynou=$pdf->getY();
$pdf->nbCell(87, $startY, 10, 4, 'BUC','C');
$pdf->nbCell(97, $startY, 22, 4, $prodinfo['quantity'],'C');
//$pret_fara_tva=$prodinfo['pret']/(1+$facturainfo['tva']/100);
//if ($prodinfo['promotion_price']>0) {
//	$pret_fara_tva=$prodinfo['promotion_price']/1;
//} else {
	$pret_fara_tva=$prodinfo['price']/1.24;
//}
$pdf->nbCell(119, $startY, 27, 4, number_format($pret_fara_tva, 2, ',', '.'),'C');
$pdf->nbCell(146, $startY, 27, 4, number_format($pret_fara_tva*$prodinfo['quantity'], 2, ',', '.'),'C');
$pdf->nbCell(173, $startY, 27, 4, number_format($pret_fara_tva*$prodinfo['quantity']*0.24, 2, ',', '.'),'C');
$totalFaraTva+=$pret_fara_tva*$prodinfo['quantity'];
$totalTva+=$pret_fara_tva*$prodinfo['quantity']*0.24;
$startY=$startynou;
if ($startY>220) {$pdf->addpage();$startY=$tb_start_rows+1;}
}

if ($discount>0) {
	$i++;
	$pdf->SetFont('dejavusansmono','',8);
	$pdf->nbCell(7, $startY, 10, 4, $i.'.','C');
	$pdf->nbCell(17, $startY, 70, 3, ccc('Discount '.$discount.'%'),'L');
	$pdf->setY($pdf->getY()+1);
	$startynou=$pdf->getY();
	$pdf->nbCell(87, $startY, 10, 4, 'BUC','C');
	$pdf->nbCell(97, $startY, 22, 4, 1,'C');
	//$pret_fara_tva=$prodinfo['pret']/(1+$facturainfo['tva']/100);
//	if ($prodinfo['promotion_price']>0) {
//		$pret_fara_tva=$prodinfo['promotion_price']/1;
//	} else {
//		$pret_fara_tva=$prodinfo['price']/1;
//	}
	$pdf->nbCell(119, $startY, 27, 4, '-'.number_format(($totalFaraTva*$discount/100), 2, ',', '.'),'C');
	$pdf->nbCell(146, $startY, 27, 4, '-'.number_format(($totalFaraTva*$discount/100), 2, ',', '.'),'C');
	$pdf->nbCell(173, $startY, 27, 4, '-'.number_format(($totalFaraTva*$discount/100)*0.24, 2, ',', '.'),'C');
	$totalFaraTva-=($totalFaraTva*$discount/100);
	//$tvadisc=number_format(($totalFaraTva*$discount/100)*0.24,2);
	$totalTva=$totalFaraTva*0.24;
}
//if ($infoOrder['handling_fee']>0) {
//	$i++;
//	$pdf->SetFont('dejavusansmono','',8);
//	$pdf->nbCell(7, $startY, 10, 4, $i.'.','C');
//	$pdf->nbCell(17, $startY, 70, 3, ccc('Taxa ramburs'),'L');
//	$pdf->setY($pdf->getY()+1);
//	$startynou=$pdf->getY();
//	$pdf->nbCell(87, $startY, 10, 4, 'BUC','C');
//	$pdf->nbCell(97, $startY, 22, 4, 1,'C');
//	$pdf->nbCell(119, $startY, 27, 4, number_format($infoOrder['handling_fee']/1, 2, ',', '.'),'C');
//	$pdf->nbCell(146, $startY, 27, 4, number_format($infoOrder['handling_fee']/1, 2, ',', '.'),'C');
//	$pdf->nbCell(173, $startY, 27, 4, number_format($infoOrder['handling_fee']/1*0, 2, ',', '.'),'C');
//	$startY=$startynou;
//	$totalFaraTva+=$infoOrder['handling_fee']/1;
//	$totalTva+=$infoOrder['handling_fee']/1*0;
//}
//if ($infoOrder['shipping_tax']>0) {
//	$i++;
//	$pdf->SetFont('dejavusansmono','',8);
//	$pdf->nbCell(7, $startY, 10, 4, $i.'.','C');
//	$pdf->nbCell(17, $startY, 70, 3, ccc('Taxa transport'),'L');
//	$pdf->setY($pdf->getY()+1);
//	$startynou=$pdf->getY();
//	$pdf->nbCell(87, $startY, 10, 4, 'BUC','C');
//	$pdf->nbCell(97, $startY, 22, 4, 1,'C');
//	$pdf->nbCell(119, $startY, 27, 4, number_format($infoOrder['shipping_tax']/1, 2, ',', '.'),'C');
//	$pdf->nbCell(146, $startY, 27, 4, number_format($infoOrder['shipping_tax']/1, 2, ',', '.'),'C');
//	$pdf->nbCell(173, $startY, 27, 4, number_format($infoOrder['shipping_tax']/1*0, 2, ',', '.'),'C');
//	$startY=$startynou;
//	$totalFaraTva+=$infoOrder['shipping_tax']/1;
//	$totalTva+=$infoOrder['shipping_tax']/1*0;
//}
if ($startY>220) {$pdf->addpage();$startY=$tb_start_rows+1;}
$tbl_gata=true;
//$pdf->Footer();
//$fuckingdisablefooter=true;

$pdf->SetAutoPageBreak(false,1);
//tipareste footerul




//	$pdf->Output('factura.pdf','S');
//$pdf->Output("factura.pdf", "I", "I");

$pdfout=$pdf->Output('factura.pdf','S');

header("Content-Description: File Transfer");
header('Content-Type: application/pdf; charset="utf-8"',true);
header("Content-Disposition: attachment; filename=factura-".$facturainfo['nr_factura']."-din-".$facturainfo['data'].".pdf;");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".strlen($pdfout));
header('Cache-Control: maxage=3600');
header('Pragma: public');
die($pdfout);


?>
