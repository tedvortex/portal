<?php
$f=$_GET['folder'].'/'.$_GET['filename'];
$w=$_GET['dimension'];
//exit(print_r($_GET));

if(
	file_exists($f) &&
	is_dir(dirname(__FILE__).'/'.$_GET['folder'].'/'.$_GET['dimension']) &&
	is_writable(dirname(__FILE__).'/'.$_GET['folder'].'/'.$_GET['dimension'].'/')
){
	list($width,$height,$ftype)=getimagesize($f);

	switch($ftype){
		case IMAGETYPE_GIF:
			$image=@imagecreatefromgif($f);
			$ext='gif';
			break;

		case IMAGETYPE_JPEG:
			$image=@imagecreatefromjpeg($f);
			$ext='jpg';
			break;

		case IMAGETYPE_PNG:
			$image=@imagecreatefrompng($f);
			$ext='png';
			break;
	}

	if(
		$_GET['folder']=='i/templates' ||
		$_GET['folder']=='i/imagini'
	){
		if(
			$width > $height
		){
			$percent=$w/$width;
		}else{
			$percent=$w/$height;
		}
	}else{
		$percent=$w/$width;
	}
	

	$new_width=$width*$percent;
	$new_height=$height*$percent;


	/*create tranparent img*/
	if($image_p=imagecreatetruecolor($new_width,$new_height)){
		imagealphablending($image_p,false);
		imagesavealpha($image_p,true);
		$transparent=imagecolorallocatealpha($image_p,255,255,255,127);
		imagefilledrectangle($image_p,0,0,$new_width,$new_height,$transparent);
		/*end create transparent img*/

		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		$expires=60*60*24*14;
		header('Pragma: public');
		header('Cache-Control: maxage='.$expires);
		header('Expires: '.gmdate('D, d M Y H:i:s',time()+$expires).' GMT');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s',time()).' GMT');
		header('Content-type: image/'.$ext);


		$nf=$_GET['folder'].'/'.$_GET['dimension'].'/'.$_GET['filename'];

		switch($ftype){
			case IMAGETYPE_GIF:
				imagegif($image_p,dirname(__FILE__)."/".$nf);
				imagegif($image_p);
				break;

			case IMAGETYPE_JPEG:
				imagejpeg($image_p,dirname(__FILE__)."/".$nf,100);
				imagejpeg($image_p);
				break;

			case IMAGETYPE_PNG:
				imagepng($image_p,dirname(__FILE__)."/".$nf,9);
				imagepng($image_p);
				break;
		}

		imagedestroy($image_p);
	}
}

elseif(
	file_exists($f)
){
	exit(header('Location: http://'.$_SERVER['HTTP_HOST'].'/'.$f));
}
?>