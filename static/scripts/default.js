var lang = document.getElementsByTagName('html')[0].getAttribute('lang');

require.config({
	paths : {
		'lightbox' : 'modules/jquery.lightbox.evolution.1.7.1.min',
		'cycle' : 'modules/jquery.cycle.2.9997.min',
		'chosen' : 'modules/jquery.chosen.0.9.11.min',
		'tooltip' : 'modules/jquery.tooltipster.2.1.min',
		'jgrowl': 'modules/jquery.jgrowl_google',
		'tinyscroll': 'modules/jquery.tinyscrollbar.v1.8.min',
		'sudoslider': 'modules/jquery.sudoSlider.min',
		'window' : 'modules/ev.window',
		'cart': 'modules/ev.cart',
		'bootstrap': 'modules/bootstrap.min',
		'cute-slider': 'cute/cute.slider',
		'cute-transitions': 'cute/cute.transitions.all',
		'datepick-ext': 'modules/jquery.datepick.ext.4.0.6.min',
		'modernizr': 'modules/modernizr-2.6.2.min',
		'php': 'modules/ev.php',
		'products': 'pages/products'
	},
	waitSeconds : 5,
	locale : "ro-ro"
});

function _g($message,$header){
	if(
		typeof($header)=='undefined'
	){
		var d = new Date();

		var weekday=new Array(7);
		weekday[0]="Duminica";
		weekday[1]="Luni";
		weekday[2]="Marti";
		weekday[3]="Miercuri";
		weekday[4]="Joi";
		weekday[5]="Vineri";
		weekday[6]="Sambata";

		var month=new Array(12);
		month[0]="Ianuarie";
		month[1]="Februarie";
		month[2]="Martie";
		month[3]="Aprilie";
		month[4]="Mai";
		month[5]="Iunie";
		month[6]="Iulie";
		month[7]="August";
		month[8]="Septembrie";
		month[9]="Octoberie";
		month[10]="Noiembrie";
		month[11]="Decembrie";

		$header=weekday[d.getUTCDay()]+', '+d.getUTCDate()+' '+month[d.getUTCMonth()]+' '+d.getUTCFullYear()+' '+(d.getHours()<10?'0'+d.getHours():d.getHours())+':'+(d.getMinutes()<10?'0'+d.getMinutes():d.getMinutes())
	}

	$.jGrowl($message,{header:$header,life: 2000 });
}

require(['jquery'], function($){
	$(function(){
		// require pages
		var template = $('div.main-template'),
		    Window;
		require(['pages/' + template.prop('id').split('template-')[1], 'pages/basic']);

		// basic javascript hooks
		$('input[name="validation"]').val(new Date().getTime());

		$('a[rel="external"], .external').on('click', function(){
			window.open(this.href);

			return false;
		});

		$(document).on('click','.form-submitter', function(e) {
			$(this).parent().find('button[type="submit"]').click();
			
			return false;
		});

		// require global modules
		require(['window'], function(w){
			Window = w;

			w.parse_hash();

			var we = $('div#window-message');

			if (we.html().length > 0) {
				eval(we.html());
			}
		});
		
		require(['lightbox'], function(){
			$('a.lightbox').lightbox();
		});
	});
});