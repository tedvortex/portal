require(['datepick-ext'], function(){
	var i = $('#departure-date,#departure-date-one-way,#departure-date-multiple,#return-date,#return-date-one-way,#return-date-multiple,#car-take,#checkin,#checkout,#check-in-date,#check-out-date,#car-give,#departure-date-multiple-another,#insurance-on,#insurance-to')
	i.datepick({
		dateFormat: 'dd/mm/yyyy',
		minDate: 0,
		maxDate: +180,
		monthsToShow: 2,
		renderer: $.datepick.themeRollerRenderer,
		onSelect: function(dates){
			if( this.hasClass == 'hasDatepick' ){
				var d = dates[0];
				i.datepick( 'setDate', new Date( d.getFullYear(), d.getMonth(), d.getDate() + 1 ) );
			}
		}
	})
});

var flight_add = $('a.flight-add');
var flight_remove = $('a.flight-remove');
var other_option = $('a.other-option');
var remove = $('a.remove');
var type = $('div.type');
var another_flight = $('div.another-flight');
var another_flight_0 = $('#another-flight-0');
var another_flight_1 = $('#another-flight-1');
var passengers_info = $('div.passengers-info');
var adults_value = $('#adults,#adults-one-way,#adults-multiple');
var childrens_value = $('#children,#children-one-way,#children-multiple');
var infants_value = $('#infant,#infant-one-way,#infant-multiple');

type.find('input').eq(0).attr('checked',true);

flight_add.click(function(){
	var t = $(this);
	if (another_flight_0.hasClass('n')) {
		another_flight_0.removeClass('n');
	} else if ( another_flight_1.hasClass('n') ) {
		another_flight_1.removeClass('n')
	}
	return false;
});
flight_remove.click(function(){
	var t = $(this);
	if (!another_flight_1.hasClass('n')) {
		another_flight_1.addClass('n');
	} else if (!another_flight_0.hasClass('n')) {
		another_flight_0.addClass('n');
	}
	return false;
});
other_option.click(function(){
	/*if ($(this).hasClass('first')) {
		return false;
	}*/
	if(
		$(this).parent().parent().parent().hasClass('closed')
	){
		$(this).parent().parent().parent().removeClass('closed')
		$(this).children().children().text('-');
		
	}else{
		$(this).parent().parent().parent().addClass('closed')
		$(this).children().children().text('+');
	}
	return false;
});
remove.click(function(){
	$(this).parent().addClass('closed');
	other_option.children().children().text('+');
	return false;
});
type.on('click','input',function(){
	var t = $(this);
	var id=t.attr('id').split('-')[1];
	if( t.is(':checked') ){
		var a = t.parent().parent().parent().children('div.tabs');
		var c = t.parent().parent().parent().children('div#tabs-'+id);
		a.addClass('n');
		c.removeClass('n');
	}
});
adults_value.on('change',function(){
	
	var t = $(this);
	var value = t.val();
	var type = '';
	
	if (t.attr('id')=='adults' ) {
		var container_adults = $('.form-passengers-info');
	} else if ( t.attr('id')=='adults-one-way' ) {
		var container_adults = $('.form-passengers-info-one-way');
		var type = '-one-way';
	} else if ( t.attr('id')=='adults-multiple' ) {
		var container_adults = $('.form-passengers-info-multiple');
		var type = '-multiple';
	}
	
	container_adults.html('');
	
	for( var i = 2; i <= value; i++ ) {
		
		var date = '';
		var month = '';
		var year = '';
		
		for( var d = 1; d <= 31; d++ ) { date = date + '<option value="'+d+'">'+d+'</option>' }
		for( var m = 1; m <= 12; m++ ) { month = month + '<option value="'+m+'">'+m+'</option>' }
		for( var y = 1950; y <= new Date().getFullYear(); y++ ) { year = year + '<option value="'+y+'">'+y+'</option>' }

		var html_adults='<div class="passengers-info cf">'
		+'<p class="small">'
		+'	<label for="adults-'+i+type+'">'+i+'. Adult</label>'
		+'		<select class="normal" autocomplete="off" id="adults-'+i+type+'" name="adults-'+i+type+'">'
		+'			<option value="1">Dl.</option>'
		+'			<option value="2">Dna.</option>'
		+'		</select>'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="name-'+i+type+'">Nume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="name-'+i+type+'" name="name-'+i+type+'" />'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="first-name-'+i+type+'">Prenume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="first-name-'+i+type+'" name="first-name-'+i+type+'">'
		+'	</p>'
		+'	<p class="small">'
		+'		<label for="birthday-'+i+type+'">zi</label>'
		+'		<select class="normal" autocomplete="off" id="birthday-'+i+type+'" name="birthday-'+i+type+'">'
		+'			'+date+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="month-'+i+type+'">luna</label>'
		+'		<select class="normal" autocomplete="off" id="month-'+i+type+'" name="month-'+i+type+'">'
		+'			'+month+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="year-'+i+type+'">an</label>'
		+'		<select class="normal" autocomplete="off" id="year-'+i+type+'" name="year-'+i+type+'">'
		+'			'+year+''
		+'		</select>'
		+'	</p>'
		+'</div>';
		
		container_adults.append(html_adults);
	
	};
});
childrens_value.on('change',function(){
	
	var t = $(this);
	var value = t.val();
	var type = '';
	
	if ( t.attr('id')=='children' ) {
		var container_childrens = $('.form-childrens-info');
	} else if ( t.attr('id')=='children-one-way' ) {
		var container_childrens = $('.form-childrens-info-one-way');
		var type = '-one-way';
	} else if ( t.attr('id')=='children-multiple' ) {
		var container_childrens = $('.form-childrens-info-multiple');
		var type = '-multiple';
	}
	
	if ( value == 0 ) { container_childrens.html('') }; 
	
	for( var i = 1; i <= value; i++ ) {
		
		var date = '';
		var month = '';
		var year = '';
		
		for( var d = 1; d <= 31; d++ ) { date = date + '<option value="'+d+'">'+d+'</option>' }
		for( var m = 1; m <= 12; m++ ) { month = month + '<option value="'+m+'">'+m+'</option>' }
		for( var y = 1950; y <= new Date().getFullYear(); y++ ) { year = year + '<option value="'+y+'">'+y+'</option>' }

		var html_childrens='<div class="passengers-info cf">'
		+'<p class="small">'
		+'	<label for="childrens-'+i+type+'">'+i+'. Copil</label>'
		+'		<select class="normal" autocomplete="off" id="childrens-'+i+'" name="childrens-'+i+type+'">'
		+'			<option value="1">Chd.</option>'
		+'		</select>'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="children-name-'+i+type+'">Nume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="children-name-'+i+type+'" name="children-name-'+i+type+'" />'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="children-first-name-'+i+type+'">Prenume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="children-first-name-'+i+type+'" name="children-first-name-'+i+type+'">'
		+'	</p>'
		+'	<p class="small">'
		+'		<label for="children-birthday-'+i+type+'">zi</label>'
		+'		<select class="normal" autocomplete="off" id="children-birthday-'+i+type+'" name="children-birthday-'+i+type+'">'
		+'			'+date+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="children-month-'+i+type+'">luna</label>'
		+'		<select class="normal" autocomplete="off" id="children-month-'+i+type+'" name="children-month-'+i+type+'">'
		+'			'+month+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="children-year-'+i+type+'">an</label>'
		+'		<select class="normal" autocomplete="off" id="children-year-'+i+type+'" name="children-year-'+i+type+'">'
		+'			'+year+''
		+'		</select>'
		+'	</p>'
		+'</div>';
		
		container_childrens.append(html_childrens);
	
	};
});
infants_value.on('change',function(){
	
	var t = $(this);
	var value = t.val();
	var type = '';
	
	if ( t.attr('id')=='infant' ) {
		var container_infants = $('.form-infants-info');
	} else if ( t.attr('id')=='infant-one-way' ) {
		var container_infants = $('.form-infants-info-one-way');
		var type = '-one-way';
	} else if ( t.attr('id')=='infant-multiple' ) {
		var container_infants = $('.form-infants-info-multiple');
		var type = '-multiple';
	}

	container_infants.html('');
	
	if ( value == 0 ) { container_infants.html('') }; 
	
	for( var i = 1; i <= value; i++ ) {
		
		var date = '';
		var month = '';
		var year = '';
		
		for( var d = 1; d <= 31; d++ ) { date = date + '<option value="'+d+'">'+d+'</option>' }
		for( var m = 1; m <= 12; m++ ) { month = month + '<option value="'+m+'">'+m+'</option>' }
		for( var y = 1950; y <= new Date().getFullYear(); y++ ) { year = year + '<option value="'+y+'">'+y+'</option>' }

		var html_infants='<div class="passengers-info cf">'
		+'<p class="small">'
		+'	<label for="infants-'+i+type+'">'+i+'. Infant</label>'
		+'		<select class="normal" autocomplete="off" id="infants-'+i+type+'" name="infants-'+i+type+'">'
		+'			<option value="1">Chd.</option>'
		+'		</select>'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="infants-name-'+i+type+'">Nume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="infants-name-'+i+type+'" name="infants-name-'+i+type+'" />'
		+'	</p>'
		+'	<p class="medium">'
		+'		<label for="infants-first-name-'+i+type+'">Prenume</label>'
		+'		<input type="text" value="" class="normal" autocomplete="off" id="infants-first-name-'+i+type+'" name="infants-first-name-'+i+type+'">'
		+'	</p>'
		+'	<p class="small">'
		+'		<label for="infants-birthday-'+i+type+'">zi</label>'
		+'		<select class="normal" autocomplete="off" id="infants-birthday-'+i+type+'" name="infants-birthday-'+i+type+'">'
		+'			'+date+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="infants-month-'+i+type+'">luna</label>'
		+'		<select class="normal" autocomplete="off" id="infants-month-'+i+type+'" name="infants-month-'+i+type+'">'
		+'			'+month+''
		+'		</select>'
		+'	</p>'
		+'	<p class="medium-small">'
		+'		<label for="infants-year-'+i+type+'">an</label>'
		+'		<select class="normal" autocomplete="off" id="infants-year-'+i+type+'" name="infants-year-'+i+type+'">'
		+'			'+year+''
		+'		</select>'
		+'	</p>'
		+'</div>';
		
		container_infants.append(html_infants);
	
	};
});