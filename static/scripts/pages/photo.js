require(['sudoslider', 'cycle'], function(){

	$('ul#image-controller')
	.cycle({
		timeout: 0,
		speed: 300,
		prev: '.prev',
		next: '.next'
	});

	var img_ctr = $('ul#image-controller');
	var img = $('ul#image');

	$('a', img_ctr)
	.click(function(){
		var t = $(this);
		var target = $(this).data('target');

		$('li.a', img)
		.removeClass('a')
		.addClass('n');

		if(
			$('li#'+target).length > 0
		){
			$('li#'+target)
			.addClass('a')
			.removeClass('n');
		}
		else{
			img
			.append(
				$( document.createElement('li') )
				.attr('id', target)
				.addClass('n')
				.append(
					$( document.createElement('a') )
					.attr({
						'href' : t.data('image'),
						'title' : t.attr('title')
					})
					.addClass('lightbox')
					.data('rel', 'gallery')
					.append(
						$( document.createElement('span' ) )
						.addClass('sprite')
						.addClass('scope')
					)
					.append(
						$( document.createElement('img') )
						.attr({
							'src' : t.data('imagelist'),
							'alt' : t.attr('title')
						})
					)
				)
			);

			$('li#'+target)
			.addClass('a')
			.removeClass('n');
		}

	});

});