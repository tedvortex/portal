
$('ul.tabs-list p').click(function(){

	var p = $(this).parent();
	var tabs_list = p.parent();
	var tabs = $('ul#'+tabs_list.data('tabs'));

	if( !p.hasClass('a') )
	{
		$('>li.a', tabs_list).removeClass('a');
		p.addClass('a');

		$('>li.a', tabs)
		.removeClass('a')
		.addClass('n');

		$('>li', tabs).eq( p.index() )
		.removeClass('n')
		.addClass('a');
	}
});

var months_test =
{
	0 : { 'i' : 2, 'n' : 'feb', 'p' : 1650 },
	1 : { 'i' : 3, 'n' : 'mar', 'p' : 2000 },
	2 : { 'i' : 4, 'n' : 'apr', 'p' : 1890 },
	3 : { 'i' : 5, 'n' : 'mai', 'p' : 1200 },
	4 : { 'i' : 6, 'n' : 'iun', 'p' : 856 },
	5 : { 'i' : 7, 'n' : 'iul', 'p' : 785 },
	6 : { 'i' : 8, 'n' : 'aug', 'p' : 785 },
	7 : { 'i' : 9, 'n' : 'sep', 'p' : 0 },
	8 : { 'i' : 10, 'n' : 'oct', 'p' : 0 }
};

var months_test2 =
{
	0 : { 'i' : 2, 'n' : 'mar', 'p' : 5756 },
	1 : { 'i' : 3, 'n' : 'apr', 'p' : 5000 },
	2 : { 'i' : 4, 'n' : 'mai', 'p' : 4892 },
	3 : { 'i' : 5, 'n' : 'iun', 'p' : 6895 },
	4 : { 'i' : 6, 'n' : 'iul', 'p' : 3985 },
	5 : { 'i' : 7, 'n' : 'aug', 'p' : 6885 },
	6 : { 'i' : 8, 'n' : 'sep', 'p' : 3596 },
	7 : { 'i' : 9, 'n' : 'oct', 'p' : 0 },
	8 : { 'i' : 10, 'n' : 'noi', 'p' : 4859 }
};

function generate_prices_chart( container, months )
{
	var current = $('ul#prices-chart');
	if( current.length > 0 )
	{
		current.remove();
	}

	var max = 0;
	$.each(months, function(indx, m) {
		max = m.p > max ? m.p : max;
	});

	var ul = $(document.createElement('ul'));

	ul
	.attr('id', 'prices-chart')
	.addClass('nav')
	.addClass('cf');

	$.each (months, function(indx, m) {

		var height = ( m.p * 70 / max );

		var li = $(document.createElement('li'));

		var div = $(document.createElement('div'));
		div.addClass('chart'+(m.p == 0 ? ' empty' : ''))

		var p = $(document.createElement('p'));
		p.css('height', height)

		p.append(
			$(document.createElement('span'))
			.addClass('price')
			.html(m.p > 0 ? m.p+' &#128;' : '-')
		);

		div.append(p);
		li.append(div);

		li.append(
				$(document.createElement('strong')).text(m.n)
		);

		ul.append(li);
	});

	$('#'+container).append(ul);
}

generate_prices_chart('prices-chart-container', months_test);
generate_prices_chart('prices-chart-container', months_test2);