$('ul#order-navigator a')
.click(function(){
	var t = $(this);

	t
	.parent()
	.parent()
	.parent()
	.find('input')
	.val( t.data('reset') )
	.parent()
	.submit()

	return false;
});

$('ul#order-navigator .step-title')
.hover(
	function(){
		$(this).parent().find('.step-nr').addClass('a-hover');
	},
	function(){
		$(this).parent().find('.step-nr').removeClass('a-hover');
	}
);

$('a#go-to-next')
.click(function(){

	$('form#order-next a.control')
	.click()
});


$('form#order-next div a.control')
.click(function(){
	var t = $(this)
	var next = t.data('next-step')

	if(
		next == 2 ||
		next == 3 ||
		next == 4
	){
		t
		.parent()
		.parent()
		.submit()
	}
});

if(
	$('div.change-quantity').length > 0
){
	$('div.change-quantity input')
	.click(function(){
		$(this).select();
	});

	$('div.change-quantity span')
	.click(function(){
		var t = $(this);
		var i = t.parent().find('input');

		if(
			t.hasClass('plus')
		){

			var sum = parseInt( i.val() ) + 1;

			if(
				sum >= i.data('min') &&
				sum <= i.data('max')
			){
				i.val( sum );
			}
		}

		if(
			t.hasClass('minus')
		){
			var dif = parseInt( i.val() ) - 1;

			if(
				dif >= i.data('min') &&
				dif <= i.data('max')
			){
				i.val( dif );
			}
		}

		i.trigger('focusout');

	});
}


var _form = $('section.form');

$('input[name=\"type\"]',_form)
.attr('checked',false)
.bind('click change',function(){
	if(this.value==0){
		$('div#fizica',_form)
		.appendTo('div#empty');
	}

	else{
		$('div#fizica')
		.insertAfter( $(this).parent().parent() );
	}
});

$('input[name="quantity\[\]"]')
.click(function(){
	$(this)
	.trigger('select')
})

$('input[name="quantity\[\]"],input[name="quantity-update\[\]"]')
.focusout(function(){
	var t=$(this)
	this.value=parseFloat(this.value)
	this.value=Math.floor(this.value)

	if(
	this.value<parseInt(t.attr('min')) ||
	this.value=='NaN'
	){
		this.value=t.attr('min')
	}

	if(this.value>parseInt(t.attr('max'))){
		this.value=t.attr('max')
	}

	t.trigger('change')
})

$('input[name=\"quantity-update\[\]\"]')
.change(function(){
	var t=$(this);
	var id=this.className.split('-')[1];

	cart(id, this.value, 'update',0,t);

	return false;
})


require(['cart'], function(){
	var cart_products = $('div#products-cart-list');
	/*$('a.add-to-cart').on('click', function(){
		var t = $(this);

		cart(this.href.split('#')[1],$('div#quantity input').val(),'add');

		return false;
	});*/

	/*$('td.delete a, div#cart-products a.delete').on('click', function() {
		var id=this.href.split('#')[1];
		var t = $(this);

		console.log('clicked');

		cart(id, 0,'remove',t);

		return false;
	});*/
	$(cart_products).on('click', 'td.delete a', function() {

		var id=this.href.split('#')[1];
		var t = $(this);

		cart(id, 0,'remove', 0, t);

		return false;
	})

});

$('a[class*=\"cart-submit\"]').click(function(){
	var _step=new String(this.className.match(/cart-submit-[0-9]{1}/))

	if(
	_step.length>0
	){
		_step=_step.split('cart-submit-')[1];

		if(
		_step>0 &&
		_step<5
		){
			$('#cart-step-'+_step).parent().submit();
		}
	}

	return false
})

$('a[class*=\"cart-edit\"]').click(function(){
	var _step=new String(this.className.match(/cart-edit-[0-9]{1}/))

	if(_step.length>0){
		_step=_step.split('cart-edit-')[1]

		if(
		_step>0 &&
		_step<5
		){
			$('input#step-reset').val(_step)
			$('form#steps-reset').submit()
		}
	}

	return false
})

$('input[name=\"person[]\"]')
.click(function(){
	var t = $(this);

	$('input[name=\"person[]\"]').removeAttr('checked');

	t.attr('checked',true);

	if (t.attr('id')=='person-1') {
		//$('div#person-placeholder').html('');
		$('div#company-form').appendTo('div#person-placeholder');
		$('div#individual-form').appendTo('div#gol');
	}

	else {
		$('div#company-form').appendTo('div#gol');
		$('div#individual-form').appendTo('div#person-placeholder');
	}
})
