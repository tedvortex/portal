require(['products'], function(){
	$('select#children').change(function(){
		var t = $(this);
		var v = parseInt(t.val());
		
		$('.children-age').each(function(){
			var t = $(this);
			var i = parseInt(t.prop('id').split('-')[1]);
			
			if (i >= v) {
				t.addClass('n');
			} else {
				t.removeClass('n');
			}
		});
	});
});