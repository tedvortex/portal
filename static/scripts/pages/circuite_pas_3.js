$('ul.tabs-list p').click(function(){

	var p = $(this).parent();
	var tabs_list = p.parent();
	var tabs = $('ul#'+tabs_list.data('tabs'));

	if( !p.hasClass('a') )
	{
		$('>li.a', tabs_list).removeClass('a');
		p.addClass('a');

		$('>li.a', tabs)
		.removeClass('a')
		.addClass('n');

		$('>li', tabs).eq( p.index() )
		.removeClass('n')
		.addClass('a');
	}
});
var checkbox = $('span.term-checkbox');
var terms = $('div#termsandconditions');
$(checkbox)
.on('click','span',function(){
	var t = $(this);

		if (
			t.hasClass('normal')
		){
			t.removeClass('normal')
			t.addClass('active')
			terms.find('input[type="checkbox"]').prop('checked', true)
		} else if (
			t.hasClass('active')
		){
			t.removeClass('active')
			t.addClass('normal')
			terms.find('input[type="checkbox"]').prop('checked', false);
		}
	}
)