function reset_quick_search_width() {
	var qs = $('section#quick-search');
	var input = $('input[name="term"]', qs);
	var p = $('p#quick-search-category');

	var new_width = qs.width() - p.outerWidth() - $('#go-quick-search').outerWidth() - 10;

	input.css('width', new_width);
}

$.fn.chain = function(target, module){
	var t = $(this);
	var target = $(target);

	$(document).on('change', '#' + t.prop('id'), function(event){
		$('option', target).remove();

		target.attr('disabled', 'disabled');

		$.ajax({
			type: 'get',
			url: 'http://' + window.location.host + '/ajax.json',
			dataType: 'jsonp',
			data: {
				module: module,
				value: t.val()
			},
			success: function(data) {
				if (data.items.length > 0) {
					for (var i in data.items) {
						target.append(
							$(document.createElement('option'))
							.val(data.items[i].id)
							.text(data.items[i].name)
						);
					}

					target
					.removeAttr('disabled')
					.trigger('change');
				}
			}
		});
	});
}

define(['pages/basic'], function () {
	$('select.chain').each(function(e,t){
		var t = $(t);

		if (t.data('chain')) {
			var target = $('#' + t.data('chain'));

			target.chain('select#' + t.prop('id'), t.data('module'));
		}
	});

	$('select.chain').each(function(e,t){
		var t = $(t);

		if (t.data('initializer')) {
			t.trigger('change');
		}
	});

	var ng_slider = $('ul.nextgen-slider');

	if (ng_slider.length > 0) {
		require(['cycle'], function(){

			var sl_text = $('ul#default-slide-text');

			sl_text.cycle({
				timeout: 0,
				speed: 1000,
				cleartypeNoBg: true
			});

			$.each( ng_slider, function(i, sl){
				sl = $(sl);

				var navigator = sl.data('navigator');
				var speed = sl.data('speed');
				var timeout = sl.data('timeout');

				sl.cycle({
					timeout: timeout,
					speed: speed,
					pause: true,
					cleartypeNoBg: true,
	   				activePagerClass: 'a',
					pager: '#'+navigator,
					pagerAnchorBuilder: function(idx, slide) {
						return '<li class="sprite"></li>';
					},
					before:function(){
						if( sl.attr('id') == 'default-slide' )
						{
							var pos = $(this).index();
	   						sl_text.cycle(pos);
						}
					}
				});
			});
		});
	}

	require(['datepick-ext'], function(){
		$('input.datepick')
		.datepick({
			dateFormat: 'dd/mm/yyyy',
			minDate: 0,
			maxDate: +180,
			monthsToShow: 2,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function(dates){
				if( this.id == 'date-check-in' ){
					var d = dates[0];

					$('input#date-check-out')
					.datepick( 'setDate', new Date( d.getFullYear(), d.getMonth(), d.getDate() + 1 ) );
				}
			}
		})
	});

	var custom_select = $('div.custom-select');

	if( custom_select.length > 0 ){
		$('.value', custom_select).click(function(){

			var t = $(this);
			var list = t.parent().find('.select-container');

			if( t.hasClass('a') ){
				t.removeClass('a');
				list.css('height', '0');
			}
			else{
				t.addClass('a');
				list.css('height', 'auto');
			}
		});

		$('li', custom_select).click(function(){
			var t = $(this);
			var val = t.parent().parent().parent().find('.value');

			t.parent().parent().css('height', '0');
			val.removeClass('a');
			$('input', val).val(t.data('value'));
			$('p', val).text(t.data('name'));
		});
	}

	var tabs_container = $('.main-tabs-container');

	if( custom_select.length > 0 ){
		$('.tab-ref', tabs_container).click(function(){
			var t = $(this);
			var pos = t.index();
			$('.tabs-selection li >span').each(
				function(){
					var cl = $(this).attr('class').split(' ')[1];

					$(this).removeClass(cl).addClass(cl.replace('-hover',''));
				}
			);
			var icon = t.children('span').attr('class').split(' ')[1];

			t.children('span').removeClass(icon).addClass(icon+'-hover');
			$('.tabs-selection li').removeClass('selected');
			t.addClass('selected');
			$('.tabs-container .tab').addClass('n');
			$('.tabs-container .tab').eq(pos).removeClass('n');
		})
	}

	$('p#quick-search-category')
	.click(function(){
		var t = $(this);
		var div = $('div#quick-search-categories');

		if(
			!t.hasClass('selected')
		){
			if(
				div.height() == 0
			){
				div
				.css({'height': 'auto', 'overflow': 'visible'});
			}
			else{
				div
				.css({'height': '0', 'overflow': 'hidden'});
			}
		}
		else{
			$('input[name="search_category"]', $('#quick-search'))
			.val('');

			t
			.removeClass('selected');

			$('strong', t)
			.text('Toate');
		}

		reset_quick_search_width();
	});

	var _hs = $('section#quick-search')

	$('#go-quick-search',_hs)
	.click(function(){
		var t = $(this)

		window.location = $('#go-quick-search',_hs).data('url') + '?term=' + escape( $('input[name="term"]',_hs).val() ) + '&shop=' + $('input[name="search_category"]',_hs).val()

		return false;

	})


	$('input',_hs)
	.keypress(function(event){
		if(
		event.keyCode == 13
		){
			var t = $(this)

			window.location = $('#go-quick-search',_hs).data('url') + '?term=' + escape( $('input[name="term"]',_hs).val() ) + '&shop=' + $('input[name="search_category"]',_hs).val()

			return false;
		}
	})

	$('div#quick-search-categories li')
	.click(function(){
		var t = $(this);
		var p = $('p#quick-search-category');

		$('input[name="search_category"]', $('#quick-search'))
		.val(t.data('cat'));

		p
		.addClass('selected');

		$('strong', p)
		.text(t.data('name'));

		t
		.parent()
		.parent()
		.css({'height': '0', 'overflow': 'hidden'});

		reset_quick_search_width();
	});

});

$('.date-icon').click(function(){
	$(this).parent().find('input').focus();
});

$('.radio-nav span').click(function(){
	var t = $(this);
	t.parent().parent().find('input[type="radio"]').attr('checked',false);
	t.parent().parent().find('span.circle').html('');
	t.parent().children('span.circle').html('<span class="selected"></span>');
	t.parent().children('input[type="radio"]').attr('checked',true);
});

$('.radio-nav label').click(function(){
	var t = $(this);
	t.parent().parent().find('input[type="radio"]').attr('checked',false);
	t.parent().parent().find('span.circle').html('');
	t.parent().children('span.circle').html('<span class="selected"></span>');
	t.parent().children('input[type="radio"]').attr('checked',true);
});

$('#prices-select select#children').change(function(){
	var t = $(this);
	var ul = t.parent().parent();

	$('li.children-age', ul).remove();

	for( var i = 0; i < t.val(); i++ ){

		var select = $(document.createElement('select'));

		select
		.addClass('multiple')
		.prop('name', 'children_ages[]')
		.data('index', i)
		.data('name', 'children_ages');

		for(var j = 1; j < 17; j++){
			select.append(
				$(document.createElement('option'))
				.prop('value', j)
				.text(j)
			);
		}

		ul.append(
			$(document.createElement('li'))
			.addClass('children-age')
			.append(
				$(document.createElement('label'))
				.text('Varsta copil')
			)
			.append(select)
		);

	}
});

var ai_input = $('div.ai-input');

$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

$('input', ai_input)
.click(function(){
	$(this).select();
})
.keydown(function(){

	var t = $(this);
	var p = t.parent();
	var d = $('>div', p);
	var ul = $('ul', d);

	var ic = $('input[name="ai_city"]', p);
	var ict = $('input[name="ai_country"]', p);

	var time = new Date().getTime();
	processTime = time;

	window.setTimeout(
		function(){
			if( processTime == time && t.val().length > 1 ){

				d.removeClass('active');
				$('li', ul).remove();

				ic.val(0);
				ict.val(0);

				$.ajax({
					url: 'http://' + window.location.host + '/ajax.json',
					data: { module : 'search', 'city' : t.val() },
					dataType: 'json',
					success: function(data){

						if( data.c.length > 0 ){
							for( var i in data.c ){

								var city = data.c[i];

								if( i == 0 ){
									var total = city.n.length;
									var part = t.val().length;

									ic.val(city.ic);
									ict.val(city.ict);

									if( t.hasClass('ai-city-cazari') ){
										$('select#country').val(city.ict).trigger('change');
									}

									t
									.val(city.n)
									.selectRange(part, total);
								}

								ul.append(
									$(document.createElement('li'))
									.data('city', city.ic)
									.data('country', city.ict)
									.text( city.n )
								);

								d.addClass('active');
							}
						}
					}
				});
			}
			else{
				d.removeClass('active');
				$('li', ul).remove();

				ic.val(0);
				ict.val(0);
			}
		},
		333
	);
})
.focusout(function(){

	var p = $(this).parent();
	var d = $('>div', p);
	var ul = $('ul', d);

	d.removeClass('active');
});

ai_input.on('click', '.ai-list li', function(){

	var t = $(this);
	var a = t.parent().parent().parent();

	var i = $('input[name="location"]', a);

	if( i.hasClass('ai-city-cazari') ){
		$('select#country').val(t.data('country')).trigger('change');
	}

	i.val(t.text());
	$('input[name="ai_city"]', a).val(t.data('city'));
	$('input[name="ai_country"]', a).val(t.data('country'));
	$('>div', a).removeClass('active');
	$('li', t.parent()).remove();

});