function reset_quick_search_width() {
	var qs = $('section#quick-search');
	var input = $('input[name="term"]', qs);
	var p = $('p#quick-search-category');

	var new_width = qs.width() - p.outerWidth() - $('#go-quick-search').outerWidth() - 10;

	input.css('width', new_width);
}

$.fn.chain = function(target, module){
	var t = $(this);
	var target = $(target);

	$(document).on('change', '#' + t.prop('id'), function(event){
		$('option', target).remove();

		target.attr('disabled', 'disabled');

		$.ajax({
			type: 'get',
			url: 'http://' + window.location.host + '/ajax.json',
			dataType: 'jsonp',
			data: {
				module: module,
				value: t.val()
			},
			success: function(data) {
				if (data.items.length > 0) {
					for (var i in data.items) {
						target.append(
							$(document.createElement('option'))
							.val(data.items[i].id)
							.text(data.items[i].name)
						);
					}

					target
					.removeAttr('disabled');

					if( t.data('schain') ){
						target
						.val(t.data('schain'));
					}

					target
					.trigger('change');
				}
			}
		});
	});
}

define(['pages/basic'], function () {
	$('select.chain').each(function(e,t){
		var t = $(t);

		if (t.data('chain')) {
			var target = $('#' + t.data('chain'));

			target.chain('select#' + t.prop('id'), t.data('module'));
		}
	});

	$('select.chain').each(function(e,t){
		var t = $(t);

		if (t.data('initializer')) {
			t.trigger('change');
		}
	});

	var ng_slider = $('ul.nextgen-slider');

	if (ng_slider.length > 0) {
		require(['cycle'], function(){

			var sl_text = $('ul#default-slide-text');

			sl_text.cycle({
				timeout: 0,
				speed: 1000,
				cleartypeNoBg: true
			});

			$.each( ng_slider, function(i, sl){
				sl = $(sl);

				var navigator = sl.data('navigator');
				var speed = sl.data('speed');
				var timeout = sl.data('timeout');

				sl.cycle({
					timeout: timeout,
					speed: speed,
					pause: true,
					cleartypeNoBg: true,
	   				activePagerClass: 'a',
					pager: '#'+navigator,
					pagerAnchorBuilder: function(idx, slide) {
						return '<li class="sprite"></li>';
					},
					before:function(){
						if( sl.attr('id') == 'default-slide' )
						{
							var pos = $(this).index();
	   						sl_text.cycle(pos);
						}
					}
				});
			});
		});
	}

	require(['datepick-ext'], function(){
		$('input.datepick')
		.datepick({
			dateFormat: 'dd/mm/yyyy',
			minDate: 0,
			maxDate: +365,
			monthsToShow: 2,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function(dates){
				if( this.id == 'date-check-in' ){
					var d = dates[0];

					$('input#date-check-out')
					.datepick( 'setDate', new Date( d.getFullYear(), d.getMonth(), d.getDate() + 1 ) );
				}
			}
		});
		
		$('input.datepick-cazari')
		.datepick({
			dateFormat: 'dd/mm/yyyy',
			minDate: 1,
			maxDate: +365,
			monthsToShow: 2,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function(dates){
				if( this.id == 'date-check-in' ){
					var d = dates[0];

					$('input#date-check-out')
					.datepick( 'setDate', new Date( d.getFullYear(), d.getMonth(), d.getDate() + 1 ) );
				}
			}
		})
	});

	var custom_select = $('div.custom-select');

	if( custom_select.length > 0 ){
		$('.value', custom_select).click(function(){

			var t = $(this);
			var list = t.parent().find('.select-container');

			if( t.hasClass('a') ){
				t.removeClass('a');
				list.css('height', '0');
			}
			else{
				t.addClass('a');
				list.css('height', 'auto');
			}
		});

		$('li', custom_select).click(function(){
			var t = $(this);
			var val = t.parent().parent().parent().find('.value');

			t.parent().parent().css('height', '0');
			val.removeClass('a');
			$('input', val).val(t.data('value'));
			$('p', val).text(t.data('name'));
		});
	}

	var tabs_container = $('.main-tabs-container');

	if( custom_select.length > 0 ){
		$('.tab-ref', tabs_container).click(function(){
			var t = $(this);
			var pos = t.index();
			$('.tabs-selection li >span').each(
				function(){
					var cl = $(this).attr('class').split(' ')[1];

					$(this).removeClass(cl).addClass(cl.replace('-hover',''));
				}
			);
			var icon = t.children('span').attr('class').split(' ')[1];

			t.children('span').removeClass(icon).addClass(icon+'-hover');
			$('.tabs-selection li').removeClass('selected');
			t.addClass('selected');
			$('.tabs-container .tab').addClass('n');
			$('.tabs-container .tab').eq(pos).removeClass('n');
		})
	}

	reset_quick_search_width();
	
	$('p#quick-search-category')
	.click(function(){
		var t = $(this);
		var div = $('div#quick-search-categories');

		if(
			!t.hasClass('selected')
		){
			if(
				div.height() == 0
			){
				div
				.css({'height': 'auto', 'overflow': 'visible'});
			}
			else{
				div
				.css({'height': '0', 'overflow': 'hidden'});
			}
		}
		else{

			$('a#go-quick-search').data('url', 'unselected');
			
			t
			.removeClass('selected');

			$('strong', t)
			.text('Selecteaza');
		}

		reset_quick_search_width();
	});
	
	$('div#quick-search-categories li')
	.click(function(){
		
		var t = $(this);
		var p = $('p#quick-search-category');

		$('a#go-quick-search').data('url', t.data('url'));

		p
		.addClass('selected');

		$('strong', p)
		.text(t.data('name'));

		t
		.parent()
		.parent()
		.css({'height': '0', 'overflow': 'hidden'});

		reset_quick_search_width();
	});
	
	$('a#go-quick-search').click(function(){
		
		var t = $(this);
		var url = t.data('url');
		var term = $('#quick-search input[name="term"]').val();
		
		if( url != 'unselected' && term.length > 2 ){
			window.location = url + '?filter-search_term=' + term;
		}
		
		return false;
		
	});
	
	$('#quick-search input[name="term"]')
	.keypress(function(event){
		
		if(
			event.keyCode == 13
		){
			var t = $(this);
			
			var a = $('a#go-quick-search');
			var url = a.data('url');
			var term = t.val();

			if( url != 'unselected' && term.length > 2 ){
				window.location = url + '?filter-search_term=' + term;
			}
			return false;
		}
	})

	/*var _hs = $('section#quick-search')

	$('#go-quick-search',_hs)
	.click(function(){
		var t = $(this)

		window.location = $('#go-quick-search',_hs).data('url') + '?term=' + escape( $('input[name="term"]',_hs).val() ) + '&shop=' + $('input[name="search_category"]',_hs).val()

		return false;

	})

	$('input',_hs)
	.keypress(function(event){
		
		if(
		event.keyCode == 13
		){
			var t = $(this)

			window.location = $('#go-quick-search',_hs).data('url') + '?term=' + escape( $('input[name="term"]',_hs).val() ) + '&shop=' + $('input[name="search_category"]',_hs).val()

			return false;
		}
	})*/

});

$('.date-icon').click(function(){
	$(this).parent().find('input').focus();
});

$('.radio-nav span').click(function(){
	var t = $(this);
	t.parent().parent().find('input[type="radio"]').attr('checked',false);
	t.parent().parent().find('span.circle').html('');
	t.parent().children('span.circle').html('<span class="selected"></span>');
	t.parent().children('input[type="radio"]').attr('checked',true);
});

$('.radio-nav label').click(function(){
	var t = $(this);
	t.parent().parent().find('input[type="radio"]').attr('checked',false);
	t.parent().parent().find('span.circle').html('');
	t.parent().children('span.circle').html('<span class="selected"></span>');
	t.parent().children('input[type="radio"]').attr('checked',true);
});

$('#prices-select select#children, #main-filters select#children').change(function(){
	var t = $(this);
	var ul = t.parent().parent();

	$('li.children-age', ul).remove();

	for (var i = 0; i < t.val(); i++ ) {

		var select = $(document.createElement('select'));

		select
		.addClass('multiple')
		.prop('name', 'children_ages[]')
		.data('index', i)
		.data('name', 'children_ages');

		for (var j = 1; j < 17; j++) {
			select.append(
				$(document.createElement('option'))
				.prop('value', j)
				.text(j)
			);
		}

		ul.append(
			$(document.createElement('li'))
			.addClass('children-age')
			.append(
				$(document.createElement('label'))
				.text('Varsta copil')
			)
			.append(select)
		);
	}
});

var ai_input = $('div.ai-input');

$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

$('input', ai_input)
.click(function(){
	$(this).select();
})
.keydown(function(){

	var t = $(this);
	var p = t.parent();
	var d = $('>div', p);
	var ul = $('ul', d);

	var ic = $('input[name="ai_city"]', p);
	var ict = $('input[name="ai_country"]', p);

	var time = new Date().getTime();
	processTime = time;

	window.setTimeout(
		function(){
			if( processTime == time && t.val().length > 1 ){

				d.removeClass('active');
				$('li', ul).remove();

				ic.val(0);
				ict.val(0);

				if( t.data('chain') ){

					$('select#'+t.data('chain'))
					.data('schain', 0);
				}


				$.ajax({
					url: 'http://' + window.location.host + '/ajax.json',
					data: { module : 'search', 'city' : t.val() },
					dataType: 'json',
					success: function(data){

						if( data.c.length > 0 ){
							for( var i in data.c ){

								var city = data.c[i];

								if( i == 0 ){
									var total = city.n.length;
									var part = t.val().length;

									ic.val(city.ic);
									ict.val(city.ict);

									if( t.data('chain') ){

										$('select#'+t.data('chain'))
										.val(city.ict)
										.data('schain', city.ic)
										.trigger('change');
									}

									t
									.val(city.n)
									.selectRange(part, total);
								}

								ul.append(
									$(document.createElement('li'))
									.data('city', city.ic)
									.data('country', city.ict)
									.text( city.n )
								);

								d.addClass('active');
							}
						}
					}
				});
			}
			else{
				d.removeClass('active');
				$('li', ul).remove();

				ic.val(0);
				ict.val(0);

				if( t.data('chain') ){

					$('select#'+t.data('chain'))
					.data('schain', 0);
				}
			}
		},
		333
	);
})
.focusout(function(){

	var p = $(this).parent();
	var d = $('>div', p);

	window.setTimeout(function(){
		d.removeClass('active');
	}, 500);
});

ai_input.on('click', '.ai-list li', function(){

	var t = $(this);
	var a = t.parent().parent().parent();

	var i = $('input[name="location"]', a);

	if( i.data('chain') ){

		$('select#'+i.data('chain'))
		.val(t.data('country'))
		.data('schain', t.data('city'))
		.trigger('change');
	}

	i.val(t.text());
	$('input[name="ai_city"]', a).val(t.data('city'));
	$('input[name="ai_country"]', a).val(t.data('country'));
	$('>div', a).removeClass('active');
	$('li', t.parent()).remove();

});

if( $('#reservation-tabs #tab-1').length > 0 ){

	$('#reservation-tabs select#sejururi-filter-children').change(function(){
		
		console.log('asd');
		
		var t = $(this);
		var ul = t.parent().parent().parent();

		$('li.children-ages', ul).remove();

		for( var i = 0; i < t.val(); i++ ){
			var select = $(document.createElement('select'));

			select
			.addClass('value')
			.prop('name', 'filter-children-ages[]')
			.data('index', i)
			.data('name', 'filter-children-ages');

			for (var j = 1; j < 17; j++) {
				select.append(
					$(document.createElement('option'))
					.prop('value', j)
					.text(j)
				);
			}

			ul.append(
				$(document.createElement('li'))
				.addClass('children-ages')
				.append(
					$(document.createElement('p'))
					.addClass('custom-label')
					.text('Varsta copil')
				)
				.append(
					$(document.createElement('div'))
					.addClass('custom-select')
					.append(select)
				)
			);
		}
	})
}

if( $('#reservation-tabs #tab-2').length > 0 && !$('#reservation-tabs #tab-2').hasClass('n') ){
    
    $('select.rooms-total').change(function(){
        var t = $(this);
        var c = $('#'+t.data('container'));
        var v = t.val();

        var current_rooms = $('>div.room', c).length;

        if( current_rooms > v ){
            for( i = current_rooms - 1; i >= v; i-- ){
                $('div.room-'+i, c).remove();
            }
        }
        else if( v > current_rooms){
            for( i = current_rooms; i < v ; i++ ){

                var select_adults =
                $(document.createElement('select'))
                .addClass('value room-adults')
                .prop('name', 'people['+i+'][adults]');

                for( var j = 1; j < 10; j++ ){
                    select_adults.append(
                        $(document.createElement('option'))
                        .prop('value', j)
                        .text(j + ' AD')
                    );
                }

                var select_children =
                $(document.createElement('select'))
                .addClass('value room-children')
                .prop('name', 'people['+i+'][children]');

                for( var j = 0; j < 10; j++ ){
                    select_children.append(
                        $(document.createElement('option'))
                        .prop('value', j)
                        .text(j + ' CH')
                    );
                }

                c.append(
                    $(document.createElement('div'))
                    .addClass('room room-'+i)
                    .append(
                        $(document.createElement('ul'))
                        .addClass('nav cf shorts person fix')
                        .append(
                            $(document.createElement('li'))
                            .addClass('room-order')
                            .append(
                                $(document.createElement('p'))
                                .addClass('custom-label')
                                .text('Camera '+(i+1)+':')
                            )
                        )
                        .append(
                            $(document.createElement('li'))
                            .append(
                                $(document.createElement('p'))
                                .addClass('custom-label')
                                .text('Nr. adulti')
                            )
                            .append(
                                $(document.createElement('div'))
                                .addClass('custom-select')
                                .append(select_adults)
                            )
                        )
                        .append(
                            $(document.createElement('li'))
                            .append(
                                $(document.createElement('p'))
                                .addClass('custom-label')
                                .text('Nr. copii')
                            )
                            .append(
                                $(document.createElement('div'))
                                .addClass('custom-select')
                                .append(select_children)
                            )
                        )
                    )
                );
            }
        }
    });
    
    $('.rooms-container').on('change', 'select.room-children', function(){
        var t = $(this);
        var ul = t.parent().parent().parent();

        $('li.children-age', ul).remove();

        for( var i = 0; i < t.val(); i++ ){

            var select = $(document.createElement('select'));

            select
            .addClass('value')
            .prop('name', t.prop('name').replace('children', 'children-ages') + '[]');

            for(var j = 1; j < 17; j++){
                select.append(
                    $(document.createElement('option'))
                    .prop('value', j)
                    .text(j)
                );
            }

            ul.append(
                $(document.createElement('li'))
                .addClass('children-age')
                .append(
                    $(document.createElement('p'))
                    .addClass('custom-label')
                    .text('Varsta copil')
                )
                .append(
                    $(document.createElement('div'))
                    .addClass('custom-select')
                    .append(select)
                )
            );
        }
    });
}


var check_select = $('.check-select');

if( check_select.length > 0 ){
	
	$('input.select-all').click(function(){
		
		var t = $(this);
		var ul = t.parent().parent();
		
		if( t.is(':checked') ){
			$('input[type="checkbox"]', ul).prop('checked', true);
		}
		else{
			$('input[type="checkbox"]', ul).prop('checked', false);
		}
		
		$('#'+ul.data('name')).text('Toate variantele');
	});
	
	$('.check-select-storage input[type="checkbox"]').click(function(){
		
		var t = $(this);
		
		if(!t.hasClass('select-all')){
			
			var ul = t.parent().parent();
			
			$('input.select-all', ul).prop('checked', false);
			
			var string = '';
			$('input:checked', ul).each(function(i, v){
				
				var name = $(v).parent().find('label').text();
				
				if( i < 3 ){
					string += ( i > 0 ? ', ' : '' ) + name
				}
				else{
					string = $('input:checked', ul).length + ' selectate';
					return false;
				}

			});
			
			string = string != '' ? string : 'Toate variantele';
			
			$('#'+ul.data('name')).text(string);
		}
	});
	
	$.each(check_select, function(indx, c){
		
		if( !$('input.select-all', c).is(':checked') ){
			
			var string = '';
			$('input:checked', c).each(function(i, v){
				
				var name = $(v).parent().find('label').text();
				
				if( i < 3 ){
					string += ( i > 0 ? ', ' : '' ) + name
				}
				else{
					string = $('input:checked', c).length + ' selectate';
					return false;
				}

			});
			
			string = string != '' ? string : 'Toate variantele';
			
			$('div.check-select-value p', c).text(string);
			
		}
	});
}
