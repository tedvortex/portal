var custom_select = $('.custom-select');

$(custom_select)
.on('click','p', function(){
	var t = $(this);
	var p = t.parent();
	var ul = $('ul', p);

	p
	.stop()
	.animate(
		{
			height : ( p.height() == t.outerHeight() ?  p.height() + ul.outerHeight() : t.outerHeight() )
		},
		300
	);

});

$(custom_select)
.on('click','a', function(){
	var t = $(this);
	var c = t.parent().parent().parent();
	var p = $('p', c);

	c
	.stop()
	.animate(
		{
			height : p.outerHeight()
		},
		300,
		function(){
			p.find('strong').text(t.attr('title'));
			$('li.n', c).removeClass('n');
			t.parent().addClass('n');
		}
	);
});
