$('.main-filters.sejururi select#filter-children').change(function(){
	var t = $(this);
	var ul = t.parent().parent().parent();

	$('li.children-ages', ul).remove();

	for( var i = 0; i < t.val(); i++ ){
		var select = $(document.createElement('select'));

		select
		.addClass('value')
		.prop('name', 'filter-children-ages[]')
		.data('index', i)
		.data('name', 'filter-children-ages');

		for (var j = 1; j < 17; j++) {
			select.append(
				$(document.createElement('option'))
				.prop('value', j)
				.text(j)
			);
		}

		ul.append(
			$(document.createElement('li'))
			.addClass('children-ages')
			.append(
				$(document.createElement('p'))
				.addClass('custom-label')
				.text('Varsta copil')
			)
			.append(select)
		);
	}
})