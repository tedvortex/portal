var pricesSelect = $('div#prices-select');
var priceIntervals = $('div#price-intervals');
var pricesCalendarController = $('div#prices-calendar-controller');
var itemOrderContainer = $('div#item-order-container');

$('ul.tabs-list p').click(function() {
	var p = $(this).parent();
	var tabs_list = p.parent();
	var tabs = $('ul#' + tabs_list.data('tabs'));

	if (!p.hasClass('a')) {
		$('>li.a', tabs_list).removeClass('a');

		p.addClass('a');

		$('>li.a', tabs).removeClass('a').addClass('n');

		$('>li', tabs).eq(p.index()).removeClass('n').addClass('a');
	}
});

$(document).on('change', 'div#prices-select select', function() {
	var pricesSelect = $('div#prices-select'),
		priceIntervals = $('div#price-intervals'),
		pricesCalendarController = $('div#prices-calendar-controller'),
		pricesCalendar = $('table#prices-calendar'),
		data = {
			module : 'croaziera',
			id : pricesSelect.data('id'),
			index : $('div#prices-calendar-controller strong.active').data('index'),
			date_start : $('input:eq(' + $('strong.active', pricesCalendarController).data('index') + ')', priceIntervals).val()
		};

	$('div#prices-select select').each(function(index, element) {
		var t = $(element);

		if (t.hasClass('multiple')) {
			if (typeof(data[t.data('name')]) == 'undefined') {
				data[t.data('name')] = [];
			}

			eval("data." + t.data('name') + "[" + t.data('index') + "] = '" + t.val() + "';");
		} else {
			eval("data." + t.prop('name') + " = '" + t.val() + "';");
		}
	});

	$.ajax({
		type: "GET",
		url: 'http://' + window.location.host + '/ajax.json',
		data: data,
		dataType: "json",
		success: function(html) {
			if (typeof(html.table) != 'undefined') {
				$('div#prices-calendar-container').html(unescape(html.table));
			} else {
				$('div > span + span', pricesCalendar).remove();
			}
		}
	});
}).on('click', 'div#prices-calendar-controller span.left', function(){
	var priceIntervals = $('div#price-intervals'),
		pricesCalendarController = $('div#prices-calendar-controller'),
		t = $('strong.active', pricesCalendarController),
        max = parseInt($('input', priceIntervals).length),
        ind = t.data('index');

	if (ind > 0) {
		var next = $('input:eq(' + (ind - 1) + ')', priceIntervals);

		if (next.length > 0) {
			t.html(next.data('locale'));
			t.data('index', next.data('index'));

			$('select:first', pricesSelect).trigger('change');
		}
	}
}).on('click', 'div#prices-calendar-controller span.right', function(){
	var priceIntervals = $('div#price-intervals'),
		pricesCalendarController = $('div#prices-calendar-controller'),
		t = $('strong.active', pricesCalendarController),
        max = parseInt($('input', priceIntervals).length),
        ind = t.data('index');

	if (ind < max) {
		var next = $('input:eq(' + (ind + 1) + ')', priceIntervals);

		if (next.length > 0) {
			t.html(next.data('locale'));
			t.data('index', next.data('index'));

			$('select:first', pricesSelect).trigger('change');
		}
	}
}).on('click', 'table#prices-calendar div.exist', function(){
	var priceIntervals = $('div#price-intervals'),
		pricesCalendar = $('table#prices-calendar'),
		t = $(this);
	
	$('div.exist', pricesCalendar).removeClass('active');
	
	t.addClass('active');
	
	if (parseInt(t.data('price')) > 0) {
		$('a.button', itemOrderContainer).removeClass('n');
	} else {
		$('a.button', itemOrderContainer).addClass('n');
	}
}).on('click', 'div#item-order-container a.button', function(){
	var t = $(this),
		pricesIntervals = $('div#price-intervals'),
		pricesCalendar = $('table#prices-calendar'),
		active = $('div.active', pricesCalendar);
	
	pricesIntervals.remove();
	
	$('input#checkin').val(active.data('date'));
	
	window.location = t.data('url') + '?' + $('form#prices-select-form').serialize();
	
	return false;
}).on('change', 'select#room', function(){
	var t = $(this)
		pricesSelect = $('div#prices-select');

	$('select#adults option').removeClass('n');
	
	$('select#adults option').each(function(ind, el){
		el = $(el);

		if (parseInt(el.val()) > parseInt(t.find('option:selected').data('adults'))) {
			el.addClass('n');
		}
	});
});

$('select:first', pricesSelect).trigger('change');