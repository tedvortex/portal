require(['products'], function(){
	/*$('select#children').change(function(){
		var t = $(this);
		var v = parseInt(t.val());

		$('.children-age').each(function(){
			var t = $(this);
			var i = parseInt(t.prop('id').split('-')[1]);

			if (i >= v) {
				t.addClass('n');
			} else {
				t.removeClass('n');
			}
		});
	});*/
	
	$(document).on('click', 'a.element-unlocker', function(){
		var element = $(this).parent().parent().parent().parent().parent().parent();
		
		if (element.data('open')) {
			$('>li:not(:first)', element).addClass('n');
			element.data('open', false);
		} else {
			$('>li:not(:first)', element).removeClass('n');
			element.data('open', true);
		}
		
		return false;
	});
	
	$('select.rooms-total').change(function(){
		var t = $(this);
		var c = $('#'+t.data('container'));
		var v = t.val();

		var current_rooms = $('>div.room', c).length;

		if( current_rooms > v ){
			for( i = current_rooms - 1; i >= v; i-- ){
				$('div.room-'+i, c).remove();
			}
		}
		else if( v > current_rooms){
			for( i = current_rooms; i < v ; i++ ){

				var select_adults =
				$(document.createElement('select'))
				.addClass('value room-adults')
				.prop('name', 'people['+i+'][adults]');

				for( var j = 1; j < 10; j++ ){
					select_adults.append(
						$(document.createElement('option'))
						.prop('value', j)
						.text(j + ' AD')
					);
				}

				var select_children =
				$(document.createElement('select'))
				.addClass('value room-children')
				.prop('name', 'people['+i+'][children]');

				for( var j = 0; j < 10; j++ ){
					select_children.append(
						$(document.createElement('option'))
						.prop('value', j)
						.text(j + ' CH')
					);
				}

				c.append(
					$(document.createElement('div'))
					.addClass('room room-'+i)
					.append(
						$(document.createElement('ul'))
						.addClass('nav cf shorts person fix')
						.append(
							$(document.createElement('li'))
							.addClass('room-order')
							.append(
								$(document.createElement('p'))
								.addClass('custom-label')
								.text('Camera '+(i+1)+':')
							)
						)
						.append(
							$(document.createElement('li'))
							.append(
								$(document.createElement('p'))
								.addClass('custom-label')
								.text('Nr. adulti')
							)
							.append(
								$(document.createElement('div'))
								.addClass('custom-select')
								.append(select_adults)
							)
						)
						.append(
							$(document.createElement('li'))
							.append(
								$(document.createElement('p'))
								.addClass('custom-label')
								.text('Nr. copii')
							)
							.append(
								$(document.createElement('div'))
								.addClass('custom-select')
								.append(select_children)
							)
						)
					)
				);
			}
		}
	});

	require(['window'], function(Window){
		$('.rooms-container').on('change', 'select.room-adults, select.rooms-total', function(){
			var t = $(this);
			var adults = 0;

			if( t.hasClass('room-adults')  ){
				var c = t.parent().parent().parent().parent().parent();
			}
			else{
				var c = $('#'+t.data('container'));
			}

			$('select.room-adults', c).each(function(i, v){
				adults += parseInt($(v).val());
			});

			if( adults > 9 ){
				Window.init('Va rugam sa verificati numar total de adulti. Fiecare rezervare permite in total 9 adulti. Va multumim!', 'Informare', 'information');
			}
		});
	});

	$('.rooms-container').on('change', 'select.room-children', function(){
		var t = $(this);
		var ul = t.parent().parent().parent();

		$('li.children-age', ul).remove();

		for( var i = 0; i < t.val(); i++ ){

			var select = $(document.createElement('select'));

			select
			.addClass('value')
			.prop('name', t.prop('name').replace('children', 'children-ages') + '[]');

			for(var j = 1; j < 17; j++){
				select.append(
					$(document.createElement('option'))
					.prop('value', j)
					.text(j)
				);
			}

			ul.append(
				$(document.createElement('li'))
				.addClass('children-age')
				.append(
					$(document.createElement('p'))
					.addClass('custom-label')
					.text('Varsta copil')
				)
				.append(
					$(document.createElement('div'))
					.addClass('custom-select')
					.append(select)
				)
			);
		}
	});
});