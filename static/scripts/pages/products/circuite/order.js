$('input[name="sejur_reservation"]').val(new Date().getTime());

$('ul.tabs-list p').click(function() {
	var p = $(this).parent();
	var tabs_list = p.parent();
	var tabs = $('#' + tabs_list.data('tabs'));

	if( !tabs_list.hasClass('form-tabs-list') ){
		if (!p.hasClass('a')) {
			$('>li.a', tabs_list).removeClass('a');
			p.addClass('a');

			$('>li.a', tabs).removeClass('a').addClass('n');

			$('>li', tabs).eq(p.index()).removeClass('n').addClass('a');
		}
	}
	else{
		if (!p.hasClass('a')) {

			$('>li.a', tabs_list).removeClass('a');
			p.addClass('a');
			$('input', p).prop('checked', 'checked');

			var current = $('>div', tabs);

			current.appendTo('#order-forms-storage');

			$('#' + p.data('tab')).appendTo(tabs);
		}
	}
});

var checkbox = $('span.term-checkbox');
var terms = $('div#termsandconditions');
$(checkbox)
.on('click','span',function(){
	var t = $(this);

		if (
			t.hasClass('normal')
		){
			t.removeClass('normal')
			t.addClass('active')
			terms.find('input[type="checkbox"]').prop('checked', true)
		} else if (
			t.hasClass('active')
		){
			t.removeClass('active')
			t.addClass('normal')
			terms.find('input[type="checkbox"]').prop('checked', false);
		}
	}
)

$('.open-circle').hover(
	function(){
		var t = $(this);
		t.parent().find('.'+( t.hasClass('open-info') ? 'info-box' : 'help-box' )).fadeIn(200);
	},
	function(){
		var t = $(this);
		t.parent().find('.'+( t.hasClass('open-info') ? 'info-box' : 'help-box' )).fadeOut(100);
	}
);


var form_inv = $('#form-individual');
var form_com = $('#form-company');

$('#person-type input[type="radio"]').click(function(){
	var val = $(this).val();

	if(val == 0){
		form_inv.appendTo('#person-type-container');
		form_com.appendTo('#order-forms-storage');
	}
	else{
		form_inv.appendTo('#order-forms-storage');
		form_com.appendTo('#person-type-container');
	}
});

$('select#country', form_inv).change(function(){

	if($(this).val() != 183){
		$('label[for="cnp"]', form_inv).text('PIN / Passport No.');
	}
	else{
		$('label[for="cnp"]', form_inv).text('CNP');
	}
});

$('select#country', form_com)
.change(function(){

	var rc = $('dl.reg-com', form_com);

	if($(this).val() != 183){
		$('label[for="company_cui"]', form_com).text('Numar inregistrare (VAT number)');

		$('dt, dd', rc).addClass('n');

	}
	else{
		$('label[for="company_cui"]', form_com).text('CUI');

		$('dt, dd', rc).removeClass('n');
	}
})
.trigger('change');

var checkin = $('div#checkin-date').data('checkin');

require(["datepick-ext"], function(){

	$.each( $('input.order-datepick'), function(i, v){

		v = $(v);

		var current_year = new Date().getFullYear();

		var start_year = current_year - 100;
		var end_year = current_year;

		if( v.data('age') > 0 ){

			start_year = current_year - v.data('age');
			end_year = start_year - 1;
		}

		v.datepick({
			dateFormat: 'dd.mm.yyyy',
			yearRange: start_year + ':' + end_year,
			defaultDate: new Date(start_year, 1-1, 1),
			monthsToShow: 1,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function(){

				var t = $(this);

				if( t.hasClass('child') ){

					$.ajax({
						type: 'get',
						url: 'http://' + window.location.host + '/ajax.json',
						dataType: 'jsonp',
						data: {
							module: 'age',
							birthdate: t.val(),
							checkin: checkin
						},
						success: function(data) {

							if( t.data('age') != data.age ){
								t.parent().parent().find('.open-info').removeClass('n');
							}
							else{
								t.parent().parent().find('.open-info').addClass('n');
							}

						}
					});
				}

			}
		});
	});

});