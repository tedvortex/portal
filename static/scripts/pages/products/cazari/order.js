$('select.rooms-total').change(function(){
	var t = $(this);
	var c = $('#'+t.data('container'));
	var v = t.val();

	var current_rooms = $('>div.room', c).length;

	if( current_rooms > v ){
		for( i = current_rooms - 1; i >= v; i-- ){
			$('div.room-'+i, c).remove();
		}
	}
	else if( v > current_rooms){
		for( i = current_rooms; i < v ; i++ ){

			var select_adults =
			$(document.createElement('select'))
			.addClass('value room-adults')
			.prop('name', 'people['+i+'][adults]');

			for( var j = 1; j < 10; j++ ){
				select_adults.append(
					$(document.createElement('option'))
					.prop('value', j)
					.text(j + ' AD')
				);
			}

			var select_children =
			$(document.createElement('select'))
			.addClass('value room-children')
			.prop('name', 'people['+i+'][children]');

			for( var j = 0; j < 10; j++ ){
				select_children.append(
					$(document.createElement('option'))
					.prop('value', j)
					.text(j + ' CH')
				);
			}

			c.append(
				$(document.createElement('div'))
				.addClass('room room-'+i)
				.append(
					$(document.createElement('ul'))
					.addClass('nav cf shorts person fix')
					.append(
						$(document.createElement('li'))
						.addClass('room-order')
						.append(
							$(document.createElement('p'))
							.addClass('custom-label')
							.text('Camera '+(i+1)+':')
						)
					)
					.append(
						$(document.createElement('li'))
						.append(
							$(document.createElement('p'))
							.addClass('custom-label')
							.text('Nr. adulti')
						)
						.append(
							$(document.createElement('div'))
							.addClass('custom-select')
							.append(select_adults)
						)
					)
					.append(
						$(document.createElement('li'))
						.append(
							$(document.createElement('p'))
							.addClass('custom-label')
							.text('Nr. copii')
						)
						.append(
							$(document.createElement('div'))
							.addClass('custom-select')
							.append(select_children)
						)
					)
				)
			);
		}
	}
});

require(['window'], function(Window){
	$('.rooms-container').on('change', 'select.room-adults, select.rooms-total', function(){
		var t = $(this);
		var adults = 0;

		if( t.hasClass('room-adults')  ){
			var c = t.parent().parent().parent().parent().parent();
		}
		else{
			var c = $('#'+t.data('container'));
		}

		$('select.room-adults', c).each(function(i, v){
			adults += parseInt($(v).val());
		});

		if( adults > 9 ){
			Window.init('Va rugam sa verificati numar total de adulti. Fiecare rezervare permite in total 9 adulti. Va multumim!', 'Informare', 'information');
		}
	});
});

$('.rooms-container').on('change', 'select.room-children', function(){
	var t = $(this);
	var ul = t.parent().parent().parent();

	$('li.children-age', ul).remove();

	for( var i = 0; i < t.val(); i++ ){

		var select = $(document.createElement('select'));

		select
		.addClass('value')
		.prop('name', t.prop('name').replace('children', 'children-ages') + '[]');

		for(var j = 1; j < 17; j++){
			select.append(
				$(document.createElement('option'))
				.prop('value', j)
				.text(j)
			);
		}

		ul.append(
			$(document.createElement('li'))
			.addClass('children-age')
			.append(
				$(document.createElement('p'))
				.addClass('custom-label')
				.text('Varsta copil')
			)
			.append(
				$(document.createElement('div'))
				.addClass('custom-select')
				.append(select)
			)
		);
	}
});

$('input[name="sejur_reservation"]').val(new Date().getTime());

$('ul.tabs-list p').click(function() {
	var p = $(this).parent();
	var tabs_list = p.parent();
	var tabs = $('#' + tabs_list.data('tabs'));

	if( !tabs_list.hasClass('form-tabs-list') ){
		if (!p.hasClass('a')) {
			$('>li.a', tabs_list).removeClass('a');
			p.addClass('a');

			$('>li.a', tabs).removeClass('a').addClass('n');

			$('>li', tabs).eq(p.index()).removeClass('n').addClass('a');
		}
	}
	else{
		if (!p.hasClass('a')) {

			$('>li.a', tabs_list).removeClass('a');
			p.addClass('a');
			$('input', p).prop('checked', 'checked');

			var current = $('>div', tabs);

			current.appendTo('#order-forms-storage');

			$('#' + p.data('tab')).appendTo(tabs);
		}
	}
});

var checkbox = $('span.term-checkbox');
var terms = $('div#termsandconditions');
$(checkbox)
.on('click','span',function(){
	var t = $(this);

		if (
			t.hasClass('normal')
		){
			t.removeClass('normal')
			t.addClass('active')
			terms.find('input[type="checkbox"]').prop('checked', true)
		} else if (
			t.hasClass('active')
		){
			t.removeClass('active')
			t.addClass('normal')
			terms.find('input[type="checkbox"]').prop('checked', false);
		}
	}
)

$('.open-circle').hover(
	function(){
		var t = $(this);
		t.parent().find('.'+( t.hasClass('open-info') ? 'info-box' : 'help-box' )).fadeIn(200);
	},
	function(){
		var t = $(this);
		t.parent().find('.'+( t.hasClass('open-info') ? 'info-box' : 'help-box' )).fadeOut(100);
	}
);


var form_inv = $('#form-individual');
var form_com = $('#form-company');

$('#person-type input[type="radio"]').click(function(){
	var val = $(this).val();

	if(val == 0){
		form_inv.appendTo('#person-type-container');
		form_com.appendTo('#order-forms-storage');
	}
	else{
		form_inv.appendTo('#order-forms-storage');
		form_com.appendTo('#person-type-container');
	}
});

$('select#country', form_inv).change(function(){

	if($(this).val() != 183){
		$('label[for="cnp"]', form_inv).text('PIN / Passport No.');
	}
	else{
		$('label[for="cnp"]', form_inv).text('CNP');
	}
});

$('select#country', form_com)
.change(function(){

	var rc = $('dl.reg-com', form_com);

	if($(this).val() != 183){
		$('label[for="company_cui"]', form_com).text('Numar inregistrare (VAT number)');

		$('dt, dd', rc).addClass('n');

	}
	else{
		$('label[for="company_cui"]', form_com).text('CUI');

		$('dt, dd', rc).removeClass('n');
	}
})
.trigger('change');

var checkin = $('div#checkin-date').data('checkin');

require(["datepick-ext"], function(){

	$.each( $('input.order-datepick'), function(i, v){

		v = $(v);

		var current_year = new Date().getFullYear();

		var start_year = current_year - 100;
		var end_year = current_year - 18;

		if( v.data('age') > 0 ){

			start_year = current_year - v.data('age');
			end_year = start_year - 1;
		}

		v.datepick({
			dateFormat: 'dd.mm.yyyy',
			yearRange: start_year + ':' + end_year,
			defaultDate: new Date(start_year, 1-1, 1),
			monthsToShow: 1,
			renderer: $.datepick.themeRollerRenderer,
			onSelect: function(){

				var t = $(this);

				if( t.hasClass('child') ){

					$.ajax({
						type: 'get',
						url: 'http://' + window.location.host + '/ajax.json',
						dataType: 'jsonp',
						data: {
							module: 'age',
							birthdate: t.val(),
							checkin: checkin
						},
						success: function(data) {

							if( t.data('age') != data.age ){
								t.parent().parent().find('.open-info').removeClass('n');
							}
							else{
								t.parent().parent().find('.open-info').addClass('n');
							}

						}
					});
				}

			}
		});
	});
});