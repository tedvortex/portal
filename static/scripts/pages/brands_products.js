var tree = $('ul.categories-tree');

$('ul.categories-tree-members a', tree).click(function() {

	var t = $(this);
	var p = t.parent();
	var st = $('li#' + p.data('storage'));

	if (p.hasClass('has-storage')) {
		var selected = $('li.selected', tree);

		if (selected.length > 0) {
			if (!p.hasClass('selected')) {

				if( $('li#' + selected.data('storage')).length > 0 ){

					$('li#' + selected.data('storage')).stop().animate({
						height : 0
					}, 300, function() {
						selected.removeClass('selected');

						p.addClass('selected');

						st.stop().animate({
							height : $('>div', st).outerHeight() + 25
						}, 300);
					});
				}
				else{
					selected.removeClass('selected');

					p.addClass('selected');

					st.stop().animate({
						height : $('>div', st).outerHeight() + 25
					}, 300);
				}


			}
		} else {

			if (!p.hasClass('selected')) {
				p.addClass('selected');

				st.stop().animate({
					height : $('>div', st).outerHeight() + 25
				}, 300);
			}
		}
	}
});