	// Globals variables //
	var _stop = 1, _dragHandle = "", _dragHandleStyle, tX, tY, HSV = {0: 360,1: 0,2: 100}, hSV = 149, wSV = 140, _colorpOffset = {x: 0, y: 0}, _canDragPicker = false, _colorpdisplay = "none", _colorpisDraggable = true, _colorpposition = "absolute";
	var _hueTrackHeight = 147, _clickMouseLocale = "", _clickMouseLocalecolorpIinnerShell = false, _refreshTimeout = 0, _colorpinitHex = "", _colorpabsPos = "";
	var _colorpbasicLimits = {"colorpbasicHex": [0,16777215],"colorpbasicR": [0,255],"colorpbasicG": [0,255],"colorpbasicB": [0,255],"colorpbasicH": [0,359],"colorpbasicS": [0,100],"colorpbasicV": [0,100],"colorpbasicC": [0,100],"colorpbasicM": [0,100],"colorpbasicY": [0,100],"colorpbasicK": [0,100]};

	Array.prototype.colorpin_array = function(_needle) {
		var _i;
		for (_i=0;_i<=this.length;_i++ ) {
			if (this[_i] == _needle) {
				return true;
			}
		}
		return false;
	}

	String.prototype.colorppadHex = function() {
		var _str = ("000000".toString() + this.toString());
		return _str.substring((_str.length - 6),_str.length); // THANKS IE!!!!
	}

	colorpdec2hex = function(_dec) {
		var _hexChars = "0123456789ABCDEF", _hex = "";
		while (_dec > 15) {
			_hex = (_hexChars.charAt((_dec - (Math.floor(_dec / 16)) * 16)) + _hex);
			_dec = Math.floor(_dec / 16);
		}
		return (_hexChars.charAt(_dec) + _hex);
	}

	colorphex2dec = function(_hex) {
		return parseInt(_hex,16);
	}

	if (typeof $G != "function") {
		$G = function(_elementID) {
			return document.getElementById(_elementID);
		}
	}

	if (typeof $S != "function") {
		$S = function(_elementID) {
			return $G(_elementID).style;
		}
	}

	colorpisHex = function(_hex) {
		return /^[a-fA-F0-9]{6}$/.test(_hex);
	}

	colorpdomEvent = function(_event) {
		_event = (_event) ? _event : window.event;

		// Opera hates this for some reason:
		if (!window.opera && _event.srcElement) {
			_event.target = _event.srcElement;
		}
		if (_event.keyCode) {
			_event.code = _event.keyCode;
		} else if (_event.which) {
			_event.code = _event.which;
		} else {
			_event.code = _event.charCode
		}
		return _event;
	}

	colorpabsPos = function(_object) {
		var r = {
			x : _object.offsetLeft,
			y : _object.offsetTop
		};
		if (_object.offsetParent) {
			var v = colorpabsPos(_object.offsetParent);
			r.x += v.x;
			r.y += v.y;
		}
		return r;
	}

	colorpinitPicker = function() {
		document.onmousemove = colorpMouseHandlerOnMouseMove;
		document.onmouseup = colorpMouseHandlerOnMouseUp;

		$S("colorp").left = 0;
		$S("colorp").top = 0;
		$S("colorp").width = "262px";
		$S("colorp").height = "268px";
		$G("colorp").innerHTML = "<div id=\"colorpDragDiv\" class=\"colorpDragDiv\"><div id=\"colorpIinnerShell\" class=\"colorpIinnerShell\"><div id=\"colorpPicker\" class=\"colorpPicker\"><div id=\"colorpSatVal\" class=\"colorpSatVal\" onmousedown=\"colorpslide('colorpSatValSlide','colorpPicker',event);\"><div id=\"colorpSatValSlide\" class=\"colorpSatValSlide\"></div></div><div id=\"colorpHueContainer\" class=\"colorpHueContainer\" onmousedown=\"colorpslide('colorpHueSlide','colorpPicker',event);\"><div id=\"colorpHueSlide\" class=\"colorpHueSlide\" style=\"top: -4px; left: -7px;\"></div><div class=\"colorpHue\"></div></div><div id=\"colorpSatContainer\" class=\"colorpSatContainer\" onmousedown=\"colorpslide('colorpSatSlide','colorpPicker',event);\"><div id=\"colorpSatSlide\" class=\"colorpSatSlide\" style=\"top: -4px; left: -7px;\"></div><div id=\"colorpSat\" class=\"colorpSat\"></div></div><div id=\"colorpValContainer\" class=\"colorpValContainer\" onmousedown=\"colorpslide('colorpValSlide','colorpPicker',event);\"><div id=\"colorpValSlide\" class=\"colorpValSlide\" style=\"top: -4px; left: -7px;\"></div><div id=\"colorpVal\" class=\"colorpVal\"></div></div></div><div class=\"colorpbPE\"><div style=\"width: 48px; margin-right: 10px; background: url(http://"+window.location.host+"static/images/hex.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/r.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/g.png) center top no-repeat;\"></div><div style=\"margin-right: 11px; background: url(http://"+window.location.host+"static/images/b.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/h.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/s.png) center top no-repeat;\"></div><div style=\"margin-right: 0; background: url(http://"+window.location.host+"static/images/v.png) center top no-repeat;\"></div><div class=\"colorpClear colorpNoDim\"></div><input type=\"text\" id=\"colorpbasicHex\" maxlength=\"6\" value=\"\" style=\"width: 48px; margin-right: 12px; background: url(http://"+window.location.host+"static/images/formFieldLargeBG.png) no-repeat;\" /><input type=\"text\" id=\"colorpbasicR\" maxlength=\"3\" /><input type=\"text\" id=\"colorpbasicG\" maxlength=\"3\" /><input type=\"text\" id=\"colorpbasicB\" maxlength=\"3\" style=\"margin-right: 13px;\" /><input type=\"text\" id=\"colorpbasicH\" maxlength=\"3\" /><input type=\"text\" id=\"colorpbasicS\" maxlength=\"3\" /><input type=\"text\" id=\"colorpbasicV\" maxlength=\"3\" style=\"margin-right: 0;\" /><div class=\"colorpClear\" style=\"width: 242px; height: 15px; margin: 0; font-size: 0;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/c.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/m.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/y.png) center top no-repeat;\"></div><div style=\"background: url(http://"+window.location.host+"static/images/k.png) center top no-repeat;\"></div><div class=\"colorpClear colorpNoDim\"></div><div><input type=\"text\" id=\"colorpbasicC\" maxlength=\"3\" /></div><div><input type=\"text\" id=\"colorpbasicM\" maxlength=\"3\" /></div><div><input type=\"text\" id=\"colorpbasicY\" maxlength=\"3\" /></div><div><input type=\"text\" id=\"colorpbasicK\" maxlength=\"3\" /></div><img src=\"http://"+window.location.host+"static/images/closeBtn.png\" style=\"float: left; width: 13px; height: 13px; padding: 7px 0 0 12px; cursor: pointer;\" onclick=\"colorphidePicker();\" alt=\"Close\" /></div></div></div>";
		$G("colorpIinnerShell").onmousedown = function(_event) {
			_clickMouseLocalecolorpIinnerShell = true;
			_canDragPicker = false;
		}
		$G("colorpDragDiv").onmousedown = function() {
			if (_clickMouseLocalecolorpIinnerShell == false) {
				_canDragPicker = true;
				_colorpOffset.x = (_mouseCoords[0] - parseInt($S("colorp").left));
				_colorpOffset.y = (_mouseCoords[1] - parseInt($S("colorp").top));
			}
		}

		if ((_colorpdisplay == "block") || (_colorpdisplay == "none")) {
			$S("colorp").display = _colorpdisplay;
		}
		if (_colorpposition != "absolute") {
			$S("colorp").position = "relative";
		}
		_colorpinitHex = (colorpisHex(_colorpinitHex)) ? _colorpinitHex : "ffffff";
		_colorpabsPos = colorpabsPos($G("colorp"));
		$G("colorpbasicHex").value = _colorpinitHex;
		colorpupdateBasicFromForm(null,"colorpbasicHex");

		var _btns = ["colorpbasicHex","colorpbasicR","colorpbasicG","colorpbasicB","colorpbasicH","colorpbasicS","colorpbasicV","colorpbasicC","colorpbasicM","colorpbasicY","colorpbasicK"];
		for (_i=0;_i<_btns.length;_i++) {
			if (["colorpbasicC","colorpbasicM","colorpbasicY","colorpbasicK"].colorpin_array(_btns[_i]) == false) {
				$G(_btns[_i]).onblur = $G(_btns[_i]).onkeyup = function(_event) {
					_event = (_event) ? colorpdomEvent(_event) : colorpdomEvent(window.event);
					colorpupdateBasicFromForm(_event,_event.target.id,true);
				}
			}

			$G(_btns[_i]).onkeydown = function(_event) {
				_event = (_event) ? colorpdomEvent(_event) : colorpdomEvent(window.event);
				var _val = $G(_event.target.id).value;
				if ([40,38].colorpin_array(_event.code)) {
					_val = (_event.target.id == "colorpbasicHex") ? colorphex2dec(_val) : parseInt(_val);
					(_event.code == 40) ? ((colorpwithin(--_val,_colorpbasicLimits[_event.target.id][0],_colorpbasicLimits[_event.target.id][1])) ? "" : _val++) : ((colorpwithin(++_val,_colorpbasicLimits[_event.target.id][0],_colorpbasicLimits[_event.target.id][1])) ? "" : _val--);
					_val = (_event.target.id == "colorpbasicHex") ? colorpdec2hex(_val).toString().colorppadHex() : _val;
					$G(_event.target.id).value = _val;
				}
				if ([13,40,38].colorpin_array(_event.code)) {
					colorpupdateBasicFromForm(_event,_event.target.id);
				}
			}
		}
	}

	colorpDragPicker = function(_event) {
		if (_colorpisDraggable == true) {
			var _xOffset = (_mouseCoords[0] - _colorpOffset.x);
			if ($S("colorp").position == "absolute") {
				_xOffset = (_xOffset < 0) ? 0 : _xOffset;
			}
			var _yOffset = (_mouseCoords[1] - _colorpOffset.y);
			if ($S("colorp").position == "absolute") {
				_yOffset = (_yOffset < 0) ? 0 : _yOffset;
			}
			$S("colorp").left = (_xOffset + "px");
			$S("colorp").top = (_yOffset + "px");
		}
	}

	colorpMouseHandlerOnMouseMove = function(_event) {
		var _scrollTop = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
		var _scrollLeft = (document.documentElement.scrollLeft) ? document.documentElement.scrollLeft : document.body.scrollLeft;

		_event = (_event) ? _event : event;
		_mouseCoords = (document.all) ? [event.clientX + _scrollLeft,event.clientY + _scrollTop] : [_event.pageX,_event.pageY];

		if (_clickMouseLocale == "colorpPicker") {
			colorpdrag(_event);
		} else if (_canDragPicker) {
			colorpDragPicker(_event);
		}
	}

	colorpMouseHandlerOnMouseUp = function(_event) {
		document.onselectstart = function() {
			return true;
		}

		if (_clickMouseLocale == "colorpPicker") {
			_stop = 1;
		}
		_clickMouseLocale = "";
		_canDragPicker = false;
		_clickMouseLocalecolorpIinnerShell = false;
	}

	colorpshowPicker = function(_obj) {
		var _x = (_obj && _obj._x) ? _obj._x : _mouseCoords[0];
		var _y = (_obj && _obj._y) ? _obj._y : _mouseCoords[1];

		if ($S("colorp").position == "relative") {
			_x -= _colorpabsPos.x;
			_y -= _colorpabsPos.y;
		}

		if (_obj && colorpisHex(_obj._hex)) {
			$G("colorpbasicHex").value = _obj._hex;
			colorpupdateBasicFromForm(null,"colorpbasicHex");
		}

		$S("colorp").left = (_x + "px");
		$S("colorp").top = (_y + "px");
		$S("colorp").display = "block";
	}

	colorphidePicker = function() {
		$S("colorp").display = "none";
	}

	colorpagent = function(_agent) {
		return Math.max(navigator.userAgent.toLowerCase().indexOf(_agent),0);
	}

	colorpslide = function(_dH,_object,_event) {
		_clickMouseLocale = "colorpPicker";
		document.onselectstart = function() {
			return false;
		}
		if (_stop) {
			_stop = 0;

			_dragHandleStyle = $S(_dH);
			_dragHandle = _dH;
			_absolutePosition = colorpabsPos($G(_object)), tX, tY;
			_absolutePosition.x += 7;
			_absolutePosition.y += 7;

			colorpdrag(_event);
		}
	}

	colorpXY = function(_event) {
		var _scrollTop = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
		var _scrollLeft = (document.documentElement.scrollLeft) ? document.documentElement.scrollLeft : document.body.scrollLeft;
		return colorpagent("msie") ? [event.clientX + _scrollLeft,event.clientY + _scrollTop] : [_event.pageX,_event.pageY];
	}

	colorptXY = function(_event) {
		tY = colorpXY(_event)[1] - _absolutePosition.y;
		tX = colorpXY(_event)[0] - _absolutePosition.x;
	}

	colorpckHSV = function(a,b,_dragHandle) {
		if (colorpwithin(a,0,b)) {
			return a;
		} else if (a > b) {
			return b;
		} else if (a < 0) {
			if (_dragHandle == "colorpbasicSatValSlide") {
				return -3;
			} else {
				return -4;
			}
		}
	}

	colorpwithin = function(_val,_low,_high) {
		return ((_val >= _low) && (_val <= _high));
	}

	colorpmkHSV = function(a,b,c) {
		return Math.min(a,Math.max(0,Math.ceil((parseInt(c) / b) * a)));
	}

	colorpdrag = function(_event) {
		_event = (!_event) ? window.event : _event;
		if (!_stop) {
			colorptXY(_event);

			clearTimeout(_refreshTimeout);
			if (_dragHandle == "colorpSatValSlide") {
				_dragHandleStyle.left = colorpckHSV(tX,wSV,_dragHandle) + "px";
				_dragHandleStyle.top = colorpckHSV(tY,wSV,_dragHandle) + "px";

				$S("colorpSatSlide").top = Math.floor((colorpckHSV(tX,wSV,"colorpSatValSlide") / wSV) * _hueTrackHeight) + "px";
				$S("colorpValSlide").top = Math.floor((colorpckHSV(tY,wSV,"colorpSatValSlide") / wSV) * _hueTrackHeight) + "px";
				HSV[1] = colorpmkHSV(100,wSV,_dragHandleStyle.left);
				HSV[2] = 100 - colorpmkHSV(100,wSV,_dragHandleStyle.top);
			} else if (_dragHandle == "colorpHueSlide") {
				var ck = colorpckHSV(tY,_hueTrackHeight,_dragHandle);
				_dragHandleStyle.top = (ck + "px");
				HSV[0] = colorpmkHSV(360,_hueTrackHeight,ck);
			} else if (_dragHandle == "colorpSatSlide") {
				var ck = colorpckHSV(tY,_hueTrackHeight,_dragHandle);
				_dragHandleStyle.top = (ck + "px");
				$S("colorpSatValSlide").left = Math.floor((colorpckHSV(tY,_hueTrackHeight,"colorpSatValSlide") / _hueTrackHeight) * wSV) + "px";
				HSV[1] = colorpmkHSV(100,_hueTrackHeight,ck);
			} else if (_dragHandle == "colorpValSlide") {
				var ck = colorpckHSV(tY,_hueTrackHeight,_dragHandle);
				_dragHandleStyle.top = (ck + "px");
				$S("colorpSatValSlide").top = Math.floor((colorpckHSV(tY,_hueTrackHeight,"colorpSatValSlide") / _hueTrackHeight) * wSV) + "px";
				HSV[2] = (100 - colorpmkHSV(100,_hueTrackHeight,ck));
			}

			$S("colorpSatVal").backgroundColor = $S("colorpSat").backgroundColor = $S("colorpVal").backgroundColor = ("#" + colorphsv2hex([HSV[0],100,100]));
			if (colorpHandler) {
				colorpHandler(colorphsv2hex(HSV));
			}
			colorpupdateBasicFormElements();
		}
	}

	colorpupdateBasicFormElements = function(_except) {
		if (_except != "colorpbasicHex") {
			$G("colorpbasicHex").value = colorphsv2hex(HSV);
		}

		var _rgb = colorphsv2rgb(HSV);
		if (_except != "colorpbasicR") {
			$G("colorpbasicR").value = parseInt(_rgb[0]);
		}
		if (_except != "colorpbasicG") {
			$G("colorpbasicG").value = parseInt(_rgb[1]);
		}
		if (_except != "colorpbasicB") {
			$G("colorpbasicB").value = parseInt(_rgb[2]);
		}

		if (_except != "colorpbasicH") {
			$G("colorpbasicH").value = parseInt(HSV[0]);
		}
		if (_except != "colorpbasicS") {
			$G("colorpbasicS").value = parseInt(HSV[1]);
		}
		if (_except != "colorpbasicV") {
			$G("colorpbasicV").value = parseInt(HSV[2]);
		}

		var _cmyk = colorprgb2cmyk(colorphsv2rgb(HSV));
		if (_except != "colorpbasicC") {
			$G("colorpbasicC").value = parseInt(_cmyk[0]);
		}
		if (_except != "colorpbasicM") {
			$G("colorpbasicM").value = parseInt(_cmyk[1]);
		}
		if (_except != "colorpbasicY") {
			$G("colorpbasicY").value = parseInt(_cmyk[2]);
		}
		if (_except != "colorpbasicK") {
			$G("colorpbasicK").value = parseInt(_cmyk[3]);
		}
	}

	colorpupdateBasicFromForm = function(_event,_elementID) {
		if (_elementID == "") {
			_event = (_event) ? _event : window.event;
			if (_event.srcElement) {
				_event.target = _event.srcElement;
			}
			var _id = _event.target.id;
		} else {
			var _id = _elementID;
		}
		var _val = $G(_id).value, _canUpdate = false, _skipHSV = false;

		if (_id == "colorpbasicHex") {
			if (colorpisHex(_val)) {
				HSV = colorprgb2hsv(colorphex2rgb(_val));
				_canUpdate = true, _skipHSV = true;
			}
		} else if (_id == "colorpbasicR") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,255)) {
				var _rgb = colorphsv2rgb(HSV);
				HSV = colorprgb2hsv([_val,_rgb[1],_rgb[2]]);
				_canUpdate = true;
			}
		} else if (_id == "colorpbasicG") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,255)) {
				var _rgb = colorphsv2rgb(HSV);
				HSV = colorprgb2hsv([_rgb[0],_val,_rgb[2]]);
				_canUpdate = true;
			}
		} else if (_id == "colorpbasicB") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,255)) {
				var _rgb = colorphsv2rgb(HSV);
				HSV = colorprgb2hsv([_rgb[0],_rgb[1],_val]);
				_canUpdate = true;
			}
		} else if (_id == "colorpbasicH") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,360)) {
				HSV = [_val,HSV[1],HSV[2]]
				_canUpdate = true;
			}
		} else if (_id == "colorpbasicS") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,360)) {
				HSV = [HSV[0],_val,HSV[2]]
				_canUpdate = true;
			}
		} else if (_id == "colorpbasicV") {
			_val = parseInt(_val);
			if (colorpwithin(_val,0,360)) {
				HSV = [HSV[0],HSV[1],_val]
				_canUpdate = true;
			}
		} else if ((_id == "colorpbasicC") || (_id == "colorpbasicM") || (_id == "colorpbasicY") || (_id == "colorpbasicK")) {
			_c = parseInt($G("colorpbasicC").value);
			_m = parseInt($G("colorpbasicM").value);
			_y = parseInt($G("colorpbasicY").value);
			_k = parseInt($G("colorpbasicK").value);

			if (_id == "colorpbasicC") {
				_c = (colorpwithin(_c,0,100)) ? _c : 0;
			} else if (_id == "colorpbasicM") {
				_m = (colorpwithin(_m,0,100)) ? _m : 0;
			} else if (_id == "colorpbasicY") {
				_y = (colorpwithin(_y,0,100)) ? _y : 0;
			} else if (_id == "colorpbasicK") {
				_k = (colorpwithin(_k,0,100)) ? _k : 0;
			}
			HSV = colorprgb2hsv(colorpcmyk2rgb([_c,_m,_y,_k]));
			_canUpdate = true;
		}

		if (_canUpdate) {
			var _hueSliderY = colorph2y(HSV[0]);
			var _satSliderY = colorps2y(HSV[1]);
			var _valSliderY = colorpv2y(HSV[2]);

			$S("colorpHueSlide").top = Math.floor(colorpckHSV((_hueSliderY - 3),_hueTrackHeight + 3,"colorpHueSlide")) + "px";
			$S("colorpSatSlide").top = Math.floor(colorpckHSV((_satSliderY - 3),_hueTrackHeight + 3,"colorpSatSlide")) + "px";
			$S("colorpValSlide").top = Math.floor(colorpckHSV((_valSliderY - 3),_hueTrackHeight + 3,"colorpValSlide")) + "px";

			$S("colorpSatValSlide").left = Math.ceil(colorpckHSV((_satSliderY - 7),wSV,"colorpSatValSlide")) + "px";
			$S("colorpSatValSlide").top = Math.ceil(colorpckHSV((_valSliderY - 7),wSV,"colorpSatValSlide")) + "px";
			$S("colorpSatVal").backgroundColor = $S("colorpSat").backgroundColor = $S("colorpVal").backgroundColor = ("#" + colorphsv2hex([HSV[0],100,100]));
			var _tmp = (_skipHSV == false) ? colorphsv2hex(HSV) : _val;

			if (colorpHandler) {
				colorpHandler(_tmp);
			}
			colorpupdateBasicFormElements(_id);
		}
	}


	/* CONVERSIONS */
	colorph2y = function(_hue) {
		return ((_hue / 360) * _hueTrackHeight);
	}
	colorps2y = function(_satVal) {
		return ((_satVal / 100) * _hueTrackHeight);
	}
	colorpv2y = function(_valVal) {
		return (Math.abs(((_valVal / 100) * (_hueTrackHeight + 3)) - _hueTrackHeight - 3));
	}
	colorphsv2hex = function(h) {
		return colorprgb2hex(colorphsv2rgb(h));
	}
	colorptoHex = function(v) {
		v=Math.round(Math.min(Math.max(0,v),255)); return("0123456789ABCDEF".charAt((v-v%16)/16)+"0123456789ABCDEF".charAt(v%16));
	}
	colorphex2rgb = function(r) {
		return({0:parseInt(r.substr(0,2),16),1:parseInt(r.substr(2,2),16),2:parseInt(r.substr(4,2),16)});
	}
	colorprgb2hex = function(r) {
		return(colorptoHex(r[0])+colorptoHex(r[1])+colorptoHex(r[2]));
	}
	colorprgb2hsv = function(r) {
		var max=Math.max(r[0],r[1],r[2]), delta=max-Math.min(r[0],r[1],r[2]), H, S, V;
		if(max!=0) {
			S=Math.round(delta/max*100);
			if(r[0]==max) H=(r[1]-r[2])/delta; else if(r[1]==max) H=2+(r[2]-r[0])/delta; else if(r[2]==max) H=4+(r[0]-r[1])/delta; var H=Math.min(Math.round(H*60),360); if(H<0) H+=360;
		}
		return({0:H?H:0,1:S?S:0,2:Math.round((max/255)*100)});
	}
	colorphsv2rgb = function(r) {
		var F, R, B, G, H=r[0]/360, S=r[1]/100, V=r[2]/100;
		if(S>0) {
			if(H>=1) H=0;
			H=6*H; F=H-Math.floor(H); A=Math.round(255*V*(1-S)); B=Math.round(255*V*(1-(S*F))); C=Math.round(255*V*(1-(S*(1-F)))); V=Math.round(255*V);
			switch(Math.floor(H)) {
				case 0: R=V; G=C; B=A; break;
				case 1: R=B; G=V; B=A; break;
				case 2: R=A; G=V; B=C; break;
				case 3: R=A; G=B; B=V; break;
				case 4: R=C; G=A; B=V; break;
				case 5: R=V; G=A; B=B; break;
			}
			return([R?R:0,G?G:0,B?B:0]);
		}
		else return([(V=Math.round(V*255)),V,V]);
	}
	colorprgb2cmyk = function(r) {
		var C=1-(r[0]/255), M=1-(r[1]/255), Y=1-(r[2]/255), K=Math.min(Y,Math.min(M,Math.min(C,1)));
		C=Math.round((C-K)/(1-K)*100);
		C = (isNaN(C)) ? 0 : C;
		M=Math.round((M-K)/(1-K)*100);
		M = (isNaN(M)) ? 0 : M;
		Y=Math.round((Y-K)/(1-K)*100);
		Y = (isNaN(Y)) ? 0 : Y;
		K=Math.round(K*100);
		return([C?C:0,M?M:0,Y?Y:0,K]);
	}
	colorpcmyk2rgb = function(r) {
		r[3]=r[3]/100; var R=(1-(r[0]/100*(1-r[3])+r[3]))*255, G=(1-(r[1]/100*(1-r[3])+r[3]))*255, B=(1-(r[2]/100*(1-r[3])+r[3]))*255;
		return([R,G,B]);
	}