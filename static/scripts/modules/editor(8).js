var _x=0;
var _y=0;
var l_zoom=1;
var s_zoom=1;
var dragObject  = null;
var mouse$_angle_add = null;
var last_w=1;
var last_h=1;
var last_id='';
var text_nr=0;
var img_nr=0;
var last_angle=0;
var max_z=0;
function werror($_text) {
	alert($_text);
}
function transalate($_angle) {
	var id=$("#"+last_id);
	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");
	var $_y=$_x=0;
	var $r= $_angle* (Math.PI/180);
	var $_el_el_h=id.find("svg image").attr('height');
	var $_el_el_w=id.find("svg image").attr('width');
	var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
	var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;
	var diff_w=(width-$_w)/2;
	var diff_h=(height-$_h)/2;
	if ($_angle>0 && $_angle<=90) {
		$_y=0;
		$_x=Math.abs(Math.sin($r))*$_el_el_h;
	}
	if ($_angle>90 && $_angle<=180) {
		$_y=Math.abs(Math.cos($r))*$_el_el_h;
		$_x=$_w;
	}
	if ($_angle>180 && $_angle<=270) {
		$_y=$_h;
		$_x=Math.abs(Math.cos($r))*$_el_el_w;
	}
	if ($_angle>270 && $_angle<=360) {
		$_y=Math.abs(Math.sin($r))*$_el_el_w;
		$_x=0;
	}
	id.find(".el_el").css('width',$_w).css('height',$_h);
	id.find(".el_el").css('top',top*1+diff_h+"px").css('left',left*1+diff_w+"px");
	//id.find("svg g").attr('transform','scale(1,-1) translate(0,-'+$_w+') translate('+$_x+','+$_y+')');
	//id.find("svg g").attr('transform','scale(-1,1) translate(-'+$_h+',0) translate('+$_x+','+$_y+')');
	id.find("svg g").attr('transform','translate('+$_x+','+$_y+')');
}
function rotate($_angle) {
	var id=$("#"+last_id);
	transalate($_angle);
	id.find("svg image").attr('transform','rotate('+$_angle+')');
	//last_angle=$_angle;
	id.find(".el_r").val($_angle);
}
function drag_rotate() {
	var id=$("#"+last_id);

	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");


	var last_id_rotate=$("#c_v_zoom>.rotate-btn");
	var $_left=last_id_rotate.css('left').replace(/px/,"");
	var $_top=last_id_rotate.css('top').replace(/px/,"");

	$_left=$_left-left;
	$_top=$_top-top;

	//$_left=$_left*1-6;
	$_top=$_top*1+21;

	$_left=($_left-(width/2));
	$_top=($_top-(height/2));

	//var rads = Math.atan($_top/$_left)*2;
	var rads = Math.atan($_top/$_left);
	var $_angle=rads/(Math.PI/180);
	var $_angle_add=0;
	if($_top < 0) {
		if($_left < 0) {
			$_angle_add = 270;
		} else {
			$_angle_add = 90;
		}
	} else {
		if($_left < 0) {
			$_angle_add = 270;
		} else {
			$_angle_add = 90;
		}
	}
	$_angle=$_angle+ $_angle_add;
	$_angle=$_angle-45;
	$_angle=parseInt($_angle, 10);
	$_angle=parseInt(last_angle)+$_angle;

	if ($_angle>=360) $_angle=$_angle-360;
	if ($_angle<0) $_angle=360+$_angle;
	$('#edit_r').val($_angle);
	$( "#slider_r" ).slider('value',$_angle);
	rotate($_angle);

}

function get_max_z() {
	$("#c_v_zoom .el_el").each(function() {
		var index_current = parseInt($(this).css("zIndex"), 10);
		if(index_current > max_z) {
			max_z = index_current;
		}
	});
}
function drag_me_rev() {
	var last_id_rotate=$("#c_v_zoom>.rotate-btn");
	last_id_rotate.appendTo("#"+last_id+" .el_el")
	var id=$("#"+last_id);
	id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
}
function drag_me() {
	var id=$("#"+last_id+' .el_el');
	var $_top=id.css("top").replace(/px/,"");
	var $_left=id.css("left").replace(/px/,"");
	var $_width=id.css("width").replace(/px/,"");
	get_max_z();
	$('#'+last_id+' .rotate-btn').appendTo('#c_v_zoom').css('left','auto').css('z-index',(max_z+1)).css('top',($_top*1-21)+'px').css('left',($_left*1+$_width*1+1)+'px').draggable({drag:function(){
		drag_rotate();
	},
	revert:function(){
		drag_me_rev();
	},
	start:function(){
		var id=$("#"+last_id);
		last_angle=id.find(".el_r").val();
		last_h=id.find(".el_el").css('height').replace(/px/,"");
		last_w=id.find(".el_el").css('width').replace(/px/,"");
	}
	});
}
function resize_to(percent){
	var id=$("#"+last_id+" .el_el");
	//alert(1+(percent/100));
	//alert(parseInt(id.css("width").replace(/px/,""))*(1+(percent/100)));
	//last_h=id.css("height").replace(/px/,"");
	//last_w=id.css("width").replace(/px/,"");
	var new_r_w=(percent*500)/100;
	var new_r_h=(percent*500)/100
	if (new_r_w>10 && new_r_h>10) {
		id.css("width",new_r_w);
		id.css("height",new_r_h);
		resize(id);
	}
}
function resize(id){

	id.parent().find(".el_w").val(id.css("width").replace(/px/,""));
	id.parent().find(".el_h").val(id.css("height").replace(/px/,""));
	//var ang_rad= $_angle* (Math.PI/180);
	var w=id.css("width").replace(/px/,"");
	var h=id.css("height").replace(/px/,"");
	//var width=(Math.cos(ang_rad)*h+Math.sin(ang_rad)*w)/2;
	//var height=(Math.sin(ang_rad)*h+Math.cos(ang_rad)*w)/2;
	var percent_h=h/last_h;
	var percent_w=w/last_w;
	new_h=percent_h*(id.find("svg image").attr('height')*1);
	new_w=percent_w*(id.find("svg image").attr('width')*1);
	id.find("svg image").attr('height',new_h);
	id.find("svg image").attr('width',new_w);
	var $_angle=id.parent().find(".el_r").val();
	transalate($_angle);
	last_h=h;
	last_w=w;
}
function load_editable($_id){
	var id=$( $_id );
	id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
	id.find(".close-btn,.move-btn,.rotate-btn").show();
	id.resizable({
		handles: "n, e, s, w,nw",
		aspectRatio: true,
		start: function(event, ui) {
			var id=$(this);
			last_w=id.css("width").replace(/px/,"");
			last_h=id.css("height").replace(/px/,"");
		},
		resize: function(event, ui) {
			var id=$(this);
			resize(id);
		}
	}).draggable({
		cancel:".close-btn,.rotate-btn",
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".el_el").css('z-index'));
			id.css('overflow','');
			id.append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep"  onmousemove="drag_me()"  title="roteste"></a><a class="move-btn spritep " title="muta"></a>');
		},
		drag: function(event, ui) {
			var id=$(this);
			id.parent().find(".el_t").val(id.css("top").replace(/px/,""));
			id.parent().find(".el_l").val(id.css("left").replace(/px/,""));
		},
		stop: function(event, ui) {
			/*var id=$(this);
			alert("sadsad");
			id.find(".el_t").val(id.css("top").replace(/px/,""));
			id.find(".el_l").val(id.css("left").replace(/px/,""));
			*/
		}
	});
}
function load_editor(){
	$( ".img" ).click(function(){
		select_tab(4);
		var id=$(this).parent();

		$("#layer_edit_img").html('<img src="http://'+window.location.host+'/rotate?i='+id.find(".el_n").val()+'"  style="width:160px;" />');
		$("#layer_edit_txt").html('');

		last_id=id.attr('id');
		if (id.data("b")!=1){
			$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
			$(".close-btn,.move-btn,.rotate-btn,#c_v_zoom .ui-resizable-n,#c_v_zoom .ui-resizable-e,#c_v_zoom .ui-resizable-s,#c_v_zoom .ui-resizable-w").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");

			var el_el_o=id.find('.el_o').val(); $("#transparency_in").val(el_el_o); $( "#slider_ts" ).slider('value',el_el_o);
			var el_el_r=id.find('.el_r').val(); $("#edit_r").val(el_el_r); $( "#slider_r" ).slider('value',el_el_r);
			load_editable(this);
		} else {
			$(".close-btn,.move-btn,.rotate-btn").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");
			$(this).append('<div class="ui-resizable-handle ui-resizable-n" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-e" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-s" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-w" style="cursor:auto;">');
		}
	});
	$( ".el_txt" ).mouseup(function(){

	});
	$( ".txt" ).click(function(){
		select_tab(3);
		var id=$(this).parent();
		$("#layer_edit_img").html('');
		$("#layer_edit_txt").val(id.find(".el_n").val());
		$("#font_size").val(id.parent().find(".el_h").val());
		/*var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;*/
		if (id.data("b")!=1){
			last_id=id.attr('id');
			$(".close-btn,.move-btn,.rotate-btn,#c_v_zoom .ui-resizable-n,#c_v_zoom .ui-resizable-e,#c_v_zoom .ui-resizable-s,#c_v_zoom .ui-resizable-w").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");

			//	var el_el_o=id.find('.el_o').val(); $("#transparency_in").val(el_el_o); $( "#slider_ts" ).slider('value',el_el_o);
			var el_el_r=id.find('.el_r').val(); $("#edit_r").val(el_el_r); $( "#slider_r" ).slider('value',el_el_r);
			load_editable(this);
		}
	});
}
function go_front($id) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var id=$("#"+$id+">div");
		var new_z=0;
		if (id.css('z-index')=='auto') new_z=0; else new_z=id.css('z-index')*1+1;
		id.css('z-index',new_z);
		id.parent().css('z-index',new_z);
		id.parent().find(".el_z").val(new_z);
	}
}
function go_back($id) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var id=$("#"+$id+">div");
		var new_z=0;
		if (id.css('z-index')=='auto') new_z=0; else {
			new_z=id.css('z-index')*1-1;
			if(new_z<0) new_z=0;
		}
		id.css('z-index',new_z);
		id.parent().css('z-index',new_z);
		id.parent().find(".el_z").val(new_z);
	}
}
function el_rotate($_angle){
	$_angle=$('#edit_r').val()*1+$_angle;
	if ($_angle>=360) $_angle=$_angle-360;
	if ($_angle<0) $_angle=360+$_angle;
	$( "#edit_r" ).val($_angle);
	$( "#slider_r" ).slider('value',$_angle);
	rotate($_angle);
}
function rotate_to($_angle) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$_angle=$_angle*1;
		if ($_angle>=360) $_angle=$_angle-360;
		if ($_angle>=360*2) $_angle=360;
		if ($_angle<0) $_angle=360+$_angle;
		$( "#edit_r" ).val($_angle);
		rotate($_angle);
		$("#"+last_id+" .el_r").val($_angle);
	}
}
function select_template($_id){
	view_2d();
	$("#view_2d").load('http://'+window.location.host+'/3d?action_2=select_template&id='+$_id, function() {
		load_editor();
	});
}
document.load_edit_or= function() {
	load_editor();
}
function reload_text(){

	var text=$("#layer_edit_txt").val();
	var hex=$("#"+last_id+" .el_c").val();
	var hex_o=$("#"+last_id+" .el_oc").val();
	var os=$("#"+last_id+" .el_os").val();
	var f=$("#"+last_id+" .el_f").val();

	$("#"+last_id+" image").attr("href",'http://'+window.location.host+'/3d?action=text&t='+text+'&size=100&c='+hex+'&oc='+hex_o+'&os='+os+'&f='+f);

	var id=$("#"+last_id+">div");
	var w=text.length*(id.css('height').replace(/px/,""))*0.4;
	id.css("width",w);
	id.parent().find(".el_w").val(w);
	$("#"+last_id+" image").attr("width",w);
}
function edit_text($_val){
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_n").val($_val);
		reload_text();
	}
}
function new_text(){
	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;
	var $_el_c='000000';
	text_nr++;
	var dv=document.createElement('div');
	var txt='<div class="div_txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'<input type="hidden" class="el_c" name="txt_c[]" value="'+$_el_c+'"/>';
	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;
	parent.document.load_edit_or();
}
function el_align($_align) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		if ($_align=='right' || $_align=='center' ) var $_left=$("#c_v").width()-($("#"+last_id+">div").width());
		if ($_align=='bottom' || $_align=='middle' ) var $_top=$("#c_v").height()-($("#"+last_id+">div").height());
		if ($_align=='left') {
			$("#"+last_id+">div").css('left',0);
			$("#"+last_id+" .el_l").val(0);
		}
		if ($_align=='right') {
			$("#"+last_id+">div").css('left',$_left);
			$("#"+last_id+" .el_l").val($_left);
		}
		if ($_align=='center') {
			$("#"+last_id+">div").css('left',$_left/2);
			$("#"+last_id+" .el_l").val($_left/2);
		}
		if ($_align=='top') {
			$("#"+last_id+">div").css('top',0);
			$("#"+last_id+" .el_t").val(0);
		}
		if ($_align=='bottom') {
			$("#"+last_id+">div").css('top',$_top);
			$("#"+last_id+" .el_t").val($_top);
		}
		if ($_align=='middle') {
			$("#"+last_id+">div").css('top',$_top/2);
			$("#"+last_id+" .el_t").val($_top/2);
		}
	}
}
function font_size($_size) {
	$("#"+last_id+" .txt_n").css('font-size',$_size+"px");
	$("#"+last_id+" .el_h").val($_size);
}
function letter_s($_space) {
	$("#"+last_id+" .txt_n").css('letter-spacing',$_space+"px");
	$("#"+last_id+" .el_w").val($_space);
}
function view_3d(view){
	$('#viz').html('<div class="loading">Se incarca ...</div>');
	$('#viz').load('http://'+window.location.host+'/3d?id_view='+view+'&'+$('#my_form').serialize());
	$('#view_2d').hide();
	$('#viz').show();
}
function view_3d_b(id){
	$(id).parent().parent().find('.selected').removeClass('selected');
	$(id).parent().addClass('selected');
	$('.b_edit').hide();
	$('.b_view').show();
}
function view_2d_b(id){
	$(id).parent().parent().find('.selected').removeClass('selected');
	$(id).parent().addClass('selected');
	$('.b_view').hide();
	$('.b_edit').show();
}
function view_2d(nr){
	$('#viz').hide();
	$('#view_2d').show();
}
function  el_delete($id) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		if ($id=='') {
			alert('Niciun element selectat');
		} else {
			if (confirm('Sunteti sigur ca dorioti sa stergeti elementul?')) $('#'+$id).remove(); else return false;
			last_id='';
		}
	}
}
function  el_clear() {
	if (confirm('Sunteti sigur ca dorioti sa stergeti toate elementele?'))	$("#c_v").html('<div id="margin_top"></div><div id="margin_left"></div><div id="margin_right"></div><div id="margin_bottom"></div>');
	else return false;
}
var SVG=function(h,w){
	var NS="http://www.w3.org/2000/svg";
	var svg=document.createElementNS(NS,"svg");
	svg.width=w;
	svg.height=h;
	return svg;
}
function new_img_text(){
	text_nr++;
	get_max_z();
	var $_value=$('#layer_edit_txt').val();
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=max_z+1;
	var $_el_r=0;
	var $_el_o=0;
	var $_el_c='000000';
	var $_el_os=0;
	var $_el_oc='000000';
	var $_el_h=40;
	var $_el_f='Arial';
	var $_el_w=$_value.length*($_el_h)*0.4;
	var dv=document.createElement('div');
	dv.id='el_txt_rand_'+text_nr;




	var txt='<div class="txt el_el" id="div_'+dv.id+'" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;height:'+$_el_h+'px; width:'+$_el_w+'px;">'

	+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>'
	+'<input type="hidden" class="el_o" name="txt_o[]" value="'+$_el_o+'"/>'
	+'<input type="hidden" class="el_c" name="txt_c[]" value="'+$_el_c+'"/>'
	+'<input type="hidden" class="el_f" name="txt_f[]" value="'+$_el_f+'"/>'
	+'<input type="hidden" class="el_os" name="txt_os[]" value="'+$_el_os+'"/>'
	+'<input type="hidden" class="el_oc" name="txt_oc[]" value="'+$_el_oc+'"/>'

	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);

	var svgNS="http://www.w3.org/2000/svg";
	var xlinkNS="http://www.w3.org/1999/xlink";
	var svg=document.createElementNS(svgNS,"svg");

	svg.setAttribute("width","100%");
	svg.setAttribute("height","100%");

	var svg_g=document.createElementNS(svgNS,"g");
	svg_g.transform="translate(0,0)";
	svg.appendChild(svg_g);
	var svg_img=document.createElementNS(svgNS,"image");

	svg_img.setAttribute("width",$_el_w);
	svg_img.setAttribute("height",$_el_h);

	svg_img.setAttributeNS(xlinkNS,"href",'http://'+window.location.host+'/3d?action=text&t='+$_value+'&c='+$_el_c+'&oc='+$_el_oc+'&os='+$_el_os+'&f='+$_el_f);
	svg_g.appendChild(svg_img);

	setTimeout(function(){
		var div=document.getElementById('div_'+dv.id);
		div.appendChild(svg);
	},300);


	/*

	+'<svg xmlns="http://www.w3.org/2000/svg" version="1.1"  width="100%" height="100%">'
	+'<g  transform="translate(0,0)"  >'
	+'<image preserveAspectRatio="none"   width="100%" height="100%"  xlink:href="3d?action=text&t='+$_value+'&size='+$_el_h+'" transform="rotate(0)" />'
	+'</g>'
	+'</svg>'*/


	dv.className='el_txt';

	dv.innerHTML=txt;
	load_editor();
}
function  blocheaza($id) {
	var id=$("#"+$id);
	if(id.data("b")==1) {
		id.data("b",0);
		load_editable(id.find(".el_el"));
	} else {
		id.data("b",1);
		$(".close-btn,.move-btn,.rotate-btn").remove();
		$("#c_v .el_el").draggable("destroy").resizable("destroy");
		id.find(".el_el").append('<div class="ui-resizable-handle ui-resizable-n" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-e" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-s" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-w" style="cursor:auto;">');
	}
}
function zoom_out() {
	if(l_zoom>1) {
		var zoom=(1-(1/l_zoom));
		do_zoom(zoom);
		l_zoom--;
		//alert(l_zoom);
		if(l_zoom==1) {
			$("#c_v_zoom").css('left','0');
			$("#c_v_zoom").css('top','0');
			$("#c_v_zoom").draggable("destroy");
		}
	}
	return false;
}
function zoom_in() {
	if(l_zoom<5) {
		var zoom=(1+1/l_zoom)
		do_zoom(zoom);
		l_zoom++;
	}
	return false;
}
function zoom_1x() {
	var zoom=1/l_zoom;
	do_zoom(zoom);
	l_zoom=1;
	$("#c_v_zoom").css('left','0');
	$("#c_v_zoom").css('top','0');
	$("#c_v_zoom").draggable("destroy");
}
function do_zoom(zoom) {
	$("#c_v_zoom").draggable({
		cancel:".close-btn,.rotate-btn",
		create: function(event, ui) {

		},
		drag: function(event, ui) {

		}
	});
	$(".close-btn,.move-btn,.rotate-btn").remove();
	$("#c_v .el_el").resizable("destroy").draggable("destroy");

	var id=$("#c_v_zoom");
	id.css("width",id.css("width").replace(/px/,"")*zoom);
	id.css("height",id.css("height").replace(/px/,"")*zoom);
	$('#c_v .el_el').each(function(i) {
		var id=$(".el_el:eq("+i+")");
		id.css("top",id.css("top").replace(/px/,"")*zoom);
		id.css("left",id.css("left").replace(/px/,"")*zoom);
		id.css("width",id.css("width").replace(/px/,"")*zoom);
		id.css("height",id.css("height").replace(/px/,"")*zoom);
		var svg=id.find("svg image");
		var g=id.find("svg g");
		var translate=g.attr('transform').replace("translate(","").replace(")","").split(",");
		g.attr("transform","translate("+(translate[0]*1)*zoom+","+(translate[1]*1)*zoom+")");
		svg.attr('height',svg.attr('height')*zoom);
		svg.attr('width',svg.attr('width')*zoom);
	});
}
function select_tab(nr) {
	$(".tab").hide();
	$("#tab-"+nr).show();
	$("#tab-nav a").removeClass("selected");
	$(".tab-"+nr+" a").addClass("selected");
}
function  transparency_to($_tr) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$('#transparency_in').val($_tr);
		transparency($_tr);
	}
}
function  transparency($_tr) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$("#"+last_id+" .el_o").val($_tr);
		$_tr=(100-$_tr*1)/100;
		$("#"+last_id+" g").attr("opacity",$_tr);
	}
}
function  add_image($_img,$w,$h) {
	img_nr++;
	get_max_z();
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=max_z+1;
	var $_el_o=0;
	var $_el_r=0;
	var $_el_w=$w;
	var $_el_h=$h;
	var dv=document.createElement('div');
	dv.id='el_img_rand_'+img_nr;
	var txt='<div class="img el_el" id="div_'+dv.id+'" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;height:'+$_el_h+'px; width:'+$_el_w+'px;">'
	+'</div>'
	+'<input type="hidden" class="el_t" name="img_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="img_n[]" value="'+$_img+'"/>'
	+'<input type="hidden" class="el_l" name="img_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="img_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="img_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="img_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="img_r[]" value="'+$_el_r+'"/>'
	+'<input type="hidden" class="el_o" name="img_o[]" value="'+$_el_o+'"/>';
	+'</div>';

	$_img='rotate?i='+$_img;

	document.getElementById('c_v_zoom').appendChild(dv);
	var svgNS="http://www.w3.org/2000/svg";
	var xlinkNS="http://www.w3.org/1999/xlink";
	var svg=document.createElementNS(svgNS,"svg");

	svg.setAttribute("width","100%");
	svg.setAttribute("height","100%");

	var svg_g=document.createElementNS(svgNS,"g");
	svg_g.transform="translate(0,0)";
	svg.appendChild(svg_g);
	var svg_img=document.createElementNS(svgNS,"image");

	svg_img.setAttribute("width",$_el_w);
	svg_img.setAttribute("height",$_el_h);

	svg_img.setAttributeNS(xlinkNS,"href",'http://'+window.location.host+'/'+$_img);
	svg_g.appendChild(svg_img);

	setTimeout(function(){
		var div=document.getElementById('div_'+dv.id);
		div.appendChild(svg);
	},300);
	dv.className='el_img';
	dv.innerHTML=txt;
	load_editor();
}
function  clone($id) {

	$id=last_id;

	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var top=($('#'+$id+">div").css("top").replace(/px/,""))*1+10;
		var left=($('#'+$id+">div").css("left").replace(/px/,""))*1+10;
		var clone =$('#'+$id).clone();
		clone.attr('id',clone.attr('id')+"_c");
		clone.append(clone.find('.el_el'));
		clone.find('.el_el').removeClass('ui-resizable');
		clone.find('.ui-draggable').remove();
		clone.find(".el_el").css('top',top+'px').css('left',left+'px').css('position','absolute');
		$("#c_v_zoom").append(clone);
		$("#c_v .el_el").resizable("destroy").draggable("destroy");
		//$("#"+last_id+" .close-btn,#"+last_id+" .move-btn,#"+last_id).hide();
		$(".close-btn,.move-btn,.rotate-btn").remove();
		load_editor();
		return false;
		$( ".el_img" ).mouseup(function(){
			$(".edit_text").hide();
			$(".edit_img").show();
			var id=$(this);
			last_id=this.id;
			if (id.data("b")!=1){
				$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
				$("#c_v .el_el").resizable("destroy").draggable("destroy");
				load_editable(last_id);
				$("#edit_r").val(id.find('.el_r').val());
			}
		});
	}
}

function max_z_act() {
	get_max_z(); $('#form_max_z').val(max_z);
	return true;
}
function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function text_color(hex) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_c").val(hex);
		reload_text();
	}
}

function select_font(font) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_f").val(font);
		reload_text();
	}
}
function outline_size(size) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_os").val(size);
		reload_text();
	} else {

	}
}
function outline_color(hex) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_oc").val(hex);
		reload_text();
	} else {

	}
}

function echo(txt,i) {
	$('#meu_'+i).val(txt);
}