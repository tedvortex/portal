var last_id='';
var last_z=0;
var text_nr=0;
var l_zoom=1;
var s_zoom=1;
function load_editable_txt($_id){
	$( "#"+$_id+" .el_el" ).draggable({
		//containment: "#c_v" ,
		cancel:".close-btn,.rotate-btn,.resize-btn",
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".el_el").css('z-index'));
			id.css('overflow','');
			id.append('<a class="close-btn spritep "  onmousedown="el_delete(last_id);" title="inchide"></a>');
			id.append('<a class="rotate-btn spritep" onmousedown="el_rotate(45);"; title="roteste"></a>');
			id.append('<a class="move-btn spritep " title="muta"></a>');
			id.append('<a class="resize-btn spritep " title="mareste"></a>');
			//id.append('<a class="resize-btn spritep ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" title="mareste"></a>');


		},
		drag: function(event, ui) {
			var id=$(this);

			id.parent().find(".el_t").val((id.css("top").replace(/px/,""))/l_zoom);
			id.parent().find(".el_l").val((id.css("left").replace(/px/,""))/l_zoom);

		}
	});
}
function load_editable($_id){
	$( "#"+$_id+" .el_el" ).resizable({
		//containment: "#c_v",
		//animate: true,
		handles: "all",
		resize: function(event, ui) {
			//alert('sadsadsad');
			var id=$(this);
			id.parent().find(".el_w").val((id.css("width").replace(/px/,""))/l_zoom);
			id.parent().find(".el_h").val((id.css("height").replace(/px/,""))/l_zoom);

			//id.data("h",id.css("height").replace(/px/,""));
			//id.data("w",id.css("width").replace(/px/,""));
		}
	}).parent()
	.draggable({
		//containment: "#c_v" ,
		cancel:".close-btn,.rotate-btn,.resize-btn",
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".el_el").css('z-index'));
			id.css('overflow','');
			id.append('<a class="close-btn spritep "  onmousedown="el_delete(last_id);" title="inchide"></a>');
			id.append('<a class="rotate-btn spritep" onmousedown="el_rotate(45);"; title="roteste"></a>');
			id.append('<a class="move-btn spritep " title="muta"></a>');
			id.append('<a class="resize-btn spritep " title="mareste"></a>');
			//id.append('<a class="resize-btn spritep ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" title="mareste"></a>');


		},
		drag: function(event, ui) {
			var id=$(this);

			id.parent().find(".el_t").val((id.css("top").replace(/px/,""))/l_zoom);
			id.parent().find(".el_l").val((id.css("left").replace(/px/,""))/l_zoom);

		}
	});
}
function load_editor(){

	$( ".el_img" ).mouseup(function(){
		var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;
		if (id.data("b")!=1){
			$("#edit_r").val(id.find('.el_r').val());
			$("#c_v .img").resizable("destroy").draggable("destroy");
			load_editable(last_id);
		}
	});

	$( ".el_txt" ).mouseup(function(){
		var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;
		if (id.data("b")!=1){
			$("#edit_r").val(id.find('.el_r').val());
			//$("#c_v .img").resizable("destroy").draggable("destroy");
			$("#c_v .txt").resizable("destroy").draggable("destroy");
			//$("#c_v .txt").draggable("destroy");
			load_editable(last_id);
		}
	});

	$( ".img" ).mouseup(function(){
		select_tab(4);
		$("#layer_edit_img").html('<img src="'+this.src+'"  style="width:160px;" />');
		$("#layer_edit_txt").html('');
	});
	$( ".txt" ).mouseup(function(){
		select_tab(3);
		var id=$(this);
		last_id=this.id;
		$("#layer_edit_img").html('');
		$("#layer_edit_txt").html('<textarea style="width:100%"  onkeyup="edit_text(this.value);">'+id.parent().find(".el_n").val()+' </textarea>');
		$("#font_size").val(id.parent().find(".el_h").val());

		//$("#edit_ls").val(id.find('.txt_n').css('letter-spacing').replace(/px/,""));
		//$("#edit_fs").val(id.find('.txt_n').css('font-size').replace(/px/,""));
	});

	$( ".div_txt" ).draggable({
		//containment: "#c_v" ,
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".txt").css('z-index'));
			id.css('overflow','');
		},
		stop: function(event, ui) {
			var id=$(this);
			id.find(".el_t").val(id.css("top").replace(/px/,""));
			id.find(".el_l").val(id.css("left").replace(/px/,""));
		}
	});

	/*$('.img').parent().parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});

	$('.txt').parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});*/
}
function go_front($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else new_z=id.css('z-index')*1+1;
	id.css('z-index',new_z);
	id.find(".el_el").css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}
function go_back($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else {
		new_z=id.css('z-index')*1-1;
		if(new_z<0) new_z=0;
	}
	id.css('z-index',new_z);
	id.find(".el_el").css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}

function el_rotate(degrees){
	//alert(degrees);
	degrees=($('#edit_r').val()*1+degrees);
	if (degrees<-180) degrees=(degrees*-1)-180;
	if (degrees>180) degrees=180-degrees;
	//$( "#slider_r" ).slider('value',degrees);
	$('#edit_r').val(degrees);
	//$( "#slider_r" ).slider('value',degrees);
	rotate_to(degrees);
}
function rotate_to($_angle) {

	$_angle=$_angle*1;
	if ($_angle<-180){
		$_angle=($_angle*-1)-180;
		$( "#edit_r" ).val($_angle);
	}
	if ($_angle>180) {
		$_angle=180-$_angle;
		$( "#edit_r" ).val($_angle);
	}
	$("#"+last_id+" .el_r").val($_angle);

	$("#c_v .el_el").resizable("destroy").draggable("destroy");
	$("#"+last_id+" .img").height('auto');
	load_editable(last_id);



	/*
	w=$("#"+last_id+">div").data("w");
	h=$("#"+last_id+">div").data("h");


	if ($_angle>90) {
	var ang_rad=(Math.PI/180)*($_angle-90);
	var width=(Math.cos(ang_rad)*h)+(Math.sin(ang_rad)*w);
	var height=Math.sin(ang_rad)*h+Math.cos(ang_rad)*w;
	} else {
	var ang_rad=(Math.PI/180)*$_angle;
	var width=(Math.cos(ang_rad)*w)+(Math.sin(ang_rad)*h);
	var height=Math.sin(ang_rad)*w+Math.cos(ang_rad)*h;
	}

	$("#"+last_id+">div").width(width);
	$("#"+last_id+">div").height(height);
	*/

	var $_url='rotate?i='+$("#"+last_id+" .el_n").val()+'&r='+$_angle;
	$("#layer_edit").html('<img src="'+$_url+'"  style="width:160px;" />');
	$("#"+last_id+" .img").attr('src',$_url);

	//rotate($("#"+last_id+" .el_el"), $_angle);
}

function rotate(object, degrees) {
	object.css({
	'-webkit-transform' : 'rotate('+degrees+'deg)',
	'-moz-transform' : 'rotate('+degrees+'deg)',
	'-ms-transform' : 'rotate('+degrees+'deg)',
	'-o-transform' : 'rotate('+degrees+'deg)',
	'transform' : 'rotate('+degrees+'deg)',
	'filter': "progid:DXImageTransform.Microsoft.Matrix(M11=0.9914448613738104, M12=-0.13052619222005157,M21=0.13052619222005157, M22=0.9914448613738104, sizingMethod='auto expand');",
	'zoom' : 1
	});
}
function select_template($_id){
	view_2d();
	$("#c_v").load("3d?action_2=select_template&id="+$_id, function() {
		load_editor();
	});
}
document.load_edit_or= function() {
	load_editor();
}
function reload_text(){
	var text=$("#layer_edit_txt textarea").val();
	var font_size=$("#font_size").val();
	$("#"+last_id+" .txt").attr('src','3d?action=text&t='+text+'&size='+font_size);
	$("#"+last_id+" .el_h").val(font_size);
}
function edit_text($_val){
	$("#"+last_id+" .el_n").val($_val);
	reload_text();
}
function new_img_text(){
	/*var txt='<img class="img" style="z-index:<?=$_el['el_z']?>; left:<?=$_el['el_l']?>px; top:<?=$_el['el_t']?>px;height:<?=$_el['el_h']?>px; width:<?=$_el['el_w']?>px;" src="images/<?=$_el['value']?>" />'
	+'<input type="hidden" class="el_n" name="img_n[]" value="<?=$_el['value']?>"/>'
	+'<input type="hidden" class="el_t" name="img_t[]" value="<?=$_el['el_t']?>"/>'
	+'<input type="hidden" class="el_l" name="img_l[]" value="<?=$_el['el_l']?>"/>'
	+'<input type="hidden" class="el_h" name="img_h[]" value="<?=$_el['el_h']?>"/>'
	+'<input type="hidden" class="el_w" name="img_w[]" value="<?=$_el['el_w']?>"/>'
	+'<input type="hidden" class="el_z" name="img_z[]" value="<?=$_el['el_z']?>"/>'
	+'<input type="hidden" class="el_r" name="img_r[]" value="<?=$_el['el_r']?>"/>';*/



	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;

	text_nr++;
	var dv=document.createElement('div');
	//var txt='<div class="el_img" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	var txt='<img class="el_el txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" src="3d?action=text&t='+$_value+'&size='+$_el_h+'" />'
	//	+'<input class="txt_n txt el_el" name="txt_n[]"  type="text" value="'+$_value+'" /> <span class="c_move">|||||||||||||||||| MOVE ||||||||||||||||||</span>'
	//+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'</div>';
	document.getElementById('c_v').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;

	load_editor();

	//parent.document.load_edit_or();


}
function new_text(){


	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;

	text_nr++;
	var dv=document.createElement('div');
	var txt='<div class="div_txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	//	+'<input class="txt_n txt el_el" name="txt_n[]"  type="text" value="'+$_value+'" /> <span class="c_move">|||||||||||||||||| MOVE ||||||||||||||||||</span>'
	+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'</div>';
	document.getElementById('c_v').appendChild(dv);
	dv.className='txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;
	parent.document.load_edit_or();
}
function el_align($_align) {
	if ($_align=='right' || $_align=='center' ) var $_left=$("#c_v").width()-($("#"+last_id+">div").width());
	if ($_align=='bottom' || $_align=='middle' ) var $_top=$("#c_v").height()-($("#"+last_id+">div").height());
	if ($_align=='left') {
		$("#"+last_id+">div").css('left',0);
		$("#"+last_id+" .el_l").val(0);
	}
	if ($_align=='right') {
		$("#"+last_id+">div").css('left',$_left);
		$("#"+last_id+" .el_l").val($_left);
	}
	if ($_align=='center') {
		$("#"+last_id+">div").css('left',$_left/2);
		$("#"+last_id+" .el_l").val($_left/2);
	}
	if ($_align=='top') {
		$("#"+last_id+">div").css('top',0);
		$("#"+last_id+" .el_t").val(0);
	}
	if ($_align=='bottom') {
		$("#"+last_id+">div").css('top',$_top);
		$("#"+last_id+" .el_t").val($_top);
	}
	if ($_align=='middle') {
		$("#"+last_id+">div").css('top',$_top/2);
		$("#"+last_id+" .el_t").val($_top/2);
	}
}
function font_size($_size) {
	$("#"+last_id+" .txt_n").css('font-size',$_size+"px");
	$("#"+last_id+" .el_h").val($_size);
}
function letter_s($_space) {
	$("#"+last_id+" .txt_n").css('letter-spacing',$_space+"px");
	$("#"+last_id+" .el_w").val($_space);
}
function view_3d(view){
	$('#viz').html('<div class="loading">Se incarca ...</div>');
	$('#viz').load('3d?id_view='+view+'&'+$('#my_form').serialize());
	$('#view_2d').hide();
	$('#viz').show();
}
function view_2d(){
	$('#viz').hide();
	$('#view_2d').show();
}
function  clone($id) {

	$id=last_id;
	//alert($id);
	//$("#c_v .el_el").resizable("destroy").draggable("destroy");



	var top=($('#'+$id+">div").css("top").replace(/px/,""))*1+10;
	var left=($('#'+$id+">div").css("left").replace(/px/,""))*1+10;


	var clone =$('#'+$id).clone();
	//alert(clone.html());
	//alert(clone.attr('id')+"_c");

	clone.attr('id',clone.attr('id')+"_c");
	clone.append(clone.find('.el_el'));
	clone.find('.el_el').removeClass('ui-resizable');
	//.find(".ui-resizable-handle,.close-btn,.move-btn,.resize-btn,.rotate-btn").remove();
	clone.find('.ui-draggable').remove();
	clone.find(".el_el").css('top',top+'px').css('left',left+'px').css('position','absolute');
	$("#c_v").append(clone);
	$("#c_v .el_el").resizable("destroy").draggable("destroy");
	load_editor();
	return false;

	$( ".el_img" ).mouseup(function(){
		$(".edit_text").hide();
		$(".edit_img").show();
		var id=$(this);
		last_id=this.id;
		//alert(last_id);
		if (id.data("b")!=1){
			$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
			//$("#c_v .el_img>div,#c_v .el_txt>div").css('background-color','transparent');
			//id.css('background-color','#999999');
			//$(this).find(".ui-resizable-handle").show();

			$("#c_v .el_el").resizable("destroy").draggable("destroy");
			load_editable(last_id);
			$("#edit_r").val(id.parent().find('.el_r').val());
		}
	});



}
function  blocheaza($id) {
	var id=$("#"+$id);
	id.data("b",1);
}
function  el_delete($id) {
	if ($id=='') {
		alert('Niciun element selectat');
	} else {
		if (confirm('Sunteti sigur ca dorioti sa stergeti elementul?')) $('#'+$id).remove(); else return false;
		last_id='';
	}
}
function  el_clear() {
	if (confirm('Sunteti sigur ca dorioti sa stergeti toate elementele?'))	$("#c_v").html('<div id="margin_top"></div><div id="margin_left"></div><div id="margin_right"></div><div id="margin_bottom"></div>');
	else return false;
}


function zoom_out() {
	if(l_zoom>1) {
		var zoom=(1-(1/l_zoom));
		do_zoom(zoom);
		l_zoom--;
	}
	return false;
}
function zoom_in() {
	if(l_zoom<5) {
		var zoom=(1+1/l_zoom)
		do_zoom(zoom);
		l_zoom++;
	}
	return false;
}
function zoom_1x() {
	var zoom=1/l_zoom;
	do_zoom(zoom);
	l_zoom=1;
	$("#c_v_zoom").css('left','0');
	$("#c_v_zoom").css('top','0');
}
function do_zoom(zoom) {
	$("#c_v_zoom").draggable({
		cancel:".close-btn,.rotate-btn,.resize-btn",
		create: function(event, ui) {

		},
		drag: function(event, ui) {

		}
	});
	$("#c_v .img").resizable("destroy").draggable("destroy");
	$("#c_v .txt").draggable("destroy");
	var id=$("#c_v_zoom");
	id.css("width",id.css("width").replace(/px/,"")*zoom);
	id.css("height",id.css("height").replace(/px/,"")*zoom);
	$('#c_v .el_el').each(function(i) {
		var id=$(".el_el:eq("+i+")");
		id.css("top",id.css("top").replace(/px/,"")*zoom);
		id.css("left",id.css("left").replace(/px/,"")*zoom);
		id.css("width",id.css("width").replace(/px/,"")*zoom);
		id.css("height",id.css("height").replace(/px/,"")*zoom);
	});
}
function select_tab(nr) {
	$(".tab").hide();
	$("#tab-"+nr).show();
	$("#tab-nav a").removeClass("selected");
	$(".tab-"+nr+" a").addClass("selected");
}

function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}