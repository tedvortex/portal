if (!Array.prototype.forEach)
{
	Array.prototype.forEach = function(fun /*, thisp */)
	{
		"use strict";

		if (this === void 0 || this === null)
		throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== "function")
		throw new TypeError();

		var thisp = arguments[1];
		for (var i = 0; i < len; i++)
		{
			if (i in t)
			fun.call(thisp, t[i], i, t);
		}
	};
}

function add_script(nr,module,callback){

	var _prev=document.getElementById('refresh-'+nr)

	if(_prev)
		_scripts.removeChild(_prev)

	nr++

	var _script=document.createElement("script")
	_script.type='text/javascript'
	_script.src=_b+'ajax.json?module='+module+'&callback='+callback+'&_='+(new Date().getTime())
	_script.id='refresh-'+nr

	_scripts.appendChild(_script)

	_script.onload =
	_script.onreadystatechange =
	function() {
		if(
			typeof(t) !== 'undefined'
		){
			clearTimeout(t)
		}

		t=setTimeout(function(){add_script(nr,module,callback)},2500)
	}
}

function reload_page(timeout){
	setTimeout("window.location.reload(true);",timeout*1000)
}

var _scripts=document.getElementById('scripts')

_b = 'http://' + window.location.host + '/';

_p = {
	cart:{
		"in_progress":false,
		"success":"",
		"timestamp":0,
		"count":0,
		"total":0,
		"products":[]
	}
}

p    = $('li#cart-container');
_pdc = $('section#product-description');
_pdl = $('div#products-cart-list');
_cs  = $('ul#cart-step');

_discount_ranges=['','primul','al doilea','al treilea']

function cart(id,amount,type,obj){
	id=id.split('#');

	id=id[0];

	$.ajax({
		url:_b+'ajax.json',
		dataType:'jsonp',
		cache:false,
		data:
		({
			module:'cart',
			id:id,
			amount:amount,
			type:type
		}),
		success:function(data){
			/*_p.cart.timestamp=data.timestamp*/

			if(data.success){
				if(
					type=='update' &&
					obj
				){
					var key = obj.attr('class').split('-')[1]

					var d=$('li.product-'+key)

					var price=$('input[name=\"price\[\]\"]',d).val()

					$('tr.price-init section.price p',d).html('<span>'+price+'</span>'+'&#160;lei')
					$('tr.price-total strong',d).html(number_format(price*amount,2,'.',''))
				}


				if(type=='remove'){
					var d = $('li.product-'+id)

					d.slideUp(
						'fast',
						function(){
							d.remove()

							_g(data.success)

							if(
								$('li',_pdl).length==0
							){
//								console.log(_pdl);
//								console.log(_cs);
								
								_pdl.html('<p class=\"no-products\">Niciun produs in cos.</p>')
								_cs.slideUp(
									'slow',
									function(){
										_cs.remove()
									}
								)
							}
						}
					)
				}

				else{
					_g(data.success)
				}

				_p.cart.in_progress=false
			}

			else if (data.error){
				Window.init(data.error, 'Actiune nereusita !', 'error');
			}
		}
	})
}


function _products(obj){
	if(
		obj.timestamp>_p.cart.timestamp
	){
		$('section#cart-products,section#cart-total',p).html('')

		$('section#product-discount',_pdc).html('')

		$('div#order-total')
		.find('td#total-step-1 sup,td#shopcart-total-order sup,td#shopcart-total-transport sup,td#shopcart-total-tax sup')
		.html('')

		$('.price-total')
		.find('strong')
		.html('')

		$('span#total-price-shopcart,span#total-weight-shopcart')
		.html('')

		var l=0

		if(
			typeof(obj.products)=='object'
		){
			for(var i in obj.products){
				if(
					typeof( obj.products[i] ) !== 'function'
				){
					l++
				}
			}
		}

		if(
			_pdl.length==1 &&
			l!=$('li',_pdl).length
		){
			Window.init('Comanda dumneavoastra a fost modificata dintrun tab extern, pagina va fi reincarcata pentru a reflecta modificarile in:<br/><br/>5 secunde.', 'Actiune nereusita !', 'error');

			reload_page(5)
		}

		if(l!==0){
			$('section#cart-products',p).append('<ul/>')

			$('section#cart-total',p).append('<p id="total-price">Total = <span>'+obj.total+' '+obj.currency+'</span>'+(parseFloat(obj.discount)>0?' <span class="discount">(Discount '+obj.discount+'%)</span>':'')+'</p>'+(obj.level>0?'<p class="discount-range">'+(obj.level>0?'Pentru '+_discount_ranges[obj.level]+' prag de discount mai ai nevoie de produse in valoare de <span class="white">'+obj.next+' '+obj.currency:'Ai atins ultimul prag de discount!')+'</span></p><p><a class="discount-range-checker">Verifica grila discount</a></p>':''))

			$('section#product-discount',_pdc).append((obj.level>0?'<p class="strong">Pentru <span> '+_discount_ranges[obj.level]+'</span> prag de discount</p><p class="light">mai ai nevoie de produse in valoare de</p><div><p class="needed"><span>'+obj.next+' </span> '+obj.currency+'</p></div>':'<p class="final-level">Ai atins ultimul nivel de discount <span>(2%)</span></p>'))

			if(parseFloat(obj.total)>0){
				$('section#cart-total',p).append('<a class="button" href="http://'+window.location.host+'/comanda">vezi cos</a>')

				$('div#order-total td#total-step-1 sup').html(obj.total)
				$('div#order-total td#shopcart-total-order sup').html(obj.total)

				$('div#order-total td#shopcart-total-transport sup').html(obj.shipping)
				$('div#order-total td#shopcart-total-tax sup').html(obj.total_ex)

				$('.price-total strong').html(obj.total)
				$('span#total-price-shopcart').html(obj.total)
				$('span#total-weight-shopcart').html(obj.w)

				if(parseFloat(obj.d)>0){
					$('div#total-pink .price-discount').find('sup').html(obj.d).end().removeClass('n')
				}

				else{
					$('div#total-pink .price-discount').addClass('n');
				}
			}

			for(var i in obj.products){
				v=obj.products[i]
				v.e=htmlentities(v.n,'ENT_COMPAT','UTF-8')
//				console.log(i);
				$('section#cart-products ul',p).append('<li id="product-'+i+'" class="'+(i%2==0?'odd':'even')+'"><div class="image"><a class="external" style="background-image:url(\''+v.image+'\')" href="'+v.l+'" title="'+v.e+'"></a></div><h2><a class="external" href="'+v.l+'" title="'+v.e+'"><span>'+v.q+' x</span> '+v.n+'</a></h2><div class="price"><p class="final">'+v.f+'</p><a class="delete sprite" href="#'+i+'"></a></div>'+(parseFloat(v.d)>0?'<div class="savings"><p><span class="green">Economisesti:</span>&#160;'+v.d+' '+obj.currency+'</p></div>':'')+'</li>')
				if(
					_pdl.length==1 &&
					v.q!=$('input.product-'+v.k,_pdl).val()
				){
					Window.init('Comanda dumneavoastra a fost modificata dintrun tab extern, pagina va fi reincarcata pentru a reflecta modificarile in:<br/><br/>5 secunde.', 'Actiune nereusita !', 'error');
					reload_page(5)
				}
			}
		}

		else
			$('section#cart-products',p).append('<p class="no-products">Niciun produs in cos.</p>')


		$('strong#cart-count').html(obj.count+'&#160;produs'+(obj.count!=1?'e':'')+' - '+obj.total+' lei')


		_p.cart.timestamp=obj.timestamp
		_p.cart.products=obj.products
		_p.cart.count=obj.count
		_p.cart.total=obj.total
	}
}