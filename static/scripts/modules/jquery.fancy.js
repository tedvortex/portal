/*!
 * Fancy Gallery v1.4.1
 *
 * Copyright 2011, Rafael Dery
 *
 * Only for sale at the envato marketplaces
 */ (function (d) {
    function v() {
        var f = document.URL,
            g = f.indexOf("://") + 3,
            h = f.indexOf("/", g);
        f = f.substring(g, h);
        g = f.lastIndexOf(".") - 1;
        g = f.lastIndexOf(".", g) + 1;
        f = f.substring(g, f.length);
        for (h = g = 0; h < f.length; ++h) {
            var r = f.charAt(h);
            g += r.charCodeAt(0)
        }
        return g != 991 && f != "" ? true : false
    }
    jQuery.fn.fancygallery = function (f) {
        function g(m) {
            if (v()) return false;
            b = d(m);
            b.addClass("clearfix");
            w = b.children("div").hide();
            if (!a.slideTitle && !a.showTitle) a.titleHeight = 0;
            b.append("<select class='fg-albumSelecter'></select>");
            d.each(b.children("div"), function (s, e) {
                var x = e.title == a.selectAlbum ? "selected='selected'" : "";
                b.find(".fg-albumSelecter").append("<option value='" + e.title + "' " + x + ">" + e.title + "</option>")
            });
            b.find(".fg-albumSelecter").change(function () {
                d.isFunction(d.prop) ? h(d(this).prop("selectedIndex")) : h(d(this).attr("selectedIndex"))
            });
            if (v()) return false;
            if (!a.dropdown || w.length <= 1) b.find(".fg-albumSelecter").hide();
            else {
                b.find(".fg-albumSelecter").uniform();
                b.find(".selector").css({
                    "float": "right",
                    "margin-bottom": 5
                })
            }
            a.divider && b.append("<hr class='fg-line' />");
            b.append("<ul class='fg-thumbHolder clearfix'></ul>");
            if (a.imagesPerPage != 0) {
                a.arrowPosition == "top" ? b.children("div:hidden").last().after('<a class="fg-prev" href="" title="Previous image stack"></a>') : b.append('<a class="fg-prev" href="" title="Previous image stack"></a>');
                b.children(".fg-prev").click(function () {
                    b.find(".fg-next").css({
                        visibility: "visible"
                    });
                    r("prev");
                    return false
                }).hover(function () {
                    d(this).toggleClass("fg-prev-over")
                }, function () {
                    d(this).toggleClass("fg-prev-over")
                });
                a.arrowPosition == "top" ? b.children("a:first").after('<a class="fg-next" href="#" title="Next image stack"></a>') : b.append('<a class="fg-next" href="#" title="Next image stack"></a>');
                b.children(".fg-next").click(function () {
                    b.find(".fg-prev").css({
                        visibility: "visible"
                    });
                    r("next");
                    return false
                }).hover(function () {
                    d(this).toggleClass("fg-next-over")
                }, function () {
                    d(this).toggleClass("fg-next-over")
                })
            }
            if (v()) return false;
            w.length > 0 && b.find(".fg-albumSelecter").change()
        }
        function h(m) {
            y = m;
            l = 0;
            n = d(w[y]).children("a");
            if (n.length == 0) {
                b.find(".fg-thumbHolder").empty().append("<p>This album has no media files!</p>");
                return false
            }
            r("next")
        }
        function r(m) {
            if (m == "prev") l -= a.imagesPerPage + z;
            b.find(".fg-thumbHolder").empty();
            m = a.imagesPerPage == 0 ? n.length : a.imagesPerPage;
            for (var s = 0; s < m; ++s) {
                var e = n.get(l),
                    x = d(e).children("img:first").attr("src"),
                    t = d(e).children("img:first").attr("title") ? d(e).children("img:first").attr("title") : "",
                    C = d(e).has("span").length ? d(e).children("span:first").html() : "",
                    A = "";
                if (a.hoverImage) A = '<img src="' + a.hoverImage + '" class="fg-hover-image" />';
                b.children(".fg-thumbHolder").append('<li class="fg-listItem" style="width:' + (a.thumbWidth + a.borderThickness * 2) + "px; height:" + (a.thumbHeight + a.titleHeight + a.borderThickness * 2) + "px; margin: " + a.rowOffset + "px " + a.columnOffset + 'px; "><a href="' + e.href + '" title="' + C + '" rel="prettyPhoto[' + b.attr("id") + ']" class="fg-image"><img class="fg-thumb" src="' + x + '" alt="' + t + '" />' + A + '</a><span class="fg-title"></span><img src="' + a.shadowImage + '" class="fg-shadow" width=' + (a.thumbWidth + a.borderThickness * 2) + " /></li>");
                e = b.find(".fg-listItem:last");
                s == 0 && a.hoverImage && e.find(".fg-hover-image").load(function () {
                    j = d(this).width();
                    k = d(this).height()
                });
                a.shadowImage == "" && e.children(".fg-shadow").css("display", "none");
                if (a.showTitle && t.length) {
                    e.children(".fg-title").text(t).css("height", a.titleHeight);
                    e.children(".fg-shadow").css("top", a.thumbHeight + a.titleHeight + a.shadowOffset + a.borderThickness * 2)
                } else e.children(".fg-shadow").css("top", a.thumbHeight + a.shadowOffset + a.borderThickness * 2);
                e.hide().fadeIn(500 + s * 200).hover(function () {
                    var c = d(this),
                        i = c.find(".fg-thumb").stop().fadeTo(400, a.inverseHoverEffect ? a.thumbOpacity : 1).attr("alt");
                    if (a.hoverImage && j) {
                        var o, p;
                        switch (a.hoverImageEffect) {
                        case "fade":
                            p = {
                                display: "block",
                                opacity: 0,
                                left: a.thumbWidth * 0.5 - j * 0.5,
                                top: a.thumbHeight * 0.5 - k * 0.5
                            };
                            o = {
                                opacity: 1
                            };
                            break;
                        case "l2r":
                            p = {
                                display: "block",
                                left: -j,
                                top: a.thumbHeight * 0.5 - k * 0.5
                            };
                            o = {
                                left: a.thumbWidth * 0.5 - j * 0.5
                            };
                            break;
                        case "r2l":
                            p = {
                                display: "block",
                                left: a.thumbWidth + j,
                                top: a.thumbHeight * 0.5 - k * 0.5
                            };
                            o = {
                                left: a.thumbWidth * 0.5 - j * 0.5
                            };
                            break;
                        case "t2b":
                            p = {
                                display: "block",
                                left: a.thumbWidth * 0.5 - j * 0.5,
                                top: -k
                            };
                            o = {
                                top: a.thumbHeight * 0.5 - k * 0.5
                            };
                            break;
                        case "b2t":
                            p = {
                                display: "block",
                                left: a.thumbWidth * 0.5 - j * 0.5,
                                top: a.thumbHeight + k
                            };
                            o = {
                                top: a.thumbHeight * 0.5 - k * 0.5
                            }
                        }
                        c.find(".fg-hover-image").css(p).stop().animate(o, 400)
                    }
                    if (i == "" || !a.slideTitle || c.find(".fg-title").height() > 0) return false;
                    a.shadowImage != "" && c.find(".fg-shadow").stop().animate({
                        top: a.thumbHeight + a.titleHeight + a.shadowOffset + a.borderThickness * 2
                    }, 200);
                    var B = c.find(".fg-title").empty();
                    c.find(".fg-title").stop().animate({
                        height: a.titleHeight
                    }, 200, function () {
                        if (d.browser.msie && d.browser.version <= 8) c.find(".fg-title").text(i);
                        else {
                            for (var u = [], q = 0; q < i.length; ++q) {
                                B.append("<span>" + i.charAt(q) + "</span>");
                                u.push(B.find("span:last").hide())
                            }
                            if (a.textFadeDirection === "reverse") u.reverse();
                            else a.textFadeDirection === "random" && u.shuffle();
                            for (q in u) d(u[q]).stop().fadeIn(50 * q)
                        }
                    })
                }, function () {
                    var c = d(this);
                    if (a.hoverImage && j) {
                        var i;
                        switch (a.hoverImageEffect) {
                        case "fade":
                            i = {
                                opacity: 0
                            };
                            break;
                        case "l2r":
                            i = {
                                left: a.thumbWidth + j
                            };
                            break;
                        case "r2l":
                            i = {
                                left: -j
                            };
                            break;
                        case "t2b":
                            i = {
                                top: a.thumbHeight + k
                            };
                            break;
                        case "b2t":
                            i = {
                                top: -k
                            }
                        }
                        c.find(".fg-hover-image").stop().animate(i, 200)
                    }
                    if (c.find(".fg-thumb").stop().fadeTo(200, a.inverseHoverEffect ? 1 : a.thumbOpacity).attr("alt") == "" || !a.slideTitle) return false;
                    c.find(".fg-title").empty().stop().animate({
                        height: 0
                    }, 200);
                    a.shadowImage != "" && c.find(".fg-shadow").stop().animate({
                        top: a.thumbHeight + a.shadowOffset + a.borderThickness * 2
                    }, 200)
                });
                t = e.find(".fg-thumb");
                d(t).load(function () {
                    var c = d(this);
                    if (a.scaleMode == "prop") {
                        var i = a.thumbHeight / a.thumbWidth;
                        if (c.height() / c.width() > i) {
                            if (c.height() > a.thumbHeight) {
                                c.attr("width", Math.round(c.width() * (a.thumbHeight / c.height())));
                                c.attr("height", a.thumbHeight)
                            }
                        } else if (c.width() > a.thumbHeight) {
                            c.attr("height", Math.round(c.height() * (a.thumbWidth / c.width())));
                            c.attr("width", a.thumbWidth)
                        }
                    } else if (a.scaleMode == "stretch") {
                        c.attr("width", a.thumbWidth);
                        c.attr("height", a.thumbHeight)
                    } else {
                        c.attr("width",
                        a.thumbWidth);
                        c.wrap("<div style='height:" + a.thumbHeight + "px; overflow:hidden; display:block;'></div>")
                    }
                    c.fadeTo(0, a.inverseHoverEffect ? 1 : a.thumbOpacity)
                });
                l == n.length - 1 ? b.find(".fg-next").css({
                    visibility: "hidden"
                }) : b.find(".fg-next").css({
                    visibility: "visible"
                });
                l == 0 && b.find(".fg-prev").css({
                    visibility: "hidden"
                });
                ++l;
                if (l == n.length) break
            }
            d("a[rel^='prettyPhoto']").prettyPhoto(a.boxOptions);
            b.find(".fg-image").css({
                "background-color": a.backgroundColor,
                padding: a.borderThickness,
                width: a.thumbWidth,
                height: a.thumbHeight
            });
            b.find(".fg-title").css({
                width: a.thumbWidth,
                "background-color": a.backgroundColor,
                color: a.titleColor,
                paddingLeft: a.borderThickness,
                paddingRight: a.borderThickness,
                top: 1 + a.thumbHeight + a.borderThickness * 2
            });
            z = l % a.imagesPerPage == 0 ? a.imagesPerPage : l % a.imagesPerPage
        }
        var a = d.extend({}, d.fn.fancygallery.defaults, f),
            w, n, y = -1,
            l = 0,
            z, b, j, k;
        if (v()) return false;
        return this.each(function () {
            g(this)
        })
    };
    Array.prototype.shuffle = function () {
        for (var f, g, h = 0; h < this.length; h++) {
            g = Math.floor(Math.random() * this.length);
            f = this[h];
            this[h] = this[g];
            this[g] = f
        }
    };
    d.fn.fancygallery.defaults = {
        thumbWidth: 140,
        thumbHeight: 79,
        backgroundColor: "#F5F5F5",
        titleColor: "#383634",
        borderThickness: 3,
        shadowOffset: 0,
        thumbOpacity: 0.6,
        titleHeight: 20,
        rowOffset: 15,
        columnOffset: 15,
        imagesPerPage: 6,
        textFadeDirection: "normal",
        scaleMode: "stretch",
        shadowImage: "images/fancygallery/shadow.png",
        hoverImage: "",
        hoverImageEffect: "fade",
        arrowPosition: "top",
        selectAlbum: "",
        dropdown: true,
        divider: true,
        showTitle: false,
        slideTitle: true,
        inverseHoverEffect: false,
        boxOptions: {}
    }
})(jQuery);