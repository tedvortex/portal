document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var _x=0;
var _y=0;

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		//  return {x:ev.pageX, y:ev.pageY};
		_x=ev.pageX;
		_Y=ev.pageY;
	} else   {

		_x=ev.clientX + document.body.scrollLeft - document.body.clientLeft;
		_Y=ev.clientY + document.body.scrollTop  - document.body.clientTop;


		//x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		//y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}


var dragObject  = null;
var mouseOffset = null;
function getMouseOffset(target, ev){
	ev = window.event;
	var docPos    = getPosition(target);
	//var mousePos  = mouseCoords(ev);
	return {x:_x - docPos.x, y:_y - docPos.y};
}
function getPosition(e){
	var left = 0;
	var top  = 0;
	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}
	left += e.offsetLeft;
	top  += e.offsetTop;
	return {x:left, y:top};
}
function mouseMove(ev){
	ev           = ev || window.event;
	mouseCoords(ev);
	if(dragObject){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = _y - mouseOffset.y;
		dragObject.style.left     =_x - mouseOffset.x;
		return false;
	}
}
function mouseUp(){
	dragObject = null;
}

function makeDraggable(el){


	dragObject  = el;
	mouseOffset = getMouseOffset(this);
	return false;

}

var last_w=1;
var last_h=1;

function drag_rotate() {
	var id=$("#"+last_id);

	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");



	var $_left=id.find(".rotate-btn").css('left').replace(/px/,"");
	var $_top=id.find(".rotate-btn").css('top').replace(/px/,"");
	echo($_top);
	echo2($_left);
	$_left=$_left*1-6;
	$_top=$_top*1+25;
	var rads = Math.atan($_top/$_left)*2;
	var $_angle=rads/(Math.PI/180);
	//echo($_angle);

	//return false;
	if ($_angle<0) $_angle=360+$_angle;
	echo2($_angle);


	var $_y=$_x=0;
	var $r= $_angle* (Math.PI/180);

	var $_el_el_h=id.find("svg image").attr('height');
	var $_el_el_w=id.find("svg image").attr('width');

	//var $_el_el_h=height;
	//var $_el_el_w=width;

	var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
	var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;

	var diff_w=(width-$_w)/2;
	var diff_h=(height-$_h)/2;

	if ($_angle>0 && $_angle<=90) {
		$_y=0;
		$_x=Math.abs(Math.sin($r))*$_el_el_h;
	}
	if ($_angle>90 && $_angle<=180) {
		$_y=Math.abs(Math.cos($r))*$_el_el_h;
		$_x=$_w;
	}
	if ($_angle>180 && $_angle<=270) {
		$_y=$_h;
		$_x=Math.abs(Math.cos($r))*$_el_el_w;
	}
	if ($_angle>270 && $_angle<=360) {
		$_y=Math.abs(Math.sin($r))*$_el_el_w;
		$_x=0;
	}
	//$_x=$_x+diff_w;
	//$_y=$_y+diff_h;
	//	echo(diff_w);
	id.find(".el_el").css('width',$_w).css('height',$_h);
	id.find(".el_el").css('width',$_w).css('height',$_h);
	//id.find(".el_el").css('top',top*1+"px").css('left',left*1+"px");
	id.find(".el_el").css('top',top*1+diff_h+"px").css('left',left*1+diff_w+"px");

	id.find("svg g").attr('transform','translate('+$_x+','+$_y+')');
	id.find("svg image").attr('transform','rotate('+$_angle+')');
	//echo($_angle);
}
function drag_me() {
	$('.rotate-btn').draggable({drag:function(){
		drag_rotate();
	},
	stop:function(){
		var id=$("#"+last_id);
		var $_angle=id.find(".rotate-btn").css('left','auto').css('right','-25px').css('top','-25px');
	},
	start:function(){
		var id=$("#"+last_id);
		last_h=id.find(".el_el").css('height').replace(/px/,"");
		last_w=id.find(".el_el").css('width').replace(/px/,"");
	}
	});
}
function echo(txt) {
	$('#meu').val(txt);
}
function echo3(txt) {
	$('#meu2').val($('#meu2').val()+"|"+txt);
}
function echo2(txt) {
	$('#meu2').val(txt);

}


var last_id='';
var text_nr=0;
var new_h=111;
function load_editable($_id){
	$( $_id ).find(".close-btn,.move-btn,.resize-btn,.rotate-btn").show();
	$( $_id ).resizable({
		//containment: "#c_v",
		//animate: true,
		handles: "all",
		aspectRatio: true,
		start: function(event, ui) {

			var id=$(this);
			last_w=id.css("width").replace(/px/,"");
			last_h=id.css("height").replace(/px/,"");
			
		},
		resize: function(event, ui) {
			//alert('sadsadsad');



			var id=$(this);
			id.parent().find(".el_w").val(id.css("width").replace(/px/,""));
			id.parent().find(".el_h").val(id.css("height").replace(/px/,""));





			var $_angle=$('#meu').val();
			var ang_rad= $_angle* (Math.PI/180);
			var w=id.css("width").replace(/px/,"");
			var h=id.css("height").replace(/px/,"");
			//alert(w);
			var width=(Math.cos(ang_rad)*h+Math.sin(ang_rad)*w)/2;
			var height=(Math.sin(ang_rad)*h+Math.cos(ang_rad)*w)/2;

			//height=((h-Math.cos(ang_rad)*w)/Math.sin(ang_rad));
			//w=141;
			//h=141;
			height=(Math.cos(ang_rad)*h-Math.sin(ang_rad)*w)/(Math.cos(ang_rad)*Math.cos(ang_rad)-Math.sin(ang_rad));
			//height=((h-Math.cos(ang_rad)*width)/Math.sin(ang_rad));
			width=height;
			//width = w* Math.sin(ang_rad) - h *Math.cos(ang_rad);


			//width =w*Math.sin(ang_rad);
			//height = h/1.41;

			var percent=h/last_h;
			echo(h);
			//echo2(last_h);


			//last_w=id.find("svg image").attr('width');
			//width=(id.find("svg image").attr('width')*w)/last_w;



			height=width;


			height=h;
			width=w;


			//echo(width);
			//var width=(Math.cos(ang_rad)*h);
			//			var height=(Math.cos(ang_rad)*h);



			//	id.find("svg").attr('width',w);
			//	id.find("svg").attr('height',h);
			//id.find("svg image").attr('width',width);
			
			new_h=percent*(id.find("svg image").attr('height')*1);
			
			echo(new_h);
			//alert();
			
			id.find("svg image").attr('height',new_h);
			//	id.find("svg g").attr('transform','translate('+x+',0)');
			//id.find("svg image").attr('transform','rotate('+$_angle+')');
			//last_w=w;
			//id.data("h",id.css("height").replace(/px/,""));
			//id.data("w",id.css("width").replace(/px/,""));


			last_h=h;
			last_w=w;
		}
	}).parent()
	.draggable({
		//containment: "#c_v" ,
		cancel:".close-btn,.rotate-btn,.resize-btn",
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".img").css('z-index'));
			id.css('overflow','');
			//id.find(".el_el").append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep" onmousedown=" makeDraggable(this);  " onmouseup="dragObject=null;"  title="roteste"></a><a class="move-btn spritep " title="muta"></a><a class="resize-btn spritep "  title="mareste"></a>');
			id.find(".el_el").append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep"  onmousemove="drag_me()" title="roteste"></a><a class="move-btn spritep " title="muta"></a><a class="resize-btn spritep "  title="mareste"></a>');


			//id.find.(".rotate-btn").draggable();


			//id.append('<a class="resize-btn spritep ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" title="mareste"></a>');


		},
		stop: function(event, ui) {
			var id=$(this);
			id.parent().find(".el_t").val(id.css("top").replace(/px/,""));
			id.parent().find(".el_l").val(id.css("left").replace(/px/,""));

		}
	});
}
function load_editor(){

	$( ".img" ).mouseup(function(){
		$(".edit_text").hide();
		$(".edit_img").show();
		var id=$(this).parent();
		last_id=id.attr('id');
		$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
		//$("#c_v .el_img>div,#c_v .el_txt>div").css('background-color','transparent');
		//id.css('background-color','#999999');
		//$(this).find(".ui-resizable-handle").show();


		$(".close-btn,.move-btn,.resize-btn,.rotate-btn").hide();

		$("#c_v .el_el").draggable("destroy").resizable("destroy");
		load_editable(this);
		$("#edit_r").val(id.parent().find('.el_r').val());
	});
	$( ".el_txt" ).mousedown(function(){
		$(".edit_text").show();
		$(".edit_img").hide();
		last_id=this.id;
		var id=$(this);
		$("#layer_edit").html('<input type="text" value="'+$(this).find(".txt").html()+'" onkeyup="edit_text(this.value);" />');
		$("#c_v .el_img>div,#c_v .el_txt>div").css('background-color','transparent');
		id.find('.div_txt').css('background-color','#999999');
		$("#edit_r").val(id.find('.el_r').val());
		$("#edit_ls").val(id.find('.txt_n').css('letter-spacing').replace(/px/,""));
		$("#edit_fs").val(id.find('.txt_n').css('font-size').replace(/px/,""));
	});

	$( ".div_txt" ).draggable({
		//containment: "#c_v" ,
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".txt").css('z-index'));
			id.css('overflow','');
		},
		stop: function(event, ui) {
			var id=$(this);
			id.find(".el_t").val(id.css("top").replace(/px/,""));
			id.find(".el_l").val(id.css("left").replace(/px/,""));
		}
	});

	/*$('.img').parent().parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});

	$('.txt').parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});*/
}
function go_front($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else new_z=id.css('z-index')*1+1;
	id.css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}
function go_back($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else {
		new_z=id.css('z-index')*1-1;
		if(new_z<0) new_z=0;
	}
	id.css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}

function el_rotate(degrees){
	degrees=$('#edit_r').val()*1+degrees;
	if (degrees<-180) degrees=(degrees*-1)-180;
	if (degrees>180) degrees=180-degrees;
	$( "#slider_r" ).slider('value',degrees);
	$('#edit_r').val(degrees);
	$( "#slider_r" ).slider('value',degrees);
	rotate_to(degrees);
}
function rotate_to($_angle) {
	$_angle=$_angle*1;
	if ($_angle<-180){
		$_angle=($_angle*-1)-180;
		$( "#edit_r" ).val($_angle);
	}
	if ($_angle>180) {
		$_angle=180-$_angle;
		$( "#edit_r" ).val($_angle);
	}

	$("#c_v .el_el").draggable("destroy").resizable("destroy");
	$("#"+last_id+" .img").height('auto');
	load_editable($("#"+last_id+" .img"));



	/*
	w=$("#"+last_id+">div").data("w");
	h=$("#"+last_id+">div").data("h");


	if ($_angle>90) {
	var ang_rad=(Math.PI/180)*($_angle-90);
	var width=(Math.cos(ang_rad)*h)+(Math.sin(ang_rad)*w);
	var height=Math.sin(ang_rad)*h+Math.cos(ang_rad)*w;
	} else {
	var ang_rad=(Math.PI/180)*$_angle;
	var width=(Math.cos(ang_rad)*w)+(Math.sin(ang_rad)*h);
	var height=Math.sin(ang_rad)*w+Math.cos(ang_rad)*h;
	}

	$("#"+last_id+">div").width(width);
	$("#"+last_id+">div").height(height);
	*/

	var $_url='rotate?i='+$("#"+last_id+" .el_n").val()+'&r='+$_angle;
	$("#layer_edit").html('<img src="'+$_url+'"  style="width:160px;" />');
	$("#"+last_id+" .img").attr('src',$_url);
	$("#"+last_id+" .el_r").val($_angle);
	//rotate($("#"+last_id+" .el_el"), $_angle);
}

function rotate(object, degrees) {
	object.css({
	'-webkit-transform' : 'rotate('+degrees+'deg)',
	'-moz-transform' : 'rotate('+degrees+'deg)',
	'-ms-transform' : 'rotate('+degrees+'deg)',
	'-o-transform' : 'rotate('+degrees+'deg)',
	'transform' : 'rotate('+degrees+'deg)',
	'filter': "progid:DXImageTransform.Microsoft.Matrix(M11=0.9914448613738104, M12=-0.13052619222005157,M21=0.13052619222005157, M22=0.9914448613738104, sizingMethod='auto expand');",
	'zoom' : 1
	});
}
function select_template($_id){
	view_2d();
	$("#c_v").load("3d?action_2=select_template&id="+$_id, function() {
		load_editor();
	});
}
document.load_edit_or= function() {
	load_editor();
}
function edit_text($_val){
	$("#"+last_id+" .txt").html($_val);
	$("#"+last_id+" .el_n").val($_val);
}
function new_text(){


	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;

	text_nr++;
	var dv=document.createElement('div');
	var txt='<div class="div_txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	//	+'<input class="txt_n txt el_el" name="txt_n[]"  type="text" value="'+$_value+'" /> <span class="c_move">|||||||||||||||||| MOVE ||||||||||||||||||</span>'
	+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'</div>';
	document.getElementById('c_v').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;
	parent.document.load_edit_or();
}
function el_align($_align) {
	if ($_align=='right' || $_align=='center' ) var $_left=$("#c_v").width()-($("#"+last_id+">div").width());
	if ($_align=='bottom' || $_align=='middle' ) var $_top=$("#c_v").height()-($("#"+last_id+">div").height());
	if ($_align=='left') {
		$("#"+last_id+">div").css('left',0);
		$("#"+last_id+" .el_l").val(0);
	}
	if ($_align=='right') {
		$("#"+last_id+">div").css('left',$_left);
		$("#"+last_id+" .el_l").val($_left);
	}
	if ($_align=='center') {
		$("#"+last_id+">div").css('left',$_left/2);
		$("#"+last_id+" .el_l").val($_left/2);
	}
	if ($_align=='top') {
		$("#"+last_id+">div").css('top',0);
		$("#"+last_id+" .el_t").val(0);
	}
	if ($_align=='bottom') {
		$("#"+last_id+">div").css('top',$_top);
		$("#"+last_id+" .el_t").val($_top);
	}
	if ($_align=='middle') {
		$("#"+last_id+">div").css('top',$_top/2);
		$("#"+last_id+" .el_t").val($_top/2);
	}
}
function font_size($_size) {
	$("#"+last_id+" .txt_n").css('font-size',$_size+"px");
	$("#"+last_id+" .el_h").val($_size);
}
function letter_s($_space) {
	$("#"+last_id+" .txt_n").css('letter-spacing',$_space+"px");
	$("#"+last_id+" .el_w").val($_space);
}
function view_3d(view){
	$('#viz').html('<div class="loading">Se incarca ...</div>');
	$('#viz').load('3d?id_view='+view+'&'+$('#my_form').serialize());
	$('#view_2d').hide();
	$('#viz').show();
}
function view_2d(){
	$('#viz').hide();
	$('#view_2d').show();
}
function  el_delete($id) {
	if ($id=='') {
		alert('Niciun element selectat');
	} else {
		if (confirm('Sunteti sigur ca dorioti sa stergeti elementul?')) $('#'+$id).remove(); else return false;
		last_id='';
	}
}
function  el_clear() {
	if (confirm('Sunteti sigur ca dorioti sa stergeti toate elementele?'))	$("#c_v").html('<div id="margin_top"></div><div id="margin_left"></div><div id="margin_right"></div><div id="margin_bottom"></div>');
	else return false;
}




