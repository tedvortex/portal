function add_script(nr,module,callback){

	var _prev=document.getElementById('refresh-'+nr)

	if(_prev)
		_scripts.removeChild(_prev)

	nr++

	var _script=document.createElement("script")
	_script.type='text/javascript'
	_script.src=_b+'ajax.json?module='+module+'&callback='+callback+'&_='+(new Date().getTime())
	_script.id='refresh-'+nr

	_scripts.appendChild(_script)

	_script.onload =
	_script.onreadystatechange =
	function() {
		if(
			typeof(t) !== 'undefined'
		){
			clearTimeout(t)
		}
		t=setTimeout(function(){add_script(nr,module,callback)},1000)
	}
}

function reload_page(timeout){
	setTimeout("window.location.reload(true);",timeout*1000)
}

var _scripts=document.getElementById('scripts')

_b = 'http://' + window.location.host + '/';

_p = {
	cart:{
		"in_progress":false,
		"success":"",
		"timestamp":0,
		"count":0,
		"total":0,
		"products":[]
	}
}

_wc    = $('li#website-cart');
_pdc = $('section#product-description');
_pdl = $('div#products-cart-list');
_cs  = $('ul#cart-step');

_discount_ranges=['','primul','al doilea','al treilea']
	
require(['window'], function(Window){
	parent.cart = function(id,amount,type,variation,obj){
		id=id.split('#');
		
		if(
				!variation
		){
			variation = 0;
		}
	
		id=id[0];
	
		$.ajax({
			url:_b+'ajax.json',
			dataType:'jsonp',
			cache:false,
			data:
			({
				module:'cart',
				id:id,
				amount:amount,
				type:type,
				variation:variation
			}),
			success:function(data){
				/*_p.cart.timestamp=data.timestamp*/
	
				if(data.success){
					if(
						type=='update' &&
						obj
					){
						var key = obj.attr('class').split('-')[1]
	
						var d=$('li.product-'+key)
	
						var price=$('input[name=\"price\[\]\"]',d).val()
	
						$('tr.price-init section.price p',d).html('<span>'+price+'</span>'+'&#160;lei')
						$('td.price-total strong',d).html(number_format(price*amount,2,'.',''))
					}
	
	
					if(type=='remove'){
						var d = $('li.product-'+id)
	
						d.slideUp(
							'fast',
							function(){
								d.remove()
	
								Window.init(data.success,'Actiune reusita!','success')
	
								if(
									$('li',_pdl).length==0
								){
									_pdl.html('<p class=\"no-products\">Niciun produs in cos.</p>')
									_cs.slideUp(
										'slow',
										function(){
											_cs.remove()
										}
									)
								}
							}
						)
					}
	
					else{
						Window.init(data.success,'Actiune reusita!','success')
					}
	
					_p.cart.in_progress=false
				}
	
				else if (data.error){
					Window.init(data.error, 'Actiune nereusita !', 'error');
				}
			}
		})
	}
});

window.productsInProgress = false;

function _products(obj){
	var p = parent._wc;

	if (obj.timestamp>_p.cart.timestamp && ! window.productsInProgress) {	
		$('div#cart-products,section#cart-total',p).html('')

		$('section#product-discount',_pdc).html('')

		$('div#order-total')
		.find('td#total-step-1 sup,td#shopcart-total-order sup,td#shopcart-total-transport sup,td#shopcart-total-tax sup')
		.html('')

		$('div#total-pink .price-total')
		.find('strong')
		.html('')


		$('span#total-price-shopcart,span#total-weight-shopcart')
		.html('')

		var l=0

		if(
			typeof(obj.products)=='object'
		){
			for(var i in obj.products){
				if(
					typeof( obj.products[i] ) !== 'function'
				){
					l++
				}
			}
		}

		if(
			_pdl.length==1 &&
			l!=$('li',_pdl).length
		){
			Window.init('Comanda dumneavoastra a fost modificata dintrun tab extern, pagina va fi reincarcata pentru a reflecta modificarile in:<br/><br/>5 secunde.', 'Actiune nereusita !', 'error');

			reload_page(5)
		}

		if(l!==0){
			$('div#cart-products',_wc).append('<ul/>')

			$('section#cart-total',_wc).append('<p id="total-price">Total = <span>'+obj.total+' '+obj.currency+'</span>'+(parseFloat(obj.discount)>0?' <span class="discount">(Discount '+obj.discount+'%)</span>':'')+'</p>'+(obj.level>0?'<p class="discount-range">'+(obj.level>0?'Pentru '+_discount_ranges[obj.level]+' prag de discount mai ai nevoie de produse in valoare de <span class="white">'+obj.next+' '+obj.currency:'Ai atins ultimul prag de discount!')+'</span></p><p><a class="discount-range-checker">Verifica grila discount</a></p>':''))

			$('section#product-discount',_pdc).append((obj.level>0?'<p class="strong">Pentru <span> '+_discount_ranges[obj.level]+'</span> prag de discount</p><p class="light">mai ai nevoie de produse in valoare de</p><div><p class="needed"><span>'+obj.next+' </span> '+obj.currency+'</p></div>':'<p class="final-level">Ai atins ultimul nivel de discount <span>(2%)</span></p>'))

			if(parseFloat(obj.total)>0){
				$('section#cart-total',_wc).append('<a class="button" href="http://'+window.location.host+'/comanda">vezi cos</a>')

				$('div#order-total td#total-step-1 sup').html(obj.total)
				$('div#order-total td#shopcart-total-order sup').html(obj.total)

				$('div#order-total td#shopcart-total-transport sup').html(obj.shipping)
				$('div#order-total td#shopcart-total-tax sup').html(obj.total_ex)

				$('div#total-pink .price-total strong').html(obj.total)
				$('span#total-price-shopcart').html(obj.total)
				$('span#total-weight-shopcart').html(obj.w)

				if(parseFloat(obj.d)>0){
					$('div#total-pink .price-discount').find('sup').html(obj.d).end().removeClass('n')
				}

				else{
					$('div#total-pink .price-discount').addClass('n');
				}
			}

			for(var i in obj.products){
				v=obj.products[i]
				v.e=htmlentities(v.n,'ENT_COMPAT','UTF-8')
				
				$('div#cart-products ul',_wc).append('<li id="product-'+i+'" class="'+(i%2==0?'odd':'even')+'"><a class="image external" style="background:url(\''+v.image+'\') center center no-repeat" href="'+v.l+'" title="'+v.e+'"></a><a class="external name" href="'+v.l+'" title="'+v.e+'">'+v.n+'</a><p>'+v.p+'</p><a class="delete sprite" title="Sterge" href="#'+i+'"></a></li>')
				if(
					_pdl.length==1 &&
					v.q!=$('input.product-'+v.k,_pdl).val()
				){
					Window.init('Comanda dumneavoastra a fost modificata dintrun tab extern, pagina va fi reincarcata pentru a reflecta modificarile in:<br/><br/>5 secunde.', 'Actiune nereusita !', 'error');
					reload_page(5)
				}
			}
		}

		else
			$('div#cart-products',_wc).append('<p class="no-products">Niciun produs in cos.</p>')


		$('span#cart-count').html(obj.count)

		if(
			$('.total-info').length > 0
		){
			$('.total-info').text('(' + obj.count + ' produse)');
		}

		_p.cart.timestamp=obj.timestamp;
		_p.cart.products=obj.products;
		_p.cart.count=obj.count;
		_p.cart.total=obj.total;
	}
}

require(['php'], function(){
	add_script(new Date().getTime(), 'products', '_products');
});