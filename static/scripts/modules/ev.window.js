define(
		[ 'window' ],
		function(Window) {
			var Window = {
				progress : false,
				html : true,
				element : null,
				regexp : /^[\s]*\</gi,

				init : function(message, header, type) {
					Window.html = true;

					if (Window.element === null) {
						$('div#scripts').append('<div id="window"><div><span class="icon"></span><div><h5></h5><div class="description"></div></div></div></div>');

						Window.element = $('div#window');
					}

					if (!message.match(Window.regexp)) {
						Window.html = false;
						message = '<p>' + message + '</p>';
					}

					if (typeof (header) == 'null') {
						header = 'Error';
					}

					if (typeof (type) == 'null') {
						header = 'alert';
					}

					$('h5', Window.element).html(header);

					$('div.description', Window.element).html(message);

					Window.element.attr('class', type);

					if (!Window.html) {
						var paragraph_height = (Window.element.outerHeight() - $('div.description p', Window.element).outerHeight() - $('h5', Window.element).outerHeight() * 2) / 2;

						if (paragraph_height > 0) {
							$('div.description p', Window.element).css('margin-top', paragraph_height);
						}
					}

					setTimeout(function() {
						$.lightbox('#window', {
							options : [ {
								'modal' : true,
								'width' : 526
							} ]
						});

						Window.progress = false;
					}, 333);
				},

				parse_hash : function() {
					if (window.location.hash.length > 0) {
						var a = window.location.hash.split('#')[1].split(',');

						if (typeof (a[0]) == 'string' && a[0] == 'action') {
							$.ajax({
								url : 'http://' + window.location.host + '/ajax.json',
								data : {
									module : 'window',
									action : a[1],
									type : a[2],
									vars : a.length > 3 ? a[3] : ''
								},
								dataType : 'json',
								success : function(data) {
									Window.init(data.message, data.header, a[2]);

									if (typeof ($.browser.msie) == 'undefined'
											|| !($.browser.msie && parseFloat($.browser.version) < 9)) {
										window.location.hash = '';
									}
								}
							});
						}
					}
				}
			}

			return Window;
		});