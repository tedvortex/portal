document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;
var _x=0;
var _y=0;
var l_zoom=1;
var s_zoom=1;
var dragObject  = null;
var mouseOffset = null;
var last_w=1;
var last_h=1;
var last_id='';
var text_nr=0;

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		//  return {x:ev.pageX, y:ev.pageY};
		_x=ev.pageX;
		_Y=ev.pageY;
	} else   {

		_x=ev.clientX + document.body.scrollLeft - document.body.clientLeft;
		_Y=ev.clientY + document.body.scrollTop  - document.body.clientTop;


		//x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		//y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function getMouseOffset(target, ev){
	ev = window.event;
	var docPos    = getPosition(target);
	//var mousePos  = mouseCoords(ev);
	return {x:_x - docPos.x, y:_y - docPos.y};
}
function getPosition(e){
	var left = 0;
	var top  = 0;
	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}
	left += e.offsetLeft;
	top  += e.offsetTop;
	return {x:left, y:top};
}
function mouseMove(ev){
	ev           = ev || window.event;
	mouseCoords(ev);
	if(dragObject){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = _y - mouseOffset.y;
		dragObject.style.left     =_x - mouseOffset.x;
		return false;
	}
}
function mouseUp(){
	dragObject = null;
}

function makeDraggable(el){


	dragObject  = el;
	mouseOffset = getMouseOffset(this);
	return false;

}

function transalate($_angle) {

	var id=$("#"+last_id);
	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");


	var $_y=$_x=0;
	var $r= $_angle* (Math.PI/180);

	var $_el_el_h=id.find("svg image").attr('height');
	var $_el_el_w=id.find("svg image").attr('width');

	//var $_el_el_h=height;
	//var $_el_el_w=width;

	var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
	var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;

	var diff_w=(width-$_w)/2;
	var diff_h=(height-$_h)/2;

	if ($_angle>0 && $_angle<=90) {
		$_y=0;
		$_x=Math.abs(Math.sin($r))*$_el_el_h;
	}
	if ($_angle>90 && $_angle<=180) {
		$_y=Math.abs(Math.cos($r))*$_el_el_h;
		$_x=$_w;
	}
	if ($_angle>180 && $_angle<=270) {
		$_y=$_h;
		$_x=Math.abs(Math.cos($r))*$_el_el_w;
	}
	if ($_angle>270 && $_angle<=360) {
		$_y=Math.abs(Math.sin($r))*$_el_el_w;
		$_x=0;
	}

	//$_x=$_x+diff_w;
	//$_y=$_y+diff_h;

	id.find(".el_el").css('width',$_w).css('height',$_h);
	id.find(".el_el").css('width',$_w).css('height',$_h);
	//id.find(".el_el").css('top',top*1+"px").css('left',left*1+"px");
	id.find(".el_el").css('top',top*1+diff_h+"px").css('left',left*1+diff_w+"px");
	id.find("svg g").attr('transform','translate('+$_x+','+$_y+')');
}
function rotate($_angle) {
	var id=$("#"+last_id);
	transalate($_angle);
	id.find("svg image").attr('transform','rotate('+$_angle+')');
	id.find(".el_r").val($_angle);
}
function drag_rotate() {
	var id=$("#"+last_id);

	var $_left=id.find(".rotate-btn").css('left').replace(/px/,"");
	var $_top=id.find(".rotate-btn").css('top').replace(/px/,"");

	$_left=$_left*1-6;
	$_top=$_top*1+21;
	var rads = Math.atan($_top/$_left)*2;
	var $_angle=rads/(Math.PI/180);
	$_angle=parseInt($_angle, 10);

	if ($_angle<0) $_angle=360+$_angle;
	rotate($_angle);
}
function drag_me() {
	$('.rotate-btn').draggable({drag:function(){
		drag_rotate();
	},
	stop:function(){
		var id=$("#"+last_id);
		id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
	},
	start:function(){
		var id=$("#"+last_id);
		last_h=id.find(".el_el").css('height').replace(/px/,"");
		last_w=id.find(".el_el").css('width').replace(/px/,"");
	}
	});
}
function echo(txt) {
	$('#meu').val(txt);
}
function echo3(txt) {
	$('#meu2').val($('#meu2').val()+"|"+txt);
}
function echo2(txt) {
	$('#meu2').val(txt);

}


function load_editable($_id){
	var id=$( $_id );
	id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
	id.find(".close-btn,.move-btn,.rotate-btn").show();
	id.resizable({
		//containment: "#c_v",
		//animate: true,
		handles: "n, e, s, w,se",
		aspectRatio: true,
		start: function(event, ui) {

			var id=$(this);
			last_w=id.css("width").replace(/px/,"");
			last_h=id.css("height").replace(/px/,"");

		},
		resize: function(event, ui) {
			//alert('sadsadsad');



			var id=$(this);
			id.parent().find(".el_w").val(id.css("width").replace(/px/,""));
			id.parent().find(".el_h").val(id.css("height").replace(/px/,""));





			var $_angle=$('#meu').val();
			var ang_rad= $_angle* (Math.PI/180);
			var w=id.css("width").replace(/px/,"");
			var h=id.css("height").replace(/px/,"");
			//alert(w);
			var width=(Math.cos(ang_rad)*h+Math.sin(ang_rad)*w)/2;
			var height=(Math.sin(ang_rad)*h+Math.cos(ang_rad)*w)/2;

			//height=((h-Math.cos(ang_rad)*w)/Math.sin(ang_rad));
			//w=141;
			//h=141;
			height=(Math.cos(ang_rad)*h-Math.sin(ang_rad)*w)/(Math.cos(ang_rad)*Math.cos(ang_rad)-Math.sin(ang_rad));
			//height=((h-Math.cos(ang_rad)*width)/Math.sin(ang_rad));
			width=height;
			//width = w* Math.sin(ang_rad) - h *Math.cos(ang_rad);


			//width =w*Math.sin(ang_rad);
			//height = h/1.41;

			var percent_h=h/last_h;
			var percent_w=w/last_w;



			//last_w=id.find("svg image").attr('width');
			//width=(id.find("svg image").attr('width')*w)/last_w;






			//var width=(Math.cos(ang_rad)*h);
			//			var height=(Math.cos(ang_rad)*h);



			//	id.find("svg").attr('width',w);
			//	id.find("svg").attr('height',h);


			new_h=percent_h*(id.find("svg image").attr('height')*1);
			new_w=percent_w*(id.find("svg image").attr('width')*1);



			id.find("svg image").attr('height',new_h);
			id.find("svg image").attr('width',new_w);
			var $_angle=id.parent().find(".el_r").val();
			transalate($_angle);
			//	id.find("svg g").attr('transform','translate('+x+',0)');
			//id.find("svg image").attr('transform','rotate('+$_angle+')');
			//last_w=w;
			//id.data("h",id.css("height").replace(/px/,""));
			//id.data("w",id.css("width").replace(/px/,""));


			last_h=h;
			last_w=w;
		}
	}).parent()
	.draggable({
		//containment: "#c_v" ,
		cancel:".close-btn,.rotate-btn",
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".img").css('z-index'));
			id.css('overflow','');
			//id.find(".el_el").append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep" onmousedown=" makeDraggable(this);  " onmouseup="dragObject=null;"  title="roteste"></a><a class="move-btn spritep " title="muta"></a><a class="resize-btn spritep "  title="mareste"></a>');
			id.find(".el_el").append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep"  onmousemove="drag_me()" title="roteste"></a><a class="move-btn spritep " title="muta"></a>');


			//id.find.(".rotate-btn").draggable();


			//id.append('<a class="resize-btn spritep ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" title="mareste"></a>');


		},
		stop: function(event, ui) {
			var id=$(this);
			id.parent().find(".el_t").val(id.css("top").replace(/px/,""));
			id.parent().find(".el_l").val(id.css("left").replace(/px/,""));

		}
	});
}
function load_editor(){
	$( ".img" ).mouseup(function(){
		$(".edit_text").hide();
		$(".edit_img").show();
		var id=$(this).parent();
		last_id=id.attr('id');
		$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
		$(".close-btn,.move-btn,.rotate-btn").hide();
		$("#c_v .el_el").draggable("destroy").resizable("destroy");
		load_editable(this);
		$("#edit_r").val(id.parent().find('.el_r').val());
	});
	$( ".el_txt" ).mouseup(function(){
		var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;
		if (id.data("b")!=1){
			$("#edit_r").val(id.find('.el_r').val());
			$("#c_v .txt").resizable("destroy").draggable("destroy");
			load_editable(last_id);
		}
	});
	
	$( ".txt" ).mouseup(function(){
		select_tab(3);
		var id=$(this);
		last_id=this.id;
		$("#layer_edit_img").html('');
		$("#layer_edit_txt").html('<textarea style="width:100%"  onkeyup="edit_text(this.value);">'+id.parent().find(".el_n").val()+' </textarea>');
		$("#font_size").val(id.parent().find(".el_h").val());

		//$("#edit_ls").val(id.find('.txt_n').css('letter-spacing').replace(/px/,""));
		//$("#edit_fs").val(id.find('.txt_n').css('font-size').replace(/px/,""));
	});
	/*
	
	$( ".el_img" ).mouseup(function(){
		
		
		var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;
		if (id.data("b")!=1){
			$("#edit_r").val(id.find('.el_r').val());
			$("#c_v .img").resizable("destroy").draggable("destroy");
			load_editable(last_id);
		}
	});

	$( ".el_txt" ).mouseup(function(){
		var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;
		if (id.data("b")!=1){
			$("#edit_r").val(id.find('.el_r').val());
			//$("#c_v .img").resizable("destroy").draggable("destroy");
			$("#c_v .txt").resizable("destroy").draggable("destroy");
			//$("#c_v .txt").draggable("destroy");
			load_editable(last_id);
		}
	});
	
	
	$( ".img" ).mouseup(function(){
		$(".edit_text").hide();
		$(".edit_img").show();
		var id=$(this).parent();
		last_id=id.attr('id');
		$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
		$(".close-btn,.move-btn,.resize-btn,.rotate-btn").hide();
		$("#c_v .el_el").draggable("destroy").resizable("destroy");
		load_editable(this);
		$("#edit_r").val(id.parent().find('.el_r').val());
		
		select_tab(4);
		$("#layer_edit_img").html('<img src="'+this.src+'"  style="width:160px;" />');
		$("#layer_edit_txt").html('');
	});
	$( ".txt" ).mouseup(function(){
		select_tab(3);
		var id=$(this);
		last_id=this.id;
		$("#layer_edit_img").html('');
		$("#layer_edit_txt").html('<textarea style="width:100%"  onkeyup="edit_text(this.value);">'+id.parent().find(".el_n").val()+' </textarea>');
		$("#font_size").val(id.parent().find(".el_h").val());

		//$("#edit_ls").val(id.find('.txt_n').css('letter-spacing').replace(/px/,""));
		//$("#edit_fs").val(id.find('.txt_n').css('font-size').replace(/px/,""));
	});
	*/
	$( ".div_txt" ).draggable({
		//containment: "#c_v" ,
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".txt").css('z-index'));
			id.css('overflow','');
		},
		stop: function(event, ui) {
			var id=$(this);
			id.find(".el_t").val(id.css("top").replace(/px/,""));
			id.find(".el_l").val(id.css("left").replace(/px/,""));
		}
	});

	/*$('.img').parent().parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});

	$('.txt').parent().contextMenu('myMenu1', {
	bindings: {
	'delete': function(t) {
	$('#'+t.id).remove();
	},
	'front': function(t) {
	go_front(t.id);
	},
	'back': function(t) {
	go_back(t.id);
	},
	'cut': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'copy': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	},
	'paste': function(t) {
	alert('Trigger was '+t.id+'\nAction was Delete');
	}
	}

	});*/
}
function go_front($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else new_z=id.css('z-index')*1+1;
	id.css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}
function go_back($id) {
	var id=$("#"+$id+">div");
	var new_z=0;
	if (id.css('z-index')=='auto') new_z=0; else {
		new_z=id.css('z-index')*1-1;
		if(new_z<0) new_z=0;
	}
	id.css('z-index',new_z);
	$("#"+$id).find(".el_z").val(new_z);
}

function el_rotate(degrees){
	degrees=$('#edit_r').val()*1+degrees;
	if (degrees<-180) degrees=(degrees*-1)-180;
	if (degrees>180) degrees=180-degrees;
	$( "#slider_r" ).slider('value',degrees);
	$('#edit_r').val(degrees);
	$( "#slider_r" ).slider('value',degrees);
	rotate_to(degrees);
}
function rotate_to($_angle) {
	$_angle=$_angle*1;
	if ($_angle<-180){
		$_angle=($_angle*-1)-180;
		$( "#edit_r" ).val($_angle);
	}
	if ($_angle>180) {
		$_angle=180-$_angle;
		$( "#edit_r" ).val($_angle);
	}

	$("#c_v .el_el").draggable("destroy").resizable("destroy");
	$("#"+last_id+" .img").height('auto');
	load_editable($("#"+last_id+" .img"));



	/*
	w=$("#"+last_id+">div").data("w");
	h=$("#"+last_id+">div").data("h");


	if ($_angle>90) {
	var ang_rad=(Math.PI/180)*($_angle-90);
	var width=(Math.cos(ang_rad)*h)+(Math.sin(ang_rad)*w);
	var height=Math.sin(ang_rad)*h+Math.cos(ang_rad)*w;
	} else {
	var ang_rad=(Math.PI/180)*$_angle;
	var width=(Math.cos(ang_rad)*w)+(Math.sin(ang_rad)*h);
	var height=Math.sin(ang_rad)*w+Math.cos(ang_rad)*h;
	}

	$("#"+last_id+">div").width(width);
	$("#"+last_id+">div").height(height);
	*/

	var $_url='rotate?i='+$("#"+last_id+" .el_n").val()+'&r='+$_angle;
	$("#layer_edit").html('<img src="'+$_url+'"  style="width:160px;" />');
	$("#"+last_id+" .img").attr('src',$_url);
	$("#"+last_id+" .el_r").val($_angle);
	//rotate($("#"+last_id+" .el_el"), $_angle);
}
/*
function rotate(object, degrees) {
object.css({
'-webkit-transform' : 'rotate('+degrees+'deg)',
'-moz-transform' : 'rotate('+degrees+'deg)',
'-ms-transform' : 'rotate('+degrees+'deg)',
'-o-transform' : 'rotate('+degrees+'deg)',
'transform' : 'rotate('+degrees+'deg)',
'filter': "progid:DXImageTransform.Microsoft.Matrix(M11=0.9914448613738104, M12=-0.13052619222005157,M21=0.13052619222005157, M22=0.9914448613738104, sizingMethod='auto expand');",
'zoom' : 1
});
}*/
function select_template($_id){
	view_2d();
	$("#view_2d").load("3d?action_2=select_template&id="+$_id, function() {
		load_editor();
	});
}
document.load_edit_or= function() {
	load_editor();
}


function reload_text(){
	var text=$("#layer_edit_txt textarea").val();
	var font_size=$("#font_size").val();
	$("#"+last_id+" .txt").attr('src','3d?action=text&t='+text+'&size='+font_size);
	$("#"+last_id+" .el_h").val(font_size);
}
function edit_text($_val){
	$("#"+last_id+" .el_n").val($_val);
	reload_text();
}

function new_text(){


	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;

	text_nr++;
	var dv=document.createElement('div');
	var txt='<div class="div_txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	//	+'<input class="txt_n txt el_el" name="txt_n[]"  type="text" value="'+$_value+'" /> <span class="c_move">|||||||||||||||||| MOVE ||||||||||||||||||</span>'
	+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;
	parent.document.load_edit_or();
}
function el_align($_align) {
	if ($_align=='right' || $_align=='center' ) var $_left=$("#c_v").width()-($("#"+last_id+">div").width());
	if ($_align=='bottom' || $_align=='middle' ) var $_top=$("#c_v").height()-($("#"+last_id+">div").height());
	if ($_align=='left') {
		$("#"+last_id+">div").css('left',0);
		$("#"+last_id+" .el_l").val(0);
	}
	if ($_align=='right') {
		$("#"+last_id+">div").css('left',$_left);
		$("#"+last_id+" .el_l").val($_left);
	}
	if ($_align=='center') {
		$("#"+last_id+">div").css('left',$_left/2);
		$("#"+last_id+" .el_l").val($_left/2);
	}
	if ($_align=='top') {
		$("#"+last_id+">div").css('top',0);
		$("#"+last_id+" .el_t").val(0);
	}
	if ($_align=='bottom') {
		$("#"+last_id+">div").css('top',$_top);
		$("#"+last_id+" .el_t").val($_top);
	}
	if ($_align=='middle') {
		$("#"+last_id+">div").css('top',$_top/2);
		$("#"+last_id+" .el_t").val($_top/2);
	}
}
function font_size($_size) {
	$("#"+last_id+" .txt_n").css('font-size',$_size+"px");
	$("#"+last_id+" .el_h").val($_size);
}
function letter_s($_space) {
	$("#"+last_id+" .txt_n").css('letter-spacing',$_space+"px");
	$("#"+last_id+" .el_w").val($_space);
}
function view_3d(view){
	$('#viz').html('<div class="loading">Se incarca ...</div>');
	$('#viz').load('3d?id_view='+view+'&'+$('#my_form').serialize());
	$('#view_2d').hide();
	$('#viz').show();
}
function view_2d(){
	$('#viz').hide();
	$('#view_2d').show();
}
function  el_delete($id) {
	if ($id=='') {
		alert('Niciun element selectat');
	} else {
		if (confirm('Sunteti sigur ca dorioti sa stergeti elementul?')) $('#'+$id).remove(); else return false;
		last_id='';
	}
}
function  el_clear() {
	if (confirm('Sunteti sigur ca dorioti sa stergeti toate elementele?'))	$("#c_v").html('<div id="margin_top"></div><div id="margin_left"></div><div id="margin_right"></div><div id="margin_bottom"></div>');
	else return false;
}




function new_img_text(){
	/*var txt='<img class="img" style="z-index:<?=$_el['el_z']?>; left:<?=$_el['el_l']?>px; top:<?=$_el['el_t']?>px;height:<?=$_el['el_h']?>px; width:<?=$_el['el_w']?>px;" src="images/<?=$_el['value']?>" />'
	+'<input type="hidden" class="el_n" name="img_n[]" value="<?=$_el['value']?>"/>'
	+'<input type="hidden" class="el_t" name="img_t[]" value="<?=$_el['el_t']?>"/>'
	+'<input type="hidden" class="el_l" name="img_l[]" value="<?=$_el['el_l']?>"/>'
+'<input type="hidden" class="el_h" name="img_h[]" value="<?=$_el['el_h']?>"/>'
	+'<input type="hidden" class="el_w" name="img_w[]" value="<?=$_el['el_w']?>"/>'
	+'<input type="hidden" class="el_z" name="img_z[]" value="<?=$_el['el_z']?>"/>'
	+'<input type="hidden" class="el_r" name="img_r[]" value="<?=$_el['el_r']?>"/>';*/



	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;

	text_nr++;
	var dv=document.createElement('div');
	//var txt='<div class="el_img" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	var txt='<img class="el_el txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" src="3d?action=text&t='+$_value+'&size='+$_el_h+'" />'
	//	+'<input class="txt_n txt el_el" name="txt_n[]"  type="text" value="'+$_value+'" /> <span class="c_move">|||||||||||||||||| MOVE ||||||||||||||||||</span>'
	//+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>';
	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;

	load_editor();

	//parent.document.load_edit_or();


}

function  blocheaza($id) {
	var id=$("#"+$id);
	id.data("b",1);
}


function zoom_out() {
	if(l_zoom>1) {
		var zoom=(1-(1/l_zoom));
		do_zoom(zoom);
		l_zoom--;
	}
	return false;
}
function zoom_in() {
	if(l_zoom<5) {
		var zoom=(1+1/l_zoom)
		do_zoom(zoom);
		l_zoom++;
	}
	return false;
}
function zoom_1x() {
	var zoom=1/l_zoom;
	do_zoom(zoom);
	l_zoom=1;
	$("#c_v_zoom").css('left','0');
	$("#c_v_zoom").css('top','0');
}
function do_zoom(zoom) {
	$("#c_v_zoom").draggable({
		cancel:".close-btn,.rotate-btn",
		create: function(event, ui) {

		},
		drag: function(event, ui) {

		}
	});
	$("#c_v .img").resizable("destroy").draggable("destroy");
	$("#c_v .txt").draggable("destroy");
	var id=$("#c_v_zoom");
	id.css("width",id.css("width").replace(/px/,"")*zoom);
	id.css("height",id.css("height").replace(/px/,"")*zoom);
	$('#c_v .el_el').each(function(i) {
		var id=$(".el_el:eq("+i+")");
		id.css("top",id.css("top").replace(/px/,"")*zoom);
		id.css("left",id.css("left").replace(/px/,"")*zoom);
		id.css("width",id.css("width").replace(/px/,"")*zoom);
		id.css("height",id.css("height").replace(/px/,"")*zoom);
		var svg=id.find("svg image");
		var g=id.find("svg g");
		var translate=g.attr('transform').replace("translate(","").replace(")","").split(",");
		g.attr("transform","translate("+(translate[0]*1)*zoom+","+(translate[1]*1)*zoom+")");
		svg.attr('height',svg.attr('height')*zoom);
		svg.attr('width',svg.attr('width')*zoom);
	});
}
function select_tab(nr) {
	$(".tab").hide();
	$("#tab-"+nr).show();
	$("#tab-nav a").removeClass("selected");
	$(".tab-"+nr+" a").addClass("selected");
}

function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function  clone($id) {

	$id=last_id;
	//alert($id);
	//$("#c_v .el_el").resizable("destroy").draggable("destroy");



	var top=($('#'+$id+">div").css("top").replace(/px/,""))*1+10;
	var left=($('#'+$id+">div").css("left").replace(/px/,""))*1+10;


	var clone =$('#'+$id).clone();
	//alert(clone.html());
	//alert(clone.attr('id')+"_c");

	clone.attr('id',clone.attr('id')+"_c");
	clone.append(clone.find('.el_el'));
	clone.find('.el_el').removeClass('ui-resizable');
	//.find(".ui-resizable-handle,.close-btn,.move-btn,.resize-btn,.rotate-btn").remove();
	clone.find('.ui-draggable').remove();
	clone.find(".el_el").css('top',top+'px').css('left',left+'px').css('position','absolute');
	$("#c_v_zoom").append(clone);
	$("#c_v .el_el").resizable("destroy").draggable("destroy");
	$("#"+last_id+" .close-btn,#"+last_id+" .move-btn,#"+last_id).hide();
	load_editor();
	return false;

	$( ".el_img" ).mouseup(function(){
		$(".edit_text").hide();
		$(".edit_img").show();
		var id=$(this);
		last_id=this.id;
		//alert(last_id);
		if (id.data("b")!=1){
			$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
			//$("#c_v .el_img>div,#c_v .el_txt>div").css('background-color','transparent');
			//id.css('background-color','#999999');
			//$(this).find(".ui-resizable-handle").show();

			$("#c_v .el_el").resizable("destroy").draggable("destroy");
			load_editable(last_id);
			$("#edit_r").val(id.parent().find('.el_r').val());
		}
	});



}