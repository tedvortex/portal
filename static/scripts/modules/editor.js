var _x=0;
var _y=0;
var l_zoom=1;
var s_zoom=1;
var dragObject  = null;
var mouse$_angle_add = null;
var last_w=1;
var last_h=1;
var last_id='';
var text_nr=0;
var img_nr=0;
var last_angle=0;
var max_z=0;
var last_h_text=40;
var $_nr_var=0;
var var_txt_prices=new Array();
var var_img_prices=new Array();
var var_options=new Array();
var actions=new Array();
var actions_p=new Array();
var actions_i=0;
var last_el=0;
var $_last_view_3d=0;
var $_last_view_2d=0;
var $_price_total_img=0;
var $_price_total_calcul=0;
var $_price_total_txt=0;

function werror($_text) {
	alert($_text);
}
function transalate($_angle,$_center_r) {
	var id=$("#"+last_id);
	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");
	var $_y=$_x=0;
	var $r= $_angle* (Math.PI/180);
	var $_el_el_h=id.find("svg image").attr('height');
	var $_el_el_w=id.find("svg image").attr('width');
	var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
	var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;
	var diff_w=(width-$_w)/2;
	var diff_h=(height-$_h)/2;
	var transl_sc=0;
	if ($_angle>0 && $_angle<=90) {
		$_y=0;
		$_x=Math.abs(Math.sin($r))*$_el_el_h;
		transl_sc=$_h;
	}
	if ($_angle>90 && $_angle<=180) {
		$_y=Math.abs(Math.cos($r))*$_el_el_h;
		$_x=$_w;
		transl_sc=$_w;
	}
	if ($_angle>180 && $_angle<=270) {
		$_y=$_h;
		$_x=Math.abs(Math.cos($r))*$_el_el_w;
		transl_sc=$_h;
	}
	if ($_angle>270 && $_angle<=360) {
		$_y=Math.abs(Math.sin($r))*$_el_el_w;
		$_x=0;
		transl_sc=$_w;
	}
	id.find(".el_el").css('width',$_w).css('height',$_h);
	if ($_center_r==null) id.find(".el_el").css('top',top*1+diff_h+"px").css('left',left*1+diff_w+"px");


	//id.find("svg g").attr('transform','scale(1,-1) translate(0,-'+$_w+') translate('+$_x+','+$_y+')');
	//id.find("svg g").attr('transform','scale(-1,1) translate(-'+$_h+',0) translate('+$_x+','+$_y+')');


	//id.find("svg g").attr('transform','scale(1,-1) translate(0,-'+transl_sc+')');
	id.find("svg g").attr('transform','translate('+$_x+','+$_y+')');
}
function rotate($_angle) {
	var id=$("#"+last_id);
	transalate($_angle);
	id.find("svg image").attr('transform','rotate('+$_angle+')');
	//last_angle=$_angle;
	id.find(".el_r").val($_angle);
}
function drag_rotate() {
	var id=$("#"+last_id);

	var height=id.find(".el_el").css('height').replace(/px/,"");
	var width=id.find(".el_el").css('width').replace(/px/,"");
	var top=id.find(".el_el").css('top').replace(/px/,"");
	var left=id.find(".el_el").css('left').replace(/px/,"");


	var last_id_rotate=$("#c_v_zoom>.rotate-btn");
	var $_left=last_id_rotate.css('left').replace(/px/,"");
	var $_top=last_id_rotate.css('top').replace(/px/,"");

	$_left=$_left-left;
	$_top=$_top-top;

	//$_left=$_left*1-6;
	$_top=$_top*1+21;

	$_left=($_left-(width/2));
	$_top=($_top-(height/2));

	//var rads = Math.atan($_top/$_left)*2;
	var rads = Math.atan($_top/$_left);
	var $_angle=rads/(Math.PI/180);
	var $_angle_add=0;
	if($_top < 0) {
		if($_left < 0) {
			$_angle_add = 270;
		} else {
			$_angle_add = 90;
		}
	} else {
		if($_left < 0) {
			$_angle_add = 270;
		} else {
			$_angle_add = 90;
		}
	}
	$_angle=$_angle+ $_angle_add;
	$_angle=$_angle-45;
	$_angle=parseInt($_angle, 10);
	$_angle=parseInt(last_angle)+$_angle;

	if ($_angle>=360) $_angle=$_angle-360;
	if ($_angle<0) $_angle=360+$_angle;
	$('#edit_r').val(360-$_angle);
	$( "#slider_r" ).slider('value',$_angle);
	rotate($_angle);

}

function get_max_z() {
	$("#c_v_zoom .el_el").each(function() {
		var index_current = parseInt($(this).css("zIndex"), 10);
		if(index_current > max_z) {
			max_z = index_current;
		}
	});
}
function drag_me_rev() {
	var last_id_rotate=$("#c_v_zoom>.rotate-btn");
	last_id_rotate.appendTo("#"+last_id+" .el_el")
	var id=$("#"+last_id);
	id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
}
function drag_me() {
	var id=$("#"+last_id+' .el_el');
	var $_top=id.css("top").replace(/px/,"");
	var $_left=id.css("left").replace(/px/,"");
	var $_width=id.css("width").replace(/px/,"");
	get_max_z();
	$('#'+last_id+' .rotate-btn').appendTo('#c_v_zoom').css('left','auto').css('z-index',(max_z+1)).css('top',($_top*1-21)+'px').css('left',($_left*1+$_width*1+1)+'px').draggable({drag:function(){
		drag_rotate();
	},
	revert:function(){
		drag_me_rev();
	},
	start:function(){
		var id=$("#"+last_id);
		last_angle=id.find(".el_r").val();
		last_h=id.find(".el_el").css('height').replace(/px/,"");
		last_w=id.find(".el_el").css('width').replace(/px/,"");
		add_log('el_rotate',last_id+'|'+last_angle);

	}
	});
}
function resize_to(percent){
	var id=$("#"+last_id+" .el_el");
	//alert(1+(percent/100));
	//alert(parseInt(id.css("width").replace(/px/,""))*(1+(percent/100)));
	last_h=id.css("height").replace(/px/,"");
	last_w=id.css("width").replace(/px/,"");
	var p_percent=last_h/last_w;
	var new_r_w=(percent*500)/100;
	var new_r_h=((percent*500)/100)*p_percent;
	if (new_r_w<10 && new_r_h<10) {
		new_r_w=10;
		new_r_h=10;
	}
	id.css("width",new_r_w);
	id.css("height",new_r_h);
	//resize(id);

	id.find("svg image").attr('height',new_r_h);
	id.find("svg image").attr('width',new_r_w);
	var $_angle=id.parent().find(".el_r").val();
	transalate($_angle,true);


}
function resize(id){

	id.parent().find(".el_w").val(id.css("width").replace(/px/,""));
	id.parent().find(".el_h").val(id.css("height").replace(/px/,""));

	var w=id.css("width").replace(/px/,"");
	var h=id.css("height").replace(/px/,"");

	var percent_h=h/last_h;
	var percent_w=w/last_w;

	new_h=percent_h*(id.find("svg image").attr('height')*1);
	new_w=percent_w*(id.find("svg image").attr('width')*1);



	set_font_size(Math.round(new_h));


	id.find("svg image").attr('height',new_h);
	id.find("svg image").attr('width',new_w);
	var $_angle=id.parent().find(".el_r").val();
	transalate($_angle);
	last_h=h;
	last_w=w;
}
function el_resize_to($_id,$_w,$_h,$_top,$_left) {
	var id=$("#"+$_id+">div");
	el_move_to($_id,$_top,$_left)
	new_r_w=$_w;
	new_r_h=$_h;
	id.css("width",new_r_w+"px");
	id.css("height",new_r_h+"px");
	id.find("svg image").attr('height',new_r_h);
	id.find("svg image").attr('width',new_r_w);
	var $_angle=id.parent().find(".el_r").val();
	transalate($_angle);
}
function load_editable($_id){
	var id=$( $_id );
	id.find(".rotate-btn").css('left','auto').css('right','-21px').css('top','-21px');
	id.find(".close-btn,.move-btn,.rotate-btn").show();
	id.resizable({
		handles: "n, e, s, w,nw",
		aspectRatio: true,
		start: function(event, ui) {
			var id=$(this);
			last_w=id.css("width").replace(/px/,"");
			last_h=id.css("height").replace(/px/,"");

			var $_top=id.css("top").replace(/px/,"");
			var $_left=id.css("left").replace(/px/,"")

			var last_id=id.parent().attr('id');
			add_log('el_resize',last_id+'|'+last_w+'|'+last_h+'|'+$_top+'|'+$_left);
		},
		resize: function(event, ui) {
			var id=$(this);
			resize(id);
		},
		stop: function(event, ui) {


		}
	}).draggable({
		cancel:".close-btn,.rotate-btn",
		start: function(event, ui) {
			var $_top=id.css("top").replace(/px/,"");
			var $_left=id.css("left").replace(/px/,"");
			var last_id=id.parent().attr('id');
			add_log('el_move',last_id+'|'+$_top+'|'+$_left);
		},
		create: function(event, ui) {
			var id=$(this);
			id.css('z-index',id.find(".el_el").css('z-index'));
			id.css('overflow','');
			id.append('<a class="close-btn spritep "  onclick="el_delete(last_id);" title="inchide"></a><a class="rotate-btn spritep"  onmousemove="drag_me()"  title="roteste"></a><a class="move-btn spritep " title="muta"></a>');
		},
		drag: function(event, ui) {
			var id=$(this);
			id.parent().find(".el_t").val(id.css("top").replace(/px/,""));
			id.parent().find(".el_l").val(id.css("left").replace(/px/,""));
		},
		stop: function(event, ui) {


			/*var id=$(this);
			alert("sadsad");
			id.find(".el_t").val(id.css("top").replace(/px/,""));
			id.find(".el_l").val(id.css("left").replace(/px/,""));
			*/
		}
	});
}
function load_editor(){
	$("#zoom_prev_move").draggable({ containment: "parent",


	drag: function( event, ui ) {

		var top=$("#zoom_prev_move").css("top").replace(/px/,"");
		var left=$("#zoom_prev_move").css("left").replace(/px/,"");
		top=(top*-1)*10;
		left=(left*-1)*10;
		$("#c_v_zoom_v2").css("top",top);
		$("#c_v_zoom_v2").css("left",left);
	}

	});
	$( ".img" ).click(function(){
		select_tab(4);
		var id=$(this).parent();
		/*var str='<img src="http://'+window.location.host+'/rotate?i='+id.find(".el_n").val()+'" style="width:160px;">';
		if($("#layer_edit_img").html()!=str) $("#layer_edit_img").html(str);*/
		$("#layer_edit_txt").html('');

		last_id=id.attr('id');
		if (id.data("b")!=1){
			last_el=0;
			$(".close-btn,.move-btn,.rotate-btn,#c_v_zoom .ui-resizable-n,#c_v_zoom .ui-resizable-e,#c_v_zoom .ui-resizable-s,#c_v_zoom .ui-resizable-w").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");

			var el_el_o=id.find('.el_o').val(); $("#transparency_in").val(el_el_o); $( "#slider_ts" ).slider('value',el_el_o);
			var el_el_r=360-id.find('.el_r').val()*1; $("#edit_r").val(el_el_r); $( "#slider_r" ).slider('value',el_el_r);
			load_editable(this);
		} else {
			$(".close-btn,.move-btn,.rotate-btn").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");
			$(this).append('<div class="ui-resizable-handle ui-resizable-n" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-e" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-s" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-w" style="cursor:auto;">');
		}
	});
	$( ".el_txt" ).mouseup(function(){

	});
	$( ".txt" ).click(function(){
		select_tab(3);
		diacritics();
		var id=$(this).parent();
		//$("#add_new_text").html('Modifica textul');
		$("#layer_edit_img").html('');
		$("#layer_edit_txt").val(id.find(".el_n").val());
		$("#font_size").val(id.parent().find(".el_h").val());
		var color=id.find(".el_c").val();
		var outline=id.find(".el_oc").val();

		//alert(id.find(".el_f").val());

		set_font(id.find(".el_f").val());



		//		console.log(color);

		$('input[name=\"font_color\"]').val('#'+color);
		$('li#font-color div.selected-color div').css('backgroundColor','#'+color);
		$('input[name=\"aoutline_color\"]').val('#'+outline);
		$('li#outline-color div.selected-color div').css('backgroundColor','#'+outline);





		//		alert(color);
		/*var id=$(this);
		last_id=this.id;
		last_z=this.style.zIndex;*/
		if (id.data("b")!=1){

			last_el=1;
			last_id=id.attr('id');
			$(".close-btn,.move-btn,.rotate-btn,#c_v_zoom .ui-resizable-n,#c_v_zoom .ui-resizable-e,#c_v_zoom .ui-resizable-s,#c_v_zoom .ui-resizable-w").remove();
			$("#c_v .el_el").draggable("destroy").resizable("destroy");

			//	var el_el_o=id.find('.el_o').val(); $("#transparency_in").val(el_el_o); $( "#slider_ts" ).slider('value',el_el_o);
			var el_el_r=360-id.find('.el_r').val()*1; $("#edit_r").val(el_el_r); $( "#slider_r" ).slider('value',el_el_r);
			load_editable(this);
		}
	});
	$( ".bg_all_dsl" ).click(function(){
		last_id=null;
		$(".close-btn,.move-btn,.rotate-btn,#c_v_zoom .ui-resizable-n,#c_v_zoom .ui-resizable-e,#c_v_zoom .ui-resizable-s,#c_v_zoom .ui-resizable-w").remove();
		$("#c_v .el_el").draggable("destroy").resizable("destroy");
	});
}
function go_front($id,log) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var id=$("#"+$id+">div");
		var new_z=0;
		if (id.css('z-index')=='auto') new_z=0; else new_z=id.css('z-index')*1+1;
		id.css('z-index',new_z);
		id.parent().css('z-index',new_z);
		id.parent().find(".el_z").val(new_z);
		if(log==null) add_log('go_front',$id);
	}
}
function go_back($id,log) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var id=$("#"+$id+">div");
		var new_z=0;
		if (id.css('z-index')=='auto') new_z=0; else {
			new_z=id.css('z-index')*1-1;
			if(new_z<0) new_z=0;
		}
		id.css('z-index',new_z);
		id.parent().css('z-index',new_z);
		id.parent().find(".el_z").val(new_z);
		if(log==null)  add_log('go_back',$id);
	}
}
function el_rotate($_angle){
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		if ($_angle>=360) $_angle=$_angle-360;
		if ($_angle<0) $_angle=360+$_angle;
		$( "#edit_r" ).val(360-$_angle);
		$( "#slider_r" ).slider('value',360-$_angle);
		rotate($_angle);
	}
}
function rotate_to($_angle) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {

		$_angle=360-$_angle*1;
		if ($_angle>=360) $_angle=$_angle-360;
		if ($_angle>=360*2) $_angle=360;
		if ($_angle<0) $_angle=360+$_angle;
		//$( "#edit_r" ).val(360-$_angle);
		rotate($_angle);
		$("#"+last_id+" .el_r").val($_angle);
	}
}
function select_template($_id){
	view_2d();
	$("#view_2d").load('http://'+window.location.host+'/3d?action_2=select_template&id='+$_id, function() {
		load_editor();
	});
}
document.load_edit_or= function() {
	load_editor();
}

function new_img_text(){


	text_nr++;
	get_max_z();
	var $_value=$('#layer_edit_txt').val();
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=max_z+1;
	var $_el_r=0;
	var $_el_o=0;
	var $_el_c=($('input[name="font_color"]').length>0?$('input[name="font_color"]').val().replace('#',''):'000000');
	var $_el_os=($('input[name="outline"]').length>0?$('input[name="outline"]').val():'0');
	var $_el_fl=0;
	var $_el_flip=0;
	var $_el_flop=0;
	var $_el_oc=($('input[name="aoutline_color"]').length>0?$('input[name="aoutline_color"]').val().replace('#',''):'000000');
	var $_el_h=($('input[name="font_size"]').length>0?$('input[name="font_size"]').val():'40');
	var $_el_f=($('input[name="font_family"]').length>0?$('input[name="font_family"]').val():'Arial');

	//alert($_el_f);

	var dv=document.createElement('div');
	dv.id='el_txt_rand_'+text_nr;
	var text=$_value;
	var text_br=text.replace(/\n\r?/g, '|br|');
	var text_br_arr=text_br.split('|br|');
	var max_l=0;
	for (var i = 0; i < text_br_arr.length; i++) {
		if (text_br_arr[i].length>max_l) max_l=text_br_arr[i].length;
	}
	$_el_h=$_el_h*i;
	//alert($_el_h);
	//var $_el_w=$_value.length*($_el_h)*0.4;
	var $_el_w=max_l*((($_el_h)*0.4)/i);


	var txt='<div class="txt el_el" id="div_'+dv.id+'" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;height:'+$_el_h+'px; width:'+$_el_w+'px;">'

	+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>'
	+'<input type="hidden" class="el_o" name="txt_o[]" value="'+$_el_o+'"/>'
	+'<input type="hidden" class="el_c" name="txt_c[]" value="'+$_el_c+'"/>'
	+'<input type="hidden" class="el_f" name="txt_f[]" value="'+$_el_f+'"/>'
	+'<input type="hidden" class="el_os" name="txt_os[]" value="'+$_el_os+'"/>'
	+'<input type="hidden" class="el_oc" name="txt_oc[]" value="'+$_el_oc+'"/>'
	+'<input type="hidden" class="el_fl" name="txt_fl[]" value="'+$_el_fl+'"/>'
	+'<input type="hidden" class="el_flip" name="txt_flip[]" value="'+$_el_flip+'"/>'
	+'<input type="hidden" class="el_flop" name="txt_flop[]" value="'+$_el_flop+'"/>'
	+'<input type="hidden" class="el_a" name="txt_a[]" value="0"/>'
	+'<input type="hidden" class="el_ibu" name="txt_ibu[]" value="000"/>'
	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);

	var svgNS="http://www.w3.org/2000/svg";
	var xlinkNS="http://www.w3.org/1999/xlink";
	var svg=document.createElementNS(svgNS,"svg");

	svg.setAttribute("width","100%");
	svg.setAttribute("height","100%");

	var svg_g=document.createElementNS(svgNS,"g");
	svg_g.transform="translate(0,0)";
	svg.appendChild(svg_g);
	var svg_img=document.createElementNS(svgNS,"image");

	svg_img.setAttribute("width",$_el_w);
	svg_img.setAttribute("height",$_el_h);

	svg_img.setAttributeNS(xlinkNS,"href",'http://'+window.location.host+'/3d?action=text&t='+text_br+'&c='+$_el_c+'&oc='+$_el_oc+'&os='+$_el_os+'&f='+$_el_f);
	svg_g.appendChild(svg_img);

	setTimeout(function(){
		var div=document.getElementById('div_'+dv.id);
		div.appendChild(svg);
	},300);


	/*

	+'<svg xmlns="http://www.w3.org/2000/svg" version="1.1"  width="100%" height="100%">'
	+'<g  transform="translate(0,0)"  >'
	+'<image preserveAspectRatio="none"   width="100%" height="100%"  xlink:href="3d?action=text&t='+$_value+'&size='+$_el_h+'" transform="rotate(0)" />'
	+'</g>'
	+'</svg>'*/


	dv.className='el_txt';

	dv.innerHTML=txt;
	load_editor();


	//$('#'+new_id+" .img").trigger('click');
	$('#'+dv.id+" .txt").trigger('click');


	var nr_var_txt_prices=var_txt_prices.length;
	//nr_var_txt_prices++;
	//alert(dv.id);
	var_txt_prices[nr_var_txt_prices]=dv.id+'|'+text;
	calcul_text();

}
function reload_text(height_edit){
	//alert(last_h_text);
	var text=$("#layer_edit_txt").val();
	var hex=$("#"+last_id+" .el_c").val();
	var hex_o=$("#"+last_id+" .el_oc").val();
	var $_angle=$("#"+last_id+" .el_r").val();
	var os=$("#"+last_id+" .el_os").val();
	var f=$("#"+last_id+" .el_f").val();

	var $_flip=$("#"+last_id+" .el_flip").val();
	var $_flop=$("#"+last_id+" .el_flop").val();
	var $_align=$("#"+last_id+" .el_a").val();
	var $_ibu=$("#"+last_id+" .el_ibu").val();

	var text_br=text.replace(/\n\r?/g, '|br|');
	$("#"+last_id+" image").attr("href",'http://'+window.location.host+'/3d?action=text&t='+text_br+'&size=100&c='+hex+'&oc='+hex_o+'&os='+os+'&f='+f+'&fi='+$_flip+'&fo='+$_flop+'&a='+$_align+'&ibu='+$_ibu);
	var id=$("#"+last_id+">div");

	var text_br_arr=text_br.split('|br|');
	var max_l=0;
	for (var i = 0; i < text_br_arr.length; i++) {
		if (text_br_arr[i].length>max_l) max_l=text_br_arr[i].length;
	}

	if (height_edit==true) {
		var h=i*last_h_text;
		var w=max_l*((h*0.4)/i);
		id.parent().find(".el_w").val(w);
		id.parent().find(".el_h").val(h);
		$("#"+last_id+" image").attr("width",w);
		$("#"+last_id+" image").attr("height",h);


		var $r= $_angle*(Math.PI/180);
		var $_el_el_h=h;
		var $_el_el_w=w;
		var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
		var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;

		w=$_w;
		h=$_h;

		id.css("height",h);
		id.css("width",w);

		transalate($_angle);
	}

}
function edit_text($_val){
	if ($("#"+last_id+" .txt").length >0){

		var id=$("#"+last_id);
		var text=id.find(".el_n").val();
		var text_br=text.replace(/\n\r?/g, '|br|');
		var text_br_arr=text_br.split('|br|');
		last_h_text=id.find(".el_h").val()/text_br_arr.length;
		id.find(".el_n").val($_val);
		reload_text(true);
	}
}
function new_text(){
	var $_value='Text';
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=9000;
	var $_el_r=0;
	var $_el_w=0;
	var $_el_h=14;
	var $_el_c='000000';
	text_nr++;
	var dv=document.createElement('div');
	var txt='<div class="div_txt" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;" >'
	+'<div class="txt_n txt el_el" style=" z-index:'+$_el_z+'; font-family:arial; letter-spacing:'+$_el_w+'px; font-size:'+$_el_h+'px; ">'+$_value+'</div>'
	+'<input type="hidden" class="el_t" name="txt_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="txt_n[]" value="'+$_value+'"/>'
	+'<input type="hidden" class="el_l" name="txt_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="txt_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="txt_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="txt_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="txt_r[]" value="'+$_el_r+'"/>'
	+'<input type="hidden" class="el_c" name="txt_c[]" value="'+$_el_c+'"/>';
	+'</div>';
	document.getElementById('c_v_zoom').appendChild(dv);
	dv.className='el_txt';
	dv.id='el_txt_rand_'+text_nr;
	dv.innerHTML=txt;
	parent.document.load_edit_or();
}
function el_move_to($_id,$_top,$_left) {
	$("#"+$_id+">div").css('left',$_left+'px');
	$("#"+$_id+">div").css('top',$_top+'px');
	$("#"+$_id+" .el_l").val($_left);
	$("#"+$_id+" .el_t").val($_top);
}
function el_align($_align) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$_top_1=$("#"+last_id+">div").css('top');
		$_left_1=$("#"+last_id+">div").css('left');
		if ($_align=='right' || $_align=='center' ) var $_left=$("#c_v").width()-($("#"+last_id+">div").width());
		if ($_align=='bottom' || $_align=='middle' ) var $_top=$("#c_v").height()-($("#"+last_id+">div").height());
		if ($_align=='left') {
			$("#"+last_id+">div").css('left',0);
			$("#"+last_id+" .el_l").val(0);
		}
		if ($_align=='right') {
			$("#"+last_id+">div").css('left',$_left);
			$("#"+last_id+" .el_l").val($_left);
		}
		if ($_align=='center') {
			$("#"+last_id+">div").css('left',$_left/2);
			$("#"+last_id+" .el_l").val($_left/2);
		}
		if ($_align=='top') {
			$("#"+last_id+">div").css('top',0);
			$("#"+last_id+" .el_t").val(0);
		}
		if ($_align=='bottom') {
			$("#"+last_id+">div").css('top',$_top);
			$("#"+last_id+" .el_t").val($_top);
		}
		if ($_align=='middle') {
			$("#"+last_id+">div").css('top',$_top/2);
			$("#"+last_id+" .el_t").val($_top/2);
		}
		add_log('el_move',last_id+'|'+$_top_1+'|'+$_left_1);

	}
}
function font_size($_size) {

	var id=$("#"+last_id+" .el_el");
	var $_angle=$("#"+last_id+" .el_r").val();
	var h=i*last_h_text;


	var h=$("#"+last_id+" image").attr("height");
	var w=$("#"+last_id+" image").attr("width");

	var percent=w/h;

	//h=$_size*1.25;
	h=$_size;
	w=h*percent;
	id.parent().find(".el_w").val(w);
	id.parent().find(".el_h").val(h);


	$("#"+last_id+" image").attr("width",w);
	$("#"+last_id+" image").attr("height",h);


	var $r= $_angle*(Math.PI/180);
	var $_el_el_h=h;
	var $_el_el_w=w;
	var $_w=Math.abs(Math.cos($r))*$_el_el_w+Math.abs(Math.sin($r))*$_el_el_h;
	var $_h=Math.abs(Math.sin($r))*$_el_el_w+Math.abs(Math.cos($r))*$_el_el_h;

	w=$_w;
	h=$_h;

	id.css("height",h);
	id.css("width",w);

	transalate($_angle);



	//resize_to($_size);

	/*
	var id=$("#"+last_id+" .el_el");

	last_h=id.css("height").replace(/px/,"");
	last_w=id.css("width").replace(/px/,"");

	var p_percent=last_h/last_w;

	var new_r_w=(percent*500)/100;
	var new_r_h=((percent*500)/100)*p_percent;



	if (new_r_w<10 && new_r_h<10) {
	new_r_w=10;
	new_r_h=10;
	}
	id.css("width",new_r_w);
	id.css("height",new_r_h);
	//resize(id);

	id.find("svg image").attr('height',new_r_h);
	id.find("svg image").attr('width',new_r_w);
	var $_angle=id.parent().find(".el_r").val();
	transalate($_angle,true);

	*/

	//$("#"+last_id+" .txt_n").css('font-size',$_size+"px");
	//$("#"+last_id+" .el_h").val($_size);
}
function letter_s($_space) {
	$("#"+last_id+" .txt_n").css('letter-spacing',$_space+"px");
	$("#"+last_id+" .el_w").val($_space);
}
function view_3d(view) {
	$_last_view_3d=view;
	$('#viz').html('<div class="loading">Se incarca ...</div>');
	$('#viz').load('http://'+window.location.host+'/3d?id_view='+view+'&'+$('#my_form').serialize()+'&'+$('#f_variations').serialize());
	$('#view_2d').hide();
	$('#viz').show();
}
function view_3d_b(id){
	if ($_last_view_3d>0) view_3d($_last_view_3d);
	$(id).parent().parent().find('.selected').removeClass('selected');
	$(id).parent().addClass('selected');
	$('.b_edit').hide();
	$('.b_view').show();
}
function view_2d_b(id){
	if ($_last_view_2d>0) view_2d($_last_view_2d);
	$(id).parent().parent().find('.selected').removeClass('selected');
	$(id).parent().addClass('selected');
	$('.b_view').hide();
	$('.b_edit').show();
}
function view_2d(nr){
	$_last_view_2d=nr;
	$('#viz').hide();
	$('#view_2d').show();
}
function  do_el_delete($id) {
	$('#'+$id).remove();
	last_id='';
}
function  el_delete($id) {
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		if ($id=='') {
			alert('Niciun element selectat');
		} else {
			if (confirm('Sunteti sigur ca dorioti sa stergeti elementul?')) {
				do_el_delete($id);


				var $_var='';
				var $_id_id_id='';
				if (last_el==0){
					var new_var_img_prices= '';
					for(var j=0;j<var_img_prices.length;j++) {
						new_var_img_prices= var_img_prices[j].split("|");
						$_var=new_var_img_prices[1];
						$_id_id_id=new_var_img_prices[0];
						if ($_id_id_id==$id) {
							var_img_prices.splice(j,1); calcul_img();
						}
					}
				} else {
					var new_var_txt_prices= '';
					for(var j=0;j<var_txt_prices.length;j++) {
						new_var_txt_prices= var_txt_prices[j].split("|");
						$_var=new_var_txt_prices[1];
						$_id_id_id=new_var_txt_prices[0];
						if ($_id_id_id==$id) {

							var_txt_prices.splice(j,1); calcul_text();
						}
					}
				}
			} else return false;


		}
	}
}
function  el_clear() {
	if (confirm('Sunteti sigur ca dorioti sa stergeti toate elementele?'))	$("#c_v_zoom").html('<div class="bg_all_dsl"  style="width:100%; height:100%; position:absolute;top:0; left:0;"></div>');
	else return false;
}
var SVG=function(h,w){
	var NS="http://www.w3.org/2000/svg";
	var svg=document.createElementNS(NS,"svg");
	svg.width=w;
	svg.height=h;
	return svg;
}
function  blocheaza($id) {
	var id=$("#"+$id);
	if(id.data("b")==1) {
		id.data("b",0);
		load_editable(id.find(".el_el"));
	} else {
		id.data("b",1);
		$(".close-btn,.move-btn,.rotate-btn").remove();
		$("#c_v .el_el").draggable("destroy").resizable("destroy");
		id.find(".el_el").append('<div class="ui-resizable-handle ui-resizable-n" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-e" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-s" style="cursor:auto;"></div><div class="ui-resizable-handle ui-resizable-w" style="cursor:auto;">');
	}
	add_log('blocheaza',$id);
}
function zoom_out() {
	if(l_zoom>1) {
		var zoom=(1-(1/l_zoom));
		l_zoom--;
		do_zoom(zoom);
		//alert(l_zoom);
		if(l_zoom==1) {
			//$("#c_v_zoom").css('left','0');
			//$("#c_v_zoom").css('top','0');
			//$("#c_v_zoom").draggable("destroy");
		}
	}
	return false;
}
function zoom_in() {
	$("#zoom_prev_img").attr("src",'http://'+window.location.host+'/3d?actions=dw_prev&'+$('#my_form').serialize());


	if(l_zoom<5) {
		var zoom=(1+1/l_zoom);
		l_zoom++;
		do_zoom(zoom);
	}
	return false;
}
function zoom_1x() {
	var zoom=1/l_zoom;
	l_zoom=1;
	do_zoom(zoom);
	//$("#c_v_zoom").css('left','0');
	//$("#c_v_zoom").css('top','0');
	//$("#c_v_zoom").draggable("destroy");
}
function do_zoom(zoom) {
	
	$(".close-btn,.move-btn,.rotate-btn").remove();
	$("#c_v .el_el").resizable("destroy").draggable("destroy");

	var id=$("#c_v");
	var id_p=$("#zoom_prev_img");
	id.css("width",id.css("width").replace(/px/,"")*zoom);
	id.css("height",id.css("height").replace(/px/,"")*zoom);
	id.css("marginTop",id.css("marginTop").replace(/px/,"")*zoom);
	id.css("marginLeft",id.css("marginLeft").replace(/px/,"")*zoom);

	//alert(l_zoom);

/*	if (l_zoom==1) {
		var $_style=$("#style_zoom_1").val();
		$("#c_v").attr("style",$_style);
		id.css("left",0).css("top",0);
		$("#zoom_prev_move").css("left",0).css("top",0);
	} else {
		$("#c_v").attr("style","height:500px;width:500px;");
	}

*/



$("#zoom_prev_move").css("left",0).css("top",0).css("height",100/l_zoom).css("width",100/l_zoom);

	$('#c_v .el_el').each(function(i) {
		var id=$(".el_el:eq("+i+")");
		id.css("top",id.css("top").replace(/px/,"")*zoom);
		id.css("left",id.css("left").replace(/px/,"")*zoom);
		id.css("width",id.css("width").replace(/px/,"")*zoom);
		id.css("height",id.css("height").replace(/px/,"")*zoom);
		var svg=id.find("svg image");
		var g=id.find("svg g");
		var translate=g.attr('transform').replace("translate(","").replace(")","").split(",");
		g.attr("transform","translate("+(translate[0]*1)*zoom+","+(translate[1]*1)*zoom+")");
		svg.attr('height',svg.attr('height')*zoom);
		svg.attr('width',svg.attr('width')*zoom);
	});
}
function select_tab(nr) {
	$(".tab").hide();
	$("#tab-"+nr).show();
	$("#tab-nav a").removeClass("selected");
	$(".tab-"+nr+" a").addClass("selected");
}
function  transparency_to($_tr) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$('#transparency_in').val($_tr);
		transparency($_tr);
	}
}
function  transparency($_tr) {
	if ($("#"+last_id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		$("#"+last_id+" .el_o").val($_tr);
		$_tr=(100-$_tr*1)/100;
		$("#"+last_id+" g").attr("opacity",$_tr);
	}
}
function  add_log($_name,$_params) {
	actions_i++
	actions[actions_i]=$_name;
	actions_p[actions_i]=$_params;
}
function  add_image($_img,$w,$h) {



	img_nr++;
	get_max_z();
	var $_el_t=20;
	var $_el_l=20;
	var $_el_z=max_z+1;
	var $_el_o=0;
	var $_el_r=0;
	var $_el_fl=0;
	var $_el_flip=0;
	var $_el_flop=0;
	var $_el_w=$w;
	var $_el_h=$h;
	var dv=document.createElement('div');
	dv.id='el_img_rand_'+img_nr;
	var txt='<div class="img el_el" id="div_'+dv.id+'" style="position:absolute; z-index:'+$_el_z+'; left:'+$_el_l+'px; top:'+$_el_t+'px;height:'+$_el_h+'px; width:'+$_el_w+'px;">'
	+'</div>'
	+'<input type="hidden" class="el_t" name="img_t[]" value="'+$_el_t+'"/>'
	+'<input type="hidden" class="el_n" name="img_n[]" value="'+$_img+'"/>'
	+'<input type="hidden" class="el_l" name="img_l[]" value="'+$_el_l+'"/>'
	+'<input type="hidden" class="el_w" name="img_w[]" value="'+$_el_w+'"/>'
	+'<input type="hidden" class="el_h" name="img_h[]" value="'+$_el_h+'"/>'
	+'<input type="hidden" class="el_z" name="img_z[]" value="'+$_el_z+'"/>'
	+'<input type="hidden" class="el_r" name="img_r[]" value="'+$_el_r+'"/>'
	+'<input type="hidden" class="el_o" name="img_o[]" value="'+$_el_o+'"/>'
	+'<input type="hidden" class="el_flip" name="img_flip[]" value="'+$_el_flip+'"/>'
	+'<input type="hidden" class="el_flop" name="img_flop[]" value="'+$_el_flop+'"/>'
	+'<input type="hidden" class="el_fl" name="img_fl[]" value="'+$_el_fl+'"/>';
	+'</div>';

	//$_img='rotate?i='+$_img;

	document.getElementById('c_v_zoom').appendChild(dv);
	var svgNS="http://www.w3.org/2000/svg";
	var xlinkNS="http://www.w3.org/1999/xlink";
	var svg=document.createElementNS(svgNS,"svg");

	svg.setAttribute("width","100%");
	svg.setAttribute("height","100%");




	var svg_f=document.createElementNS(svgNS,"filter");
	svg_f.id="sepia";
	svg.appendChild(svg_f);

	var svg_fm=document.createElementNS(svgNS,"feColorMatrix");
	//svg_fm.setAttribute("values","0.14 0.45 0.05 0 0 0.12 0.39 0.04 0 0 0.08 0.28 0.03 0 0 0 0 0 1 0");
	svg_fm.setAttribute("values","0.393 0.769 0.189 0 0 0.349 0.686 0.168 0 0 0.272 0.534 0.131 0 0 0 0 0 1 0");
	svg_f.appendChild(svg_fm);


	svg_f=document.createElementNS(svgNS,"filter");
	svg_f.id="grayscale";
	svg.appendChild(svg_f);

	svg_fm=document.createElementNS(svgNS,"feColorMatrix");
	svg_fm.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0");
	svg_f.appendChild(svg_fm);



	var svg_g=document.createElementNS(svgNS,"g");
	svg_g.setAttribute("transform","translate(0,0)");
	svg_g.setAttribute("opacity","1");
	svg.appendChild(svg_g);

	var svg_img=document.createElementNS(svgNS,"image");
	svg_img.setAttribute("width",$_el_w);
	svg_img.setAttribute("height",$_el_h);
	svg_img.setAttributeNS(xlinkNS,"href",'http://static.jobselect.ro/i/imagini/'+$_img);
	svg_g.appendChild(svg_img);

	setTimeout(function(){
		var div=document.getElementById('div_'+dv.id);
		div.appendChild(svg);
	},300);
	dv.className='el_img';
	dv.innerHTML=txt;
	load_editor();

	add_log('add_image','div_'+dv.id);



	var nr_var_txt_prices=var_img_prices.length;
	//var_img_prices[nr_var_txt_prices]=dv.id+'|'+$_img.replace("rotate?i=","");
	var_img_prices[nr_var_txt_prices]=dv.id+'|'+$_img;
	calcul_img();

}
function  clone($id) {
	$id=last_id;
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var top=($('#'+$id+">div").css("top").replace(/px/,""))*1+30;
		var left=($('#'+$id+">div").css("left").replace(/px/,""))*1+30;
		var clone =$('#'+$id).clone();
		var new_id=clone.attr('id')+"_c";
		clone.attr('id',new_id);
		//clone.append(clone.find('.el_el'));
		//clone.find('.el_el').removeClass('ui-resizable');
		clone.find('.ui-resizable-handle').remove();
		clone.find(".el_el").css('top',top+'px').css('left',left+'px').css('position','absolute');
		$("#c_v_zoom").append(clone);
		$("#c_v .el_el").resizable("destroy").draggable("destroy");
		//$("#"+last_id+" .close-btn,#"+last_id+" .move-btn,#"+last_id).hide();
		$(".close-btn,.move-btn,.rotate-btn").remove();

		load_editor();

		$('#'+new_id+" .img").trigger('click');
		$('#'+new_id+" .txt").trigger('click');


		add_log('clone',new_id);
		return false;
		$( ".el_img" ).mouseup(function(){
			$(".edit_text").hide();
			$(".edit_img").show();
			var id=$(this);
			last_id=this.id;
			if (id.data("b")!=1){
				$("#layer_edit").html('<img src="'+this.src+'"  style="width:160px;" />');
				$("#c_v .el_el").resizable("destroy").draggable("destroy");
				load_editable(last_id);
				$("#edit_r").val(360-id.find('.el_r').val()*1);
			}
		});
	}
}

function max_z_act() {
	get_max_z(); $('#form_max_z').val(max_z);
	return true;
}
function text_color(hex) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_c").val(hex);
		reload_text();
	}
}

function align(al) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_a").val(al);
		reload_text();
	}
}
function select_font(font) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_f").val(font);
		reload_text();
	}
}
function outline_size(size) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_os").val(size);
		reload_text();
	} else {

	}
}
function outline_color(hex) {
	if ($("#"+last_id+" .txt").length >0){
		$("#"+last_id+" .el_oc").val(hex);
		reload_text();
	} else {

	}
}
function select_standard($id) {
	if ($id==null) {
		$id=last_id;
		var last_filter=$("#"+$id+" .el_fl").val();
		if (last_filter==0)	add_log("standard",$id);
		if (last_filter==1)	add_log("sepia",$id);
		if (last_filter==2)	add_log("grayscale",$id);
	}
	$("#"+$id+" g").attr("filter","");
	$("#"+$id+" .el_fl").val("0");
}
function select_sepia($id) {
	if ($id==null) {
		$id=last_id;
		var last_filter=$("#"+$id+" .el_fl").val();
		if (last_filter==0)	add_log("standard",$id);
		if (last_filter==1)	add_log("sepia",$id);
		if (last_filter==2)	add_log("grayscale",$id);
	}
	$("#"+$id+" g").attr("filter","url(#sepia)");
	$("#"+$id+" .el_fl").val("1");
}
function select_grayscale($id) {
	if ($id==null) {
		$id=last_id;
		var last_filter=$("#"+$id+" .el_fl").val();
		if (last_filter==0)	add_log("standard",$id);
		if (last_filter==1)	add_log("sepia",$id);
		if (last_filter==2)	add_log("grayscale",$id);
	}
	$("#"+$id+" g").attr("filter","url(#grayscale)");
	$("#"+$id+" .el_fl").val("2");
}

function flip_ver() {
	$id=last_id;
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var $_angle=$("#"+last_id+" .el_r").val();
		var $_flop=$("#"+last_id+" .el_flip").val();
		var id=$("#"+last_id+" .el_flop");
		var $_img=$("#"+last_id+" .el_n").val();
		if (id.val()==0) id.val(1); else id.val(0);
		el_rotate($_angle*-1);
		if (last_el==0){
			$("#"+last_id+" svg image").attr("href",'http://'+window.location.host+'/rotate?i='+$_img+'&fo='+ id.val()+'&fi='+$_flop);
		} else {
			reload_text();
		}
	}
}
function flip_hor() {
	$id=last_id;
	if ($("#"+$id).data("b")==1) {
		werror("Elementul este blocat!");
	} else {
		var $_angle=$("#"+last_id+" .el_r").val();
		var id=$("#"+last_id+" .el_flip");
		var $_flop=$("#"+last_id+" .el_flop").val();
		var $_img=$("#"+last_id+" .el_n").val();
		if (id.val()==0) id.val(1); else id.val(0);
		el_rotate($_angle*-1);
		if (last_el==0){
			$("#"+last_id+" svg image").attr("href",'http://'+window.location.host+'/rotate?i='+$_img+'&fi='+ id.val()+'&fo='+$_flop);
		} else {
			reload_text();
		}

	}
}
function calcul($_id_v, $_id_o, $_var,$_id_o_d, $_o,  $_price) {
	var $_price_total=0;
	$_nr_var++;
	var $_nr_var_i=$_nr_var;

	for(var j=1;j<var_options.length;j++) {
		if(var_options[j][2]==$_id_v) {
			$_nr_var_i=j;
		}
	}
	var_options[$_nr_var_i]=new Array($_var,$_o,$_id_v,$_id_o,$_price);
	var $_html="";
	$_html+='		<table>                    ';
	$_html+='										<tr>                    ';
	$_html+='											<th class="col-1">                    ';
	$_html+='												Tip personalizare                    ';
	$_html+='											</th>                    ';
	$_html+='											<th col-2>                    ';
	$_html+='											</th>                    ';
	$_html+='											<th class="col-3 price">                    ';
	$_html+='												Pret (lei)                    ';
	$_html+='											</th>                    ';
	$_html+='										</tr>                    ';

	for(var j=1;j<var_options.length;j++) {

		$_var=var_options[j][0];
		$_o=var_options[j][1];
		$_price=var_options[j][4];

		$_html+='										<tr>                    ';
		$_html+='											<td>                    ';

		$_html+=$_var;

		$_html+='											</td>                    ';
		$_html+='											<td>                    ';
		$_html+='												<ul class="cf action-buttons">                    ';
		$_html+='													<li class="active">                    ';
		$_html+='														<a class="spritep" href="#" title="Activ"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="edit-button">                    ';
		$_html+='														<a class="spritep" onclick="select_tab(1);" href="#" title="Editeaza"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="delete">                    ';
		$_html+='														<a class="spritep" href="#" title="Sterge"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='												</ul>                    ';
		$_html+='											</td>                    ';
		$_html+='											<td class="price">                    ';
		$_html+='											'+$_price+'                    ';
		$_html+='										</td>                    ';
		$_html+='										</tr>                    ';
		$_price_total+=$_price;
	}
	$_price_total_calcul=$_price_total;

	$_html+='										<tr class="total border">                    ';
	$_html+='											<td colspan="2">                    ';
	$_html+='											<strong>Total produs</strong>                    ';
	$_html+='										</td>                    ';
	$_html+='										<td class="price">                    ';
	$_html+='											<span>                    ';
	$_html+='										'+$_price_total_calcul+'                   ';
	$_html+='									</span>                    ';
	$_html+='											</td>                    ';
	$_html+='										</tr>                    ';
	$_html+='									</table>                        ';
	$("#tab_prod_price").html($_html);
	calc_total();
}



function calcul_text() {
	var $_price_total=0;
	var $_html="";
	$_html+='		<table>                    ';
	$_html+='										<tr>                    ';
	$_html+='											<th class="col-1">                    ';
	$_html+='												Tip personalizare                    ';
	$_html+='											</th>                    ';
	$_html+='											<th col-2>                    ';
	$_html+='											</th>                    ';
	$_html+='											<th class="col-3 price">                    ';
	$_html+='												Pret (lei)                    ';
	$_html+='											</th>                    ';
	$_html+='										</tr>                    ';


	var new_var_txt_prices= '';
	var $_var='';
	var $_id_id_id='';

	for(var j=0;j<var_txt_prices.length;j++) {

		new_var_txt_prices= var_txt_prices[j].split("|");
		$_var=new_var_txt_prices[1];
		$_id_id_id=new_var_txt_prices[0];

		$_price=0;

		$_html+='										<tr>                    ';
		$_html+='											<td>                    ';
		$_html+='												'+$_var+'                    ';
		$_html+='											</td>                    ';
		$_html+='											<td>                    ';
		$_html+='												<ul class="cf action-buttons">                    ';
		$_html+='													<li class="active">                    ';
		$_html+='														<a class="spritep" href="#" title="Activ"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="edit-button">                    ';
		$_html+='														<a class="spritep" onclick="select_tab(3);" href="#" title="Editeaza"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="delete">                    ';
		$_html+='														<a class="spritep" onclick="el_delete(\''+$_id_id_id+'\');"  href="#" title="Sterge"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='												</ul>                    ';
		$_html+='											</td>                    ';
		$_html+='											<td class="price">                    ';
		$_html+='											'+$_price+'                    ';
		$_html+='										</td>                    ';
		$_html+='										</tr>                    ';

		$_price_total+=$_price;
	}
	$_price_total_txt=$_price_total;

	$_html+='										<tr class="total border">                    ';
	$_html+='											<td colspan="2">                    ';
	$_html+='											<strong>Total text</strong>                    ';
	$_html+='										</td>                    ';
	$_html+='										<td class="price">                    ';
	$_html+='											<span>                    ';
	$_html+='										'+$_price_total_txt+'                   ';
	$_html+='									</span>                    ';
	$_html+='											</td>                    ';
	$_html+='										</tr>                    ';
	$_html+='									</table>                        ';
	$("#tab_txt_price").html($_html);
	calc_total();
}



function calcul_img() {
	var $_price_total=0;
	var $_html="";
	$_html+='		<table>                    ';
	$_html+='										<tr>                    ';
	$_html+='											<th class="col-1">                    ';
	$_html+='												Tip personalizare                    ';
	$_html+='											</th>                    ';
	$_html+='											<th col-2>                    ';
	$_html+='											</th>                    ';
	$_html+='											<th class="col-3 price">                    ';
	$_html+='												Pret (lei)                    ';
	$_html+='											</th>                    ';
	$_html+='										</tr>                    ';

	var new_var_img_prices= '';
	var $_var='';
	var $_id_id_id='';

	for(var j=0;j<var_img_prices.length;j++) {

		new_var_img_prices= var_img_prices[j].split("|");
		$_var=new_var_img_prices[1];
		$_id_id_id=new_var_img_prices[0];

		$_price=0;

		$_html+='										<tr>                    ';
		$_html+='											<td>                    ';
		$_html+='												'+$_var+'                    ';
		$_html+='											</td>                    ';
		$_html+='											<td>                    ';
		$_html+='												<ul class="cf action-buttons">                    ';
		$_html+='													<li class="active">                    ';
		$_html+='														<a class="spritep" href="#" title="Activ"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="edit-button">                    ';
		$_html+='														<a class="spritep" onclick="select_tab(1);" href="#" title="Editeaza"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='													<li class="delete">                    ';
		$_html+='														<a class="spritep" onclick="el_delete(\''+$_id_id_id+'\');"  href="#" title="Sterge"></a>                    ';
		$_html+='													</li>                    ';
		$_html+='												</ul>                    ';
		$_html+='											</td>                    ';
		$_html+='											<td class="price">                    ';
		$_html+='											'+$_price+'                    ';
		$_html+='										</td>                    ';
		$_html+='										</tr>                    ';
		$_price_total+=$_price;
	}
	$_price_total_img=$_price_total;

	$_html+='										<tr class="total border">                    ';
	$_html+='											<td colspan="2">                    ';
	$_html+='											<strong>Total Imagini</strong>                    ';
	$_html+='										</td>                    ';
	$_html+='										<td class="price">                    ';
	$_html+='											<span>                    ';
	$_html+='										'+$_price_total_img+'                   ';
	$_html+='									</span>                    ';
	$_html+='											</td>                    ';
	$_html+='										</tr>                    ';
	$_html+='									</table>                        ';
	$("#tab_img_price").html($_html);
	$("#tab_img_price_2").html($_html);
	calc_total();
}
function calc_total() {
	var quantity=$("#quantity").val()*1;
	var total_final=($_price_total_img+$_price_total_calcul+$_price_total_txt+initial_price)*quantity;
	$("#price_total").html(total_final.toFixed(2));
	total_final=total_final-total_final*(final_discount/100);
	$("#price_total_final").html(total_final.toFixed(2));
	$("#price_final_left").html(total_final.toFixed(2));
}


function undo() {
	nr=actions_i;
	actions_i--;
	//alert(actions[nr]);
	if(actions[nr]=='el_transparency') {
		var action_p=actions_p[nr].split("|");
		last_id=action_p[0];
		transparency_to(action_p[1]);
	}
	if(actions[nr]=='el_rotate') {
		var action_p=actions_p[nr].split("|");
		last_id=action_p[0];
		rotate_to(action_p[1]);
	}
	if(actions[nr]=='blocheaza') blocheaza(actions_p[nr]);
	if(actions[nr]=='go_back') go_front(actions_p[nr],true);
	if(actions[nr]=='go_front') go_back(actions_p[nr],true);
	if(actions[nr]=='add_image') do_el_delete(actions_p[nr]);
	if(actions[nr]=='clone') do_el_delete(actions_p[nr]);
	//alert(actions[nr]);
	if(actions[nr]=='standard') select_standard(actions_p[nr]);
	if(actions[nr]=='grayscale') select_grayscale(actions_p[nr]);
	if(actions[nr]=='sepia') select_sepia(actions_p[nr]);

	if(actions[nr]=='el_resize') {
		var action_p=actions_p[nr].split("|");
		el_resize_to(action_p[0],action_p[1],action_p[2],action_p[3],action_p[4]);
	}
	if(actions[nr]=='el_move') {
		var action_p=actions_p[nr].split("|");
		el_move_to(action_p[0],action_p[1],action_p[2]);
	}
}
function echo(txt,i) {
	$('#meu_'+i).val(txt);
}


function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function set_font_size(number){
	$('section#font-size-container input#font_size').val(number);
	$('section#font-size-container p.selected').html('').text(number);
}

function set_font(family){
	$('section#font-container ul#fonts-list li a')
	.each(
	function()
	{
		var t = $(this);
		var f = t.data('name');
		var d = t.data('diacritics');
		if(
		f==family
		){
			t.parent().parent().parent().parent().children('p.selected').html('').text(f).data('diacritics',d);
			t.parent().parent().parent().parent().children('input').val(f);
			if(
			d==1
			){
				if(
				$('div#special-characters-container >a:not(.has-special)')
				){
					$('div#special-characters-container >a').addClass('has-special')
				}
			}else{
				if(
				$('div#special-characters-container >a').hasClass('has-special')
				){
					$('div#special-characters-container >a').removeClass('has-special')
				}
			}
		}
	}
	)
}

function diacritics(){

}

