<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
header('Content-Type: text/html;charset=UTF-8');
?>

<head>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=utf-8">
	<title>Your browser is too old to view this site</title>
	<style>
		html {
			background-color: #293d46;
		}
		body {
			background-color: #293d46;
			height: 100%;
		}

		h1 {
			width:700px;
			margin:auto;
			padding-top: 80px;
			font-size: 20px;
			line-height:22px;
			color: #fff;
			text-align: center;
		}
		ul {
			width:700px;
			position:relative;
			margin:30px auto auto auto;
		}
		ul li {
			float: left;
			display: inline;
			width: 175px;
			padding: 10px 0px;
			text-align: center;
			line-height: 60px;
		}
		ul li a {
			border-bottom: 1px dotted #333;
			font-size: 13px;
			color: #bbb;
		}
		ul li a:hover {
			border-bottom-style: solid;
			color: #fc0;
		}
	</style>
</head>
<body>
	<h1>Your browser is too old to view this site. Please upgrade to a newer browser:</h1>
	<ul>
		<li>
			<img src="<?php echo _static?>images/ie6-7/firefox.png" alt="" /><br />
			<a href="http://www.mozilla.com" rel="blank">Mozilla Firefox</a>
		</li>
		<li>
			<img src="<?php echo _static?>images/ie6-7/chrome.png" alt="" /><br />
			<a href="http://chrome.google.com" rel="blank">Google Chrome</a>
		</li>
		<li>
			<img src="<?php echo _static?>images/ie6-7/ie.png" alt="" /><br />
			<a href="http://www.microsoft.com" rel="blank">Internet Explorer 8 or 9</a>
		</li>
		<li>
			<img src="<?php echo _static?>images/ie6-7/opera.png" alt="" /><br />
			<a href="http://www.opera.com/" rel="blank">Opera</a>
		</li>
	</ul>
</body>
</html>