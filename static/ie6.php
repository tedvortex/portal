<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?

if (!preg_match('/(?i)MSIE [1-6]/',$_SERVER['HTTP_USER_AGENT']))
	header ('Location: '.BASEHREF);

header('Content-Type: text/html;charset=UTF-8');

$website_name=preg_replace('/(http:|\/)/','',BASEHREF);

if(preg_match('/(?i)msie [1-6]/',$_SERVER['HTTP_USER_AGENT']))
	$browser_type='Internet Explorer 6.0';?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ro">
<head>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=utf-8">
	<title>Pentru a putea vizualiza acest site aveti nevoie de un browser modern</title>
	<style>
	html,body{height:100%;}
	*{border:0;margin:0;padding:0;}
	body{font:normal 14px "Tahoma","Arial","Verdana",sans-serif;background:#f2f2f2;}
	#content{background:transparent url("<?=_static?>images/_background.png") no-repeat 0 0;width:490px;height:325px;padding:15px;margin:auto;position:relative;}
	#nav {position:absolute;width:502px;height:56px;background:transparent url("<?=_static?>images/_nav_bg.jpg") no-repeat scroll top left;bottom:8px;left:9px;}
	p{color:#fff;font-size:14px;padding:18px 12px;text-align:left;}
	h1{padding:0 0 0 66px;height:52px;font-size:18px;font-weight:bold;color:#fff;line-height:26px;}
	.browser{height:56px;float:left;width:167px;}
	.ie{width:168px;background:url('<?=_static?>images/_buttons.png') no-repeat 0 0;}
	.ie:hover{background-position:0 -56px;}
	.firefox{background:url('<?=_static?>images/_buttons.png') no-repeat 0 -112px;}
	.firefox:hover{background-position:0 -168px;}
	.chrome{background:url('<?=_static?>images/_buttons.png') no-repeat 0 -224px;}
	.chrome:hover{background-position:0 -280px;}
	</style>
</head>
<body>
	<div id="content">
		<h1>Browserul cu care navigati pe aceasta pagina este <?=$browser_type?>, care nu poate reda aceasta pagina! El a fost abandonat oficial de catre Microsoft in 2001. Compania ofera spre download GRATUIT versiunea 8.0.</h1>
		<p>Va oferim spre alegere cele mai populare 3 browsere, cu care puteti vizualiza orice site fara probleme!</p>
		<div id="nav">
			<a class="browser ie" href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer 8"></a>
			<a class="browser firefox" href="http://www.mozilla.com/en-US/firefox/personal.html?from=getfirefox" title="Firefox"></a>
			<a class="browser chrome" href="http://www.google.com/chrome" title="Google Chrome"></a>
		</div>
	</div>
	<script type="text/javascript">
	var $element=document.getElementById('content').offsetHeight;
	var $document=document.body.clientHeight;
	document.getElementById('content').style.top=($document-$element)/2+'px';
	</script>
</body>
</html>