<?php

/**
 * Handle file uploads via XMLHttpRequest
 */
require_once(r.'.includes/Turbine/lib/browser/browser.php');

$browser = new Browser();
global $browser;

$browser->parse();
//$browser->browser;
//$browser->ua;
//$browser->engine;
$ip=ip();

//$filename=$browser->browser.$browser->ua.$browser->engine.'.'.$ip;
//
//print_a($filename);

class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {    
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 4194184;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 4194184){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = false){
    	global $user;
    	global $browser;
    	$ip=ip();

    	
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. $uploadDirectory isn t writable");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => 'Fisierul este prea mare. Trebuie sa aiba maxim 4MB!');
        }
//        $j=0;
//        $extension='';
//        for($i=0;$i<=29;$i++){
//			if(file_exists($_SERVER['DOCUMENT_ROOT']."/.tmp_img/".$user->info('username').$i.'old'.".jpg")){
//				$extension='.jpg';
//   			 }
//   			 if(file_exists($_SERVER['DOCUMENT_ROOT']."/.tmp_img/".$user->info('username').$i.'old'.".jpeg")){
//				$extension='.jpeg';
//   			 } 
//   			 if(file_exists($_SERVER['DOCUMENT_ROOT']."/.tmp_img/".$user->info('username').$i.'old'.".png")){
//				$extension='.png';
//   			 } 
//   			 if(file_exists($_SERVER['DOCUMENT_ROOT']."/.tmp_img/".$user->info('username').$i.'old'.".gif")){
//				$extension='.gif';
//   			 }
//			$filename=$user->info('username').$i.'old'.$extension;
//
//   			 if(file_exists($_SERVER['DOCUMENT_ROOT']."/.tmp_img/".$filename)){
//   			 	$j++;
//			}
//		}
        $pathinfo = pathinfo($this->file->getName());
//        $filename = $user->info('id').'-0';
		$file=md5($browser->browser.$browser->ua.$browser->engine).'.'.$ip;
		$filename=$file.'-0';
        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
//            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
//                $filename = $user->info('username').rand(1, 29);
//            }
			for($i=0;$i<=8;$i++){
				$increment=str_replace($file.'-','',$filename);
				$max=0;
				if((is_file($uploadDirectory . $filename . '.jpg')||is_file($uploadDirectory . $filename . '.png')||is_file($uploadDirectory . $filename . '.jpeg')||file_exists($uploadDirectory . $filename . '.gif'))&&$increment<=7){
					$filename=$file.'-'.($increment+1);
				}
				if(($increment+1)==9){
					$max=1;
				}
			}
			 if ($max==1) {
           		 return array('error' => 'Maxim 8 poze!');
        	}
        }
        
        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
            return ($filename . '.' . $ext);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
        
    }    
}

// list of valid extensions, ex. array("jpeg", "xml", "bmp")
$allowedExtensions = array("jpeg","jpg","JPG","gif","png");
// max file size in bytes
$sizeLimit = 4194184;

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
$result = $uploader->handleUpload(r.'.tmp_img/');
//print_a(_static.'imagini-anunturi');
// to pass data through iframe you will need to encode all html tags
//echo $gd->url('resize',SITEROOT.'/.tmp_img/'.$result,'150x150sc-#FFFFFF');
$array_data=array(
	'url'=>$gd->url('resize','.tmp_img/'.$result,'100x100sl-#FFFFFF'),
	'file'=>$result,
	'error'=>0	
);
if(is_array($result)){
	echo (json_encode($result));
}else{
	echo (json_encode($array_data));

}
//echo (json_encode("<div><img src=\"".$gd->url('resize','.tmp_img/'.$result,'150x150sl-#FFFFFF')."\"> <span id=\"".str_replace(substr($result,-4),"",$result)."\" class=\"delete\">X</span></div>"));
?>