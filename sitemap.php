<?php
require_once(dirname(__FILE__).'/.includes/init.php');

header ("content-type: text/xml");

echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

?>
<url>
	<loc><?php echo $b?></loc>
</url>
      <?php
if(
	$statement =
	$db->query("
		select
			p.`id`,
			pd.`name`, pd.`name_seo`,
			c.`id_parent`,
			cd.`name_seo` category,
			pc.`id_category`,
			b.`name` brand
		from
			`{$t['pd']}` p

			inner join `{$t['pd']}_data` pd
				on p.`id`=pd.`id_product`

			inner join `{$t['pd']}_to_categories` pc
				on p.`id`=pc.`id_product`

			inner join `{$t['ca']}` c
				on c.`id`=pc.`id_category`

			inner join `{$t['ca']}_data` cd
				on c.`id`=cd.`_id`

			inner join `{$t['b']}` b
				on p.`id_brand`=b.`id`


		where
			p.`status`='1' and
			b.`status` = 1 
		order by
			p.`date` desc
	")
){

	while(
		$p = $statement->fetch()
	){
		$p['image']=
			$db->fetchOne("
				select
					`image`
				from
					`{$t['pd']}_images`
				where
					`id_product` = ? and
					`status` = 1
				order by `order`,`image` asc
			",
			array($p['id'])
		);
		if(
			is_file(s.'i/imagini-produse/'.$p['image'])
		){
			$p['image']=$p['image'];
		}else{
			$p['image']='no-image-160x120.jpg';
		}
		


		$p['name'] = $engine->escape( $p['name'] );

		$p['link'] = lnk( 'product', $p );



		$page_info['products'][] = $p;
	}
}

	if (!empty($page_info['products']))
	foreach ($page_info['products'] as $item) {
		//$item['category']=$db->fetchOne("select `name_seo` from `xp_ads_categories_data` where `id`=".$item['id_category']); 

		//print_a($image);
        	?>
        	
        	
        	<url>
				<loc><?php echo $item['link']?></loc>
				<image:image>
   					 <image:loc><?php echo _static.'i/imagini-produse/'.str_replace('&','&amp;',$item['image'])?></image:loc>
 				</image:image>
			</url>		

<?php
	}
?>       
</urlset>