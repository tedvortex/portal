<?php
$select = array(
	'slide' => $db->select()
                  ->from(array('s' => $t['slide']),
                         array('id', 'type', 'image'))
                  ->joinInner(array('sd' => $t['slide'].'_data'),
                              'sd.`_id` = s.`id`',
                              array('name', 'link', 'description'))
                  ->where('s.`status` = 1')
                  ->order('order ASC'),
    'countries' => $db->select()
                      ->from(array('c' => 'xp_countries'),
                             array('id'))
                      ->joinInner(array('cd' => 'xp_countries_data'),
                                  'c.`id` = cd.`_id`',
                                  array('name' => 'lower(name)', 'name_seo'))
                      ->where('c.status = 1')
                      ->where('cd.lang = ?', 'en')
                      ->order('name asc')
);