<?php
$locator = null;

if( $index = return_page_info( $t['p'], 'index' )) {
	$index['name'] = $engine->escape( $index['name']);
	$index['link'] = $b;
}

if( $account = return_page_info( $t['p'], 'account', 'and a.`status`=1', true, 'constructor' )) {
	$account['name'] = $engine->escape($account['name']);
	$account['link'] = lnk('pages', $account );
}

if( $index = return_page_info( $t['p'], 'default', 'and a.`status`=1', true, 'constructor')) {
	$index['name'] = $engine->escape( $index['name']);
	$index['link'] = lnk('pages', $index );
}

if( $register = return_page_info( $t['p'], 'register', 'and a.`status`=1', true, 'constructor')) {
	$register['name'] = $engine->escape( $register['name']);
	$register['link'] = lnk('pages', $register );
}

if( $search = return_page_info( $t['p'], 'search', 'and a.`status`=1', true, 'constructor')) {
	$search['name'] = $engine->escape( $search['name']);
	$search['link'] = lnk('pages', $search );
}

if( $special_offers = return_page_info( $t['p'], 'special_offers', 'and a.`status`=1', true, 'constructor')) {
	$special_offers['name'] = $engine->escape( $special_offers['name']);
	$special_offers['link'] = lnk('pages', $special_offers );
}

if( $terms = return_page_info( $t['p'], 'termeni si conditii de utilizare', 'and a.`status`=1', true)) {
	$terms['name'] = $engine->escape( $terms['name']);
	$terms['link'] = lnk('pages', $terms );
}

$_construct = array(
	'page' => 404,

	'parsed' => array(
		'css' => '',
		'js' => ''
	),

	'modules' => array(
		'PIE.htc' => array(
			'header' => 'Content-type: text/x-component',
			'file' => s.'javascript/PIE.htc'
		),
		'token' => array(
			'header' => 'Content-type: text/html; charset=UTF-8',
			'file' => s.'token.php'
		),
		'robots.txt' => array(
			'header' => 'Content-type: text/plain',
			'file' => r.'robots.txt'
		),
		'sitemap.xml' => array(
			'header' => 'Content-type: application/rss+xml; charset=UTF-8',
			'file' => r.'sitemap.php'
		),
		'ajax.json' => array(
			'header' => 'Content-Type: application/javascript; charset=UTF-8',
			'file' => r.'ajax.php'
		),
		'upload' => array(
			'header' => 'Content-Type: text/html; charset=UTF-8',
			'file' => r.'upload.php'
		),
		'rotate' => array(
			'header' => 'Content-Type: image/png',
			'file' => r.'rotate.php'
		),
		'3d' => array(
			'header' => 'Content-Type: text/html; charset=UTF-8',
			'file' => r.'3d.php'
		),
		'crossdomain.xml' => array(
			'header' => 'Content-type: application/xml; charset=UTF-8',
			'file' => s.'crossdomain.xml'
		),
		'uploadify.php' => array(
			'header' => 'Content-Type: application/x-httpd-php; charset=UTF-8',
			'file' => i.'uploadify.php'
		),
	),

	'navigator' => array(
		"<li><a href=\"{$b}\" title=\"Acasa\">Acasa</a></li>"
	),

	'link' => 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
);


if(
	isset($_GET['p'])
){
	if(
		isset($_construct['modules'][$_GET['p']])
	){
		$mod = $_construct['modules'][$_GET['p']];

		header( $mod['header'] ) ;
		exit( include_once( $mod['file'] ) );
	}
}


if(
	isset($_REQUEST['action'])
){
	$action = $_REQUEST['action'];
	$type = 'error';
	$location = $b;

	if(
		$action == 'activate' &&
		isset( $_REQUEST['id'] ) &&
		isset( $_REQUEST['code'] ) &&
		is_numeric( $_REQUEST['id'] ) &&
		preg_match( '%[a-z0-9]{32}%', $_REQUEST['code'] )
	){
		if(
			$u =
			$db->fetchRow("
				select `id`
				from `{$t['cs']}`
				where
					`id` = ? and
					`code` = ? ",
				array( $_REQUEST['id'], $_REQUEST['code'] )
			)
		){
			if(
				$u['status']
			){
				$type = 'information';
			}

			else{
				$db->update(
					$t['cs'],
					array( 'status' => 1, 'code' => '' ),
					array( 'id' => $u['id'], )
				);

				$type = 'success';
			}
		}
	}

	elseif(
		$action == 'login'
	){
		$type = $user->login( $_POST['partner-email'], $_POST['partner-password'] );

		if(
			$user -> logat
		){
			if(
				isset($_POST['redirect']) &&
				$_POST['redirect']!=''
			){
				exit(header("Location: {$_POST['redirect']}"));
			}
			else{
				exit(header("Location: {$b}#action,login,success"));
			}
		}else{
			exit(header("Location: {$b}#action,login,error"));
		}

		$location = '';
	}

	elseif(
		$action == 'logout'
	){
		$user->logout();

		$type = 'success';
	}

	elseif(
		$action == 'recover'
	){
		if(
			$u =
			$db->fetchRow("
				select `id`,`email`
				from `{$t['cs']}`
				where
					`id` = ? and
					`code` = ?",
				array( $_GET['id'], $_GET['code'] )
			)
		){
			$validnum = rawurlencode(md5(uniqid(rand(),true)));

			$db->query("
				update `{$t['cs']}`
				set
					`password` = :password,
					`code` = :code
				where `id` = :id ",
				array( 'password' => md7($validnum) ,'code'=>'', 'id' => $u['id'] )
			);

			$vars['password'] = $validnum;

			template_mail(
				'reset',
				$u['email'],
				$vars
			);

			$type = 'success';
		}
	}

	elseif(
		$action == 'newsletter'
	){
		if(
			isset($_POST['newsletter-email']) &&
			$_POST['newsletter-email']!=''
		){
			if(
				!preg_match(
					'/^[^@]+@[a-zA-Z_.-]+?\.[a-zA-Z0-9]{2,4}$/',
					$_POST['newsletter-email']
				)
			){
				$error['newsletter-email']='Emailul introdus nu este corect!';
			}
			$Zend_Config2 = new Zend_Config_Ini(r.'config.ini','staging');
			$db2 = Zend_Db::factory($Zend_Config2->database);
			if(
				!$verEmail=
				$db2->fetchOne("
					select *
					from `email_list_subscribers`
					where `emailaddress`=?",
				array($_POST['newsletter-email'])
				)
			){
				if(
					empty($error)
				){
					$db2->insert(
						'email_list_subscribers',
						array(
							'emailaddress'=>$_POST['newsletter-email'],
							'confirmed'=>1,
							'listid'=>1,
							'requestdate'=>time(),
							'confirmdate'=>time(),
							'subscribedate'=>time(),
							'confirmcode'=>md5($_POST['newsletter-email'])
						)
					);

					$type = 'success';
				}

				else{
					$type = 'error';
				}
			}

			else{
				$type = 'information';
			}
		}
	}

	exit(header("Location: {$location}#action,{$action},{$type}"));
}


if(
	isset($_GET['p'])
){
	if(
		isset($_construct['modules'][$_GET['p']])
	){
		$mod = $_construct['modules'][$_GET['p']];

		header( $mod['header'] ) ;
		exit( include( $mod['file'] ) );
	}


	if(
		is_numeric($_GET['p'])
	){
		$_GET['page'] = $_GET['p'];

		$_construct['item'] = '';
	}

	else{
		$_p = explode('/', $_GET['p']);

		foreach(
			$_p as
			$k => $p
		){
			$_p[$k] = mb_eregi_replace('-', ' ' , $p);
		}

		reset($_p);

		$_construct['item'] = $_p[0];
	}
}


$_construct['link'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$_construct['item'] = ( empty($_construct['item']) ? 'index' : $_construct['item'] );


if ($page_info = return_page_info($t['p'], $_construct['item'])) {
	$page_info['link'] = lnk( 'pages', $page_info );
	$page_info['name'] = $engine->escape($page_info['name']);
	$_construct['navigator'][] = "<li><a href=\"{$page_info['link']}\" title=\"{$page_info['name']}\">".$page_info['name']."</a></li>";

	$_construct['page'] = 'page';
}

elseif ($page_info = return_page_info(
		$t['ca'],
		$_construct['item']
)) {
	$parent = $_construct['item'];

	$category_link = $page_info['link'] = lnk( 'category', $page_info );
	$page_info['name'] = $engine->escape($page_info['name']);

	$_construct['navigator'][] = "<li><a href=\"{$page_info['link']}\" title=\"{$page_info['name']}\">".$page_info['name']."</a></li>";

	$_construct['page'] = $page_info['constructor'] = 'products';

	$_GET['files_extend'] = ';products_' . url($page_info['name_seo']) . '.cssp';

	$locator = 'basic';

	if (sizeof($_p) > 1) {
	    $_construct['item'] = next($_p);

	    if ($page_info['id'] == 1) {
	        $imageSql = $db->select()
                           ->from(array('si' => 'fibula_hotels_images'),
                                  array('image'))
	                       ->where('si.`id_hotel` = s.`id_hotel`')
	                       ->order('order asc')
	                       ->limit(1, 0);

	        $offerSql = $db->select()
	                       ->from(array('sc' => 'xp_sejur_cost'),
	                              array(new Zend_Db_Expr('count(distinct `id_room_type`)')))
	                       ->where('sc.`id_sejur` = s.`id`');

	        $sql = $db->select()
                      ->from(array('s' => 'xp_sejur'),
	                         array(
        	                     new Zend_Db_Expr('sql_calc_found_rows `s`.`id`'),
        	                     'offers' => new Zend_Db_Expr('(' . $offerSql . ')'),
        	                     'id_hotel', 'price','id_airport', 'stock', 'date_start', 'date_end', 'duration', 'max_stay', 'code', 'image' => new Zend_Db_Expr('(' . $imageSql . ')')))
	                  ->joinInner(array('sd' => 'xp_sejur_data'),
	                              's.`id` = sd.`_id`',
	                              array('name', 'name_seo', 'short_description', 'description','policy','terms'))
                      ->joinInner(array('cr' => 'xp_currencies'),
	                              'cr.`id` = s.`id_currency`',
	                              array('token'))
	                  ->joinInner(array('c' => 'fibula_countries'),
	                              'c.`id` = s.`id_region`',
	                              array('id as id_region'))
	                  ->joinInner(array('cd' => 'fibula_countries_data'),
	                              'c.`id` = cd.`_id`',
	                              array('name as city_name'))
	                  ->joinInner(array('cc' => 'fibula_countries'),
	                              'cc.`id` = s.`id_country`',
	                              array(''))
	                  ->joinInner(array('cdc' => 'fibula_countries_data'),
	                              'cc.`id` = cdc.`_id`',
	                              array('name as country_name'))
	                  ->joinInner(array('h' => 'fibula_hotels'),
	                              'h.`id` = s.`id_hotel`',
	                              array('rating', 'latitude', 'longitude'))
	                  ->joinInner(array('hd' => 'fibula_hotels_data'),
	                              'h.`id` = hd.`_id`',
	                              array('address', 'postal_code', 'telephone', 'fax', 'website', 'email', 'hotel' => 'name'))
                      ->where('s.`status` = 1')
                      ->where(new Zend_Db_Expr('now() between `s`.`availability_start` and `s`.`availability_end`'))
                      ->where('sd.`name_seo` = ?', $_construct['item'])
	                  ->where('sd.`lang` = ?', $_SESSION['lang']);

	        $page_info['item'] = $db->fetchRow($sql);

	        if (! $page_info['item']) {
	            $_construct['page'] = 404;
	        }
	        else {
	            $locator = 'item';

	        	$page_info['item']['link'] = $category_link . '/' . lnk('sejur', $page_info['item']);

	        	$_construct['navigator'][] = "<li><a href=\"{$page_info['item']['link']}\" title=\"{$page_info['item']['name']}\">".mb_ucfirst(mb_strtolower($page_info['item']['name']))."</a></li>";

	        	if(sizeof($_p) > 2){
	        		$_construct['item'] = end($_p);

	        		if( $_construct['item'] != 'rezerva'){
	        			$_construct['page'] = 404;
	        		}
	        		else {
	        		    $locator = 'order';

	        			$page_info['booking'] = array(
	        				'name' => 'Rezerva sejur',
	        				'link' => $page_info['item']['link'] . '/rezerva'
	        			);

	        			$_construct['navigator'][] = "<li><a href=\"{$page_info['booking']['link']}\" title=\"{$page_info['booking']['name']}\">".$page_info['booking']['name']."</a></li>";
	        		}
	        	}
	        }
	    }

	    elseif ($page_info['id'] == 2) {
	    	
    		$page_info['query'] = $db->fetchRow("
    		        select
    		            sq.*,
    		            cd.`name` city_name
    		        from
    		            `xp_search_queries` sq
    		            inner join `xp_cities_data` cd
                            on cd.`_id` = sq.`id_city`
    		        where sq.`hash` = ?",
    		        array($_construct['item']));

    		if (! $page_info['query'] && !in_array($_construct['item'], array('confirm', 'view')) ){
    		    $_construct['page'] = 404;
    		}
    		
    		elseif( $_construct['item'] == 'confirm' ){
    			
    			if( count($_p) == 3 ){
    				
    				$_construct['item'] = next($_p);
    				
    				if(
    					$page_info['confirm'] = $db->fetchRow("
    						select
    							o.`id`, o.`no_order`, o.`status`, o.`statusOmega`, o.`reservationReferenceOmega`, o.`buyer_type`, o.`buyer_last_name`, o.`buyer_first_name`, o.`buyer_company`,
    							op.`priceAvailabilityOmega`, op.`price`, op.`commission`, op.`totalAvailabilityOmega`, op.`total`, op.`hotel_name`,
    							cr.`token_omega`
    						from
    							`xp_orders` o
    							inner join `xp_orders_products` op
    								on op.`id_order` = o.`id`
    							inner join `xp_currencies` cr
    								on cr.`id` = op.`id_currency`
    						where
    							o.`statusOmega` = 'Requires confirmation' and
    							o.`id_category` = 2 and
    							o.`no_order` = ? ",
    						array(url($_construct['item']))
    					)
    				){
    					
    					if( $page_info['confirm']['buyer_type'] == 0 ){
    						$page_info['confirm']['holder'] = mb_ucwords($page_info['confirm']['buyer_first_name'].' '.$page_info['confirm']['buyer_last_name']);
    					}
    					else {
    						$page_info['confirm']['holder'] = $page_info['confirm']['buyer_company'];
    					}
    					
    					$locator = 'confirm';
    				}
    				else{
	    				$_construct['page'] = 404;
	    			}
    			}
    			else{
    				$_construct['page'] = 404;
    			}
    		}
    		
    		elseif( $_construct['item'] == 'view' ){
    			
    			if( count($_p) == 3 ){
    				
    				$_construct['item'] = next($_p);
    				
    				if(
    					$page_info['view'] = $db->fetchRow("
    						select
    							o.`id`, o.`no_order`, o.`payment_method`, o.`statusMobilpay`, o.`errorCode`, o.`status`, o.`statusOmega`, o.`omegaInitiated`, o.`reservationReferenceOmega`, o.`buyer_type`, o.`buyer_last_name`, o.`buyer_first_name`, o.`buyer_company`,
    							op.`priceAvailabilityOmega`, op.`price`, op.`commission`, op.`totalAvailabilityOmega`, op.`total`, op.`hotel_name`,
    							cr.`token_omega`
    						from
    							`xp_orders` o
    							inner join `xp_orders_products` op
    								on op.`id_order` = o.`id`
    							inner join `xp_currencies` cr
    								on cr.`id` = op.`id_currency`
    						where
    							o.`id_category` = 2 and
    							o.`no_order` = ? ",
    						array(url($_construct['item']))
    					)
    				){
    					if( $page_info['view']['buyer_type'] == 0 ){
    						$page_info['view']['holder'] = mb_ucwords($page_info['view']['buyer_first_name'].' '.$page_info['view']['buyer_last_name']);
    					}
    					else {
    						$page_info['view']['holder'] = $page_info['view']['buyer_company'];
    					}
    					
    					$locator = 'view';
    				}
    				else{
    					$_construct['page'] = 404;
    				}
    			}
    			else{
    				$_construct['page'] = 404;
    			}
    			
    		}

    		elseif (sizeof($_p) > 2) {
    		    $_construct['item'] = next($_p);
    		    
    		    $locator = 'item';

    		    $page_info['item'] = $db->fetchRow("
        		        select si.*
        		        from `xp_search_items` si
        		        where
        	                si.`id_search_query` = ?
        		            and si.`id_hotel` = ?",
    	                array($page_info['query']['id'], $_construct['item']));

    		    if (! $page_info['item']) {
    		        $_construct['page'] = 404;
    		    } elseif (count($_p) > 4) {
    		        $_construct['item'] = next($_p);

    		        $locator = 'order';

    		        $page_info['room'] = $db->fetchRow("
    		                select `sr`.*
    		                from `xp_search_rooms` as `sr`
    		                where `sr`.`id_search_room` = ?",
    		                array($_construct['item']));

    		        if (! $page_info['room']) {
    		            $_construct['page'] = 404;
    		        } else {
    		            $_construct['item'] = next($_p);

    		            $page_info['order_board'] = $db->fetchRow("
    		                select `sr`.*
    		                from `xp_search_boards` as `sr`
    		                where
		                        `sr`.`id_search_room` = ?
		                        and `sr`.`code` = ?",
    		                    array($page_info['room']['id_search_room'], $_construct['item']));

    		            if (! $page_info['room']) {
    		                $_construct['page'] = 404;
    		            }
    		        }
    		    }
    		}
    		
	    } elseif ($page_info['id'] == 4) {
	                $imageSql = $db->select()
                           ->from(array('si' => 'xp_circuit_images'),
                                  array('image'))
	                       ->where('si.`id_circuit` = s.`id`')
	                       ->order('order asc')
	                       ->limit(1, 0);

	        $offerSql = $db->select()
	                       ->from(array('sc' => 'xp_circuit_cost'),
	                              array(new Zend_Db_Expr('count(distinct `id_room_type`)')))
	                       ->where('sc.`id_circuit` = s.`id`');

	        $sql = $db->select()
                      ->from(array('s' => 'xp_circuit'),
	                         array(
        	                     new Zend_Db_Expr('sql_calc_found_rows `s`.`id`'),
        	                     'offers' => new Zend_Db_Expr('(' . $offerSql . ')'),
        	                     'id_hotel', 'price','id_airport', 'stock', 'date_start', 'date_end', 'duration', 'max_stay', 'code', 'image' => new Zend_Db_Expr('(' . $imageSql . ')')))
	                  ->joinInner(array('sd' => 'xp_circuit_data'),
	                              's.`id` = sd.`_id`',
	                              array('name', 'name_seo', 'short_description', 'description','policy','terms'))
                      ->joinInner(array('cr' => 'xp_currencies'),
	                              'cr.`id` = s.`id_currency`',
	                              array('token'))
	                  ->joinInner(array('c' => 'fibula_countries'),
	                              'c.`id` = s.`id_region`',
	                              array('id as id_region'))
	                  ->joinInner(array('cd' => 'fibula_countries_data'),
	                              'c.`id` = cd.`_id`',
	                              array('name as city_name'))
	                  ->joinInner(array('cc' => 'fibula_countries'),
	                              'cc.`id` = s.`id_country`',
	                              array(''))
	                  ->joinInner(array('cdc' => 'fibula_countries_data'),
	                              'cc.`id` = cdc.`_id`',
	                              array('name as country_name'))
	                  ->joinInner(array('h' => 'fibula_hotels'),
	                              'h.`id` = s.`id_hotel`',
	                              array('rating', 'latitude', 'longitude'))
	                  ->joinInner(array('hd' => 'fibula_hotels_data'),
	                              'h.`id` = hd.`_id`',
	                              array('address', 'postal_code', 'telephone', 'fax', 'website', 'email', 'hotel' => 'name'))
                      ->where('s.`status` = 1')
                      ->where(new Zend_Db_Expr('now() between `s`.`availability_start` and `s`.`availability_end`'))
                      ->where('sd.`name_seo` = ?', $_construct['item'])
	                  ->where('sd.`lang` = ?', $_SESSION['lang']);

	        $page_info['item'] = $db->fetchRow($sql);

	        if (! $page_info['item']) {
	            $_construct['page'] = 404;
	        } else {
	            $locator = 'item';

	        	$page_info['item']['link'] = $category_link . '/' . lnk('sejur', $page_info['item']);

	        	$_construct['navigator'][] = "<li><a href=\"{$page_info['item']['link']}\" title=\"{$page_info['item']['name']}\">".mb_ucfirst(mb_strtolower($page_info['item']['name']))."</a></li>";

	        	if(sizeof($_p) > 2){
	        		$_construct['item'] = end($_p);

	        		if( $_construct['item'] != 'rezerva'){
	        			$_construct['page'] = 404;
	        		} else {
	        		    $locator = 'order';

	        			$page_info['booking'] = array(
	        				'name' => 'Rezerva circuit',
	        				'link' => $page_info['item']['link'] . '/rezerva'
	        			);

	        			$_construct['navigator'][] = "<li><a href=\"{$page_info['booking']['link']}\" title=\"{$page_info['booking']['name']}\">".$page_info['booking']['name']."</a></li>";
	        		}
	        	}
	        }
	    } elseif ($page_info['id'] == 3) {
	                $imageSql = $db->select()
                           ->from(array('si' => 'xp_croaziera_images'),
                                  array('image'))
	                       ->where('si.`id_croaziera` = s.`id`')
	                       ->order('order asc')
	                       ->limit(1, 0);

	        $offerSql = $db->select()
	                       ->from(array('sc' => 'xp_croaziera_cost'),
	                              array(new Zend_Db_Expr('count(distinct `id_room_type`)')))
	                       ->where('sc.`id_croaziera` = s.`id`');

	        $sql = $db->select()
                      ->from(array('s' => 'xp_croaziera'),
	                         array(
        	                     new Zend_Db_Expr('sql_calc_found_rows `s`.`id`'),
        	                     'offers' => new Zend_Db_Expr('(' . $offerSql . ')'),
        	                     'id_hotel', 'price','id_airport', 'stock', 'date_start', 'date_end', 'duration', 'max_stay', 'code', 'image' => new Zend_Db_Expr('(' . $imageSql . ')')))
	                  ->joinInner(array('sd' => 'xp_croaziera_data'),
	                              's.`id` = sd.`_id`',
	                              array('name', 'name_seo', 'short_description', 'description','policy','terms'))
                      ->joinInner(array('cr' => 'xp_currencies'),
	                              'cr.`id` = s.`id_currency`',
	                              array('token'))
	                  ->joinInner(array('c' => 'fibula_countries'),
	                              'c.`id` = s.`id_region`',
	                              array('id as id_region'))
	                  ->joinInner(array('cd' => 'fibula_countries_data'),
	                              'c.`id` = cd.`_id`',
	                              array('name as city_name'))
	                  ->joinInner(array('cc' => 'fibula_countries'),
	                              'cc.`id` = s.`id_country`',
	                              array(''))
	                  ->joinInner(array('cdc' => 'fibula_countries_data'),
	                              'cc.`id` = cdc.`_id`',
	                              array('name as country_name'))
	                  ->joinInner(array('h' => 'fibula_hotels'),
	                              'h.`id` = s.`id_hotel`',
	                              array('rating', 'latitude', 'longitude'))
	                  ->joinInner(array('hd' => 'fibula_hotels_data'),
	                              'h.`id` = hd.`_id`',
	                              array('address', 'postal_code', 'telephone', 'fax', 'website', 'email', 'hotel' => 'name'))
                      ->where('s.`status` = 1')
                      ->where(new Zend_Db_Expr('now() between `s`.`availability_start` and `s`.`availability_end`'))
                      ->where('sd.`name_seo` = ?', $_construct['item'])
	                  ->where('sd.`lang` = ?', $_SESSION['lang']);

	        $page_info['item'] = $db->fetchRow($sql);

	        if (! $page_info['item']) {
	            $_construct['page'] = 404;
	        } else {
	            $locator = 'item';

	        	$page_info['item']['link'] = $category_link . '/' . lnk('sejur', $page_info['item']);

	        	$_construct['navigator'][] = "<li><a href=\"{$page_info['item']['link']}\" title=\"{$page_info['item']['name']}\">".mb_ucfirst(mb_strtolower($page_info['item']['name']))."</a></li>";

	        	if(sizeof($_p) > 2){
	        		$_construct['item'] = end($_p);

	        		if( $_construct['item'] != 'rezerva'){
	        			$_construct['page'] = 404;
	        		} else {
	        		    $locator = 'order';

	        			$page_info['booking'] = array(
	        				'name' => 'Rezerva croaziera',
	        				'link' => $page_info['item']['link'] . '/rezerva'
	        			);

	        			$_construct['navigator'][] = "<li><a href=\"{$page_info['booking']['link']}\" title=\"{$page_info['booking']['name']}\">".$page_info['booking']['name']."</a></li>";
	        		}
	        	}
	        }
	    }
	}
}

if ($_construct['page'] == 404) {
	$page_info = return_page_info($t['p'], '404');
	$_construct['page'] = 'page';

	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
	header("Status: 404 Not Found");
}

$n = sizeof( $_construct['navigator'] ) - 1;
$_construct['navigator'][$n] = preg_replace('%<li>%', '<li class="a">', $_construct['navigator'][$n]);

$_construct['template'] = (isset($page_info['constructor']) && $page_info['constructor'] != '' ? $page_info['constructor'] : $_construct['page']);