<?php
$_GET['files'] = 'basic.cssp;'.$_construct['template'].'.cssp' . (isset($_GET['files_extend']) ? $_GET['files_extend'] : '');
$_construct['parsed']['css'] = require_once(i.'Turbine/css.php');

$globals = array(
	'b'			=> $b,
	's'			=> _static,
	'google_verification'	=> $config->google->site_verification,
	'analytics'		=> $config->google->analytics,
	'language'		=> $config->language,
	'author'		=> $config->author,
	'address'		=> $config->address,
	'phone'			=> $config->phone,
	'phone2'			=> $config->phone2,
	'phone3'			=> $config->phone3,
	'voucher_min'	=> $config->voucher_min,
	'voucher_max'	=> $config->voucher_max,
	'fax'			=> $config->fax,
	'email'			=> $config->site_email,
	'facebook'		=> $config->facebook,
	'twitter'		=> $config->twitter,
	'youtube'		=> $config->youtube,
	'site_name'		=> $config->site_name,
	'no_news'		=> $config->no_news,
	'upload_terms'		=> $config->upload_terms,
	'googleplus'	=> $config->googleplus,
	'warranty'  => $config->warranty,
	'delivery'	=> $config->delivery,
	'transport'	=> $config->transport,
	'execution'	=> $config->execution,
	'points'	=> $config->points,
	'economy'	=> $config->economy,
	'per_page'	=> $config->per_page,
	'page_name'			=> $page_info['name'],
	'id'			=> $page_info['id'],
	'constructor'		=> isset($page_info['constructor'])?$page_info['constructor']:'',
	'header_title'		=> $page_info['header_title'],
	'meta_description'	=> $page_info['meta_description'],
	'meta_keywords'	=> $page_info['meta_keywords'],
	'head'			=> '.head',
	'header'		=> '.header',
	'footer'			=> '.footer',
	'boxes'			=> '.boxes',
	'boxes_sejururi'			=> '.boxes_sejururi',
	'boxes_cazari'			=> '.boxes_cazari',
	'boxes_croaziere'			=> '.boxes_croaziere',
	'boxes_circuite'			=> '.boxes_circuite',
	'javascript'		=> 'default',
	'special_offers'		=> $special_offers,
	'home'		=> $index,
	'register'		=> $register,
	'search'		=> $search,
	'account'		=> $account,
	'class'			=> (isset($page_info['class'])&&$page_info['class']?$page_info['class']:''),
	'terms'		=> $terms,
	//'count'			=> sizeof($shopcart->products),
	'css'			=> &$_construct['parsed']['css'],
	'template'		=> &$_construct['template'],
	'logged'		=> $user->logat,
	'languages'		=> $languages,
	'name'			=> ( $user->logat ? $user->info('first_name') . ' ' . $user->info('last_name') : '' ),
	'id_user'			=> ($user->logat ?$user->info('id'):0),
	'navigator'		=> '<ul class="nav">'.implode( '<li>/</li>', $_construct['navigator'] ).'</ul>',
	'tags' 				=> array(),
	'pages'		 		=> array(),
	'linkuri'		 	=> array(),
	'partners' => array(),
	'slides'	=> array('first' => array(), 'second' => array())
);

if(
	$statement = $db->query("
		select
		  c.`id`, c.`is_search`,
		  cd.`name`,cd.`name_seo`,cd.`class`
		from
			`{$t['ca']}` c
			inner join `{$t['ca']}_data` cd
			on c.`id` = cd.`_id`
		where
			c.`id_parent` = 0
		order by c.`id` asc
	")
){
	while(
		$c = $statement->fetch()
	){
		$c['name'] = mb_strtolower($engine->escape( $c['name'] ));
		$c['link'] = lnk('category',$c);
		$globals['main_categories'][$c['id']] = $c;
	}
}

if(
	$statement = $db->query("
		select
		  c.`id`, c.`is_search`,
		  cd.`name`,cd.`name_seo`,cd.`class`
		from
			`{$t['ca']}` c
			inner join `{$t['ca']}_data` cd
			on c.`id` = cd.`_id`
		where
			c.`id_parent` = 0 and
			c.`is_search` = 1
		order by c.`id` asc
	")
){
	while(
		$c = $statement->fetch()
	){
		$c['name'] = mb_strtolower($engine->escape( $c['name'] ));
		$c['link'] = lnk('category',$c);
		$globals['main_categories2'][$c['id']] = $c;
	}
}


if(
	$statement_partners = $db->query("
		select `name`, `link`, `image`
		from `xp_partners`
		where `status` = 1
		order by `order`
	")
){
	while(
		$pr = $statement_partners->fetch()
	){
		$pr['name'] = $engine->escape($pr['name']);
		$pr['link'] = $pr['link'] ? $pr['link'] : '#';

		if(
			is_file( s . 'i/parteneri/' . $pr['image'])
		){
			$pr['image'] = image_uri('i/parteneri/'.$pr['image']);
			$globals['partners'][] = $pr;
		}

	}
}

// print_a($globals['main_categories']);

$pages = array( 'header' => 'is_header' , 'footer' => 'is_footer' );
foreach ($pages as $k => $v) {
	if(
		$statement =
		$db->query("
			select
				p.`id`,
				pd.`name`,
				pd.`name_seo`,
				pd.`url`,
				pd.`class`
			from
				`{$t['p']}` p
				inner join `{$t['p']}_data` pd
					on p.`id` = pd.`_id`
			where
				p.`status` = 1 and
				p.`{$v}` = 1 and
				pd.`lang` = ?
			order by p.`order` asc
		",array($_SESSION['lang']))
	){
		$pages[ $k ] = array();

		while(
			$p = $statement->fetch()
		){
			$p['name'] = mb_strtolower( $engine->escape( $p['name'] ) );

			if(
				$v == 'is_info'
			){
				$p['name'] = ucfirst($p['name']);
			}

			$p['link'] = lnk( 'pages', $p );

			$pages[ $k ][]	= $p;
		}
	}

	$globals['pages'] = $pages;
}

$select_slides = clone($select['slide']);
$select_slides->where('s.`type` = 1 or s.`type` = 2');

if( $stmt_slides = $db->query($select_slides) )
{
	while( $sl = $stmt_slides->fetch() )
	{
		if( is_file(s.'i/slide/'.$sl['image']) )
		{
			$sl['name'] = $engine->escape($sl['name']);
			$sl['link'] = $sl['link'] ? $sl['link'] : '#';
			$sl['image'] = image_uri('i/slide/'.$sl['image']);

			$globals['slides'][ $sl['type'] == 1 ? 'first' : 'second' ][] = $sl;
		}
	}
}

$globals['countries'] = $db->fetchAll($select['countries']);


#links start

if(
	$statement =
	$db->query("
		select
			*
		from
			`xp_links`
		where
			`status`=1
		order by `id` asc
	"
	)
){
	while(
		$c = $statement->fetch()
	){
		$globals['linkuri'][] = $c;
	}
}


if(isset($globals['constructor']) && $globals['constructor'] != 'products'){
    
    #croaziere
    $durationSql = $db->select()
                      ->from(array('s' => 'xp_croaziera'),
                             array('min' => new Zend_Db_Expr('min(`duration`)'),
                                   'max' => new Zend_Db_Expr('max(`max_stay`)')))
                      ->where('`s`.`status` = 1')
                      ->where('`s`.`duration` <> 0')
                      ->where('`s`.`max_stay` <> 0');
    
    $globals['filter_duration'] = $db->fetchRow($durationSql);
    
    $defaultSql = $db->select()
                     ->from(array('s' => 'xp_croaziera'),
                            array())
                     ->joinInner(array('sd' => 'xp_croaziera_data'),
                                 'sd.`_id` = s.`id`',
                                 array())
                    ->where('`s`.`status` = 1')
                    ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)')));
    
    $countrySql = $db->select()
                     ->from(array('c' => 'fibula_countries'),
                            array('id'))
                     ->joinInner(array('cd' => 'fibula_countries_data'),
                                 '`c`.`id` = `cd`.`_id`',
                                 array('name', 'name_seo'))
                     ->where(new Zend_Db_Expr('`c`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                     ->reset(Zend_Db_Select::WHERE)
                     ->where('`s`.`status` = 1')
                     ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)'))) . ')'));
    
    $statement = $db->query($countrySql);
    
    if ($statement) {
        while ($p = $statement->fetch()) {
            $globals['croaziere_filter_countries'][] = $p;
        }
    }
    
    $globals['croaziere_route'] = $route_cities = array();
    
    $sql_route = $db->select()
                    ->from(array('ct' => 'fibula_countries'),
                           array('id'))
                    ->joinInner(array('ctd' => 'fibula_countries_data'),
                                'ctd.`_id` = ct.`id`',
                                array('name', 'name_seo'))
                    ->joinInner(array('ctc' => 'xp_croaziera_in_cities'),
                                'ctc.`id_city` = ct.`id`',
                                array(''))
                    ->joinInner(array('c' => 'fibula_countries'),
                                'c.`id` = ct.`id_parent`',
                                array('id_country' => 'id'))
                    ->joinInner(array('cd' => 'fibula_countries_data'),
                                'cd.`_id` = c.`id`',
                                array('country_name' => 'name', 'country_name_seo' => 'name_seo'))
                    ->where('ct.`status` = 1')
                    ->where('c.`status` = 1')
                    ->group('ct.id')
                    ->order(array('cd.name', 'ctd.name'));
                    
    if( $statement = $db->query($sql_route) ){
    
        while( $c = $statement->fetch() ){
    
            if( !isset($globals['croaziere_route'][ $c['id_country'] ]) ){
                $globals['croaziere_route'][ $c['id_country'] ] = array(
                    'id_country' => $c['id_country'],
                    'country_name' => $c['country_name'],
                    'country_name_seo' => $c['country_name_seo'],
                    'cities' => array()
                );
            }
    
            $globals['croaziere_route'][ $c['id_country'] ]['cities'][] = array(
                'id' => $c['id'],
                'name' => $c['name'],
                'name_seo' => $c['name_seo']
            );
    
            $route_cities[$c['id']] = $c['id_country'];
        }
    }
    
    #circuite
    $durationSql = $db->select()
                      ->from(array('s' => 'xp_circuit'),
                             array('min' => new Zend_Db_Expr('min(`duration`)'),
                                   'max' => new Zend_Db_Expr('max(`max_stay`)')))
                      ->where('`s`.`status` = 1')
                      ->where('`s`.`duration` <> 0')
                      ->where('`s`.`max_stay` <> 0');
    
    $globals['circuite_filter_duration'] = $db->fetchRow($durationSql);
    
    $defaultSql = $db->select()
                     ->from(array('s' => 'xp_circuit'),
                            array())
                     ->joinInner(array('sd' => 'xp_circuit_data'),
                                 'sd.`_id` = s.`id`',
                                 array())
                     ->where('`s`.`status` = 1')
                     ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)')));
    
    $countrySql = $db->select()
                     ->from(array('c' => 'fibula_countries'),
                            array('id'))
                     ->joinInner(array('cd' => 'fibula_countries_data'),
                                 '`c`.`id` = `cd`.`_id`',
                                 array('name', 'name_seo'))
                     ->where(new Zend_Db_Expr('`c`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                     ->reset(Zend_Db_Select::WHERE)
                     ->where('`s`.`status` = 1')
                     ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)'))) . ')'));
    
    $statement = $db->query($countrySql);
    
    if ($statement) {
        while ($p = $statement->fetch()) {
            $globals['circuite_filter_countries'][] = $p;
        }
    }
    
    $globals['circuite_route'] = $route_cities = array();
    
    $sql_route = $db->select()
                    ->from(array('ct' => 'fibula_countries'),
                           array('id'))
                    ->joinInner(array('ctd' => 'fibula_countries_data'),
                                'ctd.`_id` = ct.`id`',
                                array('name', 'name_seo'))
                    ->joinInner(array('ctc' => 'xp_circuit_in_cities'),
                                'ctc.`id_city` = ct.`id`',
                                array(''))
                    ->joinInner(array('c' => 'fibula_countries'),
                                'c.`id` = ct.`id_parent`',
                                array('id_country' => 'id'))
                    ->joinInner(array('cd' => 'fibula_countries_data'),
                                'cd.`_id` = c.`id`',
                                array('country_name' => 'name', 'country_name_seo' => 'name_seo'))
                    ->where('ct.`status` = 1')
                    ->where('c.`status` = 1')
                    ->group('ct.id')
                    ->order(array('cd.name', 'ctd.name'));
    
    if( $statement = $db->query($sql_route) ){

        while( $c = $statement->fetch() ){

            if( !isset($globals['circuite_route'][ $c['id_country'] ]) ){
                $globals['circuite_route'][ $c['id_country'] ] = array(
                    'id_country' => $c['id_country'],
                    'country_name' => $c['country_name'],
                    'country_name_seo' => $c['country_name_seo'],
                    'cities' => array()
                );
            }

            $globals['circuite_route'][ $c['id_country'] ]['cities'][] = array(
                'id' => $c['id'],
                'name' => $c['name'],
                'name_seo' => $c['name_seo']
            );

            $route_cities[$c['id']] = $c['id_country'];
        }
    }
    
    #sejururi
    $defaultSql = $db->select()
                     ->from(array('s' => 'xp_sejur'),
                            array())
                     ->joinInner(array('sd' => 'xp_sejur_data'),
                                 'sd.`_id` = s.`id`',
                                 array())
                     ->where('`s`.`status` = 1')
                     ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)')));
    
    $countrySql = $db->select()
                     ->from(array('c' => 'fibula_countries'),
                            array('id'))
                     ->joinInner(array('cd' => 'fibula_countries_data'),
                                 '`c`.`id` = `cd`.`_id`',
                                 array('name', 'name_seo'))
                     ->where(new Zend_Db_Expr('`c`.`id` in ( ' . $defaultSql->reset(Zend_Db_Select::COLUMNS)
                                                                            ->reset(Zend_Db_Select::WHERE)
                                                                            ->where('`s`.`status` = 1')
                                                                            ->columns(array(new Zend_Db_Expr('distinct(`s`.`id_country`)'))) . ')'));

    $statement = $db->query($countrySql);

    if ($statement) {
        while ($p = $statement->fetch()) {
            $globals['filter_countries'][] = $p;
        }
    }    
}