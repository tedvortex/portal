<?php

if(
	$page_info =
	return_page_info(
		$t['ca'],
		$_construct['item']
	)
){

	$parent = $_construct['item'];

	$page_info['cat_link'] = categories_link($page_info['id_parent']);

	$page_info['link'] = lnk( 'category', $page_info );

	$page_info['name'] = $engine->escape($page_info['name']);

	$_construct['navigator'][] = "<li><span class=\"sprite\"></span><a href=\"{$page_info['link']}\" title=\"{$page_info['name']}\">".ucfirst($page_info['name'])."</a></li>";

	if(
		$db->fetchOne("
			select
				c.`id`,
				c.`id_parent`,
				cd.`name`,
				cd.`name_seo`
			from
				`{$t['ca']}` c
				inner join `{$t['ca']}_data` cd
					on c.`id` = cd.`_id`
			where
				c.`status` = 1 and
				c.`id_parent` = ?
			order by c.`order` asc
			limit 1",
			$page_info['id']
		)
	){
		$_construct['page'] = 'category';
	}
	else{
		$_construct['page'] = 'products_list';
		$page_info['constructor'] = 'products_list';

		$page_info['count']=
		$db->fetchOne("
			select
				count(pc.`id`) `count`
			from
				`{$t['pd']}_to_categories` pc
				inner join `{$t['ca']}` c
					on pc.`id_category` = c.`id`
				inner join `{$t['pd']}` p
					on pc.`id_product` = p.`id`
			where
				pc.`id_category` = ? and
				c.`status` = 1 and
				p.`status` = 1
		",
		array($page_info['id'])
		);
	}

	$page_info['first_level_cat'] = $first_level_cat;
}
elseif(
	$page_info =
	$db->fetchRow("
			select
				p.`id`, p.`stock`,p.`id_variation`,p.`price`,
				pd.`name`, pd.`name_seo`, pd.`description`,
				pd.`youtube_1`,pd.`youtube_2`,pd.`youtube_3`,pd.`youtube_4`,
				pd.`youtube_5`,pd.`youtube_6`,pd.`youtube_7`,pd.`youtube_8`,
				c.`id_parent`, c.`id` id_category,
				cd.`name_seo` category,
				if( p.`price` > ifnull( pv.`price`, 0 ), p.`price`, pv.`price` ) pricep,
				if( p.`pricev` > ifnull( pv.`pricev`, 0 ), p.`pricev`, pv.`pricev` ) pricev,
				b.`id` brand_id, b.`name` brand_name, b.`image` brand_image, b.`name_seo` brand_name_seo
			from
				`{$t['pd']}` p
				inner join `{$t['pd']}_data` pd
					on p.`id` = pd.`id_product`
				inner join `{$t['pd']}_to_categories` pc
					on p.`id`=pc.`id_product`
				inner join `{$t['ca']}` c
					on pc.`id_category` = c.`id`
				inner join `{$t['ca']}_data` cd
					on c.`id` = cd.`_id`
				inner join `{$t['b']}` b
					on b.`id` = p.`id_brand`
				left join (
					select
						min(`price`) price,min(`pricev`) pricev,
						`id_product`
					from `{$t['pd']}_variations`
					where `price` > 0
					group by `id_product`
				) pv
					on p.`id` = pv.`id_product`
			where
				p.`status` = 1 and
				b.`status` = 1 and
				pd.`name_seo` = ? ",
			array( $_construct['item'] )
		)
){
		$page_info['brand_link'] = lnk(
			'brand',
			array(
				'page_name_seo' => isset($indispensable_pages_array['brands']['name_seo']) ? $indispensable_pages_array['brands']['name_seo'] : '',
				'name_seo' => $page_info['brand_name_seo']
			)
		);

		if(
			$page_info['pricep']<$page_info['pricev']
		){
			$page_info['price_old'] = $page_info['pricev'];
			$page_info['discount'] = round((($page_info['pricep']/$page_info['pricev'])*100)-100);
		}else{
			$page_info['price_old'] = $page_info['discount'] = '';
		}
		
		$page_info['cat_link'] = categories_link($page_info['id_parent']);
		$page_info['link'] = lnk( 'product', $page_info );

		$c['name'] = $engine->escape( $page_info['name'] );

		$page_info['header_title'] = $page_info['name'];
		$page_info['meta_keywords'] = $page_info['name'];
		$page_info['meta_description'] = $page_info['name'];
		$_construct['navigator'][] = "<li><span class=\"sprite\"></span><a href=\"{$page_info['link']}\" title=\"{$page_info['name']}\">".ucfirst($page_info['name'])."</a></li>";

		$db->query("update `{$t['pd']}` set `views`=`views`+1 where `id`={$page_info['id']}");

		$page_info['constructor'] = $_construct['page'] = 'product';

		$page_info['first_level_cat'] = $first_level_cat;

}
else{
	$_construct['page'] = 404;
//	print_a('test');
}
?>