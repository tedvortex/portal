<?php
if(
	!isset($_GET['lang'])
){
	$_GET['lang'] = 'ro';
}


$_SESSION['lang'] = $_GET['lang'];
$_SERVER['HTTP_HOST'] = str_replace( $_SESSION['lang'].'.', '', $_SERVER['HTTP_HOST'] );


if(
	$languages=
	$db->fetchAssoc("
		select sql_cache `token`,`name`,`regexp`,`timezone`,`locale`
		from `{$t['l']}`
		where `status`=1
	")
){
	foreach(
		$languages as
		$k => $l
	){
		$l['link'] = "http://{$l['token']}.{$_SERVER['HTTP_HOST']}";

		$languages[$k] = $l;
	}
}


date_default_timezone_set( $languages[$_SESSION['lang']]['timezone'] );
#$b = $languages[$_SESSION['lang']]['link'].'/';
$b = "http://{$_SERVER['HTTP_HOST']}/";
#define('_static','http://'.$config->static.'.'.str_replace( 'www.', '', $_SERVER['HTTP_HOST']).'/');
define('_static','http://'.$_SERVER['HTTP_HOST'].'/'.$config->static.'/');


setlocale(LC_ALL, $languages[$_SESSION['lang']]['locale']);
setlocale(LC_NUMERIC,'en_GB');
$textdomain = 'cms';
bindtextdomain( $textdomain, r.'+locales' );
textdomain( $textdomain );
?>