<?php
$currenciesAll = array('default' => 'RON');
$currenciesAllOmega = array('default' => 'RON');

if(
	$statement = $db->query("
		select `id`, `token`, `token_omega`, `exchange_rate`, `name`, `code`, `default`
		from `xp_currencies`
		where `status` = 1
		order by `default` desc
	")
){
	while( $c = $statement->fetch() ){
		
		if( $c['default'] ){
			$currenciesAll['default'] = $c['token'];
			$currenciesAllOmega['default'] = $c['token'];
		}
		
		$currenciesAll[$c['token']] = $c;
		$currenciesAllOmega[$c['token_omega']] = $c;
		
	}
}