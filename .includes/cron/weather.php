<?php
dl('pdo_mysql.so');

define('r',dirname(dirname(dirname(__FILE__))).'/');
include_once(r.'.includes/init.php');
include_once(i.'Xml/class.xml.php');
header('Content-type: text/html', true);

$c = get_web_page( 'http://weather.yahooapis.com/forecastrss?w=868274&u=c' );
$xml = xml2array( $c['content'] );

$weather = array();

foreach(
	$xml['rss']['channel'] as
	$item => $data
){
	$item = str_replace( array( 'yweather:', '_attr'), '', $item );

	if(
		!empty($data)
	){
		if(
			in_array(
				$item,
				array( 'location', 'units', 'wind', 'atmosphere', 'astronomy' )
			)
		){
			$weather ['geo'] [ $item ] = $data;
		}

		elseif(
			$item == 'item'
		){
			$weather ['condition'] = $data['yweather:condition_attr'];
			$weather ['condition']['geo:lat'] = $data['geo:lat'];
			$weather ['condition']['geo:long'] = $data['geo:long'];

			$weather ['forecast'][] = $data['yweather:forecast']['0_attr'];
			$weather ['forecast'][] = $data['yweather:forecast']['1_attr'];
		}
	}
}

$weather = serialize($weather);

if(
	!$channel =
	$db->fetchOne("
		select `id`
		from `config`
		where `key` = 'weather'
	")
){
	$db->insert(
		'config',
		array(
			'status' => 1,
			'type' => 3,
			'key' => 'weather',
			'value' => $weather
		)
	);
}

else{
	$db->query("
		update `config`
		set `value` = ?
		where `key` = 'weather' ",
		$weather
	);
}


echo 'completed without errors on: '. date('d.m.Y - H:i:s');
?>