<?php
dl('pdo_mysql.so');

define('r',dirname(dirname(dirname(__FILE__))).'/');
include_once(r.'.includes/init.php');
header('Content-type: text/plain', true);

// Trebuie sa iau XML-ul de pe bnr.ro si sa-l salvez in $curs;
$url  = 'http://www.bnro.ro/nbrfxrates.xml';

// Daca exista CURL, folosim CURL. Daca nu, folosim file_get_contents();
if( function_exists('curl_init') ) {
	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,$url);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	$curs = curl_exec($curl_handle);
	curl_close($curl_handle);
} else {
	$curs = file_get_contents($url);
}

// OK, avem fisierul nbrfxrates.xml. Acum trebuie sa il citim
// pentru asta avem nevoie de SimpleXML

if( !class_exists('SimpleXMLElement') ){
	// Daca nu suporta SimpleXML e naspa
	// Nu extind ca nu am chef sa ma complic inutil
	// Oricine vrea sa ma completeze e binevenit :-)
	echo 'Serverul nu suporta SimpleXML';
	exit;
}

// Daca am ajuns pana aici toate-s bune si ne putem pune pe treaba :-)
$rates = array();

$xml = new SimpleXMLElement($curs);
foreach( $xml->Body->Cube->Rate as $rate ){
	$final = array();
	foreach( $rate->attributes() as $att => $value ){
		if( strcmp($att,'currency') == 0 )   $final['currency']   = (string) $value;
		if( strcmp($att,'multiplier') == 0 ) $final['multiplier'] = (string) $value;
	}
	$final['rate'] = (string) $rate;
	if( empty($final['multiplier']) ) $final['multiplier'] = 1;
	array_push($rates,$final);
}

// Daca am ajuns aici, datele sunt pastrate in $rates
foreach (
	$rates as
	$rate
) {
	if(
		in_array(
			$rate['currency'],
			array('USD','EUR','GBP')
		)
	){
		$db->query("
			update `xp_currencies`
			set `exchange_rate` = ?
			where `code` = ? ",
			array( $rate['rate'], $rate['currency'] )
		);
	}
}

echo 'finished';
?>