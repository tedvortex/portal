<?php
dl('pdo_mysql.so');

define('r',dirname(dirname(dirname(__FILE__))).'/');
include_once(r.'.includes/init.php');
header('Content-type: text/html', true);

$continue = true;

#options parameters for SoapClient() request
$option = array(
	'compression' => 1,
	'trace' => 1,
	'connection_timeout' => 180
);

#try to connect with SoapClient
try {
	$Client = new SoapClient('http://test.xml.omegahotels.ro/OmegaHotelsWebService.wsdl', $option);
}
catch (Exception $e) {
	$continue = false;
}

#if SoapClient connection succeeded
if( $continue ){

	$statement = $db->query("
		select `id`, `no_order`, `statusOmega`, `reservationReferenceOmega`
		from `xp_orders`
		where
			`id_category` = 2 and
			`omegaInitiated` = 1 and
			`statusOmega` in ('Pending confirmation', 'Pending cancelation', 'Requires confirmation')
	");
	
	if( $statement ){
		
		while( $res = $statement->fetch() ){
			
			$continue = true;
			
			#if SoapClient() connection succeeded check the reservation to see the current status
			#possible cases are: Confirmed, Pending confirmation, Canceled, Pending cancelation, Reservation error and Requires confirmation
			$checkReservation = array(
				'PartnerCode' => 'mondialvoyages',
				'PlatformName' => 'mondialvoyages',
				'Username' => 'mondialvoyages',
				'Password' => 'gr$sd45Ds8vr',
				'Version' => 1,
				'ReservationReference' => $res['reservationReferenceOmega']
			);
		
			try {
				$checkReservationResultOmega = $Client->CheckReservation($checkReservation);
			}
			catch (SoapFault $Exception) {
				$continue = false;
			}

			#verify if CheckReservesion() method returned an error
			if( $continue && (
					isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) || 
					! isset($checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status)
				)
			){
				$continue = false;
			}
			
			#if CheckReservation() succeeded
			if( $continue && $res['statusOmega'] != $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status ){
				
				#save new status and datetime
				$db->query("
            		update `xp_orders`
            		set `statusOmega` = :statusOmega, `omegaLastUpdate` = :omegaLastUpdate
            		where `id` = :id_order ",
					array(
						'statusOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ReservationResponseCurrentVersion->Status,
						'omegaLastUpdate' => date('Y-m-d H:i:s', time()),
						'id_order' => $res['id']
					)
				);
				
				#save serialized CheckReselvation() method response
				$db->query("
            		update `xp_orders_products`
            		set `checkReservationCronResultOmega` = :checkReservationCronResultOmega
            		where `id_order` = :id_order ",
					array(
						'checkReservationCronResultOmega' => serialize($checkReservationResultOmega),
						'id_order' => $res['id']
					)
				);
			}
			
			#if $continue is false means that CheckReservation() has not been successfully completed
			if( ! $continue ){
			
				#set status of order with value 7, which means that it was a problem on cron job
				$db->query("
					update `xp_orders`
					set `status` = 7
					where `id` = ? ",
            		$res['id']
            	);
				
				#if recived an error on CheckReservation() method save it in database
				if( isset($checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage) ){
					 
					$db->query("
	            		update `xp_orders`
	            		set `errorMessageCheckReservationCronOmega` = :errorMessageCheckReservationCronOmega
	            		where `id` = :id_order ",
						array(
							'errorMessageCheckReservationCronOmega' => $checkReservationResultOmega->ReservationResponseReservationVersions->ErrorMessage,
							'id_order' => $res['id']
						)
					);
				}
			}
			
		}
		
	}
}