<?php
dl('pdo_mysql.so');

define('r',dirname(dirname(dirname(__FILE__))).'/');
include_once(r.'.includes/init.php');
header('Content-type: text/plain', true);


$url = "http://weather.yahooapis.com/forecastrss?w=868274&u=c";


if(
	function_exists('curl_init')
){
	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,$url);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);

	$weather_feed = curl_exec($curl_handle);

	curl_close($curl_handle);
}

else{
	$weather_feed = file_get_contents($url);
}


if(
	!$weather_feed
){
	exit('weather failed, check feed URL');
}

$weather = simplexml_load_string($weather_feed);


$channel_yweather = $weather->channel->children("http://xml.weather.yahoo.com/ns/rss/1.0");
foreach(
	$channel_yweather as
	$x => $channel_item
){
	foreach(
		$channel_item->attributes() as
		$k => $attr
	){
		$yw_channel[$x] = (string)$attr[0];
	}
}

$item_yweather = $weather->channel->item->children("http://xml.weather.yahoo.com/ns/rss/1.0");
foreach(
	$item_yweather as
	$x => $yw_item
){
	foreach(
		$yw_item->attributes() as
		$k => $attr
	){
		if(
			$k == 'day'
		){
			$day = $attr;
		}


		if(
			$x == 'forecast'
		){
			$yw_forecast[$x][$day . ''] = (string)$attr[0];
		}

		else{
			$yw_forecast[$x][$k] = (string)$attr[0];
		}
	}
}


$yw_channel = serialize( $yw_channel );
if(
	!$channel =
	$db->fetchOne("
		select `id`
		from `config`
		where `key` = 'yw_channel'
	")
){
	$db->insert(
		'config',
		array(
			'status' => 1,
			'type' => 3,
			'key' => 'yw_channel',
			'value' => $yw_channel
		)
	);
}

else{
	$db->query("
		update `config`
		set `value` = ?
		where `key` = 'yw_channel' ",
		$yw_channel
	);
}


$yw_forecast = serialize( $yw_forecast );
if(
	!$forecast =
	$db->fetchOne("
		select `id`
		from `config`
		where `key` = 'yw_forecast'
	")
){
	$db->insert(
		'config',
		array(
			'status' => 1,
			'type' => 3,
			'key' => 'yw_forecast',
			'value' => $yw_forecast
		)
	);
}

else{
	$db->query("
		update `config`
		set `value` = ?
		where `key` = 'yw_forecast' ",
		$yw_forecast
	);
}


echo 'finished';
?>