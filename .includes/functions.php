<?php

function mb_ucfirst($string,$encoding='utf-8') {
	if (function_exists('mb_strtoupper') && function_exists('mb_substr') && $string!='') {
		$string=mb_strtolower($string,$encoding);
		$upper=mb_strtoupper($string,$encoding);
		preg_match('#(.)#us',$upper,$matches);
		$string=$matches[1].mb_substr($string,1,mb_strlen($string,$encoding),$encoding);
	} else $string=ucfirst($string);
	return $string;
}

function mb_ucwords(
	$string
){
	return mb_convert_case($string, MB_CASE_TITLE);
}

/**
 * return user's ip address
 *
 * @returnstring
 */
function ip(){
	$ip = $_SERVER['REMOTE_ADDR'];

	if(
		getenv('HTTP_CLIENT_IP')
	){
		$ip = getenv('HTTP_CLIENT_IP');
	}

	elseif(
		getenv('HTTP_X_FORWARDED_FOR')
	){
		$ip = getenv('HTTP_X_FORWARDED_FOR');
	}

	elseif(
		getenv('HTTP_X_FORWARDED')
	){
		$ip = getenv('HTTP_X_FORWARDED');
	}

	elseif(
		getenv('HTTP_FORWARDED_FOR')
	){
		$ip=getenv('HTTP_FORWARDED_FOR');
	}

	elseif(
		getenv('HTTP_FORWARDED')
	){
		$ip=getenv('HTTP_FORWARDED');
	}

	return $ip;
}

/**
 * switch chars in a string a specific number of places
 *
 * @param string $s
 * @param int $n
 * @return string
 */
function str_rot(
	$s,
	$n=13
){
	$n = (int)$n%26;

	if(
		!$n
	){
		return $s;
	}

	for(
		$i=0, $l=strlen($s);
		$i<$l;
		$i++
	){
		$c=ord($s[$i]);

		if(
			$c>=97 &&
			$c<=122
		){
			$s[$i] = chr( ($c-71+$n) % 26 + 97 );
		}

		elseif(
			$c>=65 &&
			$c<=90
		){
			$s[$i] = chr( ($c-39+$n) % 26 + 65 );
		}
	}

	return $s;
}

/**
 * simple htmlentities
 *
 * @param string $string
 * @return encoded string
 */
function e(
	$string
){
	return htmlentities($string, ENT_COMPAT, 'UTF-8');
}

function get_include_contents(
	$filename
){
	if(
		is_file($filename)
	){
		ob_start();

		include_once( $filename );
		$contents = ob_get_contents();

		ob_end_clean();

		return $contents;
	}

	return null;
}

/**
 * get utf8 string from given iso-8859-2
 *
 * @param string $text
 * @return utf-8 text
 */
function utf8(
	$string
){
	return mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string, 'UTF-8, ISO-8859-2', true));
}

/**
 * limit string to number o chars
 *
 * @param string $string
 * @param int $max_length
 * @return unformatted length limtited string
 */
function limit_string(
	$string,
	$max_length
){
	if(
		mb_strlen($string) > $max_length
	){
		$string = mb_substr(trim(strip_tags($string)),0,$max_length).' ...';
	}

	return $string;
}

function limit_string2(
	$string,
	$max_length
){
	if(
		mb_strlen($string) > $max_length
	){
		$string = mb_substr(trim($string),0,$max_length).' ...';
	}

	return $string;
}
/**
 * javascript unescape ajax equivalent
 *
 * @param string $string
 * @return string
 */
function utf8_urldecode(
	$string
){
	$string = preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($string));

	return html_entity_decode($string, null, 'UTF-8');
}

/**
 * rawurlencode src strings - making slash exception
 *
 * @param string $image
 * @return string encoded
 */
function encode(
	$image
){
	$unenc = explode('/',$image);

	foreach(
		$unenc as
		$k=>$v
	){
		$unenc[$k] = rawurlencode($v);
	}

	return implode('/', $unenc);
}

/**
 * prefix + md5(string) +suffix
 *
 * @param string $string
 * @param string $prefix
 * @param string $suffix
 * @return md5
 */
function md7(
	$string,
	$prefix='xp_',
	$suffix='_21'
){
	return md5($prefix.$string.$suffix);
}

/**
 * <pre> $string </pre>
 *
 * @param string $string
 */
function print_a( $string ){
    $acceptedIps = array(
		'89.33.79.179',
    	'89.33.77.173'
    );

	//if( in_array(ip(), $acceptedIps) ){
		echo "<pre class=\"code\"><code>";
		print_r($string);
		echo "</code></pre>";
 	//}
}

/**
 * internal aprint function
 *
 * @param array $arr
 * @param int $tab
 * @return string
 */
function _aprint(
	$arr,
	$tab=1
) {
	if(
		!is_array($arr)
	){
		return '<span class="array">'.ucfirst(gettype($arr)).'</span>'._slashes($arr);
	}

	$space=str_repeat("\t",$tab);

	$out='<span class="array">array</span>('.PHP_EOL;

	end($arr);

	$end=key($arr);

	if(
		count($arr)==0
	){
		return "<span class=\"array\">array()</span>";
	}

	foreach(
		$arr as
		$key=>$val
	){
		$colon=($key==$end?'':',');


		if(
			!is_numeric($key)
		){
			$key='<span class="key">\''.str_replace(array("\\","'"),array("\\\\","\'"),htmlspecialchars($key)).'\'</span>';
		}

		else{
			$key='<span class="value">'.$key.'</span>';
		}


		if(
			is_array($val)
		){
			$val=_aprint($val,($tab+1));
		}

		else {
			if(
				!is_numeric($val)
			){
				$val='<span class="key">\''.str_replace(array("\\","'"),array("\\\\","\'"),htmlspecialchars($val)).'\'</span>';
			}

			else{
				$val='<span class="value">'.$val.'</span>';
			}
		}

		$out.=$space.$key.' => '.$val.$colon.PHP_EOL;
	}


	return ($tab==1?$out.');':$out.$space.'<span class="array">)</span>');
}

/**
 * generate php array declaration string from array
 *
 * @param array $arr
 * @param string $prefix
 */
function aprint(
	$arr,
	$prefix=''
){
	if(
		ltrim($prefix)!=''
	){
		$prefix='<span class="array">'.$prefix.'</span> =';
	}

	echo
		PHP_EOL.
		PHP_EOL.
		'<div class="aprint">
			<table>
				<tbody>
					<tr>
						<td>
							<pre>'.$prefix._aprint($arr).'</pre>
						</td>
					</tr>
				</tbody>
			</table>
		</div>'.
		PHP_EOL.
		PHP_EOL;
}

/**
 * paginate with custom url keeping get parameters
 *
 * @param int $nr_products
 * @param int $current_page
 * @param int $items_per_page
 * @param mixed $url_string
 * @param int $links_displayed
 * @return unwrapped page navigation elements
 */
function pagination(
	$nr_products,
	$current_page,
	$items_per_page,
	$url_string,
	$get_array='',
	$links_displayed=10
){
	$return=$get_string='';

	if(!empty($get_array))
		foreach ($get_array as $g)
			$get_string.=($get_array[0]==$g?'?':'&').$g.'='.$db->escape($_GET[$g]);

	if(!preg_match('%◬%',$url_string))
		$url_string.='◬';


	#generate number display
	$max_pages=floor($nr_products/$items_per_page);

	if(($nr_products%$items_per_page)!=0)
		$max_pages++;

	$page_start=1;
	$page_end=$max_pages;
	#end generate number display


	#generate start/end page
	if($max_pages>$links_displayed) {
		$page_start=$current_page-floor(($links_displayed-1)/2);

		if($page_start<1)
			$page_start=1;

		$page_end=$current_page+floor(($links_displayed-1)/2);

		if ($page_end>$max_pages)
			$page_end=$max_pages;
	}
	#end generate start/end page


	#generate elements
	if ($current_page>1)
		$return.="<a href=\"".mb_ereg_replace('◬','pagina-'.($current_page-1),$url_string)."\">&#171;</a>";

	for ($i=$page_start;$i<=$page_end;$i++)
		$return.="&#160;<a href=\"".mb_ereg_replace('◬','pagina-'.$i,$url_string)."\" class=\"".($current_page==$i?'a':'')."\">{$i}</a>";

	if($current_page<$max_pages)
		$return.="&#160;<a href=\"".mb_ereg_replace('◬','pagina-'.($current_page+1),$url_string)."\">&#187;</a>";
	#end generate elements


	return $return;
}

function pagination_personalize(
	$mod,
	$nr_products,
	$current_page = 1,
	$items_per_page = 45
){
	$return = '';
	$links_displayed = 5;

	if(
		in_array( $mod, array('template','image') ) &&
		$nr_products > $items_per_page
	){

		#generate number display
		$max_pages=floor($nr_products/$items_per_page);

		if(($nr_products%$items_per_page)!=0)
			$max_pages++;

		$page_start=1;
		$page_end=$max_pages;
		#end generate number display

		#generate start/end page
		if($max_pages>$links_displayed) {
			$page_start=$current_page-floor(($links_displayed-1)/2);

			if($page_start<1)
				$page_start=1;

			$page_end=$current_page+floor(($links_displayed-1)/2);

			if ($page_end>$max_pages)
				$page_end=$max_pages;
		}
		#end generate start/end page

		//print_a('page start: '.$page_start);
		//print_a('page end: '.$page_end);

		#generate elements
		if ($current_page>1)
			$return.="<a class=\"change-query\" data-page=\"".($current_page-1)."\" data-mod=\"".$mod."\" data-type=\"page\">&#171;</a>";

		for ($i=$page_start;$i<=$page_end;$i++)
			$return.="<a class=\"change-query".( $current_page == $i ? ' a' : "" )."\" data-page=\"".$i."\" data-mod=\"".$mod."\" data-type=\"page\">".$i."</a>";

		if($current_page<$max_pages)
			$return.="<a class=\"change-query\" data-page=\"".($current_page+1)."\" data-mod=\"".$mod."\" data-type=\"page\">&#187;</a>";
		#end generate elements
	}

	return $return;
}



function decimal(
	$num,
	$sep='.',
	$sig=2
){
	$num = number_format( str_replace(',','',$num), $sig+1, $sep, '' );

	if(
		!strpos($num,'.')
	){
		$num.='.';
	}

	$ext=explode('.',$num);
	$decimal=$ext[1];
	$ext='';

	for(
		$i=strlen($decimal);
		$i<$sig;
		$i++
	){
		$ext.='0';
	}

	$num.=$ext;

	$num=substr($num,0,((strpos($num,$sep)+1)+$sig));


	return $num;
}

function decimal_remove(
	$num,
	$sep = '.'
){


	$num = str_replace(".","",decimal($num, $sep, 0));

	return $num;
}

function image_uri(
	$image
){
	global $browser;

	$_image = s.$image;

	if(
		!(
			is_file($_image) &&
			file_exists($_image)
		) ||

		(
			$browser->engine=='ie' &&
			floatval($browser->engine_version) <= 8
		)
	){
		return _static.$image;
	}

	else{
		$pathinfo = pathinfo($_image);

		$file=
		array(
			'pathinfo'=>$pathinfo,
			'imagetype'=>strtolower($pathinfo['extension']),
			'imagedata'=>base64_encode( file_get_contents($_image) ),
		);

		//return 'data:image/'.$file['imagetype'].';base64,'.$file['imagedata'];
		return _static.$image;
	}
}

function return_page_info(
	$table,
	$name,
	$condition = 'and a.`status`=1',
	$linked_tables = true,
	$connector = 'name_seo',
	$fields = '*,b.`_id` id'
){
	global $db;

	if(
		$linked_tables
	){
		return
		$db->fetchRow("
			select {$fields}
			from
				`{$table}` a
				inner join `{$table}_data` b
					on a.`id` = b.`_id`
			where
				`{$connector}` = ?
				{$condition}
			",
			array(
				$name
			)
		);
	}

	return
	$db->fetchRow("
		select {$fields}
		from `{$table}` a
		where
			`{$connector}` = ?
			{$condition}
		",
		array($name)
	);
}

function url(
	$string,
	$lang = null,
	$replace = '-',
	$regexp = null,
	$encode = true
){
	global $languages;

	$lang = ( $lang == null && !empty($_SESSION['lang']) ? $_SESSION['lang'] : $lang );

	$regexp = ( $regexp == null && isset($languages[$lang]['regexp']) ? $languages[$lang]['regexp'] : $regexp );

	$string = mb_strtolower(mb_ereg_replace("[^a-zA-Z0-9{$regexp}]+", $replace, trim($string)));
	$string = mb_ereg_replace( "{$replace}$" , '', $string );
	$string = mb_ereg_replace( "^{$replace}", '', $string );

	if(
		$encode
	){
		$string = rawurlencode( $string );
	}

	return $string;
}

function beautify(
	$string,
	$separator = 'em',
	$direction = 1
){
	$regexp = array( '^(\w+)', '(\w+)$', '^((?:\w+\s)+)', '((?:\s\w+)+)$' );

	return mb_ereg_replace( $regexp[$direction], "<{$separator}>\\1</{$separator}>", $string);
}

function update_languages(
	&$languages,
	$id,
	$table
){
	global $db;

	if(
		is_array($languages)
	){
		reset($languages);

		foreach(
			$languages as
			&$l
		){
			$alternate_link = $db->fetchOne("select `name_seo` from `{$table}_data` where `_id` = ? and `lang` = ? ", array($id, $l['token']));

			$l['link'] .= '/'.url( ( $alternate_link != 'index' ? $alternate_link : '' ), $l['token'] );
		}
	}

	return false;
}


function sendHTMLemail(
	$femail = '',
	$fname = '',
	$to,
	$subject,
	$message,
	$remail = null,
	$attachments = null
){
	global $config;

	$remail = ( !$remail ? $config->reply_email : $remail );
	$femail = ( !$femail ? $config->site_email : $femail );
	$fname = ( !$fname ? $config->email_name : $fname );

	$transport 	= Swift_MailTransport::newInstance();
	$mailer 	= Swift_Mailer::newInstance($transport);

	$message=
	Swift_Message::newInstance($subject)
		->setFrom(array($femail=>$fname))
		->setReplyTo($remail)
		->setTo((array)$to)
		->setDate(time())
		->setBody($message,'text/html')
		->addPart(strip_tags(str_replace("<br/>","\r\n",$message)),'text/plain');

	if(
		!empty($attachments)
	){
		foreach(
			$attachments as
			$a
		){
			$message->attach(Swift_Attachment::fromPath($a));
		}
	}

	$result=$mailer->send($message);

	return($result?true:false);
}

function template_mail(
	$template,
	$emails,
	$vars
){
	global $db,$t;

	if(
		!isset( $vars['attachment'] )
	){
		$vars['attachment'] = '';
	}

	if(
		$date_email =
		$db->fetchRow("
			select *
			from
				`xp_emails` e
				inner join `xp_emails_data` d
					on e.`id` = d.`_id`
			where
				e.`template` = ? and
				d.`lang` = ?",
			array( $template, $_SESSION['lang'] )
		)
	){
		foreach(
			$vars as
			$key=>$value
		){
			$date_email['content'] = str_replace( "[{$key}]", $value, $date_email['content'] );
			$date_email['subject'] = str_replace( "[{$key}]", $value, $date_email['subject'] );
		}

		if(
			!is_array($emails)
		){
			$emails = explode(",",$emails);
		}

		foreach(
			$emails as
			$email
		){
			sendHTMLemail(
				$date_email['sender'],
				$date_email['sender_name'],
				$email,
				$date_email['subject'],
				$date_email['content'],
				'',
				$vars['attachment']
			);
		}

		return true;
	}

	return false;
}

function steps_unset(
	$step
){
	for(
		$i = $step;
		$i <= 4;
		$i++
	){
		unset($_SESSION['order']['step-'.$i]);

		$_SESSION['order']['step'] = ( $_SESSION['order']['step'] > ( $step - 1 ) ? ( $step - 1 ) : $_SESSION['order']['step'] );
	}
}

/**  * translate Function
  *
  * @param    string   Text which should be translated
  * @param    array    Params with Vars which should be replaced (enclosed with {})
  *
  * @return   string   Translated Text
  */
function tr(
	$string,
	array $params = array()
){
	global $db, $t;

	$lang = 'en';

	$key = limit_string( url( $string, $lang, '_', null, false ), 666 );
	$trans = $string;

	/*if(
		!$trans =
		$db->fetchOne("
			select `translation`
			from
				`{$t['translation']}` t
				inner join `{$t['translation']}_data` td
					on t.`id` = td.`_id`
			where
				t.`key` = ? and
				td.`lang` = ? ",
			array( $key, $_SESSION['lang'] )
		)
	){
		$trans = $string;

		if(
			!$id = $db->fetchOne("
				select `id`
				from `{$t['translation']}`
				where `key` = ? ",
				$key
			)
		){
			$db->insert(
				$t['translation'],
				array(
					'date' 	=> time(),
					'key' 	=> $key
				)
			);

			$id = $db->lastInsertId();
		}

		$db->insert(
			$t['translation'].'_data',
			array(
				'_id' 	=> $id,
				'lang' 	=> $_SESSION['lang'],
				'translation' => $string
			)
		);
	}*/

	if(
		!count($params)
	){
		return $trans;
	}

	foreach(
		array_keys($params) as
		$element
	){
		$search[] = '{' . $element . '}';
	}

	return str_replace( $search, $params, $trans );
}

function lnk(
	$module,
	$page_info
){
	$link = array( 'http://' . $_SERVER['HTTP_HOST'] );

	if(
		$module == 'pages'
	){
		$link[] = url($page_info['name_seo']);
	}

	elseif(
		$module == 'sejur'
	){
		$link = array(
		    url($page_info['name_seo']));
	}

	elseif(
		$module == 'category'
	){
		$link[] = url($page_info['name_seo']);
	}

	return implode ('/', $link);
}

function watermark(
	$image_file,
	$watermark
){
	list($width, $height, $ftype) = getimagesize($image_file);
	switch ($ftype) {
		case IMAGETYPE_GIF: $image = imagecreatefromgif($image_file);break;
		case IMAGETYPE_JPEG: $image = imagecreatefromjpeg($image_file);break;
		case IMAGETYPE_PNG: $image = imagecreatefrompng($image_file);break;
	}
	//die($image_file);
	$image_w=imagecreatefrompng( $watermark );

	$watermark_size=($width>$height?$height:$width)/2;

	imagealphablending($image_w,true);
	imagesavealpha($image_w,true);

	imagealphablending($image,true);
	imagesavealpha($image,true);

	imagecopyresampled($image,$image_w,($width-$watermark_size)/2,($height-$watermark_size)/2,0,0,$watermark_size,$watermark_size,600,600);
	imagejpeg($image,$image_file , 100);
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function write_tourists_row( $order, $is_contact_persons = false, $is_child = false, $age = 0, $values = array() ){

	$name = $is_child ? 'children' : 'adults';

	$v = isset($values[$order]) ? $values[$order] : array();

	$return =
	'<div class="tourist-row">
		<h2>'.( $order == 0 && !$is_child ? 'Detaliile primului turist' : 'Detalii turist '.($order+1)).' - '.($is_child ? 'copil' : 'adult').' '.($is_contact_persons ? '(Acesta va fi si persoana de contact)' : '').'</h2>
		<ul class="cf nav">
			<li class="fl no-label">
				<select name="'.$name.'['.$order.'][title]">
					'.( $is_child ?
					'<option value="3"'.($v && $v['title'] == 3 ? ' selected="selected"' : '').'>Chd.</option>'
					:'<option value="0"'.($v && $v['title'] == 0 ? ' selected="selected"' : '').'>Dl.</option>
					<option value="1"'.($v && $v['title'] == 1 ? ' selected="selected"' : '').'>D-na.</option>
					<option value="2"'.($v && $v['title'] == 2 ? ' selected="selected"' : '').'>D-ra.</option>'
					).'
				</select>
			</li>
			<li class="fl">
				<label for="'.$name.'-lastname-'.$order.'" class="order-label">Nume <span>*</span></label>
				<input id="'.$name.'-lastname-'.$order.'" type="text" name="'.$name.'['.$order.'][lastname]" value="'.($v ? $v['lastname'] : '').'" required="required" />
			</li>
			<li class="fl">
				<label for="'.$name.'-firstname-'.$order.'" class="order-label">Prenume <span>*</span></label>
				<input id="'.$name.'-firstname-'.$order.'" type="text" name="'.$name.'['.$order.'][firstname]" value="'.($v ? $v['firstname'] : '').'" required="required" />
			</li>
			<li class="fl birthdate">
				<div class="help cf">
					<label class="order-label fl" for="'.$name.'-birthdate-'.$order.'">Data nasterii <span>*</span></label>
					'.( $is_child ?
					'<strong class="open-circle open-info n">i</strong>
					<strong class="open-circle open-help">?</strong>
					<div class="open-box info-box">
						<span class="triangle"><span></span></span>
						La data cazarii varsta copilului va depasii varsta aleasa de dvs. pentru acesta. Este posibil ca acest lucru sa modifice pretul rezervarii dvs. Va sugeram sa reveniti la pasul anterior pentru a alege varstele corespunzatoare!
					</div>
					<div class="open-box help-box">
						<span class="triangle"><span></span></span>
						Varsta copilului la check-in trebuie sa fie mai mica de '.($age+1).' an(i)
					</div>' : ''
					).'
				</div>
				<div>
					<input id="'.$name.'-birthdate-'.$order.'" class="order-datepick'.($is_child ? ' child' : '').'" type="text" name="'.$name.'['.$order.'][birthdate]" value="'.($v ? $v['birthdate'] : '').'" required="required" data-age="'.$age.'" />
				</div>
			</li>
		</ul>
	</div>';

	return $return;
}

function write_tourists_form( $adults, $children_ages, $values = array() ){

	$return = '';

	$order = -1;
	for( $i = 0 ; $i < $adults; $i++ ){
		$return .= write_tourists_row(++$order, ($i == 0 ? true : false), false, 0, (isset($values['adults']) ? $values['adults'] : array()) );
	}

	$order = -1;
	if( $children_ages ){
		foreach($children_ages as $age){
			$return .= write_tourists_row(++$order, false, true, $age, (isset($values['children']) ? $values['children'] : array()));
		}
	}

	return $return;
}

function return_html_mail_list( $list, $name = '' ) {
	$html = '';

	if( $name ){
		$html .= '<h1>'.$name.'</h1>';
	}

	if( $list ){
		$html .=
		'<table style="font-family: arial,sans-serif;" border="0" cellspacing="0" cellpadding="0">
			<tbody>';

		foreach( $list as $name => $value ){
			$html .= '<tr><td style="padding:5px 10px">'.$name.':</td><td style="padding:5px 10px">'.$value.'</td></tr>';
		}

		$html .=
		'
			</tbody>
		</table>
		';
	}

	return $html;
}

function return_html_mail_tourists($adults = array(), $children = array()){

	$html = '';
	$titles = array( 0 => 'Dl.', 1 => 'D-na.', 2 => 'D-ra.');

	if( $adults ){

		$html .=
		'<h1>Adulti</h1>
		<table style="font-family: arial,sans-serif; width: 80%;" border="0" cellspacing="0" cellpadding="0">
			<tbody>
				<tr style="background-color:#eee;text-align:left;">
					<th style="padding:10px"></th>
					<th style="padding:10px">Nume</th>
					<th style="padding:10px">Prenume</th>
					<th style="padding:10px">Data nasterii</th>
				</tr>
		';

		foreach($adults as $a){
			$html .=
			'<tr>
				<td style="width:100px;padding:5px 10px;border-bottom:1px solid #ddd">'.$titles[$a['title']].'</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['lastname'].'</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['firstname'].'</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['birthdate'].'</td>
			</tr>';
		}

		$html .=
		'
			</tbody>
		</table>
		';

	}

	if( $children ){

		$html .=
		'<h1>Copii</h1>
		<table style="font-family: arial,sans-serif; width: 80%;" border="0" cellspacing="0" cellpadding="0">
			<tbody>
				<tr style="background-color:#eee;text-align:left;">
					<th style="padding:10px"></th>
					<th style="padding:10px">Nume</th>
					<th style="padding:10px">Prenume</th>
					<th style="padding:10px">Data nasterii</th>
				</tr>
		';
	
		foreach($children as $a){
			$html .=
			'<tr>
				<td style="width:100px;padding:5px 10px;border-bottom:1px solid #ddd">Chd.</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['lastname'].'</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['firstname'].'</td>
				<td style="padding:5px 10px;border-bottom:1px solid #ddd">'.$a['birthdate'].'</td>
			</tr>';
		}

		$html .=
		'
			</tbody>
		</table>
		';
	}

	return $html;
}

function age($date, $today = '', $today_format = 'd.m.Y' ){

	if( $today != ''){
		$present = DateTime::createFromFormat( $today_format.' H:i:s', $today.' 00:00:00' );
	}
	else{
		$present = DateTime::createFromFormat('d.m.Y H:i:s', date('d.m.Y H:i:s',time()));
	}

	$birthdate = DateTime::createFromFormat('d.m.Y H:i:s', $date.' 00:00:00' );

	if( $interval = $birthdate->diff($present) ){

		if( $interval->invert == 0 ){
			//print_a($present->format('d.m.Y H:i:s'). ' - '.$birthdate->format('d.m.Y H:i:s') );
			//print_a($interval);

			if( $interval->y == 0 ){
				return 1;
			}
			else{
				return $interval->y;
			}
		}
	}

	return 0;
}

function filters_url ($filter = array(), $sfilters = array(), $suffix = '/filtru/', $pretender = ':', $separator = ',', $glue = ';')
{
    $url = array();

    if (is_array($sfilters) && ! empty($sfilters)) {
        foreach ($sfilters as $f) {
            $url[$f['name']] = filter_url($f, $pretender, $separator);
        }
    }

    if (! empty($url)) {
        if (! empty($filter)) {
            $filter['name'] = url($filter['name']);
            $filter['options'][0] = url($filter['options'][0]);

            if (isset($url[$filter['name']])) {
                $temp = explode($separator, preg_replace('%^[a-z0-9\-]+' . $pretender . '%', '', $url[$filter['name']]));
                $coex = true;

                foreach ($temp as $k => $exist) {
                    if ($exist == $filter['options'][0]) {
                        $coex = false;

                        unset($temp[$k]);
                    }
                }

                $url[$filter['name']] = (sizeof($temp) > 0 ? $filter['name'] . $pretender : '') .
                implode($separator, $temp);

                if (empty($url[$filter['name']])) {
                    unset($url[$filter['name']]);
                }

                if (! $coex) {
                    $url = implode($glue, $url);
                }

                else {
                    $temp[] = $filter['options'][0];

                    $url[$filter['name']] = $filter['name'] . $pretender . implode($separator, $temp);

                    $url = implode($glue, $url);
                }
            }

            else {
                $append = filter_url($filter, $pretender, $separator);

                $url = implode($glue, $url);

                if (mb_strlen($url) > 0) {
                    $url .= $glue . $append;
                }

                else {
                    $url .= $append;
                }
            }
        }
    }

    else {
        if (! empty($filter)) {
            $url = filter_url($filter, $pretender, $separator);
        }
    }

    if (is_array($url)) {
        $url = implode($glue, $url);
    }

    if (mb_strlen($url) > 0) {
        return $suffix . $url;
    }

    return false;
}

function filter_url ($f, $pretender = ':', $separator = ',')
{
    if (isset($f['options']) && is_array($f['options']) && ! empty($f['options'])) {
        $options = array();

        foreach ($f['options'] as $o) {
            $options[] = url($o);
        }

        $url = url($f['name']) . $pretender . implode($separator, $options);

        return $url;
    }

    return false;
}

function generate_random_string( $length = 6, $prefix = '', $sep = '-'){

	$code = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

	if( $prefix ){
		return $prefix.$sep.$code;
	}

	return $code;
}

/**
 * Return value in all currencies from xp_currencies and exchange rates used.
 * In calculation is accepted a commission.
 * $value is value to exchange
 * $currency is current currency of $value
 * $commission is the percentage amount of the commission
 * $type represent type of token
 * @param double $value
 * @param string $currency
 * @param double $commission
 * @param string $type
 * @return multitype:number
 */
function exchange($value, $currency, $commission = 0, $type = 'omega',$decimals=3,$round='ceil'){
	
	global $currenciesAll, $currenciesAllOmega;
	
	$return = array(
		'params' => array(
			'value' => $value,
			'currency' => $currency,
			'commission' => $commission	
		),
		'default' => $currenciesAllOmega['default'],
		'exchange_rates' => array(),
		'exchange' => array(),
		'exchange_commissioned' => array()	
	);
	
	$currencies = $type == 'omega' ? $currenciesAllOmega : $currenciesAll;
	
	$default_value = $value * $currencies[$currency]['exchange_rate'];
	
	foreach( $currencies as $token => $c ){
		
		if( $token != 'default' ){
			$return['exchange_rates'][$token] = $c['exchange_rate'];
			
			$exchange = $default_value / $c['exchange_rate'];
			if ($round==false) {
				$return['exchange'][$token] = number_format($exchange, $decimals, '.', '');
				$return['exchange_commissioned'][$token] = number_format( $exchange + $exchange * $commission , $decimals, '.', '');
			} elseif ($round=='ceil') {
				$return['exchange'][$token] = ceil($exchange);
				$return['exchange_commissioned'][$token] = ceil( $exchange + $exchange * $commission );
			} elseif ($round=='floor') {
				$return['exchange'][$token] = floor($exchange);
				$return['exchange_commissioned'][$token] = floor( $exchange + $exchange * $commission );
			}
			
		}
	}
	
	return $return;
}

/**
 * Return commissioned value acording with module. 
 * If $commission is greater than 1 will use his value for calculation, not the module commission.
 * @param double $value
 * @param string $module
 * @param number $commission
 * @param number $decimals
 */
function commission($value, $module = 'omega', $commission = 1, $decimals = 3){
	
	if( $commission <= 1 ){
		
		global $config;
		
		if( $module == 'omega' ){
			$commission = $config->comision_omega;
		}
	}
	
	return number_format($value * $commission, $decimals, '.', '');
}