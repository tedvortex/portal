<?php
define('i',r.'.includes/');
define('init',r.'.includes/init/');
define('p',r.'.includes/Page/');
define('t',r.'_template/');
define('c',r.'_content/');
define('s',r.'static/');

set_include_path(i);

setlocale(LC_TIME,"ro_RO");
setlocale(LC_ALL,"ro_RO");

#require_once(init.'gd.php');
require_once(init . 'config.php');
require_once(init . 'session.php');
require_once(init . 'db.php');
require_once(init . 'template.php');
require_once(init . 'language.php');
require_once(init . 'currency.php');
require_once(init . 'swift.php');
require_once(init . 'form.php');
require_once(init . 'user.php');
#require_once(init . 'shopcart.php');
#require_once(init . 'ae.php');

require_once(i . 'functions.php');

$config->setReadOnly();