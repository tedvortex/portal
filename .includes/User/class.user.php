<?php
class NSS_user {
	private $table = '';
	private $db;
	public $ses_name = 'default_session';
	public $u_info = array();
	public $logat = false;
	public $id=0;

	function NSS_user(
		$ses_name = null,
		$table = null,
		$db
	){
		$this->db = $db;

		if(
			isset($ses_name) &&
			$ses_name != ''
		){
			$this->ses_name = $ses_name;
		}

		if(
			isset($table) &&
			$table != ''
		){
			$this->table=$table;
		}

		if(
			!isset($_SESSION[$ses_name])
		){
			$_SESSION[$ses_name] = 0;
		}

		if(
			$this->logat=
			$this->e_logat()
		){
			$this->id = (int)$_SESSION[$ses_name];
			self::get_data();
		}
	}

	private function get_data(){
		if(
			$this->e_logat()
		){
			if(
				$this->u_info=
				$this->db->fetchRow("
					select *
					from `{$this->table}`
					where id = ?",
					$_SESSION[$this->ses_name]
				)
			){
				unset( $this->u_info['password'] );

				$this->id = $this->u_info['id'];
			}
		}
	}


	function login(
		$email,
		$password,
		$ttl=60,
		$need_actived=true
	){
		if(
			!empty($email) &&
			!empty($password) &&
			$info=
			$this->db->fetchRow("
				select `id`, `status`
				from `{$this->table}`
				where
					`email` = ? and
					`password`= ? ",
				array($email,md7($password))
			)
		){
			if(
				$info['status'] == 1 ||
				(
					$info['status'] == 0 &&
					!$need_actived
				)
			){
				
				$_SESSION[$this->ses_name] = $info['id'];
				$_SESSION['ttl'] = $ttl;
				$_SESSION['last_action'] = time();

				$this->id = (int)$_SESSION[$this->ses_name];
				$this->logat = true;
				self::get_data();

				return 'success';
			}

			elseif(
				$info['status'] == 0
			){
				return 'information';
			}

			elseif(
				$info['status'] == 2
			){
				return 'alert';
			}
		}

		return 'error';
	}

	function logout(){
		if(
			$this->logat === true
		){
			unset( $_SESSION[$this->ses_name] );
			$this->logat = false;

			return 'Ati fost delogat!';
		}

		else{
			return 'Sunteti deja delogat!';
		}
	}

	function e_logat() {
		if(
			!empty($this->u_logat)
		){
			return $this->u_logat;
		}

		if(
			isset( $_SESSION[$this->ses_name] ) &&
			!empty( $_SESSION[$this->ses_name] )
		){
			if(
				$exist=
				$this->db->fetchRow("
					select `id`
					from `{$this->table}`
					where id= ?",
					$_SESSION[$this->ses_name]
				)
			){
				$this->u_logat = true;

				return $this->u_logat;
			}
		}

		return false;
	}

	function info(
		$key=''
	){
		if(
			isset($this->u_info) &&
			!empty($this->u_info)
		){
			if(
				isset($this->u_info[$key]) &&
				!empty($key)
			){
				return $this->u_info[$key];
			}

			return $this->u_info;
		}

		elseif(
			$this->logat
		){
			$this->u_info=
			$this->db->fetchRow("
				select *
				from `{$this->table}`
				where `id`= ? ",
				$_SESSION [$this->ses_name]
			);

			return ( isset($this->u_info[$key]) && !empty($key) ? $this->u_info[$key] : 	$this->u_info );
		}

		return false;
	}
}
?>