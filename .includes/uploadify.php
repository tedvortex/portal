<?php
if( !defined('r') ){
    define('r',dirname(dirname(__FILE__))).'/';
}

if( isset( $_POST['session_id'] ) ){
    session_write_close();
    session_id($_POST['session_id']);
    session_start();

    require( 'init/user.php' );
}

$errors = array(
	UPLOAD_ERR_OK,
	UPLOAD_ERR_INI_SIZE,
	UPLOAD_ERR_FORM_SIZE,
	UPLOAD_ERR_PARTIAL,
	UPLOAD_ERR_NO_FILE,
	UPLOAD_ERR_NO_TMP_DIR,
	UPLOAD_ERR_CANT_WRITE,
	UPLOAD_ERR_EXTENSION
);

$folder = '.tmp_img';
$fileTypes = array('jpg','jpeg','gif','png', 'doc', 'docx', 'pdf');

if( !empty($_FILES) ){
    $fileParts = pathinfo($_FILES['Filedata']['name']);

    if( $_FILES['Filedata']['error'] > 0 ){
    	exit( $errors[ $_FILES['Filedata']['error'] ] );
    }

    if (in_array($fileParts['extension'],$fileTypes)) {
	    $fileName = "{$user->id}-{$_POST['session_id']}-{$_POST['number']}.{$fileParts['extension']}";

		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = r . $folder;
		$targetFile = $targetPath . '/' . $fileName;

		move_uploaded_file($tempFile,$targetFile);
		exit( $fileName );
	}

	exit('Invalid file type.');
}