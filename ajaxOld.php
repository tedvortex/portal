<?php
header("X-Content-Type-Options: nosniff", true);
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false); // required for certain browsers
header("Pragma: no-cache");
header("Expires: 0");

require_once (i . 'Turbine/lib/browser/browser.php');

$browser = new Browser();
$browser->parse();

$h = $f = '';
$return = array(
        'error' => 'Invalid request'
);

if (isset($_GET['callback']) && $_GET['callback'] != '') {
    $_GET['callback'] = stripcslashes($_GET['callback']);
    $h = $_GET['callback'] . '(';
    $f = ')';
}

if (isset($_GET['module'])) {
    $mod = $_GET['module'];

    if ($mod == 'window') {
    	$vars = array();
    	if( isset($_GET['vars']) && $_GET['vars'] ){
    		$get = explode('&', $_GET['vars']);

    		if($get){
    			foreach($get as $v){
    				$split = explode(':', $v);

    				if( count($split) == 2 && $split[0] && $split[1] ){
    					$vars[$split[0]] = $split[1];
    				}
    			}
    		}
    	}

    	if ($parent = $db->fetchOne("
				select `id`
				from `{$t['w']}`
				where `constructor` = ?", $_GET['action'])) {
            if ($message = $db->fetchRow("
					select  	d.`name` header, d.`description` message
					from
						`{$t['w']}` c
						inner join `{$t['w']}_data` d
							on c.`id` = d.`_id`
					where
						`constructor` = ? and
						`id_parent` = ? and
						`lang` = ? ",
                    array(
                            $_GET['type'],
                            $parent,
                            $_SESSION['lang']
                    )
            )) {
				if( !empty($vars) ){
					foreach($vars as $parameter => $value){
						$message['message'] = str_replace('['.$parameter.']', $value, $message['message']);
					}
				}
            	$return = $message;
            }
        }
    }

    elseif ($mod == 'fibula-city'
            && isset($_GET['value'])
            && is_numeric($_GET['value']))
    {
        $statement = $db->query("
                select c.`id`, cd.`name`
                from
                    `fibula_countries` c
                    inner join `fibula_countries_data` cd
                        on c.`id` = cd.`_id`
                where
                    cd.`lang` = ?
                    and c.`id_parent` = ?
                    and c.`status` = 1
                    and (
                        select count(`id`)
                        from `fibula_hotels` h
                        where `h`.`id_region` = `c`.`id`
                    ) > 0
	            order by cd.`name`",
                array('ro', $_GET['value']));

        if ($statement) {
            $return = array(
                'success' => true,
                'items' => array()
            );

            while ($row = $statement->fetch()) {
                $return['items'][] = $row;
            }
        }
    }

    elseif ($mod == 'fibula-hotel'
            && isset($_GET['value'])
            && is_numeric($_GET['value']))
    {
        $statement = $db->query("
			select c.`id`, cd.`name`, c.`rating`
			from
				`fibula_hotels` c
				inner join `fibula_hotels_data` cd
					on c.`id` = cd.`_id`
			where
				c.`id_region` = ?
			order by cd.`name` asc",
                array($_GET['value']));

        if ($statement) {
            $return = array(
                'success' => true,
                'items' => array(
                    0 => array(
                        'id' => 0,
                        'name' => tr('Alege hotel')
                    )
                )
            );

            while ($row = $statement->fetch()) {
            	if (
	   				$row['rating'] == '5.00' ||
	   				$row['rating'] == '4.00' ||
	   				$row['rating'] == '3.00' ||
		   			$row['rating'] == '2.00' ||
		   			$row['rating'] == '1.00'
	   			 ) {
	   			 	$r = explode('.',$row['rating']);
	   			 	$row['name'].=' - '.$r[0].' *';
	   			 }
	   			 elseif (
	   				$row['rating'] == '4.50' ||
	   				$row['rating'] == '3.50' ||
		   			$row['rating'] == '2.50' ||
		   			$row['rating'] == '1.50'
	   			 ) {
	   			 	$r = explode('.',$row['rating']);
	   			 	$row['name'].=' - '.$r[0].' * +';
	   			 }
	   			 elseif (
	   				$row['rating'] == '5.30' ||
	   				$row['rating'] == '4.30' ||
		   			$row['rating'] == '3.30' ||
		   			$row['rating'] == '2.30' ||
		   			$row['rating'] == '1.30'
	   			 ) {
	   			 	$r = explode('.',$row['rating']);
	   			 	$row['name'].=' - '.$r[0].' tridenti';
	   			 }
	   			 elseif (
	   				$row['rating'] == '5.20'
	   			 ) {
	   			 	$row['name'].=' - HV1';
	   			 }
	   			 elseif (
	   				$row['rating'] == '4.20'
	   			 ) {
	   			 	$row['name'].=' - HV2';
	   			 }
                $return['items'][] = $row;
            }
        }
    }

    elseif ($mod == 'city'
            && isset($_GET['value'])
            && is_numeric($_GET['value']))
    {
        $statement = $db->query("
                select c.`id`, cd.`name`
                from
                    `xp_cities` c
                    inner join `xp_cities_data` cd
                        on c.`id` = cd.`_id`
                where
                    cd.`lang` = ?
                    and c.`id_country` = ?
                    and c.`status` = 1
	            order by cd.`name`",
                array('en', $_GET['value']));

        if ($statement) {
            $return = array(
                'success' => true,
                'items' => array()
            );

            while ($row = $statement->fetch()) {
                $return['items'][] = $row;
            }
        }
    }

    elseif ($mod == 'hotel'
            && isset($_GET['value'])
            && is_numeric($_GET['value']))
    {
        $statement = $db->query("
                select c.`id`, concat(cd.`name`, ' - ', c.`rating`, '*') name
                from
                    `xp_hotels` c
                    inner join `xp_hotels_data` cd
                        on c.`id` = cd.`_id`
                where
                    c.`id_city` = ?
	            order by c.`rating` desc, cd.`name`",
                array($_GET['value']));

        if ($statement) {
            $return = array(
                'success' => true,
                'items' => array(
                    0 => array(
                        'id' => 0,
                        'name' => tr('Alege hotel')
                    )
                )
            );

            while ($row = $statement->fetch()) {
                $return['items'][] = $row;
            }
        }
    }

    elseif ($mod == 'sejur'
            && isset($_GET['id'], $_GET['duration'], $_GET['index'], $_GET['transport'], $_GET['room'], $_GET['board'], $_GET['adults'], $_GET['children'], $_GET['date_start'])
            && is_numeric($_GET['id'])
            && is_numeric($_GET['duration'])
            && is_numeric($_GET['index'])
            && is_numeric($_GET['transport'])
    ) {
        $sejur = $db->fetchRow("
                select s.*, c.`token`
                from
                    `xp_sejur` s
                    inner join `xp_sejur_data` sd
                        on s.`id` = sd.`_id`
                    inner join `xp_currencies` c
                        on c.`id` = s.`id_currency`
                where
                    s.`status` = 1
                    and s.`id` = ?",
                array($_GET['id']));

        if ($sejur) {
            $select = $db->select()
                         ->from(array('he' => 'fibula_hotels_entities'),
                                array(new Zend_Db_Expr('distinct `he`.*')))
                         ->where('`he`.`id_hotel` = ?', $sejur['id_hotel']);

            if (is_numeric($_GET['adults'])) {
                $select->where('`he`.`adults` = ?', $_GET['adults']);
            }

            if (is_numeric($_GET['children'])) {
                $select->where('`he`.`children` = ?', $_GET['children']);
            }

            $select->where(new Zend_Db_Expr('exists (' . $db->select()
                                                               ->from(array('sc' => 'xp_sejur_cost'),
                                                                      array(new Zend_Db_Expr('distinct `id_hotel_entity`')))
                                                               ->where('`sc`.`id_sejur` = ?', $sejur['id'])
                                                               ->where('`sc`.`id_room_type` = ?', $_GET['room'])
                                                               ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                                               ->where('`sc`.`nights` = ?', $_GET['duration'])
                                                               ->where('`sc`.`id_board_type` = ?', $_GET['board'])
                                                               ->where('`sc`.`id_hotel_entity` = `he`.`id`')
                                                               ->limit(1)
                                                       . ')'));

            $availableChildrenEntity = $availableChildrenCount = array();
            
           //print_a($i);
           //print_a($_GET['children']);
            
            if ($_GET['children']>0) {

	            for ($i = 0; $i < $_GET['children']; $i++) {
	                if (isset($_GET['children_ages'][$i])) {
	                    $selectHec{$i} = $db->select()
	                                        ->from(array('hec' => 'fibula_hotels_entities_children'),
	                                               array('mindif' . $i => new Zend_Db_Expr('max(' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ' - `hec`.`age_start`)'),
	                                                     'maxdif' . $i => new Zend_Db_Expr('max(`hec`.`age_end` - ' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ')'),
	                                                     'id_hotel_entity',
	                                                     'count',
	                                                     'id'))
	                                        ->joinInner(array('he' => 'fibula_hotels_entities'),
	                                                    '`he`.`id` = `hec`.`id_hotel_entity`',
	                                                    array(''))
	                                        ->where('`hec`.`age_start` <= ? and `hec`.`age_end` >= ?', $_GET['children_ages'][$i])
	                                        ->where('`he`.`id_hotel` = ?', $sejur['id_hotel'])
	                                        ->group('id');
	
	                    if (is_numeric($_GET['adults'])) {
	                        $selectHec{$i}->where('`he`.`adults` = ?', $_GET['adults']);
	                    }
	
	                    if (is_numeric($_GET['children'])) {
	                        $selectHec{$i}->where('`he`.`children` = ?', $_GET['children']);
	                    }
	
	                    $select->joinInner(array('hec' . $i => $selectHec{$i}),
	                                       '`he`.`id` = `hec' . $i . '`.`id_hotel_entity`',
	                                       array(''));
	
	                    $select->order('mindif' . $i);
	                    $select->order('maxdif' . $i);
	
	                    $availableChildrenEntity[] = '`hec' . $i . '`.`id`';
	                    $availableChildrenCount[] = '`hec' . $i . '`.`count`';
	                }
	            }
            }

           // print_a($availableChildrenEntity);
            
            if (count($availableChildrenEntity) > 0) {
                $select->where(implode($availableChildrenEntity, ' <> '))
                       ->where(implode($availableChildrenCount, ' + ') . ' = ?', $_GET['children']);
            }
//            print_a($select->__toString());
            $hotel_entity = $db->fetchRow($select);
//             print_a(microtime(true) - $time);exit();

            if ($hotel_entity) {
                $date_start = DateTime::createFromFormat('Y-m', $_GET['date_start']);

                if ($date_start) {
                    $_GET['date_start'] .= '-01';

                    if (! isset($_GET['date_end'])) {
                        $_GET['date_end'] = (intval($date_start->format('m'), 10) == 12 ? $date_start->format('Y') + 1 : $date_start->format('Y')) . '-' . sprintf('%02d', (intval($date_start->format('m'), 10) == 12 ? 1 : intval($date_start->format('m'), 10) + 1)) . '-01';
                    }

                    $select = $db->select()
                                 ->from(array('sc' => 'xp_sejur_cost'),
                                        array('*'))
                                 ->where('sc.`id_hotel_entity` = ?', $hotel_entity['id'])
                                 ->where('sc.`id_sejur` = ?', $_GET['id'])
                                 ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                 ->where('sc.`date_start` >= ? and sc.`date_start` >=now()', $_GET['date_start'])
                                 ->where('sc.`date_start` < ?', $_GET['date_end'])
                                 ->where('sc.`id_room_type` = ?', $_GET['room'])
                                 ->where('sc.`nights` = ?', $_GET['duration'])
                                 ->where('sc.`id_board_type` = ?', $_GET['board'])
                                 ->order('sc.date_start asc');

                    $statement = $db->query($select);

                    if ($statement) {
                    	$max_week = 0;

                        $return['interval'] = array();

                        while ($row = $statement->fetch()) {
                            $start = new DateTime($row['date_start']);

                            $day = $start->format('w');
                            $max_week = (intval($start->format('W'), 10) > $max_week ? intval($start->format('W'), 10) : $max_week);

                            $return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)] = array_merge(
                                (isset($return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)])
                                        ? $return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)]
                                        : array()),
                                array(
                                    'price' . ($row['is_standard_price'] == 2 ? '_discount' : '') => sprintf('%0d', $row['price']),
                                    'day' => $start->format('d'),
                                    'date' => $start->format('Y-m-d'),
                                    'id' => $row['id'],
                                ));
                        }

                        $start = new DateTime($_GET['date_start']);
                        $end = new DateTime($_GET['date_end']);

                        $interval = $start->diff($end, true);

                        if ($interval) {
                        	$max_week = 0;

                            for ($i = 0; $i < $interval->format('%a'); $i++) {
                                $day = $start->format('w');
                                $max_week = (intval($start->format('W'), 10) > $max_week ? intval($start->format('W'), 10) : $max_week);

                                if (! isset($return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)])) {
                                    $return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)] = array(
                                        'price' => 0,
                                        'day' => $start->format('d'),
                                        'date' => $start->format('Y-m-d'),
                                        'id' => 0,
                                    );
                                }

                                $start->modify('+1 day');
                            }
                        }

                        $content = '<div id="prices-calendar-controller"><span class="sprite left"></span><span class="sprite right"></span><strong class="active" data-index="' . $_GET['index'] . '">' . mb_ucfirst(strftime('%B %Y',$date_start->format('U'))).  '</strong></div><table id="prices-calendar"><thead><tr><th><div>Lu.</div></th><th><div>Ma.</div></th><th><div>Mi.</div></th><th><div>Jo.</div></th><th><div>Vi.</div></th><th><div>Sa.</div></th><th><div>Du.</div></th></tr></thead><tbody>';
                        ksort($return['interval']);

                        foreach ($return['interval'] as $interval) {
                            foreach ($interval as $months => $weeks) {
                                ksort($weeks);

                                foreach ($weeks as $days) {
                                    $content .= '<tr>';

                                    for ($i = 1; $i < 8; $i++) {
                                        if (isset($days[$i])) {
                                            $day = $days[$i];
											$exchange = exchange($day['price'],$sejur['token'],0,'fibula',2);

											if (isset($day['price_discount'])) {
												$exchange2 = exchange($day['price_discount'],$sejur['token'],0,'fibula',2);
											}

                                            $content .= '<td><div class="exist' . (isset($day['price_discount']) ? ' discount' : '') . '" data-price="'
                                                      . (isset($day['price_discount']) ? $day['price_discount'] : $day['price'])
                                                      . '" data-date="' . $day['date'] . '"><span style="color:#DB6815">' . $day['day'] . '</span>'
                                                      . (isset($day['price']) || isset($day['price_discount'])
                                                      	? '<span>' . (isset($day['price_discount'])
                                                      				  ? '<strong style="color:#c00;">'.$exchange2['exchange']['EURO'].'</strong>'
                                                      				  : ($day['price'] ? $exchange['exchange']['EURO'] : '')) . '</span>'
                                          				: '')
                                                      . '</div></td>';
                                        } else {
                                            $content .= '<td><div></div></td>';
                                        }
                                    }

                                    $content .= '</tr>';
                                }
                            }
                        }

                        $content .= '</tbody></table>';
                        $content .= '<p><span style ="color:#ee3000;">* Tarifele sunt exprimate in Euro / Camera</span></br> <span style="font-size:13px">Pentru a efectua rezervarea, va rugam sa faceti click pe una dintre variante</span></p>';

                        $return['table'] = $content;
                    }
                }
            }

//             unset($return['interval']);

            if (isset($return['table'])) {
                unset($return['error']);
            }
        }
    }

    elseif ($mod == 'circuit'
            && isset($_GET['id'], $_GET['duration'], $_GET['index'], $_GET['transport'], $_GET['room'], $_GET['board'], $_GET['adults'], $_GET['children'], $_GET['date_start'])
            && is_numeric($_GET['id'])
            && is_numeric($_GET['duration'])
            && is_numeric($_GET['index'])
            && is_numeric($_GET['transport'])
    ) {
        $sejur = $db->fetchRow("
                select s.*, c.`token`
                from
                    `xp_circuit` s
                    inner join `xp_circuit_data` sd
                        on s.`id` = sd.`_id`
                    inner join `xp_currencies` c
                        on c.`id` = s.`id_currency`
                where
                    s.`status` = 1
                    and s.`id` = ?",
                array($_GET['id']));

        if ($sejur) {
            $select = $db->select()
                         ->from(array('he' => 'fibula_hotels_entities'),
                                array(new Zend_Db_Expr('distinct `he`.*')))
                         ->where('`he`.`id_hotel` = ?', $sejur['id_hotel']);

            if (is_numeric($_GET['adults'])) {
                $select->where('`he`.`adults` = ?', $_GET['adults']);
            }

            if (is_numeric($_GET['children'])) {
                $select->where('`he`.`children` = ?', $_GET['children']);
            }

            $select->where(new Zend_Db_Expr('exists (' . $db->select()
                                                               ->from(array('sc' => 'xp_circuit_cost'),
                                                                      array(new Zend_Db_Expr('distinct `id_hotel_entity`')))
                                                               ->where('`sc`.`id_circuit` = ?', $sejur['id'])
                                                               ->where('`sc`.`id_room_type` = ?', $_GET['room'])
                                                               ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                                               ->where('`sc`.`nights` = ?', $_GET['duration'])
                                                               ->where('`sc`.`id_board_type` = ?', $_GET['board'])
                                                               ->where('`sc`.`id_hotel_entity` = `he`.`id`')
                                                               ->limit(1)
                                                       . ')'));

            $availableChildrenEntity = $availableChildrenCount = array();

            for ($i = 0; $i < $_GET['children']; $i++) {
                if (isset($_GET['children_ages'][$i])) {
                    $selectHec{$i} = $db->select()
                                        ->from(array('hec' => 'fibula_hotels_entities_children'),
                                               array('mindif' . $i => new Zend_Db_Expr('max(' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ' - `hec`.`age_start`)'),
                                                     'maxdif' . $i => new Zend_Db_Expr('max(`hec`.`age_end` - ' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ')'),
                                                     'id_hotel_entity',
                                                     'count',
                                                     'id'))
                                        ->joinInner(array('he' => 'fibula_hotels_entities'),
                                                    '`he`.`id` = `hec`.`id_hotel_entity`',
                                                    array(''))
                                        ->where('`hec`.`age_start` <= ? and `hec`.`age_end` >= ?', $_GET['children_ages'][$i])
                                        ->where('`he`.`id_hotel` = ?', $sejur['id_hotel'])
                                        ->group('id');

                    if (is_numeric($_GET['adults'])) {
                        $selectHec{$i}->where('`he`.`adults` = ?', $_GET['adults']);
                    }

                    if (is_numeric($_GET['children'])) {
                        $selectHec{$i}->where('`he`.`children` = ?', $_GET['children']);
                    }

                    $select->joinInner(array('hec' . $i => $selectHec{$i}),
                                       '`he`.`id` = `hec' . $i . '`.`id_hotel_entity`',
                                       array(''));

                    $select->order('mindif' . $i);
                    $select->order('maxdif' . $i);

                    $availableChildrenEntity[] = '`hec' . $i . '`.`id`';
                    $availableChildrenCount[] = '`hec' . $i . '`.`count`';
                }
            }

            if (count($availableChildrenEntity) > 0) {
                $select->where(implode($availableChildrenEntity, ' <> '))
                       ->where(implode($availableChildrenCount, ' + ') . ' = ?', $_GET['children']);
            }
//            print_a($select->__toString());
            $hotel_entity = $db->fetchRow($select);
//             print_a(microtime(true) - $time);exit();

            if ($hotel_entity) {
                $date_start = DateTime::createFromFormat('Y-m', $_GET['date_start']);

                if ($date_start) {
                    $_GET['date_start'] .= '-01';

                    if (! isset($_GET['date_end'])) {
                        $_GET['date_end'] = date('Y-') . sprintf('%02d', (intval($date_start->format('m')) + 1)) . '-01';
                    }

                    $select = $db->select()
                                 ->from(array('sc' => 'xp_circuit_cost'),
                                        array('*'))
                                 ->where('sc.`id_hotel_entity` = ?', $hotel_entity['id'])
                                 ->where('sc.`id_circuit` = ?', $_GET['id'])
                                 ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                 ->where('sc.`date_start` >= ?', $_GET['date_start'])
                                 ->where('sc.`date_start` < ? and sc.`date_start` >=now()', $_GET['date_end'])
                                 ->where('sc.`id_room_type` = ?', $_GET['room'])
                                 ->where('sc.`nights` = ?', $_GET['duration'])
                                 ->where('sc.`id_board_type` = ?', $_GET['board'])
                                 ->order('sc.is_standard_price asc');

                    $statement = $db->query($select);

                    if ($statement) {
                        $return['interval'] = array();

                        while ($row = $statement->fetch()) {
                            $start = new DateTime($row['date_start']);

                            $day = $start->format('w');

                            $return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)] = array_merge(
                                (isset($return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)])
                                        ? $return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)]
                                        : array()),
                                array(
                                    'price' . ($row['is_standard_price'] == 2 ? '_discount' : '') => sprintf('%0d', $row['price']),
                                    'day' => $start->format('d'),
                                    'date' => $start->format('Y-m-d'),
                                    'id' => $row['id'],
                                ));
                        }

                        $start = new DateTime($_GET['date_start']);
                        $end = new DateTime($_GET['date_end']);

                        $interval = $start->diff($end, true);

                        if ($interval) {
                        	$max_week = 0;

                            for ($i = 0; $i < $interval->format('%a'); $i++) {
                                $day = $start->format('w');
                                $max_week = (intval($start->format('W'), 10) > $max_week ? intval($start->format('W'), 10) : $max_week);

                                if (! isset($return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)])) {
                                    $return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)] = array(
                                        'price' => 0,
                                        'day' => $start->format('d'),
                                        'date' => $start->format('Y-m-d'),
                                        'id' => 0,
                                    );
                                }

                                $start->modify('+1 day');
                            }
                        }

                        $content = '<div id="prices-calendar-controller"><span class="sprite left"></span><span class="sprite right"></span><strong class="active" data-index="' . $_GET['index'] . '">' . mb_ucfirst(strftime('%B %Y',$date_start->format('U'))).  '</strong></div><table id="prices-calendar"><thead><tr><th><div>Lu.</div></th><th><div>Ma.</div></th><th><div>Mi.</div></th><th><div>Jo.</div></th><th><div>Vi.</div></th><th><div>Sa.</div></th><th><div>Du.</div></th></tr></thead><tbody>';
                        ksort($return['interval']);

                        foreach ($return['interval'] as $interval) {
                            foreach ($interval as $months => $weeks) {
                                ksort($weeks);

                                foreach ($weeks as $days) {
                                    $content .= '<tr>';

                                    for ($i = 1; $i < 8; $i++) {
                                        if (isset($days[$i])) {
                                            $day = $days[$i];

                                            $content .= '<td><div class="exist' . (isset($day['price_discount']) ? ' discount' : '') . '" data-price="'
                                                      . (isset($day['price_discount']) ? $day['price_discount'] : $day['price'])
                                                      . '" data-date="' . $day['date'] . '"><span style="color:#c00">' . $day['day'] . '</span>'
                                                      . (isset($day['price']) || isset($day['price_discount'])
                                                      	? '<span>' . (isset($day['price_discount'])
                                                      				  ? '<strong>'.$day['price_discount'].'</strong>'
                                                      				  : ($day['price'] ? $day['price'] : '')) . '</span>'
                                          				: '')
                                                      . '</div></td>';
                                        } else {
                                            $content .= '<td><div></div></td>';
                                        }
                                    }

                                    $content .= '</tr>';
                                }
                            }
                        }

                        $content .= '</tbody></table>';
                        $content .= '<p>* Tarifele sunt exprimate in Euro / Camera</br> <span style="font-size:13px">Pentru a efectua rezervarea, va rugam sa faceti click pe una dintre variante</span></p>';

                        $return['table'] = $content;
                    }
                }
            }

//             unset($return['interval']);

            if (isset($return['table'])) {
                unset($return['error']);
            }
        }
    }

    elseif ($mod == 'croaziera'
            && isset($_GET['id'], $_GET['duration'], $_GET['index'], $_GET['transport'], $_GET['room'], $_GET['board'], $_GET['adults'], $_GET['children'], $_GET['date_start'])
            && is_numeric($_GET['id'])
            && is_numeric($_GET['duration'])
            && is_numeric($_GET['index'])
            && is_numeric($_GET['transport'])
    ) {
        $sejur = $db->fetchRow("
                select s.*, c.`token`
                from
                    `xp_croaziera` s
                    inner join `xp_croaziera_data` sd
                        on s.`id` = sd.`_id`
                    inner join `xp_currencies` c
                        on c.`id` = s.`id_currency`
                where
                    s.`status` = 1
                    and s.`id` = ?",
                array($_GET['id']));

        if ($sejur) {
            $select = $db->select()
                         ->from(array('he' => 'fibula_hotels_entities'),
                                array(new Zend_Db_Expr('distinct `he`.*')))
                         ->where('`he`.`id_hotel` = ?', $sejur['id_hotel']);

            if (is_numeric($_GET['adults'])) {
                $select->where('`he`.`adults` = ?', $_GET['adults']);
            }

            if (is_numeric($_GET['children'])) {
                $select->where('`he`.`children` = ?', $_GET['children']);
            }

            $select->where(new Zend_Db_Expr('exists (' . $db->select()
                                                               ->from(array('sc' => 'xp_croaziera_cost'),
                                                                      array(new Zend_Db_Expr('distinct `id_hotel_entity`')))
                                                               ->where('`sc`.`id_croaziera` = ?', $sejur['id'])
                                                               ->where('`sc`.`id_room_type` = ?', $_GET['room'])
                                                               ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                                               ->where('`sc`.`nights` = ?', $_GET['duration'])
                                                               ->where('`sc`.`id_board_type` = ?', $_GET['board'])
                                                               ->where('`sc`.`id_hotel_entity` = `he`.`id`')
                                                               ->limit(1)
                                                       . ')'));

            $availableChildrenEntity = $availableChildrenCount = array();

            for ($i = 0; $i < $_GET['children']; $i++) {
                if (isset($_GET['children_ages'][$i])) {
                    $selectHec{$i} = $db->select()
                                        ->from(array('hec' => 'fibula_hotels_entities_children'),
                                               array('mindif' . $i => new Zend_Db_Expr('max(' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ' - `hec`.`age_start`)'),
                                                     'maxdif' . $i => new Zend_Db_Expr('max(`hec`.`age_end` - ' . $db->quote($_GET['children_ages'][$i], 'INTEGER') . ')'),
                                                     'id_hotel_entity',
                                                     'count',
                                                     'id'))
                                        ->joinInner(array('he' => 'fibula_hotels_entities'),
                                                    '`he`.`id` = `hec`.`id_hotel_entity`',
                                                    array(''))
                                        ->where('`hec`.`age_start` <= ? and `hec`.`age_end` >= ?', $_GET['children_ages'][$i])
                                        ->where('`he`.`id_hotel` = ?', $sejur['id_hotel'])
                                        ->group('id');

                    if (is_numeric($_GET['adults'])) {
                        $selectHec{$i}->where('`he`.`adults` = ?', $_GET['adults']);
                    }

                    if (is_numeric($_GET['children'])) {
                        $selectHec{$i}->where('`he`.`children` = ?', $_GET['children']);
                    }

                    $select->joinInner(array('hec' . $i => $selectHec{$i}),
                                       '`he`.`id` = `hec' . $i . '`.`id_hotel_entity`',
                                       array(''));

                    $select->order('mindif' . $i);
                    $select->order('maxdif' . $i);

                    $availableChildrenEntity[] = '`hec' . $i . '`.`id`';
                    $availableChildrenCount[] = '`hec' . $i . '`.`count`';
                }
            }

            if (count($availableChildrenEntity) > 0) {
                $select->where(implode($availableChildrenEntity, ' <> '))
                       ->where(implode($availableChildrenCount, ' + ') . ' = ?', $_GET['children']);
            }
//             print_a($select->__toString());
            $hotel_entity = $db->fetchRow($select);
//             print_a(microtime(true) - $time);exit();

            if ($hotel_entity) {
                $date_start = DateTime::createFromFormat('Y-m', $_GET['date_start']);

                if ($date_start) {
                    $_GET['date_start'] .= '-01';

                    if (! isset($_GET['date_end'])) {
//                         $_GET['date_end'] = date('Y-') . sprintf('%02d', (intval($date_start->format('m')) + 1)) . '-01';
                        $_GET['date_end'] = $date_start->format('Y') . '-' . sprintf('%02d', (intval($date_start->format('m')) + 1)) . '-01';
                    }

                    $select = $db->select()
                                 ->from(array('sc' => 'xp_croaziera_cost'),
                                        array('*'))
                                 ->where('sc.`id_hotel_entity` = ?', $hotel_entity['id'])
                                 ->where('sc.`id_croaziera` = ?', $_GET['id'])
                                 ->where('`sc`.`id_sejur_transport` = ?', $_GET['transport'])
                                 ->where('sc.`date_start` >= ? and sc.`date_start` >=now()', $_GET['date_start'])
                                 ->where('sc.`date_start` < ?', $_GET['date_end'])
                                 ->where('sc.`id_room_type` = ?', $_GET['room'])
                                 ->where('sc.`nights` = ?', $_GET['duration'])
                                 ->where('sc.`id_board_type` = ?', $_GET['board'])
                                 ->order('sc.is_standard_price asc');

                    $statement = $db->query($select);

                    if ($statement) {
                        $return['interval'] = array();

                        while ($row = $statement->fetch()) {
                            $start = new DateTime($row['date_start']);

                            $day = $start->format('w');

                            $return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)] = array_merge(
                                (isset($return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)])
                                        ? $return['interval'][$start->format('Y')][$start->format('n')][$start->format('W')][($day > 0 ? $day : 7)]
                                        : array()),
                                array(
                                    'price' . ($row['is_standard_price'] == 2 ? '_discount' : '') => sprintf('%0d', $row['price']),
                                    'day' => $start->format('d'),
                                    'date' => $start->format('Y-m-d'),
                                    'id' => $row['id'],
                                ));
                        }

                        $start = new DateTime($_GET['date_start']);
                        $end = new DateTime($_GET['date_end']);

                        $interval = $start->diff($end, true);

                        if ($interval) {
                        	$max_week = 0;

                            for ($i = 0; $i < $interval->format('%a'); $i++) {
                                $day = $start->format('w');
                                $max_week = (intval($start->format('W'), 10) > $max_week ? intval($start->format('W'), 10) : $max_week);

                                if (! isset($return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)])) {
                                    $return['interval'][$start->format('Y')][$start->format('n')][(intval($start->format('W'), 10) < $max_week ? intval($start->format('W'), 10) + $max_week : intval($start->format('W'), 10))][($day > 0 ? $day : 7)] = array(
                                        'price' => 0,
                                        'day' => $start->format('d'),
                                        'date' => $start->format('Y-m-d'),
                                        'id' => 0,
                                    );
                                }

                                $start->modify('+1 day');
                            }
                        }

                        $content = '<div id="prices-calendar-controller"><span class="sprite left"></span><span class="sprite right"></span><strong class="active" data-index="' . $_GET['index'] . '">' . mb_ucfirst(strftime('%B %Y',$date_start->format('U'))).  '</strong></div><table id="prices-calendar"><thead><tr><th><div>Lu.</div></th><th><div>Ma.</div></th><th><div>Mi.</div></th><th><div>Jo.</div></th><th><div>Vi.</div></th><th><div>Sa.</div></th><th><div>Du.</div></th></tr></thead><tbody>';
                        ksort($return['interval']);

                        foreach ($return['interval'] as $interval) {
                            foreach ($interval as $months => $weeks) {
                                ksort($weeks);

                                foreach ($weeks as $days) {
                                    $content .= '<tr>';

                                    for ($i = 1; $i < 8; $i++) {
                                        if (isset($days[$i])) {
                                            $day = $days[$i];

                                            $content .= '<td><div class="exist' . (isset($day['price_discount']) ? ' discount' : '') . '" data-price="'
                                                      . (isset($day['price_discount']) ? $day['price_discount'] : $day['price'])
                                                      . '" data-date="' . $day['date'] . '"><span style="color:#376900">' . $day['day'] . '</span>'
                                                      . (isset($day['price']) || isset($day['price_discount'])
                                                      	? '<span>' . (isset($day['price_discount'])
                                                      				  ? '<strong style="color:#376900;">'.$day['price_discount'].'</strong>'
                                                      				  : ($day['price'] ? $day['price'] : '')) . '</span>'
                                          				: '')
                                                      . '</div></td>';
                                        } else {
                                            $content .= '<td><div></div></td>';
                                        }
                                    }

                                    $content .= '</tr>';
                                }
                            }
                        }

                        $content .= '</tbody></table>';
                        $content .= '<p>* Tarifele sunt exprimate in Euro / Camera</br> <span style="font-size:13px">Pentru a efectua rezervarea, va rugam sa faceti click pe una dintre variante</span></p>';

                        $return['table'] = $content;
                    }
                }
            }

//             unset($return['interval']);

            if (isset($return['table'])) {
                unset($return['error']);
            }
        }
    }

	elseif( $mod == 'age' &&
			isset($_GET['birthdate']) &&
			isset($_GET['checkin'])
	){
		if(DateTime::createFromFormat( 'd.m.Y H:i:s', $_GET['birthdate'].' 00:00:00' )){
			if(DateTime::createFromFormat( 'Y-m-d H:i:s', $_GET['checkin'].' 00:00:00' )){

				$age = age($_GET['birthdate'], $_GET['checkin'], 'Y-m-d');

				$return = array(
					'success' => true,
					'age' => $age
				);
			}
		}
	}

	elseif( $mod == 'search' &&
			isset($_GET['city']) &&
			$_GET['city']
	){
		$return = array(
			'success' => true,
			'c' => array()
		);

		$select = $db->select()
					 ->from(array('c' => 'xp_cities'),
							array('id_city' => 'id', 'id_country'))
					 ->join(array('cd' => 'xp_cities_data'),
							'cd.`_id` = c.`id`',
							array('city' => 'name'))
					 ->join(array('ctr' => 'xp_countries'),
							'ctr.`id` = c.`id_country`',
							array())
					 ->join(array('ctrd' => 'xp_countries_data'),
							'ctrd.`_id` = ctr.`id`',
							array('country' => 'name'))
					 /* ->where( 'lower(cd.`name`) like ?', mb_strtolower($_GET['city'])."%")
					 ->orWhere( 'lower(concat(cd.`name`, ", ", ctrd.`name`)) like ?', mb_strtolower($_GET['city'])."%") */
					 ->where(new Zend_Db_Expr('(lower(cd.`name`) like ? or lower(concat(cd.`name`, ", ", ctrd.`name`)) like ?)'), mb_strtolower($_GET['city']) . '%')
					 ->where('cd.`lang` = ?', 'en')
					 ->where('ctrd.`lang` = ?', 'en')
					 ->where('c.`status` = 1')
					 ->order('cd.name ASC');

		if( $statement = $db->query($select) ){
			while($row = $statement->fetch()){

				$return['c'][] = array(
					'ic' => $row['id_city'],
					'ict' => $row['id_country'],
					'c' => $row['city'],
					'ct' => $row['country'],
					'n' => $row['city'].', '.$row['country']
				);
			}
		}
	}
}

echo $h . json_encode($return) . $f;